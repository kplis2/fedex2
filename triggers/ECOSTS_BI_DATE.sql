--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOSTS_BI_DATE FOR ECOSTS                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable nextdate date;
  declare variable ref integer;
  declare variable todate date;
begin
  select min(c.fromdate)
    from ecosts c
    where c.employee = new.employee
      and c.fromdate > new.fromdate
    into :nextdate;

  select first 1 c.ref, c.todate
    from ecosts c
    where c.employee = new.employee
      and c.fromdate < new.fromdate
      order by c.fromdate desc
    into :ref, :todate;

  if (ref is not null and todate is null) then
    update ecosts set todate = new.fromdate - 1 where ref = :ref;

  if (new.todate is null) then
    new.todate = nextdate - 1;

  if (new.todate >= nextdate) then
    exception data_do_w_srodku_innego;
  if (todate is not null and new.fromdate <= todate) then
    exception data_do_w_srodku_innego;
end^
SET TERM ; ^
