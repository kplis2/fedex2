--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_AU_TOWARY FOR ATRYBUTY                       
  ACTIVE AFTER UPDATE POSITION 3 
as
declare variable atrakr varchar(20);
declare variable sql varchar(255);
begin
  if (new.wartosc<>old.wartosc or   (new.wartosc is not null and old.wartosc is null) or (new.wartosc is null and old.wartosc is not null)
    or (new.nrwersji = 0 and old.nrwersji <> 0)) then
  begin
    select DEFCECHY.redundantakr from DEFCECHY
      where defcechy.symbol=new.cecha
      into :atrakr;
    if (atrakr<> '') then
    begin
      sql = 'update TOWARY set '||atrakr||'='''||new.wartosc||''' where KTM='''||new.ktm||'''';
      insert into expimp (s2) values( :sql);
      execute statement :sql;
    end
  end
end^
SET TERM ; ^
