--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTKONTA_BIU FOR SPEDYTKONTA                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (new.oddzial is null) then
    execute procedure getconfig('AKTUODDZIAL') returning_values new.oddzial;
end^
SET TERM ; ^
