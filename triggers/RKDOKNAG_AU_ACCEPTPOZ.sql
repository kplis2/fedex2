--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AU_ACCEPTPOZ FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
BEGIN
  /* Trigger ma za zadanie zaakceptować pozycje dokumentu w nastepstwie akceptacji dokunentu
     a takze wycofanie akceptacji pozycji w nastepstwie wycofania akceptacji dokumewntu */

  -- wycofanie akceptacji pozycji
  IF (((OLD.NUMER > 0) AND (NEW.NUMER = 0 OR NEW.NUMER IS NULL))
    OR (COALESCE(OLD.STATUS,0) > 0 AND COALESCE(NEW.STATUS,0) = 0)
  ) THEN BEGIN
    -- najpierw wycofuje pozycje powodujace zmniejszenie stanów
    UPDATE RKDOKPOZ SET ACCEPT=0 WHERE DOKUMENT=NEW.REF AND ACCEPT=1 AND ((PM='-' AND KWOTA>0) OR (PM='+' AND KWOTA<0));
    UPDATE RKDOKPOZ SET ACCEPT=0 WHERE DOKUMENT=NEW.REF AND ACCEPT=1;
    EXECUTE PROCEDURE RKDOK_OBL_WAR(NEW.REF);
  END
  -- akceptacja pozycji
  IF (((NEW.NUMER > 0) AND (OLD.NUMER = 0 OR OLD.NUMER IS NULL))
    OR (COALESCE(OLD.STATUS,0) = 0 AND COALESCE(NEW.STATUS,0) > 0)
  ) THEN BEGIN
    -- najpierw akceptuje pozycje powodujace zwikszenie stanów
    UPDATE RKDOKPOZ SET ACCEPT=1 WHERE DOKUMENT=NEW.REF AND ACCEPT=0 AND PM <> NEW.PM ORDER BY lp;
    UPDATE RKDOKPOZ SET ACCEPT=1 WHERE DOKUMENT=NEW.REF AND COALESCE(ACCEPT,0)=0 AND ((PM='-' and KWOTA<0) or (PM='+' and KWOTA>0)) AND pm = NEW.pm ORDER BY lp;
    UPDATE RKDOKPOZ SET ACCEPT=1 WHERE DOKUMENT=NEW.REF AND COALESCE(ACCEPT,0)=0 ORDER BY lp;
    EXECUTE PROCEDURE RKDOK_OBL_WAR(NEW.REF);
  END
END^
SET TERM ; ^
