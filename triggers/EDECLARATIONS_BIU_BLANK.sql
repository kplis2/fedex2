--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLARATIONS_BIU_BLANK FOR EDECLARATIONS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
declare variable isgroup smallint;
declare variable groupcount integer;
declare variable groupsymbol type of column edeclarations.groupname;
begin
  --ustawienie infomracji kto i kiedy rejestrowal nieobecnosc

  if (new.person = 0) then new.person = null;
  if (new.emonth = 0) then new.emonth = null;
  if (new.eyear = 0) then new.eyear = null;
  if (new.period = '') then new.period = null;
  if (new.fromdate = '1899-12-30') then new.fromdate = null;
  if (new.todate = '1899-12-30') then new.todate = null;
  if (new.correction = 0) then new.correction = null;
  if (new.state is null) then new.state = 0;

  if (inserting) then
  begin
    if (new.regtimestamp is null) then
    begin
      execute procedure get_global_param ('AKTUOPERATOR')
        returning_values new.regoperator;
      new.regtimestamp = current_timestamp(0);
    end

    if (new.company is null) then
      execute procedure get_global_param('CURRENTCOMPANY')
        returning_values new.company;

    select isgroup, symbol from edecldefs
      where ref = new.edecldef
      into :isgroup, :groupsymbol;

    if (coalesce(new.groupname,'') = '' and isgroup = 1) then  --PR65868
    begin
      select count(ref) + 1 from edeclarations
        where edecldef = new.edecldef
          and company = new.company
          and eyear = new.eyear
        into :groupcount;

      groupsymbol = groupsymbol || '/' || new.eyear || '/';
      new.groupname = groupsymbol || groupcount;
      while(exists(select first 1 1 from edeclarations
              where groupname = new.groupname and eyear = new.eyear and company = new.company)
      ) do begin
        groupcount = groupcount + 1;
        new.groupname = groupsymbol || groupcount;
      end
    end
  end

  if (coalesce(new.groupname,'') <> '' and new.groupname is distinct from old.groupname) then
    if (exists(select first 1 1 from edeclarations where groupname = new.groupname and company = new.company)) then
      exception universal 'Istnieje już deklaracja zbiorcza o takiej nazwie!';

  if (coalesce(old.groupname,'') <> '' and coalesce(new.groupname,'') = '') then
    exception universal 'Nie można zmienić nazwy deklaracji zbiorczej na pustą!';
end^
SET TERM ; ^
