--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_PR_ITEM_BU_ORDER FOR RP_PR_ITEM                     
  ACTIVE BEFORE UPDATE POSITION 1 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 5) then exit;
  if (new.numer is null) then new.numer = old.numer;
  if (new.ord = 1 or new.numer = old.numer) then
  begin
    new.ord = 0;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.numer <> old.numer) then
    select max(numer) from rp_pr_item where program = new.program
      into :maxnum;
  else
    maxnum = 0;
  if (new.numer < old.numer) then
  begin
    update rp_pr_item set ord = 1, numer = numer + 1
      where ref_id <> new.ref_id and numer >= new.numer
        and numer < old.numer and program = new.program;
  end else if (new.numer > old.numer and new.numer <= maxnum) then
  begin
    update rp_pr_item set ord = 1, numer = numer - 1
      where ref_id <> new.ref_id and numer <= new.numer
        and numer > old.numer and program = new.program;
  end else if (new.numer > old.numer and new.numer > maxnum) then
    new.numer = old.numer;
end^
SET TERM ; ^
