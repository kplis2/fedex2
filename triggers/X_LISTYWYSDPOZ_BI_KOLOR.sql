--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_LISTYWYSDPOZ_BI_KOLOR FOR LISTYWYSDPOZ                   
  ACTIVE BEFORE INSERT POSITION 20 
AS
begin
  --[PM] XXX kolor w gridzie do pakowania
  if (coalesce(new.ilosc,0) = coalesce(new.iloscspk,0) ) then
    new.x_kolor = 1;
  else
    new.x_kolor = 0;
end^
SET TERM ; ^
