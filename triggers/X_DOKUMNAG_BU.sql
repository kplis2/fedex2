--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMNAG_BU FOR DOKUMNAG                       
  ACTIVE BEFORE UPDATE POSITION 20 
AS
begin
  --[PM] XXX #OPOZNIONEPAKOWANIE
  if (old.x_opoznionepakowanie is distinct from new.x_opoznionepakowanie
      and new.x_opoznionepakowanie is null
      ) then
    new.x_opoznionepakowanie = 0;
  if (new.zrodlo =99 and new.akcept=0 and old.akcept=1) then
    if (exists (select first 1 1 from mwsords m where m.ref=new.frommwsord and m.status<>2)) then
      exception universal 'Nie można wycofać akceptacji dokumentu, prosze wycofac zlecenie magazynowe.';
end^
SET TERM ; ^
