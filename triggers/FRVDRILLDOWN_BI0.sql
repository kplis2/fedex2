--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRVDRILLDOWN_BI0 FOR FRVDRILLDOWN                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRVDRILLDOWN')
      returning_values new.REF;
end^
SET TERM ; ^
