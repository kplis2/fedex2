--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWS_DOKUMNAG_BIU FOR DOKUMNAG                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.mwspreparegoods is null) then new.mwspreparegoods = 1;
  if (new.mwsdoc is null or (new.akcept = 1 and old.akcept <> 1) or inserting) then
    select mws from defmagaz where symbol = new.magazyn
      into new.mwsdoc;
  if (new.mwsdoc is null) then new.mwsdoc = 0;
  if (new.mwsdone is null) then new.mwsdone = 0;
  if (new.mwsblockmwsords is null) then new.mwsblockmwsords = 0;
  if (new.mwsdocreal is null) then new.mwsdocreal = 1;
  if (new.mwsmanacts is null) then new.mwsmanacts = 0;
  if (new.stockmwsord is null) then new.stockmwsord = 0;
  if (new.mwsordwaiting <> -1 or new.mwsordwaiting is null) then
  begin
    select mwsordwaiting from defdokummag where typ = new.typ and magazyn = new.magazyn
      into new.mwsordwaiting;
    if (new.mwsordwaiting is null) then new.mwsordwaiting = 0;
  end
  if (new.mwsrealprogress is null) then new.mwsrealprogress = '';
  if (new.mwsstatus is null) then new.mwsstatus = 0;
  if (new.mwsdisposition is null) then new.mwsdisposition = 0;
end^
SET TERM ; ^
