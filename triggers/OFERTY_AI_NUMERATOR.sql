--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERTY_AI_NUMERATOR FOR OFERTY                         
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable rnumer integer;
declare variable rsymbol varchar(40);
declare variable numerator varchar(40);
begin
 if(new.numer is null) then begin
    execute procedure GETCONFIG('OFERTYNUMERATOR') returning_values :numerator;
    if(:numerator is not null and :numerator<>'') then
    begin
      execute procedure GET_NUMBER(:numerator,'','',new.data, 0, new.ref) returning_values :rnumer, :rsymbol;
      update oferty set numer=:rnumer, symbol=:rsymbol where ref = new.ref;
    end
  end
end^
SET TERM ; ^
