--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AD_ZAMOWIENIE FOR RKDOKNAG                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable zamowienie integer;
begin
  if(old.numer>0 and old.anulowany=0) then begin
    for select ZAMOWIENIE
          from RKDOKPOZ
          where DOKUMENT=old.ref
          into :zamowienie
    do begin
      if(:zamowienie is not null and :zamowienie<>0) then
        execute procedure RK_OBLICZ_ZALICZKI(:zamowienie);
    end
  end
end^
SET TERM ; ^
