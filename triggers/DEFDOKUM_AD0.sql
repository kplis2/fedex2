--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDOKUM_AD0 FOR DEFDOKUM                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable aa integer;
begin
  if(old.symbol <> '') then begin
    select count(*) from DEFDOKUM where PRZECIW = old.symbol into :aa;
    if(:aa > 0) then exception
      DEFDOKUM_PRZECIW_FKU;
    select count(*) from DEFDOKUM where KORYGUJACY = old.symbol into :aa;
    if(:aa > 0) then exception
      DEFDOKUM_KORYGUJACY_FKU;
  end
end^
SET TERM ; ^
