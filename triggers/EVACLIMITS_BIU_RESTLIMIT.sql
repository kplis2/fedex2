--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BIU_RESTLIMIT FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 6 
as
begin
--DS: wyliczenie, ile urlopu pozostalo do wykorzystania

  if (inserting or new.actlimit <> old.actlimit or new.disablelimit <> old.disablelimit
    or new.prevlimit <> old.prevlimit or new.supplementallimit <> old.supplementallimit
    or new.limitcons <> old.limitcons or new.addlimit <> old.addlimit
    or new.vacreqlimit <> old.vacreqlimit or new.vacreqused <> old.vacreqused
    or new.vacmdlimit <> old.vacmdlimit or new.vacmdused <> old.vacmdused
  ) then
  begin
    new.restlimit = new.actlimit + new.prevlimit + new.addlimit
                  - new.limitcons + new.supplementallimit + new.disablelimit;
    new.vacmdremain = new.vacmdlimit - new.vacmdused;
    if(new.vacmdremain < 0) then new.vacmdremain = 0;
    new.vacreqremain = new.vacreqlimit - new.vacreqused;
  end
end^
SET TERM ; ^
