--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AU0 FOR POZFAK                         
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
 if((new.gr_vat <> old.gr_vat)
   or (new.vat <> old.vat)
   or (new.wartnet<> old.wartnet )
   or (new.wartbru <> old.wartbru)
   or (new.wartbruzl <> old.wartbruzl)
   or (new.wartnetzl <> old.wartnetzl)
   or (new.pwartnet<> old.pwartnet )
   or (new.pwartbru <> old.pwartbru)
   or (new.pwartbruzl <> old.pwartbruzl)
   or (new.pwartnetzl <> old.pwartnetzl)
   or (new.opk <> old.opk)
 ) then
    execute procedure FAK_POZ_CHANGED(new.dokument,new.ref, old.gr_vat, old.pgr_vat);
 if(new.waga <> old.waga) then
   update NAGFAK set WAGA = WAGA + (new.waga - old.waga) where ref =new.dokument;
 if(new.objetosc <> old.objetosc) then
   update NAGFAK set objetosc = objetosc + (new.objetosc - old.objetosc) where ref =new.dokument;
end^
SET TERM ; ^
