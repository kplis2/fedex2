--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CZAWODY_BI_REF FOR CZAWODY                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CZAWODY')
      returning_values new.REF;
end^
SET TERM ; ^
