--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLARATIONS_BI_REF FOR EDECLARATIONS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDECLARATIONS')
      returning_values new.REF;
end^
SET TERM ; ^
