--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FARELDOCS_BD_CLOSED FOR FARELDOCS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable period_status integer;
  declare variable company integer;
begin
    select company
      from fxdassets f
      where f.ref = old.fxdasset
    into :company;

    select status from amperiods
    where ammonth = extract(month from old.docdate)
    and amyear = extract(year from old.docdate)
    and company = :company
    into :period_status;
    if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można usunąć dokumentu dla zamkniętego okresu!';
end^
SET TERM ; ^
