--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKNOT_BD_WYCOF FOR NAGFAKNOT                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable numberg varchar(255);
declare variable rejestr varchar(3);
declare variable typ varchar(3);
declare variable BLOCKREF int;
begin
  if (old.AKCEPTACJA=1) then begin
    -- odakceptowanie noty jeśli usuwanie nie zostanie zablokowane...
    execute procedure GETCONFIG('NAGFAKNOTNUM') returning_values :numberg;
    select REJESTR,TYP from NAGFAK where REF=old.dokument into :rejestr,:typ;
    execute procedure FREE_NUMBER(:numberg, :rejestr, :typ, old.data, old.NUMER, null)
      returning_values :BLOCKREF;
  end
end^
SET TERM ; ^
