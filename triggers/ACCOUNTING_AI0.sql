--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCOUNTING_AI0 FOR ACCOUNTING                     
  ACTIVE AFTER INSERT POSITION 0 
as
  declare variable pid integer;
begin
  for
    select id from bkperiods
      where company = new.company and yearid = new.yearid
      into :pid
  do begin
    insert into turnovers (accounting, period, debit, credit, account, yearid, bktype, company, ebdebit, ebcredit)
      values (new.ref, :pid, 0, 0, new.account, new.yearid, new.bktype, new.company, 0, 0);
/*    insert into turnoversd (accounting, period, debit, credit, account, yearid, bktype, company, ebdebit, ebcredit)
      values (new.ref, :pid, 0, 0, new.account, new.yearid, new.bktype, new.company, 0, 0);
*/
      -- testowy komentarz do fb-diff-work - Mariusz Kukawski 2007-08-09
  end
end^
SET TERM ; ^
