--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHW_AU_REPLICAT FOR DEFCECHW                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
   if(
     new.wartstr <> old.wartstr or (new.wartstr is null and old.wartstr is not null) or (new.wartstr is not null and old.wartstr is null) or
     new.wartnum <> old.wartnum or (new.wartnum is null and old.wartnum is not null) or (new.wartnum is not null and old.wartnum is null) or
     new.wartdate <> old.wartdate or (new.wartdate is null and old.wartdate is not null) or (new.wartdate is not null and old.wartdate is null) or
     new.opis <> old.opis or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null) or
     new.kod <> old.kod or (new.kod is null and old.kod is not null) or (new.kod is not null and old.kod is null)
    )then
      update DEFCECHY set STATE=-1
        where SYMBOL=old.cecha;
end^
SET TERM ; ^
