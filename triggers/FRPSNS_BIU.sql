--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNS_BIU FOR FRPSNS                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable status smallint;
begin
  select status from frhdrs where symbol = new.frhdr
    into :status;

  if (new.kopia is null) then
    new.kopia = 0;

  if (status <> 0) then
    exception FRHDRS_NOT_ACCESIBLE;
end^
SET TERM ; ^
