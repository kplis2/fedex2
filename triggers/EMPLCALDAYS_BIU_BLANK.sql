--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_BIU_BLANK FOR EMPLCALDAYS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
--Trigger blankujacy, wynik polaczenia innych triggerow o charkterze balnka (BS34575)

  if (inserting or new.CDATE <> old.cdate) then
  begin
  --wczesniej jako EMPLCALDAYS_BI_DATES
    new.PDAY = extract(DAY from new.CDATE);
    new.PMONTH = extract(MONTH from new.CDATE);
    new.PYEAR = extract(YEAR from new.CDATE);
  end

  if (new.overtime_type is null) then new.overtime_type = 0;
  if (new.compensated_time is null) then new.compensated_time = 0;
  if (new.compensatory_time is null) then new.compensatory_time = 0;
  if (new.overtime0 is null) then new.overtime0 = 0;
  if (new.overtime50 is null) then new.overtime50 = 0;
  if (new.overtime100 is null) then new.overtime100 = 0;
  if (new.nighthours is null) then new.nighthours = 0;
  if (new.customized is null) then new.customized = 0;
end^
SET TERM ; ^
