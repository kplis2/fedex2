--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERBLOCK_AU0 FOR NUMBERBLOCK                    
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.number <> old.number or (new.number is not null and old.number is null)) and new.number > 0) then
    update NUMBERFREE set NUMBLOCK = new.ref where ref=new.number;

end^
SET TERM ; ^
