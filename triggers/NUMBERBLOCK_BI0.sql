--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERBLOCK_BI0 FOR NUMBERBLOCK                    
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable keyval varchar(20);
declare variable numoddzial varchar(20);
declare variable number integer;
declare variable lastdata timestamp;
declare variable isf_company smallint;
declare variable current_company integer;
begin
  if(new.numrejdok is null) then new.numrejdok = '';
  if(new.numtypdok is null) then new.numtypdok = '';
  if(new.archiwum is null) then new.archiwum = 0;

  select company from NUMBERGEN where nazwa = new.numerator into :isf_company;
  if (isf_company = 1) then
  begin
      execute procedure get_global_param('CURRENTCOMPANY')
        returning_values :current_company;
      if (new.company is null) then new.company = :current_company;
  end
  if (new.company is null) then new.company = - 1;
  if(new.number is null and new.data <= current_date) then
  begin
    execute procedure NUMBER_FORMAT(new.numerator, new.numrejdok, new.numtypdok, new.data) returning_values :keyval, :numoddzial;
    if(new.numnumber > 0) then
      select min(ref) from NUMBERFREE
      where nazwa = new.numerator and key_val = :keyval and lastdata = new.data and typ = 1 and number = new.numnumber and (:ISF_company != 1 or company = :current_company)
      into new.number;
    if(new.number is null) then
      select min(ref) from NUMBERFREE
      where nazwa = new.numerator and key_val = :keyval and lastdata = new.data and typ = 1 and (:ISF_company != 1 or company = :current_company)
      into new.number;
    if(new.number is null and new.data <= current_date)then begin
      /*brak wolnego - próba założenia nowego numeru*/
      select min(number), min(LASTDATA) from NUMBERFREE where oddzial = :numoddzial and nazwa=new.numerator and key_val = :keyval and typ = 0 and (:ISF_company != 1 or company = :current_company) into :number, :lastdata;
      if((new.data >= :lastdata or (:lastdata is null)) and new.data <= current_date) then begin
        if(:number is null) then begin
          /* zalozenie pierwszego rekordu w danej dziedzinie numeratora */
          insert into NUMBERFREE( ODDZIAL, NAZWA, KEY_VAL,NUMBER,TYP, LASTDATA, company) values(:numoddzial, new.numerator,:keyval,0,0,new.data, new.company);
          number = 0;
        end
        /*numer kolejny */
        number = number + 1;
        /* zapis ostatniego użytego numeru */
        update NUMBERFREE set NUMBER=:number, LASTDATA = new.data where oddzial = :numoddzial and nazwa=new.numerator and key_val =:keyval and typ = 0 and (:ISF_company != 1 or company = :current_company);
        execute procedure GEN_REF('NUMBERFREE') returning_values new.number;
        insert into NUMBERFREE (REF, NAZWA, KEY_VAL, NUMBER, TYP, ODDZIAL, LASTDATA, company)
          values (new.number, new.numerator, :keyval, :number, 1, :numoddzial, new.data, new.company);
      end
    end
    if(new.number is null) then exception NUMBERBLOCK_BRAKFREENUM;
    else select NUMBERFREE.number from NUMBERFREE where ref=new.number and (:ISF_company != 1 or company = :current_company) into new.numnumber;
  end else if(new.number > 0) then begin
    select numberfree.key_val, number, LASTDATA from NUMBERFREE where ref=new.number and (:ISF_company != 1 or company = :current_company) into :keyval, new.numnumber, new.data;
    new.numrejdok = substring(:keyval from 1 for 3);
    new.numtypdok = substring(:keyval from 4 for 3);
  end
end^
SET TERM ; ^
