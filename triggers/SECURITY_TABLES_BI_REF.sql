--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SECURITY_TABLES_BI_REF FOR SECURITY_TABLES                
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null) then
    execute procedure GEN_REF('SECURITY_TABLES')
      returning_values new.ref;
end^
SET TERM ; ^
