--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIGVALS_BI_REPLICAT FOR KONFIGVALS                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('KONFIGVALS',new.akronim, new.oddzial, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
