--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_BI FOR KONTAKTY                       
  ACTIVE BEFORE INSERT POSITION 3 
AS
BEGIN
  new.cplopermod = 0;
  if(new.ckontrakt=0) then new.ckontrakt=NULL;
  if(new.zadanie=0) then new.zadanie=NULL;
  if (new.data is null) then
    new.data = CURRENT_TIMESTAMP(0);
  new.datazal = CURRENT_TIMESTAMP(0);
  if(new.inout = 0) then new.przeczytano = 1;
END^
SET TERM ; ^
