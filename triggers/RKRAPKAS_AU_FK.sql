--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_AU_FK FOR RKRAPKAS                       
  ACTIVE AFTER UPDATE POSITION 1 
as
  declare variable konf smallint;
  declare variable bkref integer;
  declare variable blokadasid smallint;
  declare variable operator integer;
begin
  --sprawdzenie czy okres ksiegowy dla company jest juz zablokowany
  select min(sidblocked) from bkperiods
    where ptype = 1 and old.datado < fdate and  old.datado > sdate and company = old.company
    into :blokadasid;
  if (:blokadasid = 1) then exception SID_FK_BLOKADAKSIEGOWA;
  --akceptacja dokumentu - sprawdzenie, czy dekretowac
  if (new.status = 1 and old.status = 0) then
  begin
    -- zarejestrowanie dokumentu ksiegowego
    execute procedure GET_CONFIG('SIDFK_KASAUTO', 2) returning_values :konf;
    if(konf = '1') then begin
      execute procedure get_global_param('AKTUOPERATOR') returning_values :operator;
      execute procedure DOCS_TO_BKDOCS(2, new.ref, operator, 0, 1);
    end
  end else

  if (new.status = 0 and old.status > 0) then
  begin
    execute procedure GET_CONFIG('SIDFK_KASBLOK', 2) returning_values :konf;
    if (konf = '1' or konf = '2') then begin
      select max(ref) from BKDOCS
        where OTABLE = 'RKRAPKAS' and OREF = old.ref
        into :bkref;

      if(bkref > 0) then begin
        if (konf = '1') then exception RKRAPKAS_ZAKSIEGOWANY;
        else begin
          select sidblocked from bkperiods
            where ptype = 1 and old.datado < fdate and  old.datado > sdate and company = old.company
          into :blokadasid;
          if (:blokadasid = 1) then exception SID_FK_BLOKADAKSIEGOWA;
          execute procedure DOCUMENTS_DELFROM_BKDOCS(2, old.ref);
        end
      end
    end
  end
end^
SET TERM ; ^
