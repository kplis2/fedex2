--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_LISTYWYSDROZ_OPK_AU FOR LISTYWYSDROZ_OPK               
  ACTIVE AFTER UPDATE POSITION 50 
as
begin
-- sluzy do ujednolicenia symbolu dokumentu spedycyjnego przewoxnika dla wszystkich opakowan
  if (new.symbolsped <> old.symbolsped) then
  begin
    update listywysdroz_opk opk set symbolsped = new.symbolsped
      where opk.listwysd = new.listwysd and opk.ref <> new.ref;
    update listywysd sd set sd.symbolsped = new.symbolsped
      where ref = new.listwysd;
  end
end^
SET TERM ; ^
