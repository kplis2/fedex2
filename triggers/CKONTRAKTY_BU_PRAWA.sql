--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BU_PRAWA FOR CKONTRAKTY                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
 if(new.prawadef=0) then begin
   if((new.cpodmiot<>old.cpodmiot) or ((new.cpodmiot is not null) and (old.cpodmiot is null))) then
     select PRAWA,PRAWAGRUP from CPODMIOTY where ref = new.cpodmiot
     into new.prawa, new.prawagrup;
   else begin
     new.prawa = '';
     new.prawagrup = '';
   end
 end
 if(new.prawa<>old.prawa) then
   execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if((new.cpodmiot<>old.cpodmiot) or (new.cpodmiot is not null and old.cpodmiot is null)) then
   select SKROT,SLODEF,OPEROPIEK,COMPANY from CPODMIOTY where REF=new.CPODMIOT
   into new.cpodmskrot, new.cpodmslodef, new.cpodmoperopiek, new.company;
 else if(new.cpodmiot is null) then begin
   new.cpodmskrot = NULL;
   new.cpodmslodef = NULL;
   new.cpodmoperopiek = NULL;
 end
 if((new.osoba<>old.osoba) or (new.OSOBA is not NULL and old.osoba is null)) then
   select NAZWA from PKOSOBY where REF=new.OSOBA
   into new.PKOSOBYNAZWA;
 else if(new.osoba is null) then begin
   new.PKOSOBYNAZWA = NULL;
 end
 if((new.cpodmiot<>old.cpodmiot) or ((new.cpodmiot is not null) and (old.cpodmiot is null))) then
   select ancestors from CPODMIOTY where ref = new.cpodmiot into new.cpodmancestors;
end^
SET TERM ; ^
