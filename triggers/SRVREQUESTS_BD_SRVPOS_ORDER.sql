--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVREQUESTS_BD_SRVPOS_ORDER FOR SRVREQUESTS                    
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update srvpositions set ord = 0 where srvrequest = old.ref;
end^
SET TERM ; ^
