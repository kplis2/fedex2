--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMACTIVS_BI0 FOR PMACTIVS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.pmposition is not null) then 
  begin
    select PMPLAN, PMELEMENT from PMPOSITIONS
      where REF=new.pmposition into new.pmplan, new.pmelement;
    if(new.version is null or new.version='') then begin
    select version, versionnum from pmplans where ref=new.pmplan into new.version, new.versionnum;
    end
    if(new.mainref is null) then new.mainref=new.ref;
  end
end^
SET TERM ; ^
