--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RELATIONS_BI0 FOR RELATIONS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('RELATIONS') returning_values new.ref;
end^
SET TERM ; ^
