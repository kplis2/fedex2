--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLPOZ_BU0 FOR KPLPOZ                         
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable typ smallint;
declare variable cennik integer;
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.cenamag is null or (old.wersjaref <> new.wersjaref)) then begin
    select KPLNAG.typmag from KPLNAG where REF=new.nagkpl into :typ;
    execute procedure KOMPLET_GET_CENAMAG(new.ktm, new.wersjaref,:typ) returning_values new.cenamag;
  end
  if(new.cenaspr is null or (new.wersjaref <> old.wersjaref)) then begin
    select CENNIK from KPLNAG where REF=new.nagkpl into :cennik;
    select CENNIK.cenanet from CENNIK where CENNIK=:cennik and KTM = new.ktm and WERSJA = new.wersja into new.cenaspr;
  end
  if(new.cenamag is null) then new.cenamag = 0;
  if(new.cenaspr is null) then new.cenaspr = 0;
  new.wartmag = new.cenamag * new.ilosc;
  new.wartspr = new.cenaspr * new.ilosc;

end^
SET TERM ; ^
