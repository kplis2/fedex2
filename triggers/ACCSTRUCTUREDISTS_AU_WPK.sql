--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_AU_WPK FOR ACCSTRUCTUREDISTS              
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable is_pattern smallint_id;
declare variable slave_ref accstructure_id;
declare variable current_company companies_id;
begin
  -- Nalezy sprawdzic, czy nie zupdatowalismy wzorca - jezeli tak
  -- to synchronizujemy wszystkie podległe krotki
  if (coalesce(new.pattern_ref,0)=0) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :current_company;
    --Nie mamy pattern_ref wiec szansa ze jestesmy wzorcem, zbadajmy zakład
    select c.pattern
      from companies c
      where c.ref = :current_company
      into :is_pattern;
    if (coalesce(is_pattern,0)=1) then
    begin
      --jednak jestesmy wzorcem, wiec wynniśmy zsynchronizowac podlegle krotki
      for
        select a.ref
          from accstructuredists a
          where a.pattern_ref = new.ref
          into :slave_ref
      do begin
        --synchronizacja krotki z wzorcem, ustawiam internal = 1 , żeby nie uruchamiać mechanizmów synchronizacji wtórnej
        execute procedure synchronize_accstrucutredists(new.ref,:slave_ref,1);
      end
    end 
  end
end^
SET TERM ; ^
