--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_BI_REF FOR PKOSOBY                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PKOSOBY')
      returning_values new.REF;
end^
SET TERM ; ^
