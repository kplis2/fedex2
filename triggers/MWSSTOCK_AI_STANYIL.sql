--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_AI_STANYIL FOR MWSSTOCK                       
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (not exists(select wersjaref from stanyil where magazyn = new.wh and wersjaref = new.vers)
  ) then
    insert into STANYIL(MAGAZYN,KTM,WERSJA,ILOSC,CENA, WARTOSC)
      values(new.wh, new.good, new.versnumber, 0, 0, 0);
end^
SET TERM ; ^
