--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BU_BLOKADA FOR DOKUMNAG                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable blok smallint;
declare variable datablok timestamp;
begin
  if (new.akcept <> old.akcept ) then
  begin
    select defmagaz.blokada, defmagaz.datablokady from defmagaz  where defmagaz.symbol = new.magazyn
     into :blok, :datablok;
    if (blok = 1 and (new.data <= :datablok or :datablok is null)) then exception BLOKADA_MAGAZYNU;
  end
end^
SET TERM ; ^
