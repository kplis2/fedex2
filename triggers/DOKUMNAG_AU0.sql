--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AU0 FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable akcept smallint;
declare variable zew smallint;
declare variable kor smallint;
declare variable wyd smallint;
declare variable firma smallint;
declare variable flagi varchar(40);
declare variable blok varchar(40);
declare variable przekroczono numeric(14,2);
declare variable licznik_wdniu integer;
declare variable spzap INTEGER;
declare variable spzeur INTEGER;
declare variable spzeur2 INTEGER;
declare variable DOSTAWCA INTEGER;
declare variable kurseur numeric(14,4);
declare variable prog numeric(14,2);
declare variable sumwaga numeric(14,4);
declare variable faktura integer;
declare variable oldakcept varchar(25);
declare variable newakcept varchar(25);
declare variable blokada smallint;
declare variable nieobrot smallint;
declare variable wydania smallint;
declare variable dokumpozref integer;
declare variable defoperzam varchar(3);
declare variable wsadowo smallint;
declare variable histzam int;
declare variable status integer;
declare variable msg varchar(1024);
begin
  --artur
  if(new.akcept <> old.akcept) then begin
    if(old.akcept = 0) then
      oldakcept  = 'Niezaakceptowany';
    if(old.akcept = 1) then
      oldakcept = 'Zaakceptowany';
    if(old.akcept = 9) then
      oldakcept = 'Wycofany do poprawy';
    if(old.akcept = 10) then
      oldakcept = 'Wycofany do poprawy bez wycofania dokumentu magazynowego';
    if(new.akcept = 0) then
      newakcept = 'Niezaakceptowany';
    if(new.akcept = 1) then
      newakcept = 'Zaakceptowany';
    if(new.akcept = 9) then
      newakcept = 'Wycofany do poprawy';
    if(new.akcept = 10) then
      newakcept = 'Wycofany do poprawy bez wycofania dokumentu magazynowego';

    insert into security_log (logfil, tablename, item, key1, oldvalue, newvalue)
      values ('DOKUMNAG', 'DOKUMNAG', 'AKCEPTACJA', new.ref, :oldakcept, :newakcept);
  end
  --artur

  -- akceptacja/wycofanie dla wsadowego przetwarzania zamˇwie˝
  if(old.akcept <> new.akcept) then
  begin
    select coalesce(dp.wsadowo,0), dm.realzamoper
        from defdokummag dm
          left join defoperzam dp on (dm.realzamoper = dp.symbol)
        where dm.typ = new.typ
          and dm.magazyn = new.magazyn
      into :wsadowo, :defoperzam;
    if(:wsadowo = 1) then
    begin
      -- akceptacja
      if(old.akcept = 0 and new.akcept = 1) then
        execute procedure dokumnag_realoper(new.ref, :defoperzam, 'M');
      -- wycofanie akceptacji
      if((old.akcept = 1 and new.akcept = 0) or (old.akcept = 9 and new.akcept = 0)) then
      begin
        for
          select distinct h.ref
            from histzam h
            where h.dokumentmain = old.ref
          into :histzam
        do begin
          execute procedure oper_back_oper(0,:histzam,0,0,0)
            returning_values :status, :msg;
        end
      end
    end
  end

  if(new.kurs <> old.kurs) then
    update DOKUMPOZ set RABAT=-101 where dokument = new.ref;

-- MS: BS39572 jesli DOKUMNAG dotyczy wielu zamowien to sie nanosi to pierwsze i reszta sie nie podczepia
-- dotyczy przypadku kiedy FROMZAM is null czyli po uprzednim wycofaniu dokumentu
--  if((new.zamowienie <> old.zamowienie) or (new.zamowienie is not null and old.zamowienie is null))
--   then update DOKUMPOZ set FROMZAM = new.zamowienie where DOKUMENT = new.ref and FROMZAM is null;

  if((new.dostawa <> old.dostawa) or(new.dostawa is not null and old.dostawa is null) ) then begin
    if(new.akcept  <> 1)then
      update DOKUMPOZ set DOSTAWA = new.dostawa where DOKUMENT = new.ref and (DOSTAWA is null or DOSTAWA = 0 or (dostawa = old.dostawa and old.dostawa is not null));
    else if(old.akcept = 1) then
    exception  MAGDOK_DOST_CHANGE_NOT_ALLOW;
  end

  -- zmiana pola data na dokumpoz PR 12677
  if ((new.akcept = 1 and old.akcept <> 1) or (new.data is not null and old.data is null) or (new.data <> old.data)) then
    update dokumpoz set data = new.data where dokument = new.ref;
  -- zwolnienie blokad na magazynie przed akceptacja
  if(new.akcept=1 and old.akcept<>1 and new.zrodlo=0) then begin
    select DEFDOKUM.blokada , DEFDOKUM.NIEOBROT from DEFDOKUM where DEFDOKUM.SYMBOL=new.typ into :blokada, :nieobrot;
    if(:blokada = 1 and nieobrot <> 1) then begin
      for select REF from DOKUMPOZ where DOKUMENT = new.ref into :dokumpozref
      do
        delete from STANYREZ where POZZAM=:dokumpozref and ZREAL = 3;
    end
  end
  if(new.AKCEPT <> old.AKCEPT and ((new.akcept < 7) or (old.akcept < 7))) then begin
     akcept = new.akcept;
     /*wskazanie, ze to akceptacja dokumentu po wycofaniu do poprawy*/
     if(new.akcept > 0 and old.akcept = 9) then akcept = 5;
     execute procedure DOKUMNAG_ACK(new.ref,:AKCEPT);
     if(new.zamowienie > 0) then
       execute procedure MAG_CHECK_ZAM_ACK(new.zamowienie);
     if(new.zamowienie > 0 and new.akcept = 1) then

        execute procedure DOKUMNAG_OBLILREALONZAM(new.ref);

  end
  -- zalozenie ponowne blokad po wycofaniu akceptacji
  if(new.akcept=0 and old.akcept<>0 and new.zrodlo=0) then begin
    select DEFDOKUM.blokada , DEFDOKUM.NIEOBROT, DEFDOKUM.WYDANIA
      from DEFDOKUM
      where DEFDOKUM.SYMBOL=new.typ into :blokada, :nieobrot, :wydania;
    if(:blokada = 1 and :nieobrot <> 1 and :wydania=1) then begin
      for select REF from DOKUMPOZ where DOKUMENT = new.ref into :dokumpozref
      do
        execute procedure REZ_SET_KTM_FAK(:dokumpozref,3);
    end
  end
  /* pod-aczenie siŕ do faktury*/
  if(old.faktura <> new.faktura or (new.faktura is not null and old.faktura is null) or (new.faktura is null and old.faktura is not null) or (new.symbol <> old.symbol)) then begin
    if(new.faktura is not null) then
       execute procedure NAGFAK_UPDATE_MAGCONNECT(new.faktura);
    if(old.faktura is not null) then
       execute procedure NAGFAK_UPDATE_MAGCONNECT(old.faktura);
  end

  -- aktualizacja znacznika rozliczenia magazynowego
  if(new.akcept<>old.akcept) then begin
    for select distinct pozfak.dokument
      from dokumpoz
      join pozfak on (pozfak.ref=dokumpoz.frompozfak)
      where dokumpoz.dokument=new.ref
      into :faktura
    do begin
      execute procedure NAGFAK_OBL_MAGROZLICZ(:faktura);
    end
  end

  if((new.zamowienie <> old.zamowienie) or (new.zamowienie is null and old.zamowienie is not null)) then
      execute procedure NAGZAM_HASDOKMAGBEZFAK(old.zamowienie);
  if(new.zamowienie > 0 and
    (   (new.faktura is not null and old.faktura is null)
     or (new.faktura is null and old.faktura is not null)
     or (old.zamowienie is null)
     or (new.zamowienie <> old.zamowienie)
    )) then
      execute procedure NAGZAM_HASDOKMAGBEZFAK(new.zamowienie);
  if(new.numblock <> old.numblock or (new.numblock is null and old.numblock is not null)) then
    update NUMBERBLOCK set DOKDOKUMNAG = null where ref=old.numblock;
  if(new.numblock <> old.numblock or (new.numblock is not null and old.numblock is null)) then
    update NUMBERBLOCK set DOKDOKUMNAG = null where ref=new.numblock;
 if((new.sposdost <> old.sposdost) or (new.sposdost is not null and old.sposdost is null) or (new.sposdost is null and old.sposdost is not null)
   )then begin
   /*utrzymanie jednego sposobu dostawy w ramach grupy spedycyjnej */
   update DOKUMNAG set SPOSDOST = new.sposdost where GRUPASPED = new.grupasped and (SPOSDOST is null or (SPOSDOST <> new.sposdost));
   update LISTYWYSD set SPOSDOST = new.sposdost where LISTYWYSD.refdok = new.ref and listywysd.akcept = 0;
 end
  if(new.symbol <> old.symbol) then begin
    update DOKUMNAG set DOKUMNAG.grupaspeddesc = new.symbol where GRUPASPED = new.ref and ref <> new.ref ;
  end
 if(new.grupadruk > 0 and old.grupadruk is null) then
   update DOKUMNAG set GRUPADRUK = new.grupadruk where GRUPASPED = new.grupasped and GRUPADRUK is null;
  if(old.grupasped <> new.grupasped) then begin
    execute procedure dokumnag_grupased_koszt(old.grupasped);
    execute procedure dokumnag_grupased_koszt(new.grupasped);
  end else if(new.grupasped > 0 and (new.waga <> old.waga or (new.kosztdost <> old.kosztdost))) then begin
    execute procedure dokumnag_grupased_koszt(new.grupasped);
  end
 /*aktualizacja wartosci na dokumencie przeciwnym*/
 if(new.ref2 is not null and new.ref2<>0 and (new.wartosc<>old.wartosc or
   (new.wartosc is null and old.wartosc is not null) or (new.wartosc is not null and old.wartosc is null))) then
   update dokumnag d set d.wartosc2 = new.wartosc where d.ref = new.ref2;
 /* naliczenie wagi z dokumentow na rejestrze wjazdow i wyjazdow */
 if(new.akcept<>old.akcept and new.cinout is not null) then begin
   sumwaga = 0;
   select sum(WAGA) from DOKUMNAG where CINOUT=new.cinout and ((AKCEPT=1) or (AKCEPT=8))  into :sumwaga;
   update CINOUTS set DOCWEIGHT=:sumwaga where REF=new.cinout;
 end
 if((new.symbol <> old.symbol or (new.wartosc <> old.wartosc))
   and new.ref2 > 0
 ) then
   update DOKUMNAG set SYMBOL2 = new.symbol, WARTOSC2 = new.wartosc where ref=new.ref2;

  /* uzupelnianie tabeli splat po zmianie klienta/dostawcy */
  if(new.zrodlo = 0) then begin
    if (new.klient <> old.klient or new.dostawca<>old.dostawca) then begin
      delete from NAGFAKTERMPLAT where doktyp = 'M' and dokref = new.ref;
      if (new.dostawca is not null) then begin
        INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
          select 'M', new.ref, new.data +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 6 and k.slopoz = new.dostawca;
      end else if(new.klient is not null) then begin
        INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
          select 'M', new.ref, new.data +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 1 and k.slopoz = new.klient;
      end
    end
  end

end^
SET TERM ; ^
