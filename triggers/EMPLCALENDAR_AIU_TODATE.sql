--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALENDAR_AIU_TODATE FOR EMPLCALENDAR                   
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
as
declare variable todate date;
declare variable fromdate date;
declare variable enddate timestamp;
begin
/*MWr: Ustalenie czasu trwania rekordow przy edycji rekordu. Zmiana 'daty od'
       na rekordzie pociaga za soba odswiezenie wartosci TODATE na wszystkich
       wpisach w tabeli EMPLCALENDAR danego pracownika. Data '3000-01-01' sluzy
       do wywolania triggera podczas zmiany dat konca lub usuniecia umowy.*/

  if (inserting or new.fromdate <> old.fromdate or new.todate = '3000-01-01') then
  begin
  --ustalenie daty konca ostatniej umowy
    enddate = null;
    select max(coalesce(enddate, todate, '3000-01-01'))
      from emplcontracts
      where employee = new.employee
      into :enddate;

    if (enddate is null) then
    begin
    --nie wykryto umow, data TODATE dla najmlodszego wpisu zostanie uzupelniona koncem roku
      select cast(extract(year from max(fromdate)) || '/12/31' as date)
        from emplcalendar
        where employee = new.employee
        into: enddate;
    end else if (enddate = '3000-01-01') then
      enddate = null;

    --sprawdzenie, czy obecny stan umow nie spowoduje wpisu: fromdate > todate
    if (exists(select first 1 1 from emplcalendar
          where employee = new.employee and fromdate > :enddate))
    then
       exception emplcalendar_overrange;

  --ustalenie daty rozpoczyna sie od najmlodszego rekordu
    update emplcalendar
      set todate = :enddate
      where employee = new.employee
        and fromdate = (select max(fromdate)
                          from emplcalendar
                          where employee = new.employee);

  --przejscie przez wszystkie wpisy
    for
      select fromdate
        from emplcalendar
        where employee = new.employee
        order by fromdate desc
        into :fromdate
    do begin
    --aktualizacja TODATE dla rekordu wczesniejszego
      todate = fromdate - 1;
      update emplcalendar
        set todate = :todate
        where ref in (select first 1 ref from emplcalendar
                        where employee = new.employee
                          and fromdate < :fromdate
                        order by fromdate desc);
    end
  end
end^
SET TERM ; ^
