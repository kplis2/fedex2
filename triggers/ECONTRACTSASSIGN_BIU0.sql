--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRACTSASSIGN_BIU0 FOR ECONTRACTSASSIGN               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.accord is null) then new.accord = 0;
  if (new.employees is null) then
    exception EMPLOYEE_MUST_HAVE_A_VALUE;
end^
SET TERM ; ^
