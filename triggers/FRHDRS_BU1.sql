--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRHDRS_BU1 FOR FRHDRS                         
  ACTIVE BEFORE UPDATE POSITION 1 
AS
declare variable period varchar(6);
declare variable company integer;
declare variable regoper integer;
declare variable ref integer;
begin
  if (coalesce(old.status, 0) <> coalesce(new.status, 0) and coalesce(new.status, 0) = 1) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY') returning_values :company;
    execute procedure get_global_param('AKTUOPERATOR') returning_values :regoper;
    if (extract(month from current_date)<10) then
        period = extract(year from current_date) || '0' || extract(month from current_date);
    else
        period = extract(year from current_date) || extract(month from current_date);
    insert into frvhdrs (frhdr, period, company, regdate, regoper, frvtype)
        values (new.symbol,:period,:company,current_date, :regoper, 1) returning ref into :ref;

    delete from frvhdrs where ref = :ref;
  end
end^
SET TERM ; ^
