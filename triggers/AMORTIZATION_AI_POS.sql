--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMORTIZATION_AI_POS FOR AMORTIZATION                   
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable share numeric(14,2);
declare variable account ACCOUNT_ID;
declare variable dist1def integer;
declare variable dist1symbol SYMBOLDIST_ID;
declare variable dist2def integer;
declare variable dist2symbol SYMBOLDIST_ID;
declare variable dist3def integer;
declare variable dist3symbol SYMBOLDIST_ID;
declare variable dist4def integer;
declare variable dist4symbol SYMBOLDIST_ID;
declare variable dist5def integer;
declare variable dist5symbol SYMBOLDIST_ID;
declare variable calpos varchar(10);
declare variable sum_amount numeric(14,2);
declare variable pos_amount numeric(14,2);
declare variable sum_share numeric(14,2);
begin
  execute procedure get_config('CALCULATE_AMORTPOS',0) returning_values :calpos;
  sum_amount = 0;
  pos_amount = 0;
  sum_share = 0;
  if (new.tamort <> 0 and calpos = '1') then begin
    for select fa.share, fa.account, fa.dist1ddef, fa.dist1symbol,
          fa.dist2ddef, fa.dist2symbol, fa.dist3ddef, fa.dist3symbol,
          fa.dist4ddef, fa.dist4symbol, fa.dist5ddef, fa.dist5symbol from fxdassetca fa
        where fa.typeamortization = 0 and fa.amperiod <= new.amperiod
          and fa.status = 1 and fa.fxdasset = new.fxdasset
        into :share, :account, :dist1def, :dist1symbol, :dist2def, :dist2symbol,
          :dist3def, :dist3symbol, :dist4def, :dist4symbol, :dist5def, :dist5symbol
    do begin
    --do zaokraglen wynikajacych z podzialu kwoty amortyzacji
      sum_share = sum_share + share;
      pos_amount = (new.tamort*:share/100);
      sum_amount = sum_amount + pos_amount;
      if (sum_share >= 100) then  pos_amount = pos_amount -(sum_amount - new.tamort);
      insert into amortpos(amortization, share, amount, account, dist1def,
        dist1symbol, dist2def, dist2symbol, dist3def, dist3symbol, dist4def,
        dist4symbol, dist5def, dist5symbol, typeamortization)
      values(new.ref, :share, :pos_amount, :account, :dist1def, :dist1symbol,
        :dist2def, :dist2symbol, :dist3def, :dist3symbol, :dist4def, :dist4symbol,
        :dist5def, :dist5symbol, 0);
    end
    sum_amount = 0;
    pos_amount = 0;
    sum_share = 0;
  end
  if (new.famort <> 0 and calpos = '1') then begin
    for select fa.share, fa.account, fa.dist1ddef, fa.dist1symbol,
          fa.dist2ddef, fa.dist2symbol, fa.dist3ddef, fa.dist3symbol,
          fa.dist4ddef, fa.dist4symbol, fa.dist5ddef, fa.dist5symbol from fxdassetca fa
        where fa.typeamortization = 1 and fa.amperiod <= new.amperiod
          and fa.status = 1 and fa.fxdasset = new.fxdasset
        into :share, :account, :dist1def, :dist1symbol, :dist2def, :dist2symbol,
          :dist3def, :dist3symbol, :dist4def, :dist4symbol, :dist5def, :dist5symbol
    do begin
      sum_share = sum_share + share;
      pos_amount = (new.famort*:share/100);
      sum_amount = sum_amount + pos_amount;
      if (sum_share >= 100) then  pos_amount = pos_amount -(sum_amount - new.famort);
      insert into amortpos(amortization, share, amount, account, dist1def,
        dist1symbol, dist2def, dist2symbol, dist3def, dist3symbol, dist4def,
        dist4symbol, dist5def, dist5symbol, typeamortization)
      values(new.ref, :share, :pos_amount, :account, :dist1def, :dist1symbol,
        :dist2def, :dist2symbol, :dist3def, :dist3symbol, :dist4def, :dist4symbol,
        :dist5def, :dist5symbol, 1);
    end
  end
end^
SET TERM ; ^
