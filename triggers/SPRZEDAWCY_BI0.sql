--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDAWCY_BI0 FOR SPRZEDAWCY                     
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable contactcontrol varchar(100);
begin
   if(new.firma is null) then new.firma = 0;
   if(new.zewn is null) then new.zewn = 0;
   if(new.probrutto is null)then new.probrutto = 0;
   if(new.kontofk='') then new.kontofk = null;
   if(new.firma = 0) then begin
     if(new.imie is null) then new.imie = '';
     if(new.nazwisko is null) then new.nazwisko = '';
     new.nazwa = new.imie || ' ' ||new.nazwisko;
   end
   if(new.skrot='') then exception SPRZEDAWCY_SKROTEMPTY;

       -- sprawdzenie poprawnosci telefonu
  execute procedure get_config('CONTACTCONTROL',2) returning_values :contactcontrol;
  if (:contactcontrol is null) then contactcontrol = '0';
  if ((:contactcontrol = '1') and (new.telefon is not null)) then
    execute procedure contact_control('PHONE',new.telefon,0,'SPRZEDAWCY','TELEFON',new.ref,new.skrot);
end^
SET TERM ; ^
