--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKLINK_AU0 FOR DOKLINK                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if(new.status <> old.status) then begin
    if(new.status = 2) then begin
      update doklink set status = 1 where dokplik = new.dokplik and ref <> new.ref and status = 2;
      update dokplik set plik = new.plik, dokszabl = new.dokszabl, format = new.format,
        rozmiar = new.rozmiar where ref = new.dokplik;
    end else if(old.status = 2) then begin
      update dokplik set plik = null, dokszabl = null, format = null, rozmiar = null where ref = old.dokplik;
    end
    -- jesli oznaczamy plik do usuniecia, a jego jeszcze nie ma w repozytorium to usuwamy go od razu
    if(new.status=3 and new.nowy=1) then begin
      delete from doklink where ref=new.ref;
    end
  end
end^
SET TERM ; ^
