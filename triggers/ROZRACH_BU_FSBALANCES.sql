--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BU_FSBALANCES FOR ROZRACH                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable debit numeric(14,2);
  declare variable credit numeric(14,2);
  declare variable fsbalances integer;
  declare variable typ varchar(15);

begin
  if (new.slodef is not null) then
  begin
    select typ from slodef where ref=new.slodef
      into :typ;
    if (typ = 'KLIENCI') then
      select dictgrp from klienci where ref=new.slopoz
        into new.dictgrp;
    else if (typ = 'DOSTAWCY') then
      select dictgrp from dostawcy where ref=new.slopoz
        into new.dictgrp;
  end
  if (old.winien<>new.winien or old.ma<>new.ma
    or (old.winien is null and new.winien is not null)
    or (old.ma is null and new.ma is not null)
    or old.slodef<>new.slodef or old.slopoz<>new.slopoz
    or old.kontofk<>new.kontofk or old.dictgrp<>new.dictgrp
    or (old.dictgrp is null and new.dictgrp is not null)
    or (old.btranamountbank <> new.btranamountbank)
    or (old.dictgrp is not null and new.dictgrp is null)) then
  begin
    -- usuniecie sald
    debit = 0;
    credit = 0;

    if (old.winien > old.ma) then
      debit = old.winien - old.ma;
    if (old.ma > old.winien) then
      credit = old.ma - old.winien;

    select ref from fsbalances
      where account = old.kontofk and dictdef = old.slodef and dictpos = old.slopoz
        and company = old.company and currency=old.waluta
      into :fsbalances;

    if (fsbalances is not null) then
    begin
      update fsbalances set debit = debit - :debit, credit = credit - :credit, btranamountbank = btranamountbank - old.btranamountbank
        where ref = :fsbalances;
    end
    if (old.dictgrp is not null) then
      update fsbalances set grpdebit = grpdebit - :debit, grpcredit = grpcredit - :credit, grpbtranamountbank = grpbtranamountbank - old.btranamountbank
        where dictgrp = old.dictgrp and company = old.company and currency = old.waluta;

    -- dodanie sald
    fsbalances = null;
    debit = 0;
    credit = 0;
    if (new.winien > new.ma) then debit = new.winien - new.ma;
    if (new.ma > new.winien) then credit = new.ma - new.winien;
    select ref from fsbalances
      where account = new.kontofk and dictdef = new.slodef and dictpos = new.slopoz
        and company = new.company and currency = new.waluta
      into :fsbalances;
    if (fsbalances is null) then
    begin

      insert into fsbalances (account, dictdef, dictpos, company, debit, credit, currency, btranamountbank)
        values (new.kontofk, new.slodef, new.slopoz, new.company, :debit, :credit, new.waluta, new.btranamountbank);
    end else
    begin
      update fsbalances set debit = debit + :debit, credit = credit + :credit,
          btranamountbank = btranamountbank + new.btranamountbank,
          dictgrp = new.dictgrp
        where ref = :fsbalances;
    end
    if (new.dictgrp is not null) then
      update fsbalances set grpdebit = grpdebit + :debit, grpcredit = grpcredit + :credit,
          grpbtranamountbank = grpbtranamountbank + new.btranamountbank
        where dictgrp = new.dictgrp and company = new.company and currency = new.waluta;
  end
end^
SET TERM ; ^
