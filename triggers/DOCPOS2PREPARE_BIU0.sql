--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOCPOS2PREPARE_BIU0 FOR DOCPOS2PREPARE                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.dquantity is null) then new.dquantity = 0;
  if (new.rquantity is null) then new.rquantity = 0;
  if (new.tquantity is null) then new.tquantity = 0;
end^
SET TERM ; ^
