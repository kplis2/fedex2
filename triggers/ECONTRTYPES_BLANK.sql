--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRTYPES_BLANK FOR ECONTRTYPES                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.empltype = 1 or new.prcoststype is null) then
    new.prcoststype = 0;

  if (new.empltype <> 1 or new.enddefault = 0) then --PR24342
    new.enddefault = null;

  if (new.prcoststype = 0) then
    new.prcosts = null;

  new.dictcontent = coalesce(new.dictcontent,0);
  new.editableprt = coalesce(new.editableprt,0);
  new.showstartdate = coalesce(new.showstartdate,0); --BS39732

  new.controverlap = coalesce(new.controverlap,0); --PR60331
  if (new.empltype = 1 and new.controverlap <> 2) then
    new.controverlap = 2;
end^
SET TERM ; ^
