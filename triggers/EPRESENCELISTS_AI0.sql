--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTS_AI0 FOR EPRESENCELISTS                 
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable oref integer;
declare variable eref integer;
declare variable workstart time;
declare variable workend time;
declare variable timestart varchar(20);
declare variable timestop varchar(20);
declare variable calendar integer;
declare variable wastoday smallint;
declare variable contract integer;
declare variable workdim float;
declare variable proportional smallint;
declare variable workdimdiff integer;
begin
  for
    select o.ref, e.ref, e.autopresent - 1, e.calendar
      from employees e
        left join operator o on (o.employee = e.ref)
      where e.emplstatus > 0 and e.autopresent > 0
      into oref, eref, wastoday, calendar
  do begin
    -- okreslenie umowy o prace
    execute procedure EWORKTIME_CALCULATE(eref, calendar, 0, new.ref,new.listsdate)
      returning_values (workstart, workend);
    timestart = substring(cast(workstart as varchar(20)) from 1 for 5);
    timestop = substring(cast(workend as varchar(20)) from 1 for 5);
    if (timestart = '') then timestart = null;
    if (timestop = '') then timestop = null;
    -- wstawienie pracownika na liste
    if (timestart is not null and timestop is not null) then
      insert into epresencelistnag (operator, employee, timestart, timestop, epresencelist, wastoday, shift)
        values(:oref, :eref, :timestart, :timestop, new.ref, :wastoday, 0);
  end
end^
SET TERM ; ^
