--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_AU_STOCKDOCS FOR MWSORDS                        
  ACTIVE AFTER UPDATE POSITION 1 
AS
declare variable stockdocplus varchar(3);
declare variable stockdocminus varchar(3);
declare variable vers integer;
declare variable good varchar(20);
declare variable quantity numeric(14,4);
declare variable quantityc numeric(14,4);
declare variable toinsert numeric(14,4);
declare variable mwsconstlocp integer;
declare variable mwspallocp integer;
declare variable mwsconstlocl integer;
declare variable mwspallocl integer;
declare variable docplusref integer;
declare variable docminusref integer;
declare variable rec smallint;
declare variable stockdoc integer;
declare variable doctype varchar(3);
declare variable lot integer;
begin
  -- JEDNO ZLECENIE INWENTARYZACJI DOTYCZY ZAWSZE JEDNEJ LOKACJI PALETOWEJ !!!
  -- wystawienie dokumentów inwentaryzacyjnych rozliczajacych zlecenie inwentaryzacji
  docplusref = null;
  docminusref = null;
  rec = 1;
  if (new.stocktaking = 1 and new.status = 5 and old.status <> 5 and new.stocktakingsettlmode = 1) then
  begin
    select stockdocplus, stockdocminus
      from mwsordtypes
      where ref = new.mwsordtype
      into stockdocplus, stockdocminus;
    -- dokumenty inwentaryzacyjne wystawiamy dla pogrupowanych operacji po wersjach EWENTUALNIE RÓWNIEZ DOSTAWACH
    for
      select vers, lot, good, quantity, quantityc, mwsconstlocp,
          mwspallocp, mwsconstlocl, mwspallocl
        from MWS_STOCKDOCS_POS(new.ref,new.lotreg)
        into vers, lot, good, quantity, quantityc, mwsconstlocp,
          mwspallocp, mwsconstlocl, mwspallocl
    do begin
      stockdoc = null;
      -- rozliczamy stwierdzony brak towaru wiec generujemy dokument inwentaryzacji rozchodowy
      if (quantity > quantityc) then
      begin
        stockdoc = docminusref;
        doctype =  stockdocminus;
        toinsert = quantity - quantityc;
      end else
      begin
        stockdoc = docplusref;
        doctype =  stockdocplus;
        toinsert = quantityc - quantity;
      end
      -- rozliczamy stwierdzony nadmiar towaru wiec generujemy dokument inwentaryzacji przychodowy
      execute procedure MWS_GEN_STOCKDOCS(stockdoc, new.wh, null, doctype, new.ref,
          new.symbol, new.operator, vers, good, toinsert, mwsconstlocp,
          mwspallocp, mwsconstlocl, mwspallocl, null)
        returning_values stockdoc;
      if (quantity > quantityc) then
        docminusref = stockdoc;
      else
        docplusref = stockdoc;
    end
  end
end^
SET TERM ; ^
