--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLPOZZAM_AU0 FOR CPLPOZZAM                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable ret integer;
declare variable sumpkt integer;
begin
  if((new.NAGRODA<>old.NAGRODA) or
     (new.OPIS<>old.OPIS) or
     (new.CENAPKT<>old.CENAPKT) or
     (new.ILOSC<>old.ILOSC)) then begin
       execute procedure CPLOPER_WYSTAWUSUN(new.ref) returning_values :ret;
  end
  select sum(ILPKT) from CPLPOZZAM where CPLNAGZAM = new.CPLNAGZAM into :sumpkt;
  update CPLNAGZAM set SUMILPKT = :sumpkt where REF = new.CPLNAGZAM;
end^
SET TERM ; ^
