--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOSTDODATKI_BI_REF FOR SPOSDOSTDODATKI                
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SPOSDOSTDODATKI')
      returning_values new.REF;

end^
SET TERM ; ^
