--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NEOSSSERVER_DATA_BI FOR NEOSSSERVER_DATA               
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_neossserver_data,1);
end^
SET TERM ; ^
