--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CFAZY_BU_REPLICAT FOR CFAZY                          
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
    or new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
    or new.grupa <> old.grupa or (new.grupa is null and old.grupa is not null) or (new.grupa is not null and old.grupa is null)
    or new.stopien <> old.stopien or (new.stopien is null and old.stopien is not null) or (new.stopien is not null and old.stopien is null)
    or new.numer <> old.numer or (new.numer is null and old.numer is not null) or (new.numer is not null and old.numer is null)
    or new.contrtype <> old.contrtype or (new.contrtype is null and old.contrtype is not null) or (new.contrtype is not null and old.contrtype is null)
    or new.document <> old.document or (new.document is null and old.document is not null) or (new.document is not null and old.document is null)
    or new.aktywne <> old.aktywne or (new.aktywne is null and old.aktywne is not null) or (new.aktywne is not null and old.aktywne is null)
    or new.zakonczono <> old.zakonczono or (new.zakonczono is null and old.zakonczono is not null) or (new.zakonczono is not null and old.zakonczono is null)
    ) then
   waschange = 1;

  execute procedure rp_trigger_bu('CFAZY',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
