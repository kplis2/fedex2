--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYPOZ_AD0 FOR NOTYPOZ                        
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  execute procedure NOTYNAG_OBL(old.dokument);
  if (not exists(select first 1 1 from notypoz np where np.dokument = old.dokument)) then
    delete from notynag nn where nn.ref = old.dokument;
end^
SET TERM ; ^
