--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_KLIENCIHIST_BI FOR INT_KLIENCIHIST                
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('INT_KLIENCIHIST')
      returning_values new.ref;
  if(coalesce(new.nazwa,'') = '')then
    new.nazwa = coalesce(new.imie,'') || coalesce(new.nazwisko,'');
  if(coalesce(new.fskrot, '') = '')then
    new.fskrot = substring(new.nazwa from 1 for 40);
  if(coalesce(new.krajid, 'PL') = 'PL') then
    new.zagraniczny = 0;
  else
    new.zagraniczny = 1;
end^
SET TERM ; ^
