--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VREGS4BKREG_BIU0 FOR VREGS4BKREG                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  select vtype from vatregs where symbol = new.vatreg and company = new.company
    into new.vtype;
end^
SET TERM ; ^
