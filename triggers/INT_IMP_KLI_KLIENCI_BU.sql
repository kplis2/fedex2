--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_IMP_KLI_KLIENCI_BU FOR INT_IMP_KLI_KLIENCI            
  ACTIVE BEFORE UPDATE POSITION 5 
as
begin
  if (old.dataostprzetw is distinct from new.dataostprzetw
      and new.dataostprzetw is not null
      and new.dataprzetw is null) then
    new.dataprzetw = new.dataostprzetw;

  if (old.error is distinct from new.error
      and new.error is null) then
    new.error = 0;

  if (old.errorzalezne is distinct from new.errorzalezne
      and new.errorzalezne is null) then
    new.errorzalezne = 0;
end^
SET TERM ; ^
