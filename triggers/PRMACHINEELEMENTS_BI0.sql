--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMACHINEELEMENTS_BI0 FOR PRMACHINEELEMENTS              
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ktm is not null and new.wersjaref is null) then
  begin
    select first 1 w.ref
      from wersje w
      where w.ktm = new.ktm
        and w.akt = 1
      order by w.nrwersji
      into new.wersjaref;
  end
end^
SET TERM ; ^
