--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERTY_BI_REF FOR OFERTY                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('OFERTY')
      returning_values new.REF;
end^
SET TERM ; ^
