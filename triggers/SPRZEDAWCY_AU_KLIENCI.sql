--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDAWCY_AU_KLIENCI FOR SPRZEDAWCY                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.sprzedawcawewn <> old.sprzedawcawewn or (new.sprzedawcawewn is null and old.sprzedawcawewn is not null) or (new.sprzedawcawewn is not null and old.sprzedawcawewn is null)) then
    update klienci set klienci.sprzedawca = new.sprzedawcawewn where klienci.sprzedawcazew = new.ref;
end^
SET TERM ; ^
