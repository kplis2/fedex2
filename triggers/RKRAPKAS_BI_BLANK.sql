--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_BI_BLANK FOR RKRAPKAS                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.przychod is null) then
    new.przychod = 0;
  if (new.rozchod is null) then
    new.rozchod = 0;
  if (new.przychodzl is null) then
    new.przychodzl = 0;
  if (new.rozchodzl is null) then
    new.rozchodzl = 0;
  if (new.skwota is null) then
    new.skwota = 0;
  if (new.skwotazl is null) then
    new.skwotazl = 0;
  if (new.symbol is null or new.symbol='') then
    new.symbol = cast(new.numer as varchar(20));
end^
SET TERM ; ^
