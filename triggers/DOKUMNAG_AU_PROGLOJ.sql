--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AU_PROGLOJ FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 7 
as
declare variable par varchar(255);
begin
  execute procedure GETCONFIG('WLACZPROGRAM') returning_values :par;
  if (((new.akcept in (0,7) or (new.akcept >=9)) and old.akcept in (1,8))
  )then
    delete from cploper
    where cploper.typdok='DOKUMNAG' and cploper.refdok=new.ref;
  if (:par = '1' and new.akcept in (1,8) and
     ( (old.akcept in (0,7))
       or (old.akcept >=9)
     )
  )then
    execute procedure CRM_PROGRAMLOJ_OBLICZFORDOK('DOKUMNAG',new.ref,1,'A');
end^
SET TERM ; ^
