--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_BU_DEBIT FOR BKVATPOS                       
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable sell integer;
begin
  select g.vtype
    from grvat g
    where g.symbol = new.taxgr
    into :sell;
  if (sell = 0
  and (new.debittype <> old.debittype
      or new.debitprocent <> old.debitprocent
      or new.debited <> old.debited
      or new.netv <> old.netv
      or new.grossv <> old.grossv
      or new.vatgr <> old.vatgr
      or new.taxgr <> old.taxgr
      or new.vate <> old.vate )) then
  begin
      --Najpierw sprawdzamy, czy chcemy odliczyć VAT i w jaki sposób
      if (new.debittype = 0) then --Nie chcemy odliczać wiec 0 procent
      begin
        if(new.debitprocent <> cast(0 as procent))then
          new.debitprocent = cast(0 as procent);
      end else if (new.debittype = 1) then  --Chcemy wszystko czyli 100 procent
      begin
        if(new.debitprocent <> cast(100 as procent))then
          new.debitprocent = cast(100 as procent);
      end
      --Jezeli chemy odliczyc po prostu procent to wpisujemy go z reki,
      --jednak nalezy sprawdzic czy nie wpsialismy ponad 100%
      --ew procedura struktury mogla naliczyc za duzo wiec kontrulujemy
    
      if (new.debitprocent > cast(100 as procent)) then
        exception fk_debit_procent;
    
      --Jeżeli mamy pewnosc, że procent jest spoko to musimy wyliczyć wartosc odliczenia
      -- bo albo 100%, albo X% albo procent ze struktury
      -- ale tylko jak nie wyliczone - inaczej z interfacu uzupelniane
      if(new.vatv = 0 and new.debitprocent <> 0)then
      begin
        if (new.debitprocent > cast(0 as procent)) then
        begin
          new.vatv = (new.vate * new.debitprocent)/100.00;
          new.debited = 1;
        end else begin -- inaczej zawsze zero bo innej opcji nie ma
          new.vatv = 0;
          new.debited = 0;
        end
      end
  end
end^
SET TERM ; ^
