--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BIU_EVACUCP FOR EABSENCES                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 6 
as
declare variable paydays integer;
declare variable nopaydays integer;
declare variable absences integer;
declare variable emplfromdate date;
declare variable empltodate date;
declare variable restdays integer;
declare variable dwp integer;
declare variable dwnp integer;
begin
  execute procedure get_config('VACUCPPAYCOLUMN', 2) returning_values dwp;
  execute procedure get_config('VACUCPNOPAYCOLUMN', 2) returning_values dwnp;
  if(new.ecolumn in (:dwp,:dwnp) and (
      old.ecolumn is distinct from new.ecolumn
      or old.workdays is distinct from new.workdays
      or old.fromdate is distinct from new.fromdate
      or old.todate is distinct from new.todate
      or old.emplcontract is distinct from new.emplcontract   )) then
  begin
    if(new.emplcontract is null) then
      EXCEPTION BRAK_UMOWY;
    else
    begin
    select fromdate, coalesce(enddate,todate) from emplcontracts where ref = new.emplcontract
    into :emplfromdate,:empltodate;
    if(new.fromdate < emplfromdate or new.fromdate > empltodate) then
      exception universal'Data nieobecności nie zawiera się w okresie umowy';
    else if(new.todate > empltodate) then
      exception universal'Data nieobecności nie zawiera się w okresie umowy';
    select nopaylimit, paylimit
      from e_calculate_ucpvaclimits(new.emplcontract,new.fromdate)
      into :nopaydays, :paydays;

      select sum(workdays)
        from eabsences
        where ecolumn = new.ecolumn
          --and fromdate <= new.fromdate
          and ayear = extract(year from new.fromdate)
          and correction in (0,2)
          and emplcontract = new.emplcontract
        into :absences;
        absences = coalesce(:absences,0) + new.workdays - coalesce(old.workdays,0);
      if(new.ecolumn = :dwp) then
      begin
        if(coalesce(:paydays,0) = 0) then
          EXCEPTION universal'Umowa nie posiada warunku dni wolnych płatnych!';
        else if(coalesce(:paydays,0) < :absences) then
        begin
          restdays = paydays - (absences - new.workdays + coalesce(old.workdays,0));
          EXCEPTION universal'Do wykorzystania pozostało: '||:restdays;
        end
        else if(coalesce(:paydays,0) < new.workdays) then
          EXCEPTION universal'Liczba dni nieobecności przekracza limit wynikający z umowy';
      end
      else if(new.ecolumn = :dwnp) then
      begin
        if(coalesce(:nopaydays,0) = 0) then
          EXCEPTION universal'Umowa nie posiada warunku dni wolnych niepłatnych!';
        else if(coalesce(:nopaydays,0) < :absences) then
        begin
          restdays = nopaydays - (absences - new.workdays + coalesce(old.workdays,0));
          EXCEPTION universal'Do wykorzystania pozostało: '||:restdays;
        end
        else if(coalesce(:nopaydays,0) < new.workdays) then
          EXCEPTION universal'Liczba dni nieobecności przekracza limit wynikający z umowy';
      end
    end
  end
end^
SET TERM ; ^
