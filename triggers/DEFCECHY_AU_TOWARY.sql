--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHY_AU_TOWARY FOR DEFCECHY                       
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable sql varchar(255);
begin
  if((new.redundantakr is null and old.redundantakr is not null) or (new.redundantakr <> old.redundantakr))then begin
    if(old.redundantakr <> '') then begin
      sql = 'update TOWARY set '||old.redundantakr||'=''''';
      execute statement :sql;
    end
  end
  if((new.redundantakr is not null and old.redundantakr is null) or (new.redundantakr <> old.redundantakr))then begin
    if(new.redundantakr <> '') then begin
      sql = 'update TOWARY set '||new.redundantakr||'=(select WARTOSC from ATRYBUTY where ATRYBUTY.KTM = TOWARY.KTM and ATRYBUTY.NRWERSJI = 0 and ATRYBUTY.CECHA='''||new.symbol||''')';
      execute statement :sql;
    end
  end
end^
SET TERM ; ^
