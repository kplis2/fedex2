--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELDAYS_AU FOR EDELDAYS                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.delegation <> old.delegation or
        new.fromdate<>old.fromdate or
        new.hours<>old.hours or
        new.breakfast<>old.breakfast or
        new.dinner<>old.dinner or
        new.supper<>old.supper or
        new.citytransport<>old.citytransport or
        new.toairport<>old.toairport or
        new.meals<>old.meals or
        new.accommodation<>old.accommodation or
        new.nottocount<>old.nottocount or
        new.incountry<>old.incountry) then
    execute procedure edel_count_all(new.delegation);
end^
SET TERM ; ^
