--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNSALG_BIU0 FOR FRPSNSALG                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable lastvar integer;
begin
  execute procedure STATEMENT_MULTIPARSE(new.proced, 'FRF_', '', '', 1, 1)
    returning_values new.decl, new.body, :lastvar;
end^
SET TERM ; ^
