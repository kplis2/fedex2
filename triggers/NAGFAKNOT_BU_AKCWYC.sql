--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKNOT_BU_AKCWYC FOR NAGFAKNOT                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable numberg varchar(255);
declare variable rejestr varchar(3);
declare variable typ varchar(3);
declare variable BLOCKREF int;

begin
  if (old.AKCEPTACJA=1 and new.AKCEPTACJA=0) then begin
    --odakceptowanie noty
    new.dataakc = null;
    execute procedure GETCONFIG('NAGFAKNOTNUM') returning_values :numberg;
    new.operakcept = null;
    select REJESTR,TYP from NAGFAK where REF=new.dokument into :rejestr,:typ;
    execute procedure FREE_NUMBER(:numberg, :rejestr, :typ, new.data, old.NUMER, null)
      returning_values :BLOCKREF;
      new.numer =null;
      new.symbol=null;
  end
  if (old.akceptacja=0 and new.akceptacja=1) then begin
    --akceptacja noty
    new.dataakc = current_timestamp(0);
    execute procedure GETCONFIG('NAGFAKNOTNUM') returning_values :numberg;
    execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR') returning_values new.operakcept;
    select REJESTR,TYP from NAGFAK where REF=new.dokument into :rejestr,:typ;
    execute procedure GET_NUMBER(:numberg, :rejestr, :typ, new.data, NULL, new.ref)
      returning_values new.numer, new.symbol;
  end
end^
SET TERM ; ^
