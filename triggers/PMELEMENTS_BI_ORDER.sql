--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMELEMENTS_BI_ORDER FOR PMELEMENTS                     
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 1 and new.number is not null) then begin
    new.ord = 0;
    exit;
  end
  select max(number) from pmelements where pmplan = new.pmplan
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  begin
    update pmelements set ord = 1, number = number + 1
      where number >= new.number and pmplan = new.pmplan;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;

end^
SET TERM ; ^
