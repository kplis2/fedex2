--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER XK_GOODS_RELOCATE_BIU0 FOR XK_GOODS_RELOCATE              
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.const is null) then new.const = 0;
  if (new.wcategory is null or new.wcategory = '') then new.wcategory = 'A';
  if (new.interval is null or new.interval = 0) then
    new.interval = 14;
end^
SET TERM ; ^
