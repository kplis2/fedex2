--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECREDITS_BI_REF FOR ECREDITS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECREDITS')
      returning_values new.REF;
end^
SET TERM ; ^
