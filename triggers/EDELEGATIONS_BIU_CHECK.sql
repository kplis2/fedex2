--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELEGATIONS_BIU_CHECK FOR EDELEGATIONS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if((new.fromdate is null) or (new.todate is null)) then
    exception EDELEGATIONS_DATY;
  if(new.fromdate > new.todate) then
    exception EDELEGATIONS_FROMDATE_GT_TODATE;
end^
SET TERM ; ^
