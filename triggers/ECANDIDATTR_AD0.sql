--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECANDIDATTR_AD0 FOR ECANDIDATTR                    
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable RecCand_ref int;
begin
  -- przeliczam oceny !
  for
    select ref  from ereccands
      where CANDIDATE=old.CANDIDATE
      into :RecCand_ref
  do
    execute procedure eRecCandCompRating(RecCand_ref);
end^
SET TERM ; ^
