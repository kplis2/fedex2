--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FLDFLAGSDEF_BI0 FOR FLDFLAGSDEF                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.number is null) then
  begin
    select case when max(number) is null then 1 else max(number)+1 end
      from FLDFLAGSDEF where fldflagsgrp=new.fldflagsgrp
      into new.number;
    new.ORD = 1;
  end
  if (new.ord is null) then
    new.ord = 0;
end^
SET TERM ; ^
