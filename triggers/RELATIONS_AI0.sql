--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RELATIONS_AI0 FOR RELATIONS                      
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.relationdef = 1 and new.act > 0 and new.stableto = 'POZZAM'
      and (new.quantityto > 0 or new.quantitytoreal > 0)
  ) then
  begin
    execute procedure pozzam_obl(new.srefto);
  end
end^
SET TERM ; ^
