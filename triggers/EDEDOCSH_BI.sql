--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDEDOCSH_BI FOR EDEDOCSH                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(coalesce(new.ref,0)=0) then
    execute procedure gen_ref('EDEDOCSH') returning_values new.ref;
  new.regdate = current_timestamp(0);
  execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR') returning_values new.regoper;
  if(new.status is null) then new.status = 0;
  if(new.direction is null) then new.direction = 1;
  if(new.manualinit is null) then new.manualinit = 0;
  if(new.signstatus is null) then new.signstatus = 0;
end^
SET TERM ; ^
