--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHAREAS_AI0 FOR WHAREAS                        
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.number is not null and new.l is not null) then
    execute procedure MWS_ROWDIST_CALCULATE(new.whsecrow);
end^
SET TERM ; ^
