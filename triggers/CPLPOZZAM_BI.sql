--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLPOZZAM_BI FOR CPLPOZZAM                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  IF (NEW.REF IS NULL) THEN
      execute procedure GEN_REF('CPLPOZZAM') returning_values new.ref;
END^
SET TERM ; ^
