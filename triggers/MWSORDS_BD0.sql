--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_BD0 FOR MWSORDS                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.status > 0) then
    exception MWSORDS_ACCEPTED;
end^
SET TERM ; ^
