--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BU_OPERATOR FOR EABSENCES                      
  ACTIVE BEFORE UPDATE POSITION 9 
AS
begin
--MWr: Ustawienie infomracji kto i kiedy zaktualizowal nieobecnosc

  if (new.regtimestamp <> current_timestamp(0) and (new.payamount <> old.payamount
       or (new.payamount is null and old.payamount is not null)
       or (new.payamount is not null and old.payamount is null))) then
  begin
    execute procedure get_global_param ('AKTUOPERATOR')
      returning_values new.chgoperator;
    new.chgtimestamp = current_timestamp(0);
  end
end^
SET TERM ; ^
