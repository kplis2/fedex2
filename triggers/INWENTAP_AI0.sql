--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTAP_AI0 FOR INWENTAP                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable inwentaparent integer;
declare variable pozmaster integer;
declare variable statusmaster smallint;
declare variable ilroznicamaster numeric(14,4);
begin
  if(new.blokadanalicz = 0) then execute procedure INWENTA_OBLNAG(new.inwenta);
  select inwenta.inwentaparent from INWENTA where ref=new.inwenta into :inwentaparent;
  if(:inwentaparent > 0) then begin
    execute procedure INWENTAP_OBLICZMASTER(:inwentaparent, new.wersjaref, new.cena, new.dostawa, new.ilinw);
    if(new.pozmaster is null) then begin
      execute procedure INWENTAP_POZMASTER(new.ref) returning_values :pozmaster;
      if(:pozmaster > 0) then begin
        select status, ilroznica from INWENTAP where ref=:pozmaster into :statusmaster, :ilroznicamaster;
        update INWENTAP set POZMASTER = :pozmaster, STATUSMASTER = :statusmaster, ILROZNICAMASTER = :ilroznicamaster, status = -1 where ref=new.ref;
      end
    end
  end
  update INWENTAP set STATUSMASTER = new.status, ILROZNICAMASTER =new.ilroznica, status = -1 where INWENTAP.pozmaster = new.ref;
end^
SET TERM ; ^
