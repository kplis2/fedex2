--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACPLAN_BIU_CHECK FOR EVACPLAN                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.fromdate > new.todate) then
    exception data_do_musi_byc_wieksza;

  if (new.state > 0 and (new.fromdate <> old.fromdate or new.todate <> old.todate
                        or new.employee <> old.employee or new.vtype <> old.vtype
                        or coalesce(new.ecolumn,0) <> coalesce(old.ecolumn,0))
  ) then
    exception universal 'Wystąpiła próba modyfikacji obsłużonego wniosku!';
end^
SET TERM ; ^
