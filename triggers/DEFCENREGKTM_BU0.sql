--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENREGKTM_BU0 FOR DEFCENREGKTM                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable REG VARCHAR(40);
declare variable CEN VARCHAR(40);
declare variable REGULA VARCHAR(60);
begin
if (new.ktm <> old.ktm) then
  begin
    select max(defcenreg.defcennik) -- pobrannie cennika do ktorego chcemy przypisac ktm
      from defcenreg
      where defcenreg.ref = new.defcenreg into cen;
    for select defcenreg.ref, defcenreg.nazwa
      from defcenreg
      where defcenreg.defcennik = :cen
--dla kazdej reguly z cennika zawirającego regule do ktorej przyporzedakowujemy KTM
      into :REG, :REGULA
      do begin
           if (exists (select defcenreg from defcenregktm
                                where defcenregktm.defcenreg = :REG
                                  and defcenregktm.ktm = new.ktm ))
           then exception universal 'Ktm juz zostal zdefiniownay w '||REGULA;
         end
  end
end^
SET TERM ; ^
