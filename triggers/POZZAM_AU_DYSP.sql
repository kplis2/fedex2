--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AU_DYSP FOR POZZAM                         
  ACTIVE AFTER UPDATE POSITION 4 
as
declare variable dore smallint;
declare variable dodp smallint;
declare variable dodb smallint;
declare variable dodk smallint;
declare variable dodd smallint;
declare variable typ smallint;
declare variable ilosc decimal(14,4);
declare variable cnt smallint;
declare variable pozzamdref integer;
declare variable iloscd numeric(14,4);
begin
 if(new.autopozzamdysp <>'' and new.ildysp <> old.ildysp) then begin
    select sum(ilosc) from POZZAMDYSP where POZZAM = new.ref into :ilosc;
    if(:ilosc is null) then ilosc = 0;
    if(:ilosc <> new.ildysp) then begin
      typ = 0;
      /*okreslenie rodzaju dyspozycij na podstawie operacji, w trakcie której jest zamówienie*/
      select REALIZACJA, DYSPP, DYSPB, DYSPK, DYSPD from DEFOPERZAM where SYMBOL = new.autopozzamdysp
      into :dore, dodp, :dodb, :dodk, :dodd;
      if(:dodp > 0) then typ = 1;
      else if(:dodb > 0) then typ = 2;
      else if(:dodk > 0) then typ = 3;
      else if(:dodd > 0) then typ = 4;
      if(:typ > 0) then begin
        /*sprawdzenie ilosci już na POZZAMDYPD danego typu*/
        select count(*) from POZZAMDYSP where pozzam = new.ref and typ = :typ into :cnt;
        ilosc = new.ildysp - :ilosc;
        if(:cnt = 1) then
          update POZZAMDYSP set ILOSC = ILOSC + :ilosc, ILOSCO = 0, ILOSCM = 0 where pozzam = new.ref and typ = :typ;
          /*wymuszenie automatycznego przeliczenia iloci*/
        else if(:ilosc > 0) then begin
          insert into POZZAMDYSP(POZZAM,TYP,JEDNO,JEDN, ILOSC) values (new.ref, :typ, new.jedno, new.jedn, :ilosc);
        end else begin
          ilosc = - :ilosc;
          for select ref, ILOSC from POZZAMDYSP where pozzam = new.ref and typ = :typ
          order by ref desc
          into :pozzamdref, :iloscd
          do begin
            if(:ilosc > 0) then begin
              if(:ilosc > :iloscd) then begin
                ilosc = :ilosc - :iloscd;
                delete from POZZAMDYSP where ref=:pozzamdref;
              end else begin
                update POZZAMDYSP set ILOSC = ILOSC - :ilosc, ILOSCO = 0, ILOSCM = 0 where ref=:pozzamdref;
                ilosc = 0;
              end

            end
          end
        end
        delete from POZZAMDYSP where ILOSC = 0 and pozzam = new.ref;
      end
    end
 end
end^
SET TERM ; ^
