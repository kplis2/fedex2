--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYS_AD0 FOR LISTYWYS                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable numerator varchar(40);
declare variable BLOCKREF integer;
begin
  if(old.numer > 0) then begin
     execute procedure GETCONFIG('LISTYWYSNUM') returning_values :numerator;
     if(:numerator is null) then numerator = '';
     if(:numerator <> '' ) then begin
       execute procedure FREE_NUMBER(:numerator, old.spedytor, old.trasa, old.dataplwys, old.numer, 0) returning_values :blockref;
     end
  end
end^
SET TERM ; ^
