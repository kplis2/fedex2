--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCESREQ_BI0 FOR TOWAKCESREQ                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.ref is null or (new.ref = 0)) then
    execute procedure GEN_REF('TOWAKCESREQ') returning_values new.ref;
  select aktm from towakces where ref=new.rtowakces
    into new.ktm;
end^
SET TERM ; ^
