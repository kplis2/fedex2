--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_APPINI_AI FOR S_APPINI                       
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if(position('Customized' in  new.ident) > 0) then
  begin
    execute procedure SET_CUSTOMIZED(new.section, new.ident, 1);
  end
end^
SET TERM ; ^
