--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_BU0 FOR BKACCOUNTS                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable synlen int;
begin
  if (new.symbol <> old.symbol) then
  begin
    if (exists(select ref from accounting
                where yearid = old.yearid and account starting with old.symbol and company = old.company)) then
      exception fk_cant_update_bkaccount;

    select synlen from bkyears
      where yearid = new.yearid and company = new.company
      into :synlen;
    if (coalesce(char_length(new.symbol),0) <> synlen) then -- [DG] XXX ZG119346
      exception FK_ERROR_SYNLEN;
  end
  if (new.rights = ';') then new.rights = '';
  if (new.rightsgroup = ';') then new.rightsgroup = '';
  if (new.matchable is null) then new.matchable = 0;
  if (new.matchable > 0 and new.matchingslodef is null) then
    exception BKACCOUNT_ERROR 'Nie wybrano słownika dla konta rozliczeniowego';
end^
SET TERM ; ^
