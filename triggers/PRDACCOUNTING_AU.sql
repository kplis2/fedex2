--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTING_AU FOR PRDACCOUNTING                  
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable multi smallint;
declare variable bkacc integer;
declare variable descript varchar(255);
declare variable multiprd smallint;
begin
  multiprd = 0;
  select coalesce(b.multiprd,0) from bkaccounts b where (substring(coalesce(new.account,'') from 1 for 3) = b.symbol
  and cast(substring(coalesce(new.decreeperiod,0) from 1 for 4) as integer) = b.yearid) and b.company = new.company into :multi;
  --jezeli konto nie ma wielu kont odpisu to sprawdzamy czy konto odpisu ma wyrozniki wielowartosciowe
  if(:multi = 0) then
  select first 1 MULTI
    from check_account(new.company, new.deductaccount, cast(substring(new.firstperiod from  1 for 4) as integer), 0)
    into :multiprd;
  if(:multi = 0 and :multiprd = 0
       and (new.deductaccount <> old.deductaccount
         or new.deductamount <> old.deductamount
         or new.firstdeductamount<>old.firstdeductamount
         or new.dist1ddef <> old.dist1ddef
         or new.dist1symbol <> old.dist1symbol
         or new.dist2ddef <> old.dist2ddef
         or new.dist2symbol <> old.dist2symbol
         or new.dist3ddef <> old.dist3ddef
         or new.dist3symbol <> old.dist3symbol
         or new.dist4ddef <> old.dist4ddef
         or new.dist4symbol <> old.dist4symbol
         or new.dist5ddef <> old.dist5ddef
         or new.dist5symbol <> old.dist5symbol
         or new.dist6ddef <> old.dist6ddef
         or new.dist6symbol <> old.dist6symbol
         )) then begin
    delete from PRDACCOUNTINGPOS where PRDACCOUNTING = new.ref and prdversion = new.firstprdversion;
    execute procedure FK_GET_ACCOUNT_DESCRIPT(new.company, new.decreeperiod, new.deductaccount) returning_values :descript;
    insert into PRDACCOUNTINGPOS(PRDACCOUNTING, DEDUCTACCOUNT, DEDUCTAMOUNT, FIRSTDEDUCTAMOUNT,
                                 DIST1DDEF, DIST1SYMBOL, DIST2DDEF, DIST2SYMBOL, DIST3DDEF, DIST3SYMBOL, DIST4DDEF,
                                 DIST4SYMBOL, DIST5DDEF, DIST5SYMBOL, DIST6DDEF, DIST6SYMBOL, DECREE, DESCRIPT, DIVRATE, PRDVERSION)
    values(new.ref, new.deductaccount, new.deductamount, new.firstdeductamount,
           new.DIST1DDEF, new.DIST1SYMBOL, new.DIST2DDEF, new.DIST2SYMBOL, new.DIST3DDEF, new.DIST3SYMBOL, new.DIST4DDEF,
           new.DIST4SYMBOL, new.DIST5DDEF, new.DIST5SYMBOL, new.DIST6DDEF, new.DIST6SYMBOL, new.decree, :descript, 100, new.firstprdversion);
  end
  if((:multi = 1 or :multiprd = 1) and (new.deductamount <> old.deductamount
         or new.firstdeductamount<>old.firstdeductamount)) then begin
    update PRDACCOUNTINGPOS set DEDUCTAMOUNT = new.deductamount*(DIVRATE/100),
           FIRSTDEDUCTAMOUNT = new.firstdeductamount*(DIVRATE/100)
    where PRDACCOUNTING = new.ref;
  end
  if(coalesce(new.firstperiod,'')<> coalesce(old.firstperiod,'')) then
    update prdversions p set p.fromperiod = new.firstperiod where p.ref = new.firstprdversion;

end^
SET TERM ; ^
