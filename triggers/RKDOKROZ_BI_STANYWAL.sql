--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKROZ_BI_STANYWAL FOR RKDOKROZ                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
-- Trigger ma za zadanie uaktualnienie TABELI RKSTANYWAL
declare variable kurs numeric(14,4);
declare variable stanowisko char(2);
declare variable data timestamp;
declare variable RKDOKNAG_ref int;
declare variable lpnag integer;
declare variable lppoz integer;
begin
  -- operacja insert trzeba 'dodac' nowy stan
  select N.REF, P.Kurs, N.STANOWISKO, N.DATA, n.lp, p.lp
    from RKDOKPOZ P
      join RKDOKNAG N on N.REF = P.DOKUMENT
    where P.REF=new.RKDOKPOZ
    into :RKDOKNAG_ref, :kurs, stanowisko, data, :lpnag, :lppoz;

  if (new.rkstanwal is null) then begin
    -- tworze nowy stan walutowy
    if (coalesce(:kurs,0) = 0) then exception RKDOKROZ_EXPT 'Nie wypeniony kurs na pozycji dokumentu';

    INSERT INTO RKSTANWAL (RKSTNKAS, DATA , STAN, RKDOKPOZ, KURS , RKDOKNAG,
        PRZYCHODY, ROZCHODY, LPNAG, LPPOZ)
      VALUES (:stanowisko, :data, new.Kwota, new.RKDOKPOZ, :kurs, :RKDOKNAG_ref,
        iif(new.Kwota>0, new.Kwota,0), iif(new.Kwota<0,-new.Kwota ,0), :lpnag, :lppoz)
      returning ref into new.rkstanwal;
  end else begin
    -- sciagam z istniejacego stanu
    update RKSTANWAL set STAN = STAN + new.KWOTA,
        Przychody = Przychody + iif(new.Kwota>0, new.Kwota ,0),
        Rozchody = Rozchody + iif(new.Kwota<0, -new.Kwota, 0)
      where ref = new.RKSTANWAL;
  end
end^
SET TERM ; ^
