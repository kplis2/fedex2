--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTINGPOS_BI FOR PRDACCOUNTINGPOS               
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable prdversion integer;
declare variable firstperiod varchar(6);
begin
  if(new.deductamount is null) then new.deductamount = 0;
  if(new.firstdeductamount is null) then new.firstdeductamount = 0;
  if(new.prdversion is null) then
    exception UNIVERSAL 'Brak okreslonej wersji scenariusza odpisu RMK';

end^
SET TERM ; ^
