--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZLECPOZ_BI_REF FOR ZLECPOZ                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ZLECPOZ')
      returning_values new.REF;
end^
SET TERM ; ^
