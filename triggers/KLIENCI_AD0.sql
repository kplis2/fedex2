--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AD0 FOR KLIENCI                        
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable slodef integer;
begin
  if(old.crm = 1) then begin
    select ref from SLODEF where TYP='KLIENCI' into :slodef;
    if(:slodef is null) then exception SLODEF_BEZKLIENTOW;
    delete from CPODMIOTY where SLODEF = :slodef and SLOPOZ = old.ref;
  end
  delete from PLATNICY where PLATNIK = old.ref;
  delete from bankaccounts where DICTDEF = 1 and DICTPOS = old.ref;
end^
SET TERM ; ^
