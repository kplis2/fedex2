--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOST_EDERULESAFTER_BD_ORDER FOR SPOSDOST_EDERULESAFTER         
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update SPOSDOST_EDERULESAFTER set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and sposdost = old.sposdost;
end^
SET TERM ; ^
