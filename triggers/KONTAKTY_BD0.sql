--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_BD0 FOR KONTAKTY                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
begin
  select count(*) from CPLOPER where KONTAKT=old.ref into :cnt;
  if(:cnt > 0) then exception KONTAKTY_CPLOPERJOINED;
  if(old.zalezny=1 and old.emailwiad is not NULL) then exception KONTAKTY_EMAILWIADJOINED;
end^
SET TERM ; ^
