--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSCHOOLS_AIUD_E_VAC_CARD FOR EPERSCHOOLS                    
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 2 
AS
  declare variable employee integer;
begin

  if (updating and new.fromdate = old.fromdate
    and new.todate is not distinct from old.todate
    and new.isvacbase is not distinct from old.isvacbase
    and new.education = old.education
    and new.person = old.person
  ) then
    exit;

  for
    select ref from employees
      where person = coalesce(new.person, old.person)
      into :employee
  do
    execute procedure e_vac_card(current_date, -employee);
end^
SET TERM ; ^
