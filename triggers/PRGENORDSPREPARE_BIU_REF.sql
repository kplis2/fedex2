--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRGENORDSPREPARE_BIU_REF FOR PRGENORDSPREPARE               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0) then
    execute procedure gen_ref('PRGENORDSPREPARE') returning_values new.ref;
end^
SET TERM ; ^
