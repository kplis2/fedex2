--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLNAGZAM_BI0 FOR CPLNAGZAM                      
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable kod varchar(40);
declare variable suffix varchar(10);
declare variable cpodm integer;
declare variable i integer;
declare variable n integer;
begin
  kod = '';
  suffix = '';
  i = 1;
  select CPODMIOT,NRKARTY from CPLUCZEST where REF = new.CPLUCZEST into :cpodm, :kod;
  if((:kod='') or (:kod is NULL)) then exception CPLNAGZAM_BRAKKODU;
  kod = :kod || '/' || cast(new.data as date);
  select count(*) from CPLNAGZAM where (SYMBOL=:kod) and (REF<>new.REF) into :n;
  while(n>0) do begin
    suffix = '/' || cast(i as varchar(10));
    i = :i+1;
    select count(*) from CPLNAGZAM where (SYMBOL=:kod || :suffix) and (REF<>new.REF) into :n;
  end
  new.symbol = :kod || :suffix;
  if(new.status=3) then new.datareal = current_date;
end^
SET TERM ; ^
