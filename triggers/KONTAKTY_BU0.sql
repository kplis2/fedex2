--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_BU0 FOR KONTAKTY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
begin
  select count(*) from CPLOPER where KONTAKT=old.ref into :cnt;
  if(:cnt > 0 and new.cplopermod = 0) then begin
    if((new.data <> old.data) or (new.data is null and old.data is not null) or (new.data is not null and old.data is null) or
       (new.pkosoba <> old.pkosoba) or (new.pkosoba is null and old.pkosoba is not null) or (new.pkosoba is not null and old.pkosoba is null) or
       (new.inout <> old.inout) or (new.inout is null and old.inout is not null) or (new.inout is not null and old.inout is null) or
       (new.rodzaj <> old.rodzaj) or (new.rodzaj is null and old.rodzaj is not null) or (new.rodzaj is not null and old.rodzaj is null) or
       (new.opis <> old.opis) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null) or
       (new.operator <> old.operator) or (new.operator is null and old.operator is not null) or (new.operator is not null and old.operator is null)
    ) then exception KONTAKTY_CPLOPERJOINED;
  end
  new.cplopermod = 0;
end^
SET TERM ; ^
