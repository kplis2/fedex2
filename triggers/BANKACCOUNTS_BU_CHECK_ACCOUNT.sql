--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_BU_CHECK_ACCOUNT FOR BANKACCOUNTS                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable bank varchar(64);
declare variable control smallint_id;
begin

  if ((new.account is distinct from old.account) or (new.bank is distinct from old.bank)) then
  begin
    update bankaccounts set eaccount = replace(replace(new.account, '-', ''),  ' ',  '')
      where ref = new.ref;

    if (exists(select first 1 1
                 from banki
                 where symbol = new.bank
                   and coalesce(zagraniczny, 0) = 0)
        or (coalesce(new.bank, '') = '')) then

      select bankshortname
        from efunc_get_bank_from_acc(new.eaccount)
        into bank;-- PR55109 WG; BS106390

      if (bank is not null) then
        new.bank = :bank;
  end
end^
SET TERM ; ^
