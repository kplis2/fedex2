--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BI_REF FOR EMPLCONTRACTS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMPLCONTRACTS')
      returning_values new.REF;
end^
SET TERM ; ^
