--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCERTIFS_BIU_WEW_ZEW FOR EWORKCERTIFS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.seniorityflags containing 'STW' and new.seniorityflags containing 'STZ') then
    exception seniority_flag;
end^
SET TERM ; ^
