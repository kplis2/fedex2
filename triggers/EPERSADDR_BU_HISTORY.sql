--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSADDR_BU_HISTORY FOR EPERSADDR                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
--Obsluga statusu adresu, przeniesie adrsu do historii

  if (new.status = 2 and old.status = 1) then
  begin
  --adres przeniesimy do historii tylko wtedy, kiedy cos zmienilismy
    if (old.person <> new.person or
      old.addrtype <> new.addrtype or
      coalesce(old.city,'') <> coalesce(new.city,'') or
      coalesce(old.post,'') <> coalesce(new.post,'') or
      coalesce(old.postcode,'') <> coalesce(new.postcode,'') or
      coalesce(old.street,'') <> coalesce(new.street,'') or
      coalesce(old.houseno,'') <> coalesce(new.houseno,'') or
      coalesce(old.localno,'') <> coalesce(new.localno,'') or
      coalesce(old.phone,'') <> coalesce(new.phone,'') or
      coalesce(old.cpwoj16m,0) <> coalesce(new.cpwoj16m,0) or
      coalesce(old.stpref,'') <> coalesce(new.stpref,'') or
      coalesce(old.communityid,0) <> coalesce(new.communityid,0) or
      coalesce(old.powiat,'') <> coalesce(new.powiat,'') or
      coalesce(old.powiatid,0) <> coalesce(new.powiatid,0)
    ) then begin
    --oznaczenie starego adresu jako archiwalny (status=2)
      insert into epersaddr (person, addrtype, fromdate, todate, city, post,
          postcode, street, houseno, localno, phone, stpref, cpwoj16m,
          communityid, status, powiat, powiatid)
        values (old.person, old.addrtype, old.fromdate, current_date-1, old.city, old.post,
          old.postcode, old.street, old.houseno, old.localno, old.phone, old.stpref, old.cpwoj16m,
          old.communityid, new.status, old.powiat, old.powiatid);
    --nowy/zmodyfikowany adres dostaje status adresu aktualnego:
      new.status = 1;
      new.fromdate = current_date;
    end else
      exception epersaddr_nochange_to_history;
  end
end^
SET TERM ; ^
