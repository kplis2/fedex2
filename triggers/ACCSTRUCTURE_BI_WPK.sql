--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BI_WPK FOR ACCSTRUCTURE                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable slodef_company smallint_id;
declare variable bkaccount bkaccounts_id;
declare variable pattern_bkaccount bkaccounts_id;
declare variable yearid years_id;
declare variable symbol varchar(3);
declare variable patern_slodef slo_id;
declare variable dest_company companies_id;
begin
  --badamy czy accstructure zostala skopiowana ze wzorca
  if (new.pattern_ref is not null) then
  begin
    -- trzeba by teraz skopiować slodefy jak są per company, lub dowiązania
    slodef_company = null;
    select s.company
      from slodef s
      where s.ref = new.dictdef
      into :slodef_company;
    if (slodef_company is distinct from null) then
    begin
      patern_slodef = new.dictdef;
      select b.company
        from bkaccounts b
        where b.ref = new.bkaccount
        into :dest_company;
      --okazuje sie, slownik jest dopiety do firmy wiec musimy zrobic kopie slownika
      execute procedure copy_slodef_slopoz(new.dictdef,null,:dest_company)
        returning_values new.dictdef;
      -- a na koniec dopiać synchronizacje do niego
      update slodef s
        set s.patternref = :patern_slodef
        where s.ref = new.dictdef;
    end
  end else
  begin
    if (exists (select first 1 1 from bkaccounts b where b.ref = new.bkaccount and coalesce(b.pattern_ref,0)>0)) then
      exception universal'Konto syntetyczne synchronizowane, brak możliwości dodania poziomu analitycznego z poza wzorca';
  end
end^
SET TERM ; ^
