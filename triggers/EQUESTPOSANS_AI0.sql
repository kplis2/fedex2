--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EQUESTPOSANS_AI0 FOR EQUESTPOSANS                   
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable symbol string10;
  declare variable ecandidate integer;
  declare variable ereccand integer;
begin
  select max(pd.symbol)
    from equestpos p
      join equestposdef pd on (p.question = pd.question)
    where p.ref = new.equestpos
    into :symbol;
  if (symbol = 'WAGEEXP') then
  begin
    select h.ecandidate, h.ereccand
      from equestpos p
        join equestheader h on (h.ref = p.equestheader)
      where p.ref = new.equestpos
      into :ecandidate, :ereccand;
    update ereccands set wageexp = substring(new.answer from 1 for 100) where ref = :ereccand;
  end
end^
SET TERM ; ^
