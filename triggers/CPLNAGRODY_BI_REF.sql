--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLNAGRODY_BI_REF FOR CPLNAGRODY                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPLNAGRODY')
      returning_values new.REF;
end^
SET TERM ; ^
