--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AI_ALTPOZ FOR DOKUMPOZ                       
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable docsetsautoadd smallint;
declare variable kplnagref integer;
declare variable skad smallint;
begin
  execute procedure getconfig('DOCSETSAUTOADD')
    returning_values :docsetsautoadd;
  if (:docsetsautoadd is null) then docsetsautoadd = 0;

  select coalesce(zrodlo,0) from dokumnag where ref = new.dokument
  into :skad;

  if (:skad = 0 and coalesce(new.fake,0) = 0 and new.frompozzam is null and
      new.orgdokumpoz is null and :docsetsautoadd > 0 and
      exists(select first 1 1 from towary where ktm = new.ktm and usluga <> 1 and coalesce(altposmode,0) > 0)) then
  begin
    select first 1 ref from kplnag
      where ktm = new.ktm and wersjaref = new.wersjaref and akt = 1 and glowna = 1
    into :kplnagref;

    if (:kplnagref is not null) then
    begin

      insert into dokumpoz(dokument, numer, ord, ktm, wersjaref, ilosc, alttopoz, fake,
          kortopoz,
          cenacen, cenabru, cenanet, cenawal,
          x_slownik)  --xxx MKD
        select new.dokument, new.numer, 1, p.ktm, p.wersjaref, p.ilosc * new.ilosc, new.ref, 1,
            case when new.kortopoz is null then null
              else
                (select dp1.ref
                  from dokumpoz dp
                    left join dokumpoz dp1 on (dp.ref = dp1.alttopoz)
                  where dp.ref = new.kortopoz
                    and dp1.wersjaref = p.wersjaref)
            end,
            0, 0, 0, 0,
           new.x_slownik-- XXX MKD
          from kplpoz p
          where p.nagkpl = :kplnagref;
    end
  end

  if (coalesce(new.fake,0) > 0) then
  begin
    update dokumpoz set havefake = 1
      where ref = new.alttopoz and coalesce(havefake, 0) <> 1;
  end
end^
SET TERM ; ^
