--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDADDONACC_BI_REF FOR CDADDONACC                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  IF (NEW.REF IS NULL) THEN
    execute procedure gen_ref('CDADDONACC') returning_values NEW.REF;
end^
SET TERM ; ^
