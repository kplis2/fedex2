--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AU_ZAMOWIENIE FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable zamowienie integer;
begin
  if((new.numer<>old.numer) or (new.anulowany<>old.anulowany)
     or (new.status <> old.status)) then
  begin
    for select ZAMOWIENIE
          from RKDOKPOZ
          where DOKUMENT=new.ref
          into :zamowienie
    do begin
      if(:zamowienie is not null and :zamowienie<>0) then begin
        execute procedure RK_OBLICZ_ZALICZKI(:zamowienie);
      end
    end
  end
end^
SET TERM ; ^
