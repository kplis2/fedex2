--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_BI_REF FOR RKDOKNAG                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('RKDOKNAG')
      returning_values new.REF;
end^
SET TERM ; ^
