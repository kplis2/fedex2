--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_AU1 FOR PRSCHEDOPERS                   
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable seclen double precision;
begin

  --aktualizacja czasow zakocznenia i poczatku na przewodniku, ew. aktualizacja czasu zakonczenia
  seclen = 1;
  seclen = :seclen/24/60/60;
  if(new.calcdeptimes in(1,3) and (
    new.starttime <> old.starttime or (new.starttime is not null and old.starttime is null)
    or (abs(new.endtime - old.endtime) > :seclen ) or (new.endtime is not null and old.endtime is null)
    )
  )then begin
    execute procedure PRSCHEDGUIDE_CHECKTIMESTARTEND(new.guide);
    update prschedopers set prschedopers.calcdeptimes = 2
      where schedule = new.schedule and machine = new.machine
      and (status = 0 or (status = 1) or(status = 2)) and ref<>new.ref and calcdeptimes = 1;
    /*update prschedopers set prschedopers.calcdeptimes = 2
      where schedule = new.schedule and guide = new.guide
      and (status = 0 or (status = 1) or(status = 2)) and ref<>new.ref and calcdeptimes = 1;*/
  end
  --okreslenie statusu materialowego na przewodniku
  if(new.calcdeptimes = 1 and new.statusmat <> old.statusmat) then
    execute procedure prschedguides_statusmatcheck(new.guide);
end^
SET TERM ; ^
