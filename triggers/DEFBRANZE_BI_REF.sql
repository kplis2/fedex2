--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFBRANZE_BI_REF FOR DEFBRANZE                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFBRANZE')
      returning_values new.REF;
end^
SET TERM ; ^
