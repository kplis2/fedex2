--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGESTYPES_AD FOR PRSHORTAGESTYPES               
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  delete from PRSHORTAGESPRDEPARTS p where p.prshortagetype = old.symbol;
end^
SET TERM ; ^
