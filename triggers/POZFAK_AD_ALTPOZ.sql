--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AD_ALTPOZ FOR POZFAK                         
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if (coalesce(old.fake,0) > 0) then
  begin
    if (not exists(select first 1 1 from pozfak where alttopoz = old.alttopoz)) then
      update pozfak
        set havefake = 0
      where ref = old.alttopoz;
  end
  if (coalesce(old.havefake,0) > 0) then
  begin
    delete from pozfak
      where dokument = old.dokument
        and alttopoz = old.ref
        and coalesce(fake,0) > 0;
  end
end^
SET TERM ; ^
