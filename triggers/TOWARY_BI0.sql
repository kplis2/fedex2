--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWARY_BI0 FOR TOWARY                         
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable haspkwiu smallint;
declare variable cnt integer;
declare variable numer integer;
declare variable symbol varchar(40);
begin

  select coalesce(tow.rights,''), coalesce(tow.rightsgroup,'')
    from towtypes tow where tow.numer = new.usluga
  into new.prawa, new.prawagrup;

  if(new.AKT is NULL)then new.AKT=1;
  if(new.cena_zakn is null) then new.cena_zakn = 0;
  if(new.cena_zakb is null) then new.cena_zakb = 0;
  if(new.cena_fabn is null) then new.cena_fabn = 0;
  if(new.cena_fabb is null) then new.cena_fabb = 0;
  if(new.cena_kosztn is null) then new.cena_kosztn = 0;
  if(new.cena_kosztb is null) then new.cena_kosztb = 0;
  if(new.cena_ZAKNL is null) then new.cena_zaknl = 0;
  if(new.dniwazn is null) then new.dniwazn = 0;
  if(new.serial is null) then new.serial = 0;
  if(new.serjedn is null) then new.serjedn = 0;
  if(new.serpojedyn is null) then new.serpojedyn = 0;
  if(new.sercyfr is null) then new.sercyfr = 0;
  if(new.serautowyd is null) then new.serautowyd = 0;
  if(new.iloscwoddziale is null) then new.iloscwoddziale = 0;
  if(new.usluga is null) then new.usluga = 0;
  if(new.altposmode is null) then new.altposmode = 0;
  if(new.magbreak is null) then begin
    execute procedure GETCONFIG('MAGBREAK') returning_values new.magbreak;
    if(new.magbreak is null) then new.magbreak = 0;
  end
  if(new.serial = 0) then begin
    new.serjedn = 0;
    new.serpojedyn = 0;
    new.sercyfr = 0;
    new.serautowyd = 0;
  end
  new.iss = 0; new.isc = 0;
  new.isz = 0;
  new.ds = 0;
  new.dz = 0;
  new.dc = 0;
  new.dm = 0;
  new.lastcenzmiana = current_time;
  if(new.ktm is null) then new.ktm = '';
  if(new.nazwa is null) then new.nazwa = '';
  if(new.miara is null) then new.miara = '';
  if(new.vat = '') then new.vat = null;
  if(new.ktm = '') then exception TOWARY_BEZKTM;
  if(new.miara = '') then exception TOWARY_BEZKTM 'Brak określonej jednostki miary dla asortymentu.';
  if(new.nazwa = '') then exception TOWARY_BEZNAZWY;
  if(new.kodkresk is not null and new.kodkresk <> '') then new.kodkreskadd = current_date;
  if(new.vat is not null) then begin
    select HASPKWIU from VAT where VAT.grupa = new.vat into :haspkwiu;
    if(:haspkwiu > 0) then begin
      if(new.pkwiu is null or new.pkwiu = '') then exception TOWARY_MUSTHAVEPKWIU;
    end
  end
  if(new.vat is not null) then begin
    select count(*) from VAT where grupa = new.vat into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then exception
      TOWARY_BLEDNYVAT;
  end
  new.regdate = current_date;
  if(new.akt = 1) then begin
    numer=0;
    execute procedure Get_NUMBER('Kod w kasie', null, null, null, 0, null) returning_values :numer, :symbol;
    if (:numer is null or :numer=0) then exception GENERATOR_NOT_DEFINED 'Niepoprawnie wygenerowany kod zewnetrzny. Akceptacja niemożliwa.';
    else new.kodwkasie = cast(:numer as varchar(6));
  end
  if (coalesce(new.mwsnazwa,'') = '') then
    new.mwsnazwa = new.nazwa;
end^
SET TERM ; ^
