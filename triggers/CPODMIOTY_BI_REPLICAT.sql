--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_BI_REPLICAT FOR CPODMIOTY                      
  ACTIVE BEFORE INSERT POSITION 3 
AS
begin
 execute procedure rp_trigger_bi('CPODMIOTY',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
