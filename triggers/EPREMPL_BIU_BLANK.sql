--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPREMPL_BIU_BLANK FOR EPREMPL                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
  declare variable fromdate timestamp;
  declare variable todate timestamp;
  declare variable empltype smallint;
begin

  if (inserting or new.employee <> old.employee or new.epayroll <> old.epayroll) then
  begin
    new.status = coalesce(new.status,0);

    select person
      from employees where ref = new.employee
      into new.person;

    execute procedure efunc_prdates(new.epayroll, 0)
      returning_values fromdate, todate;

    select empltype
      from epayrolls
      where ref = new.epayroll
      into :empltype;

    select first 1 coalesce(a.department, c.department), coalesce(a.branch, c.branch) --BS54808
      from emplcontracts c
      left join econtrannexes a on (a.emplcontract = c.ref and a.fromdate <= :todate
                                  and (a.todate is null or a.todate >= :fromdate))
      where c.employee = new.employee
        and c.fromdate <= :todate
        and c.empltype = :empltype  --BS55105
        and coalesce(c.enddate,c.todate,:fromdate) >= :fromdate
      order by c.fromdate desc, a.fromdate desc
      into new.department, new.branch;
  end
end^
SET TERM ; ^
