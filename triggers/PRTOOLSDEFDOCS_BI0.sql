--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDEFDOCS_BI0 FOR PRTOOLSDEFDOCS                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.inssue is null) then new.inssue = 0;
  if (new.outside is null) then new.outside = 0;
  if (new.operrights is null) then new.operrights = '';
  if (new.editemployee is null) then new.editemployee = 0;
  if (new.editprschedzam is null) then new.editprschedzam = 0;
  if (new.editprschedguide is null) then new.editprschedguide = 0;
  if (new.editprmachine is null) then new.editprmachine = 0;
  if (new.editslodef is null) then new.editslodef = 0;
end^
SET TERM ; ^
