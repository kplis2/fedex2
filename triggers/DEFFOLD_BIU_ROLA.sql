--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFOLD_BIU_ROLA FOR DEFFOLD                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 10 
AS
begin
  if(new.rola is null) then
    new.rola = 0;
  if(new.rola = 0) then
    new.domyslny = 0;
  else if (new.rola = 5) then
   new.domyslny = 2;
  else
    new.domyslny = 1;
  if(new.rola = 5) then--korzen
    new.piktogram = 13;
end^
SET TERM ; ^
