--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANPERSONS_BI_REF FOR PMPLANPERSONS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('PMPLANPERSONS') returning_values new.REF;

end^
SET TERM ; ^
