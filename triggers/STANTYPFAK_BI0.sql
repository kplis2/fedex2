--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANTYPFAK_BI0 FOR STANTYPFAK                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable zak smallint;
begin
  if(new.stansprzed is null) then new.stanfaktyczny = '';
  else new.stanfaktyczny = new.stansprzed;
  select KOREKTA, ZAKUP, SAD from TYPFAK where SYMBOL = new.TYPFAK into new.korekta, :zak, new.sad;
  select ZAKUPU from STANSPRZED where  STANOWISKO = new.stansprzed into new.zakupu;
  if(:zak <> new.zakupu) then
    exception STANTYPDAFAK_ZAKNIESG;
  if(new.zakupu is null) then new.zakupu = 0;
  if(new.akcjaackafter is null) then  new.akcjaackafter = '';
  if(new.realzamclose is null) then new.realzamclose = 0;
end^
SET TERM ; ^
