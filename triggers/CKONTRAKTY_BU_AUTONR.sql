--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BU_AUTONR FOR CKONTRAKTY                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable nazwa varchar(40);
declare variable contrtype varchar(40);
declare variable symbol varchar(40);
declare variable autonr smallint_id;
declare variable BLOCKREF integer;
begin
  if (new.contrtype <> old.contrtype) then
  begin
     select ct.numbergen, ct.symbol
      from contrtypes ct
      where ct.ref = old.contrtype
    into :nazwa, :contrtype;
     --
    if (coalesce(old.numer, 0) <> 0) then begin
      execute procedure free_number(:nazwa, '', substring(:contrtype from 1 for 20), old.datazal, old.numer, 0)
        returning_values :blockref;
    end

    select ct.numbergen, ct.symbol, ct.autonr
      from contrtypes ct where ct.ref = new.contrtype
    into :nazwa, :contrtype, :autonr;

    if (:autonr = 1) then begin
      execute procedure get_number(:nazwa, '', substring(:contrtype from 1 for 20),new.datazal, null, new.ref)
      returning_values new.numer, :symbol;
      new.symbol = substring(:symbol from 1 for 20);
    end
    else begin
      new.numer = null;
      new.symbol = null;
    end
  end
end^
SET TERM ; ^
