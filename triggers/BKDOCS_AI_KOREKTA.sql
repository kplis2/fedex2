--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AI_KOREKTA FOR BKDOCS                         
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable kind SMALLINT;
declare variable netvmulsign SMALLINT;
begin
  --  Jeżeli jestemy korektą należy skopiować i dowiązać pozycje z dokumentu korygoweanego
  --  jednak z wartociami zerowymi
  -- Zeruje wartoci zgodnie z projektem PR54296


  if (coalesce(new.bkdocsvatcorrectionref,0)>0) then
  begin
    netvmulsign = -1; --- BS92417
    -- Pobieramy wartość 1 ( zakup ) lub 2 ( sprzedaż )
    select bp.kind
        from bkdoctypes bp
        where bp.ref = new.doctype
      into :kind;


      -- Jeli zakup to zerujemy mnożnik wartości netto, która nie wchodzi do podstawy opodatkowania
      if(kind = 1) then begin
        netvmulsign = 0;
      end

    insert into bkvatpos ( BKDOC, NUMBER, NETV, VATV, GROSSV, TAXGR, VATGR, ACCOUNT1, ACCOUNT2,
      VATREG, CURRNETV, ORD, DEBITED, DESCRIPT, DIST1DDEF, DIST1SYMBOL, DIST2DDEF, DIST2SYMBOL,
      DIST3DDEF, DIST3SYMBOL, DIST4DDEF, DIST4SYMBOL, DIST5DDEF, DIST5SYMBOL, COMPANY, VATGRF,
      VATVF, NET, DIST6DDEF, DIST6SYMBOL, FUELKIND, FUELQUANTITY, MATCHINGSYMBOL1, MATCHINGSYMBOL2,
      BKVATPOSVATCORRENCTIONREF, VATE, DEBITTYPE, DEBITPROCENT)
    select new.ref, NUMBER,:netvmulsign*netv, -(vatv), -(GROSSV), TAXGR, VATGR, ACCOUNT1, ACCOUNT2,
      VATREG, CURRNETV, ORD, DEBITED, DESCRIPT, DIST1DDEF, DIST1SYMBOL, DIST2DDEF, DIST2SYMBOL,
      DIST3DDEF, DIST3SYMBOL, DIST4DDEF, DIST4SYMBOL, DIST5DDEF, DIST5SYMBOL, COMPANY, VATGRF,
      VATVF, NET, DIST6DDEF, DIST6SYMBOL, FUELKIND, FUELQUANTITY, MATCHINGSYMBOL1, MATCHINGSYMBOL2,
      REF, -(VATE), DEBITTYPE, DEBITPROCENT
      from bkvatpos b
      where b.bkdoc = new.bkdocsvatcorrectionref;
  end
end^
SET TERM ; ^
