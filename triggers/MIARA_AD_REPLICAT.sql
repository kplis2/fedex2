--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MIARA_AD_REPLICAT FOR MIARA                          
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('MIARA', old.miara, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
