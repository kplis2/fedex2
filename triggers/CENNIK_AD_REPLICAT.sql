--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_AD_REPLICAT FOR CENNIK                         
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('CENNIK', old.cennik, old.wersjaref, old.jedn, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
