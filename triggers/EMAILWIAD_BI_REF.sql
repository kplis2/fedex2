--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_BI_REF FOR EMAILWIAD                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMAILWIAD')
      returning_values new.REF;
end^
SET TERM ; ^
