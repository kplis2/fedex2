--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FLDFLAGSDEF_BIU0 FOR FLDFLAGSDEF                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.fldflagsgrp='') then new.fldflagsgrp = null;
  if (new.symbol='') then new.symbol = null;
  if (new.name='') then new.name = null;
end^
SET TERM ; ^
