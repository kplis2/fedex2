--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYMENT_AIU_EMPLOYEES FOR EMPLOYMENT                     
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
  declare variable minfromdate date;
  declare variable maxfromdate date;
  declare variable todate date;
begin
  select first 1 fromdate
    from employment
    where employee = new.employee
    order by fromdate
    into :minfromdate;

  select first 1 fromdate
    from employment
    where employee = new.employee
    order by fromdate desc
    into :maxfromdate;

  select todate
    from employment
    where employee = new.employee and fromdate = :maxfromdate
    into :todate;

  if (minfromdate <= current_date and (todate >= current_date or todate is null)) then
   begin
     update employees set fromdate = :minfromdate, todate = :todate, emplstatus = 1
       where ref = new.employee;
   end
  else
    begin
      update employees set fromdate = :minfromdate, todate = :todate, emplstatus = 0
        where ref = new.employee;
    end
end^
SET TERM ; ^
