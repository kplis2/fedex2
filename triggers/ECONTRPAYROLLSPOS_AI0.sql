--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRPAYROLLSPOS_AI0 FOR ECONTRPAYROLLSPOS              
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.countnag = 0 and new.points <> 0) then
    execute procedure ECONTRPAYROLLS_CALCULATE(new.econtrpayrollnag);
  if (new.accord = 2 and new.points <> 0 and new.countnag = 0 and new.econtractsdef is not null) then
    execute procedure ECONTRPAYROLLS_CALC_ADDINGS(new.econtrpayrollnag);
end^
SET TERM ; ^
