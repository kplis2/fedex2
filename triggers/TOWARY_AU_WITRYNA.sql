--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWARY_AU_WITRYNA FOR TOWARY                         
  ACTIVE AFTER UPDATE POSITION 9 
AS
begin
  if (new.witryna <> old.witryna) then
    update wersje set witryna = new.witryna where ktm = new.ktm and nrwersji = 0;
end^
SET TERM ; ^
