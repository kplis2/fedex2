--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_BU_LISTA FOR CPLUCZEST                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.nadrzedny <> old.nadrzedny or (new.nadrzedny is null and old.nadrzedny is not null) or (new.nadrzedny is not null and old.nadrzedny is null) or (new.lista is null)) then begin
    if(new.nadrzedny is not null and new.nadrzedny > 0) then begin
      select lista from cpluczest where ref = new.nadrzedny into new.lista;
      if(new.lista is null) then new.lista = ';';
      new.lista = ';' || new.ref || new.lista;
    end else begin
      new.lista = ';'|| new.ref ||';';
      end
  end
end^
SET TERM ; ^
