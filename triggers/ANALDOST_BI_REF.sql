--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ANALDOST_BI_REF FOR ANALDOST                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ANALDOST')
      returning_values new.REF;
end^
SET TERM ; ^
