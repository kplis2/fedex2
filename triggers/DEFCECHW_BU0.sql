--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHW_BU0 FOR DEFCECHW                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable typ smallint;
declare variable tworzyktm integer;
declare variable dlugosc integer;
declare variable len integer;
begin
    select TYP,TWORZYKTM,DLUGOSC from DEFCECHY where DEFCECHY.symbol = new.cecha into :typ,:tworzyktm,:dlugosc;
    if(:typ < 2) then begin
      new.wartdate = null;
      select str from change_numeric_to_str(new.wartnum) into new.wartstr;
    end else if(:typ = 2) then begin
      new.wartnum = null;
      new.wartdate = null;
    end else begin
      new.wartnum = null;
      new.wartstr = coalesce(cast(new.wartdate as date),'');
    end
  if(new.wartstr is null and new.wartnum is null and new.wartdate is null) then
      exception DEFCECHW_NOVALUE;
  if(coalesce(new.wartstr,'')<>coalesce(old.wartstr,'')) then begin
    len = coalesce(char_length(new.wartstr),0); -- [DG] XXX ZG119346
    if(:tworzyktm>0 and :dlugosc>0 and :dlugosc<>:len) then
      exception DEFCECHW_NOVALUE 'Długość wartości cechy powinna wynosić '||:dlugosc||' zn.';
  end
  end^
SET TERM ; ^
