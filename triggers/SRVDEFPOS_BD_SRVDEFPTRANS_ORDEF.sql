--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOS_BD_SRVDEFPTRANS_ORDEF FOR SRVDEFPOS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update srvdefpostrans set ord = 0 where srvdefpos = old.ref;
end^
SET TERM ; ^
