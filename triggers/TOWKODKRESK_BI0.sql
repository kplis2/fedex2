--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWKODKRESK_BI0 FOR TOWKODKRESK                    
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable cnt integer;
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select ktm from WERSJE where ref=new.wersjaref into new.ktm;
  else if(new.ktm is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = 0 into new.wersjaref;
  select count(*) from TOWKODKRESK where wersjaref = new.wersjaref into :cnt;
  if(:cnt is null or (:cnt = 0)) then new.gl = 1;
  if(new.towjednref is null or (new.towjednref = 0)) then
     select REF from TOWJEDN where KTM = new.ktm and towjedn.glowna > 0 into new.towjednref;
  if(new.kodkresk = '') then exception KODKRESK_EMPTY;
end^
SET TERM ; ^
