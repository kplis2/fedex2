--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOT_BD0 FOR DOKUMNOT                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable chronologia smallint;
declare variable cnt integer;
declare variable sblokadadelay varchar(255);
declare variable blokadadelay integer;
declare variable local integer;
begin
  /* procedura odakceptowania tutaj, bo w trigerze AFTER już nie ma pozycji - usuniete przez foregin key'a */
  execute procedure CHECK_LOCAL('DOKUMNOT',old.ref) returning_values :local;
  if(:local = 1) then begin
    select count(*) from DOKUMNOT where GRUPA = old.grupa and REF > old.ref into :cnt;
    if(:cnt > 0) then begin
      select count(ref) from dokumnotp where dokument = old.ref into :cnt;
      if (:cnt > 0) then
        exception DOKUMNOT_SADOKNEXT;
    end
  end
  if(old.AKCEPT = 1) then begin
   select chronologia from DEFMAGAZ where symbol=old.magazyn into :chronologia;
   if(:chronologia is null) then chronologia = 0;
   /*kontrola chronologii*/
   if(:chronologia > 0) then begin
      cnt = null;
      select count(*) from DOKUMNOT where MAGAZYN=old.magazyn and DATA>=(old.data+:chronologia) and akcept = 1 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt > 0) then
        exception DOKUMNG_AKCEP_POZNIEJ;
   end
   execute procedure GETCONFIG('SIDFK_BLOKADA') returning_values :sblokadadelay;
   if(old.blokada > 0 and old.token <> 7) then begin
     if(:sblokadadelay <> '') then
       blokadadelay = cast(:sblokadadelay as integer);
     else blokadadelay = 0;
     if(bin_and(old.blokada,1)>0) then exception DOKUMNAG_USUBLOK;
     if(bin_and(old.blokada,4)>0 and current_date >= old.data + :blokadadelay) then exception DOKUMNAG_ZAKSIEGOWANY;
   end
   execute procedure DOKUMNOT_ACK(old.ref,0);
  end else
    if(old.blokada > 0 and old.token <> 7) then
      exception DOKUMNAG_USUBLOK;
end^
SET TERM ; ^
