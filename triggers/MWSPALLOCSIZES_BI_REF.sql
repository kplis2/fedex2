--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSPALLOCSIZES_BI_REF FOR MWSPALLOCSIZES                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSPALLOCSIZES') returning_values new.ref;
end^
SET TERM ; ^
