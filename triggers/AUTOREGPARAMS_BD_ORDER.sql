--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AUTOREGPARAMS_BD_ORDER FOR AUTOREGPARAMS                  
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update autoregparams set ord = 0, numer = numer - 1
    where (autoregschem <> old.autoregschem or parametr <> old.parametr) and numer > old.numer and autoregschem = old.autoregschem;
end^
SET TERM ; ^
