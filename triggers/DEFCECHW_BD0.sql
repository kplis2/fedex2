--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHW_BD0 FOR DEFCECHW                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable ilosc integer;
begin
ilosc = 0;
  if(old.wartstr is not NULL) then begin
    select count(*) from ATRYBUTY where CECHA = old.cecha and WARTOSC = old.wartstr into :ilosc;
    if(ilosc>0) then exception DEFCECHW_ATR_WART1;
  end
end^
SET TERM ; ^
