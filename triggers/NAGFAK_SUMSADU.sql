--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_SUMSADU FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
 if(new.SAD=1) then begin
   if(new.kosztdost is null) then new.kosztdost=0;
   if(new.sumvat is null) then new.sumvat=0;
   if(new.sumclo is null) then new.sumclo=0;
   new.sumsad=new.sumclo+new.sumvat+new.kosztdost;
 end
end^
SET TERM ; ^
