--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERFREE_BU0 FOR NUMBERFREE                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.ref is null or (new.ref = 0)) then
    execute procedure GEN_REF('NUMBERFREE') returning_values new.ref;
end^
SET TERM ; ^
