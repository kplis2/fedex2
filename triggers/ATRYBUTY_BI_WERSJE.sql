--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_BI_WERSJE FOR ATRYBUTY                       
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable vwersje smallint;
begin
  select wersje from defcechy where symbol = new.cecha into :vwersje;
  if (:vwersje = 0 and new.nrwersji>0) then exception ATRYBUT_CECHA;
end^
SET TERM ; ^
