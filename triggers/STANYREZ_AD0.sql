--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYREZ_AD0 FOR STANYREZ                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable typ char;
begin
 select d.typ
    from defmagaz d
    where d.symbol = old.magazyn
    into :typ;
 if(((old.status = 'R') or (old.status = 'Z')) and old.zreal <> 1)then
   if(old.onstcen = 1) then
   update STANYCEN set ZAREZERW= ZAREZERW-old.ilosc where MAGAZYN=old.magazyn AND KTM=old.ktm AND WERSJA = old.wersja
              and ((old.cena = cena) or (old.cena is null or (old.cena = 0 and :typ = 'E')))
              and ((old.dostawa = dostawa) or (old.dostawa is null));
   else
   update STANYIL set ZAREZERW= ZAREZERW-old.ilosc where MAGAZYN=old.magazyn AND KTM=old.ktm AND WERSJA = old.wersja;
 else if((old.status = 'B') and (old.zreal <> 1)) then
   if(old.onstcen = 1) then
   update STANYCEN set ZABLOKOW = ZABLOKOW-old.ilosc where MAGAZYN=old.magazyn AND KTM=old.ktm AND WERSJA = old.wersja
              and ((old.cena = cena) or (old.cena is null or (old.cena = 0 and :typ = 'E')))
              and ((old.dostawa = dostawa) or (old.dostawa is null));
   else
   update STANYIL set ZABLOKOW = ZABLOKOW-old.ilosc where MAGAZYN=old.magazyn AND KTM=old.ktm AND WERSJA = old.wersja;
 else if((old.status = 'D') and (old.zreal = 0)) then
   if(old.onstcen = 1) then
   update STANYCEN set ZAMOWIONO = ZAMOWIONO-old.ilosc where MAGAZYN=old.magazyn AND KTM=old.ktm AND WERSJA = old.wersja
              and ((old.cena = cena) or (old.cena is null or (old.cena = 0 and :typ = 'E')))
              and ((old.dostawa = dostawa) or (old.dostawa is null));
   else
   update STANYIL set ZAMOWIONO = ZAMOWIONO-old.ilosc where MAGAZYN=old.magazyn AND KTM=old.ktm AND WERSJA = old.wersja;
end^
SET TERM ; ^
