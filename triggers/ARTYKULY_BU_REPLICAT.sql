--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ARTYKULY_BU_REPLICAT FOR ARTYKULY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.dzial <> old.dzial or (new.dzial is null and old.dzial is not null) or (new.dzial is not null and old.dzial is null) or
     new.numer <> old.numer or (new.numer is null and old.numer is not null) or (new.numer is not null and old.numer is null) or
     new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null) or
     new.klasa <> old.klasa or (new.klasa is null and old.klasa is not null) or (new.klasa is not null and old.klasa is null) or
     new.state = -2
  ) then
     execute procedure REPLICAT_STATE('ARTYKULY',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
end^
SET TERM ; ^
