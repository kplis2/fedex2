--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BI_ORDER FOR DOKUMPOZ                       
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;

  /*
  rozwiazanie dotyczace fake-ow ustalone z Marcinem Smereka
  pozycje fake dostaja numer pozycji nadrzednej - alttopoz
  */
  if (coalesce(new.alttopoz,0) > 0 and coalesce(new.fake,0) > 0) then
  begin
    select numer from dokumpoz where ref = new.alttopoz
    into new.numer;
    new.ord = 1;
  end

  if (new.ord = 1 and new.numer is not null) then begin
    new.ord = 0;
    exit;
  end
  select max(numer) from dokumpoz where dokument = new.dokument
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update dokumpoz set ord = 1, numer = numer + 1
      where numer >= new.numer and dokument = new.dokument;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
