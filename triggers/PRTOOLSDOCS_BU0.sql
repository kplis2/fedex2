--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCS_BU0 FOR PRTOOLSDOCS                    
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable numberg varchar(20);
  declare variable editemployee smallint;
  declare variable editprschedzam smallint;
  declare variable editprschedguide smallint;
  declare variable editprmachine smallint;
  declare variable editslodef smallint;
  declare variable authorization smallint;
begin
  if (old.akcept <> new.akcept) then
  begin
    select p.numbergen, p.editemployee, p.editprschedzam, p.editprschedguide,
        p.editprmachine, p.editslodef, p.authorization
      from prtoolsdefdocs p
      where p.prdsdef = new.prdsdef
        and p.symbol = new.prtoolsdefdoc
      into :numberg, :editemployee, :editprschedzam, :editprschedguide,
        :editprmachine, :editslodef, :authorization;
    if (:numberg is null) then
      exception prtoolsdocs_error 'Brak zdefiniowanego numeratora';

    if (old.akcept = 0 and new.akcept = 1) then
    begin
      execute procedure get_number(:numberg, new.prdsdef, new.prtoolsdefdoc, new.docdate, new.numberblock, new.ref)
        returning_values new.number, new.symbol;

      if (:editemployee = 2 and coalesce(new.employee,0) = 0) then
        exception prtoolsdocs_error 'Brak wskazanego pracownika na dokumencie';
      else if (:editemployee = 0 ) then 
        new.employee = null;
      if (:editprmachine = 2 and coalesce(new.prmachine,0) = 0) then
        exception prtoolsdocs_error 'Brak wskazanej maszyny na dokumencie';
      else if (:editprmachine = 0 ) then
        new.prmachine = null;
      if (:editprschedguide = 2 and coalesce(new.prschedguides,0) = 0) then
        exception prtoolsdocs_error 'Brak wskazanego przewodnika na dokumencie';
      else if (:editprschedguide = 0 ) then
        new.prschedguides = null;
      if (:editprschedzam = 2 and coalesce(new.prschedzam,0) = 0) then
        exception prtoolsdocs_error 'Brak wskazanego zlecenia na dokumencie';
      else if (:editprschedzam = 0 ) then
        new.prschedzam = null;
      if (:editslodef = 2 and (coalesce(new.slodef,0) = 0 or coalesce(new.slopoz,0) = 0)) then
        exception prtoolsdocs_error 'Brak wskazanego slownika na dokumencie';
      else if (:editslodef = 0 ) then
      begin
        new.slodef = null;
        new.slopoz = null;
      end
      if (coalesce(:authorization,0) > 0 and coalesce(new.authorizationemployee,0) = 0) then
        exception prtoolsdocs_error 'Dokument wymaga autoryzacji';

    end else if (old.akcept = 1 and new.akcept = 0) then
    begin
      execute procedure free_number(:numberg, new.prdsdef, new.prtoolsdefdoc, new.docdate, new.number, 0)
        returning_values new.numberblock;
      new.number = -1;
      new.symbol = 'TYM/'||cast(new.ref as varchar(40));
      new.authorizationemployee = null;
    end
  end

  if (new.prschedguides is not null and new.prschedzam is null) then
    select g.prschedzam
      from prschedguides g
      where g.ref = new.prschedguides
      into new.prschedzam;
end^
SET TERM ; ^
