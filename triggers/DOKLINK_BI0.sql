--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKLINK_BI0 FOR DOKLINK                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable blokada smallint;
declare variable operblok integer;
declare variable aktuoperator integer;
begin
  if(coalesce(new.status,0) <> 2) then begin
    if(not exists (select d.ref from doklink d where d.dokplik = new.dokplik and d.status = 2)) then new.status = 2;
    else new.status = 1;
  end
  if(new.wersja is null) then new.wersja = 1;
  select blokada, operblok from dokplik where ref = new.dokplik into :blokada, :operblok;
  --if(coalesce(blokada,0) <> 1 and coalesce(blokada,0) <> 3) then
  --  exception doklink_error 'Dokument niepobrany. Dodawanie plików zabronione';
  execute procedure get_global_param('AKTUOPERATOR') returning_values :aktuoperator;
  if(:blokada=1 and coalesce(operblok,0) <> coalesce(aktuoperator,0)) then
    exception doklink_error 'Dokument pobrany przez innego operatora. Dodawanie plików zabronione' || coalesce(operblok,0) || coalesce(aktuoperator,0);
  if(not exists (select ref from doklink where dokplik = new.dokplik and coalesce(ref,0) <> coalesce(new.ref,0))) then new.status = 2;
  execute procedure get_global_param ('AKTUOPERATOR') returning_values new.addoper;
  if(new.versioning is null)then new.versioning = 0;
  -- podczas dodawania nowych plikow oznaczamy, ze ich jeszcze nie ma w repozytorium
  new.nowy = 1;
  new.formatfield = 'PLIK=*=@      *;';
end^
SET TERM ; ^
