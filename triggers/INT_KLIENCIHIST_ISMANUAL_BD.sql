--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_KLIENCIHIST_ISMANUAL_BD FOR INT_KLIENCIHIST                
  ACTIVE BEFORE DELETE POSITION 10 
as
begin
  if(exists(
      select nz.ref from nagzam nz
        where nz.int_klienthist is not distinct from old.ref
      union
      select nf.ref from nagfak nf
        where nf.int_klienthist is not distinct from old.ref
      union
      select dn.ref from dokumnag dn
        where dn.int_klienthist is not distinct from old.ref)
  )then
    exception universal 'Nie można usunąć. Dane rozliczeniowe są używane.';
end^
SET TERM ; ^
