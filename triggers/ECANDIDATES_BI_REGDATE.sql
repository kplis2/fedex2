--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECANDIDATES_BI_REGDATE FOR ECANDIDATES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  new.regdate = current_timestamp(0);
  execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoper;
end^
SET TERM ; ^
