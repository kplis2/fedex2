--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKWITNAG_BI_REF FOR DOKWITNAG                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DOKWITNAG')
      returning_values new.REF;
end^
SET TERM ; ^
