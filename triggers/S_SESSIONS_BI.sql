--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_SESSIONS_BI FOR S_SESSIONS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  new.connectionid = current_connection;
  new.regdate = current_timestamp(0);
  new.lastdate = current_timestamp(0);
end^
SET TERM ; ^
