--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODBIORCY_BI0 FOR ODBIORCY                       
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable cnt integer;
declare variable skrotkl shortname_id; --XXX ZG133796 MKD
begin
  if (new.nazwa is null or (new.nazwa = '')) then begin
     select fskrot from klienci where ref = new.klient into :skrotkl;
     new.nazwa =skrotkl;
  end
  if (new.company is null) then
    select k.company from klienci k where k.ref = new.klient into new.company;
  if(new.dulica is null) then new.dulica = '';
  if(new.dmiasto is null) then new.dmiasto = '';
  if(new.dpoczta is null) then new.dpoczta = '';
  if(new.dkodp is null) then new.dkodp = '';
  if(new.nazwafirmy is null) then new.nazwafirmy = '';
  if(new.nazwa is null) then new.nazwa = '';
  if(new.gl is null) then new.gl = 0;
  if(new.gl = 0) then begin
    select count(*) from ODBIORCY where KLIENT = new.klient and ref <> new.ref into :cnt;
    if(:cnt is null or (:cnt = 0)) then
      new.gl = 1;
  end
  if(new.odbiorcaspedyc > 0) then
    select NAZWAFIRMY||' '|| NAZWA from ODBIORCY where ref=new.odbiorcaspedyc into new.odbiorcaspedycdesc;
end^
SET TERM ; ^
