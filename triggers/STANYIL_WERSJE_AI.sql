--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_WERSJE_AI FOR STANYIL                        
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.ilosc > 0 or new.zamowiono > 0 or new.zarezerw > 0 or new.zablokow > 0) then
  begin
    update wersje set wersje.ilosc = wersje.ilosc + new.ilosc,
        wersje.zamowiono = wersje.zamowiono + new.zamowiono,
        wersje.zarezerw = wersje.zarezerw + new.zarezerw,
        wersje.zablokow = wersje.zablokow + new.zablokow
      where wersje.ref = new.wersjaref;
  end
end^
SET TERM ; ^
