--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_AU_DOCFROMOPERRAP FOR PROPERSRAPS                    
  ACTIVE AFTER UPDATE POSITION 2 
as
declare variable depto smallint;
begin
  if (coalesce(new.amount,0) <> coalesce(old.amount,0)) then
  begin
    if (exists (select first 1 1 from prschedopers p left join prshmat m on (p.shoper = m.opermaster)
        where p.ref = new.prschedoper and m.autodocout > 0)) then
    begin
      --generowanie dokumentow RW do raportow produkcyjnych
      execute procedure prschedguide_real(new.ref, null, 1, 1);
    end
    --generowanie dokumentow PW do raportow produkcyjnych
    if (exists (select  first 1 1
          from prschedopers p
           left join prschedguides g on (p.guide = g.ref)
           left join prsheets s on (s.ref = g.prsheet)
         where s.autodocin = 1 and p.ref = new.prschedoper)
    ) then
      execute procedure pr_proper_check_last_reported(new.prschedoper)
        returning_values depto;
      if (depto = 0) then
        execute procedure prschedguide_real(new.ref, null, 1, 0);
    -- przekazanie wyrobu gotowego jako surowiec na inny przewodnik
    if (exists(select first 1 1 from prschedguides where ref = new.prschedguide and prodtransfer = 1)) then
      execute procedure prod_transfer(new.ref,1);
  end
end^
SET TERM ; ^
