--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCPOS_BU0 FOR PRTOOLSDOCPOS                  
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (coalesce(new.prtool,'') = '') then
    new.prtool = new.prtoolstype;
  if (exists (select first 1 1 from prtoolsdocs d where d.ref = new.prtoolsdoc and d.akcept <> 0)) then
    exception prtoolsdocs_error 'Nie można modyfikować pozycji na zaakceptowanym dokumentcie';
end^
SET TERM ; ^
