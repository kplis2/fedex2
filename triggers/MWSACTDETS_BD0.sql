--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTDETS_BD0 FOR MWSACTDETS                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.status > 0) then
    exception MWSACTDET_DELETE_NOT_ALLOWED;
end^
SET TERM ; ^
