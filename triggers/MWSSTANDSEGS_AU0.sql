--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDSEGS_AU0 FOR MWSSTANDSEGS                   
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.l <> old.l) then
  begin
    if (new.mwsconstlocsq is null or new.mwsconstlocsq = 0) then
      exception MWSSTANDSEG_CONSTLOCSQ_NOT_SET;
    update whareas set l = new.l / new.mwsconstlocsq
      where mwsstandseg = new.ref;
  end
end^
SET TERM ; ^
