--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMPOZ_AU_KOMPLETY FOR DOKUMPOZ                       
  ACTIVE AFTER UPDATE POSITION 133 
as
declare variable docsetsautoadd smallint;
declare variable kplnagref integer;
declare variable skad smallint;
begin
--po zmianie wersji  sprawdzanie czy to nie naglowek kompletu
--jezeli tak dodanie pozycji jako fakow
if (  new.wersjaref <> old.wersjaref ) then begin
      execute procedure getconfig('DOCSETSAUTOADD')
        returning_values :docsetsautoadd;
      if (:docsetsautoadd is null) then docsetsautoadd = 0;
    
      select coalesce(zrodlo,0) from dokumnag where ref = new.dokument
      into :skad;
    if ( :skad = 0 and coalesce(new.fake,0) = 0 and new.frompozzam is null and
          new.orgdokumpoz is null and :docsetsautoadd > 0 and
          exists(select first 1 1 from towary where ktm = new.ktm and usluga <> 1 and coalesce(altposmode,0) > 0)) then
      begin
      select first 1 ref from kplnag
          where ktm = new.ktm and wersjaref = new.wersjaref and akt = 1 and glowna = 1
        into :kplnagref;
    
        if (:kplnagref is not null) then
        begin
    
          insert into dokumpoz(dokument, numer, ord, ktm, wersjaref, ilosc, alttopoz, fake,
              kortopoz,
              cenacen, cenabru, cenanet, cenawal,
              x_slownik)
            select new.dokument, new.numer, 1, p.ktm, p.wersjaref, p.ilosc * new.ilosc, new.ref, 1,
                case when new.kortopoz is null then null
                  else
                    (select dp1.ref
                      from dokumpoz dp
                        left join dokumpoz dp1 on (dp.ref = dp1.alttopoz)
                      where dp.ref = new.kortopoz
                        and dp1.wersjaref = p.wersjaref)
                end,
                0, 0, 0, 0,
               new.x_slownik
              from kplpoz p
              where p.nagkpl = :kplnagref;
        end
    end
end
 --przepisywanie na fake slowników (inwestycji)   
  if (coalesce(new.x_slownik,0) <> coalesce(old.x_slownik, 0)
        and coalesce(new.havefake,0) = 1) then begin
    update dokumpoz dp set dp.x_slownik = new.x_slownik, dp.int_id = new.int_id,
        dp.int_sesjaostprzetw = new.int_sesjaostprzetw,
        dp.int_dataostprzetw = new.int_dataostprzetw,
        dp.int_zrodlo = new.int_zrodlo
        where dp.alttopoz = new.ref;
  end
end^
SET TERM ; ^
