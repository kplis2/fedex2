--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BIBKBLOCK FOR NAGFAK                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.bkblock is null) then new.bkblock = 0;
  if (new.bkblockdescript is null) then new.bkblockdescript = '';
  if (new.dataotrz is null) then new.dataotrz = new.datasprz;
end^
SET TERM ; ^
