--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFWORKFLOWS_BI_REF FOR WFWORKFLOWS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('WFWORKFLOWS')
      returning_values new.REF;
end^
SET TERM ; ^
