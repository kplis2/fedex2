--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CAKCJENAG_BI_REF FOR CAKCJENAG                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CAKCJENAG')
      returning_values new.REF;
end^
SET TERM ; ^
