--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFSONDY_BI_REF FOR DEFSONDY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFSONDY')
      returning_values new.REF;
end^
SET TERM ; ^
