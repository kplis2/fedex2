--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDVERSIONS_BI FOR CDVERSIONS                     
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable okres varchar(6);
begin
  if (exists(select ref from cdversions where fromperiod = new.fromperiod and ref<> new.ref
             and costdistribution=new.costdistribution)) then
    exception universal 'Wersja rozdzielnika już istnieje dla danego okresu.';
end^
SET TERM ; ^
