--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_BU0 FOR STANYCEN                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable stminus integer;
declare variable typ_mag char(1);
begin
  if(new.ref is null) then new.ref = gen_id(GEN_STANYCEN,1);
  if(new.wersjaref is null) then new.wersjaref = 0;
  if(new.wersjaref <> old.wersjaref and new.wersjaref <> 0) then
    select nrwersji from WERSJE where ref=new.wersjaref into new.wersja;
  else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.ilosc is null) then new.ilosc = 0;
  select ODDZIAL,STANYCEN,TYP from DEFMAGAZ where SYMBOL = new.magazyn
    into new.oddzial, :stminus, :typ_mag;
  if(:stminus is null) then stminus = 0;
  if(:stminus=0 and new.ilosc<>old.ilosc and new.ilosc<0) then
    exception STCEN_MINUS;
  if(new.cena is null) then new.cena = 0;
  if(:typ_mag<>'S') then begin
    new.wartosc = new.ilosc * new.cena;
  end else if(new.ilosc<>0) then begin
    new.cena = new.wartosc/new.ilosc;
  end
  if((new.dostawa<>old.dostawa) or (new.data<>old.data)) then begin
    select datawazn from DOSTAWY where REF=new.dostawa into new.datawazn;
    if(new.datawazn is not null) then new.datafifo = new.datawazn;
    else new.datafifo = new.data;
  end
  if (new.data is null) then exception STANYCEN_BEZ_DATY;
  if(new.zarezerw is null) then new.zarezerw =0;
  if(new.zablokow is null) then new.zablokow =0;
  if(new.zamowiono is null) then new.zamowiono =0;
end^
SET TERM ; ^
