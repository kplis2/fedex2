--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFEVENTS_BI_GEN FOR WFEVENTS                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_wfevents,1);
end^
SET TERM ; ^
