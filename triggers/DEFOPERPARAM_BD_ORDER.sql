--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERPARAM_BD_ORDER FOR DEFOPERPARAM                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update DEFOPERPARAM set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer;
end^
SET TERM ; ^
