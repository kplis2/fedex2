--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_BIUD_STATUS FOR EPRPOS                         
  ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 0 
AS
  declare variable status smallint;
  declare variable payroll integer;
  declare variable employee integer;
  declare variable newbtransfer integer;
begin
/*MWr: Nie mozna modyfikowac pozycji jezeli nalezy ona do listy zamknietej lub
 rozliczenia zaakceptowanego. BTRANSFER uzupelnia sie przy generowaniu przelewow*/

  if (deleting) then
  begin
    payroll = old.payroll;
    employee = old.employee;
    newbtransfer = coalesce(old.btransfer,0);
  end else
  begin
    payroll = new.payroll;
    employee = new.employee;
    newbtransfer = coalesce(new.btransfer,0);
  end

  if (newbtransfer = coalesce(old.btransfer,0)) then
  begin
    select status from epayrolls
      where ref = :payroll
      into :status;

    if (status > 0) then
      exception payroll_is_closed;

    select status from eprempl
      where epayroll = :payroll
        and employee = :employee
      into :status;

    if (status > 0) then
      exception eprempl_accepted;
  end
end^
SET TERM ; ^
