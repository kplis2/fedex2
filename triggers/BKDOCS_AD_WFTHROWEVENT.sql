--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AD_WFTHROWEVENT FOR BKDOCS                         
  ACTIVE AFTER DELETE POSITION 10 
AS
begin
  execute procedure wf_throwevent('BKDOC_DEL','SENTE.BKDOC',old.ref,
   coalesce(old.symbol, current_timestamp(0)),1);
end^
SET TERM ; ^
