--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCEBASES_AIU_EABSENCES FOR EABSENCEBASES                  
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
begin
--Aktualizacja stawki dziennej na tabeli nieobecnosci
  if (new.sbase <> old.sbase or (new.sbase is null and old.sbase is not null) or (new.sbase is not null and old.sbase is null) or
      new.vbase <> old.vbase or (new.vbase is null and old.vbase is not null) or (new.vbase is not null and old.vbase is null) or
      new.vworkeddays <> old.vworkeddays or (new.vworkeddays is null and old.vworkeddays is not null) or (new.vworkeddays is not null and old.vworkeddays is null) or
      new.sworkeddays <> old.sworkeddays or (new.sworkeddays is null and old.sworkeddays is not null) or (new.sworkeddays is not null and old.sworkeddays is null) or
      new.takeit <> old.takeit or (new.takeit is null and old.takeit is not null) or (new.takeit is not null and old.takeit is null) or
      new.period <> old.period or (new.period is null and old.period is not null) or (new.period is not null and old.period is null) or
      new.workdays <> old.workdays or (new.workdays is null and old.workdays is not null) or (new.workdays is not null and old.workdays is null) or
      new.vworkedhours <> old.vworkedhours or (new.vworkedhours is null and old.vworkedhours is not null) or (new.vworkedhours is not null and old.vworkedhours is null)
  ) then
    update eabsences
      set monthpaybase = null,
          hourpayamount = null    --wynulowanie wartosci wymusza ponowne rozliczenie nieobecnosci
      where baseabsence = new.eabsence
      order by fromdate;  --BS46198
end^
SET TERM ; ^
