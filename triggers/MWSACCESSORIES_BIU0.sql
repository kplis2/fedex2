--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACCESSORIES_BIU0 FOR MWSACCESSORIES                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (new.act is null) then new.act = 1;
  if (new.capacity is null) then new.capacity = 0;
  if (new.restcapacity is null) then new.restcapacity = 0;
  if (new.lifthight is null) then new.lifthight = 0;
  if (new.liftupspeed is null) then new.liftupspeed = 0;
  if (new.liftupspeedwithload is null) then new.liftupspeedwithload = 0;
  if (new.liftdownspeed is null) then new.liftdownspeed = 0;
  if (new.liftdownspeedwithload is null) then new.liftdownspeedwithload = 0;
  if (new.ridespeed is null) then new.ridespeed = 0;
  if (new.ridespeedwithload is null) then new.ridespeedwithload = 0;
  if (new.acceleration is null) then new.acceleration = 0;
  if (new.distance is null) then new.distance = 0;
end^
SET TERM ; ^
