--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTAP_AD0 FOR INWENTAP                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable inwentaparent integer;
declare variable cnt integer;
declare variable status integer;
declare variable sumilinw numeric(14,4);
begin

  if(old.blokadanalicz = 0) then execute procedure INWENTA_OBLNAG(old.inwenta);
  select inwenta.inwentaparent from INWENTA where ref=old.inwenta into :inwentaparent;
  if(:inwentaparent > 0 and old.pozmaster > 0) then
    execute procedure INWENTAP_OBLICZMASTER(:inwentaparent, old.wersjaref, old.cena, old.dostawa, old.ilinw);
  if(old.pozmaster > 0) then begin
    select count(*) from INWENTAP where POZMASTER = old.pozmaster into :cnt;
    if(:cnt is null or (cnt = 0)) then
      delete from INWENTAP where REF=old.pozmaster;
  end

  /* Obliczenie sumy pozycji dla inwentury partiami i wersjami */
  select status
    from inwentap_set_status(old.ref, old.wersjaref, old.inwenta, old.dostawa, old.ilstan, 0)
    into :status;

end^
SET TERM ; ^
