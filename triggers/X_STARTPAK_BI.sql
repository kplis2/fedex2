--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_STARTPAK_BI FOR X_STARTPAK                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  -- [DG] XXX ZG97597
  -- !!!
  -- Nie ruszać tego triggera - jest wykorzystywany w procedurze X_STARTPAK_INSERT
  -- (a ta w LISTYWYSD_GETDOC). W LISTYWYSD_GETDOC są dodawane i usuwane wpisy do
  -- tej tabeli na początku/końcu wywolywania procedury - exception jest zabezpieczeniem
  -- przed odpaleniem procedury przez wiecej niz 1 operatora na raz. Exception jest
  -- przechwytywany i obslugiwany w X_STARTPAK_INSERT (jako zwracany status)
  if (exists(select first 1 1 from x_startpak)) then
    exception universal 'W tabeli X_STARTPAK moze byc tylko 1 wpis!';
end^
SET TERM ; ^
