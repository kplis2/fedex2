--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_AU_DISTS FOR DECREES                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable distposcount integer;
declare variable vstatement varchar(1024);
declare variable vrefdistpos integer;

begin
  /*Sprawdzam ile wpisow w distpos*/
  select count(d.ref) from distpos d where d.decrees = new.ref into :distposcount;
  if(:distposcount is null) then distposcount = 0;
  /*Zmiana konta lub zmiana wartosci wyroznikow*/
  if (new.account <> old.account or
      (coalesce(old.dist1symbol, '') <> coalesce(new.dist1symbol, '') or
      coalesce(old.dist2symbol, '') <> coalesce(new.dist2symbol, '') or
      coalesce(old.dist3symbol, '') <> coalesce(new.dist3symbol, '') or
      coalesce(old.dist4symbol, '') <> coalesce(new.dist4symbol, '') or
      coalesce(old.dist5symbol, '') <> coalesce(new.dist5symbol, '') or
      coalesce(old.dist6symbol, '') <> coalesce(new.dist6symbol, '')))
  then begin
    -- usun poprzednie wyrozniki do tego dekretu
    delete from distpos where decrees = new.ref;
    /*Wyrozniki jednowartosciowe - jesli nowe konto posiada wyrozniki to dodajemy*/
    if((new.dist1ddef is not null and coalesce(new.dist1symbol, '') <> '')
       or (new.dist2ddef is not null and coalesce(new.dist2symbol, '') <> '')
       or (new.dist3ddef is not null and coalesce(new.dist3symbol, '') <> '')
       or (new.dist4ddef is not null and coalesce(new.dist4symbol, '') <> '')
       or (new.dist5ddef is not null and coalesce(new.dist5symbol, '') <> '')
       or (new.dist6ddef is not null and coalesce(new.dist6symbol, '') <> '')) then
         insert into distpos (number, tempdictdef, tempsymbol, tempdictdef2, tempsymbol2,tempdictdef3, tempsymbol3,
                          tempdictdef4, tempsymbol4, tempdictdef5, tempsymbol5, tempdictdef6, tempsymbol6,
                          decrees, bkdoc, period, account, debit, credit, transdate, company, status)
           values (1, new.dist1ddef, new.dist1symbol,new.dist2ddef, new.dist2symbol,new.dist3ddef, new.dist3symbol,
               new.dist4ddef, new.dist4symbol, new.dist5ddef, new.dist5symbol, new.dist6ddef, new.dist6symbol,
               new.ref, new.bkdoc, new.period, new.account, new.debit, new.credit, new.transdate, new.company, new.status);
  end
  /*Zmienił się okres lub data na dekrecie - aktualizacja wszystkich wpisow w distpos*/
  if(new.period<>old.period or new.transdate<>old.transdate) then begin
    update distpos d set d.period = new.period, d.transdate = new.transdate where d.decrees = new.ref;
  end
  /*zmieniła sie kwota na dekrecie lub strona*/
  if((new.debit<>old.debit or new.credit<>old.credit)
    and coalesce(new.blokobldists,0)=0 and :distposcount = 1)
  then begin
    /*Zmieniła się kwota na dekrecie, a jest jeden wpis w distpos - aktualizacja wpisu w distpos*/
      update distpos d set d.debit = new.debit, d.credit = new.credit where d.decrees = new.ref;
  end

  if ( new.side <> old.side) then begin
    /*BS46501 - w ten sposob, bo zamiana wartosci dwoch pol w jednym rekordzie,
       bez trzeciego pola posredniego, nie zadziala - trzeba wstawic po prostu wartosci*/
    for
      select  'update distpos d set d.debit =' ||d.credit|| ', d.credit ='|| d.debit || 'where d.decrees =' || new.ref, d.ref
        from distpos d
        where d.decrees = new.ref
        into :vstatement, :vrefdistpos
      do begin
        vstatement = vstatement ||'and ref = '||vrefdistpos;
        execute statement vstatement;
        vstatement = null;
      end
  end

  if(new.blokobldists=1) then update decrees set blokobldists=0 where ref=new.ref;
end^
SET TERM ; ^
