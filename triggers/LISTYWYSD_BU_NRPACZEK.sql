--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_BU_NRPACZEK FOR LISTYWYSD                      
  ACTIVE BEFORE UPDATE POSITION 6 
as
declare variable spedycja smallint;
declare variable rodzajwys smallint;
declare variable spedytor varchar(40);
declare variable typnumerow smallint;
declare variable wysylkapal smallint;
declare variable nrref integer;
declare variable nrakt bigint;
declare variable nrkoniec bigint;
declare variable nrpierwszego integer;
declare variable nropk integer;
declare variable lworef integer;
declare variable cnt integer;
begin
  if (coalesce(old.akcept, 0) < 2 and new.akcept = 2) then begin
    select s.listywys, s.rodzajwys from sposdost s where s.ref = new.sposdost
    into :spedycja, :rodzajwys;

    if (coalesce(:spedycja,0) > 0 and coalesce(:rodzajwys,0) > 0) then begin
      select count(spedytor) from spedytwys where sposdost = new.sposdost
      into :cnt;
      if (:cnt > 1) then
        exception LISTYWYSD_NRPACZEK 'Sposób dostawy przypisany dla wielu spedytorów.';

      select spedytor from spedytwys where sposdost = new.sposdost
      into :spedytor;

      select typnumerow from spedytorzy where symbol = :spedytor into :typnumerow;
      --sprawdzenie czy wyslka paletowa
      if (exists(select first 1 1 from listywysdroz_opk where listwysd = new.ref and typ = 1)) then
        wysylkapal = 1;
      else wysylkapal = 0;

      nrpierwszego = 0;
      --nadanie numerow paczek i dokumentu spedycyjnego
      if (:typnumerow in (1,2)) then begin
        select ref, nrakt, nrkoniec from numerysped
          where spedytor = :spedytor
            and aktywny = 1
            and ((:wysylkapal = 0 and typ in (0,2)) or
                 (:wysylkapal = 1 and typ in (1,2)))
        into :nrref, :nrakt, :nrkoniec;

        if (coalesce(:nrref, 0) > 0 and
            coalesce(:nrakt,0) = coalesce(:nrkoniec,0)) then
          exception LISTYWYSD_NRPACZEK 'Brak numerów w puli numerów paczek dla spedytora '||:spedytor;
        if (coalesce(:nrref, 0) > 0) then begin
          for
            select ref from listywysdroz_opk
              where listwysd = new.ref
                and rodzic is null
                and stan = 1
            into :lworef
          do begin
            nrakt = :nrakt + 1;
            if (:nrpierwszego = 0) then nrpierwszego = :nrakt;
            update listywysdroz_opk set nrpaczki = :nrakt where ref = :lworef;
          end
          update numerysped set nrakt = :nrakt where ref = :nrref;
        end
        else exception LISTYWYSD_NRPACZEK 'Nie znaleziono numeratora paczek dla spedytora '||:spedytor;

        nrref = 0;
        nrakt = 0;
        nrkoniec = 0;
        select ref, nrakt, nrkoniec from numerysped
          where spedytor = :spedytor
            and aktywny = 1
            and typ = 5
        into :nrref, :nrakt, :nrkoniec;

        if (coalesce(:nrref, 0) > 0 and
            coalesce(:nrakt,0) = coalesce(:nrkoniec,0)) then
          exception LISTYWYSD_NRPACZEK 'Brak numerów w puli numerów dokumentów spedycyjnych dla spedytora'||:spedytor;
        if (coalesce(:nrref, 0) = 0) then begin
          new.symbolsped = :nrpierwszego;
        end
        else begin
          nrakt = :nrakt + 1;
          new.symbolsped = :nrakt;
          update numerysped set nrakt = :nrakt where ref = :nrref;
        end
      end
      else if (:typnumerow = 3) then begin
        nrref = 0;
        nrakt = 0;
        nrkoniec = 0;
        nropk = 0;
        select ref, nrakt, nrkoniec from numerysped
          where spedytor = :spedytor
            and aktywny = 1
            and ((:wysylkapal = 0 and typ in (3,5)) or
                 (:wysylkapal = 1 and typ in (4,5)))
        into :nrref, :nrakt, :nrkoniec;
        if (coalesce(:nrref, 0) > 0 and
            coalesce(:nrakt,0) = coalesce(:nrkoniec,0)) then
          exception LISTYWYSD_NRPACZEK 'Brak numerów w puli numerów listów dla spedytora '||:spedytor;
        if (coalesce(:nrref, 0) > 0) then begin
          nrakt = :nrakt + 1;
          for
            select ref from listywysdroz_opk
              where listwysd = new.ref
                and rodzic is null
                and stan = 1
              order by ref
            into :lworef
          do begin
            nropk = :nropk + 1;
            update listywysdroz_opk set nrpaczki = :nrakt, nropk = :nropk  where ref = :lworef;
          end
          new.symbolsped = :nrakt;
          update numerysped set nrakt = :nrakt where ref = :nrref;
        end
        else exception LISTYWYSD_NRPACZEK 'Nie znaleziono numeratora listów dla spedytora '||:spedytor;
      end
    end
  end
end^
SET TERM ; ^
