--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AI_CPL FOR CPODMIOTY                      
  ACTIVE AFTER INSERT POSITION 2 
as
declare variable cpl integer;
declare variable trybred integer;
declare variable nrkarty varchar(40);
declare variable refref integer;
begin
  /* automatyczne dodanie uczestnika programu lojalnociowego*/
  if(new.aktywny =1 ) then begin
    select TRYBRED from SLODEF where REF = new.slodef into :trybred;
    if(:trybred=1) then begin
      nrkarty = new.kodzewn;
      refref = new.kodzewn;
    end else begin
      nrkarty = NULL;
      refref = NULL;
    end
    for select REF from CPL where SLODEF = new.slodef and STATUS = 1 and AUTOUCZ = 1
    into :cpl
    do begin
      insert into CPLUCZEST(REF, CPL, CPODMIOT, PKOSOBA, DATAOTW, AKT, ILPKT, DATALAST, NRKARTY, CSTATUS)
        values(:refref, :cpl, new.ref, NULL,current_date, 1, 0, current_date, :nrkarty, new.cstatus);
      refref = NULL;
    end
  end
end^
SET TERM ; ^
