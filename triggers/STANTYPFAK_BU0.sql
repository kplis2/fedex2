--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANTYPFAK_BU0 FOR STANTYPFAK                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable zak smallint;
begin
  if(new.stansprzed is not null) then new.stanfaktyczny = new.stansprzed;
  else new.stanfaktyczny = '';
  select KOREKTA, SAD from TYPFAK where SYMBOL = new.typfak into new.korekta, new.sad;
  select ZAKUPU from STANSPRZED where  STANOWISKO = new.stansprzed into new.zakupu;
  if(:zak <> new.zakupu) then
    exception STANTYPDAFAK_ZAKNIESG;
  if(new.akcjaackafter is null) then  new.akcjaackafter = '';
end^
SET TERM ; ^
