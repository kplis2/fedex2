--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DIMELEMDEF_BIU FOR DIMELEMDEF                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable isslodef smallint;
declare variable slodef integer;
declare variable sloname varchar(20);
begin
  if (new.act is null) then new.act = 0;
  if (new.dontdelete is null) then new.dontdelete = 1;
  select dimhierdef.slodef, dimhierdef.isslodef, slodef.nazwa
    from dimhierdef
      left join slodef on (slodef.ref = dimhierdef.slodef)
    where dimhierdef.ref = new.dimhier
    into :slodef,  :isslodef, :sloname;
  if (:sloname is null) then sloname = '';
  if (:isslodef = 1 and (new.slodef is null or new.slodef <> :slodef)) then
    exception DIMELEM_SLODEF_NOT_CONNECTED 'Wymiar związany ze słownikiem '||:sloname;
  if (new.dim is null) then
    select dimhierdef.dim from dimhierdef where dimhierdef.ref = new.dimhier
      into new.dim;
end^
SET TERM ; ^
