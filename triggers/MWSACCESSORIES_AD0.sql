--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACCESSORIES_AD0 FOR MWSACCESSORIES                 
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  -- usuniecie lokacji stalej dla akcesorium magazynowego - najczesciej dla wozkow
  delete from mwsconstlocs where ref = old.mwsconstloc;
end^
SET TERM ; ^
