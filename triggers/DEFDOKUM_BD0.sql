--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDOKUM_BD0 FOR DEFDOKUM                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
begin
  if(old.symbol <> '') then begin
    select count(*) from  DOKUMNAG where TYP=old.symbol into :cnt;
    if(:cnt > 0) then exception DEFDOKUM_DELETE_NOT_ALLOW;
  end
end^
SET TERM ; ^
