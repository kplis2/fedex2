--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EQUESTHEADER_BI_BLANK FOR EQUESTHEADER                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  new.regdate = current_timestamp;
  if (new.ereccand is null) then
    select ref from ereccands where recruit = new.erecruit and candidate = new.ecandidate
      into new.ereccand;
end^
SET TERM ; ^
