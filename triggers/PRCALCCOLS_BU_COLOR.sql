--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCCOLS_BU_COLOR FOR PRCALCCOLS                     
  ACTIVE BEFORE UPDATE POSITION 2 
as
begin
  if(new.excluded<>old.excluded or new.calctype<>old.calctype)  then begin
    if(new.parent is null and new.calctype=0) then new.color = 3;
    else if(new.excluded=1) then new.color = 2;
    else new.color = new.calctype;
  end
end^
SET TERM ; ^
