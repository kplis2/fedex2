--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDIMHIER_BI0 FOR FRDIMHIER                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.istime is null) then
    select dimdef.istime
      from dimhierdef
      join dimdef on (dimdef.ref = dimhierdef.dim)
      where dimhierdef.ref = new.dimhierdef
      into new.istime;
end^
SET TERM ; ^
