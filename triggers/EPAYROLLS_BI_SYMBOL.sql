--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_BI_SYMBOL FOR EPAYROLLS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable numberg varchar(255);
begin
--MWr: Przypisanie symbolu na podstawie numeratora

  if (new.prtype <> 'UCP') then
    execute procedure GETCONFIG('EPAYROLLSNUM') returning_values :numberg;
  else
    execute procedure GETCONFIG('EPRBILLSNUM') returning_values :numberg;

  --spr. czy numerator przypisany do list plac / rachunku UCP:
  if (coalesce(numberg,'') <> '') then
    execute procedure Get_NUMBER(:numberg, new.company, new.prtype, substring(new.cper from 1 for 4) || '/' || substring(new.cper from 5 for 6) || '/1', 0, null)
      returning_values new.number, new.symbol;
  else
    new.symbol = new.prtype || '/' || new.cper || '/' || new.number;

  if (exists(select first 1 1 from epayrolls where symbol = new.symbol and company = new.company)) then
  begin
    if (new.prtype <> 'UCP') then
      exception EPAYROLLS_SYMBOL;
    else
      exception EPAYROLLS_SYMBOL_UCP;
  end
end^
SET TERM ; ^
