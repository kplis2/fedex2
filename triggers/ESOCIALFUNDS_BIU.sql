--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ESOCIALFUNDS_BIU FOR ESOCIALFUNDS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
begin

  if (coalesce(new.amount,0.00) = 0.00) then
    exception esocfunds_amount;

  if (coalesce(new.fundtype,0) = 0) then
    exception esocfunds_fundtype;

  if (new.funddate is null) then
    exception esocfunds_funddate;

  if (coalesce(new.employee,0) = 0) then
    exception esocfunds_employee;


end^
SET TERM ; ^
