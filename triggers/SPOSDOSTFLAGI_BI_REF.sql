--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOSTFLAGI_BI_REF FOR SPOSDOSTFLAGI                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SPOSDOSTFLAGI')
      returning_values new.REF;
end^
SET TERM ; ^
