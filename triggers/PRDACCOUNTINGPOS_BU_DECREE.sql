--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTINGPOS_BU_DECREE FOR PRDACCOUNTINGPOS               
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable bkperiod varchar(6);
declare variable bkdocsymbol varchar(20);
declare variable prdversperiod varchar(6);
declare variable prdaccounting integer;
begin
 if(new.DIVRATE <> old.DIVRATE or new.PRDVERSION <> old.PRDVERSION  or
    new.FIRSTDEDUCTAMOUNT <> old.FIRSTDEDUCTAMOUNT or new.DEDUCTAMOUNT <> old.DEDUCTAMOUNT or
    new.DIST6DDEF <> old.DIST6DDEF or new.DECREE <> old.DECREE or
    new.DIST5DDEF <> old.DIST5DDEF or new.DIST4DDEF <> old.DIST4DDEF or
    new.DIST3DDEF <> old.DIST3DDEF or new.DIST2DDEF <> old.DIST2DDEF or
    new.DIST1DDEF <> old.DIST1DDEF or new.PRDACCOUNTING <> old.PRDACCOUNTING or
    new.DIST6SYMBOL <> old.DIST6SYMBOL or new.DIST5SYMBOL <> old.DIST5SYMBOL or
    new.DIST4SYMBOL <> old.DIST4SYMBOL or new.DIST3SYMBOL <> old.DIST3SYMBOL or
    new.DIST2SYMBOL <> old.DIST2SYMBOL or new.DIST1SYMBOL <> old.DIST1SYMBOL or
    new.DEDUCTACCOUNT <> old.DEDUCTACCOUNT or new.DESCRIPT <> old.DESCRIPT)
  then begin
    select p.fromperiod, p.prdaccounting from prdversions p where p.ref = new.prdversion into :prdversperiod, :prdaccounting;
    select first 1 b.symbol, b.period from decrees d join bkdocs b on (b.ref = d.bkdoc)
      where d.period>=:prdversperiod and d.prdaccounting = :prdaccounting and d.ref <> new.decree
       into :bkdocsymbol, :bkperiod;
    if(coalesce(:bkdocsymbol,'') <> '') then
      exception prd_deducted 'Istnieje dokument RMK ' || :bkdocsymbol || '(' || :bkperiod || '). Nie można poprawić!';
  end
end^
SET TERM ; ^
