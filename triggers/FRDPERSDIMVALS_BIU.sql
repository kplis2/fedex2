--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMVALS_BIU FOR FRDPERSDIMVALS                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable status smallint;
declare variable lastvar integer;
begin
  if (new.planformula is not null and new.planformula <> ''
      and new.planeditable = 0) then
    new.planeditable = 1;
  if ((new.planformula is null or new.planformula = '')
      and new.planeditable = 1) then
    new.planeditable = 0;
  if (new.planeditable is null) then new.planeditable = 0;
  select frhdrs.status
    from frdpersdimhier
      join frdimhier on (frdpersdimhier.frdimshier = frdimhier.ref)
      join frhdrs on (frhdrs.symbol = frdimhier.frhdr)
    where frdpersdimhier.ref = new.frdpersdimshier
    into :status;
  if ((new.planformula is null or new.planformula = '') and old.planformula is not null and old.planformula <> '') then
    new.planformula = '';
  if ((new.realformula is null or new.realformula = '') and old.realformula is not null and old.realformula <> '') then
    new.realformula = '';
  if ((new.val1formula is null or new.val1formula = '') and old.val1formula is not null and old.val1formula <> '') then
    new.val1formula = '';
  if ((new.val2formula is null or new.val2formula = '') and old.val2formula is not null and old.val2formula <> '') then
    new.val2formula = '';
  if ((new.val3formula is null or new.val3formula = '') and old.val3formula is not null and old.val3formula <> '') then
    new.val2formula = '';
  if ((new.val4formula is null or new.val4formula = '') and old.val4formula is not null and old.val4formula <> '') then
    new.val4formula = '';
  if ((new.val5formula is null or new.val5formula = '') and old.val5formula is not null and old.val5formula <> '') then
    new.val5formula = '';
  execute procedure STATEMENT_MULTIPARSE(new.planformula, 'FRDSCR_', '', '', 1, 1)
    returning_values new.decl, new.body, :lastvar;
  execute procedure STATEMENT_MULTIPARSE(new.realformula, 'FRDSCR_', '', '', 1, 1)
    returning_values new.declreal, new.bodyreal, :lastvar;
  execute procedure STATEMENT_MULTIPARSE(new.val1formula, 'FRDSCR_', '', '', 1, 1)
    returning_values new.declval1, new.bodyval1, :lastvar;
  execute procedure STATEMENT_MULTIPARSE(new.val2formula, 'FRDSCR_', '', '', 1, 1)
    returning_values new.declval2, new.bodyval2, :lastvar;
  execute procedure STATEMENT_MULTIPARSE(new.val3formula, 'FRDSCR_', '', '', 1, 1)
    returning_values new.declval3, new.bodyval3, :lastvar;
  execute procedure STATEMENT_MULTIPARSE(new.val4formula, 'FRDSCR_', '', '', 1, 1)
    returning_values new.declval4, new.bodyval4, :lastvar;
  execute procedure STATEMENT_MULTIPARSE(new.val5formula, 'FRDSCR_', '', '', 1, 1)
    returning_values new.declval5, new.bodyval5, :lastvar;
/*  if (:status <> 0) then
    exception FRHDRS_NOT_ACCESIBLE;*/
end^
SET TERM ; ^
