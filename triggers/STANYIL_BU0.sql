--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_BU0 FOR STANYIL                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.iloscpot is null) then new.iloscpot = 0;
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm, akt, nazwa from WERSJE where ref=new.wersjaref into new.wersja, new.ktm, new.akt, new.wnazwa;
    select NAZWA, MIARA, CHODLIWY, grupa, usluga, altposmode from TOWARY where KTM = new.ktm
    into new.nazwat, new.miara, new.chodliwy, new.grupa, new.usluga, new.altposmode;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then begin
    select ref, akt, nazwa from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref, new.akt, new.wnazwa;
    select NAZWA, MIARA, CHODLIWY, grupa, usluga, altposmode from TOWARY where KTM = new.ktm
    into new.nazwat, new.miara, new.chodliwy, new.grupa, new.usluga, new.altposmode;
  end
  if(new.zarezerw is null) then new.zarezerw =0;
  if(new.zablokow is null) then new.zablokow =0;
  select ODDZIAL from DEFMAGAZ where SYMBOL = new.magazyn into new.oddzial;
  if((new.ilosc <> old.ilosc)
     or (new.stanmax <> old.stanmax) or (new.stanmax is not null and old.stanmax is null) or (new.stanmax is null and old.stanmax is not null)
     or (new.stanmin <> old.stanmin) or (new.stanmin is not null and old.stanmin is null) or (new.stanmin is null and old.stanmin is not null)
  ) then begin
    /*okrelenie stosunku dla barier min-max*/
    new.minmaxstan = 0;
    if(new.ilosc < new.stanmin) then new.minmaxstan = 1;
    else if(new.ilosc > new.stanmax) then new.minmaxstan = 3;
    else if((new.stanmax is not null) or (new.stanmin is not null)) then new.minmaxstan = 2;
    if(coalesce(new.stanmin,0)<>coalesce(old.stanmin,0) or coalesce(new.stanmax,0)<>coalesce(old.stanmax,0)) then begin
      update or insert into STANYMINMAX(MAGAZYN,WERSJAREF,DATA,STANMIN,STANMAX)
      values(new.magazyn, new.wersjaref, current_date, new.stanmin, new.stanmax);
    end
    execute procedure GET_STANYIL_KOLOR(new.ilosc,new.stanmin,new.stanmax,new.kolor,new.minmaxstan,new.ktm) returning_values new.kolor;
  end
  if(new.iloscpod is null) then new.iloscpod = 0;
  if((new.ilosc <> old.ilosc) or (new.zarezerw<>old.zarezerw) or (new.zablokow<>old.zablokow)) then begin
    new.iloscwolna = new.ilosc-new.zarezerw-new.zablokow;
  end
end^
SET TERM ; ^
