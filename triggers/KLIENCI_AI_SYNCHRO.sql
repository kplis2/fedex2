--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AI_SYNCHRO FOR KLIENCI                        
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable synchro smallint;
declare variable refout integer;
declare variable i integer;
begin
  select konfig.wartosc from konfig where konfig.akronim = 'SYNCHROKLIENT'
    into :synchro;
  if (synchro = 1 ) then
  begin
  if (new.synchro is null) then
   begin
    if (new.dostawca is null) then
    begin
      execute procedure GEN_REF('DOSTAWCY') returning_values :refout;
      update klienci set klienci.synchro = :refout
        where klienci.ref = new.ref;
      ---------
      insert into DOSTAWCY(REF, NAZWA, ID, KONTOFK,MIASTO, KODP,
          ULICA, NRDOMU, NRLOKALU, TELEFON, FAX, EMAIL, WWW,
          AKTYWNY, POCZTA, KRAJ, NIP, REGON, FIRMA,  TYP,
          KODZEWN, WALUTA,BANK, RACHUNEK,SYNCHRO,COMPANY,POWIAZANY, cpowiat)
        values (:refout, new.NAZWA, new.FSKROT, new.KONTOFK, new.MIASTO, new.KODP,
          new.ULICA, new.NRDOMU, new.NRLOKALU, new.TELEFON,new.FAX, new.EMAIL, new.WWW,
          new.AKTYWNY, new.POCZTA, new.KRAJ, new.NIP, new.REGON, new.FIRMA,  new.TYP,
          new.KODZEWN, new.WALUTA,new.bank, new.rachunek,1,new.company,new.powiazany, new.cpowiat);
      --new.dostawca = :refout;
      update klienci set klienci.dostawca = :refout, klienci.synchro = null
        where klienci.ref = new.ref;
    end
   end
  end
end^
SET TERM ; ^
