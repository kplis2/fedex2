--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VAT_BU_CHECKACCOUNTING FOR VAT                            
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable dictdef integer;
begin
  if (new.grupa <> old.grupa) then
  begin
    select ref from slodef where typ = 'VAT'
      into :dictdef;
    execute procedure check_dict_using_in_accounts(dictdef, old.grupa, null);
  end
end^
SET TERM ; ^
