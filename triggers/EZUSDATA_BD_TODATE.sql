--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EZUSDATA_BD_TODATE FOR EZUSDATA                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable todate date;
begin
/*MWr: Ustalenie czasu trwania przy usunieciu rekordu. Mozna by wykorzystac
      EZUSDATA_AIU_TODATE ale nie ma potrzeby 'odbudowywania' tabeli na nowo*/

  select first 1 fromdate - 1 from ezusdata
    where person = old.person
      and fromdate > old.fromdate
    order by fromdate
    into :todate;

  update ezusdata
     set todate = :todate
     where ref in (select first 1 ref from ezusdata
                    where person = old.person
                      and fromdate < old.fromdate
                    order by fromdate desc);
end^
SET TERM ; ^
