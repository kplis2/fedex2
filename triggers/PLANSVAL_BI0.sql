--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLANSVAL_BI0 FOR PLANSVAL                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.planval is null) then new.planval = -1;
  if (new.dyspval is null) then new.dyspval = 0;
  if (new.realval is null) then new.realval = 0;
end^
SET TERM ; ^
