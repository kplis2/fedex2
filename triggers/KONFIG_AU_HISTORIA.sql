--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIG_AU_HISTORIA FOR KONFIG                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable fromdate varchar(20);
declare variable currentbranch varchar(255);
begin
  if (new.historia <> old.historia and new.historia = 0) then
  begin
    for
      select k.oddzial, max(k.fromdate)
        from konfigvals k
          where k.akronim = new.akronim
        group by k.oddzial
      into :currentbranch, :fromdate
    do begin
      delete from konfigvals k
        where k.akronim = new.akronim and k.oddzial = :currentbranch
          and k.fromdate < :fromdate;
    end
  end
end^
SET TERM ; ^
