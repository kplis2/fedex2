--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OLAPDIMENSIONS_BI_BLANK FOR OLAPDIMENSIONS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.rights is null) then new.rights = '';
  if (new.rightsgroup is null) then new.rightsgroup = '';
end^
SET TERM ; ^
