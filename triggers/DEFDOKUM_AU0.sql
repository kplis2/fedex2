--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDOKUM_AU0 FOR DEFDOKUM                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.symbol <> old.symbol) then begin
    update DEFDOKUM set PRZECIW = new.symbol where PRZECIW = old.symbol;
    update DEFDOKUM set KORYGUJACY = new.symbol where KORYGUJACY = old.symbol;
  end
end^
SET TERM ; ^
