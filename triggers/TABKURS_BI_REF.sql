--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TABKURS_BI_REF FOR TABKURS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('TABKURS')
      returning_values new.REF;
end^
SET TERM ; ^
