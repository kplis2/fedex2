--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOST_BU_REPLICAT FOR SPOSDOST                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if(new.listywys = 0) then new.trasy = 0;
  if(new.usluga = '') then new.usluga = null;
  if(
    ((new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null) or
    new.koszt <> old.koszt or (new.koszt is null and old.koszt is not null) or (new.koszt is not null and old.koszt is null) or
    new.prog <> old.prog or (new.prog is null and old.prog is not null) or (new.prog is not null and old.prog is null) or
    new.opis <> old.opis or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null) or
    new.czas <> old.czas or (new.czas is null and old.czas is not null) or (new.czas is not null and old.czas is null)
    )and new.witryna = 1)  or (new.witryna <> old.witryna)
  )then
   waschange = 1;

  execute procedure rp_trigger_bu('SPOSDOST',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
