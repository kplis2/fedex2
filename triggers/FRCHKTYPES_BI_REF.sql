--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRCHKTYPES_BI_REF FOR FRCHKTYPES                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRCHKTYPES')
      returning_values new.REF;
end^
SET TERM ; ^
