--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_BI_REF FOR MWSORDS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSORDS') returning_values new.ref;
end^
SET TERM ; ^
