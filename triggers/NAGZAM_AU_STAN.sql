--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_AU_STAN FOR NAGZAM                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable status integer;
declare variable oldilosc numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable termdost timestamp;
declare variable org_ref integer;
declare variable typ integer;
declare variable pozref integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable dostawa integer;
declare variable cenamag numeric(14,4);
declare variable stan char(1);
declare variable wydania integer;
begin
  select typzam.wydania from typzam where symbol = new.typzam into :wydania;
  if(new.stan <> old.stan and :wydania <> 3)then begin
      update POZZAM set STAN = new.stan where (STAN <> new.stan or STAN is null)
        and (STAN = old.stan or STAN is null or new.stan = 'N') and ZAMOWIENIE = new.ref;
  end
  if(new.stan <> old.stan and :wydania = 3) then begin
      update POZZAM set STAN = new.stan where (STAN <> new.stan or STAN is null)
        and (STAN = old.stan or STAN is null) and ZAMOWIENIE = new.ref and pozzam.out = 1;
  end
  if(new.kstan <> old.kstan and :wydania = 3) then begin
      update POZZAM set STAN = new.kstan where (STAN <> new.kstan or STAN is null)
        and (STAN = old.kstan or STAN is null) and ZAMOWIENIE = new.ref and pozzam.out = 0;
  end
  if(new.termdost <> old.termdost and :wydania <> 3) then begin
      update STANYREZ set DATA = new.termdost where ZAMOWIENIE=new.ref;
  end
  if(:wydania = 3 and new.termdost <> old.termdost and new.onmainschedule = 0) then begin
      update STANYREZ set DATA = new.termdost where ZAMOWIENIE=new.ref and POZZAM = 0;
      for select REF from POZZAM where ZAMOWIENIE = new.ref and out = 0
      into :pozref
      do begin
        update STANYREZ set DATA = new.termdost where ZAMOWIENIE = new.ref and POZZAM = :pozref;
      end
  end
  if(:wydania = 3 and new.termstart <> old.termstart and new.onmainschedule = 0) then begin
      for select REF from POZZAM where ZAMOWIENIE = new.ref and out = 1
      into :pozref
      do begin
        update STANYREZ set DATA = new.termstart where ZAMOWIENIE = new.ref and POZZAM = :pozref;
      end
   end
-- obsluga rezerwacji naglowka
  termdost = new.termdost;
  typ = new.typ;
  if((new.kktm<>'' or old.kktm<>'') and new.onmainschedule = 0) then begin
    if(:termdost is null) then termdost = current_date;
    if (old.magazyn <> new.magazyn) then /*Sprawdza czy zosta zmieniony magazyn na zamówieniu jak tak to przenosi rezerwacje*/
    begin
      ilosc = new.kilosc;
      if (old.kstan <> 'N' and coalesce(old.kktm,'') <> '') then
      begin
        execute procedure REZ_SET_KTM(old.ref, 0, old.kktm, old.kwersja, 0, 0, 0, 'N',:termdost)
          returning_values :status;
        if(:status <> 1) then exception REZ_WRONG;
      end
      if (new.kstan <> 'N' and coalesce(new.kktm,'') <> '') then
      begin
        execute procedure REZ_SET_KTM(new.ref, 0, new.kktm, new.kwersja, :ilosc, 0, 0, new.kstan,:termdost)
          returning_values :status;
        if (:status <> 1) then exception REZ_WRONG;
      end
    end
    org_ref = new.org_ref;
    if(:org_ref is null) then org_ref = 0;
    if(((new.kktm <> old.kktm) or (new.kwersja <> old.kwersja) or
           ( (new.kstan = 'N' or (new.kstan <> old.kstan)) and old.kstan <> 'N' and old.kstan <> '')) AND OLD.KKTM <> ''
    )then begin
           /* zdjecie blokad/rezerwacji na starych zasadach */
/*          if(new.ilonklon > 0) then exception POZZAM_CHANGE_NOTALLOW;*/
           ilosc = old.kilosc;
           execute procedure REZ_SET_KTM(old.ref, 0, old.kktm, old.kwersja, 0, 0, 0, 'N',:termdost) returning_values :status;
           if(:status <> 1) then exception REZ_WRONG;
           if(:typ >=2) then ilosc = new.kilreal;
           else ilosc = new.kilosc;
/*           if(new.blokadarez = 1) then
             ilosc = 0;*/
           if(:ilosc > 0) then begin
             execute procedure REZ_SET_KTM(new.ref, 0, new.kktm, new.kwersja,:ilosc, 0, 0, new.kstan, :termdost) returning_values :status;
             if(:status <> 1) then exception REZ_WRONG;
           end
     end else if(
           ((new.kstan <> 'N' and (old.kstan = 'N' or (old.kstan <> new.kstan))and old.kstan is not null)
            or (((new.kilosc <> old.kilosc)-- or (new.ilonklon <> old.ilonklon)
                  --or (new.iloscm <> old.iloscm) or (new.ilonklonm <> old.ilonklonm)
                ) and :typ <=1
               )
            or ( (new.kilreal <> old.kilreal)
                 and :typ>=2
               )) and new.kktm <> ''
     )then begin
           if(:typ >=2) then begin
              oldilosc = old.kilreal;
              ilosc = new.kilreal;
           end else begin
              oldilosc = old.kilosc;--m - old.ilonklonm;
              ilosc = new.kilosc;--m - new.ilonklonm;
           end
           if(((:ilosc <>:oldilosc) or (old.kilzreal <> new.kilzreal) or (new.kstan <> old.kstan)) /*and new.blokadarez = 0*/)
           then begin
             execute procedure REZ_SET_KTM(new.ref, 0, new.kktm, new.kwersja,:ilosc, 0, 0, new.kstan, :termdost) returning_values :status;
             if(:status <> 1) then exception REZ_WRONG;
           end
     end
/*     if(new.ilosc <> old.ilosc) then begin
       execute procedure NAGZAM_CHECK_ZREAL(new.zamowienie);*/
   end
   if(new.onmainschedule = 0 and old.onmainschedule = 1) then begin
     --blokady na pozycjach - przywrocenie
     for select POZZAM.REF, POZZAM.KTM, POZZAM.wersja, POZZAM.cenamag, pozzam.dostawamag, pozzam.stan, pozzam.iloscm - pozzam.ilonklonm, pozzam.ilrealm
     from POZZAM where ZAMOWIENIE = new.ref
     order by NUMER
     into :pozref, :ktm, :wersja, :cenamag, :dostawa, :stan, :ilosc, :oldilosc
     do begin
       if(:typ >= 2) then
         ilosc = :oldilosc;
       execute procedure REZ_SET_KTM(old.ref, :pozref, :ktm, :wersja, :ilosc, :cenamag, :dostawa, :stan,old.termdost)
        returning_values :status;
       if(:status <> 1) then exception REZ_WRONG;
     end
     --nalozenie blokad naglowka
       if(:typ >=2) then begin
         ilosc = new.kilreal;
       end else begin
         ilosc = new.kilosc;--m - new.ilonklonm;
       end
       execute procedure REZ_SET_KTM(new.ref, 0, new.kktm, new.kwersja,:ilosc, 0, 0, new.kstan, :termdost) returning_values :status;
       if(:status <> 1) then exception REZ_WRONG;
   end
end^
SET TERM ; ^
