--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_BD FOR PRSCHEDZAM                     
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable main integer;
declare variable guide integer;
begin
  --przenisienie blokad i rezerwacji z przewodnikow na zamowienie
  select prschedules.main from prschedules where ref=old.prschedule into :main;
  if(:main = 1) then begin
    update PRSCHEDGUIDES set STAN = 'W', KSTAN = 'W' where PRSCHEDZAM = old.ref; --zwolnienei blokad, ale bez rozpisywania blokad
  end
  update prschedguides set pggamountcalc = 1 where prschedzam = old.ref;
  delete from PRSCHEDGUIDES where prschedzam = old.ref;
  if(:main = 1) then begin --wykonywane po usuniciu przewodnikow, by sie prawidlowo odkrecily rezerwacje/blokady z przewodnikow
    update NAGZAM set nagzam.onmainschedule = 0 where ref=old.zamowienie;--tutaj nastepuje przywrocenie przez NAGZAM wlasnych blokad
  end
end^
SET TERM ; ^
