--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU1 FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 2 
as
declare variable reffak integer;
declare variable fakturazal integer;
begin
  if((new.magazyn <> old.magazyn) or (new.magazyn is not null and old.magazyn is null))then
    update POZFAK set MAGAZYN = new.magazyn where (MAGAZYN = old.magazyn or (MAGAZYN is null)) and DOKUMENT = new.ref;
  if(new.symbol <> old.symbol) then
    update NAGFAK set SYMBOLFAK = new.symbol where REFFAK = new.ref;
  if(new.symbol <> old.symbol) then
    update NAGZAM set SYMBFAK = new.symbol where FAKTURA = new.ref;
  if(new.numblock <> old.numblock or (new.numblock is null and old.numblock is not null)) then
    update NUMBERBLOCK set DOKNAGFAK = null where ref=old.numblock;
  if(new.numblock <> old.numblock or (new.numblock is not null and old.numblock is null)) then
    update NUMBERBLOCK set DOKNAGFAK = new.ref where ref=new.numblock;
  if((new.listywysd is null and old.listywysd is not null) or (new.listywysd <> old.listywysd) or(new.dozaplaty <> old.dozaplaty)) then
    execute procedure LISTYWYSD_OBLICZPOBRANIE(old.listywysd);
  else if(new.listywysd > 0 and new.akceptacja > 0 and
         ((new.listywysd is not null and old.listywysd is null)
          or(new.listywysd <> old.listywysd)
          or(new.dozaplaty <> old.dozaplaty)
          or(new.sposzap <> old.sposzap)
         ))
     then
       execute procedure LISTYWYSD_OBLICZPOBRANIE(new.listywysd);
  if(new.akceptacja  <> old.akceptacja  or (new.anulowanie <> old.anulowanie) ) then begin
    if(exists(select ref from NAGFAK where GRUPADOK = new.grupadok and FROMNAGZAM > 0)) then
      execute procedure NAGFAKZAL_OBLONZAM(new.ref);
    else
      for select NAGFAKZAL.fakturazal  from NAGFAKZAL where NAGFAKZAL.FAKTURA = new.ref
      into :fakturazal
      do begin
        execute procedure NAGFAKZAL_OBLONZAM(:fakturazal);
      end
  end
end^
SET TERM ; ^
