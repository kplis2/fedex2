--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLNAGZAM_BI_REF FOR CPLNAGZAM                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPLNAGZAM')
      returning_values new.REF;
end^
SET TERM ; ^
