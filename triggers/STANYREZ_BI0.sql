--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYREZ_BI0 FOR STANYREZ                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable typ char(1);
declare variable numer integer;
declare variable iloscpo numeric(14,4);
declare variable iloscmag numeric(14,4);
begin
  if(new.priorytet is null) then new.priorytet = 10;
  if(new.zreal is null) then new.zreal = 0;
  if(new.dokummag is null) then new.dokummag =0;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.databl is null) then new.databl = current_date;
  if(new.zamowienie is null) then new.zamowienie = 0;
  if(new.datazmbl is null) then new.datazmbl = current_date;
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  select TYP from DEFMAGAZ where DEFMAGAZ.symbol=new.magazyn into :typ;
  if(:typ='C') then begin
    new.dostawa = NULL;
  end
  if(:typ='P') then begin
    if(new.dostawa = 0) then new.dostawa = NULL;
    if(new.dostawa is null) then new.cena = NULL;
  end
  new.onstcen = 1;
  if((:TYP='C' and (new.cena is null)) or
       (:typ='P' and (new.cena is null and new.dostawa is null))
  )then new.onstcen = 0;

  if(new.status = 'D') then begin
    new.ilplus = new.ilosc;
    new.ilminus = 0;
  end else begin
    new.ilminus = new.ilosc;
    new.ilplus = 0;
  end
  /*rozpoznanie typu STANYREZ - dokumentu powiazanego*/
  new.pozzamref = null;
  if(new.typ is null or (new.typ = '') or (new.typ = ' ')) then begin
    if(new.zamowienie > 0) then begin
      if(new.pozzam > 0) then begin
        if(
          exists (select REF from POZZAM where ZAMOWIENIE = new.zamowienie and REF=new.pozzam)
        )then begin
          new.typ = 'Z';--pozuycja zamowienia
          new.pozzamref = new.pozzam;
        end else begin
          new.typ = 'G';--pozycja przewodnika
          select prschedguidespos.pozzamref from prschedguidespos where ref=new.pozzam into new.pozzamref;
        end
      end else if(new.pozzam = 0) then begin
          new.typ = 'Z';--naglowek zamowienia
      end else
          new.typ = 'G';--naglowek przewodnika
    end else if(new.zreal=2) then begin
      new.typ = 'F';--faktura pozycje
    end else if(new.zreal=3) then begin
      new.typ = 'M';--pozycja dokumentu magazynowego
    end
  end
end^
SET TERM ; ^
