--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELADVANCES_BI0 FOR EDELADVANCES                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDELADVANCES')
      returning_values new.REF;
end^
SET TERM ; ^
