--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMELEMENTS_BI_PRZELICZ FOR PMELEMENTS                     
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
 -- sumowanie wartosci przychodowej planowanej
 new.calcsumval = new.calcmval + new.calcsval + new.calcwval + new.calceval + new.calcval;

 -- sumowanie wartosci kosztowej planowanej
 new.budsumval = new.budmval + new.budsval +  new.budwval +  new.budeval + new.budval;

  -- sumowanie wartosci przychodowej rzeczywistej
 new.erealsumval = new.erealmval + new.erealsval +  new.erealwval +  new.erealeval + new.erealval;

  -- sumowanie wartosci kosztowej rzeczywistej
 new.realsumval = new.realmval + new.realsval +  new.realwval +  new.realeval + new.realval;

 new.budproc = iif(new.budsumval<>0,100 * new.realsumval / new.budsumval,0);
end^
SET TERM ; ^
