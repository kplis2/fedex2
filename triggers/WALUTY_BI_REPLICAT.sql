--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WALUTY_BI_REPLICAT FOR WALUTY                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('WALUTY',new.symbol, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
