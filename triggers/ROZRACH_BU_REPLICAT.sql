--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BU_REPLICAT FOR ROZRACH                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.slodef<>old.slodef
      or new.slopoz<>old.slopoz
      or new.kontofk<>old.kontofk
      or new.symbfak<>old.symbfak
      or new.opis<>old.opis or (new.opis is not null and old.opis is null) or (new.opis is null and old.opis is not null)
      or new.klient<>old.klient or (new.klient is not null and old.klient is null) or (new.klient is null and old.klient is not null)
      or new.faktura<>old.faktura or (new.faktura is not null and old.faktura is null) or (new.faktura is null and old.faktura is not null)
      or new.dataotw<>old.dataotw or (new.dataotw is not null and old.dataotw is null) or (new.dataotw is null and old.dataotw is not null)
      or new.dataplat<>old.dataplat or (new.dataplat is not null and old.dataplat is null) or (new.dataplat is null and old.dataplat is not null)
      or new.datazamk<>old.datazamk or (new.datazamk is not null and old.datazamk is null) or (new.datazamk is null and old.datazamk is not null)
      or new.winien<>old.winien  or (new.winien is not null and old.winien is null) or (new.winien is null and old.winien is not null)
      or new.ma<>old.ma or (new.ma is not null and old.ma is null) or (new.ma is null and old.ma is not null)
      or new.saldo<>old.saldo or (new.saldo is not null and old.saldo is null) or (new.saldo is null and old.saldo is not null)
      or new.mazl<>old.mazl or (new.mazl is not null and old.mazl is null) or (new.mazl is null and old.mazl is not null)
      or new.winienzl<>old.winienzl or (new.winienzl is not null and old.winienzl is null) or (new.winienzl is null and old.winienzl is not null)
      or new.walutowy<>old.walutowy or (new.walutowy is not null and old.walutowy is null) or (new.walutowy is null and old.walutowy is not null)
      or new.waluta<>old.waluta or (new.waluta is not null and old.waluta is null) or (new.waluta is null and old.waluta is not null)
      or new.skad<>old.skad or (new.skad is not null and old.skad is null) or (new.skad is null and old.skad is not null)
      or new.odsetki<>old.odsetki or (new.odsetki is not null and old.odsetki is null) or (new.odsetki is null and old.odsetki is not null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('ROZRACH',old.slodef, old.slopoz, old.symbfak, old.kontofk, old.company, old.token, old.state,
        new.slodef, new.slopoz, new.symbfak, new.kontofk, new.company, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
