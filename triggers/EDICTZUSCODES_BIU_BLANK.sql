--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTZUSCODES_BIU_BLANK FOR EDICTZUSCODES                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.code = '') then
    new.code = null;
  if (new.descript = '') then
    new.descript = null;
end^
SET TERM ; ^
