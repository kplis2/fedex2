--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCH_AD_REPLICAT FOR FSCLRACCH                      
  ACTIVE AFTER DELETE POSITION 1 
AS
begin
  execute procedure RP_TRIGGER_AD('FSCLRACCH', old.ref, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
