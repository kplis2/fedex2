--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDEFPCARDSGROUP_BU_CHKGRPOS FOR EDEFPCARDSGROUP                
  ACTIVE BEFORE UPDATE POSITION 2 
as
begin
/*MWr: Kontrola, aby zmiana typu grupy (grupa jako suma grup) byla robiona
       tylko dla grupy bez przypisanych pozycji*/

  if (new.isgroupsum <> old.isgroupsum) then
    if (exists(select first 1 1 from edefpcardsgrpos where defpcardgr = new.ref)) then
      exception edefpcardsgroup_posingroup;
end^
SET TERM ; ^
