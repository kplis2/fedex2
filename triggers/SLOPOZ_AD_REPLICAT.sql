--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_AD_REPLICAT FOR SLOPOZ                         
  ACTIVE AFTER DELETE POSITION 1 
AS
begin
  execute procedure RP_TRIGGER_AD('SLOPOZ', old.ref, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
