--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATAL_BU_ORDER FOR CKATAL                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 1;
  if (new.numer is null) then new.numer = old.numer;
  if (new.ord = 0 or new.numer = old.numer) then
  begin
    new.ord = 1;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.numer <> old.numer) then
    select max(numer) from ckatal where cdefkatal = new.cdefkatal and cpodmiot = new.cpodmiot
      into :maxnum;
  else
    maxnum = 0;
  if (new.numer < old.numer) then
  begin
    update ckatal set ord = 0, numer = numer + 1
      where ref <> new.ref and numer >= new.numer
        and numer < old.numer and cdefkatal = new.cdefkatal and cpodmiot = new.cpodmiot;
  end else if (new.numer > old.numer and new.numer <= maxnum) then
  begin
    update ckatal set ord = 0, numer = numer - 1
      where ref <> new.ref and numer <= new.numer
        and numer > old.numer and cdefkatal = new.cdefkatal and cpodmiot = new.cpodmiot;
  end else if (new.numer > old.numer and new.numer > maxnum) then
    new.numer = old.numer;
end^
SET TERM ; ^
