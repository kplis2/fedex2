--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BU0 FOR POZZAM                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable bn char(1);
declare variable cennik integer;
declare variable cena decimal(14,4);
declare variable rabat decimal(14,2);
declare variable stawka_vat decimal(14,2);
declare variable eksport smallint;
declare variable kurs numeric(14,4);
declare variable typ char(1);
declare variable wydania smallint;
declare variable zewn smallint;
declare variable zamtyp smallint;
declare variable kilosc decimal(14,4);
declare variable jednprzelicz numeric(14,4);
declare variable jednconst smallint;
declare variable SYMBOL varchar(120);
declare variable termdost timestamp;
declare variable dostawca integer;
declare variable cnt integer;
declare variable walnag varchar(3);
declare variable walpln varchar(3);
declare variable rabatkask smallint;
declare variable nagzamid varchar(30);
declare variable prdepart varchar(40);
declare variable rabatnag varchar(40);
declare variable dokladnosc smallint;
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.kilosc is null) then new.kilosc = 0;
  if(new.kilcalcorg is null) then new.kilcalcorg = 1;
  if(new.ilonklon is null) then new.ilonklon = 0;
  if(new.ilonklonm is null) then new.ilonklonm = 0;
  if(new.ilosco is null) then new.ilosco = 0;
  if(new.ilrealo is null) then new.ilrealo = 0;
  if(new.iloscm is null) then new.iloscm = 0;
  if(new.ilrealm is null) then new.ilrealm = 0;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.ilreal is null) then new.ilreal = 0;
  if(new.ilzreal is null) then new.ilzreal = 0;
  if(new.ilzrealoverlimit is null) then new.ilzrealoverlimit = 0;
  if(new.ildysp is null) then new.ildysp = 0;
  if(new.ilzdysp is null) then new.ilzdysp = 0;
  if(new.ilzadysp is null) then new.ilzadysp = 0;
  if(new.rabat is null) then new.rabat = 0;
  if(new.rabattab is null) then new.rabattab = 0;
  if(new.cenamag is null) then new.cenamag = 0;
  if(new.blokadarez is null) then new.blokadarez = 0;
  if(new.hasownsupply is null) then new.hasownsupply = 0;
  if (new.ilsped is null) then new.ilsped = 0;
  if (new.ilosconmwsacts is null) then new.ilosconmwsacts = 0;
  if (new.ilosconmwsactsc is null) then new.ilosconmwsactsc = 0;
  if (new.anulowano is null) then new.anulowano = 0;
  if (coalesce(new.fake,0)=1) then
    new.opk = 1;
  else
    new.opk = 0;
  if((new.magazyn is null or new.magazyn = '') and new.prshmat is not null) then
      select warehouse from prshmat where ref = new.prshmat into new.magazyn;
  if(new.magazyn='') then new.magazyn=null;
  if(new.mag2='') then new.mag2=null;
  zamtyp = 0;
  select TYPZAM.WYDANiA,TYPZAM.ZEWN,NAGZAM.TYP,NAGZAM.KILOSC, NAGZAM.prdepart
    from TYPZAM
      join NAGZAM on (NAGZAM.TYPZAM = TYPZAM.SYMBOL)
    where NAGZAM.REF = new.zamowienie
    into :wydania,:zewn, :zamtyp, :kilosc, :prdepart;
  if(new.out is null) then begin
    if(wydania = 0) then new.out = 0;
    else new.out = 1;
  end
  select kurs from NAGZAM where nagzam.ref = new.zamowienie into :kurs;
  if(:kurs is null) then kurs = 1;
  if(new.hasownsupply = 1 and old.hasownsupply = 0) then begin
    /*zaożenie indywidualnej dostawy*/
    select ID,TERMDOST,DOSTAWCA from NAGZAM where REF=new.zamowienie into :symbol,:termdost,:dostawca;
    if(new.termdost is not null) then begin
       termdost = new.termdost;
       symbol = :symbol||'/'||cast(:termdost as date);
    end
    new.cenamag = null;
    new.dostawamag = null;
    select max(ref) from DOSTAWY where SYMBOL = :symbol into new.dostawamag;
    if(new.dostawamag is null) then begin
      execute procedure GEN_REF('DOSTAWY') returning_values new.dostawamag;
      insert into dostawy(REF,SYMBOL,RECZNYSYMBOL,DATA,MAGAZYN) values (new.dostawamag, :symbol,1,:termdost,new.magazyn);
    end
  end else if(new.hasownsupply = 0 and old.hasownsupply = 1) then begin
    /*usuniecie indywidualnej dostawy*/
    new.cenamag = null;
    new.dostawamag = null;
    select count(*) from POZZAM where DOSTAWAMAG=old.dostawamag and REF<>new.ref into :cnt;
    if(:cnt=0) then delete from DOSTAWY where REF=old.dostawamag and STATUS='O';
  end else if(new.hasownsupply = 1 and old.hasownsupply = 1 and new.dostawamag is not null) then begin
    /*poprawienie indywidualnej dostawy*/
    if((new.termdost<>old.termdost) or (new.termdost is not null and old.termdost is null) or (new.termdost is null and old.termdost is not null) or
       (new.magazyn<>old.magazyn)) then begin
      select ID,TERMDOST,DOSTAWCA from NAGZAM where REF=new.zamowienie into :symbol,:termdost,:dostawca;
      if(new.termdost is not null) then begin
         termdost = new.termdost;
         symbol = :symbol||'/'||cast(:termdost as date);
      end
      update DOSTAWY set SYMBOL=:symbol,DATA=:termdost,MAGAZYN=new.magazyn
        where REF = new.dostawamag;
    end
  end
     /*kontrola rezerwacji na cene lub dostawe w zaleznosci od typu magazynu*/
     if((new.magazyn <> old.magazyn) or
        ((new.cenamag <> old.cenamag) or (new.cenamag is null and old.cenamag is not null) or (new.cenamag is not null and old.cenamag is null)) or
        ((new.dostawamag <> old.dostawamag) or (new.dostawamag is null and old.dostawamag is not null) or (new.dostawamag is not null and old.dostawamag is null)))
     then begin
       select TYP from DEFMAGAZ where SYMBOL = new.magazyn into :typ;
       if(:typ<> 'P' and :typ <> 'C' and :wydania <> 2 and :wydania <> 3) then begin
         new.cenamag = 0;
         new.dostawamag = null;
       end
       if(:typ = 'C') then new.dostawamag = null;
     end
  /* reczne redagowanie realizacji czesciowej */
  if(:zamtyp=2 and new.ilreal<>old.ilreal) then begin
    new.ilosc = new.ilreal;
    new.iloscm = new.ilrealm;
    new.ilzreal = new.ilreal;
    new.ilzrealm = new.ilrealm;
  end
  if(   (new.ilosc <> old.ilosc) or (new.rabat <> old.rabat)
     or (new.cenacen <> old.cenacen) or (new.kilosc <> old.kilosc)
     or (new.jedn is null) or (new.jedn = 0) or (new.jedno is null) or (new.jedno = 0)
     or (new.iloscm <> old.iloscm) or (new.kurscen<>old.kurscen)
  ) then begin
    if(new.kilosc <> old.kilosc) then
    begin
      if (wydania = 2) then
        select KILOSC from NAGZAM where REF=new.zamowienie into new.ilosc;
      else if (wydania = 3 and new.out <> 0) then
        exception prpozzam_error 'Parametr z karty techn. Zmiana na zleceniu niemożliwa.';
      new.ilosc = new.ilosc * new.kilosc;
      --uwzglednienie dokladnosci
      select dokladnosc from MIARA M left join towjedn J on (J.jedn=M.miara) where J.ref = new.jedn into :dokladnosc;
      if (dokladnosc = 1) then new.ilosc=cast(new.ilosc as numeric(14,1));
      else if (dokladnosc = 2) then new.ilosc=cast(new.ilosc as numeric(14,2));
      else if (dokladnosc = 3) then new.ilosc=cast(new.ilosc as numeric(14,3));
      else if (dokladnosc = 4) then new.ilosc=cast(new.ilosc as numeric(14,4));
      else new.ilosc=cast(new.ilosc as numeric(14,0));
    end
    if(new.rabat = -101) then new.rabat = old.rabat;
    select nagzam.rabat, eksport, waluta, kurs, dostawca, bn
      from NAGZAM
      where nagzam.ref = new.zamowienie into :rabat, :eksport, :walnag, :kurs, :dostawca, :bn;
    if(:eksport = 1)then
      execute procedure GETCONFIG('VATEXPORT') returning_values new.gr_vat;
    else if(new.gr_vat is not null) then begin
      select vat.stawka from vat where vat.grupa=new.gr_vat into :stawka_vat;
    end else begin
      select vat.stawka, vat.grupa
        from wersje
          join vat on (vat.grupa = wersje.vat)
        where ref = new.wersjaref
        into :stawka_vat, new.gr_vat;
      if (new.gr_vat is null) then
        select vat.stawka,vat.grupa
          from vat
            join towary on(towary.vat = vat.grupa)
          where ktm = new.ktm
          into :stawka_vat,new.gr_vat;
    end
    if(:stawka_vat is null) then stawka_vat = 0;
    if(:bn is null) then
      execute procedure GETCONFIG('WGCEN') returning_values :bn;
    if((:bn <> 'B') and (:bn <> 'N') ) then bn = 'N';
    if(:eksport is null) then eksport = 0;
    if(:rabat is null) then rabat = 0;
    if(:kurs is null) then kurs = 1;
    /*ustalenie jednostki w zależnosci od rodzaju zamowienia*/
    if(new.jedn is null or (new.jedn = 0)) then begin
      if(:wydania = 1 and :zewn = 1) then
        select REF from TOWJEDN where KTM = new.ktm and C = 2 into new.jedn;
      else if(:wydania = 1 and :zewn = 0)then
        select REF from TOWJEDN where KTM = new.ktm and S = 2 into new.jedn;
      else if(:wydania = 0 ) then
        select ref from TOWJEDN where KTM = new.ktm and Z  = 2 into new.jedn;
      if(new.jedn is null or (new.jedn = 0))then
        select ref from TOWJEDN where KTM = new.KTM and GLOWNA = 1 into new.jedn;
    end
    if(new.jedno is null or (new.jedno = 0))then begin
      if(:wydania = 0) then
        select REF from TOWJEDN where KTM = new.ktm and Z = 2 into new.jedno;
      if(:wydania = 1) then
        select REF from TOWJEDN where KTM = new.ktm and S = 2 into new.jedno;
    end
    if(new.jedno is null or (new.jedno = 0))then
      new.jedno = new.jedn;
    if(new.jedn = new.jedno) then begin
      new.ilosco = new.ilosc;
      new.ilrealo = new.ilreal;
    end else begin
      select PRZELICZ, CONST from TOWJEDN where REF=new.jedno into :jednprzelicz, :jednconst;
      if(new.ilrealo is null or (new.ilrealo = 0) or (:jednconst = 1)) then
        new.ilrealo = new.ilreal / :jednprzelicz;
    end
    select PRZELICZ, CONST from TOWJEDN where REF=new.jedn into :jednprzelicz, :jednconst;
    if(new.iloscm is null or (new.iloscm = 0) or (:jednconst = 1)) then begin
      new.iloscm = new.ilosc * :jednprzelicz;
    end
    if(new.ilrealm is null or (new.ilrealm = 0) or (:jednconst = 1)) then
      new.ilrealm = new.ilreal * :jednprzelicz;
  /* obliczenie ceny */
    if(new.cenacen is null) then begin
      if(new.refcennik is not null) then cennik = new.refcennik;
      else select KCENNIK from NAGZAM where nagzam.ref = new.zamowienie into :cennik;
      if(:cennik is null) then
        select grupykli.cennik from klienci join grupykli on(klienci.grupa = grupykli.ref) join nagzam on(klienci.ref = nagzam.klient) where nagzam.ref = new.zamowienie into :cennik;
      if(:bn = 'B') then begin
        select  cenabru, waluta from cennik where cennik = :cennik and ktm = new.ktm and wersja = new.wersja and jedn = new.jedn into new.cenacen, new.walcen;
        if(new.cenacen is null) then
          select  cenabru, waluta from cennik where cennik = :cennik and ktm = new.ktm and wersja = 0 and jedn = new.jedn into new.cenacen, new.walcen;
      end else begin
        select  cenanet, waluta from cennik where cennik = :cennik and ktm = new.ktm and wersja = new.wersja and jedn = new.jedn into new.cenacen, new.walcen;
        if(new.cenacen is null) then
          select  cenanet, waluta from cennik where cennik = :cennik and ktm = new.ktm and wersja = 0 and jedn = new.jedn into new.cenacen, new.walcen;
      end
      cena = new.cenacen;
    end else
      cena = new.cenacen;
    /* przeliczenie walut cennika na walutę dokumenut*/
    if(:kurs is null or :kurs=0) then kurs = 1;
    if(new.walcen <> :walnag and new.walcen is not null and new.walcen <> '') then begin
      execute procedure GETCONFIG('WALPLN') returning_values :walpln;
      if(:walpln is null) then walpln = 'PLN';
      if(new.kurscen is null or new.kurscen=0) then new.kurscen = 1;
      cena = :cena * new.kurscen / :kurs;
    end else new.kurscen = 1;

    /*obliczenie rabatu */
    execute procedure GETCONFIG('RABATNAG') returning_values :rabatnag;
    if(:wydania=1 and :rabatnag='1' and new.rabattab<>0) then begin
      if(new.prec=4) then cena = cast((:cena * (100- new.rabat) /100) as decimal(14,4));
      else cena = cast((:cena * (100- new.rabat) /100) as decimal(14,2));
    end else begin
      if(dostawca is not null and dostawca>0) then
        select d.rabatkask from dostawcy d where d.ref = :dostawca into :rabatkask;
      if(rabatkask is not null and rabatkask = 1)then begin
        if(new.prec=4) then cena = cast(:cena * (100- :rabat)*(100 - new.rabat)/10000 as decimal(14,4));
        else cena = cast(:cena * (100- :rabat)*(100 - new.rabat)/10000 as decimal(14,2));
      end else begin
        if(new.rabat is not null) then rabat = rabat + new.rabat;
        if(:rabat <> 0)then begin
          if(new.prec=4) then cena = cast(:cena * (100- :rabat)/100 as decimal(14,4));
          else cena = cast(:cena * (100- :rabat)/100 as decimal(14,2));
        end
      end
    end
    /* obliczenie cen */
    if(:bn = 'B') then begin
       new.cenabru = cena;
       new.cenabruzl = cena * :kurs;
       new.wartbru = new.ilosc * new.cenabru;
       new.wartbruzl = new.ilosc * new.cenabruzl;
       new.kwartbru = new.kilosc * new.cenabru;

       new.wartnet = cast((new.wartbru / (1+:stawka_vat / 100)) as decimal(14,2));
       new.wartnetzl = cast((new.wartbruzl / (1+:stawka_vat / 100)) as decimal(14,2));
       new.cenanet = cast((new.cenabru / (1+(:stawka_vat/100))) as decimal(14,2));
       new.cenanetzl = cast((new.cenabruzl / (1+(:stawka_vat/100))) as decimal(14,2));
       new.kwartnet = cast((new.kwartbru / (1+:stawka_vat / 100)) as decimal(14,2));
    end else begin
       new.cenanet = cena;
       new.cenanetzl = cena * :kurs;
       new.wartnet = new.ilosc * new.cenanet;
       new.wartnetzl = new.ilosc * new.cenanetzl;
       new.kwartnet = new.kilosc * new.cenanet;

       new.wartbru = new.wartnet + cast((new.wartnet * (:stawka_vat / 100)) as decimal(14,2));
       new.wartbruzl = new.wartnetzl + cast((new.wartnetzl * (:stawka_vat / 100)) as decimal(14,2));
       new.cenabru = new.cenanet + cast((new.cenanet * (:stawka_vat / 100)) as decimal(14,2));
       new.cenabruzl = new.cenanetzl + cast((new.cenanetzl * (:stawka_vat / 100)) as decimal(14,2));
       new.kwartbru = new.kwartnet + cast((new.wartnet * (:stawka_vat / 100)) as decimal(14,2));
    end
  end
  if((new.cenabru <> old.cenabru) or (new.cenanet <> old.cenanet) or (new.ilreal<> old.ilreal))then begin
    if(new.gr_vat is not null) then begin
      select vat.stawka from vat where vat.grupa=new.gr_vat into :stawka_vat;
    end else begin
      select vat.stawka,vat.grupa
      from vat  join towary on(towary.vat = vat.grupa)
      where ktm = new.ktm into :stawka_vat,new.gr_vat;
    end
    if(:stawka_vat is null) then stawka_vat = 0;
    select eksport, bn from NAGZAM where nagzam.ref = new.zamowienie into :eksport, :bn;
    if(:eksport is null) then eksport = 0;
    if(:bn is null) then
      execute procedure GETCONFIG('WGCEN') returning_values :bn;
    /*ustalenie jednostki w zależnosci od rodzaju zamowienia*/
    if(new.jedn is null or (new.jedn = 0)) then begin
      if(:wydania = 1 and :zewn = 1) then
        select REF from TOWJEDN where KTM = new.ktm and C = 2 into new.jedn;
      else if(:wydania = 1 and :zewn = 0)then
        select REF from TOWJEDN where KTM = new.ktm and S = 2 into new.jedn;
      else if(:wydania = 0 ) then
        select ref from TOWJEDN where KTM = new.ktm and Z  = 2 into new.jedn;
      if(new.jedn is null or (new.jedn = 0))then
        select ref from TOWJEDN where KTM = new.KTM and GLOWNA = 1 into new.jedn;
    end
    if(new.jedno is null or (new.jedno = 0))then begin
      if(:wydania = 0) then
        select REF from TOWJEDN where KTM = new.ktm and Z = 2 into new.jedno;
      if(:wydania = 1) then
        select REF from TOWJEDN where KTM = new.ktm and S = 2 into new.jedno;
    end
    if(new.jedno is null or (new.jedno = 0))then
      new.jedno = new.jedn;
    if(new.jedn = new.jedno) then begin
      new.ilosco = new.ilosc;
      new.ilrealo = new.ilreal;
    end else begin
      select PRZELICZ, CONST from TOWJEDN where REF=new.jedno into :jednprzelicz, :jednconst;
      if(new.ilrealo is null or (new.ilrealo = 0) or (:jednconst = 1)) then
        new.ilrealo = new.ilreal / :jednprzelicz;
    end
    select PRZELICZ, CONST from TOWJEDN where REF=new.jedn into :jednprzelicz, :jednconst;
    if(new.iloscm is null or (new.iloscm = 0) or (:jednconst = 1)) then
      new.iloscm = new.ilosc * :jednprzelicz;
    if(new.ilrealm is null or (new.ilrealm = 0) or (:jednconst = 1)) then
      new.ilrealm = new.ilreal * :jednprzelicz;
    if((:bn <> 'B') and (:bn <> 'N') ) then bn = 'N';
    if(:bn = 'B') then begin
      new.wartrbru = new.ilreal * new.cenabru;
      new.wartrbruzl = new.ilreal * new.cenabruzl;
      if(:eksport = 0) then begin
        new.wartrnet = cast((new.wartrbru / (1+:stawka_vat / 100)) as decimal(14,2));
        new.wartrnetzl = cast((new.wartrbruzl / (1+:stawka_vat / 100)) as decimal(14,2));
      end else begin
        new.wartrnet = new.wartrbru;
        new.wartrnetzl = new.wartrbruzl;
      end
    end else begin
      new.wartrnet = new.ilreal * new.cenanet;
      new.wartrnetzl = new.ilreal * new.cenanet;
      if(:eksport = 0) then begin
        new.wartrbru = new.wartrnet + cast((new.wartrnet * (:stawka_vat / 100)) as decimal(14,2));
        new.wartrbruzl = new.wartrnetzl + cast((new.wartrnetzl * (:stawka_vat / 100)) as decimal(14,2));
      end else begin
        new.wartrbru = new.wartrnet;
        new.wartrbruzl = new.wartrnetzl;
      end
    end
  end
  if((new.ilosc <> old.ilosc) or (new.cenamag <> old.cenamag))then
    new.wartmag = new.ilosc * new.cenamag;
  if(new.wartmag is null) then new.wartmag = 0;
  if(new.wartrmag is null) then new.wartrmag = 0;
    if(:kilosc > 0) then begin
      if(:zamtyp < 2) then begin
        new.kwartmag = new.wartmag / :kilosc;
      end
      else begin
        new.kwartmag = new.wartrmag / :kilosc;
      end
    end else
      new.kwartmag = 0;
  if(new.ilosco <> old.ilosco or (new.jedno <> old.jedno)) then begin
    select TOWJEDN.waga*new.ilosco, TOWJEDN.vol * new.ilosco from TOWJEDN where ref=new.jedno into new.waga, new.objetosc;
  end
  /* pola zlotowkowe */
/*  new.cenanetzl = new.cenanet * :kurs;
  new.cenabruzl = new.cenabru * :kurs;
  new.wartbruzl = new.wartbru * :kurs;
  new.wartnetzl = new.wartnet * :kurs;
  new.wartrnetzl = new.wartrnet * :kurs;
  new.wartrbruzl = new.wartrbru * kurs;*/
  if(:kilosc > 0 and new.ilosc < new.ilzreal) then exception prnagzam_error substring('Ilość zreal. < ilość zlec.- poz:'||new.numer||' zlec:'||:nagzamid from 1 for 55);
  if (:wydania = 3) then
  begin
    if (new.magazyn <> old.magazyn and new.out = 1) then
    begin
      if (not exists(select first 1 1
          from prdepartswarehouses w
          where w.prdepart = :prdepart and w.warehouse = new.magazyn and w.out = new.out)
      ) then
        exception prpozzam_error 'Magazyn '||new.magazyn||' niedozwolony dla rozchodu z wydziału '||:prdepart;
    end
    if (new.magazyn <> old.magazyn and new.out = 0) then
    begin
      if (not exists(select first 1 1
          from prdepartswarehouses w
          where w.prdepart = :prdepart and w.warehouse = new.magazyn and w.out = new.out)
      ) then
        exception prpozzam_error 'Magazyn '||new.magazyn||' niedozwolony dla przychodu z wydziału '||:prdepart;
    end
    if (new.magazyn <> old.magazyn and new.out in (1,2)) then
    begin
      if (exists(select first 1 1
           from prschedguidespos p
             left join prschedguidedets d on (d.prschedguidepos = p.ref)
           where p.pozzamref = new.ref and d.ssource = 0)
      ) then
        exception prpozzam_error 'Zarejestrowano już rozpiski magazynowe. Zmiana mag. niemożliwa';
    end
    else if (new.magazyn <> old.magazyn and new.out = 0) then
    begin
      if (exists(select first 1 1
          from prschedguides g
            left join prschedguidedets d on (d.prschedguide = g.ref)
          where g.pozzam = new.ref and d.ssource = 0)
      ) then
        exception prpozzam_error 'Zarejestrowano już rozpiski magazynowe. Zmiana mag. niemożliwa';
    end
    if (new.out = 0 and (new.ktm <> old.ktm or new.wersjaref <> old.wersjaref or coalesce(new.prsheet,0) <> coalesce(old.prsheet,0))
    ) then
    begin
      if (not exists (select first 1 1 from prsheets s where s.ktm = new.ktm and s.ref = new.prsheet and new.prsheet is not null)
      ) then
        exception prpozzam_error 'Niezgodna karta technologiczna z wyrobem';
      if (not exists (select first 1 1 from prsheets s where s.ktm = new.ktm and s.wersjaref is not null
          and s.wersjaref = new.wersjaref and s.ref = new.prsheet and new.prsheet is not null)
      ) then
        exception prpozzam_error 'Niezgodna karta technologiczna z wyrobem';
      if (exists(select first 1 1
          from PR_POZZAM_CASCADE(new.ref,0,0,0,1) pr
          where pr.ref <> new.ref)
      ) then
        exception prpozzam_error 'Niedozwolona zmiana na zleceniu z wygenerowanymi pozycjami materiałowymi';
    end
  end
end^
SET TERM ; ^
