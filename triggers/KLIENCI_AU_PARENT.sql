--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AU_PARENT FOR KLIENCI                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable ref integer;
begin
  if (coalesce(new.ancestors,0) <> coalesce(old.ancestors,0)) then
  begin
    for select ref from KLIENCI where parent = new.ref into :ref
    do begin
       if (new.ancestors is null or new.ancestors='') then
         update KLIENCI set ancestors = ';'||new.ref||';' where ref = :ref;
       else
         update KLIENCI set ancestors = new.ancestors || new.ref || ';' where ref = :ref;
    end
  end
end^
SET TERM ; ^
