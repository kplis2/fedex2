--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KOSZTDOST_BI0 FOR KOSZTDOST                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  IF (NEW.REF IS NULL or (new.ref = 0)) THEN
    execute procedure GEN_REF('KOSZTDOST') returning_values new.ref;
  if(new.sposdostcennik is not null) then
    select sposdost from sposdostcennik where ref=new.sposdostcennik into new.spedytor;
  if(new.wagap is null) then new.wagap = 0;
  if(new.waga is null) then new.waga = 0;
  if(new.odlegloscp is null) then new.odlegloscp = 0;
  if(new.odleglosc is null) then new.odleglosc = 0;
  if(new.iloscpalp is null) then new.iloscpalp = 0;
  if(new.iloscpal is null) then new.iloscpal = 0;
  if(new.ilosckartp is null) then new.ilosckartp = 0;
  if(new.ilosckart is null) then new.ilosckart = 0;
end^
SET TERM ; ^
