--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLATNOSCI_BU_REPLICAT FOR PLATNOSCI                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or (new.opis <> old.opis) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
      or (new.termin <> old.termin) or (new.termin is null and old.termin is not null) or (new.termin is not null and old.termin is null)
      or (new.autozap  <> old.autozap ) or (new.autozap is null and old.autozap is not null) or (new.autozap is not null and old.autozap is null)
      or (new.faktoring <> old.faktoring ) or (new.faktoring is null and old.faktoring is not null) or (new.faktoring is not null and old.faktoring is null)
      or (new.witryna <> old.witryna ) or (new.witryna is null and old.witryna is not null) or (new.witryna is not null and old.witryna is null)
      or (new.nazwadruk <> old.nazwadruk ) or (new.nazwadruk is null and old.nazwadruk is not null) or (new.nazwadruk is not null and old.nazwadruk is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('PLATNOSCI',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
