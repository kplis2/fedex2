--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_BU2 FOR PRSCHEDGUIDEDETS               
  ACTIVE BEFORE UPDATE POSITION 2 
AS
declare variable autodoc smallint;
begin
  if (new.prschedguidepos is not null and new.out = 1 and old.overlimit > 0 and new.overlimit = 0) then
  begin
    select p.autodocout
      from prschedguidespos p
      where p.ref = new.prschedguidepos
      into autodoc;
    if (autodoc is null) then autodoc = 0;
    if (autodoc > 0 and new.autodoc = 0) then
      exception prschedguidedets_error 'Pozycja rozchodowana automatycznie. Ręcznie tylko ponad limit.';
  end
  if (new.prschedguide is not null and new.out = 0 and old.overlimit > 0 and new.overlimit = 0) then
  begin
    select p.autodocin
      from prschedguides g
        left join prsheets p on (p.ref = g.prsheet)
      where g.ref = new.prschedguide
      into autodoc;
    if (autodoc is null) then autodoc = 0;
    if (autodoc > 0 and new.autodoc = 0) then
      exception prschedguidedets_error 'Pozycja przyjmowana automatycznie. Ręcznie tylko ponad limit.';
  end
end^
SET TERM ; ^
