--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_HASHDATA_BI FOR S_HASHDATA                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable nr integer;
begin
  if(new.number is null) then begin
    select max(number)+1 from s_hashdata where datatype=new.datatype into :nr;
    if(:nr is null) then nr = 1;
    new.number = :nr;
  end
end^
SET TERM ; ^
