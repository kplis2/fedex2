--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHAREAS_BI0 FOR WHAREAS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  select coordxb, coordxe from whsecrows where ref = new.whsecrow
    into new.coordxb, new.coordxe;
end^
SET TERM ; ^
