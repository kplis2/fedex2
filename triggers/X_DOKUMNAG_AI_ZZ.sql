--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMNAG_AI_ZZ FOR DOKUMNAG                       
  ACTIVE AFTER INSERT POSITION 101 
as
declare variable REFMWSORD mwsords_id;
begin
-- MKD ZG 133728
  if (new.typ = 'ZZ' and new.akcept = 1) then
  begin
    execute procedure x_mws_akcept_and_realize_cor( new.ref )
      returning_values REFMWSORD;
  end
end^
SET TERM ; ^
