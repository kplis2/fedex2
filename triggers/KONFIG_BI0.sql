--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIG_BI0 FOR KONFIG                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.akronim = '') then new.akronim = null;
  if (new.multi = 0 and new.historia = 1) then
    exception konfig_historia;
end^
SET TERM ; ^
