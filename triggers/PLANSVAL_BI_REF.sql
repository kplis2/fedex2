--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLANSVAL_BI_REF FOR PLANSVAL                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PLANSVAL')
      returning_values new.REF;
end^
SET TERM ; ^
