--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDAWCY_AI0 FOR SPRZEDAWCY                     
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable slodef integer;
declare variable cnt integer;
begin
 if(new.crm = 1) then begin
   select ref from SLODEF where TYP = 'SPRZEDAWCY' into :slodef;
   if(:slodef is null) then exception SLODEF_BEZSPRZEDAWCY;
   select count(*) from CPODMIOTY where SLODEF = :slodef AND SLOPOZ = new.ref into :cnt;
   if(:cnt > 0) then
     update CPODMIOTY set SKROT = substring(new.skrot from 1 for 40), NAZWA = new.nazwa,IMIE = new.imie, NAZWISKO = new.nazwisko,firma = new.firma,   --XXX ZG133796 MKD
                          TELEFON = new.telefon, EMAIL = new.email,
                           nip = new.nip,
                           ULICA = new.adres, miasto = new.miasto, kodp = new.kodpoczt, poczta = new.poczta, kontofk = new.kontofk
        where SLODEF = :slodef AND SLOPOZ = new.ref ;
   else
     insert into CPODMIOTY(AKTYWNY, SKROT, NAZWA, IMIE, NAZWISKO, FIRMA, TELEFON, EMAIL,
           NIP, SLODEF, SLOPOZ, ULICA, MIASTO, KODP, POCZTA, KONTOFK, company)
      values(1, substring(new.skrot from 1 for 40), new.nazwa, new.imie, new.nazwisko, new.firma, new.telefon, new.email,  --XXX ZG133796 MKD
             new.nip, :slodef, new.ref, new.adres, new.miasto, new.kodpoczt, new.poczta, new.kontofk, new.company);

 end
end^
SET TERM ; ^
