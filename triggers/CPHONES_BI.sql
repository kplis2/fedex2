--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPHONES_BI FOR CPHONES                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable len integer;
begin
  if(new.fullnumber is not null and new.fullnumber <> '') then begin
    execute procedure string_reverse(new.fullnumber) returning_values new.mirrornumber;
    len = coalesce(char_length(new.fullnumber),0); -- [DG] XXX ZG119346
    new.directnumber = substring(new.fullnumber from  :len - 6 for :len);
    if (:len > 7) then
      new.areacode = substring(new.fullnumber from  :len - 8 for :len - 7);
    if (:len > 10) then
      new.international = substring(new.fullnumber from  :len - 10 for :len - 9);
  end
end^
SET TERM ; ^
