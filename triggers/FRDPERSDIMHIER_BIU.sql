--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMHIER_BIU FOR FRDPERSDIMHIER                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable status smallint;
begin
  if (new.showval1 is null) then new.showval1 = 0;
  if (new.showval2 is null) then new.showval2 = 0;
  if (new.showval3 is null) then new.showval3 = 0;
  if (new.showval4 is null) then new.showval4 = 0;
  if (new.showval5 is null) then new.showval5 = 0;
  if (new.plancalcmeth is null) then new.plancalcmeth = 0;
  if (new.realcalcmeth is null) then new.realcalcmeth = 0;
  if (new.val1calcmeth is null) then new.val1calcmeth = 0;
  if (new.val2calcmeth is null) then new.val2calcmeth = 0;
  if (new.val3calcmeth is null) then new.val3calcmeth = 0;
  if (new.val4calcmeth is null) then new.val4calcmeth = 0;
  if (new.val5calcmeth is null) then new.val5calcmeth = 0;
  if (new.plancalcfrom is null) then new.plancalcfrom = 0;
  if (new.realcalcfrom is null) then new.realcalcfrom = 1;
  if (new.val1calcfrom is null) then new.val1calcfrom = 2;
  if (new.val2calcfrom is null) then new.val2calcfrom = 3;
  if (new.val3calcfrom is null) then new.val3calcfrom = 4;
  if (new.val4calcfrom is null) then new.val4calcfrom = 5;
  if (new.val5calcfrom is null) then new.val5calcfrom = 6;
  if (new.plancalcfrom = 0) then new.plancalcfroms = 'PLANAMOUNT';
  if (new.plancalcfrom = 1) then new.plancalcfroms = 'REALAMOUNT';
  if (new.plancalcfrom = 2) then new.plancalcfroms = 'VAL1';
  if (new.plancalcfrom = 3) then new.plancalcfroms = 'VAL2';
  if (new.plancalcfrom = 4) then new.plancalcfroms = 'VAL3';
  if (new.plancalcfrom = 5) then new.plancalcfroms = 'VAL4';
  if (new.plancalcfrom = 6) then new.plancalcfroms = 'VAL5';
  if (new.realcalcfrom = 0) then new.realcalcfroms = 'PLANAMOUNT';
  if (new.realcalcfrom = 1) then new.realcalcfroms = 'REALAMOUNT';
  if (new.realcalcfrom = 2) then new.realcalcfroms = 'VAL1';
  if (new.realcalcfrom = 3) then new.realcalcfroms = 'VAL2';
  if (new.realcalcfrom = 4) then new.realcalcfroms = 'VAL3';
  if (new.realcalcfrom = 5) then new.realcalcfroms = 'VAL4';
  if (new.realcalcfrom = 6) then new.realcalcfroms = 'VAL5';
  if (new.val1calcfrom = 0) then new.val1calcfroms = 'PLANAMOUNT';
  if (new.val1calcfrom = 1) then new.val1calcfroms = 'REALAMOUNT';
  if (new.val1calcfrom = 2) then new.val1calcfroms = 'VAL1';
  if (new.val1calcfrom = 3) then new.val1calcfroms = 'VAL2';
  if (new.val1calcfrom = 4) then new.val1calcfroms = 'VAL3';
  if (new.val1calcfrom = 5) then new.val1calcfroms = 'VAL4';
  if (new.val1calcfrom = 6) then new.val1calcfroms = 'VAL5';
  if (new.val2calcfrom = 0) then new.val2calcfroms = 'PLANAMOUNT';
  if (new.val2calcfrom = 1) then new.val2calcfroms = 'REALAMOUNT';
  if (new.val2calcfrom = 2) then new.val2calcfroms = 'VAL1';
  if (new.val2calcfrom = 3) then new.val2calcfroms = 'VAL2';
  if (new.val2calcfrom = 4) then new.val2calcfroms = 'VAL3';
  if (new.val2calcfrom = 5) then new.val2calcfroms = 'VAL4';
  if (new.val2calcfrom = 6) then new.val2calcfroms = 'VAL5';
  if (new.val3calcfrom = 0) then new.val3calcfroms = 'PLANAMOUNT';
  if (new.val3calcfrom = 1) then new.val3calcfroms = 'REALAMOUNT';
  if (new.val3calcfrom = 2) then new.val3calcfroms = 'VAL1';
  if (new.val3calcfrom = 3) then new.val3calcfroms = 'VAL2';
  if (new.val3calcfrom = 4) then new.val3calcfroms = 'VAL3';
  if (new.val3calcfrom = 5) then new.val3calcfroms = 'VAL4';
  if (new.val3calcfrom = 6) then new.val3calcfroms = 'VAL5';
  if (new.val4calcfrom = 0) then new.val4calcfroms = 'PLANAMOUNT';
  if (new.val4calcfrom = 1) then new.val4calcfroms = 'REALAMOUNT';
  if (new.val4calcfrom = 2) then new.val4calcfroms = 'VAL1';
  if (new.val4calcfrom = 3) then new.val4calcfroms = 'VAL2';
  if (new.val4calcfrom = 4) then new.val4calcfroms = 'VAL3';
  if (new.val4calcfrom = 5) then new.val4calcfroms = 'VAL4';
  if (new.val4calcfrom = 6) then new.val4calcfroms = 'VAL5';
  if (new.val5calcfrom = 0) then new.val5calcfroms = 'PLANAMOUNT';
  if (new.val5calcfrom = 1) then new.val5calcfroms = 'REALAMOUNT';
  if (new.val5calcfrom = 2) then new.val5calcfroms = 'VAL1';
  if (new.val5calcfrom = 3) then new.val5calcfroms = 'VAL2';
  if (new.val5calcfrom = 4) then new.val5calcfroms = 'VAL3';
  if (new.val5calcfrom = 5) then new.val5calcfroms = 'VAL4';
  if (new.val5calcfrom = 6) then new.val5calcfroms = 'VAL5';
  if (new.val1name is null) then new.val1name = '';
  if (new.val2name is null) then new.val2name = '';
  if (new.val3name is null) then new.val3name = '';
  if (new.val4name is null) then new.val4name = '';
  if (new.val5name is null) then new.val5name = '';
  select frhdrs.status
    from frdimhier
      join frhdrs on (frhdrs.symbol = frdimhier.frhdr)
    where frdimhier.ref = new.frdimshier
    into :status;
  if (new.shortname is null) then
    select frdimhier.shortname
      from frdimhier
      where frdimhier.ref = new.frdimshier
      into new.shortname;
 /* if (:status <> 0) then
    exception FRHDRS_NOT_ACCESIBLE;*/
end^
SET TERM ; ^
