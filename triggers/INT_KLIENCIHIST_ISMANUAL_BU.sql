--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_KLIENCIHIST_ISMANUAL_BU FOR INT_KLIENCIHIST                
  ACTIVE BEFORE UPDATE POSITION 15 
as
begin
  --exception universal 'int_kli_test ' || coalesce(new.is_manual,'<pusty>') || coalesce(old.is_manual,'<pusty>');
  --ML obsluga pola is_manual ZG107762
  if(new.is_manual is distinct from old.is_manual)then
  begin
    if(new.is_manual not in(-1,0,1,2)) then
      exception universal 'Nieodzwolona wartosc IS_MANUAL';
    else if(new.is_manual = -1)then
    begin
      if(old.is_manual = 0) then
      begin
        --exception universal '0';
        new.is_manual = 0;
      end
      else if(
        ((select count(1) from nagfak nf where coalesce(nf.int_klienthist, 0) = new.ref)
          +(select count(1) from dokumnag dn where coalesce(dn.int_klienthist, 0) = new.ref)
          +(select count(1) from nagzam nz where coalesce(nz.int_klienthist, 0) = new.ref)
        ) > 1 --uzywane w wiecej niz jednym zamowieniu
      )then
      begin
        --exception universal '2';
        new.is_manual = 2;
      end
      else
      begin
         --exception universal '1';
        new.is_manual = 1;
      end

      --exception universal 'int_kli_test ' || coalesce(new.is_manual,'<pusty>');
    end

  end

end^
SET TERM ; ^
