--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMCONTRACTS_BU0 FOR PMCONTRACTS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
    if(coalesce(new.orgref,0) <> coalesce(old.orgref,0)) then begin
        select SYMBOL from pmcontracts where REF = new.orgref into new.orgsymbol;
    end
end^
SET TERM ; ^
