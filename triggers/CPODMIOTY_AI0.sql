--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AI0 FOR CPODMIOTY                      
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable slotyp varchar(15);
declare variable newref integer;
declare variable parentkli integer;
begin
  if(new.slopoz is null) then begin
    select TYp from SLODEF where ref = new.slodef into :slotyp;
    if(:slotyp='KLIENCI') then begin
      parentkli = null;
      if (new.parent is not null and new.parent <> 0) then
      begin
        select slopoz from cpodmioty where ref = new.parent and slodef = 1 into :parentkli;
        if (parentkli is null or parentkli = 0) then
          exception universal 'Błąd synchronizacji. Podmiot nadrzędny nie ma odpowiednika w kartotece klientów.';
      end
      execute procedure GEN_REF('KLIENCI') returning_values :newref;
      update CPODMIOTY set SLOPOZ = :newref where REF =new.ref;
      insert into KLIENCI(REF, AKTYWNY, FSKROT, NAZWA, ULICA, NRDOMU, NRLOKALU, NIP, MIASTO,
                          KODP, POCZTA, KRAJ, TELEFON, KOMORKA, FAX,WWW,
                          EMAIL, EMAILWINDYK, REGON, FIRMA, IMIE, NAZWISKO, CRM,
                          UWAGI, NIEWYPLAC, TYP, KODZEWN, KONTOFK,
                          DATAZAL, CPWOJ16M, COMPANY, REGION, cpowiat, parent)
                  values(:newref, new.aktywny, new.skrot,new.nazwa, new.ulica, new.nrdomu, new.nrlokalu, new.nip, new.miasto,
                          new.kodp, new.poczta, new.kraj, new.telefon, new.komorka, new.fax, new.www,
                          new.email, new.emailwindyk, new.regon, new.firma, new.imie, new.nazwisko, 1,
                          new.uwagi, new.niewyplac, new.typ, new.kodzewn, new.kontofk,
                          new.datazal, new.cpwoj16m, new.company, new.region, new.cpowiaty, :parentkli);
    end else if(:slotyp = 'PKLIENCI') then begin
      execute procedure GEN_REF('PKLIENCI') returning_values :newref;
      update CPODMIOTY set SLOPOZ = :newref where REF =new.ref;
      insert into PKLIENCI(REF, SKROT,NAZWA, ULICA, NRDOMU, NRLOKALU, MIASTO, KODP,
                           TELEFON, fax, www, email,crm,
                           zrodlo, forma, branza, status, uwagi)
                 values (:newref, new.skrot, new.nazwa, new.ulica, new.nrdomu, new.nrlokalu, new.miasto, new.kodp,
                        new.telefon, new.fax, new.www, new.email,1,
                        new.zrodlo, new.forma, new.branza, new.cstatus, new.uwagi);
    end else if(:slotyp = 'DOSTAWCY') then begin
      execute procedure GEN_REF('DOSTAWCY') returning_values :newref;
      update CPODMIOTY set SLOPOZ = :newref where REF =new.ref;
      insert into DOSTAWCY(REF, AKTYWNY, ID, NAZWA, ulica, nrdomu, nrlokalu, nip, miasto,
                           kodp, poczta, telefon, fax, www, email, regon,CRM, TYP, uwagi, kontofk, COMPANY)
                  values (:newref, new.aktywny, new.skrot, new.nazwa, new.ulica, new.nrdomu, new.nrlokalu, new.nip, new.miasto,
                          new.kodp, new.poczta, new.telefon, new.fax, new.www, new.email, new.regon,1,new.typ,new.uwagi, new.kontofk, new.company);
    end else if(:slotyp = 'SPRZEDAWCY') then begin
      execute procedure GEN_REF('SPRZEDAWCY') returning_values :newref;
      update CPODMIOTY set SLOPOZ = :newref where REF =new.ref;
      insert into SPRZEDAWCY(REF, SKROT, NAZWA,imie, nazwisko, adres, nrdomu, nrlokalu, nip, miasto,
                           kodpoczt, poczta, telefon,firma, email, akt ,CRM, KONTOFK)
                  values (:newref, new.SKROT, new.nazwa,new.imie, new.nazwisko,  new.ulica, new.nrdomu, new.nrlokalu, new.nip, new.miasto,
                          new.kodp, new.poczta, new.telefon,new.firma, new.email, 1,1, new.kontofk);
    end else if(:slotyp = 'ESYSTEM') then begin
      execute procedure GEN_REF('SLOPOZ') returning_values :newref;
      update CPODMIOTY set SLOPOZ = :newref where REF =new.ref;
      insert into SLOPOZ(REF, SLOWNIK, KOD, NAZWA, CRM, KONTOKS)
                  values(:newref, new.slodef, substring(new.skrot from 1 for 40), new.nazwa, 1, new.kontofk);  --XXX ZG133796 MKD
    end else exception CPODMIOTY_NOSLOPOZ;

  end else if (new.slodef is not null) then begin
    select TYP from SLODEF where ref = new.slodef into :slotyp;
    if(:slotyp='KLIENCI') then begin
      update KLIENCI set CRM=1
      where REF=new.slopoz;
    end else if(:slotyp = 'PKLIENCI') then begin
      update PKLIENCI set CRM=1
       where REF=new.slopoz;
    end else if(:slotyp = 'DOSTAWCY') then begin
      update DOSTAWCY set CRM=1
       where REF=new.slopoz;
    end else if(:slotyp = 'SPRZEDAWCY') then begin
      update SPRZEDAWCY set CRM=1
       where REF=new.slopoz;
    end else if(:slotyp = 'ESYSTEM') then begin
      update SLOPOZ set CRM=1
      where REF=new.slopoz;
    end else exception CPODMIOTY_NOSLOPOZ;
  end
end^
SET TERM ; ^
