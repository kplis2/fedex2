--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BI0_ELZAB FOR WERSJE                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable free_no integer;
declare variable curr_no integer;
declare variable is_last smallint;
begin
  --wyszukiawnie pierwszego wolnego numeru sposrod wersji aktywnych i bedacych towarami lub produktami
  if (new.usluga in (0,2) and exists(select symbol from urzsprtyp where symbol='ELZAB')) then begin
    is_last=1;
    free_no=1;
    for select cast(kodzewn as integer) from wersje  where usluga in (0,2) and akt=1 and kodzewn is not null and kodzewn<>''
      order by cast(kodzewn as integer) into :curr_no  do
    begin
      if (free_no<>curr_no) then begin
        new.kodzewn = cast(:free_no as varchar(20));
        is_last=0;
        break;
      end
      free_no = free_no + 1;
    end
    if (is_last=1) then new.kodzewn=free_no;
    --jezeli nr wersji jest 0 to nanies na kartoteke towarowa
    if (new.nrwersji=0 and new.usluga=0) then
      update towary set kodzewn=new.kodzewn where ktm=new.ktm;
  end
end^
SET TERM ; ^
