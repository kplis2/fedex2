--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDTYPEDEST4OP_BU0 FOR MWSORDTYPEDEST4OP              
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable supervisor smallint;
begin
  select coalesce(o.supervisor,0)
    from opermag o
    where o.operator = new.operator and o.magazyn = new.wh
    into supervisor;
  if (new.canchangemwsconstlocl <> old.canchangemwsconstlocl) then
  begin
    execute procedure mwsordtypedest4op_grants(coalesce(new.canchangemwsconstlocl,0),:supervisor)
      returning_values new.grants;
  end
end^
SET TERM ; ^
