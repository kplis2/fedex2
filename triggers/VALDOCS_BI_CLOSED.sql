--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_BI_CLOSED FOR VALDOCS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable period_status integer;
begin
  select status from amperiods where ref = new.amperiod into :period_status;
  if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można wystawić dokumentu dla zamkniętego okresu!';
end^
SET TERM ; ^
