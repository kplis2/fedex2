--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZESTP_AI0 FOR SPRZEDZESTP                    
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable typ char(1);
begin
  execute procedure SPRZEDZEST_OBL_NAG(new.zestawienie);
  select TYP from SPRZEDZEST where REF=new.zestawienie into :typ;
  if(:typ = 'Z')then begin
    if(new.zam > 0) then update NAGZAM set SPRZEDROZL = 1 where REF=new.zam;
    if(new.fak > 0) then update NAGFAK set SPRZEDROZL = 1 where REF=new.fak;
  end
end^
SET TERM ; ^
