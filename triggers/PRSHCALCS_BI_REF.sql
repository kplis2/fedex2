--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHCALCS_BI_REF FOR PRSHCALCS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRSHCALCS')
      returning_values new.REF;
end^
SET TERM ; ^
