--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERPOS_AU0 FOR BTRANSFERPOS                   
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slodef integer;
declare variable slopoz integer;
declare variable company companies_id;
begin
  if(new.amount <> old.amount) then
     update BTRANSFERS
      set AMOUNT = (select sum(AMOUNT)
                      from BTRANSFERPOS
                      where BTRANSFERPOS.btransfer = new.btransfer)
        where REF=new.btransfer;
  if(new.amount <> old.amount
     or (new.settlement <> old.settlement)
     or (new.account <> old.account)) then
  begin
    select SLODEF, SLOPOZ, company
      from BTRANSFERS
      where ref = new.btransfer
    into :slodef, :slopoz, :company;
    if((new.settlement <> old.settlement) or (new.account <> old.account)) then
      execute procedure rozrach_btran_count(:slodef, :slopoz, old.account,old.settlement,:company);
    execute procedure rozrach_btran_count(:slodef, :slopoz, new.account,new.settlement,:company);

  end
end^
SET TERM ; ^
