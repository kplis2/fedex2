--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_AD0 FOR PERSONS                        
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable dictdef integer;
begin
  for
    select ref from slodef
      where typ = 'PERSONS'
      into :dictdef
  do begin
    delete from bankaccounts
      where dictdef = :dictdef and dictpos = old.ref;
  end

  delete from dokzlacz d where d.ztable = 'PERSONS' and d.zdokum = old.ref;
end^
SET TERM ; ^
