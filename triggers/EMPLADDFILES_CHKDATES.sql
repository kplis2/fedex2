--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILES_CHKDATES FOR EMPLADDFILES                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable symbol varchar(10);
declare variable mdc integer;
declare variable mdused integer;
declare variable mdlimit integer;
declare variable minfromdate timestamp;
begin
  if (coalesce(new.VALIDITYDATE,new.fromdate) < new.FROMDATE) then
    EXCEPTION VALIDITYDATE_BIGER;
  if (coalesce(new.TODATE,new.fromdate) < new.FROMDATE) then
    EXCEPTION DATE_EXECUT_BIGER;


  execute procedure get_config('VACMDCOLUMN', 2) returning_values :mdc;
  select symbol from empladdfiletypes where ref = coalesce(new.filetype,old.filetype) into :symbol;
  if(:symbol = 'STD_OPKKOD' and (
      new.fromdate is distinct from old.fromdate
      or new.todate is distinct from old.todate
      or new.kind is distinct from old.kind )) then
  begin
    if(exists(select first 1 1
                from empladdfiles
                where employee = new.employee
                  and filetype = new.filetype
                  and ref <> new.ref
                  and (fromdate <= new.fromdate and todate >= new.fromdate
                  or fromdate <= new.todate and todate >= new.todate
                  or (todate is null and fromdate <= new.fromdate)
                  or (new.todate is null and fromdate >= new.fromdate)
                  or fromdate >= new.fromdate and todate <= new.todate )
                  )) then
      exception universal'Nie można założyć nakładających się wpisów.';
    if(not exists(select first 1 1
                from emplfamily f
                  join edictzuscodes z on z.ref = f.conecttype
                where employee = new.employee
                  and z.code in (11,21)
                  and new.fromdate between f.birthdate and dateadd(14 year to f.birthdate)
                  and coalesce(new.todate,dateadd(14 year to f.birthdate)) between f.birthdate and dateadd(14 year to f.birthdate))
    ) then
      exception universal'Pracownik nie posiada dziecka w wieku do 14 lat dla zadanego okresu';
    select min(fromdate)
      from employment
      where employee = new.employee
    into minfromdate;
    if(minfromdate > new.fromdate) then
      exception universal'Nie można założyć kartoteki dodatkowej przed pierwszą umową.';
    if(inserting) then
    begin
      select sum(workdays)
        from eabsences
        where ecolumn = :mdc
          and correction in (0,2)
          and employee = new.employee
          and fromdate >= extract(year from new.fromdate)||'/01/01'
          and (todate <= coalesce(extract(year from new.todate),extract(year from(new.fromdate)))||'/12/31' )
      into :mdused;
      mdlimit = new.kind;
      if(mdlimit = 3) then
        mdlimit = 0;
      if(mdused >= mdlimit) then
        exception universal'Wykorzystano już dni opieki w tym roku.';
    end
  end
end^
SET TERM ; ^
