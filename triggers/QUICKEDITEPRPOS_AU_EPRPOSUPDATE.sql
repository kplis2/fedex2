--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER QUICKEDITEPRPOS_AU_EPRPOSUPDATE FOR QUICKEDITEPRPOS                
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable ecolumn integer;
begin
  if (coalesce(new.pvalue,0) <> coalesce(old.pvalue,0)) then
  begin
    execute procedure get_global_param('QEDITEPRPOS_ECOLUMN')
      returning_values ecolumn;
    execute procedure update_eprpos_value(new.employee, :ecolumn, new.epayroll, new.pvalue);
  end
end^
SET TERM ; ^
