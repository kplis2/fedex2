--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_AD0 FOR LISTYWYSD                      
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable ildok integer;
declare variable ilspr integer;
declare variable waga numeric(15,4);
declare variable objetosc numeric(15,4);
declare variable koszt numeric(14,2);
begin
  if(old.refdok > 0) then
    update DOKUMNAG set WYSYLKA = NULL
      where WYSYLKA = old.listawys and REF=old.refdok and DOKUMNAG.akcept = 1;
  if(old.refzam > 0) then
    update NAGZAM set WYSYLKA = NULL
      where REF = old.refzam and WYSYLKA = old.listawys;
  if (old.refsrv > 0) then
    update srvrequests set WYSYLKA = NULL
      where srvrequests.ref = old.refsrv and srvrequests.wysylka=old.ref;
  delete from MSGORDS where TYPDOK='D' and REFDOK = old.ref;
  if(old.listawys is not null) then begin
    ildok = 0;
    select count(*) from listywysd where listawys =  old.listawys  into :ildok;
    if(:ildok is null) then ildok = 0;
    waga = 0;
    objetosc = 0;
    koszt = 0;
    select sum(waga),sum(kosztdost), sum(objetosc) from listywysd where listawys=old.listawys into :waga,:koszt,objetosc;
    if(:waga is null) then waga = 0;
    if(:koszt is null) then koszt = 0;
    if(:objetosc is null) then objetosc = 0;
    ilspr = 0;
    select count(*) from listywysd where listawys=old.listawys and wyschecked=1 into :ilspr;
    if(:ilspr is null) then ilspr = 0;
    update listywys set ildok = :ildok, waga = :waga, kosztdost = :koszt, ilspr = :ilspr, objetosc = :objetosc
      where ref=old.listawys;
  end
end^
SET TERM ; ^
