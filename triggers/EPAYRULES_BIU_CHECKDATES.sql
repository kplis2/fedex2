--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYRULES_BIU_CHECKDATES FOR EPAYRULES                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
AS
begin
  if (new.fromdate > new.todate) then
     exception data_do_musi_byc_wieksza;
end^
SET TERM ; ^
