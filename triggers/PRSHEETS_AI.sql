--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_AI FOR PRSHEETS                       
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if(new.status=2 and new.status=1) then begin
    update PRSHEETS set STATUS=0 where status = 2 and KTM=new.ktm and WERSJA=new.wersja and REF<>new.ref;
  end
  if (not exists (select ref from wersje w where w.ktm = new.ktm and w.ref = new.wersjaref)) then
    exception PRSHEETS_DATA 'Niezgodna wersja';
end^
SET TERM ; ^
