--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_BD_ROZLICZ FOR PMPOSITIONS                    
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.usedval > 0) then
    exception PMPOSITION_ROZLICZ;
end^
SET TERM ; ^
