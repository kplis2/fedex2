--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCR_AU0 FOR PROMOCR                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable wart DECIMAL(14,2);
declare variable il DECIMAL(14,2);
declare variable rab DECIMAL(14,2);
begin
  select min(wartosc),min(ilosc) from promocr where promocja = new.promocja into :wart,:il;
  select min(procent) from promocr where promocja = new.promocja and (ilosc=0 or (ilosc is null)) and (wartosc=0 or (wartosc is null)) into :rab;
  if(:wart is null) then wart = 0;
  if(:il is null) then il = 0;
  if(:rab is null) then rab = 0;
  update PROMOCJE set WARTOSC=:wart,MINILOSC=:il,PROCENTRAB=:rab where ref=new.promocja;
end^
SET TERM ; ^
