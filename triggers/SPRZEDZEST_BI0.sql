--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZEST_BI0 FOR SPRZEDZEST                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable okres varchar(6);
begin
  if(new.data is null) then new.data = current_date;
  okres = cast( extract( year from new.data) as char(4));
  if(extract(month from new.data) < 10) then
      okres = :okres ||'0' ||cast(extract(month from new.data) as char(1));
  else
      okres = :okres ||cast(extract(month from new.data) as char(2));
  if(new.okres is null or (new.okres = '      ')) then
   new.okres = :okres;
  else begin
    if(new.okres <> :okres) then exception SPRZEDZEST_DATA_NIE_W_OKRESIE;
  end
  if(new.status is null or (new.status = '')) then new.status = 'O';
  if(new.dowyplaty is null) then new.dowyplaty = 0;
  if(new.wyplacono is null) then new.wyplacono = 0;
  if(new.typ is null or (new.typ = '')) then begin
    select sprzeddef.typ from sprzednag join sprzeddef on (sprzednag.sprzeddef = sprzeddef.ref)
     where sprzednag.ref = new.sprzednag into new.typ;
  end
  if(new.typ is null or (new.typ = '')) then begin
    new.typ = 'Z';
  end
end^
SET TERM ; ^
