--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CNOTATKI_AU0 FOR CNOTATKI                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.ckontrakt<>old.ckontrakt or (new.ckontrakt is not null and old.ckontrakt is null) or (new.ckontrakt is null and old.ckontrakt is not null)) then begin
    if(old.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYNOT(old.ckontrakt);
    if(new.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYNOT(new.ckontrakt);
  end
end^
SET TERM ; ^
