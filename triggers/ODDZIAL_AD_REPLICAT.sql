--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODDZIAL_AD_REPLICAT FOR ODDZIALY                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('ODDZIALY', old.oddzial, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
