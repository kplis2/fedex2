--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFORMY_BD0 FOR DEFFORMY                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
begin
  select count(*) from DEFDOKWIT where forma = old.symbol into :cnt;
  if(cnt is null) then cnt = 0;
  /* proba usuniecia formularza zwiazanego z definicja dokumentu witryny*/
  if(cnt > 0) then exception DEFFORMY_WITH_DEFDOKWIT;

end^
SET TERM ; ^
