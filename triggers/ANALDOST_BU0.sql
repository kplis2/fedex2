--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ANALDOST_BU0 FOR ANALDOST                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable okres varchar(6);
begin
  if(new.data <> old.data or (old.data is null) or (old.okres is null)) then begin
    if(new.data is null) then new.data = current_date;
    okres = cast( extract( year from new.data) as char(4));
    if(extract(month from new.data) < 10) then
        okres = :okres ||'0' ||cast(extract(month from new.data) as char(1));
    else
       okres = :okres ||cast(extract(month from new.data) as char(2));
    new.okres = :okres;
  end
end^
SET TERM ; ^
