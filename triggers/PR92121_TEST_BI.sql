--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PR92121_TEST_BI FOR PR92121_TEST                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_pr92121,1);
end^
SET TERM ; ^
