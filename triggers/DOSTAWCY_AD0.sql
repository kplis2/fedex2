--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_AD0 FOR DOSTAWCY                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable slodefref integer;
begin
  select min(slodef.ref) from slodef where slodef.typ = 'DOSTAWCY' into :slodefref;
  if(old.crm = 1) then begin
    if(:slodefref is null) then exception SLODEF_BEZDOSTAWCY;
    delete from CPODMIOTY where SLODEF = :slodefref AND SLOPOZ = old.ref;
  end
  delete from bankaccounts where DICTDEF = :slodefref and DICTPOS = old.ref;
end^
SET TERM ; ^
