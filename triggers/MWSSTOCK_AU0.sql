--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_AU0 FOR MWSSTOCK                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable autogoodsrefill smallint;
declare variable quantity numeric(14,4);
begin
  -- usuwanie stanow zerowych
  if (new.quantity = 0 and new.reserved = 0 and new.blocked = 0 and new.ordsuspended = 0
      and new.suspended = 0 and new.ordered = 0
  ) then
    -- mozemy skasowac stan na lokacji tylko gdy nic na nim nie ma i nie ma operacji na ten stan
      delete from mwsstock where ref = new.ref;
  -- zweryfikowanie stanyu arkusza inwentaryzacyjnego
  if (new.quantity <> old.quantity and new.locdest = 6) then
    execute procedure mws_check_inwenta_status(new.ref, new.good, new.vers, new.lot, new.wh);
  if (new.actloc <> old.actloc) then
    execute procedure xk_mws_recalc_sellav_for_stock (new.vers,new.wh);
end^
SET TERM ; ^
