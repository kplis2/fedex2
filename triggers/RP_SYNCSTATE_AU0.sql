--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_SYNCSTATE_AU0 FOR RP_SYNCSTATE                   
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.tabela <> old.tabela)
    or (new.fromlocat <> old.fromlocat)
    or (new.tolocat <> old.tolocat)
    or (new.tolocat2 <> old.tolocat2)
  ) then
    update RP_SYNCDEF set STATE = -1 where TABELA = new.tabela;
  if(new.tabela <> old.tabela) then
    update RP_SYNCDEF set STATE = -1 where TABELA = old.tabela;
end^
SET TERM ; ^
