--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENNIK_BU0 FOR DEFCENNIK                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable il integer;
declare variable cnt integer;
declare variable walpln varchar(5);
begin
   if (coalesce(new.symbol,'') <> coalesce(old.symbol, '') or
       coalesce(new.oddzial,'') <> coalesce(old.oddzial, '') or
       coalesce(new.akt, 0) <> coalesce(old.akt, 0)) then begin
     if (coalesce(new.symbol, '') in ('A','B') and
         exists(select first 1 1 from defcennik where
                  coalesce(symbol, '') = coalesce(new.symbol, '') and
                  coalesce(oddzial, '') = coalesce(new.oddzial, '') and
                  ref <> new.ref and AKT = new.akt)) then exception cenniki_symbol;
  end

  if (new.rabatstalacena is null) then new.rabatstalacena = 0;
  select count(*) from GRUPYKLI where cennik = new.ref into :il;
  if(:il > 0 AND new.akt = 0) then exception CENNIK_CON_GRUPYKLI;
  if(NEW.datado is not null and new.datado is not null) then
    if(new.datado < new.dataod) then
       exception DEFCENNIK_DATAODDO;
  if(new.waluta <> old.waluta or (new.waluta is null and old.waluta is not null) or (new.waluta is not null and old.waluta is null)) then begin
    select count(*) from CENNIK where CENNIK = new.REF into :cnt;
    if(:cnt > 0) then
      exception DEFCENNIK_WALHASPOS;
  end
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln is null) then new.waluta = 'PLN';
  if(new.waluta is null) then begin
     new.waluta = :walpln;
  end
  if(new.waluta = :walpln or (new.kurs is null)) then new.kurs = 1;
  if(new.marza is null) then new.marza = 0;
  if(new.oddzial='') then new.oddzial = NULL;
  if(new.kolejnosc is null or new.kolejnosc = 0) then new.kolejnosc = 1;
  if(new.waluta is null) then begin
     execute procedure GETCONFIG('WALPLN') returning_values new.waluta;
      if( new.waluta is null) then new.waluta = 'PLN';
  end
end^
SET TERM ; ^
