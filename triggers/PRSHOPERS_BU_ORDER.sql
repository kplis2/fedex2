--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_BU_ORDER FOR PRSHOPERS                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.number is null) then new.number = old.number;
  if (new.ord = 1 or (new.number = old.number and coalesce(old.opermaster,0) = coalesce(new.opermaster,0))) then
  begin
    new.ord = 0;
    exit;
  end
  if (new.number <> old.number and coalesce(old.opermaster,0) = coalesce(new.opermaster,0)) then
    select max(number)
      from prshopers
      where sheet = new.sheet
        and coalesce(opermaster, 0) = coalesce(old.opermaster, 0)
        and ref <> new.ref
      into :maxnum;
  else if (coalesce(old.opermaster,0) <> coalesce(new.opermaster,0)) then
  begin
    select max(number)
      from prshopers
      where sheet = new.sheet
        and coalesce(opermaster, 0) = coalesce(new.opermaster, 0)
        and ref <> new.ref
      into :maxnum;
  end else
    maxnum = 0;

  maxnum = coalesce(maxnum,0);

  if (coalesce(old.opermaster,0) <> coalesce(new.opermaster,0)) then
  begin
    update prshopers set ord = 1, number = number - 1
      where sheet = new.sheet
        and ref <> new.ref and number > old.number
        and coalesce(opermaster, 0) = coalesce(old.opermaster, 0);
    if (new.number > :maxnum) then
      new.number = :maxnum + 1;
    else
      update prshopers o set o.number = o.number + 1, o.ord = 1
        where o.sheet = new.sheet and o.number >= new.number
          and coalesce(o.opermaster,0) = coalesce(new.opermaster,0)
          and o.ref <> new.ref
        order by o.number;
  end else if ((new.number > old.number and new.number > maxnum) or (new.number < 1 and old.number > 0)) then
    new.number = old.number;
  else if (new.number < old.number) then
  begin
    update prshopers set ord = 1, number = number + 1
      where ref <> new.ref and number >= new.number
        and number < old.number and sheet = new.sheet
        and coalesce(opermaster, 0) = coalesce(old.opermaster, 0);
  end else if (new.number > old.number and new.number <= maxnum) then
  begin
    update prshopers set ord = 1, number = number - 1
      where ref <> new.ref and number <= new.number
        and number > old.number and sheet = new.sheet
        and coalesce(opermaster, 0) = coalesce(old.opermaster, 0);
  end
  if (new.number is null) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
