--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZNAG_BU_CHECKPMG FOR REZNAG                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
begin
  if (old.pmelement is null and new.pmelement is not null) then
  begin
    select count(reznag) from rezpoz where reznag = new.ref into cnt;
    if (cnt > 1) then
      exception REZNAG_WRONGROZLICZ;
  end
end^
SET TERM ; ^
