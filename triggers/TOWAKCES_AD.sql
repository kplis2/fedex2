--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCES_AD FOR TOWAKCES                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.doubleakc = 1 and old.token <> 7) then begin
    delete from towakces where towakces.ktm = old.aktm and towakces.aktm=old.ktm and towakces.doubleakc < 2;
    update towakces set towakces.doubleakc = 1 where towakces.ktm = old.aktm
        and towakces.doubleakc = 2;
  end
end^
SET TERM ; ^
