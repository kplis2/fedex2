--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLUMNS_BIU_BLANK FOR ECOLUMNS                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.emplcard is null) then new.emplcard = 0;
  if (new.colour is null) then new.colour = 0;
  if (new.ehrm is null) then new.ehrm = 0;
end^
SET TERM ; ^
