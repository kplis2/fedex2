--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOKLI_BU0 FOR PROMOKLI                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if((new.grupakli <> old.grupakli)
     or (new.grupakli is not null and old.grupakli is null)
     or (old.nazwa is null and new.grupakli > 0)) then
    select grupykli.opis from GRUPYKLI where grupykli.ref = new.grupakli into new.nazwa;
  else if((new.klient <> old.klient)
      or (new.klient is not null and old.klient is null)
      or (old.nazwa is null and new.klient > 0)) then
    select klienci.nazwa from KLIENCI where ref=new.klient into new.nazwa;

-- BS34153
  if (coalesce(new.klient, 0) <> coalesce(old.klient, 0) and coalesce(new.klient, 0) > 0) then
    if (exists(select first 1 1 from promokli where klient = new.klient and promocja = new.promocja and ref != new.ref)) then
      exception universal 'Ten klient jest już na liscie uprawnionych';

  if (coalesce(new.grupakli, 0) <> coalesce(old.grupakli, 0) and coalesce(new.grupakli, 0) > 0) then
    if (exists(select first 1 1 from promokli where grupakli = new.grupakli and promocja = new.promocja and ref != new.ref)) then
      exception universal 'Ta grupa klientów jest już na liscie uprawnionych';
-- end
end^
SET TERM ; ^
