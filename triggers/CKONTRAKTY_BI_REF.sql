--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BI_REF FOR CKONTRAKTY                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CKONTRAKTY')
      returning_values new.REF;
end^
SET TERM ; ^
