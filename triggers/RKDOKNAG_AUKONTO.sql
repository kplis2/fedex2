--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AUKONTO FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  --odtworzenie konta na pozycjach dokumentu kasowego jeli zmienil sie podmiot na naglowku
  if(coalesce(new.slodef,0) <> coalesce(old.slodef,0)
    or coalesce(new.slopoz,0) <> coalesce(old.slopoz,0)
  ) then
    update RKDOKPOZ set KONTO = null where DOKUMENT = new.ref;
end^
SET TERM ; ^
