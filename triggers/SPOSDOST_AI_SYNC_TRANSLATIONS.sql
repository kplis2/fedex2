--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOST_AI_SYNC_TRANSLATIONS FOR SPOSDOST                       
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  execute procedure SYNC_SYS_TRANSLATIONS('SPOSDOST', new.ref, new.nazwa);
end^
SET TERM ; ^
