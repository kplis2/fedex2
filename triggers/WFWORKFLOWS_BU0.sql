--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFWORKFLOWS_BU0 FOR WFWORKFLOWS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.status is null) then new.status = 0;
  if(new.status <> old.status)then begin
    if(new.status = 3) then new.enddate = current_timestamp(0);
    else new.enddate = null;
  end
  if(replace(coalesce(new.symbol,''), ' ', '') = '') then exception universal 'Wprowadź symbol obiegu dokumentów';
  if(new.contrtype is not null and old.contrtype is null) then begin
    select phase from contrtypes where ref = new.contrtype into new.faza;
    if(new.faza is null) then exception universal 'Brak wskazania fazy domyślnej na typie obiegu dokumentów';
  end
end^
SET TERM ; ^
