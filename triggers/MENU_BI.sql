--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MENU_BI FOR MENU                           
  ACTIVE BEFORE INSERT POSITION 1 
AS
BEGIN
  new.numergl = 0;
  if(new.rodzic is not null) then select POZIOM + 1 from MENU where REF=new.rodzic into new.poziom;
  if (new.poziom is null) then new.poziom = 0;
  execute procedure REPLICAT_STATE('MENU',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
END^
SET TERM ; ^
