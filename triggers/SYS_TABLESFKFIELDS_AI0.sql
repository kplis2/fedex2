--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SYS_TABLESFKFIELDS_AI0 FOR SYS_TABLESFKFIELDS             
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.null_flag = 1) then
    update sys_tablesfk set null_flag = 1 where fk_name = new.fk_name;
end^
SET TERM ; ^
