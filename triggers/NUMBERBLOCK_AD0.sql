--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERBLOCK_AD0 FOR NUMBERBLOCK                    
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if(old.dokdokumnag > 0) then
    update DOKUMNAG set NUMBLOCK = null where REF=old.dokdokumnag;
  if(old.doknagfak > 0) then
    update NAGFAK set NUMBLOCK = null where REF=old.doknagfak;
end^
SET TERM ; ^
