--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_BD FOR PRSCHEDOPERS                   
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable prschedoper integer;
declare variable amountin numeric(14,4);

begin
  if(old.status > 1) then
  begin
    if (exists (select first 1 1 from propersraps r where r.prschedoper = old.ref)) then
      exception prschedopers_error substring('Rozpoczęta realizacja operacji '''||old.oper||'''.Usunięcie niemożliwe' from 1 for 70);
    if (exists (select first 1 1 from prshortages s where s.prschedoper = old.ref)) then
      exception prschedopers_error substring('Zarejestrowano już braki do operacji '''||old.oper||'''.Usunięcie niemożliwe' from 1 for 70);
  end
  if(old.amountresult <> 0) then begin
    for select psod.depto, pso.amountin
     from prschedoperdeps psod
     left join prschedopers pso on (pso.ref = psod.depto)
     where psod.depfrom = old.ref
     into :prschedoper, :amountin
    do begin
      if(amountin = old.amountresult) then
        execute procedure prschedoper_amountin(:prschedoper);
    end
  end
  delete from prschedoperdeps where DEPTO = old.ref;
  delete from prschedoperdeps where DEPFROM = old.ref;
  if(old.shoper is null) then
    update prschedopers set opermanadded = null where opermanadded = old.ref;
end^
SET TERM ; ^
