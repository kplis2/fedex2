--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAKUSL_BUID FOR POZFAKUSL                      
  ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 0 
as
begin
  if ((updating and new.rozliczono = old.rozliczono) and
      exists (select first 1 1 from pozfak p left join nagfak n on (p.dokument = n.ref)
                where p.ref = case
                                when deleting then old.refpozfak
                                else new.refpozfak
                              end
                  and n.akceptacja = 1)) then
    exception POZFAKUSL_ROZLICZONO 'Faktura jest już zaakceptowana, modyfikacja pozycji kosztowych niemożliwa!';
end^
SET TERM ; ^
