--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTA_BD_ORDER FOR INWENTA                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update inwenta set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and magazyn = old.magazyn;
end^
SET TERM ; ^
