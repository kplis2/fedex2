--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BIU_CHECKTODATE FOR EMPLCONTRACTS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
declare variable timelimit smallint;
begin
--MWr: Kontrola 'Daty do'

  if (new.econtrtype <> old.econtrtype or (new.econtrtype is null and old.econtrtype is not null) or (new.econtrtype is not null and old.econtrtype is null) or
      new.enddate <> old.enddate or (new.enddate is null and old.enddate is not null) or (new.enddate is not null and old.enddate is null) or
      new.todate <> old.todate or (new.todate is null and old.todate is not null) or (new.todate is not null and old.todate is null)
  ) then begin

    select timelimit from econtrtypes
      where new.econtrtype = contrtype
      into :timelimit;

    if (timelimit = 1) then
    begin
      if (new.todate is null) then
        exception timelimitcontr_todate_required;
      else if (new.todate < new.enddate) then
        exception enddate_after_todate;
    end
  end
end^
SET TERM ; ^
