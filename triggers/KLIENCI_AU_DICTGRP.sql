--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AU_DICTGRP FOR KLIENCI                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
  declare variable dictdef integer;
begin
  if ((old.dictgrp is null and new.dictgrp is not null) or
      (old.dictgrp is not null and new.dictgrp is not null and new.dictgrp <> old.dictgrp)) then begin
    select first 1 ref from slodef where typ='KLIENCI' into :dictdef;
    update rozrach set dictgrp=new.dictgrp where slodef=:dictdef and slopoz = new.ref;
    select first 1 ref from slodef where typ='DOSTAWCY' into :dictdef;
    update rozrach set dictgrp=new.dictgrp where slodef=:dictdef and slopoz = new.dostawca;
  end
  else if (new.dictgrp is null and old.dictgrp is not null) then begin
    select first 1 ref from slodef where typ='KLIENCI' into :dictdef;
    update rozrach set dictgrp=new.dictgrp where slodef=:dictdef and slopoz = new.ref;
    select first 1 ref from slodef where typ='DOSTAWCY' into :dictdef;
    update rozrach set dictgrp=new.dictgrp where slodef=:dictdef and slopoz = old.dostawca;
  end
end^
SET TERM ; ^
