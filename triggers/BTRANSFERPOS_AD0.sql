--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERPOS_AD0 FOR BTRANSFERPOS                   
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable slodef integer;
declare variable slopoz integer;
declare variable company companies_id;
begin
  if (old.amount <> 0) then
  begin
    update BTRANSFERS
      set amount = (select sum(amount)
                      from BTRANSFERPOS
                      where BTRANSFERPOS.btransfer = old.btransfer)
      where ref = old.btransfer;
  end

  if (old.amount <> 0) then
  begin
    select slodef, slopoz, company
      from btransfers
      where ref = old.btransfer
    into :slodef, :slopoz, :company;
    execute procedure rozrach_btran_count(:slodef, slopoz, old.account, old.settlement, :company);
  end
end^
SET TERM ; ^
