--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLITRAS_BI0 FOR KLITRAS                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable cnt integer;
begin
  select count(*) from KLITRAS where KLIENT = new.klient into :cnt;
  if(:cnt is null) then cnt = 0;
  if(:cnt = 0) then new.glowna = 1;
end^
SET TERM ; ^
