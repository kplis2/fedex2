--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_AI_ETRAININGS FOR EMPLTRAININGS                  
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable enumber numeric(14,2);
  declare variable costtype smallint;
  declare variable personcost numeric(14,2);
  declare variable totalcost numeric(14,2);
begin
  select count(ref)
    from empltrainings
    where training = new.training
    into :enumber;

  enumber = coalesce(:enumber, 0);

  select costtype, personcost, totalcost
    from etrainings
    where ref = new.training
    into :costtype, :personcost, :totalcost;

  if (costtype = 0) then
    totalcost = personcost * enumber;
  else if (costtype = 1 and enumber > 0) then
    personcost = totalcost / enumber;
  else if (costtype = 1 and enumber = 0) then
    personcost = 0;

  if (costtype is not null) then
    update etrainings set personcost = :personcost, totalcost = :totalcost where ref = new.training;
end^
SET TERM ; ^
