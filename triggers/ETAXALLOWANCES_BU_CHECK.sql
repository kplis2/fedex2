--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETAXALLOWANCES_BU_CHECK FOR ETAXALLOWANCES                 
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.incallowance > 0 and new.taxallowance > 0) then
    exception universal 'Nie można otrzymać jednocześnie zwolnienia od dochodu i ulgi podatkowej';
end^
SET TERM ; ^
