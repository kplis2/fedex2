--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AUTOREGSCHEME_BI_REF FOR AUTOREGSCHEME                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('AUTOREGSCHEME')
      returning_values new.REF;
end^
SET TERM ; ^
