--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDEFKATALP_BU_ORDER FOR CDEFKATALP                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 1;
  if (new.numer is null) then new.numer = old.numer;
  if (new.ord = 0 or new.numer = old.numer) then
  begin
    new.ord = 1;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.numer <> old.numer) then
    select max(numer) from cdefkatalp where cdefkatal = new.cdefkatal
      into :maxnum;
  else
    maxnum = 0;
  if (new.numer < old.numer) then
  begin
    update cdefkatalp set ord = 0, numer = numer + 1
      where akronim <> new.akronim and numer >= new.numer
        and numer < old.numer and cdefkatal = new.cdefkatal;
  end else if (new.numer > old.numer and new.numer <= maxnum) then
  begin
    update cdefkatalp set ord = 0, numer = numer - 1
      where akronim <> new.akronim and numer <= new.numer
        and numer > old.numer and cdefkatal = new.cdefkatal;
  end else if (new.numer > old.numer and new.numer > maxnum) then
    new.numer = old.numer;
end^
SET TERM ; ^
