--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CINOUTS_BI FOR CINOUTS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CINOUTS')
      returning_values new.REF;

  if(new.inweight is null) then new.inweight = 0;
  if(new.outweight is null) then new.outweight = 0;
  if(new.docweight is null) then new.docweight = 0;
  if(new.outweight<>0 and new.docweight<>0) then new.diffweight = new.outweight-new.inweight-new.docweight;
  else new.diffweight = 0;
END^
SET TERM ; ^
