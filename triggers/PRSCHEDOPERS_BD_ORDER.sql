--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_BD_ORDER FOR PRSCHEDOPERS                   
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.ord = 0) then exit;
  update prschedopers set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number
      and coalesce(guide, null, 0) = coalesce(old.guide, null, 0);
end^
SET TERM ; ^
