--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALDAYS_AU_EMPLCALDAYS FOR ECALDAYS                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.daykind <> old.daykind or new.descript <> old.descript
    or new.workstart <> old.workstart or new.workend <> old.workend
    or new.workstart2 <> old.workstart2 or new.workend2 <> old.workend2
  ) then begin
    update emplcaldays
      set daykind = new.daykind, descript = new.descript,
          workstart = new.workstart, workend = new.workend,
          workstart2 = new.workstart2, workend2 = new.workend2,
          nomworktime = new.worktime
      where ecalday = new.ref and autosync = 1;
  end
end^
SET TERM ; ^
