--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERPARAM_AI0 FOR DEFOPERPARAM                   
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable aa integer;
begin
  for select ref from OPERATOR order by ref into :aa do begin
    insert into OPERPARAM(OPERATOR,NAZWA,WARTOSC, NUMER) values(:aa,new.akronim,new.wartosc,new.numer);
  end
end^
SET TERM ; ^
