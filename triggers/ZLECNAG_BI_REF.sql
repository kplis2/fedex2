--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZLECNAG_BI_REF FOR ZLECNAG                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ZLECNAG')
      returning_values new.REF;
end^
SET TERM ; ^
