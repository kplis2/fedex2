--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCPOS_BI0 FOR PRTOOLSDOCPOS                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (coalesce(new.prtool,'') = '') then
    new.prtool = new.prtoolstype;
  if (exists (select first 1 1 from prtoolsdocs d where d.ref = new.prtoolsdoc and d.akcept <> 0)) then
    exception prtoolsdocs_error 'Nie można dodawać pozycji do zaakceptowanego dokumentu';
end^
SET TERM ; ^
