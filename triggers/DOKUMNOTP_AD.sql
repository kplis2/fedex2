--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOTP_AD FOR DOKUMNOTP                      
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable local integer;
begin
  execute procedure check_local('DOKUMNOT',old.dokument) returning_values :local;
  if(:local > 0) then begin
    update DOKUMNOTP set WARTOSC =
      cast(DOKUMNOTP.CENANEW*(select DOKUMROZ.ILOSC from DOKUMROZ where DOKUMROZ.REF=DOKUMNOTP.dokumrozkor) as numeric(14,2))
      - cast(DOKUMNOTP.CENAOLD*(select DOKUMROZ.ILOSC from DOKUMROZ where DOKUMROZ.REF=DOKUMNOTP.dokumrozkor) as numeric(14,2))
    where DOKUMENT=old.dokument;
    update DOKUMNOT set WARTOSC = (select sum(WARTOSC) from DOKUMNOTP where DOKUMNOTP.dokument = old.dokument)
    where REF=old.dokument;
  end
end^
SET TERM ; ^
