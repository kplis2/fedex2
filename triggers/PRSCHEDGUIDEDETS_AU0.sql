--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_AU0 FOR PRSCHEDGUIDEDETS               
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if ((new.quantity <> old.quantity or new.overlimit <> old.overlimit) and new.out in (1,3) and coalesce(new.sourceupdate,0) = 0) then
    EXECUTE PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(new.prschedguidepos);
  else if ((new.quantity <> old.quantity or new.shortage <> old.shortage
        or new.byproduct <> old.byproduct or new.overlimit <> old.overlimit
        or new.accept <> old.accept)
        and new.out = 0 and new.prschedguide is not null and new.prschedoper is not null
  ) then
  begin
    -- sprawdzenie czy nie probujemy przyjac wiecej niz na operacji
    execute procedure PRSCHEDOPER_CHECK_AMOUNTZREAL(new.prschedoper);
    -- wyliczenie ilosci zrealizowanej
    EXECUTE PROCEDURE PRSCHEDGUIDE_AMOUNTZREAL(new.prschedguide);
    if (new.prshortage is not null) then
      EXECUTE PROCEDURE PRSHORTAGE_AMOUNTZREAL(new.prshortage);
  end
  if(new.out = 1 and new.quantity > 0
      and ((new.accept > 0 and old.accept = 0) or (new.prschedguide <> old.prschedguide))
  ) then
    execute procedure prschedguides_statuschange(new.prschedguide,1,null);
  if(old.out = 1 and new.accept = 0 and old.accept > 0 and old.quantity > 0) then
  begin
    if (not exists(select first 1 1 from prschedguidedets d where d.out = 1 and d.accept > 0
        and d.quantity > 0 and d.prschedguide = old.prschedguide)
    ) then
      execute procedure prschedguides_statuschange(old.prschedguide,0,null);
  end
  if (new.prschedguidepos <> old.prschedguidepos) then
  begin
    EXECUTE PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(new.prschedguidepos);
    EXECUTE PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(old.prschedguidepos);
  end
  if (old.ssource = 1 and new.quantity <> old.quantity
      and exists(
          select first 1 1 from prschedguidedets d
            where d.stable = old.stable and d.ref = old.sref and d.quantity <> new.quantity
      )
  ) then
    execute procedure prschedguidedet_change(2,'PRSCHEDGUIDEDETS',old.ref,old.prschedguidepos);
  if (new.accept <> old.accept and new.ssource = 1
      and exists(select first 1 1 from prschedguidedets d where d.ssource = new.ssource and d.ref = new.sref and d.accept <> new.accept)
  ) then
      update prschedguidedets d set d.accept = new.accept where ref = new.sref;
end^
SET TERM ; ^
