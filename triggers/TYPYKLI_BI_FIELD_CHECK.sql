--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPYKLI_BI_FIELD_CHECK FOR TYPYKLI                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (coalesce(new.opis,'') = '') then
  begin
    exception universal 'Podaj opis kontrahenta.';
  end
  if (coalesce(new.slodefs,'') = '') then
  begin
    exception universal 'Wybierz grupe kontrahenta.';
  end
end^
SET TERM ; ^
