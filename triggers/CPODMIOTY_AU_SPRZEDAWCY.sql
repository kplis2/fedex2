--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_SPRZEDAWCY FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 8 
as
declare variable slodef integer;
declare variable crm smallint;
begin
  select ref from SLODEF where TYP = 'SPRZEDAWCY' into :slodef;
  if(:slodef = new.slodef and
      (new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.ulica <> old.ulica or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
      new.nip <> old.nip or (new.nip is not null and old.nip is null) or (new.nip is null and old.nip is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.kodp <> old.kodp or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
      new.poczta <> old.poczta or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.firma <> old.firma or (new.firma is not null and old.firma is null) or (new.firma is null and old.firma is not null) or
      new.skrot <> old.skrot or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
      new.imie <> old.imie or (new.imie is not null and old.imie is null) or (new.imie is null and old.imie is not null) or
      new.nazwisko <> old.nazwisko or (new.nazwisko is not null and old.nazwisko is null) or (new.nazwisko is null and old.nazwisko is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null)
      )
   ) then begin
        update SPRZEDAWCY set NAZWA = new.nazwa, SKROT = new.skrot, IMIE = new.imie, NAZWISKO = new.nazwisko, FIRMA = new.firma,
                      ADRES = new.ulica, NIP = new.nip, MIASTO = new.miasto,
                      KODPOCZT = new.kodp, POCZTA = new.poczta,
                      TELEFON = new.telefon, EMAIL = new.email, KONTOFK = new.kontofk
          where REF=new.slopoz;
  end
  if (new.slodef=:slodef) then begin
    select CRM from SPRZEDAWCY where REF=new.slopoz into :crm;
    if(:crm='0') then begin
      update SPRZEDAWCY set CRM=1
      where REF=new.slopoz;
    end
  end
end^
SET TERM ; ^
