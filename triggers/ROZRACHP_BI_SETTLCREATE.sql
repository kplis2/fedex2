--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_BI_SETTLCREATE FOR ROZRACHP                       
  ACTIVE BEFORE INSERT POSITION 3 
as
begin
  if(new.settlcreate = 1) then
    if(exists(select ref from ROZRACHP where SLODEF = new.slodef and SLOPOZ = new.slopoz and KONTOFK = new.kontofk and SYMBFAK = new.symbfak and SETTLCREATE =1 )) then
      exception universal 'Rozrachunek o symbolu '||new.symbfak||' juz istnieje dla tego kontrah.';
end^
SET TERM ; ^
