--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_BU_WPK FOR BKACCOUNTS                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  --Odpiąlem synchronizacje na roku księgowym, przez co ospiey si synchronziacje na koncie syntetycznym
  -- teraz, żeby nie synchronizway sie poziomy analityki, też należy je odpiąć.
  if (coalesce(new.pattern_ref,0) = 0 and coalesce(old.pattern_ref,0) > 0 ) then
  begin
    update accstructure a
      set a.pattern_ref = null
      where a.bkaccount = new.ref;
  end else if (new.pattern_ref is not null ) then
  begin
    --standardowy update usera
    if (coalesce(new.internalupdate,0)=0) then
    begin
      -- jeżeli podlega synchronizacji to zawsze synchronizuj ze wzorcem, mimo, że idzie jakis update
      select symbol, descript, bktype, saldotype, settl, multicurr, sttltype,
        prd, multiprd, rights, rightsgroup, doopisu
        from bkaccounts
        where ref = new.pattern_ref
      into new.symbol, new.descript, new.bktype, new.saldotype, new.settl, new.multicurr, new.sttltype,
        new.prd, new.multiprd, new.rights, new.rightsgroup, new.doopisu;
    end else
    begin
      -- teraz przypadek jak internalupdate = 1 zatem update wewnetrzny, pewnie z poziomu zakladu wzorcowego
      -- zatem zakladam, ze wartosci sa ok, tylko odznaczam internal, zeby kolejna proba nadpisania przy synchronizacji
      -- sie nie udala
      new.internalupdate = 0;
    end
  end
end^
SET TERM ; ^
