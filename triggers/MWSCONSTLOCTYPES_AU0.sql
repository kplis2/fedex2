--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCTYPES_AU0 FOR MWSCONSTLOCTYPES               
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if ((new.locdest is not null and old.locdest is null)
       or (new.locdest <> old.locdest and new.locdest is not null and old.locdest is not null)
  ) then
    update mwsconstlocs set locdest = new.locdest where mwsconstloctype = new.ref;
end^
SET TERM ; ^
