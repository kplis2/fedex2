--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AU_OPERACJA FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 10 
AS
declare variable pozoperref integer;
begin
  if(coalesce(new.operacja,'') <> coalesce(old.operacja,'')) then begin
    select RKDEFOPER.dompozoper from RKDEFOPER where RKDEFOPER.symbol=new.operacja into :pozoperref;
    if(:pozoperref is null) then
      select min(RKPOZOPER.ref) from RKPOZOPER where RKPOZOPER.operacja = new.operacja into :pozoperref;
    update RKDOKPOZ set RKDOKPOZ.pozoper = :pozoperref where rkdokpoz.dokument = new.ref;
  end
end^
SET TERM ; ^
