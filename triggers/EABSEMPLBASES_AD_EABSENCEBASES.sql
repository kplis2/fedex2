--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSEMPLBASES_AD_EABSENCEBASES FOR EABSEMPLBASES                  
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
--MWr Usuwa z tabeli rozliczenia nieobecnosci nierozliczonych delete'owana podstawe

  delete from eabsencebases b
  where b.employee = old.employee
    and b.period = old.period
    and not exists (select first 1 1
                     from eabsences a
                     join epayrolls r on (r.ref = a.epayroll and r.status > 0)
                     where a.baseabsence = b.eabsence
                       and a.baseabsence = a.ref);  -->usun tylko przy nieciaglych
end^
SET TERM ; ^
