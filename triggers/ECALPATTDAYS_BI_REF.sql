--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALPATTDAYS_BI_REF FOR ECALPATTDAYS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECALPATTDAYS')
      returning_values new.REF;
end^
SET TERM ; ^
