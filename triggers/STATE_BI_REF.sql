--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STATE_BI_REF FOR STATE                          
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('STATE')
      returning_values new.REF;
end^
SET TERM ; ^
