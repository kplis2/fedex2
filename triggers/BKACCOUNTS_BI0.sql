--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_BI0 FOR BKACCOUNTS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable synlen int;
begin
  if (new.multicurr is null) then new.multicurr = 0;
  if (new.prd is null) then new.prd = 0;
  select synlen from bkyears where yearid = new.yearid and company = new.company into :synlen;
  if (coalesce(char_length(new.symbol),0) <> synlen) then exception FK_ERROR_SYNLEN; -- [DG] XXX ZG119346
  if (new.matchable is null) then new.matchable = 0;
  if (new.matchable > 0 and new.matchingslodef is null) then
    exception BKACCOUNT_ERROR 'Nie wybrano słownika dla konta rozliczeniowego';
end^
SET TERM ; ^
