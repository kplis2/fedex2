--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RELATIONS_AU0 FOR RELATIONS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (old.srefto is not null and new.relationdef = 1 and (new.act <> old.act or new.quantityto <> old.quantityto
      or new.quantitytoreal <> old.quantitytoreal)
  ) then
  begin
    if (old.stableto = 'POZZAM') then
      execute procedure POZZAM_OBL(old.srefto);
  end
end^
SET TERM ; ^
