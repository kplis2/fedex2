--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESPOS_AI0 FOR PRSCHEDGUIDESPOS               
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable prpreprod smallint;
begin
  select g.prpreprod
    from prschedguides g
    where g.ref = new.prschedguide
    into prpreprod;
  if (prpreprod is null) then prpreprod = 0;
  if(new.amount >0 and new.pggamountcalc = 0 and prpreprod = 0) then
    execute procedure PRSCHEDGUIDESPOS_AMOUNTcalc(null,new.ref);
  if (new.amountzreal > 0 and new.pozzamref is not null) then
    execute PROCEDURE PRPOZZAM_AMOUNTZREAL(new.pozzamref);
end^
SET TERM ; ^
