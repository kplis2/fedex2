--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERBLOCK_BD0 FOR NUMBERBLOCK                    
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable keyval varchar(20);
declare variable numoddzial varchar(20);
declare variable number integer;
declare variable lastdata timestamp;
declare variable oldnumber integer;
declare variable olddata timestamp;
declare variable typ smallint;
begin
  /*zwolnienie poprzedniego numeru*/
  if(old.number > 0) then begin
    execute procedure NUMBER_FORMAT(old.numerator, old.numrejdok, old.numtypdok, old.data) returning_values :keyval, :numoddzial;
    select min(number), min(LASTDATA) from NUMBERFREE where oddzial = :numoddzial and nazwa=old.numerator and key_val = :keyval and typ = 0 into :number, :lastdata;
    if(:number is null) then number = 0;
    select NUMBER, LASTDATA, typ from NUMBERFREE where ref=old.number into :oldnumber, :olddata, :typ;
    if(:typ <> 1) then exception NUMBERBLOCK_NUMUSED;
    if(:olddata = :lastdata and :oldnumber = :number) then begin
      update NUMBERFREE set NUMBER = NUMBER - 1 where oddzial = :numoddzial and nazwa=old.numerator and key_val = :keyval and typ = 0;
      update NUMBERFREE set NUMBLOCK = null where REF=old.number;
      delete from NUMBERFREE where ref=old.number;
      delete from NUMBERFREE where NUMBER = 0 and oddzial = :numoddzial and nazwa=old.numerator and key_val = :keyval and typ = 0;
    end else
      update NUMBERFREE set NUMBLOCK = null where ref=old.number;
  end
end^
SET TERM ; ^
