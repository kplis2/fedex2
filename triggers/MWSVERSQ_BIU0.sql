--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSVERSQ_BIU0 FOR MWSVERSQ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.mwsquick is null) then new.mwsquick = 0;
  if (new.mwstotal is null) then new.mwstotal = 0;
  if (new.mwssellav is null) then new.mwssellav = 0;
end^
SET TERM ; ^
