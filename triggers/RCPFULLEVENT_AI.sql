--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RCPFULLEVENT_AI FOR RCPFULLEVENT                   
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable format varchar(255);
  declare variable ret smallint;
begin
  ret = 1;
  execute procedure get_config('RCPFILEFORMAT',2) returning_values format;
  if (format = 'NNNNNYYYYMMDDHHmmSSRKE') then
    execute procedure import_rcp_nymdhmsrke(new.line)
      returning_values :ret;
  --nierozpoznany format
  else exception rcp_import_wrong_format;

  if (ret = 0) then
    exception rcp_import_wrong_format;
  else
  --jak wszystko poszlo ok to nie ma sensu zasmiecac
    delete from rcpfullevent where ref = new.ref;
    
end^
SET TERM ; ^
