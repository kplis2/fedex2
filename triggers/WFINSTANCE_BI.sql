--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFINSTANCE_BI FOR WFINSTANCE                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_wfinstance,1);
end^
SET TERM ; ^
