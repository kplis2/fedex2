--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EINTERNALREVS_BI_REF FOR EINTERNALREVS                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EINTERNALREVS')
      returning_values new.REF;
end^
SET TERM ; ^
