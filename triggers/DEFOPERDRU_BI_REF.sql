--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERDRU_BI_REF FOR DEFOPERDRU                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFOPERDRU')
      returning_values new.REF;
end^
SET TERM ; ^
