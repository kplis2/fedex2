--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTSPOS_SET_STATE FOR EPRESENCELISTSPOS              
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.state is null) then begin
     new.state = 0;
  end
end^
SET TERM ; ^
