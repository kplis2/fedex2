--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYNAG_BI_REF FOR NOTYNAG                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NOTYNAG')
      returning_values new.REF;
end^
SET TERM ; ^
