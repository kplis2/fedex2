--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPEMPLACCIDENT_BI FOR BHPEMPLACCIDENT                
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('BHPEMPLACCIDENT') returning_values new.REF;
END^
SET TERM ; ^
