--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BI FOR POZZAM                         
  ACTIVE BEFORE INSERT POSITION 5 
AS
declare variable aktywny smallint;
declare variable typ char(1);
declare variable symbol varchar(120);
declare variable towakt varchar(255);
declare variable termdost timestamp;
declare variable datawe timestamp;
declare variable dostawca integer;
declare variable wydania smallint;
declare variable outonpozzamenabled smallint;
declare variable kilosc numeric(14,2);
declare variable typzam varchar(3);
declare variable altallow smallint;
BEGIN
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.opk is NULL) then new.opk = 0;
  if(new.replicat is NULL) then new.replicat = 0;
  if(new.kilosc is null) then new.kilosc = 0;
  if(new.kilcalcorg is null) then new.kilcalcorg = 1;
  if(new.ilonklon is null) then new.ilonklon = 0;
  if(new.ilonklonm is null) then new.ilonklonm = 0;
  if(new.ilosco is null) then new.ilosco = 0;
  if(new.ilrealo is null) then new.ilrealo = 0;
  if(new.iloscm is null) then new.iloscm = 0;
  if(new.ilrealm is null) then new.ilrealm = 0;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.ilreal is null) then new.ilreal = 0;
  if(new.ilrealm is null) then new.ilrealm = 0;
  if(new.ildysp is null) then new.ildysp = 0;
  if(new.ilzdysp is null) then new.ilzdysp = 0;
  if(new.ilzadysp is null) then new.ilzadysp = 0;
  if(new.ilzreal is null) then new.ilzreal = 0;
  if(new.ilzrealoverlimit is null) then new.ilzrealoverlimit = 0;
  if(new.ilzrealm is null) then new.ilzrealm = 0;
  if(new.rabat is null) then new.rabat = 0;
  if(new.rabattab is null) then new.rabattab = 0;
  if(new.cenamag is null) then new.cenamag = 0;
  if(new.blokadarez is null) then new.blokadarez = 0;
  if(new.hasownsupply is null) then new.hasownsupply = 0;
  if (new.ilsped is null) then new.ilsped = 0;
  if (new.ilosconmwsacts is null) then new.ilosconmwsacts = 0;
  if (new.ilosconmwsactsc is null) then new.ilosconmwsactsc = 0;
  if (new.anulowano is null) then new.anulowano = 0;
  new.wartmag = new.cenamag * new.ilosc;
  if(new.magazyn='') then new.magazyn=null;
  if(new.mag2='') then new.mag2=null;
  if(new.magazyn is null) then begin
    select magazyn from NAGZAM where REF=new.zamowienie into new.magazyn;
    if((new.magazyn is null or new.magazyn = '') and new.prshmat is not null) then
      select warehouse from prshmat where ref = new.prshmat into new.magazyn;
    if(new.magazyn='') then new.magazyn=null;
  end
  select AKT from TOWARY where KTM = new.KTM into :aktywny;
  execute procedure GETCONFIG('TOWARAKTYWNY') returning_values :towakt;
  if(:towakt='1' and :aktywny<>1) then
    exception TOWAR_NIEAKTYWNY 'Wybrany towar '||new.ktm||' jest nieaktywny.';
  if  (new.stan is null or (new.stan = ' ') or (new.stan = ''))then
    select STAN from NAGZAM where nagzam.ref = new.zamowienie into new.stan;
  if  (new.stan is null or (new.stan = ' ')) then new.stan = 'N';

  select t.wydania, t.outonpozzamenabled, n.termdost, n.datawe, n.typzam
    from nagzam n
      left join typzam t on (n.typzam = t.symbol)
    where n.ref = new.zamowienie
    into :wydania, :outonpozzamenabled, termdost, datawe, :typzam;

  if  (:outonpozzamenabled = 1 and wydania > 1 and new.out is null) then begin
    if(new.prsheet is not null) then new.out = 0;
    else new.out = 1;
  end else if(new.out is null) then begin
    if(wydania = 0) then new.out = 0;
    else new.out = 1;
  end
  if (wydania = 3 and new.termdost is null) then
    new.termdost = coalesce(termdost,datawe);
  select TOWJEDN.waga*new.ilosco, TOWJEDN.vol * new.ilosco from TOWJEDN where ref=new.jedno into new.waga, new.objetosc;
  /*zaożenie indywidualnej dostawy*/
  if  (new.hasownsupply = 1) then begin
    select ID,TERMDOST,DOSTAWCA from NAGZAM where REF=new.zamowienie into :symbol,:termdost,:dostawca;
    if  (new.termdost is not null) then begin
       termdost = new.termdost;
       symbol = :symbol||'/'||cast(:termdost as date);
    end
    new.cenamag = null;
    new.dostawamag = null;
    select max(ref) from DOSTAWY where SYMBOL = :symbol into new.dostawamag;
    if  (new.dostawamag is null) then begin
      execute procedure GEN_REF('DOSTAWY') returning_values new.dostawamag;
      insert into dostawy(REF,SYMBOL,RECZNYSYMBOL,DATA,MAGAZYN) values (new.dostawamag, :symbol,1,:termdost,new.magazyn);
    end
  end
  /*kontrola rezerwacji na cene lub dostawe w zaleznosci od typu magazynu*/
  if  ((new.cenamag is not null) or (new.dostawamag is not null) )then begin
    select TYP from DEFMAGAZ where SYMBOL = new.magazyn into :typ;
    if  (:typ <> 'P' and :typ <> 'C') then begin
      new.cenamag = 0;
      new.dostawamag = null;
    end
      if  (:typ = 'C') then new.dostawamag = null;
  end

  if (new.ildoprod is null) then new.ildoprod = 0;
  if (new.ilwyprod is null) then new.ilwyprod = 0;

  if (coalesce(new.fake,0)=1) then
  begin
    select altallow from typzam where symbol = :typzam
    into :altallow;

    if (coalesce(:altallow,0) = 0) then
      exception nag_fake;

    new.opk = 1;
  end
END^
SET TERM ; ^
