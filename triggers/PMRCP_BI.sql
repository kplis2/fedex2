--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMRCP_BI FOR PMRCP                          
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  IF (NEW.REF IS NULL) THEN
    execute procedure GEN_REF('PMRCP') returning_values new.ref;
END^
SET TERM ; ^
