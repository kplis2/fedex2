--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMSER_BI_ORDER FOR DOKUMSER                       
  ACTIVE BEFORE INSERT POSITION 4 
as
declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 1 and new.numer is not null) then begin
    new.ord = 0;
    exit;
  end
  new.ord = 1;
  select max(numer) from dokumser where refpoz = new.refpoz
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update dokumser set ord = 0, numer = numer + 1
      where numer >= new.numer and refpoz = new.refpoz;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
