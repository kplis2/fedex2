--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOSTSPOS_BIU_CHECK FOR ECOSTSPOS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable percent numeric(14,2);
begin
  if (new.percent < 0) then
    exception negative_value;
  select sum(p.percent)
    from ecostspos p
    where p.ecost = new.ecost
    into :percent;
  if (coalesce(percent,0) + new.percent - coalesce(old.percent,0) > 100) then
    exception more_than_100_percent;
  if(coalesce(new.bksymbol,'') = '' and coalesce(new.mpkdpos,'') = '') then
    exception empty_bksymbol;
end^
SET TERM ; ^
