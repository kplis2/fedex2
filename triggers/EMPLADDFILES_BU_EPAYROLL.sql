--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILES_BU_EPAYROLL FOR EMPLADDFILES                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable status smallint;
begin
  if (old.filetype = 5 and old.epayroll is not null and (old.epayroll <> new.epayroll or new.epayroll is null)) then
  begin
    select p.status
      from epayrolls p
      where p.ref = old.epayroll
      into :status;
    if (status > 0) then
      exception payroll_is_closed;
  end
end^
SET TERM ; ^
