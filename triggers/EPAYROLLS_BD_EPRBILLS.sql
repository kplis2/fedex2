--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_BD_EPRBILLS FOR EPAYROLLS                      
  ACTIVE BEFORE DELETE POSITION 1 
AS
begin
  if (old.empltype = 2) then
  begin
    update eprpos set cauto = 2 where payroll = old.ref;
    delete from eprpos where payroll = old.ref;
    delete from eprempl where epayroll = old.ref;
  end
end^
SET TERM ; ^
