--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_AD0 FOR DEFMAGAZ                       
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if (exists(select ref from DEFOPERMAG where MAG2=old.SYMBOL)) then
    exception DEFMAGAZ_DEFOPERMAG_JOIN;
  delete from DEFOPERMAG where MAGAZYN = old.symbol;
end^
SET TERM ; ^
