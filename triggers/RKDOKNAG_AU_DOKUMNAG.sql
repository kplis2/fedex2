--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AU_DOKUMNAG FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable dokumnag integer;
declare variable kwotazlp numeric(14,2);
declare variable kwotazlm numeric(14,2);
declare variable local integer;
begin
  -- aktualizacja kwoty zaliczki na dok mag
  execute procedure CHECK_LOCAL('RKRAPKAS',old.ref) returning_values :local;
  if((new.numer<>old.numer or (new.numer is not null and old.numer is null)
    or (new.numer is null and old.numer is not null)) and (local = 1)) then begin
    for select distinct dokumnag
      from rkdokpoz
      where dokument=new.ref and dokumnag is not null
      into :dokumnag
    do begin
      kwotazlp = null;
      select sum(rkdokpoz.kwotazl)
        from rkdokpoz
        left join rkdoknag on (rkdoknag.ref=rkdokpoz.dokument)
        where rkdokpoz.dokumnag=:dokumnag and rkdokpoz.PM='+' and rkdoknag.numer>0
        into :kwotazlp;
      if(:kwotazlp is null) then kwotazlp = 0;
      kwotazlm = null;
      select sum(rkdokpoz.kwotazl)
        from rkdokpoz
        left join rkdoknag on (rkdoknag.ref=rkdokpoz.dokument)
        where rkdokpoz.dokumnag=:dokumnag and rkdokpoz.PM='-' and rkdoknag.numer>0
        into :kwotazlm;
      if(:kwotazlm is null) then kwotazlm = 0;
      update dokumnag set kwotazal = :kwotazlp-:kwotazlm where ref=:dokumnag;
    end
  end
end^
SET TERM ; ^
