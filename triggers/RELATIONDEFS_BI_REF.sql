--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RELATIONDEFS_BI_REF FOR RELATIONDEFS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('RELATIONDEFS') returning_values new.ref;
end^
SET TERM ; ^
