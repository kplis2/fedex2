--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_REPLICAT_AI0 FOR RP_REPLICAT                    
  ACTIVE AFTER INSERT POSITION 0 
as
begin
 if(new.replicat_type = 0) then begin
   /*dodanie definicji replikacji - dpdanie itemu glownego dla tablei*/
   insert into RP_ITEM(ID_OBJ,PARENT_ID,STABLE,TYP,FIELD)
     values(new.S_OBJ,0,new.SD_TABLE,1,'');
   /*dodanie itemu glownego dla korzenia definijcji obiektu docelowego*/
   insert into RP_ITEM(ID_OBJ,PARENT_ID,STABLE,TYP,FIELD)
     values(new.D_OBJ,0,'',0,'');
 end
end^
SET TERM ; ^
