--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_WERSJE_AD FOR STANYIL                        
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.ilosc > 0 or old.zamowiono > 0 or old.zarezerw > 0 or old.zablokow > 0) then
  begin
    update wersje set wersje.ilosc = wersje.ilosc - old.ilosc,
        wersje.zamowiono = wersje.zamowiono - old.zamowiono,
        wersje.zarezerw = wersje.zarezerw - old.zarezerw,
        wersje.zablokow = wersje.zablokow - old.zablokow
      where wersje.ref = OLD.wersjaref;
  end
end^
SET TERM ; ^
