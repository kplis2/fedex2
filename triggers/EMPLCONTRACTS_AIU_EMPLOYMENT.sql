--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_AIU_EMPLOYMENT FOR EMPLCONTRACTS                  
  ACTIVE AFTER INSERT OR UPDATE POSITION 2 
AS
  declare variable eref integer;
  declare variable fromdate date;
  declare variable etodate date;
  declare variable atodate date;
  declare variable annexe integer;
begin
--Przeniesienie zmian na przebieg zatrudnienia po insercie/update umowy

  if (new.empltype = 1) then --PR60202
  begin
    if (inserting or old.empltype <> 1) then
    begin
      insert into employment (employee, fromdate, todate, emplcontract, workpost, workdim,
          dimnum, dimden, branch, department, salary, paymenttype, caddsalary, funcsalary,
          dictdef, dictpos, bksymbol, local, proportional, emplgroup, workcat, worksystem,
          epayrule
      ) values (new.employee, new.fromdate, new.enddate, new.ref, new.workpost, new.workdim,
          new.dimnum, new.dimden, new.branch, new.department, new.salary, new.paymenttype,
          new.caddsalary, new.funcsalary, new.dictdef, new.dictpos, new.bksymbol, new.local,
          new.proportional, new.emplgroup, new.workcat, new.worksystem, new.epayrule
      );
    end else if (
      new.branch <> old.branch or (new.branch is null and old.branch is not null) or (new.branch is not null and old.branch is null) or
      new.department <> old.department or (new.department is null and old.department is not null) or (new.department is not null and old.department is null) or
      new.workpost <> old.workpost or (new.workpost is null and old.workpost is not null) or (new.workpost is not null and old.workpost is null) or
      new.workdim <> old.workdim or (new.workdim is null and old.workdim is not null) or (new.workdim is not null and old.workdim is null) or
      new.salary <> old.salary or (new.salary is null and old.salary is not null) or (new.salary is not null and old.salary is null) or
      new.caddsalary <> old.caddsalary or (new.caddsalary is null and old.caddsalary is not null) or (new.caddsalary is not null and old.caddsalary is null) or
      new.funcsalary <> old.funcsalary or (new.funcsalary is null and old.funcsalary is not null) or (new.funcsalary is not null and old.funcsalary is null) or
      new.econtrtype <> old.econtrtype or (new.econtrtype is null and old.econtrtype is not null) or (new.econtrtype is not null and old.econtrtype is null) or
      new.fromdate <> old.fromdate or (new.fromdate is null and old.fromdate is not null) or (new.fromdate is not null and old.fromdate is null) or
      new.enddate <> old.enddate or (new.enddate is null and old.enddate is not null) or (new.enddate is not null and old.enddate is null) or
      new.paymenttype <> old.paymenttype or (new.paymenttype is null and old.paymenttype is not null) or (new.paymenttype is not null and old.paymenttype is null) or
      new.dictpos <> old.dictpos or (new.dictpos is null and old.dictpos is not null) or (new.dictpos is not null and old.dictpos is null) or
      new.bksymbol <> old.bksymbol or (new.bksymbol is null and old.bksymbol is not null) or (new.bksymbol is not null and old.bksymbol is null) or
      new.local <> old.local or (new.local is null and old.local is not null) or (new.local is not null and old.local is null) or
      new.proportional <> old.proportional or (new.proportional is null and old.proportional is not null) or (new.proportional is not null and old.proportional is null) or
      new.emplgroup <> old.emplgroup or (new.emplgroup is null and old.emplgroup is not null) or (new.emplgroup is not null and old.emplgroup is null) or
      new.worksystem <> old.worksystem or (new.worksystem is null and old.worksystem is not null) or (new.worksystem is not null and old.worksystem is null) or
      new.workcat <> old.workcat or (new.workcat is null and old.workcat is not null) or (new.workcat is not null and old.workcat is null) or
      new.epayrule is distinct from old.epayrule
    ) then begin

        if (new.enddate <> old.enddate or (new.enddate is null and old.enddate is not null)) then
        begin
        /*jezeli zmieniamy date konca umowy trzeba sprawdzic czy nie dotyczy ona
          ostatniego wpisu w emplomencie i jednoczesnie oddelegowania (BS25412)*/

          select first 1 ref, annexe, todate from employment
            where emplcontract = new.ref
            order by fromdate desc
            into :eref, :annexe, :etodate;

          select todate from econtrannexes
            where atype = 2 and ref = :annexe
            into :atodate;

          if (atodate > etodate and (new.enddate is null or new.enddate > atodate)) then
          begin
          --nalezy uzupelniec wpis w emplomencie o oddelegowaniu oraz przywrocic wpis dodtyczacy umowy
            update employment set todate = :atodate where ref = :eref;

            insert into employment (fromdate, todate, employee, emplcontract, annexe, workdim)
            values (:atodate + 1, new.enddate, new.employee, new.ref, null, 1);
          end
        end

        if (new.fromdate <> old.fromdate) then
          update employment
            set fromdate = new.fromdate, workpost = new.workpost, workdim = new.workdim, dimnum = new.dimnum,
              dimden = new.dimden, branch = new.branch, department = new.department, salary = new.salary,
              paymenttype = new.paymenttype, caddsalary = new.caddsalary, funcsalary = new.funcsalary,
              dictdef = new.dictdef, dictpos = new.dictpos, bksymbol = new.bksymbol, local = new.local,
              proportional = new.proportional, emplgroup = new.emplgroup, workcat = new.workcat,
              worksystem = new.worksystem, epayrule = new.epayrule
            where emplcontract = new.ref and annexe is null and fromdate = old.fromdate;
        else
          update employment set workpost = new.workpost, workdim = new.workdim, dimnum = new.dimnum,
              dimden = new.dimden, branch = new.branch, department = new.department, salary = new.salary,
              paymenttype = new.paymenttype, caddsalary = new.caddsalary, funcsalary = new.funcsalary,
              dictdef = new.dictdef, dictpos = new.dictpos, bksymbol = new.bksymbol, local = new.local,
              proportional = new.proportional, emplgroup = new.emplgroup, workcat = new.workcat,
              worksystem = new.worksystem, epayrule = new.epayrule
          where emplcontract = new.ref and annexe is null;

      --okreslenie najmlodszego wpisu w tabeli employment
        select first 1 ref, fromdate from employment
          where emplcontract = new.ref
          order by fromdate desc
          into :eref, :fromdate;
    
        if (fromdate > new.enddate) then
        begin
        --nalezy usunac wpisy w employmencie po dacie zwolnienia (np. w.p oddelegowania) - BS25412
          select first 1 ref, fromdate from employment
            where fromdate <= new.enddate and emplcontract = new.ref
            order by fromdate desc
            into :eref, :fromdate;
    
          delete from employment
            where emplcontract = new.ref and fromdate > :fromdate;
        end
    
        update employment set todate = new.enddate where ref = :eref;
    end
  end else if (old.empltype = 1 and old.empltype <> new.empltype) then
  --jezeli dany delete burzyl by ciaglosc zatrudnienia, wychwyci to trigger na before
    delete from employment where emplcontract = old.ref;
end^
SET TERM ; ^
