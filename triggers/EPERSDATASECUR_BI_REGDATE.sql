--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSDATASECUR_BI_REGDATE FOR EPERSDATASECUR                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.regdate is null) then new.regdate = current_timestamp(0);
end^
SET TERM ; ^
