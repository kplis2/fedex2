--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER QUERYTYPES_BI_BLANK FOR QUERYTYPES                     
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  if (new.rights is null) then new.rights = '';
  if (new.rightsgroup is null) then new.rightsgroup = '';
end^
SET TERM ; ^
