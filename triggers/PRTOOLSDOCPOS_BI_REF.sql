--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCPOS_BI_REF FOR PRTOOLSDOCPOS                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (coalesce(new.ref, 0) = 0) then
    execute procedure gen_ref('PRTOOLSDOCPOS') returning_values new.ref;
end^
SET TERM ; ^
