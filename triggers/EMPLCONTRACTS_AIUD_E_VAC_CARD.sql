--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_AIUD_E_VAC_CARD FOR EMPLCONTRACTS                  
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 10 
AS
declare variable ondate timestamp;
begin
--Aktualizacja kart urlopowych (BS53156)

  if (1 in (new.empltype, old.empltype) and (deleting
    or coalesce(new.empltype,0) <> coalesce(old.empltype,0)
    or new.fromdate is distinct from old.fromdate
    or new.enddate is distinct from old.enddate
    or coalesce(new.workdim,0) <> coalesce(old.workdim,0))
  ) then begin

    if (not updating) then ondate = coalesce(new.fromdate,old.fromdate);
    else if (new.fromdate <= old.fromdate) then ondate = new.fromdate;
    else ondate = old.fromdate;

    execute procedure e_vac_card (:ondate, coalesce(-new.employee, -old.employee));
  end

  else if (coalesce(new.empltype, old.empltype) > 1 and (deleting
    or coalesce(new.empltype,0) <> coalesce(old.empltype,0)
    or new.fromdate is distinct from old.fromdate
    or new.enddate is distinct from old.enddate )
  ) then begin

    if (not updating) then ondate = coalesce(new.fromdate,old.fromdate);
    else if (new.fromdate < old.fromdate) then
    begin
      execute procedure refresh_evacucp(new.fromdate, coalesce(new.ref,old.ref));
    end
    else if(new.fromdate > old.fromdate) then
      if(exists(select first 1 1
                  from eabsences
                  where fromdate < new.fromdate
                    and emplcontract = new.ref)) then
      begin
        exception universal'Istnieja zarejestrowane nieobecnosci. Zmiana niemozliwa';
      end
      else
        execute procedure refresh_evacucp(old.fromdate, new.ref);
    else
    begin
      execute procedure refresh_evacucp(old.fromdate, old.ref);
    end
  end
end^
SET TERM ; ^
