--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_AI0 FOR DECREES                        
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable bktype smallint;
declare variable balsum numeric(14,2);
begin
  select b.bktype from bkaccounts b where b.ref = new.accref into :bktype;
  if (bktype < 2) then
  begin
    select baldebit - balcredit from bkdocs b
      where b.ref = new.bkdoc
      into :balsum;
    balsum = balsum + new.debit - new.credit;
    update bkdocs b
      set sumdebit = sumdebit + new.debit,
          sumcredit = sumcredit + new.credit,
          baldebit = case when :balsum > 0 then :balsum else 0 end,
          balcredit = case when :balsum < 0 then -:balsum else 0 end
      where b.ref = new.bkdoc;
  end
end^
SET TERM ; ^
