--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDULERESOURCES_BI0 FOR PRSCHEDULERESOURCES            
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('PRSCHEDULERESOURCES') returning_values new.ref;
end^
SET TERM ; ^
