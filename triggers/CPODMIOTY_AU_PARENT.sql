--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_PARENT FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable ref integer;
begin
  if (coalesce(new.ancestors,0) <> coalesce(old.ancestors,0)) then
  begin
    update ckontrakty set cpodmancestors = new.ancestors where cpodmiot = new.ref;
    update pkosoby set cpodmancestors = new.ancestors where cpodmiot = new.ref;
    update zadania set cpodmancestors = new.ancestors where cpodmiot = new.ref;
    update kontakty set cpodmancestors = new.ancestors where cpodmiot = new.ref;
    update cnotatki set cpodmancestors = new.ancestors where cpodmiot = new.ref;
    for select ref from cpodmioty where parent = new.ref into :ref
    do begin
       if (new.ancestors is null or new.ancestors='') then
         update cpodmioty set ancestors = ';'||new.ref||';' where ref = :ref;
       else
         update cpodmioty set ancestors = new.ancestors || new.ref || ';' where ref = :ref;
    end
  end
end^
SET TERM ; ^
