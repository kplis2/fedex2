--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVPOSITIONS_BI_ORDER FOR SRVPOSITIONS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from srvpositions where srvrequest = new.srvrequest
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update srvpositions set ord = 0, number = number + 1
      where number >= new.number and srvrequest = new.srvrequest;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
