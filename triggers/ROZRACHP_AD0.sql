--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_AD0 FOR ROZRACHP                       
  ACTIVE AFTER DELETE POSITION 0 
as
  declare variable zaprej varchar(255);
  declare variable sprzedzestid integer;
begin
  execute procedure GETCONFIG('ZAPREJ') returning_values :zaprej;
  if (zaprej = '1' and old.faktura is not null) then
    execute statement 'execute procedure NAGFAK_OBL_ZAP(' || old.faktura || ')';
  select SPRZEDZESTID
    from ROZRACH where kontofk = old.kontofk and symbfak = old.symbfak
      and slodef = old.slodef and slopoz = old.slopoz and company = old.company
    into :sprzedzestid;
  if (sprzedzestid > 0) then
    execute procedure SPRZEDZEST_OBL_ROZRACH(sprzedzestid);

  if (not exists(select ref from rozrachp where kontofk = old.kontofk and symbfak = old.symbfak
      and slodef = old.slodef and slopoz = old.slopoz and company = old.company))
  then begin
    delete from rozrach where kontofk = old.kontofk and slodef = old.slodef
      and slopoz = old.slopoz and symbfak = old.symbfak and company = old.company and token <> 7;
  end else begin
    execute procedure ROZRACH_OBL_NAG(old.kontofk,old.slodef, old.slopoz, old.symbfak, old.company);
  end
  if (old.stable = 'ROZRACHPRZEN') then
    delete from ROZRACHP where STABLE = 'ROZRACHPRZEN' and sref = old.sref;
end^
SET TERM ; ^
