--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ESOCIALFUNDS_BU FOR ESOCIALFUNDS                   
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable temp_c integer;
begin
  temp_c = 0;

 -- exception test_break new.fundtype||' '||old.fundtype ||' '||cast(old.funddate as date)||' '||cast(new.funddate as date);

  if ((old.fundtype <> new.fundtype) or (cast(old.funddate as date) <> cast(new.funddate as date))) then
    begin
      SELECT Count(ref) FROM esocialfunds ef WHERE ef.employee = new.employee and ef.fundtype = new.fundtype and ef.funddate = new.funddate
        into :temp_c;
    end    
  if (temp_c <> 0) then
   exception esocfunds_duplicate;
end^
SET TERM ; ^
