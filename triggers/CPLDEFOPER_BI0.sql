--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLDEFOPER_BI0 FOR CPLDEFOPER                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ilpkt is null) then new.ilpkt = 0;
  if(new.ilpktconst is null) then new.ilpktconst = 0;
  if(new.typpkt=0) then new.typpkt = NULL;
  if((new.symbol is null) or (new.symbol='')) then new.symbol = new.nazwa;
end^
SET TERM ; ^
