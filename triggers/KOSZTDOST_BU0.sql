--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KOSZTDOST_BU0 FOR KOSZTDOST                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(coalesce(new.sposdostcennik,0)<>coalesce(old.sposdostcennik,0)) then begin
    if(new.sposdostcennik is not null) then
      select sposdost from sposdostcennik where ref=new.sposdostcennik into new.spedytor;
    else
      new.spedytor = NULL;
  end
  if(new.wagap is null) then new.wagap = 0;
  if(new.waga is null) then new.waga = 0;
  if(new.odlegloscp is null) then new.odlegloscp = 0;
  if(new.odleglosc is null) then new.odleglosc = 0;
  if(new.iloscpalp is null) then new.iloscpalp = 0;
  if(new.iloscpal is null) then new.iloscpal = 0;
  if(new.ilosckartp is null) then new.ilosckartp = 0;
  if(new.ilosckart is null) then new.ilosckart = 0;
end^
SET TERM ; ^
