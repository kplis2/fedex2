--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHG_AU0_DEFCECHY FOR DEFCECHG                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable cechyref varchar(30);
begin
  if(old.nazwa is null or old.nazwa<>new.nazwa or new.nazwa is null) then begin
    for select SYMBOL from DEFCECHY where GRUPA = new.REF into :cechyref
    do begin
      update defcechy set NAZWAGRUPY = new.nazwa where SYMBOL = :cechyref;
    end
  end
end^
SET TERM ; ^
