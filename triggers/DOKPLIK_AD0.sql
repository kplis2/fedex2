--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_AD0 FOR DOKPLIK                        
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable numbergen varchar(80);
declare variable ref integer;
declare variable segnazwa varchar(80);
declare variable typnazwa varchar(80);
declare variable typsymbol varchar(20);
declare variable segsymbol varchar(20);
begin
  if(old.typ is not null and old.numer is not null) then begin
    numbergen = null;
    select symbol, nazwa from SEGREGATOR where SEGREGATOR.REF=old.SEGREGATOR into :segsymbol, :segnazwa;
    select numerator, symbol, nazwa from dokpliktyp where ref = old.typ into :numbergen, :typsymbol, :typnazwa;
    if(numbergen is not null)then begin
      if(coalesce(:typsymbol, '')='') then typsymbol = substring(:typnazwa from 1 for 20);
      if(coalesce(:segsymbol, '')='') then segsymbol = substring(:segnazwa from 1 for 20);
      execute procedure free_number(:numbergen,:typsymbol,:segsymbol, old.data, old.numer,0) returning_values :ref;
    end
  end
end^
SET TERM ; ^
