--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BD_ORDER FOR MWSACTS                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then
    exit;
  select max(number) from mwsacts where mwsord = old.mwsord
    into :maxnum;
  if (old.number = :maxnum) then
    exit;
  update mwsacts set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and mwsord = old.mwsord;
  update mwsords set mwsords.mwsactsnumber = :maxnum - 1 where mwsords.ref = old.mwsord; --Marcin F.
end^
SET TERM ; ^
