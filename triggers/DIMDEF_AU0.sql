--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DIMDEF_AU0 FOR DIMDEF                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable dimhier integer;
begin
  if (new.istime <> old.istime) then
    for
      select dimhierdef.ref
        from dimhierdef
        where dimhierdef.dim = new.ref
        into :dimhier
    do begin
      update frdimhier set frdimhier.istime = new.istime
        where frdimhier.dimhierdef = :dimhier;
    end
end^
SET TERM ; ^
