--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRPAYROLLSNAG_BI0 FOR ECONTRPAYROLLSNAG              
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable cnt integer;
begin
  if (new.premcalc is null) then new.premcalc = 0;
  if (new.status is null) then new.status = 0;
  select econtrpayrolls.dataod, econtrpayrolls.datado
    from econtrpayrolls
    where econtrpayrolls.ref = new.econtrpayroll
    into new.bdate, new.edate;
  if (new.operator is null and new.employee is not null) then
  begin
/*    select count(*) from operator where operator.employee = new.employee
      into :cnt;
    if (:cnt = 0) then
      exception OPERATOR_NOT_ASSIGNED;
    else */if (:cnt = 1) then
      select operator.ref from operator where operator.employee = new.employee
        into new.operator;
  end
end^
SET TERM ; ^
