--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLNAG_AU0 FOR KPLNAG                         
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable stat integer;
declare variable towtypekpl varchar(100);
begin
 if(new.glowna=1 and (old.glowna=0 or (old.glowna is null))) then begin
   update KPLNAG set glowna=0 where wersjaref=new.wersjaref and ref<>new.ref and glowna=1;
   /* aktualizacja cen kompletow, na worku bedzie trzeba obudowac konfigiem */
   execute procedure CENNIK_AKTUKOMPLETY(new.ktm,NULL) returning_values :stat;
 end
 if (new.glowna <> old.glowna) then
 begin
  execute procedure get_config('TOWTYPEKOMPLET',2) returning_values :towtypekpl;
  if (:towtypekpl is null or :towtypekpl = '') then
    towtypekpl = '2';
   execute procedure CALCULATE_KOMPLETY_POT(new.wersjaref,null,cast(:towtypekpl as integer));
 end
end^
SET TERM ; ^
