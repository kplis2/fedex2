--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BI_REF FOR NAGFAK                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NAGFAK')
      returning_values new.REF;
end^
SET TERM ; ^
