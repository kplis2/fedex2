--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER HISTZAM_BI_REF FOR HISTZAM                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('HISTZAM')
      returning_values new.REF;
end^
SET TERM ; ^
