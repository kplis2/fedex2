--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER GRUPYKLI_BI_REPLICAT FOR GRUPYKLI                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
 execute procedure rp_trigger_bi('GRUPYKLI',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
