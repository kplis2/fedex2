--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERBLOCK_BI_REF FOR NUMBERBLOCK                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NUMBERBLOCK')
      returning_values new.REF;
end^
SET TERM ; ^
