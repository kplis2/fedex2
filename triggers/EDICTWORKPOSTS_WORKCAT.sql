--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTWORKPOSTS_WORKCAT FOR EDICTWORKPOSTS                 
  ACTIVE AFTER INSERT OR UPDATE POSITION 1 
AS
    declare variable nc_from smallint;
    declare variable nc_to smallint;
begin
  if ((new.fromcat is null and new.tocat is not null) or ((new.fromcat is not null and new.tocat is null))) then
    exception WORKCAT_NOTCLOSE;

  if (new.fromcat is not null) then
  begin
    select nrarab
      from eworkcatpos
      where ref = NEW.FROMCAT
      into: nc_from;

    select nrarab
      from eworkcatpos
      where ref = NEW.TOCAT
      into: nc_to;

    if (:nc_from > :nc_to) then
      exception WORKCAT_WRONGORDER;
  end
end^
SET TERM ; ^
