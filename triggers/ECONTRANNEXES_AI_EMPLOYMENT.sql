--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_AI_EMPLOYMENT FOR ECONTRANNEXES                  
  ACTIVE AFTER INSERT POSITION 1 
AS
  declare variable todate timestamp;
  declare variable fromdate timestamp;
  declare variable tmp_todate timestamp;
  declare variable mref integer;
  declare variable tmp_mref integer;
  declare variable annexe integer;
begin
--MWr(PR24342) Przeniesienie zmian na przebieg zatrudnienia po insercie aneksu/oddelegowania

  if (exists (select first 1 1 from emplcontracts  --PR60202 
                where ref = new.emplcontract and empltype = 1)
  ) then begin

    --info o refie i zakresie rekordu wczesniejszego
    select first 1 ref, todate, fromdate from employment
      where emplcontract = new.emplcontract and fromdate <= new.fromdate
      order by fromdate desc
      into :mref, :tmp_todate, :fromdate;
  
    select first 1 fromdate - 1 from employment
      where employee = new.employee and fromdate > new.fromdate
      order by fromdate
      into :todate;
  
    todate = coalesce(todate,tmp_todate);
  
  /*info z rekordu wczesniejszego, z ktorego bedziemy pobierac dane, przenoszone
    dalej na mlodsze pozycje. Rekordem takim na pewno nie jest oddelegowanie*/
    select first 1 m.ref, m.annexe  from employment m
      left join econtrannexes a on (a.ref = m.annexe)
      where m.emplcontract = new.emplcontract and m.fromdate < new.fromdate
        and (a.ref is null or a.atype <> 2)
      order by m.fromdate desc
      into :tmp_mref, :annexe;
  
  /*jezeli dodajemy oddelegowanie, ktore wypelnia calą 'luke czasową' miedzy jedna,
    a drugą pozycja w przebiegu zatrudnienia, to new.todate = :todate i nie wchodzimy nizej*/
    if (new.atype = 2 and (todate is null or new.todate <> :todate)) then
    begin
    /*oddelegowanie dzieli pozcyje w przebiegu zatrudnienia. Ponizej przepisanie
      wartosci sprzed oddelegowanie, ktore jednoczesnie beda obowiazywac za oddeleg.*/
  
      mref = coalesce(tmp_mref, mref);
  
      insert into employment (employee, fromdate, todate, emplcontract, workpost, workdim,
          dimnum, dimden, branch, department, salary, paymenttype, caddsalary, funcsalary, annexe,
          dictdef, dictpos, bksymbol, local, proportional, emplgroup, workcat, worksystem, epayrule
      ) select employee, new.todate + 1, :todate, emplcontract, workpost, workdim, dimnum, dimden,
           branch, department, salary, paymenttype, caddsalary, funcsalary, annexe, dictdef,
           dictpos, bksymbol, local, proportional, emplgroup, workcat, worksystem, epayrule
        from employment
        where ref = :mref;
    
      if (new.fromdate = fromdate) then
        update employment set todate = new.todate
          where emplcontract = new.emplcontract and fromdate = new.fromdate;
      else
        todate = new.todate;
    end
  
    if (new.fromdate = fromdate) then
    begin
    --sytuacja, gdzie dodajemy rekord dzien pozniej za jakims oddelegowaniem
      update employment set workpost = new.workpost, workdim = new.workdim, dimnum = new.dimnum,
          dimden = new.dimden, branch = new.branch, department = new.department, salary = new.salary,
          paymenttype = new.paymenttype, caddsalary = new.caddsalary, funcsalary = new.funcsalary,
          annexe = new.ref, dictdef = new.dictdef, dictpos = new.dictpos, bksymbol = new.bksymbol,
          local = new.local, proportional = new.proportional, emplgroup = new.emplgroup, workcat = new.workcat,
          worksystem = new.worksystem, epayrule = new.epayrule
        where emplcontract = new.emplcontract and fromdate = new.fromdate;
    end else begin
    --bezposredni insert do przebiegu zatrudnienia dodawanego rekordu
      insert into employment (employee, fromdate, todate, emplcontract, workpost, workdim,
          dimnum, dimden, branch, department, salary, paymenttype, caddsalary, funcsalary, annexe,
          dictdef, dictpos, bksymbol, local, proportional, emplgroup, workcat, worksystem, epayrule
      ) values (new.employee, new.fromdate, :todate, new.emplcontract, new.workpost, new.workdim,
          new.dimnum, new.dimden, new.branch, new.department, new.salary, new.paymenttype,
          new.caddsalary, new.funcsalary, new.ref, new.dictdef, new.dictpos, new.bksymbol,
          new.local, new.proportional, new.emplgroup, new.workcat, new.worksystem, new.epayrule);
      update employment set todate = new.fromdate - 1 where ref = :mref;
    end
  
  /*Dodanie rekordu moze wymagac update tych wartosci w przebiegu zatrudnienia, ktore
    odnosza sie do pozycji, ktore 'zaslonimy' nowym rekordem. Nie dotyczy to oddelegowania*/
    if (new.atype = 1) then
      if (exists(select first 1 1 from employment
                   where coalesce(annexe,0) = coalesce(:annexe,0) and fromdate > new.fromdate)
      ) then
         execute procedure employment_rechange_annexerefs(new.fromdate + 1, new.emplcontract, :annexe, new.ref);
  end
end^
SET TERM ; ^
