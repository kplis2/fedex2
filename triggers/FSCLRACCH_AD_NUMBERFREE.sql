--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCH_AD_NUMBERFREE FOR FSCLRACCH                      
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable gennumerator varchar(80);
declare variable blockref integer;
begin
  if (old.fstype=0) then
  begin
    execute procedure getconfig('NUMBERGEN_CLRACC') returning_values :gennumerator;
    execute procedure FREE_NUMBER(:gennumerator,'ROZL', 'ROZL',old.regdate,old.number,0) returning_values :blockref;
  end
  else  if (old.fstype=1) then
  begin
    execute procedure getconfig('NUMBERGEN_KOMPENSAT') returning_values :gennumerator;
    execute procedure FREE_NUMBER(:gennumerator,'KOMP', 'KOMP',old.regdate,old.number,0) returning_values :blockref ;
  end
end^
SET TERM ; ^
