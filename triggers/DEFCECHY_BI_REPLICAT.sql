--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHY_BI_REPLICAT FOR DEFCECHY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('DEFCECHY',new.symbol, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
