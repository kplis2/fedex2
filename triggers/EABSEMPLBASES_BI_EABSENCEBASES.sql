--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSEMPLBASES_BI_EABSENCEBASES FOR EABSEMPLBASES                  
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable eabsref integer;
declare variable status smallint;
begin
-- MWr Przenoszenie podstaw do juz zarejestrowanych nieobecnosci po zamknieciu list plac
  for
    select a.ref, coalesce(r.status,0)
      from eabsences a
        left join epayrolls r on (r.ref = a.epayroll)
      where a.employee = new.employee
        and a.fbase <= new.period
        and a.lbase >= new.period
        and a.baseabsence = a.ref   -->zmieniaj tylko dla nieciaglych
        and not exists (select first 1 1 from eabsencebases b
                          where b.eabsence = a.baseabsence
                            and b.period = new.period)
     into :eabsref, status
  do begin
    if (status = 0) then
      insert into eabsencebases (eabsence, employee, period, sbase, takeit, vbase, workdays, vworkeddays, sworkeddays, sbasegross, vworkedhours)
      values (:eabsref, new.employee, new.period, new.sbase, new.takeit, new.vbase, new.workdays, new.vworkeddays, new.sworkeddays, new.sbasegross, new.vworkedhours);
  end
end^
SET TERM ; ^
