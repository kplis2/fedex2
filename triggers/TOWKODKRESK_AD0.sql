--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWKODKRESK_AD0 FOR TOWKODKRESK                    
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable towkodkreskref integer;
begin
  if(old.gl = 1) then begin
    /*przeniesenie glownosci na inny rekord*/
    select min(ref) from TOWKODKRESK where wersjaref = old.wersjaref into :towkodkreskref;
    if(:towkodkreskref > 0) then
      update TOWKODKRESK set GL = 1 where ref=:towkodkreskref;
    else
      update WERSJE set KODKRESK = '' where ref =old.wersjaref;
  end

end^
SET TERM ; ^
