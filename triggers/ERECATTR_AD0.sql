--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECATTR_AD0 FOR ERECATTR                       
  ACTIVE AFTER DELETE POSITION 0 
AS
declare RecCand_ref int;
begin
  /* przeliczam wszystkie oceny !*/
  for select REF from ereccands where RECPOST = old.RECPOST into :RecCand_ref do
    execute procedure eRecCandCompRating( :RecCand_ref );
end^
SET TERM ; ^
