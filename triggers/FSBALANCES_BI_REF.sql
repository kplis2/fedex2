--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSBALANCES_BI_REF FOR FSBALANCES                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FSBALANCES')
      returning_values new.REF;
end^
SET TERM ; ^
