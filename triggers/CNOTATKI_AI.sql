--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CNOTATKI_AI FOR CNOTATKI                       
  ACTIVE AFTER INSERT POSITION 0 
AS
DECLARE VARIABLE PROCED VARCHAR(255);
begin
  if (new.stable is not null and new.refdok is not null) then
  begin
    proced = 'update '||new.STABLE||' set CNOTATKA=1 where cnotatka=0 or cnotatka is null and REF='||new.refdok;
    execute statement :proced;
  end
end^
SET TERM ; ^
