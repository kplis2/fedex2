--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENNIK_AU0 FOR DEFCENNIK                      
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.akt <> old.akt ) then begin
     update CENNIK set akt = new.akt where cennik = new.ref;
   end
  if(new.waluta <> old.waluta) then
     update CENNIK set WALUTA = NEW.WALUTA where cennik = new.ref;
  if((new.replicat <> old.replicat) or (new.replicat is not null and old.replicat is null) or (new.replicat is null and old.replicat is not null)) then
     update CENNIK set REPLICAT = new.REPLICAT where cennik = new.ref;
end^
SET TERM ; ^
