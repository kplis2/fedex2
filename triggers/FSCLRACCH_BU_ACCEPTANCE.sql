--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCH_BU_ACCEPTANCE FOR FSCLRACCH                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable numberg varchar(40);
  declare variable currdebit2c numeric(14,2);
  declare variable currcredit2c numeric(14,2);
  declare variable wconfig varchar(40);
  declare variable operid integer;
  declare variable dokument integer;
  declare variable accpt integer;
-- sprawdzanie typu
  declare variable mindef integer;
  declare variable minpos integer;
  declare variable minacc ACCOUNT_ID;
  declare variable maxdef integer;
  declare variable maxpos integer;
  declare variable maxacc ACCOUNT_ID;
  declare variable local integer;
  declare variable operdate timestamp;
begin
  execute procedure CHECK_LOCAL('FSCLRACCH', old.ref) returning_values local;

  if (old.status < 1 and new.status > 0 and local = 1) then
  begin
    select min(p.dictdef), min(p.dictpos), min(p.account),
        max(p.dictdef), max(p.dictpos), max(p.account)
      from fsclraccp P
      where P.fsclracch = new.ref
      into :mindef, :minpos, :minacc, :maxdef, :maxpos, :maxacc;

    -- 0/1 rozliczenie/kompensata
    if (minDEF = maxDEF and minPOs = maxPOs and minacc = maxacc) then
      new.fstype = 0; -- to jest rozliczenie
    else
      new.fstype = 1; -- to jest kompensata

    select count(*)
      from fsclraccp P
      where P.fsclracch = new.ref
      into :minpos;
    if (minpos < 2) then
      exception universal 'Nie można zaakceptować rozliczenia/kompensaty bez pozycji';
  end

  if (old.status < 1 and new.status = 1 and new.fstype = 0 and local = 1) then
  begin
    select max(operdate)
      from fsclraccp
      where fsclracch = NEW.REF
    into :operdate;
    update fsclraccp set operdate = :operdate where fsclracch = new.ref;

    select sum(currdebit2c), sum(currcredit2c)
      from fsclraccp
      where fsclracch = new.ref
      into :currdebit2c, :currcredit2c;
    if (currdebit2c <> currcredit2c) then
      exception CANT_ACCEPT_CRLACC;
    execute procedure GET_CONFIG('NUMBERGEN_KOMPENSAT', 2)
      returning_values :numberg;
    if (numberg is null or numberg = '') then
      exception NOTYNAGGEN_NOTDEFINED 'Numerator dla rozliczeń rozrachunków nie zdefiniowany';
    if (new.number is null or new.number=0) then
      execute procedure get_number(:numberg, 'KOMP', 'KOMP', new.regdate, 0, new.ref)
        returning_values new.number, new.symbol;
    new.accdate = current_timestamp(0);
    /*BS20084
    execute procedure FS_REG_SETTLEMENTS_FROM_CRLACC(new.ref);
    */
  end
  if (new.status = 0 and old.status > 0 and new.fstype = 0) then
  begin
    new.accdate = null;
    delete from rozrachp where fsclracch = new.ref;
    update fsclraccp set cleared = 0 where fsclracch = new.ref;
    --uwolnic numer
  end

  if (old.status < 1 and new.status = 1 and new.fstype = 1 and local = 1) then
  begin
    select max(operdate)
      from fsclraccp
      where fsclracch = NEW.REF
    into :operdate;
    update fsclraccp set operdate = :operdate where fsclracch = new.ref;

-- slownikk wartosci pola FSTYPEAKC
-- 0 - dokument z FK (rozliczenie generuje zapisy, komensata nie)
-- 1 - dokument SID - zapisy zawsze
-- 2 - dokument SID - wspolpraca z FK
-- 3 - dokument SID - wspolpraca z FK + aut. dekretacja
    if (new.fstypeakc <> 1) then
    begin
      if (new.fstypeakc = 3 or new.fstypeakc = 0) then
      begin
        execute procedure get_global_param('AKTUOPERATOR')
          returning_values :wconfig;
        operid = cast(wconfig as integer);
        execute procedure GET_CONFIG('DOK_KS_FOR_KOMPESN', 2)
          returning_values :wconfig;
        dokument = cast(wconfig as integer);
        execute procedure GET_CONFIG('ACCEPT_WITH_IMPORT', 2)
          returning_values :wconfig;
        accpt = cast(wconfig as integer);
        execute procedure GET_CONFIG('NUMBERGEN_KOMPENSAT', 2)
          returning_values :numberg;
        if (numberg is null or numberg = '') then
          exception NOTYNAGGEN_NOTDEFINED 'Numerator dla kompensowania rozrachunków nie zdefiniowany';
        if (new.number is null or new.number=0) then  -- po odakceptowaniu nie nadajemy ponownie
          execute procedure get_number(:numberg, 'KOMP', 'KOMP', new.regdate, 0, new.ref)
            returning_values new.number, new.symbol;
        new.accdate = current_timestamp(0);

        execute procedure compensation_book(NEW.ref, dokument, operid, accpt, new.docdate, new.symbol)
          returning_values :dokument;
        if(exists(select ref from bkdocs where ref = :dokument)) then
          new.status = 2;
      end
    end

    if (new.fstypeakc = 1) then
    begin
      -- jak to ma być w sid to jest narazie dla mnie zagadka
      -- zrobiem identyko jak rozliczenia
      select sum(currdebit2c), sum(currcredit2c)
        from fsclraccp
        where fsclracch = new.ref
        into :currdebit2c, :currcredit2c;
      if (currdebit2c <> currcredit2c) then
        exception CANT_ACCEPT_CRLACC;
      execute procedure GET_CONFIG('NUMBERGEN_CLRACC', 2)
        returning_values :numberg;
      if (numberg is null or numberg = '') then
        exception NOTYNAGGEN_NOTDEFINED 'Numerator dla rozliczeń rozrachunków nie zdefiniowany';
      if (new.number is null or new.number=0) then  -- po odakceptowaniunie nadajemy ponownie
        execute procedure get_number(:numberg, 'ROZL', 'ROZL', new.regdate, 0, new.ref)
          returning_values new.number, new.symbol;
      new.accdate = current_timestamp(0);
      /*BS20084
      execute procedure FS_REG_SETTLEMENTS_FROM_CRLACC(new.ref);
      */
    end
  end

--  if (new.status = 0 and old.status > 0 and new.fstype = 0) then
  if ((new.status = 0 and old.status > 0) and ((new.fstype = 0) or (new.fstypeakc = 1 and new.fstype = 1))) then
  begin
    new.accdate = null;
    delete from rozrachp where fsclracch = new.ref;
    update fsclraccp set cleared = 0 where fsclracch = new.ref;
    --uwolnic numer
  end

  if (new.status = 0 and old.status = 1 and new.fstype = 1) then
  begin
    new.accdate = null;
    update fsclraccp set cleared = 0 where fsclracch = new.ref;
  end

  if (new.status < 2 and old.status = 2 and new.fstype = 1) then
  begin
    update bkdocs set status = 0 where otable = 'FSCLRACCH' and oref = new.ref;
    delete from bkdocs b where B.otable = 'FSCLRACCH' and b.oref = new.ref;
    update fsclraccp set cleared = 0 where fsclracch = new.ref;
    --new.number = 0;
  end
end^
SET TERM ; ^
