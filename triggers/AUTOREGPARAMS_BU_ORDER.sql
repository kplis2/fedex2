--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AUTOREGPARAMS_BU_ORDER FOR AUTOREGPARAMS                  
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable maxnum integer;
begin
  if (NEW.ord is null) then new.ord = 1;
  if (new.numer is null) then new.numer = old.numer;
  if (new.ord = 0 or new.numer = old.numer) then
  begin
    new.ord = 1;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.numer <> old.numer) then
    select max(numer) from autoregparams where autoregschem = new.autoregschem
      into :maxnum;
  else
    maxnum = 0;
  if (new.numer < old.numer) then
  begin
    update autoregparams set ord = 0, numer = numer + 1
      where (autoregschem <> old.autoregschem or parametr <> old.parametr) and numer >= new.numer
        and numer < old.numer and autoregschem = new.autoregschem;
  end else if (new.numer > old.numer and new.numer <= maxnum) then
  begin
    update autoregparams set ord = 0, numer = numer - 1
      where (autoregschem <> old.autoregschem or parametr <> old.parametr) and numer <= new.numer
        and numer > old.numer and autoregschem = new.autoregschem;
  end else if (new.numer > old.numer and new.numer > maxnum) then
    new.numer = old.numer;
end^
SET TERM ; ^
