--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSBALANCES_BI0 FOR FSBALANCES                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.dictgrp is not null) then
    select max(grpdebit), max(grpcredit)
      from fsbalances where dictgrp = new.dictgrp and company = new.company and currency = new.currency
      into new.grpdebit, new.grpcredit;
  if (new.grpdebit is null) then new.grpdebit = 0;
  if (new.grpcredit is null) then new.grpcredit = 0;
  if (new.btranamountbank is null) then new.btranamountbank = 0;
  if (new.grpbtranamountbank is null) then new.grpbtranamountbank = 0;

end^
SET TERM ; ^
