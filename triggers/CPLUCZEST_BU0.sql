--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_BU0 FOR CPLUCZEST                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cplstatus integer;
declare variable nazwaucz varchar(255);
declare variable nazwacpl varchar(255);
begin
  if(new.ilpkt is null) then new.ilpkt = 0;
  if(new.ilpkt <> old.ilpkt) then begin
    select STATUS from CPL where REF=new.cpl into :cplstatus;
    if(:cplstatus <> 1) then exception CPL_NIEAKTYWNY;
  end
  if(new.ilpkt <> old.ilpkt) then
    new.datalast = current_date;
  if(new.akt = 0 and old.akt = 1) then
    new.datadeaktyw = current_date;
  else if(new.akt = 1 and old.akt = 0) then
    new.datadeaktyw = null;
  if((new.pkosoba <> old.pkosoba) or (new.pkosoba is not null and old.pkosoba is null) or (new.pkosoba is null and old.pkosoba is not null)
   or (new.cpodmiot <> old.cpodmiot) or (new.cpodmiot is not null and old.cpodmiot is null) or (new.cpodmiot is null and old.cpodmiot is not null)
   or (new.cpl <> old.cpl) or (new.cpl is not null and old.cpl is null) or (new.cpl is null and old.cpl is not null)
   or (new.nrkarty <> old.nrkarty) or (new.nrkarty is not null and old.nrkarty is null) or (new.nrkarty is null and old.nrkarty is not null)
   or (new.nazwa <> old.nazwa) or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null)
   or (new.skrot <> old.skrot) or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null)
  ) then begin
    select NAZWA from CPL where REF=new.cpl into :nazwacpl;
    if(new.pkosoba is not null) then begin
      select NAZWA from PKOSOBY where PKOSOBY.REF = new.pkosoba into :nazwaucz;
      select SKROT,NAZWA,CSTATUS from CPODMIOTY where CPODMIOTY.REF = new.cpodmiot into new.skrot, new.nazwafirmy, new.cstatus;
    end else if(new.cpodmiot is not NULL) then begin
      select SKROT,SKROT,NAZWAFIRMY,CSTATUS from CPODMIOTY where CPODMIOTY.REF = new.cpodmiot into new.skrot, :nazwaucz, new.nazwafirmy, new.cstatus;
    end
    if(nazwaucz is null) then nazwaucz = '';
    if(nazwacpl is null) then nazwacpl = '';
    if(new.nrkarty is not null and new.nrkarty <> '') then new.nazwa = substring(nazwaucz||' - '||nazwacpl||' - '||new.nrkarty from 1 for 80);
    else new.nazwa = substring(nazwaucz||' - '||nazwacpl from 1 for 80);
  end
end^
SET TERM ; ^
