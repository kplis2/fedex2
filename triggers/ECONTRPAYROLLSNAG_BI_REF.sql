--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRPAYROLLSNAG_BI_REF FOR ECONTRPAYROLLSNAG              
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECONTRPAYROLLSNAG')
      returning_values new.REF;
end^
SET TERM ; ^
