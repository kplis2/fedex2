--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSHISTORY_AD FOR PRTOOLSHISTORY                 
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if (not exists (select first 1 1 from prtoolshistory p
    where p.prtools = old.prtools and p.tooltype = old.tooltype and coalesce(p.price,0) = coalesce(old.price,0)
      and coalesce(p.invno,'') = coalesce(old.invno,'') and p.prdsdef = old.prdsdef)
    and exists (select first 1 1 from prtools t where t.symbol = old.prtools and t.tooltype = old.tooltype
      and coalesce(t.price,0) = coalesce(old.price,0) and coalesce(t.invno,'') = coalesce(old.invno,'')
      and t.prdsdef = old.prdsdef and t.amount = 0)
  ) then
    delete from prtools t where t.symbol = old.prtools and t.tooltype = old.tooltype
      and coalesce(t.price,0) = coalesce(old.price,0) and coalesce(t.invno,'') = coalesce(old.invno,'')
      and t.prdsdef = old.prdsdef and t.amount = 0;
end^
SET TERM ; ^
