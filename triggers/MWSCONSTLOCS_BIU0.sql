--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCS_BIU0 FOR MWSCONSTLOCS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable deliveryarea smallint;
begin
  if (new.deep is null) then new.deep = 0;
  if (new.stocktaking is null) then new.stocktaking = 0;
  if (new.act is null) then new.act = 1;
  if (new.refill is null) then new.refill = 0;
  if (new.indyviduallot is null) then new.indyviduallot = 0;
  if (new.goodssellav is null) then
  begin    -- towary są dostpne do sprzedazy tylko na sektorach
    select deliveryarea from whsecs where ref = new.whsec into deliveryarea;
    if (deliveryarea is null or (deliveryarea <> 2 and deliveryarea <> 6 and deliveryarea <> 7)) then
      new.goodssellav = 0;
    else
      new.goodssellav = 1;
  end
  if (new.goodsav is null) then new.goodsav = 0;
  if (new.actchangedatetime is null) then new.actchangedatetime = current_timestamp(0);
  if (new.act <> old.act) then new.actchangedatetime = current_timestamp(0);
  if (new.wcategory is null or new.wcategory = '') then new.wcategory = 'A';
  if (new.locdest is null and new.mwsconstloctype is not null) then
    select locdest from mwsconstloctypes where ref = new.mwsconstloctype
      into new.locdest;
end^
SET TERM ; ^
