--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERATOR_BU0 FOR OPERATOR                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  /* sprawdzenie prawidlowego przypisania przelozonego */
  if (new.przelozony<>old.przelozony and old.ref = new.ref and new.ref = new.przelozony) then
    exception superior_check;
end^
SET TERM ; ^
