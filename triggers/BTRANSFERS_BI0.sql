--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERS_BI0 FOR BTRANSFERS                     
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  if (new.towho is null) then new.towho = '';
  if (new.toacc is null) then new.toacc = '';
  if (new.amount is null) then new.amount = 0;
  if (new.data is null) then new.data = current_date;
  if (new.dataopen is null) then new.dataopen = current_date;
  if (new.slodef > 0 and new.slopoz > 0) then
    select kodks
      from slo_daneks(new.slodef, new.slopoz)
      into new.slokodks;
  select RIGHTS, RIGHTSGRUP
    from BTRANTYPE
    where SYMBOL = new.btype
    into new.rights,  new.rightsgrup;
  if (new.rights is null) then new.rights = '';
  if (new.rightsgrup is null) then new.rightsgrup = '';
  if (new.btrangroup is null or new.btrangroup = 0) then new.btrangroup = new.ref;
  if (new.status is null) then new.status = 0;
end^
SET TERM ; ^
