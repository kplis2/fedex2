--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECSTATUS_BI FOR ERECSTATUS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_erecstatus,1);
end^
SET TERM ; ^
