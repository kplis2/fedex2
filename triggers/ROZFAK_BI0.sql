--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZFAK_BI0 FOR ROZFAK                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.sumwartnet is null) then new.sumwartnet = 0;
  if(new.sumwartbru is null) then new.sumwartbru = 0;
  if(new.sumwartvat is null) then new.sumwartvat = new.sumwartbru - new.sumwartnet;
  if(new.psumwartvat is null) then new.psumwartvat = new.psumwartbru - new.psumwartnet;
  if(new.sumwartnetzl is null) then new.sumwartnetzl = 0;
  if(new.sumwartbruzl is null) then new.sumwartbruzl = 0;
  if(new.sumwartvatzl is null) then new.sumwartvatzl = new.sumwartbruzl - new.sumwartnetzl;
  if(new.psumwartvatzl is null) then new.psumwartvatzl = new.psumwartbru - new.psumwartnetzl;
  if(new.blokadaprzelicz is null) then new.blokadaprzelicz = 0;

end^
SET TERM ; ^
