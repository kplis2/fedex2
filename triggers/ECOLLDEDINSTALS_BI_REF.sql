--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLLDEDINSTALS_BI_REF FOR ECOLLDEDINSTALS                
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('ECOLLDEDINSTALS') returning_values new.ref;
end^
SET TERM ; ^
