--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTCEN_AU0 FOR DOSTCEN                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable stawka numeric(14,4);
declare variable przelicz numeric(14,4);
declare variable waluta varchar(10);
declare variable oddzial varchar(20);
begin
  if(new.gl = 1 and old.gl = 0) then  --skasuj znacznik GL w pozostalych rekordach tego towaru
    update DOSTCEN set GL = 0 where GL = 1 and WERSJAREF = new.wersjaref and (DOSTAWCA <> new.dostawca or WALUTA <> new.waluta or ODDZIAL <> new.oddzial);
  if(new.cenazakgl = 1 and old.cenazakgl = 0) then  --skasuj znacznik CENAZAKGL w pozostalych rekordach tego towaru
    update DOSTCEN set CENAZAKGL = 0 where CENAZAKGL = 1 and WERSJAREF = new.wersjaref and (DOSTAWCA <> new.dostawca or WALUTA <> new.waluta or ODDZIAL <> new.oddzial);

  if(new.dostawca<>old.dostawca) then begin
    if(old.akt=1) then begin --jesli na starym dostawcy byl akt, to musimy go przeniesc na inny rekord
      waluta = null;
      oddzial = null;
      select first 1 WALUTA,ODDZIAL from DOSTCEN where WERSJAREF = new.wersjaref and DOSTAWCA = old.dostawca
      into :waluta, :oddzial;
      if(:waluta is not null) then
        update DOSTCEN set AKT=1 where AKT=0 and WERSJAREF=new.wersjaref and DOSTAWCA = old.dostawca and WALUTA = :waluta and ODDZIAL = :oddzial;
    end
    if(new.akt=1) then begin --na nowym dostawcy moze juz byc akt, wiec trzeba go skasowac
      update DOSTCEN set AKT = 0 where AKT = 1 and WERSJAREF = new.wersjaref and DOSTAWCA = new.dostawca and (WALUTA <> new.waluta or ODDZIAL <> new.oddzial);
    end
    if(new.akt=0) then begin --nowy dostawca moze byc po raz pierwszy, wiec trzeba ustawic mu akt
      if(not exists(select first 1 1 from DOSTCEN where AKT=1 and WERSJAREF = new.wersjaref and DOSTAWCA = new.dostawca)) then
        update DOSTCEN set AKT=1 where AKT=0 and WERSJAREF=new.wersjaref and DOSTAWCA = new.dostawca and WALUTA = new.waluta and ODDZIAL = new.oddzial;
    end
  end else if(new.akt = 1 and old.akt = 0) then  --skasuj znacznik AKT w pozostalych rekordach tego towaru i dostawcy
    update DOSTCEN set AKT = 0 where AKT = 1 and WERSJAREF = new.wersjaref and DOSTAWCA = new.dostawca and (WALUTA <> new.waluta or ODDZIAL <> new.oddzial);

  if(new.cenazakgl = 1 and ((new.cenanet <> old.cenanet) or (new.cena_koszt <> old.cena_koszt) or
     (new.cena_fab <> old.cena_fab) or (new.cenazakgl <> old.cenazakgl) or (new.jedn <> old.jedn))) then begin
    select STAWKA from TOWARY join VAT on (TOWARY.VAT = VAT.GRUPA) where TOWARY.KTM = new.ktm into :stawka;
    stawka = 1 + :stawka/100;
    if(new.jedn > 0) then
      select przelicz from TOWJEDN where REF=new.jedn into :przelicz;
    else
      przelicz = 1;
    update WERSJE set CENA_ZAKN = new.cenanet / :przelicz, CENA_ZAKB = new.cenanet * :stawka / :przelicz,
                      CENA_FABN = new.cena_fab / :przelicz, cena_FABB = new.cena_FAB * :stawka / :przelicz,
                      CENA_KOSZTN = new.cena_koszt / :przelicz, cena_KOSZTB = new.cena_koszt * :stawka / :przelicz
       where REF=new.wersjaref;
  end
  if(new.gl = 1 and ((new.gl <> old.gl) or (new.symbol_dost <> old.symbol_dost))) then begin
    update WERSJE set SYMBOL_DOST = new.symbol_dost
       where REF=new.wersjaref;
  end
end^
SET TERM ; ^
