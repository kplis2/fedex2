--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_BU_WPK FOR SLODEF                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  -- sprawdzay czy to slownik zakladowy. Jak tak to sprawdamy czy nie podlega
  -- synchronizacji bo jezeli tak to nie pozwol edytowac
  if (new.company is not null) then
  begin
    -- zatem slownik indywidualny - sprawdz synchronizacje
    if (new.patternref is not null) then
    begin
      -- w takim razie skoro podlega sync to tylko wzorzec moze modyfikowac
      -- sprawdzamy cz faktyczie update puscil wzorzec
      if (coalesce(new.internalupdate,0)=0) then
      begin
        -- mamy sytuacje ze nalezy snchronizowac ze wzorcem
        select nazwa, typ, predef, crm, kasa, prefixfk, gridname, gridnamedict, formname,
          firma, trybred, mag, opak, kartoteka, analityka, kodln, state, token, isdist,
          mastertable, logistyka, personel, bkbrowsewindow, produkcja, symbol, dictposfield,
          kodfield, nazwafield, kodksfield, nipfield, iscompany, multidist
        from slodef
        where ref = new.patternref
        into new.nazwa, new.typ, new.predef, new.crm, new.kasa, new.prefixfk, new.gridname, new.gridnamedict, new.formname,
          new.firma, new.trybred, new.mag, new.opak, new.kartoteka, new.analityka, new.kodln,  new.state, new.token, new.isdist,
          new.mastertable, new.logistyka, new.personel, new.bkbrowsewindow, new.produkcja, new.symbol, new.dictposfield,
          new.kodfield, new.nazwafield, new.kodksfield, new.nipfield, new.iscompany, new.multidist;
      end else begin
        -- internal wiec zakladam ze wartosci ok, tylko zeruje internal
        new.internalupdate = 0;
      end
    end
  end
end^
SET TERM ; ^
