--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDALGORITHMS_BI1 FOR EDALGORITHMS                   
  ACTIVE BEFORE INSERT POSITION 1 
AS
  declare variable lastvar1 integer;
  declare variable lastvar2 integer;
  declare variable lastvar3 integer;
begin
  execute procedure STATEMENT_MULTIPARSE(new.proced1, 'ED_', '', '', 1, 2)
    returning_values new.decl1, new.body1, :lastvar1;
  execute procedure STATEMENT_MULTIPARSE(new.proced2, 'ED_', '', '', 1, 2)
    returning_values new.decl2, new.body2, :lastvar2;
  execute procedure STATEMENT_MULTIPARSE(new.proced3, 'ED_', '', '', 1, 2)
    returning_values new.decl3, new.body3, :lastvar3;
end^
SET TERM ; ^
