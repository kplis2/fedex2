--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BI_PRSHMAT FOR POZZAM                         
  ACTIVE BEFORE INSERT POSITION 5 
as
declare variable cnt integer;
declare variable sheet integer;
begin
  if (new.prshmat is null and exists(select ref from nagzam where ref = new.zamowienie and prsheet is not null and typ in (0,1,3))) then
  begin
    select prsheet
      from nagzam
      where ref = new.zamowienie
      into :sheet;

    if (:sheet is null) then
      exit;

    select count(pm.ref)
      from prshmat pm
      where pm.wersjaref = new.wersjaref
        and pm.sheet = :sheet
      into :cnt;

    if (cnt = 1) then
      select pm.ref
        from prshmat pm
        where pm.wersjaref = new.wersjaref
          and pm.sheet = :sheet
        into new.prshmat;
    else
        exception PRSCHEDGUIDES_ERROR 'Brak wskazania surowca z k.tech';
  end 
end^
SET TERM ; ^
