--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AU_UPDATE_SRVCONTRACTS FOR KLIENCI                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable ktel varchar(255);
begin
  ktel = coalesce(new.telefon,'') ;
  if (new.komorka is not null and new.komorka <> '') then
  begin
    ktel = ktel || '|' || new.komorka;
  end

  if ((ktel<>old.telefon) or (new.fskrot <> old.fskrot) or (old.prostynip<>new.prostynip)
      or (new.ulica<>old.ulica) or (new.nrdomu<>old.nrdomu) or (new.nrlokalu<>old.nrlokalu)
      or (new.miasto<>old.miasto) or (new.kodp<>old.kodp)) then
  begin
    update srvcontracts s
      set  s.name = new.fskrot,
           s.taxid = new.prostynip,
           s.street = new.ulica,
           s.houseno = new.nrdomu,
           s.localno = new.nrlokalu,
           s.city = new.miasto,
          s.postcode = new.kodp,
           s.phone = :ktel
        where s.dictpos = new.ref and s.dictdef=1;
  end
end^
SET TERM ; ^
