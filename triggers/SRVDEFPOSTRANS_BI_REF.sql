--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOSTRANS_BI_REF FOR SRVDEFPOSTRANS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVDEFPOSTRANS')
      returning_values new.REF;
end^
SET TERM ; ^
