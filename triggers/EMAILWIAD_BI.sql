--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_BI FOR EMAILWIAD                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable lokres varchar(6);
BEGIN
  if(new.idodpna is not null) then begin
    select OKRES from EMAILWIAD where REF=new.idodpna into :lokres;
  end else begin
    lokres = new.okres;
    lokres = cast( extract( year from new.data) as char(4));
    if(extract(month from new.data) < 10) then
        lokres = :lokres ||'0' ||cast(extract(month from new.data) as char(1));
    else begin
        lokres = :lokres ||cast(extract(month from new.data) as char(2));
    end
  end
  --BS41102
  if(new.folder > 0) then
    select deffold.mailbox from deffold where ref = new.folder into new.mailbox;
  new.okres = :lokres;
END^
SET TERM ; ^
