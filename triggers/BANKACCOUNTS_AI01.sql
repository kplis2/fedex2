--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_AI01 FOR BANKACCOUNTS                   
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if(new.gl = 1) then begin
    update bankaccounts set GL = 0 where GL = 1 and DICTDEF = new.DICTDEF and DICTPOS = new.DICTPOS and ref <> new.ref;
    if(new.DICTDEF = 1) then
      update KLIENCI set BANK = new.bank, RACHUNEK = new.account where ref = new.DICTPOS;
    else if (new.DICTDEF = 6) then
      update DOSTAWCY set BANK = new.bank, RACHUNEK = new.account where ref = new.DICTPOS;
  end
  if(new.DICTDEF = 6) then
    update DOSTAWCY set STATE = -1 where REF=new.DICTPOS;
  else if(new.DICTDEF = 1) then
    update KLIENCI set STATE = -1 where REF=new.DICTPOS;
end^
SET TERM ; ^
