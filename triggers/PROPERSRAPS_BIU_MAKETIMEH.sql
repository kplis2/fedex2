--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_BIU_MAKETIMEH FOR PROPERSRAPS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable maketime double precision;
begin
  if (inserting) then
  begin
    if(new.MAKETIMEFROM is not null and new.MAKETIMETO is not null
       and (new.MAKETIMEHOURS is null or new.MAKETIMEHOURS=0)
    ) then
    begin
      maketime = new.MAKETIMETO - new.MAKETIMEFROM;
      maketime = maketime * 24;
      new.maketimehours = cast(maketime as numeric(14,2));
    end
  end else if (updating) then
  begin
    if(new.MAKETIMEFROM is not null and new.MAKETIMETO is not null
       and (((new.maketimefrom is not null and old.maketimefrom is null) or new.maketimefrom <> old.maketimefrom)
         or ((new.maketimeto is not null and old.maketimeto is null) or new.maketimeto <> old.maketimeto))
    ) then
    begin
      maketime = new.MAKETIMETO - new.MAKETIMEFROM;
      maketime = maketime * 24;
      new.maketimehours = cast(maketime as numeric(14,2));
    end
  end
end^
SET TERM ; ^
