--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BD_ORDER FOR RKDOKPOZ                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then
    exit;
  select max(lp) from rkdokpoz where dokument = old.dokument
     into :maxnum;
  if (old.lp = :maxnum) then
    exit;
  update rkdokpoz set ord = 0, lp = lp - 1
    where ref <> old.ref and lp > old.lp and dokument = old.dokument;
end^
SET TERM ; ^
