--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCH_BI0 FOR FSCLRACCH                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable country_currency varchar(3);
  declare variable symbol varchar(100);
  declare variable name varchar(255);
  declare variable account varchar(30);
begin

  if (new.rights is null) then new.rights = '';
  if (new.rightsgroup is null) then new.rightsgroup = '';
  if (new.fstype is null) then
    new.fstype = 0;
  if (new.status is null) then
    new.status = 0;

  if (new.fstypeakc is null) then
    execute procedure GET_CONFIG('SIDFK_ROZLKOMPEN', 2)
      returning_values new.fstypeakc;

  if (new.regdate is null) then
    new.regdate = current_timestamp(0);

  if (new.docdate is null) then
    new.docdate = current_date;

  select id from bkperiods
    where company = new.company and ptype=1 and sdate<= cast(new.regdate as date)
      and fdate >= cast(new.regdate as date)
    into new.period;

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values :country_currency;
  if (country_currency = new.currency) then
    new.multicurr = 0;
  else
    new.multicurr = 1;

  if (new.dictpos is not null) then
  begin
    execute procedure SLO_DANE(new.dictdef, new.dictpos)
      returning_values :symbol, :name, :account;
    new.dictsymbol = substring(:symbol from 1 for 80);
  end
  new.okres = cast( extract( year from new.regdate) as char(4));
  if(extract(month from new.regdate) < 10) then
       new.okres = new.okres ||'0' ||cast(extract(month from new.regdate) as char(1));
  else begin
       new.okres = new.okres ||cast(extract(month from new.regdate) as char(2));
  end
end^
SET TERM ; ^
