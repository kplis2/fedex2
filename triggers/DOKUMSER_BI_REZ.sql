--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMSER_BI_REZ FOR DOKUMSER                       
  ACTIVE BEFORE INSERT POSITION 5 
AS
declare variable nser smallint;
declare variable wydania smallint;
declare variable mag varchar(3);
declare variable werref integer;
begin
  if(new.typ = 'M') then
    select d.notserial, m.wydania, n.magazyn, p.wersjaref
      from DOKUMPOZ P
        left join dokumnag N on (P.dokument = n.ref)
        left join defmagaz d on (N.magazyn = d.symbol)
        left join defdokum m on (N.typ = m.symbol)
      where P.REF = new.refpoz into :nser, :wydania, :mag, :werref;
  else if(new.typ = 'F') then
    select d.notserial, (n.zakup - 1)* -1, n.magazyn, p.wersjaref
      from pozfak P
        left join nagfak N on (P.dokument = N.ref)
        left join defmagaz d on (d.symbol = n.magazyn)
      where p.REF=new.refpoz into :nser, :wydania, :mag, :werref;
  else if(new.typ = 'Z') then
    select d.notserial, t.wydania, n.magazyn, p.wersjaref
      from pozzam P
        left join nagzam N on (p.zamowienie = n.ref)
        left join defmagaz d on (N.magazyn = d.symbol)
        left join typzam t on (t.symbol = n.typzam)
      where p.REF=new.refpoz into :nser, :wydania, :mag, :werref;

  if (new.orgdokumser is null and new.stan = 'B' and nser = 0 and wydania = 1) then begin
    execute procedure DOKUMSER_ADD_REZ(:mag, :werref, new.odserial, new.ilosc);
  end
end^
SET TERM ; ^
