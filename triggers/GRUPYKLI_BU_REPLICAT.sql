--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER GRUPYKLI_BU_REPLICAT FOR GRUPYKLI                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.ref <> old.ref)
   or(new.opis <> old.opis ) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
   or(new.cennik <> old.cennik ) or (new.cennik is null and old.cennik is not null) or (new.cennik is not null and old.cennik is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('GRUPYKLI',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
