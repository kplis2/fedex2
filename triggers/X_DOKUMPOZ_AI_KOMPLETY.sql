--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMPOZ_AI_KOMPLETY FOR DOKUMPOZ                       
  ACTIVE AFTER INSERT POSITION 133 
as
begin
--przepisywanie na fake slowników (inwestycji)
  if (coalesce(new.x_slownik,0) <> 0
        and coalesce(new.havefake,0) = 1) then begin
    update dokumpoz dp set dp.x_slownik = new.x_slownik, dp.int_id = new.int_id,
        dp.int_sesjaostprzetw = new.int_sesjaostprzetw,
        dp.int_dataostprzetw = new.int_dataostprzetw,
        dp.int_zrodlo = new.int_zrodlo
        where dp.alttopoz = new.ref;
  end
end^
SET TERM ; ^
