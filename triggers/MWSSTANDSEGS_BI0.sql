--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDSEGS_BI0 FOR MWSSTANDSEGS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.mwsconstlocsq is null) then new.mwsconstlocsq = 1;
  if (new.w is null) then new.w = 0;
  if (new.l is null) then new.l = 0;
  select symbol from whsecrows where ref = new.whsecrow into new.whsecrowdict;
  select symbol from whsecs where ref = new.whsec into new.whsecdict;
end^
SET TERM ; ^
