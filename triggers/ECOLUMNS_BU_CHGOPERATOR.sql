--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLUMNS_BU_CHGOPERATOR FOR ECOLUMNS                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.number <> old.number or new.name <> old.name or new.coltype <> old.coltype
    or coalesce(new.cflags,'') <> coalesce(old.cflags,'') or coalesce(new.typ,'') <> coalesce(old.typ,'')) then
  begin
    execute procedure get_global_param ('AKTUOPERATOR')
      returning_values new.chgoperator;
    new.chgtimestamp = current_timestamp(0);
  end
end^
SET TERM ; ^
