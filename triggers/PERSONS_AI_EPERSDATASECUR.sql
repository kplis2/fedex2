--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_AI_EPERSDATASECUR FOR PERSONS                        
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable aktuoperator integer;
begin
  execute procedure get_global_param ('AKTUOPERATOR')
    returning_values aktuoperator;

  insert into epersdatasecur (person, kod, descript, operator)
    values (new.ref, 0, 'Rejestracja danych osobowych', :aktuoperator);
end^
SET TERM ; ^
