--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AD0 FOR NAGFAK                         
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable numberg varchar(40);
declare variable local smallint;
declare variable BLOCKREF integer;
declare variable status integer;
declare variable msg varchar(4096);
begin
  execute procedure CHECK_LOCAL('NAGFAK',old.ref) returning_values :local;
  if(old.numer > 0 and :local = 1) then begin
     select number_gen from REJFAK where old.rejestr = REJFAK.symbol into :numberg;
     if(old.zakup = 1) then
       execute procedure free_number(:numberg,old.rejestr,old.typ,old.dataotrz,old.numer, 0) returning_values :blockref;
     else
       execute procedure free_number(:numberg,old.rejestr,old.typ,old.data,old.numer,0) returning_values :blockref;
  end
  if(old.fromnagzam>0 and old.historia>0 and old.skad=0) then
    execute procedure OPER_BACK_OPER(old.fromnagzam,old.historia,0,0,0) returning_values :status,:msg;
  update DOKUMNAG set FAKTURA = NULL where FAKTURA = old.ref;
  update NAGZAm set FAKTURA = NULL, SYMBFAK='', FAKDEPEND = 0 where FAKTURA=old.ref;
  update NAGFAK set REFFAK = NULL, SYMBOLFAK = '' where REFFAK = old.ref;
  if(old.numblock > 0) then
    update NUMBERBLOCK set DOKNAGFAK = null where ref=old.numblock;
  update LISTYWYSD set KOSZTDOSTFAKTURA = null where KOSZTDOSTFAKTURA = old.ref;
  if(old.listywysd > 0) then
    execute procedure listywysd_obliczpobranie(old.listywysd);
  delete from ROZRACHROZ where DOKTYP = 'F' and DOKREF = old.ref;
  delete from MSGORDS where TYPDOK='F' and REFDOK = old.ref;
  delete from cploper
    where cploper.typdok='NAGFAK' and cploper.refdok=old.ref;
  delete from NAGFAKTERMPLAT where doktyp='F' and dokref = old.ref;
  if(exists(select d.ref from dokumnag d where d.fakturawolna = old.ref)) then
    update dokumnag d set d.fakturawolna = null where d.fakturawolna = old.ref;

  delete from dokzlacz d where d.ztable = 'NAGFAK' and d.zdokum = old.ref;

end^
SET TERM ; ^
