--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BU0 FOR ROZRACH                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable kod varchar(100);
declare variable nazwa varchar(155);
declare variable kontofk ACCOUNT_ID;
declare variable datazamk timestamp;
declare variable datazamklastoper timestamp;
declare variable cnt integer;
declare variable wartosc varchar(1024);
declare variable fdate timestamp;
begin
  if (new.DATAOTW is not null and new.DATAPLAT is not null and
     (new.dataotw<>old.dataotw or new.dataplat<>old.dataplat)
     and new.DATAOTW>new.DATAPLAT) then
      exception ROZRACH_DATA_OTW;

  if (new.kontofk is null) then new.kontofk = '';
  if (new.ma is null) then new.ma = 0;
  if (new.winien is null) then new.winien = 0;
  if (new.odsetki = 0) then new.odsetki = null;
  if (new.dataplat is null) then new.dataplat = new.dataotw;
  if (new.planpayday is null) then new.planpayday = new.dataplat;
  if (new.rkdokpozamount is null) then new.rkdokpozamount = 0;
  if (new.btranamountbank is null) then new.btranamountbank = 0;

  if (new.ma=new.winien and new.datazamk is null) then
  begin
    new.datazamk = null;
    new.datazamklastoper = null;
    select count(*), max(ROZRACHP.stransdate), max(ROZRACHP.data)
      from ROZRACHP
      where SLODEF = new.slodef and slopoz = new.slopoz
      and KONTOFK = new.kontofk and symbfak = new.symbfak
      and company=new.company
      into :cnt, :datazamk, :datazamklastoper;

    if (cnt>0) then
    begin
      new.datazamk = datazamk;
      new.datazamklastoper = :datazamklastoper;
      if (new.datazamk is null) then
        new.datazamk = current_date;
      if (new.datazamklastoper is null) then
        new.datazamklastoper = current_date;

      new.dni = new.datazamk - new.dataplat;
    end
  end else if (new.ma<>new.winien and new.datazamk is not null) then
  begin
    new.datazamk = null;
    new.datazamklastoper = null;
  end

  if (new.datazamk is null) then
    new.dni = current_date-cast(new.dataplat as date);

/*  if (new.faktura<>old.faktura or new.dataotw<>old.dataotw
       or new.dataplat<>old.dataplat or new.winien<>old.winien
       or new.ma<>old.ma)
  then
    new.state = 1;*//*Kuba - po co to byo ?*/

  if (new.oddzial is null or new.oddzial='') then
  begin
    if (new.faktura>0) then
      select ODDZIAL, sposzap from NAGFAK where ref = new.faktura
        into new.oddzial, new.sposzap;
  end
  else if (new.faktura>0) then
    select sposzap from NAGFAK where ref = new.faktura
      into new.sposzap;


  if (new.oddzial is null or new.oddzial = '') then
  begin
    execute procedure GETCONFIG('AKTUODDZIAL')
      returning_values new.oddzial;
  end

  if ((new.slokod is null or new.slokod = '') and new.slodef > 0 and new.slopoz > 0) then
  begin
    execute procedure SLO_DANE(new.slodef, new.slopoz)
      returning_values :kod, :nazwa, :kontofk;
    new.slokod = substring(kod from  1 for 80);
  end

  new.saldo = new.winien - new.ma;
  new.saldowm = new.winien - new.ma;
  new.saldowmzl = new.winienzl - new.mazl;
  if (new.saldo < 0) then new.saldo = -new.saldo;
--  new.saldodoprzelewu = new.ma - new.winien - new.btranamountbank;
--  if (new.saldodoprzelewu < 0) then new.saldodoprzelewu = 0;
  /*if (new.zamk = 1 and new.saldo <> 0) then
    new.zamk = 0;*/



  if (new.saldo<>old.saldo) then begin
    select k.wartosc from konfig k where k.akronim = 'ROZRACHZAMK'
    into :wartosc;
    wartosc = coalesce(:wartosc,'0');
    if (:wartosc = '1' ) then
        if (new.saldo = 0 ) then new.zamk = 1;
        else new.zamk = 0;
  end

end^
SET TERM ; ^
