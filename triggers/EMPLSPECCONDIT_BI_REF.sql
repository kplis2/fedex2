--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLSPECCONDIT_BI_REF FOR EMPLSPECCONDIT                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMPLSPECCONDIT')
      returning_values new.REF;
end^
SET TERM ; ^
