--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TRASY_BI_REF FOR TRASY                          
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('TRASY')
      returning_values new.REF;
end^
SET TERM ; ^
