--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNS_AU_SUMORD FOR FRPSNS                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable minord type of column frpsns.countord;
begin
  if (new.countord <> old.countord) then begin
    if (new.countord > 255) then begin
      exception UNIVERSAL 'Prawdopodobne zapętlenie wierszy sumujących';
    end else if (new.countord < old.countord) then
    -- BS106559 Jesli zmniejszamy kolejnosc obliczania pola, sprawdzamy czy zmiana tej wartosci
    -- umozliwi poprawne skonfigurowanie pozostalych wierszy tj. czy zalezne pola
    -- beda mogly zachowac relatywna kolejnosc wzgledem siebie,
    -- czy nie wymusi to aby countorder osiagnal wartosc ujemna.
    begin
      execute procedure frpsns_count_min_ord(old.ref) returning_values :minord;
      if (new.countord < :minord) then
        exception universal 'Nie można zmniejszyć kolejności sumowania dla danego wiersza.'||
          ' Ze względu na sumowane przez wiersz pozycje, najmniejszą możliwą wartością '||
          'kolejności obliczania jest: '||:minord||'.';
    end
    -- kopniecie aby przeliczyc countord dalej
    update frsumpsns set sumpsn = sumpsn where sumpsn = new.ref and frversion = 0;
  end
end^
SET TERM ; ^
