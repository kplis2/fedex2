--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNS_AI_FRPSNSALG FOR FRPSNS                         
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable cref integer;
  declare variable cnumber integer;
begin
  if(new.kopia <> 1) then
  begin
    -- utworzenie algorytmów dla dodanej pozycji sprawozdania
    for
      select ref, number
        from frcols
        where frhdr = new.frhdr
        into :cref, :cnumber
    do begin
      insert into frpsnsalg (frpsn, frcol, algtype, frversion)
        values (new.ref, :cref, case when :cnumber = 1 then 1 else 2 end, 0);
    end
  end

end^
SET TERM ; ^
