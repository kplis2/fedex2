--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_BU0 FOR NAGZAM                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable kosztdost DECIMAL(14,2);
  declare variable walpln varchar(3);
  declare variable prog numeric(14,2);
  declare variable moddata smallint;
  declare variable lokres varchar(6);
  declare variable dataokres timestamp;
begin
  if(new.kilosc is null) then new.kilosc = 0;
  if(new.kilreal is null) then new.kilreal = 0;
  if(new.kilzreal is null) then new.kilzreal = 0;
  if(new.sprzedawca = 0) then new.sprzedawca = NULL;
  if(old.rejestr <> new.rejestr) then new.datazm = current_date;
  if(new.rabat is null) then new.rabat = 0;
  if(new.prpreprod is null) then new.prpreprod = 0;
  if(new.autoscheduled is null) then new.autoscheduled = 0;
  if(new.anulowano is null) then new.anulowano = 0;
  if(new.datawe<>old.datawe) then begin
    select defrejzam.moddata from defrejzam where defrejzam.symbol=new.rejestr into :moddata;
    if(:moddata=1) then begin
      dataokres=new.datawe;
      lokres = cast( extract( year from :dataokres) as char(4));
      if(extract(month from :dataokres) < 10) then
        lokres = :lokres ||'0' ||cast(extract(month from :dataokres) as char(1));
      else
        lokres = :lokres ||cast(extract(month from :dataokres) as char(2));
      new.okres=:lokres;
    end
  end
--okreslenie magazynu na zleceniu produkcyjnym jesli nie ustawiony
  if((new.magazyn is null or old.prsheet is null or new.prsheet <> old.prsheet) and new.prsheet is not null) then
    select warehouse from prsheets where ref = new.prsheet into new.magazyn;
  if((new.magazyn <> old.magazyn) or (new.magazyn is not null and old.magazyn is null)) then
    select ODDZIAL from DEFMAGAZ where SYMBOL = new.magazyn into new.oddzial;
  if((new.mag2 <> old.mag2) or (new.mag2 is not null and old.mag2 is null)) then
    select oddzial from DEFMAGAZ where SYMBOL = new.mag2 into new.oddzial2;
  if((new.sprzedawca <> old.sprzedawca) or (new.sprzedawca is not null and old.sprzedawca is null)) then
    select ODDZIAL from SPRZEDAWCY where ref = new.sprzedawca into new.oddzialspr;
  if (new.kwersjaref <> old.kwersjaref or (new.kwersjaref is null and old.kwersjaref is not null)) then begin
    new.kktm = null;
    new.kwersja = null;
  end
  if(new.kwersjaref <> old.kwersjaref or (new.kwersjaref is not null and old.kwersjaref is null))then
    select WERSJE.nrwersji, WERSJE.ktm from WERSJE where ref = new.kwersjaref into new.kwersja, new.kktm;
  else if(new.kktm <> old.kktm or (new.kktm is not null and old.kktm is null))then
    select WERSJE.nrwersji, WERSJE.ref from WERSJE where KTM = new.kktm and nrwersji = coalesce(new.kwersja,0) into new.kwersja, new.kwersjaref;

  if ((new.sposdost is not null and old.sposdost is null) or (new.sposdost <> old.sposdost)
    or (new.sposzap is not null and old.sposzap is null) or (new.sposzap <> old.sposzap)
    or (new.sumwartbru<>old.sumwartbru)) then--KK
  begin
    prog = null;
    select max(prog)
      from sdostkoszt
      where platnosc=new.sposzap and sposdost=new.sposdost and prog<=new.sumwartbru
      into :prog;
    if (prog is not null) then
    begin
      select koszt
        from sdostkoszt
        where platnosc=new.sposzap and sposdost=new.sposdost and prog=:prog
        into new.kosztdost;
    end else
    begin
      select prog, koszt from sposdost where ref = new.sposdost
        into :prog, new.kosztdost;
      if (new.sumwartbru>=prog and prog<>0) then
        new.kosztdost = 0;
    end
  end

  if((new.typ <> old.typ)or(old.kosztdost <> new.kosztdost) or(new.rabat <> old.rabat)or (new.sumwartbru <> old.sumwartbru)or (new.sumwartrbru <> old.sumwartrbru)) then begin
    if(new.kosztdost is null) then kosztdost = 0;
    else kosztdost = new.kosztdost;
    if(new.sumwartbru is null) then new.sumwartbru = 0;
    if(new.sumwartrbru is null) then new.sumwartrbru = 0;
    if(new.rabat is not null) then
       kosztdost = kosztdost - cast((:kosztdost * new.rabat/100) as DECIMAL(14,2));
    if(:kosztdost is null) then kosztdost = 0;
    if(new.typ >=2) then
      new.dozaplaty = kosztdost + new.sumwartrbru;
    else
      new.dozaplaty = kosztdost + new.sumwartbru;
  end
  if(new.org_ref = 0 or (new.org_ref is null))then begin
   new.org_id = '';
  end
  if(new.waluta is null or (new.waluta = '')) then begin
    execute procedure GETCONFIG('WALPLN') returning_values new.waluta;
    if(new.waluta is null) then new.waluta = 'PLN';
  end
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln is null or (new.waluta = '')) then walpln = 'PLN';
  if(new.waluta <> :walpln ) then new.walutowe = 1;
  else new.walutowe = 0;
  if((new.org_ref > 0) and ((old.org_ref is null)or(old.org_ref = 0))) then
    /*nastapila realizacja zamowienia*/
    new.datareal = current_date;
  if(new.stan is null or (new.stan = '') or (new.stan = ' ')) then new.stan = 'N';
  if(new.kstan is null or (new.kstan = '') or (new.kstan = ' ')) then new.kstan = 'N';
--  if(new.wysylka is null and new.wysylkadod > 0) then new.wysylkadod = 0;
  if(new.wysylkadod is null) then new.wysylkadod = 0;
  if((new.kktm <> old.kktm ) or (new.kktm is not null and old.kktm is null) or (new.kktm is null and old.kktm is not null)
    or (new.kilosc <> old.kilosc) or (new.kilzreal <> old.kilzreal)) then
    execute procedure plans_knagzam_minus(new.ref);
  if(new.klient > 0 and ((new.klient <> old.klient) or (old.klient is null))) then begin
    select REF from SLODEF where  TYP='KLIENCI' into new.slodef;
    new.slopoz = new.klient;
    if (new.editype is null or new.editype = '') then
    select first 1 k.editype
      from klienciedi k
      join urzsprtyp u on u.symbol=k.editype
      where k.klient = new.klient and new.datawe >= k.fromdate and new.datawe <= k.todate and
           u.expzam=1 into new.editype;
  end else if(new.dostawca > 0 and ((new.dostawa <> old.dostawca) or (old.dostawca is null))) then begin
    select REF from SLODEF where  TYP='DOSTAWCY' into new.slodef;
    new.slopoz = new.dostawca;
  end
  if(new.termdost = '1899-12-30') then new.termdost = null;
end^
SET TERM ; ^
