--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AIU_CHECKLIMIT FOR EABSENCES                      
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
as
  declare variable limit integer;
  declare variable tcol integer;
  declare variable tcol2 integer;
  declare variable wcdays integer;
  declare variable adays integer;
  declare variable aworkdays integer;
  declare variable confecol varchar(50);
  declare variable conflimit varchar(50);
  declare variable i smallint;
  declare variable tmode smallint;
  declare variable vrl integer;
  declare variable mdl integer;
  declare variable mdl1 integer;
  declare variable mdl2 integer;
  declare variable msg varchar(1024);
  declare variable msg1 varchar(1024);
  declare variable msg2 varchar(1024);
begin
--Kontrola przekroczenia limitow dniowych dla wybranych nieobecnosi

  if (inserting or new.fromdate <> old.fromdate or new.todate <> old.todate
       or new.ecolumn <> old.ecolumn or new.employee <> old.employee or new.workdays <> old.workdays
  ) then begin
    i = 1;
    tmode = 0;
    while (i <= 4 and tmode = 0)
    do begin
      if (i = 1) then begin
        confecol = 'VACREQCOLUMN';
        conflimit = 'VACREQLIMIT';
      end else
      if (i = 2) then begin
        confecol = 'VACMDCOLUMN';
        conflimit = 'VACMDLIMIT';
      end else
      if (i = 3) then begin
        confecol = 'CAREADULTCOLUMN';
        conflimit = 'LIMIT_OPIEKA_DOROSLY';
      end else
      if (i = 4) then begin
        confecol = 'CARECHILDCOLUMN';
        conflimit = 'LIMIT_OPIEKA_DZIECKO';  --sprawdzenie musi byc po oopieka_doroslym z uwagi na :tcol2
      end

      execute procedure get_config(:confecol, 2) returning_values :tcol;
      if (i = 3) then tcol2 = tcol;
      if (tcol = new.ecolumn) then tmode = i;
      i = i + 1;
    end

    --testowanie ma sens jezeli dla rejestrowanej nieobecnosci znaleziono ograniczenia
    if (tmode > 0) then
    begin
      --dla opieki dziecko nalezy w limicie uwzglednic wykorzystana opieke nad doroslym
      if (tmode <> 4) then tcol2 = 0;

      select sum(workdays), sum(days) from eabsences
        where ecolumn in (:tcol, :tcol2)
          and employee = new.employee
          and ayear = new.ayear
          and correction in (0,2)
        into :aworkdays, :adays;

      --dla urlopu na zadanie sprawdzamy jeszcze dane ze swiadectwa pracy
      if (tmode = 1) then
      begin
        select sum(ewr.daylimit) from eworkcertifs ew
          join ewrkcrtlimits ewr on (ewr.workcertif = ew.ref)
          where ewr.ecolumn = :tcol
            and ew.employee = new.employee
            and extract(year from ew.todate) = new.ayear
          into :wcdays;
        aworkdays = coalesce(aworkdays,0) + coalesce(wcdays,0);
      end

      --limity bedziemy testowac dla zmiennej ADAYS wiec nadpisanie
      if (tmode in (1,2)) then
        adays = aworkdays;

      --nie ma po co pobierac limitow jezeli nieobecnosc nie byla notowana
      if (adays > 0) then
      begin
        if (tmode in (1,2)) then
        begin

      --Sprawdzamy czy pracownik zlozyl oswiadczenie o MD, a nastepnie czy dziecko nie ukonczylo 14 lat(PR60335)
          execute procedure E_GET_REQMDLIMITS(new.employee, new.fromdate) returning_values :vrl, :mdl1, :msg1;
          execute procedure E_GET_REQMDLIMITS(new.employee, new.todate) returning_values :vrl, :mdl2, :msg2;
          if(mdl1 = 0) then
          begin
            mdl = 0;
            msg = msg1;
          end
          else if(mdl2 = 0) then
          begin
            mdl = 0;
            msg = msg2;
          end
          if (mdl1 < mdl2) then
            mdl = mdl1;
          else
            mdl = mdl2;
          if(tmode = 1) then
            limit = vrl;
          else if(tmode = 2) then
            limit = mdl;
        end
        else
          execute procedure get_pval(new.fromdate, new.company, :conflimit) returning_values :limit;

        if (limit >= 0 and adays > limit) then
        begin
          if (tmode = 1) then exception przekroczony_lim_url_na_zadanie;
          else if (tmode = 2) then
          begin
            if(msg = '') then
              exception przekroczony_lim_url_md;
            else
              exception przekroczony_lim_url_md''||:msg;
          end
          else if (tmode = 3) then exception przekroczony_lim_opieki_dorosly;
          else if (tmode = 4) then exception przekroczony_lim_opieki;
        end
      end
    end
  end
end^
SET TERM ; ^
