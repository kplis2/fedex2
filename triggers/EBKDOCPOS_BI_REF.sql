--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EBKDOCPOS_BI_REF FOR EBKDOCPOS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EBKDOCPOS') returning_values new.ref;
end^
SET TERM ; ^
