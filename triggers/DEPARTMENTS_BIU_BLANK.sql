--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEPARTMENTS_BIU_BLANK FOR DEPARTMENTS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable tmpsymbol varchar(40);
declare variable tmpdparent varchar(40);
declare variable tmpdparentlist varchar(255);
begin
  if (new.name = '') then new.name = null;
  if (new.symbol = '') then new.symbol = null;
  if (new.dparent = '') then new.dparent = null;
  if (new.dparent is not null) then begin
    if(inserting or (updating and coalesce(new.dparent,0)<>coalesce(old.dparent,0))) then begin
      select ref from DEPARTMENTS where symbol=new.dparent into new.parent;
    end
  end else begin
    new.parent = null;
  end
  if (new.isactive is null) then new.isactive = 1;
end^
SET TERM ; ^
