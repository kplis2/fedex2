--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_PRAWA FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(
    (new.prawa <> old.prawa) or (new.prawa is not null and old.prawa is null) or (new.prawa is null and old.prawa is not null) or
    (new.prawagrup <> old.prawagrup) or (new.prawagrup is not null and old.prawagrup is null) or (new.prawagrup is null and old.prawagrup is not null) or
    (new.skrot <> old.skrot) or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
    (new.aktywny <> old.aktywny) or (new.aktywny is not null and old.aktywny is null) or (new.aktywny is null and old.aktywny is not null) or
    (new.slodef <> old.slodef) or (new.slodef is not null and old.slodef is null) or (new.slodef is null and old.slodef is not null) or
    (new.operopiek <> old.operopiek) or (new.operopiek is not null and old.operopiek is null) or (new.operopiek is null and old.operopiek is not null) or
    (new.cstatus <> old.cstatus) or (new.cstatus is not null and old.cstatus is null) or (new.cstatus is null and old.cstatus is not null) or
    (new.typ <> old.typ) or (new.typ is not null and old.typ is null) or (new.typ is null and old.typ is not null)
  ) then
    update PKOSOBY set PRAWA = new.PRAWA, PRAWAGRUP = new.PRAWAGRUP,
                       CPODMSKROT = new.SKROT, CPODMAKTYWNY = new.AKTYWNY,
                       CPODMSLODEF = new.SLODEF, CPODMOPEROPIEK = new.OPEROPIEK, CPODMCSTATUS = new.cstatus, CPODMTYP = new.typ
    where CPODMIOT = new.REF;
  if(
    (new.prawa <> old.prawa) or (new.prawa is not null and old.prawa is null) or (new.prawa is null and old.prawa is not null) or
    (new.prawagrup <> old.prawagrup) or (new.prawagrup is not null and old.prawagrup is null) or (new.prawagrup is null and old.prawagrup is not null) or
    (new.skrot <> old.skrot) or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
    (new.slodef <> old.slodef) or (new.slodef is not null and old.slodef is null) or (new.slodef is null and old.slodef is not null) or
    (new.operopiek <> old.operopiek) or (new.operopiek is not null and old.operopiek is null) or (new.operopiek is null and old.operopiek is not null) or
    (new.telefon <> old.telefon) or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null)
  ) then begin
    update KONTAKTY set CPODMSKROT = new.SKROT,
                        CPODMSLODEF = new.SLODEF, CPODMOPEROPIEK = new.OPEROPIEK, CPODMTELEFON = new.TELEFON
    where CPODMIOT = new.REF;
    update KONTAKTY set PRAWA = new.PRAWA, PRAWAGRUP = new.PRAWAGRUP
    where CPODMIOT = new.REF and PRAWADEF=0;

    update ZADANIA set CPODMSKROT = new.SKROT,
                       CPODMSLODEF = new.SLODEF, CPODMOPEROPIEK = new.OPEROPIEK, CPODMTELEFON = new.TELEFON
    where CPODMIOT = new.REF;
    update ZADANIA set PRAWA = new.PRAWA, PRAWAGRUP = new.PRAWAGRUP
    where CPODMIOT = new.REF and PRAWADEF=0;

    update DOKPLIK set CPODMSKROT = new.SKROT,
                       CPODMOPEROPIEK = new.OPEROPIEK
    where CPODMIOT = new.REF;
    update DOKPLIK set PRAWA = new.PRAWA, PRAWAGRUP = new.PRAWAGRUP
    where CPODMIOT = new.REF and PRAWADEF=0;

    update CKONTRAKTY set CPODMSKROT = new.SKROT,
                          CPODMSLODEF = new.SLODEF, CPODMOPEROPIEK = new.OPEROPIEK
    where CPODMIOT = new.REF;
    update CKONTRAKTY set PRAWA = new.PRAWA, PRAWAGRUP = new.PRAWAGRUP
    where CPODMIOT = new.REF and PRAWADEF=0;

    update CNOTATKI set CPODMSKROT = new.SKROT,
                        CPODMSLODEF = new.SLODEF, CPODMOPEROPIEK = new.OPEROPIEK, CPODMTELEFON = new.TELEFON
    where CPODMIOT = new.REF;
    update CNOTATKI set PRAWA = new.PRAWA, PRAWAGRUP = new.PRAWAGRUP
    where CPODMIOT = new.REF and PRAWADEF=0;
  end
end^
SET TERM ; ^
