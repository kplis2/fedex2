--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCT_BI0 FOR PROMOCT                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.grupatow is null) then new.grupatow = '';
  if(new.ktm is null) then new.ktm = '';
  if(new.ktm='') then begin
    new.wersjaref = null;
    new.wersja = 0;
  end
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji from WERSJE where ref=new.wersjaref into new.wersja;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;

end^
SET TERM ; ^
