--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_APPINI_BU0 FOR S_APPINI                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if((new.section <> old.section)
   or (new.ident <> old.ident)
   or (new.val <> old.val))
  then begin
    new.change = 1;
    new.lastchangedate = current_timestamp(0);
  end
end^
SET TERM ; ^
