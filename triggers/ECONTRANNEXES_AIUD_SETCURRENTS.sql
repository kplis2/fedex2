--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_AIUD_SETCURRENTS FOR ECONTRANNEXES                  
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 2 
as
declare variable emplcontract integer;
begin
/*MWr: Aktualizacja wartosci biezacych na umowie. Trigger zastepuje wczesniej
       istniejace triggery: ECONTRANNEXES_AIU oraz _AD_EMPLCONTRACTS (PR24342)*/

  if (coalesce(old.mflag,0) = 0 and (inserting or deleting
      or new.branch is distinct from old.branch or new.department is distinct from old.department
      or new.workpost is distinct from old.workpost or new.workdim is distinct from old.workdim
      or new.salary is distinct from old.salary or new.caddsalary is distinct from old.caddsalary
      or new.funcsalary is distinct from old.funcsalary or new.emplgroup is distinct from old.emplgroup
      or new.worksystem is distinct from old.worksystem or new.epayrule is distinct from old.epayrule)
  ) then begin
    if (deleting) then emplcontract = old.emplcontract;
    else emplcontract = new.emplcontract;
    execute procedure emplcontracts_currents_update(:emplcontract);
  end
end^
SET TERM ; ^
