--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMCLAIMBILLPOS_AD FOR CRMCLAIMBILLPOS                
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable crmclaim integer;
declare variable amount numeric(14,2);
declare variable suma numeric(14,2);
begin
  crmclaim = 0;
  select crmclaim from crmclaimbills where ref = old.crmclaimbill into :crmclaim;
  if (crmclaim <> 0) then update crmclaimpos set amountbill = 0 where crmclaim = :crmclaim and crmclaimdef = old.crmclaimdef;

-- Zliczanie pozycji rozliczenia
suma = 0;
  for
    select amount from crmclaimbillpos CA where CA.crmclaimbill = old.crmclaimbill
    into :amount
  do begin
    suma = suma + amount;
  end
  update crmclaimbills CS set CS.amount = :suma where CS.ref = old.crmclaimbill;
end^
SET TERM ; ^
