--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTING_BU FOR PRDACCOUNTING                  
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable sumdeductamount numeric(14,2);
declare variable sumfirstdeductamount numeric(14,2);
declare variable prdversion integer;
begin
  if(new.status = 1 and old.status <> 1) then begin
    execute procedure PRDACCOUNTING_CHECK_PRDVERSIONS(new.ref);
  end
end^
SET TERM ; ^
