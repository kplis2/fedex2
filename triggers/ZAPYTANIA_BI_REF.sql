--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZAPYTANIA_BI_REF FOR ZAPYTANIA                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ZAPYTANIA')
      returning_values new.REF;
end^
SET TERM ; ^
