--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_SYNCSTATE_BU0 FOR RP_SYNCSTATE                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.tolocat2 is null) then new.tolocat2 = 0;
  if(new.fromlocat is null) then new.fromlocat = 0;
  if(new.tolocat is null) then new.tolocat = 0;
  if(new.tolocat < -3) then exception RP_SYNCSTATE_TOLERR;
  if(new.fromlocat < -2) then exception RP_SYNCSTATE_FROMERR;

end^
SET TERM ; ^
