--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSSTOCK_BI_KARDEX_CHECK FOR MWSSTOCK                       
  ACTIVE BEFORE INSERT POSITION 5 
as
declare variable status smallint_id;
declare variable msg string255;
begin
  if (exists (select first 1 1 from mwsconstlocs c where c.ref = new.mwsconstloc and c.x_row_type=2)) then
  begin
    select STATUS, MSG
      from X_WMS_CHECK_WEIGHT_4_KARDEX(new.mwsconstloc, new.vers, new.quantity+new.ordered)
      into :STATUS, :MSG;
    if (status=0) then exception universal :msg;
  end
end^
SET TERM ; ^
