--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_DOSTAWCY FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable slodef integer;
declare variable crm smallint;
begin
  if(new.synchro is null and old.synchro is null) then begin
    select ref from SLODEF where TYP = 'DOSTAWCY' into :slodef;
    if(:slodef = new.slodef and
     (new.skrot <> old.skrot or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.ulica <> old.ulica or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
      new.nrdomu <> old.nrdomu or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null) or
      new.nrlokalu <> old.nrlokalu or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null) or
      new.nip <> old.nip or (new.nip is not null and old.nip is null) or (new.nip is null and old.nip is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.kodp <> old.kodp or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
      new.poczta <> old.poczta or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.fax <> old.fax or (new.fax is not null and old.fax is null) or (new.fax is null and old.fax is not null) or
      new.www <> old.www or (new.www is not null and old.www is null) or (new.www is null and old.www is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.regon <> old.regon or (new.regon is not null and old.regon is null) or (new.regon is null and old.regon is not null) or
      new.typ <> old.typ or (new.typ is not null and old.typ is null) or (new.typ is null and old.typ is not null) or
      new.uwagi <> old.uwagi or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null) or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null) or
      new.cpowiaty <> old.cpowiaty or (new.cpowiaty is not null and old.cpowiaty is null) or (new.cpowiaty is null and old.cpowiaty is not null)
     )
     ) then begin
        update DOSTAWCY set ID = new.skrot, NAZWA = new.nazwa,
                      ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu,
                      NIP = new.nip, MIASTO = new.miasto,
                      KODP = new.kodp, POCZTA = new.poczta, UWAGI = new.uwagi,
                      REGON = new.regon, TELEFON = new.telefon, EMAIL = new.email, WWW = new.www, FAX = new.fax,
                      TYP = new.typ, KONTOFK = new.kontofk, cpowiat = new.cpowiaty
          where REF=new.slopoz;
    end
    if (new.slodef=:slodef) then begin
      select CRM from DOSTAWCY where REF=new.slopoz into :crm;
      if(:crm='0') then begin
        update DOSTAWCY set CRM=1
        where REF=new.slopoz;
      end
    end
  end
  if(new.synchro=1) then update cpodmioty set synchro=null where ref=new.ref;
end^
SET TERM ; ^
