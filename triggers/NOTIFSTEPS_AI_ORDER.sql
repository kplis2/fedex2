--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSTEPS_AI_ORDER FOR NOTIFSTEPS                     
  ACTIVE AFTER INSERT POSITION 20 
as
declare variable maxnr integer;
begin
  select max(n.number)
    from notifsteps n
    where n.notification = new.notification
      and n.ref <> new.ref
    into :maxnr;
  if (maxnr is null) then maxnr = 1;

  if (new.number < :maxnr) then
    update notifsteps n set n.number = n.number + 1
      where n.number >= new.number and n.ref <> new.ref and n.notification = new.notification;
end^
SET TERM ; ^
