--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYEES_BIU_BLANK FOR EMPLOYEES                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin

  select fname, sname, symbol, middlename, person from cpersons
    where ref = new.person  and company = new.company
    into new.fname, new.sname, new.symbol, new.middlename, new.personnames;

  if (new.fileno is null) then new.fileno = '';
  if (new.branch = '') then new.branch = null;
  if (new.department = '') then new.department = null;
  if (new.ehrm is null and coalesce(new.person,0) > 0) then
    select ehrm from persons where ref = new.person into new.ehrm;
  if (new.outofwork30 is null) then new.outofwork30 = 0;
  if (new.syears is null) then new.syears = 0;
  if (new.smonths is null) then new.smonths = 0;
  if (new.sdays is null) then new.sdays = 0;
  if (new.pr_id = '') then new.pr_id = null;

  if (new.superiorfromdepart is null) then new.superiorfromdepart = 0;
  else if (new.superiorfromdepart = 1 and (new.superior is not null or new.superiordeputy is not null)) then
  begin
    new.superior = null;
    new.superiordeputy = null;
  end

  if (inserting or (updating and coalesce(new.department,'') <> coalesce(old.department,''))) then begin
    if(new.department is not null) then select departmentlist from departments where symbol=new.department into new.departmentlist;
    else new.departmentlist = '';
  end
end^
SET TERM ; ^
