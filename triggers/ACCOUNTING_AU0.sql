--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCOUNTING_AU0 FOR ACCOUNTING                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.bktype <> old.bktype) then
  begin
    update turnovers set bktype = new.bktype where accounting = new.ref;
    update turnoversd set bktype = new.bktype where accounting = new.ref;
  end
end^
SET TERM ; ^
