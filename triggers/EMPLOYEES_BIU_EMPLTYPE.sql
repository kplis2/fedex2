--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYEES_BIU_EMPLTYPE FOR EMPLOYEES                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.econtrtype is not null) then
    select empltype from econtrtypes
      where contrtype=new.econtrtype
      into new.empltype;
  else new.empltype = 0;
end^
SET TERM ; ^
