--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CSTATUSY_BI_REF FOR CSTATUSY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CSTATUSY')
      returning_values new.REF;
end^
SET TERM ; ^
