--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELPOS_AD FOR EDELPOS                        
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if(exists(select first 1 1 from edelegations where ref = old.delegation)) then begin
    execute procedure EDELPOS_TODAY(old.DELEGATION);
  end
end^
SET TERM ; ^
