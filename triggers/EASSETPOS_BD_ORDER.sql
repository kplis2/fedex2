--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EASSETPOS_BD_ORDER FOR EASSETPOS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update eassetpos set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and eassetofcomp = old.eassetofcomp;
end^
SET TERM ; ^
