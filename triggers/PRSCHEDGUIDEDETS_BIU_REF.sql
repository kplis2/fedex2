--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_BIU_REF FOR PRSCHEDGUIDEDETS               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('PRSCHEDGUIDEDETS') returning_values new.ref;
end^
SET TERM ; ^
