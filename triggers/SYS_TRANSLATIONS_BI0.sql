--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SYS_TRANSLATIONS_BI0 FOR SYS_TRANSLATIONS               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if(new.ref is null or new.ref=0)  then
    execute procedure GEN_REF('SYS_TRANSLATIONS') returning_values new.ref;
end^
SET TERM ; ^
