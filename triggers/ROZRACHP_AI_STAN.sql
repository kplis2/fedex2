--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_AI_STAN FOR ROZRACHP                       
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable kod varchar(100);
declare variable nazwa varchar(255);
declare variable kontofk KONTO_ID;
begin

  execute procedure SLO_DANE(new.slodef, new.slopoz)
  returning_values kod, nazwa, kontofk;

  if(exists(select 1 from rozrachstan
    where slodef = new.slodef and slopoz = new.slopoz and company = new.company
    and kontofk = new.kontofk and waluta = new.waluta)) then
    begin
      update rozrachstan
        set waluta = new.waluta,
        ma = ma + new.ma, winien = winien + new.winien,
        mazl = mazl + new.mazl, winienzl = winienzl + new.winienzl
        where slodef = new.slodef and slopoz = new.slopoz and company = new.company
        and kontofk = new.kontofk and waluta = new.waluta;
    end
  else
    begin
      insert into rozrachstan (slodef, slopoz, company, kontofk, waluta, slonazwa, winien, ma, winienzl, mazl)
        values (new.slodef, new.slopoz, new.company, new.kontofk, new.waluta, :nazwa,
        new.winien, new.ma, new.winienzl, new.mazl);
    end
end^
SET TERM ; ^
