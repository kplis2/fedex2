--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ANALDOST_BI0 FOR ANALDOST                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable skrot shortname_id; --XXX ZG133796 MKD
declare variable okres varchar(6);
begin
  select ID from DOSTAWCY where new.dostawca = DOSTAWCY.ref into :skrot;
  if(:skrot is null or(:skrot = '')) then skrot = 'nieznany';
  if(new.data is null) then new.data = current_date;
  new.symbol = skrot || '/'||cast(new.data as date);
  if(new.wzrost is null ) then new.wzrost = 0;
  if(new.dotyczy is null) then new.dotyczy = 0;
  if(new.sprzedaz is null) then new.sprzedaz = 0;
  if(new.statystyka is null) then new.statystyka = 0;
  if(new.data is null) then new.data = current_date;
  okres = cast( extract( year from new.data) as char(4));
  if(extract(month from new.data) < 10) then
      okres = :okres ||'0' ||cast(extract(month from new.data) as char(1));
  else
      okres = :okres ||cast(extract(month from new.data) as char(2));
  new.okres = :okres;
  new.akt = 1;
end^
SET TERM ; ^
