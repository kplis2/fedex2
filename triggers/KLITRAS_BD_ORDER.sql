--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLITRAS_BD_ORDER FOR KLITRAS                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update klitras set ord = 0, numer = numer - 1
    where (klient <> old.klient or trasa <> old.trasa)  and numer > old.numer and trasa = old.trasa;
end^
SET TERM ; ^
