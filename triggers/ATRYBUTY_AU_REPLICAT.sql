--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_AU_REPLICAT FOR ATRYBUTY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.CECHA <> old.cecha
      or new.wartosc<>old.wartosc or (new.wartosc is null and old.wartosc is not null)
      or (new.wartosc is not null and old.wartosc is null)
      or new.pwartosc <> old.pwartosc or (new.pwartosc is null and old.pwartosc is not null)
      or (new.pwartosc is not null and old.pwartosc is null)) then
    update WERSJE set STATE = -1
      where ktm = old.ktm and nrwersji = old.nrwersji;
end^
SET TERM ; ^
