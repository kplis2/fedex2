--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLATNICY_BU_DOMYSLNY FOR PLATNICY                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if ((new.domyslny <> old.domyslny or (new.domyslny is not null and old.domyslny is null)) and new.domyslny = 1) then
    if (exists(select P.klient from platnicy P where P.klient = new.klient and P.domyslny = 1)) then
      update platnicy P set P.domyslny = 0 where P.klient = new.klient and P.domyslny = 1;
end^
SET TERM ; ^
