--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPSORTPOS_AIU_CHKDOUBLE FOR BHPSORTPOS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
/*MWr: nie mozna pozwolic aby do danego sortu mozna bylo dopisac wiecej niz
      jeden artykul o takim samym numerze ktm i wersji. Generowanie dzialalo by
      wowczas blednie, z uwagi na fakt usuwania artykulow przed generow. planu*/

 if (exists(select first 1 1 from bhpsortpos where bhpsortdef = new.bhpsortdef
              and ktm = new.ktm and versiondefault = new.versiondefault and ref <> new.ref))
 then
   exception bhp_article_double;

end^
SET TERM ; ^
