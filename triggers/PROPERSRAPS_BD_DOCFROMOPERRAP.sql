--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_BD_DOCFROMOPERRAP FOR PROPERSRAPS                    
  ACTIVE BEFORE DELETE POSITION 2 
as
begin
  if (old.prgruparozl is not null) then
  begin
    execute procedure prschedguide_real(old.ref, null, 2, 1);
    execute procedure prschedguide_real(old.ref, null, 2, 0);
  end
end^
SET TERM ; ^
