--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKLIENCI_BU FOR PKLIENCI                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable contactcontrol varchar(100);
begin
      -- sprawdzenie poprawnosci telefonu
  if(new.telefon <> old.telefon) then begin
    execute procedure get_config('CONTACTCONTROL',2) returning_values :contactcontrol;
    if (:contactcontrol is null) then contactcontrol = '0';
    if ((:contactcontrol = '1') and (new.telefon is not null)) then
      execute procedure contact_control('PHONE',new.telefon,0,'PKLIENCI','TELEFON',new.ref,new.skrot);
  end
end^
SET TERM ; ^
