--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_AI_CALCWHAREAS FOR WHSECROWS                      
  ACTIVE AFTER INSERT POSITION 1 
AS
declare variable whsecrowsref integer;
begin
  -- po dostawieniu nowego rzedu trzeba przeliczyc wszystkie obszary w rzedach
  for
    select ref from whsecrows
      where whsec = new.whsec and number >= new.number
      into :whsecrowsref
  do begin
    execute procedure MWS_ROWDIST_CALCULATE(whsecrowsref);
  end
end^
SET TERM ; ^
