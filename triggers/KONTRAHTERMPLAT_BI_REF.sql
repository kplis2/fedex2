--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTRAHTERMPLAT_BI_REF FOR KONTRAHTERMPLAT                
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('KONTRAHTERMPLAT')
      returning_values new.REF;
end^
SET TERM ; ^
