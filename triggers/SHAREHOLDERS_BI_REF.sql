--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SHAREHOLDERS_BI_REF FOR SHAREHOLDERS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SHAREHOLDERS')
      returning_values new.REF;
--  exception test_break new.ref;

end^
SET TERM ; ^
