--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AU0 FOR KLIENCI                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slodef integer;
declare variable operator integer;
declare variable cnt integer;
declare variable prawagrup varchar(80);
declare variable klihasodb varchar(255);
declare variable dataakt timestamp;
declare variable parentpod integer;
declare variable currentyear integer;
declare variable bankaccount integer;
begin
  parentpod = null;
  if(new.crm = 1 and old.crm = 0) then begin
    select ref from SLODEF where TYP='KLIENCI' into :slodef;
    if(:slodef is null) then exception SLODEF_BEZKLIENTOW;
    operator = NULL;
    if(new.sprzedawca is not null) then begin
      select count(*) from OPERATOR where SPRZEDAWCA=new.sprzedawca into :cnt;
      if(:cnt=1) then
        select ref from OPERATOR where SPRZEDAWCA=new.sprzedawca into :operator;
    end
    execute procedure GETCONFIG('DOMPRAWAGRUP') returning_values :prawagrup;
    select count(*) from CPODMIOTY where SLODEF=:slodef and SLOPOZ = new.ref into :cnt;
    if (new.parent is not null and new.parent <> 0) then
    begin
        select ref from cpodmioty where slodef = 1 and slopoz = new.parent into :parentpod;
        if (parentpod is null or parentpod = 0) then
          exception universal 'Błąd synchronizacji. Klient nadrzędny nie ma odpowiednika w kartotece podmiotów.';
    end
    if(:cnt > 0) then
       update CPODMIOTY set AKTYWNY = new.aktywny, SKROT = substring(new.fskrot from 1 for 40), NAZWA = new.nazwa, --XXX ZG133796 MKD
                            TELEFON = new.telefon, FAX = new.fax, KOMORKA = new.komorka,
                            EMAIL = new.email, EMAILWINDYK = new.emailwindyk, WWW = new.www,
                            NIP = new.nip, REGON = new.regon,
                            MIASTO = new.miasto, ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu,
                            KODP = new.kodp, POCZTA = new.poczta, KRAJ = new.kraj,
                            KMIASTO = new.kmiasto, KULICA = new.kulica, KNRDOMU = new.knrdomu, KNRLOKALU = new.knrlokalu,
                            KKODP = new.kkodp, KPOCZTA = new.kpoczta, KKRAJ = new.kkraj,
                            KORESPOND = new.korespond, REGION = new.region,
                            FIRMA = new.firma, nazwisko = new.nazwisko, imie = new.imie,
                            UWAGI = new.uwagi, NIEWYPLAC = new.niewyplac, TYP = new.typ, KODZEWN = new.kodzewn,
                            OPEROPIEK = :operator, PRAWAGRUP = :prawagrup, KONTOFK = new.kontofk,
                            DATAZAL = new.datazal, CPWOJ16M = new.cpwoj16m, COMPANY = new.company,
                            cpowiaty = new.cpowiat, SYNCHRO=1, parent = :parentpod
       where SLODEF=:slodef and SLOPOZ = new.ref ;
    else
      insert into CPODMIOTY(AKTYWNY, FIRMA, SKROT, NAZWA, TELEFON, FAX, KOMORKA,
                            EMAIL, EMAILWINDYK, WWW, NIP, REGON, SLODEF, SLOPOZ,
                            ULICA, NRDOMU, NRLOKALU, MIASTO, KODP, POCZTA, KRAJ, IMIE, NAZWISKO,
                            UWAGI, NIEWYPLAC, TYP, KODZEWN, OPEROPIEK, PRAWAGRUP, KONTOFK,
                            DATAZAL,CPWOJ16M, COMPANY, REGION, PARENT)
         values(new.aktywny, new.firma,substring(new.fskrot from 1 for 40), new.nazwa, new. telefon, new.fax, new.komorka,  --XXX ZG133796 MKD
               new.email, new.emailwindyk, new.www, new.nip, new.regon, :slodef, new.ref,
               new.ulica, new.nrdomu, new.nrlokalu, new.miasto, new.kodp, new.poczta, new.kraj, new.imie, new.nazwisko,
               new.uwagi, new.niewyplac, new.typ, new.kodzewn, :operator, :prawagrup, new.kontofk,
               new.datazal, new.cpwoj16m, new.company, new.region, :parentpod);
  end
  else if(new.crm = 0 and old.crm = 1) then begin
    select ref from SLODEF where TYP='KLIENCI' into :slodef;
    if(:slodef is null) then exception SLODEF_BEZKLIENTOW;
    delete from CPODMIOTY where SLODEF = :slodef and SLOPOZ = old.ref;
  end
  if(new.crm = 1 and old.crm = 1 and new.token <> 7 and
    (
      new.aktywny <> old.aktywny or (new.aktywny is not null and old.aktywny is null) or (new.aktywny is null and old.aktywny is not null) or
      new.fskrot <> old.fskrot or (new.fskrot is not null and old.fskrot is null) or (new.fskrot is null and old.fskrot is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.ulica <> old.ulica or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
      new.nrdomu <> old.nrdomu or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null) or
      new.nrlokalu <> old.nrlokalu or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null) or
      new.nip <> old.nip or (new.nip is not null and old.nip is null) or (new.nip is null and old.nip is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.kodp <> old.kodp or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
      new.poczta <> old.poczta or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
      new.kraj <> old.kraj or (new.kraj is not null and old.kraj is null) or (new.kraj is null and old.kraj is not null) or
      new.kmiasto <> old.kmiasto or (new.kmiasto is not null and old.kmiasto is null) or (new.kmiasto is null and old.kmiasto is not null) or
      new.kkodp <> old.kkodp or (new.kkodp is not null and old.kkodp is null) or (new.kkodp is null and old.kkodp is not null) or
      new.kpoczta <> old.kpoczta or (new.kpoczta is not null and old.kpoczta is null) or (new.kpoczta is null and old.kpoczta is not null) or
      new.kkraj <> old.kkraj or (new.kkraj is not null and old.kkraj is null) or (new.kkraj is null and old.kkraj is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.fax <> old.fax or (new.fax is not null and old.fax is null) or (new.fax is null and old.fax is not null) or
      new.komorka <> old.komorka or (new.komorka is not null and old.komorka is null) or (new.komorka is null and old.komorka is not null) or
      new.www <> old.www or (new.www is not null and old.www is null) or (new.www is null and old.www is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.emailwindyk <> old.emailwindyk or (new.emailwindyk is not null and old.emailwindyk is null) or (new.emailwindyk is null and old.emailwindyk is not null) or
      new.regon <> old.regon or (new.regon is not null and old.regon is null) or (new.regon is null and old.regon is not null) or
      new.firma <> old.firma or (new.firma is not null and old.firma is null) or (new.firma is null and old.firma is not null) or
      new.nazwisko <> old.nazwisko or (new.nazwisko is not null and old.nazwisko is null) or (new.nazwisko is null and old.nazwisko is not null) or
      new.imie <> old.imie or (new.imie is not null and old.imie is null) or (new.imie is null and old.imie is not null) or
      new.uwagi <> old.uwagi or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null) or
      new.niewyplac <> old.niewyplac or (new.niewyplac is not null and old.niewyplac is null) or (new.niewyplac is null and old.niewyplac is not null) or
      new.kodzewn <> old.kodzewn or (new.kodzewn is not null and old.kodzewn is null) or (new.kodzewn is null and old.kodzewn is not null) or
      new.typ <> old.typ or (new.typ is not null and old.typ is null) or (new.typ is null and old.typ is not null) or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null) or
      new.datazal <> old.datazal or (new.datazal is not null and old.datazal is null) or (new.datazal is null and old.datazal is not null) or
      new.cpwoj16m <> old.cpwoj16m or (new.cpwoj16m is not null and old.cpwoj16m is null) or (new.cpwoj16m is null and old.cpwoj16m is not null) or
      new.region <> old.region or (new.region is not null and old.region is null) or (new.region is null and old.region is not null) or
      new.parent <> old.parent or (new.parent is not null and old.parent is null) or (new.parent is null and old.parent is not null)
    )
  ) then begin
    select ref from SLODEF where TYP='KLIENCI' into :slodef;
    if(:slodef is null) then exception SLODEF_BEZKLIENTOW;
    if (new.parent is not null and new.parent <> 0) then
    begin
        select ref from cpodmioty where slodef = 1 and slopoz = new.parent into :parentpod;
        if (parentpod is null or parentpod = 0) then
          exception universal 'Błąd synchronizacji. Klient nadrzędny nie ma odpowiednika w kartotece podmiotów.';
    end
    update CPODMIOTY set AKTYWNY = new.aktywny, SKROT = substring(new.fskrot from 1 for 40), NAZWA = new.nazwa, --XXX ZG133796 MKD
                      ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu, NIP = new.nip, MIASTO = new.miasto,
                      KODP = new.kodp, POCZTA = new.poczta, KRAJ = new.kraj,
                      KKODP = new.kkodp, KPOCZTA = new.kpoczta, KKRAJ = new.kkraj,
                      KULICA = new.kulica, KNRDOMU = new.knrdomu, KNRLOKALU = new.knrlokalu, KMIASTO = new.kmiasto, KORESPOND = new.korespond,
                      REGON = new.regon, TELEFON = new.telefon, EMAIL = new.email, EMAILWINDYK = new.emailwindyk, KOMORKA = new.komorka,
                      WWW = new.www, FAX = new.fax, FIRMA = new.firma, NAZWISKO = new.nazwisko, IMIE = new.imie,
                      UWAGI = new.uwagi, NIEWYPLAC = new.niewyplac, TYP = new.typ, KODZEWN = new.kodzewn, KONTOFK = new.kontofk,
                      DATAZAL = new.datazal, CPWOJ16M = new.cpwoj16m, SYNCHRO = 1, REGION = new.region, parent = :parentpod
          where SLODEF=:slodef and SLOPOZ = new.ref ;
  end

  execute procedure GETCONFIG('KLIHASODB') returning_values :klihasodb;
  if(:klihasodb = '1') then begin
    if((new.fskrot <> old.fskrot) or
      (new.dnazwa <> old.dnazwa) or
      (new.dulica <> old.dulica) or
      (new.dmiasto <> old.dmiasto) or
      (new.dkodp <> old.dkodp) or
      (new.dpoczta <> old.dpoczta) or
      (coalesce(new.grupakup,0)<>coalesce(old.grupakup,0))
    ) then begin

      update ODBIORCY set NAZWA = new.fskrot, NAZWAFIRMY = new.dnazwa, DMIASTO = new.dmiasto,
          DULICA = new.dulica, DNRDOMU = new.nrdomu, DNRLOKALU = new.nrlokalu,
          DPOCZTA = new.dpoczta, DKODP = new.dkodp, GRUPAKUP = new.grupakup
        where KLIENT = new.ref and GL=1;
    end
  end
    /*tutaj mozna nie odejmowac jedynki i dac pozniej >=*/
  if(new.nip<>old.nip) then begin
    update dokumnag set nip=new.nip where klient=new.ref and (nip<>new.nip or nip is null);
  end
  if(new.fskrot<>old.fskrot) then begin
    update platnicy set fskrot=new.fskrot where platnik=new.ref;
  end
  if (new.nazwa <> old.nazwa) then
  begin
    update rozrach set slonazwa = new.nazwa
      where slodef in (select ref from slodef where typ = 'KLIENCI')
        and slopoz = new.ref;
  end

  -- aktulizacja opisu kont ksiegowych dla bieżącego roku
  if(new.nazwa<>old.nazwa) then begin
    currentyear = extract(year from current_timestamp(0));
    for select ref from slodef where typ = 'KLIENCI'
      into :slodef
    do begin
      execute procedure accounting_recalc_descript(:slodef, :currentyear, new.company, new.kontofk);
    end
  end
end^
SET TERM ; ^
