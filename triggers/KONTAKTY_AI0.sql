--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_AI0 FOR KONTAKTY                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable datakon timestamp;
begin
  /* wypenij date rozpoczecia wspolpracy z podmiotem */
  select DATAROZP from CPODMIOTY where REF = new.cpodmiot into :datakon;
  if (datakon is null) then update CPODMIOTY set DATAROZP=new.data where REF = new.cpodmiot;
  if(new.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYKON(new.ckontrakt);
end^
SET TERM ; ^
