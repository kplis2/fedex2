--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLARATIONS_BU_UPDATE_FIELDS FOR EDECLARATIONS                  
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  --gdy wracamy do redakcji wyczysc pola odnosnie wyslania i UPO
  if (new.state = 0) then
  begin
    new.refid = null;
    new.status = null;
    new.statusdict = null;
  end

  if (new.state = 4 and new.state > old.state) then
    new.sendtimestamp = current_timestamp(0);

  if (new.status <> coalesce(old.state,0)) then
    new.responsetimestamp = current_timestamp(0);
end^
SET TERM ; ^
