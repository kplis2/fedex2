--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREEMATCHINGGROUPS_BI_REF FOR DECREEMATCHINGGROUPS           
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('DECREEMATCHINGGROUPS') returning_values new.ref;
end^
SET TERM ; ^
