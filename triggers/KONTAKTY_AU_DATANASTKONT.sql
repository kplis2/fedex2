--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_AU_DATANASTKONT FOR KONTAKTY                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
  declare variable zadaniaref integer;
  declare variable defzadanieref integer;
  declare variable eol varchar(10);
  declare variable stanzadania integer;
  declare variable ilosckontaktow integer;
  declare variable iloscnotatek integer;
  declare variable iloscdokumentow integer;
begin
  eol='
';
  ilosckontaktow = 0;
  iloscnotatek = 0;
  iloscdokumentow = 0;
  if(new.datanastkont is not null and old.datanastkont is null) then begin
    select ref from defzadan where defzadan.domyslne = 1 into :defzadanieref;
    if (defzadanieref is null) then
      exception ZADANIE_DOMYSLNE_BRAK;
    else begin
      execute procedure GEN_REF('ZADANIA') returning_values :zadaniaref;
      insert into zadania  (ref, kontakt, zadanie, operzal, operwyk,
                            data, datado, stan, cpodmiot, osoba,
                            datazal, opis, prawa, prawagrup)
      values (:zadaniaref, new.ref, :defzadanieref, new.operator, new.operator,
              new.datanastkont, new.datanastkont, 0, new.cpodmiot, new.pkosoba,
              current_date, 'Zadanie założone automatycznie dla kontaktu z dnia '||new.nazwa ||:eol||'Treść kontaktu: '||new.opis,
              new.prawa, new.prawagrup);
      update kontakty set refwygzad = :zadaniaref where ref = new.ref;
    end
  end
  if(new.datanastkont is not null and old.datanastkont is not null and new.datanastkont <> old.datanastkont) then begin
    select zadania.stan from zadania where ref = new.refwygzad into :stanzadania;
    if(stanzadania <> 0) then exception ZADANIE_ZMIANADATY;
    else update zadania
         set zadania.data = new.datanastkont, zadania.datado = new.datanastkont
         where zadania.ref = new.refwygzad;
  end
  if(new.datanastkont is null and old.datanastkont is not null) then begin
    select zadania.stan from zadania where ref = new.refwygzad into :stanzadania;
    if(stanzadania <> 0) then exception ZADANIE_WYKONANE;
    select count(*) from kontakty where kontakty.zadanie = new.refwygzad into :ilosckontaktow;
    select count(*) from cnotatki where cnotatki.zadanie = new.refwygzad into :iloscnotatek;
    select count(*) from dokplik where dokplik.zadanie = new.refwygzad into :iloscdokumentow;
    if (ilosckontaktow=0 and iloscnotatek=0 and iloscdokumentow=0) then
      delete from zadania where zadania.ref = new.refwygzad;
    else
      exception ZADANIE_USUNIECIE;
 end
end^
SET TERM ; ^
