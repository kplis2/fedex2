--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BI_REPLICAT FOR KLIENCI                        
  ACTIVE BEFORE INSERT POSITION 4 
AS
begin
 execute procedure rp_trigger_bi('KLIENCI',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
