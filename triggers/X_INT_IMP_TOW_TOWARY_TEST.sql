--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_INT_IMP_TOW_TOWARY_TEST FOR INT_IMP_TOW_TOWARY             
  INACTIVE BEFORE INSERT POSITION 99 
AS
begin /*$$IBE$$ 
  if (new.symbol = 'TEST_SENTE') then
    exception universal 'Testowy exception Sente - test obsługi błędów';
 $$IBE$$*/ POST_EVENT '$$IBE$$';
end^
SET TERM ; ^
