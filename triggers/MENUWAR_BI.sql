--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MENUWAR_BI FOR MENUWAR                        
  ACTIVE BEFORE INSERT POSITION 1 
AS
BEGIN
  execute procedure REPLICAT_STATE('MENUWAR',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
END^
SET TERM ; ^
