--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMDYSP_BI FOR POZZAMDYSP                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable przelicz numeric(14,4);
BEGIN
  if(new.jedn is null or (new.jedn = 0))then
    select jedn from POZZAM where ref=new.pozzam into new.jedn;
  if(new.jedno is null or (new.jedno = 0)) then
    select jedno from POZZAM where ref=new.pozzam into new.jedno;
  if(new.ilosc is null or (new.ilosc < 0)) then
    new.ilosc = 0;
  if(new.jedno = new.jedn) then
    new.ilosco = new.ilosc;
  if(new.ilosc > 0) then begin
    if(new.iloscm is null or (new.iloscm <=0)) then begin
      select przelicz from TOWJEDN where ref=new.jedn into :przelicz;
      if(:przelicz is null) then przelicz = 1;
      new.iloscm = new.ilosc * :przelicz;
    end
    if(new.ilosco is null or (new.ilosc is null)) then begin
      select przelicz from TOWJEDN where ref=new.jedno into :przelicz;
      if(:przelicz is null) then przelicz = 1;
      new.ilosco = new.iloscm / :przelicz;
    end
  end else begin
    new.ilosco = 0;
    new.iloscm = 0;
  end
  /*okrelenie magazynu lub dostawcy, jeli to puste*/
  if(new.typ <= 3 and (new.magazyn is null or (new.magazyn = ''))) then begin
    if(new.typ = 1) then
      select NAGZAM.autodysppmag from NAGZAM join POZZAM on (POZZAM.zamowienie = nagzam.ref) where POZZAM.REF = new.pozzam into new.magazyn;
    else if(new.typ = 2) then
      select nagzam.autodyspbmag from NAGZAM join POZZAM on (POZZAM.zamowienie = nagzam.ref) where POZZAM.REF = new.pozzam into new.magazyn;
    else
      select nagzam.autodyspkmag from NAGZAM join POZZAM on (POZZAM.zamowienie = nagzam.ref) where POZZAM.REF = new.pozzam into new.magazyn;
    -- jesli nie zdefiniowano magazynu na operacji to biezemy z pozycji zamowienia
    if(coalesce(new.magazyn,'')='') then select MAGAZYN from POZZAM where ref=new.pozzam into new.magazyn;
  end else if(new.typ = 4 and new.dostawca is null) then begin
    select DOSTCEN.dostawca
      from DOSTCEN
      join POZZAM on (dostcen.wersjaref = pozzam.wersjaref and dostcen.gl = 1)
      where pozzam.ref = new.pozzam
      into new.dostawca;
    if(new.dostawca is null) then
      select DOSTCEN.dostawca
        from DOSTCEN
        join POZZAM on (dostcen.ktm = pozzam.ktm and dostcen.wersja=0 and dostcen.gl = 1)
        where pozzam.ref = new.pozzam
        into new.dostawca;
  end
END^
SET TERM ; ^
