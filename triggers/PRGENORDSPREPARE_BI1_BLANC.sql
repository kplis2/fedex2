--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRGENORDSPREPARE_BI1_BLANC FOR PRGENORDSPREPARE               
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  if(new.autosched is null) then new.autosched = 0;
  new.amountgen = 0;
  new.status = 0;
end^
SET TERM ; ^
