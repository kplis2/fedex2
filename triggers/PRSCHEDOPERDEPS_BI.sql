--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERDEPS_BI FOR PRSCHEDOPERDEPS                
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  select OPER, SHOPER from prschedopers
    where ref=new.depfrom
    into new.depformoper, new.depfromshoper;
  new.typ = 0;
  if (new.kamount is null or new.kamount = 0) then
    new.kamount = 1;
end^
SET TERM ; ^
