--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVGOODDEF_AU_REPLICAT FOR SRVGOODDEF                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.number <> old.number ) or (new.number is not null and old.number is null) or (new.number is null and old.number is not null)
  or (new.contrtype <> old.contrtype ) or (new.contrtype is not null and old.contrtype is null) or (new.contrtype is null and old.contrtype is not null)
  or (new.descript <> old.descript ) or (new.descript is not null and old.descript is null) or (new.descript is null and old.descript is not null)
  or (new.symbol <> old.symbol ) or (new.symbol is not null and old.symbol is null) or (new.symbol is null and old.symbol is not null)
  or (new.fieldtype <> old.fieldtype ) or (new.fieldtype is not null and old.fieldtype is null) or (new.fieldtype is null and old.fieldtype is not null)
  or (new.dictbase <> old.dictbase ) or (new.dictbase is not null and old.dictbase is null) or (new.dictbase is null and old.dictbase is not null)
  or (new.dicttable <> old.dicttable ) or (new.dicttable is not null and old.dicttable is null) or (new.dicttable is null and old.dicttable is not null)
  or (new.dictfield <> old.dictfield ) or (new.dictfield is not null and old.dictfield is null) or (new.dictfield is null and old.dictfield is not null)
  or (new.dictindex <> old.dictindex ) or (new.dictindex is not null and old.dictindex is null) or (new.dictindex is null and old.dictindex is not null)
  or (new.dictprefix <> old.dictprefix ) or (new.dictprefix is not null and old.dictprefix is null) or (new.dictprefix is null and old.dictprefix is not null)
  or (new.dictfilter <> old.dictfilter ) or (new.dictfilter is not null and old.dictfilter is null) or (new.dictfilter is null and old.dictfilter is not null)
  or (new.ord <> old.ord ) or (new.ord is not null and old.ord is null) or (new.ord is null and old.ord is not null)
   ) then
     update CONTRTYPES set STATE = -1 where ref=new.contrtype;
end^
SET TERM ; ^
