--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AI_STAN FOR POZZAM                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable termdost date;
declare variable ilosc numeric(14,4);
declare variable status integer;
declare variable org_ref integer;
declare variable reff integer;
declare variable typ integer;
declare variable termstart date;
declare variable wydania integer;
begin
     select wydania, TYP, cast(TERMDOST as date), cast(TERMstart as date), REF, ORG_REF
       from NAGZAM join typzam on (typzam.symbol = nagzam.typzam)
       where nagzam.ref=new.zamowienie
       into :wydania, :typ, :termdost, :termstart, :reff, :org_ref;
     if(:termdost is null) then termdost = current_date;
     if(:termstart is null) then termstart = :termdost;
     if(:wydania = 3 and new.stan <> 'D')then-- pozycja nie-przychodowa, wiec czas rezerwacji to czas rozpoczecia zamowienia
       termdost = :termstart;
     if(:typ >=2) then ilosc = new.ilrealm;
     else begin
       ilosc = new.iloscm - new.ilonklonm;
     end
     if(new.blokadarez = 1) then
       ilosc = 0;
     if((new.STAN ='B') or (new.stan = 'R') or (new.stan = 'D')) then begin
           execute procedure REZ_SET_KTM(new.zamowienie, new.ref, new.ktm, new.wersja, :ilosc, new.cenamag, new.dostawamag,new.stan,:termdost) returning_values :status;
       if(:status <> 1) then exception REZ_WRONG;
     end
     execute procedure NAGZAM_CHECK_ZREAL(new.zamowienie);
end^
SET TERM ; ^
