--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCES_AI FOR TOWAKCES                       
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable awersjaref integer;
begin
  -- zaozenie powiazania odwrotnego dla wersji podstawowej
  if (new.doubleakc = 1 and new.token <> 7) then begin
    if ( not exists(select ref from towakces where towakces.aktm = new.ktm
        and towakces.ktm = new.aktm)
       ) then begin
      select wersje.ref from wersje where wersje.ktm = new.ktm and wersje.nrwersji = 0
        into :awersjaref;
      insert into towakces (KTM, AKTM, AWERSJA, AWERSJAREF, TYP, OPIS, REQUIRED,
                            AKCESDEGREE, DOUBLEAKC, TOWAKCESGRP)
                     values(new.aktm, new.ktm, 0, :awersjaref, new.typ, new.opis, new.required,
                            new.akcesdegree, new.doubleakc, new.towakcesgrp);
    end
  end
end^
SET TERM ; ^
