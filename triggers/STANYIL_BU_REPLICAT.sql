--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_BU_REPLICAT FOR STANYIL                        
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
   if((new.magazyn <> old.magazyn)
    or (new.wersjaref <> old.wersjaref)
    or (new.wersja <> old.wersja)
    or (new.cena <> old.cena) or (new.cena is not null and old.cena is null) or (new.cena is null and old.cena is not null)
    or (new.wartosc <> old.wartosc ) or (new.wartosc is not null and old.wartosc is null) or (new.wartosc is null and old.wartosc is not null)
    or (new.zamowiono <> old.zamowiono ) or (new.zamowiono is not null and old.zamowiono is null) or (new.zamowiono is null and old.zamowiono is not null)
    or (new.zarezerw <> old.zarezerw ) or (new.zarezerw is not null and old.zarezerw is null) or (new.zarezerw is null and old.zarezerw is not null)
    or (new.oddzial <> old.oddzial ) or (new.oddzial is not null and old.oddzial is null) or (new.oddzial is null and old.oddzial is not null)
    or (new.glowny <> old.glowny ) or (new.glowny is not null and old.glowny is null) or (new.glowny is null and old.glowny is not null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('STANYIL',old.magazyn, old.wersjaref, null, null, null, old.token, old.state,
        new.magazyn, new.wersjaref, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
