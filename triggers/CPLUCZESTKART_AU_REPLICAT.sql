--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZESTKART_AU_REPLICAT FOR CPLUCZESTKART                  
  ACTIVE AFTER UPDATE POSITION 2 
as
begin
  if((new.cpluczestid <> old.cpluczestid)
   or (new.symbol <> old.symbol) or (new.symbol is not null and old.symbol is null) or (new.symbol is null and old.symbol is not null)
   or (new.data <> old.data ) or (new.data is not null and old.data is null) or (new.data is null and old.data is not null)
   or (new.datawaz <> old.datawaz ) or (new.datawaz is not null and old.datawaz is null) or (new.datawaz is null and old.datawaz is not null)
   or (new.zablokowana <> old.zablokowana ) or (new.zablokowana is not null and old.zablokowana is null) or (new.zablokowana is null and old.zablokowana is not null)
   or (new.zablokowanaopis <> old.zablokowanaopis ) or (new.zablokowanaopis is not null and old.zablokowanaopis is null) or (new.zablokowanaopis is null and old.zablokowanaopis is not null)
   or (new.uwagi <> old.uwagi ) or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null)
  ) then begin
    update CPLUCZEST set STATE = -1 where REF=new.cpluczestid;
    if(new.cpluczestid <> old.cpluczestid) then
      update CPLUCZEST set STATE = -1 where REF=old.cpluczestid;
  end

end^
SET TERM ; ^
