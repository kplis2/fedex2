--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_UNIQUE FOR EMPLTRAININGS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (new.TRAINING is null) then
    exception TRAINING;

  if (exists(select ref from EMPLTRAININGS where EMPLOYEE = new.EMPLOYEE AND REF <> new.REF AND TRAINING = new.TRAINING)) then
    exception TRAINING_JUZ_JEST;
end^
SET TERM ; ^
