--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYNAG_BU0 FOR NOTYNAG                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (old.status > 0 and new.status > 0
    and (old.debit <> new.debit or old.credit <> new.credit or old.bdebit <> new.bdebit
    or old.bcredit <> new.bcredit or old.interests <> new.interests)
    ) then exception NOTYNAG_ACCEPTED;
  if(new.bcredit is null) then new.bcredit = 0;
  if(new.bdebit is null) then new.bdebit = 0;
  if(new.credit is null) then new.credit = 0;
  if(new.debit is null) then new.debit = 0;
  if(new.interests is null) then new.interests = 0;
  if(new.interestszl is null) then new.interestszl = 0;
  if(new.symbol is null) then new.symbol = '';
  if (new.anulowanie = 0 and old.anulowanie is not null and old.anulowanie > 0) then
    if (exists (select * from bkdocs where otable = 'NOTYNAG_A' and oref = new.ref)) then
      new.anulowanie = 1;
  if (old.status is not null and old.status > 1 and new.status < 2 and new.anulowanie = 1) then
    exception BKDOC_INTEREST_NOTE;
end^
SET TERM ; ^
