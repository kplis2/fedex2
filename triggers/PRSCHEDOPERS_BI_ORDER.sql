--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_BI_ORDER FOR PRSCHEDOPERS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  if(new.ord = 1) then exit;
  new.ord = 1;
  select max(number) from prschedopers where coalesce(guide, null, 0) = coalesce(new.guide, null, 0)
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  begin
    update prschedopers set ord = 0, number = number + 1
      where number >= new.number and coalesce(guide, null, 0) = coalesce(new.guide, null, 0);
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
