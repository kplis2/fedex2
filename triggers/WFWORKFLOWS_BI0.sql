--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFWORKFLOWS_BI0 FOR WFWORKFLOWS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  new.startdate = current_timestamp(0);
  new.status = 0;
  if(replace(coalesce(new.symbol,''), ' ', '') = '') then exception universal 'Wprowadź symbol obiegu dokumentów';
  if(new.contrtype is not null) then begin
     select phase from contrtypes where ref = new.contrtype into new.faza;
     if(new.faza is null) then exception universal 'Brak wskazania fazy domyślnej na typie obiegu dokumentów';
  end
end^
SET TERM ; ^
