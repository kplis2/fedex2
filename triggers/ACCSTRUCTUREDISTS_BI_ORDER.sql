--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_BI_ORDER FOR ACCSTRUCTUREDISTS              
  ACTIVE BEFORE INSERT POSITION 2 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 1 and new.number is not null) then begin
    new.ord = 0;
    exit;
  end
  select max(number) from accstructuredists
  where
    otable = new.otable
    and oref = new.oref
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  begin
    update accstructuredists set ord = 1, number = number + 1, internalupdate = 1
      where number >= new.number
      and otable = new.otable
      and oref = new.oref;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
