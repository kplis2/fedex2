--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLS_AU0 FOR PRTOOLS                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.amount = 0 and not exists (select first 1 1 from prtoolshistory p
    where p.prtools = new.symbol and p.tooltype = new.tooltype  and coalesce(p.price,0) = coalesce(new.price,0)
      and coalesce(p.invno,'') = coalesce(new.invno,'') and p.prdsdef = new.prdsdef)
  ) then
    delete from prtools p where p.symbol = new.symbol and p.tooltype = new.tooltype
      and coalesce(p.price,0) = coalesce(new.price,0) and coalesce(p.invno,'') = coalesce(new.invno,'')
      and p.prdsdef = new.prdsdef;
end^
SET TERM ; ^
