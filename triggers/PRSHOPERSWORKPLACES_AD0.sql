--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERSWORKPLACES_AD0 FOR PRSHOPERSWORKPLACES            
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable prshopersworkplace integer;
begin
  if(old.main = 1) then begin
    select min(p.ref) from prshopersworkplaces p where p.prshoper = old.prshoper into :prshopersworkplace;
    if(coalesce(prshopersworkplace,0)>0) then
      update prshopersworkplaces p set p.main = 1 where p.ref = :prshopersworkplace;
    else
      update prshopers p set p.workplace = null, p.eopertime = null where p.ref = old.prshoper;
  end
end^
SET TERM ; ^
