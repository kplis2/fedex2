--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTAP_BU0 FOR INWENTAP                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable zpartiami integer;
declare variable zbiorczy integer;
declare variable zamk integer;
declare variable strategia varchar(100);
declare variable datapom timestamp;
declare variable cenapom numeric(14,4);
declare variable mag varchar(3);
declare variable typ_inwenta smallint;
declare variable sumilinw numeric(14,4);
declare variable mws smallint;
begin
  if(new.blokadanalicz is null) then new.blokadanalicz = 0;
  if(new.ilkorekta is null) then new.ilkorekta = 0;
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.wersjaref is null) then
    exception INWENTAP_BRAKTOWARU;
  if(new.ilstan is null) then new.ilstan = 0;
  if(new.ilinw is null) then new.ilinw = 0;

  select i.zpartiami, i.zbiorczy, i.zamk, i.magazyn, d.mws
    from INWENTA i
      left join defmagaz d on (d.symbol = i.magazyn)
    where i.ref=new.inwenta
  into :zpartiami, :zbiorczy, :zamk, :mag, :mws;
  if(:zpartiami is null) then zpartiami = 0;
  if(:zbiorczy is null) then zbiorczy = 0;
  if (new.mwsilroz is null) then new.mwsilroz = 0;
  if (mws = 1 and zamk = 0 and (new.ilstan <> old.ilstan or new.mwsilroz <> old.mwsilroz)) then
    new.ilinw = new.ilstan + new.mwsilroz;
  /*Obliczenie sumy pozycji dla inwentury partiami i wersjami*/
  if(new.status = -1) then begin
    new.status = old.status;
  end else if(new.ilstan<>old.ilstan or new.ilinw<>old.ilinw) then begin
    if(new.ilstan <> old.ilstan and old.blokadanalicz = new.blokadanalicz) then
      new.status = 1;
    else if(old.blokadanalicz = new.blokadanalicz) then begin
      select status
        from inwentap_set_status(new.ref, new.wersjaref, new.inwenta, new.dostawa, new.ilstan, new.ilinw)
        into new.status;
    end
  end

  if(new.ilkorekta <> 0 and :zbiorczy = 0 and :zamk = 0) then
    new.ilkorekta = 0;
  if(new.wersjaref <> old.wersjaref) then
    select NAZWA from TOWARY where ktm = new.ktm into new.nazwat;
  if((new.cenaplus is null or new.cenaplus = 0) and new.blokadanalicz = 0 and :zamk=0) then
  begin
    execute procedure INWENTAP_ILPLUS_WYCENA(new.ref,new.wersjaref,:mag,new.dostawa) returning_values new.cenaplus;
  end
  if((new.cena is null or new.cena=0) and new.blokadanalicz = 0 and :zamk=0) then
    select cena_zakn from wersje where ref=new.wersjaref into new.cena;
  new.ilroznica = new.ilinw-new.ilstan;
  new.wartosc = new.ilroznica * new.cenaplus;
  if(new.ilinw < 0) then exception INWENTAP_EXPT;
  if((new.ilinw<>old.ilinw) or (new.wersjaref<>old.wersjaref) or (new.cena<>old.cena) or (new.cenaplus<>old.cenaplus) ) then begin
  if(:zamk>0 and new.blokadanalicz=0)then
      exception INWENTA_ZAAKCEPTOWANE 'Nie można zmieniać danych na zamkniętej inwentaryzacji!';
  end
end^
SET TERM ; ^
