--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_AD_AUTONR FOR CKONTRAKTY                     
  ACTIVE AFTER DELETE POSITION 1 
AS
  declare variable nazwa string40;
  declare variable contrtype string40;
  declare variable blockref integer;
begin
  if (coalesce(old.numer, 0) <> 0) then begin
    select ct.numbergen, ct.symbol
      from contrtypes ct
      where ct.ref = old.contrtype
      into :nazwa, :contrtype;

    execute procedure free_number(:nazwa, '', substring(:contrtype from 1 for 20), old.datazal, old.numer, 0)
      returning_values :blockref;
  end
end^
SET TERM ; ^
