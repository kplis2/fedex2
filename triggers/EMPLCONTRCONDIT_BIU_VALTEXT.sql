--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRCONDIT_BIU_VALTEXT FOR EMPLCONTRCONDIT                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable isdate smallint;
declare variable etype integer;
begin
  select e.etype
    from edictcontrcondit e
    where e.ref = new.contrcondit
    into :etype;

    if (etype in (1,3) ) then
      new.val_text = cast(new.val_numeric as varchar(255));
    else if (etype = 2) then
      new.val_text = cast(new.val_date as varchar(255));
end^
SET TERM ; ^
