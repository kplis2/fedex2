--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TRASY_BI1_REPLICAT FOR TRASY                          
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
 execute procedure rp_trigger_bi('TRASY',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
