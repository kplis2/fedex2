--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODDZIALY_AU0 FOR ODDZIALY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable odd varchar(255);
declare variable cnt smallint;
declare variable slodef integer;
declare variable currentyear integer;
begin
  if(new.glowny = 1 and old.glowny = 0) then
    update ODDZIALY set GLOWNY = 0 where COMPANY = new.company and ODDZIAL <> new.oddzial;
  if((new.location <> old.location) or (new.symbol <> old.symbol))then begin
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values :odd;
    if(:odd = old.symbol) then begin
      select count(*) from KONFIG where AKRONIM = 'AKTULOCAT' into :cnt;
      if(:cnt > 0) then
        update KONFIG set WARTOSC = new.location where AKRONIM = 'AKTULOCAT';
      else
        insert into KONFIG(AKRONIM,WARTOSC,ORD) values('AKTULOCAT',new.location,1);
      if(old.symbol <> new.symbol) then
        update KONFIG set WARTOSC = new.symbol where AKRONIM = 'AKTUODDZIAL';
    end
  end

  -- aktulizacja opisu kont ksiegowych dla bieżącego roku
  if(new.nazwa<>old.nazwa) then begin
    currentyear = extract(year from current_timestamp(0));
    for select ref from slodef where typ = 'ODDZIALY'
      into :slodef
    do begin
      execute procedure accounting_recalc_descript(:slodef, :currentyear, new.company, new.symbol);
    end
  end
end^
SET TERM ; ^
