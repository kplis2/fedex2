--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLPOS_BI0 FOR PRTOOLPOS                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.amount is null) then new.amount = 0;
  if (new.amountfree is null) then new.amountfree = 0;
  if (new.changeamount is null) then new.changeamount = 0;
end^
SET TERM ; ^
