--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SECURITY_LOG_PARAMS_BI_REF FOR SECURITY_LOG_PARAMS            
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0) then
    execute procedure gen_ref('SECURITY_LOG_PARAMS') returning_values new.ref;
end^
SET TERM ; ^
