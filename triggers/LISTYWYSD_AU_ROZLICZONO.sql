--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_AU_ROZLICZONO FOR LISTYWYSD                      
  ACTIVE AFTER UPDATE POSITION 2 
as
declare variable kalkulator varchar(255);
declare variable proced varchar(255);
begin
  if(new.rozliczono<>old.rozliczono and new.rozliczono=1) then begin
    if(new.sposdost is not null) then begin
      select kalkulatorrozlicz from sposdost where ref=new.sposdost into :kalkulator;
      if(:kalkulator is not null and :kalkulator<>'') then begin
        proced = 'execute procedure '||:kalkulator||'('||new.ref||')';
        if(:proced is not null) then execute statement :proced;
      end
    end
  end
end^
SET TERM ; ^
