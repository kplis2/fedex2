--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TEST_BS107032_BI FOR TEST_BS107032                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_test_bs107032_id,1);
end^
SET TERM ; ^
