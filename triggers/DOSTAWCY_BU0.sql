--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_BU0 FOR DOSTAWCY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable wymagajnip VARCHAR(40);
  declare variable dl smallint;
  declare variable contactcontrol varchar(100);
begin
 if(new.powiazany is null) then new.powiazany = 0;
 if(new.bank ='') then new.bank = null;
 if(new.waluta  = '') then new.waluta = NULL;
 if(new.id = '') then exception DOSTAWCY_IDEMPTY;
 if(new.rachunek is null) then new.rachunek = '';
 if(coalesce(new.krajid,'') <> coalesce(old.krajid,'') and new.krajid is not null) then
   select name from countries where symbol = new.krajid into new.kraj;
  if(new.waluta is null) then begin
    execute procedure GETCONFIG('WALPLN') returning_values new.waluta;
    if(new.waluta is null) then new.waluta = 'PLN';
  end
 if(new.dostawamiara = '') then new.dostawamiara = null;

   /* czy wymagac NIPu */
  if (new.firma=1 and new.zagraniczny=0 and new.nip='') then
  begin
    execute procedure GETCONFIG('WYMAGAJNIP') returning_values :wymagajnip;
    if (:wymagajnip = '1') then exception BRAK_NIPU;
  end

  /* sprawdzenie dlugosci kodu ksiegowego */
  select KODLN from slodef where typ='DOSTAWCY' into :dl;
  if (:dl is not null) then
    if (coalesce(char_length(new.kontofk),0)<>0 and coalesce(char_length(new.kontofk),0) <> :dl) then  -- [DG] XXX ZG119346
      exception BAD_LENGTH_FKKOD;

  /* utworzenie prostego nipu */
  if(new.nip<>old.nip or (new.nip is not null and old.nip is null) or (new.nip is null and old.nip is not null)) then
    new.prostynip = replace(new.nip, '-', '');
  -- obsluga blokady dostawcy do przelewow
  if (new.blokada=0) then new.opisblokady='';

      -- sprawdzenie poprawnosci telefonu
  if (new.telefon <> old.telefon) then begin
    execute procedure get_config('CONTACTCONTROL',2) returning_values :contactcontrol;
    if (:contactcontrol is null) then contactcontrol = '0';
    if ((:contactcontrol = '1') and (new.telefon is not null)) then
      execute procedure contact_control('PHONE',new.telefon,0,'DOSTAWCY','TELEFON',new.ref,new.id);
  end
end^
SET TERM ; ^
