--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_BIU0 FOR EPAYROLLS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  select ealgtype, coalesce(isaddit,0)
    from eprollstypes
    where prtype = new.prtype
    into new.ealgtype, new.isaddit;

  if (new.status is null) then
    new.status = 0;
  if (new.ehrmstatus is null) then
    new.ehrmstatus = 0;
  if (old.status = 0 and new.status = 1 and new.ehrmstatus = 0) then
    new.ehrmstatus = 1;
  if (old.status = 0 and new.status = 2)
    then exception payroll_is_open;

  if (new.lumpsumtax is null) then
    new.lumpsumtax = 0;
  if (new.corlumpsumtax = 0) then
    new.corlumpsumtax = null;
  if (updating and new.corlumpsumtax is null and old.corlumpsumtax is not null)
    then new.lumpsumtax = 0;

 --PR22136:
  if (new.prtype = 'PRN') then
    new.empltype = 3;
  else if (new.prtype = 'UCP') then
    new.empltype = 2;
  else
    new.empltype = 1;

 --PR30950:
  if (new.prtype = 'UCP') then begin
    new.separate = null;
    new.tosbase = null;
    new.tovbase = null;
    if (new.billdate is null and (inserting or new.billdate is distinct from old.billdate)) then
      exception universal 'Należy określić datę wystawienia rachunku!';  --PR55960
  end else begin
    new.separate = coalesce(new.separate,0);
    new.tosbase = coalesce(new.tosbase,1);
    new.tovbase = coalesce(new.tovbase,1);
  end
end^
SET TERM ; ^
