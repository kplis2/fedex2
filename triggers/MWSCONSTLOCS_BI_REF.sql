--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCS_BI_REF FOR MWSCONSTLOCS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSCONSTLOCS') returning_values new.ref;
end^
SET TERM ; ^
