--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFREJZAM_BU_REPLICAT FOR DEFREJZAM                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.symbol<>old.symbol or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null)
      or new.nazwa<>old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
      or new.szkice<>old.szkice or (new.szkice is null and old.szkice is not null) or (new.szkice is not null and old.szkice is null)
      or new.stan<>old.stan or (new.stan is null and old.stan is not null) or (new.stan is not null and old.stan is null)
      or new.number_gen<>old.number_gen or (new.number_gen is null and old.number_gen is not null) or (new.number_gen is not null and old.number_gen is null)
      or new.stan<>old.stan or (new.stan is null and old.stan is not null) or (new.stan is not null and old.stan is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('DEFREJZAM',old.symbol, null, null, null, null, old.token, old.state,
        new.symbol, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
