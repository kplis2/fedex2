--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_ITEM_AU0 FOR RP_ITEM                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.STABLE <> old.STABLE) then
     delete from RP_ITEM where ID_OBJ = old.ID_OBJ and PARENT_ID = old.ref_id;
  if((new.ord = 0 or (new.ord = 3)) and old.ord <> 2) then
   execute procedure order_RP_ITEM(new.id_obj,new.parent_id,0);

end^
SET TERM ; ^
