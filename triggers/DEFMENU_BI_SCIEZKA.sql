--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMENU_BI_SCIEZKA FOR DEFMENU                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable symbol varchar(255);
begin
  if(new.poziom is not null and new.poziom<>0) then
    select sciezka from DEFMENU where ref=new.poziom into new.sciezka;
  if(new.sciezka is null) then new.sciezka = '';
  symbol = '';
  if(new.elem is not null) then
    select WARTOSC from DEFELEM where REF=new.elem into :symbol;
  new.sciezka = new.sciezka || :symbol;
end^
SET TERM ; ^
