--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMETHODCOLS_BI FOR PRMETHODCOLS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null) then begin
    execute procedure GEN_REF('PRMETHODCOLS') returning_values new.ref;
  end
  if(new.calckind is null) then new.calckind = 0;
  if(new.calcfilter is null) then new.calcfilter = '';
  if (new.calctoparent is null) then new.calctoparent = 1;
  if (new.color is null) then new.color = 0;
end^
SET TERM ; ^
