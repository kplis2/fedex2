--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCEBASES_AD_EABSENCES FOR EABSENCEBASES                  
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
--MWr Aktualizacja stawek na nieobecnoci po usunieciu podstawy z rozliczenia nieob.

  update eabsences
    set monthpaybase = null,
        hourpayamount = null
    where baseabsence = old.eabsence;
end^
SET TERM ; ^
