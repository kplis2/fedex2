--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BI FOR DOKUMNAG                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable NUMBERG varchar(40);
declare variable lokres varchar(6);
declare variable numpoakcept smallint;
declare variable cnt integer;
declare variable chronologia smallint;
declare variable notserial smallint;
declare variable slodeftyp varchar(20);
declare variable prostynipkon varchar(40);
declare variable prostynipdok varchar(40);
declare variable blokadafak smallint;
BEGIN
  if(new.operator = 0) then new.operator = NULL;
  if(new.operator is null) then
    execute procedure get_global_param('AKTUOPERATOR') returning_values new.operator;
  if(new.operator is null) then exception DOKUMNAG_OPERATOR 'Brak wskazania operatora na '||new.symbol;

  --sprawdzenie uprawnień operatora
  if(not exists(select magazyn from opermag where magazyn=new.magazyn and operator=new.operator and
               (dokumtypesnewright is null or dokumtypesnewright like '%;'||new.typ||';%'))) then
    exception DOKUMNAG_OPERATOR substring('Operator nie ma uprawnień do wystawiania '||new.typ||' na '||new.magazyn from 1 for 75);

  if (not exists(select first 1 1 from defdokummag d where d.magazyn = new.magazyn and d.typ = new.typ)) then
    exception dokumnag_expt 'Typ dokumentu '||new.typ||' nie jest dozwolony na magazynie '||new.magazyn;

  if(new.akcept is null or (new.akcept <> 8 and new.akcept <> 7)) then
    select autoakc ,magbreak from defmagaz where defmagaz.symbol = new.magazyn into new.akcept, new.magbreak;
  select defdokummag.bezfaktur from defdokummag where magazyn=new.magazyn and typ=new.typ into :blokadafak;


  if(:blokadafak is not null) then
   new.blokadafak=:blokadafak;
  if(new.AKCEPT is NULL) then new.AKCEPT = 0;
  if(new.AKCEPT = 1) then new.dataakc = current_date;
  if(new.blokada is null) then new.blokada = 0;
  if(new.blokadafak is null) then new.blokadafak = 0;
  if(new.acceptmistake is null) then new.acceptmistake = 0;
  if(new.sellmistake is null) then new.sellmistake = 0;
  if(new.checkmistake is null) then new.checkmistake = 0;
  if(new.wydano is null) then new.wydano = 0;
  if(new.wartosc is null) then new.wartosc = 0;
  if(new.pwartosc is null) then new.pwartosc = 0;
  if(new.wartoscl is null) then new.wartoscl = 0;
  if(new.sumwartwal is null) then new.sumwartwal = 0;
  if(new.sumwartzl is null) then new.sumwartzl = 0;
  if(new.wartoscwal is null) then new.wartoscwal = 0;
  if(new.waluta='') then new.waluta = NULL;
  if(new.kurs is null) then new.kurs = 1;
  if(new.wydrukowano is null) then new.wydrukowano = 0;
  if(new.waga is null) then new.waga = 0;
  if(new.objetosc is null) then new.objetosc = 0;
  if(new.proglojdone is null) then new.proglojdone = 0;
  if(new.status is null) then new.status = 0;
  if(new.ilosclwys is null) then new.ilosclwys = 0;
  new.wartosczl = new.wartoscwal * new.kurs;
  new.odchylenie = new.sumwartzl - new.wartosczl;
  if(new.notserialized is null) then new.notserialized = 0;
  if(new.magazyn <>'' and new.notserialized=0) then begin
    select NOTSERIAL from DEFMAGAZ where SYMBOL = new.magazyn into :notserial;
    if(:notserial = 1) then new.notserialized = 1;
  end
  select WALUTOWY from DEFDOKUM where symbol = new.typ into new.walutowy;
  if(new.bn is null or new.bn=' ') then
    select BN from DEFDOKUM where symbol = new.typ into new.bn;
  if(new.bn is null or new.bn=' ') then new.bn = 'N';

  if(new.magbreak is null) then new.magbreak = 0;
  if(new.wysylkadone is null) then new.wysylkadone = 0;
  if((new.symbol is null or (new.symbol = '')) AND (new.numer is null or (new.numer = 0))) then begin


    /*numerator*/
    select DEFMAGAZ.numpoakcept, DEFMAGAZ.number_gen from DEFMAGAZ where SYMBOL=new.magazyn into :numpoakcept, :numberg;
    if(:numpoakcept is null) then numpoakcept = 0;
    if(:numberg is not null) then
    begin
       --exception universal numpoakcept||'   '||new.akcept;

      if(:numpoakcept = 1 and new.akcept = 0) then
      begin
        new.numer = -1;
        new.symbol = 'TYM/'||cast(new.ref as varchar(20));
      end
      else
      begin
        execute procedure Get_NUMBER(:numberg, new.magazyn, new.typ, new.data, new.numblock, new.ref) returning_values new.numer, new.symbol;
        new.numblock = null;
        new.oldsymbol = new.numer || ';' || new.symbol;
      end
    end

  end
  /*sprawdzenie okresu z data */
  if((new.oddzial is null) or (new.oddzial = '')) then  begin
    select ODDZIAL from DEFMAGAZ where SYMBOL = new.magazyn into new.oddzial;
    if(new.oddzial is null or (new.oddzial = '')) then
      execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
  end
  if(new.company is null) then select company from oddzialy where oddzial = new.oddzial into new.company;
  if(((new.oddzial2 is null) or (new.oddzial2 = '')) and new.mag2 <>'') then
    select ODDZIAL from DEFMAGAZ where SYMBOL = new.mag2 into new.oddzial2;
  lokres = new.okres;
    lokres = cast( extract( year from new.data) as char(4));
    if(extract(month from new.data) < 10) then
       lokres = :lokres ||'0' ||cast(extract(month from new.data) as char(1));
    else begin
       lokres = :lokres ||cast(extract(month from new.data) as char(2));
    end
  if((new.okres is null) or (new.okres = '      ')) then
    new.okres = :lokres;
  else if(new.okres <> :lokres) then exception DOKUMNAG_DATA_POZA_OKRES;
  /*kontrola akceptacji dokumentow pozniejszych */
  if(new.akcept = 1)  then begin
    select DEFMAGAZ.chronologia from DEFMAGAZ where SYMBOL=new.magazyn into :chronologia;
    if(:chronologia is null) then chronologia = 0;
    if(:chronologia > 0) then begin
      cnt = null;
      select count(*) from DOKUMNAG where MAGAZYN=new.magazyn and DATA>=(new.data+:chronologia) and akcept = 1 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt > 0) then
        exception DOKUMNG_NEW_AKCEP_POZNIEJ;
    end
  end
  if(new.akcept=7 and new.nip is not null and new.nip<>'') then begin
    if(new.klient is not null) then begin
      prostynipkon=null;
      select PROSTYNIP from KLIENCI where REF=new.klient into :prostynipkon;
      prostynipdok = replace(new.nip, '-', '');
      if(prostynipdok<>prostynipkon or prostynipkon is null) then begin
        new.klient = null;
        select min(REF) from KLIENCI where PROSTYNIP = :prostynipdok into new.klient;
        if(new.klient is null) then select min(REF) from KLIENCI where FSKROT = new.nip into new.klient;
      end
      if(new.klient is null) then exception DOKUMNAG_NOKONTRAH 'Podczas zakladania dok. mag. nie znaleziono klienta: '||new.nip;
    end
    if(new.dostawca is not null) then begin
      prostynipkon = null;
      select PROSTYNIP from DOSTAWCY where REF=new.dostawca into :prostynipkon;
      prostynipdok = replace(new.nip, '-', '');
      if(prostynipdok <> :prostynipkon or :prostynipkon is null) then begin
        new.dostawca = null;
        select min(REF) from DOSTAWCY where PROSTYNIP = :prostynipdok into new.dostawca;
        if(new.dostawca is null) then select min(REF) from DOSTAWCY where ID=new.nip into new.dostawca;
      end
      if(new.dostawca is null) then exception DOKUMNAG_NOKONTRAH 'Podczas zakladania dok. mag. nie znaleziono dostawcy: '||new.nip;
    end
  end
  if(new.klient > 0) then begin
    if(coalesce(new.slopoz,0)=0) then select REF from SLODEF where TYP='KLIENCI' into new.slodef;
    select TYP from SLODEF where REF=new.slodef into :slodeftyp;
    if(:slodeftyp='KLIENCI' and (new.slopoz is null or new.slopoz=0)) then new.slopoz = new.klient;
    new.nip = null;
    select NIP from KLIENCI where REF=new.klient into new.NIP;
    if(new.NIP is null or (new.NIP='')) then select FSKROT from KLIENCI where REF=new.klient into new.NIP;
  end else if(new.dostawca > 0) then begin
    if(coalesce(new.slopoz,0)=0) then select REF from SLODEF where  TYP='DOSTAWCY' into new.slodef;
    select TYP from SLODEF where REF=new.slodef into :slodeftyp;
    if(:slodeftyp='DOSTAWCY' and (new.slopoz is null or new.slopoz=0)) then new.slopoz = new.dostawca;
    new.nip = null;
    select NIP from DOSTAWCY where REF=new.dostawca into new.NIP;
    if(new.NIP is null or (new.NIP='')) then select ID from DOSTAWCY where REF=new.dostawca into new.NIP;
  end
  new.wasdeakcept = 0;
  if(new.wysylka = 0) then new.wysylka = null;
  if(new.trasadost = 0) then new.trasadost = null;
  if(new.wysylka > 0) then begin
    /*tu tylko mogl operator wskazac wysylki niezamknite - lub replikacja*/
    select listywys.trasa, listywys.dataplwys from LISTYWYS where ref=new.wysylka and STAN = 'O' into new.trasadost, new.termdost;
  end
  if(new.odbiorcaid  > 0)then begin
   select ODBIORCY.odbiorcaspedyc from ODBIORCY where ref=new.odbiorcaid into new.odbiorcaidsped;
   if(new.odbiorcaidsped is null) then
     new.odbiorcaidsped = new.odbiorcaid;
  end else
    new.odbiorcaidsped = null;
  if(new.grupasped is null) then new.grupasped = new.ref;
  if(new.grupasped <> new.ref) then
    select SYMBOL from DOKUMNAG where ref = new.grupasped into new.grupaspeddesc;
  else
    new.grupaspeddesc = '';
  if(new.grupasped <> new.ref) then
    select DOKUMNAG.SPOSDOST, DOKUMNAG.FLAGISPED, DOKUMNAG.sposdostauto from DOKUMNAG where REF=new.grupasped into new.sposdost, new.flagisped, new.sposdostauto;
  else if(new.sposdostauto is null) then begin
   if(new.sposdost > 0) then select AUTOZMIANA from SPOSDOSt where ref=new.sposdost into new.sposdostauto;
  end
  if(new.sposdostauto is null) then new.sposdostauto = 0;
  if(new.krajwysylki = '') then new.krajwysylki = null;
  if(new.historia is not null) then select skad, dokumentmain from histzam where ref = new.historia into new.skad, new.dokumentmain;

  if(new.faktura is not null) then begin
    execute procedure DOKUMNAG_GET_SYMBFAK(new.ref,new.faktura) returning_values new.symbolfak;
  end
  --redundantne dane dotyczące typu dokumentu
  select dd.wydania, dd.zewn, dd.koryg, dd.transport
    from defdokum dd
    where dd.symbol = new.typ
  into new.wydania, new.zewn, new.koryg, new.transport;
end^
SET TERM ; ^
