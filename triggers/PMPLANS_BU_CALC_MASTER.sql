--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANS_BU_CALC_MASTER FOR PMPLANS                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (coalesce(new.pmplanmaster,0) <> coalesce(old.pmplanmaster,0)) then
  begin
    if (old.pmplanmaster is not null) then
      execute procedure pmplans_calc (old.pmplanmaster);
    if (new.pmplanmaster is not null) then
      execute procedure pmplans_calc (new.pmplanmaster);
  end
end^
SET TERM ; ^
