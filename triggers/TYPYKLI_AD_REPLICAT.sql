--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPYKLI_AD_REPLICAT FOR TYPYKLI                        
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('TYPYKLI', old.ref, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
