--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_AI_NOTATKA FOR KONTAKTY                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable notref integer;
declare variable konnazwa varchar(40);
begin
  if(new.tworznotatke=1 and new.notatka is null) then begin
    notref = gen_id(GEN_CNOTATKI,1);
    select NAZWA from CTYPYKON where REF=new.rodzaj into :konnazwa;
    insert into CNOTATKI(REF,NAZWA,DATA,TRESC,CPODMIOT,PKOSOBA,KONTAKT,OPERATOR)
    values (:notref,:konnazwa,new.data,new.opis,new.cpodmiot,new.pkosoba,new.ref,new.operator);
    update KONTAKTY set notatka = :notref where REF=new.ref;
  end
end^
SET TERM ; ^
