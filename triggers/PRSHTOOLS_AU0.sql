--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHTOOLS_AU0 FOR PRSHTOOLS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if((new.econdition <> old.econdition) or (new.econdition is not null and old.econdition is null) or (new.econdition is null and old.econdition is not null)
    or (new.ehourfactor <> old.ehourfactor) or (new.ehourfactor is not null and old.ehourfactor is null) or (new.ehourfactor is null and old.ehourfactor is not null)
    or (new.esheetrate <> old.esheetrate) or (new.esheetrate is not null and old.esheetrate is null) or (new.esheetrate is null and old.esheetrate is not null)
  ) then begin
    execute procedure prsheets_calculate(new.sheet,'','S');
  end
end^
SET TERM ; ^
