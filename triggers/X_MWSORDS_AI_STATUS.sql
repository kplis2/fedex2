--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSORDS_AI_STATUS FOR MWSORDS                        
  ACTIVE AFTER INSERT POSITION 50 
as
  declare variable grupasped dokumnag_id;
  declare variable x_imp_ref integer_id;
begin
  if (not exists (select 1 from mwsords ms where  ms.ref <> new.ref and ms.docgroup = new.docgroup)) then begin

    select d.grupasped from dokumnag d where d.ref = new.docid into :grupasped;
    for
      select d.int_id from dokumnag d
        where d.grupasped = :grupasped
          and coalesce(d.x_imp_ref,0) <> 0   --jak sie recznie wystawia wz to nie generuje WT, bo znajduje pusty string... Ldz
      into :x_imp_ref
    do begin
      update x_imp_dokumnag xd set xd.status = 12, xd.mwsstatus = 1
        where xd.nagid = :x_imp_ref;
    end
  end
end^
SET TERM ; ^
