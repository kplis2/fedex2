--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMHIER_BI0 FOR FRDPERSDIMHIER                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.ord is null) then new.ord = 0;
  if(new.val1editable is null) then new.val1editable = 0;
  if(new.val2editable is null) then new.val2editable = 0;
  if(new.val3editable is null) then new.val3editable = 0;
  if(new.val4editable is null) then new.val4editable = 0;
  if (new.plancalcmeth is null) then new.plancalcmeth = 0;
  if (new.realcalcmeth is null) then new.realcalcmeth = 0;
  if (new.val1calcmeth is null) then new.val1calcmeth = 0;
  if (new.val2calcmeth is null) then new.val2calcmeth = 0;
  if (new.val3calcmeth is null) then new.val3calcmeth = 0;
  if (new.val4calcmeth is null) then new.val4calcmeth = 0;
  if (new.val5calcmeth is null) then new.val5calcmeth = 0;
  if (new.plancalcfrom is null) then new.plancalcfrom = 0;
  if (new.realcalcfrom is null) then new.realcalcfrom = 1;
  if (new.val1calcfrom is null) then new.val1calcfrom = 2;
  if (new.val2calcfrom is null) then new.val2calcfrom = 3;
  if (new.val3calcfrom is null) then new.val3calcfrom = 4;
  if (new.val4calcfrom is null) then new.val4calcfrom = 5;
  if (new.val5calcfrom is null) then new.val5calcfrom = 6;
end^
SET TERM ; ^
