--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCARTCOLOURS_BI_REF FOR MWSCARTCOLOURS                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSCARTCOLOURS') returning_values new.ref;
end^
SET TERM ; ^
