--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_BD0 FOR MWSSTOCK                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.quantity > 0 or old.blocked > 0 or old.reserved > 0 or old.ordsuspended > 0
      or old.suspended > 0 or old.ordered > 0
  ) then
    exception MWSSTOCK_OVER_ZERO;
end^
SET TERM ; ^
