--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_BI1 FOR WHSECROWS                      
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  if (new.w is null) then new.w = 0;
  if (new.whsecdict is null) then
    select symbol||'/' from whsecs where ref = new.whsec into new.whsecdict;
end^
SET TERM ; ^
