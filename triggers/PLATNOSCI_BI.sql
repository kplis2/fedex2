--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLATNOSCI_BI FOR PLATNOSCI                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable status integer;
declare variable msg varchar(60);
BEGIN
  if (new.witryna is null) then new.witryna = 0;
  if (new.autozap is null) then new.autozap = 0;
  if (new.rachunek is not null) then  begin
    execute procedure control_bankaccount_number(new.rachunek,'PL') returning_values :status, :msg;
    if(status = 0) then exception universal :msg;
  end
  if (new.rabat is null) then new.rabat = 0;
  if (new.rabat < 0) then exception universal 'Nie można podawać ujemnych rabatów.';
END^
SET TERM ; ^
