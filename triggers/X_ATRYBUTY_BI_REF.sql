--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_ATRYBUTY_BI_REF FOR X_ATRYBUTY                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('X_ATRYBUTY')
      returning_values new.REF;
end^
SET TERM ; ^
