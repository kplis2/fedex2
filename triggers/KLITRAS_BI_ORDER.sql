--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLITRAS_BI_ORDER FOR KLITRAS                        
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from klitras where trasa = new.trasa
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update klitras set ord = 0, numer = numer + 1
      where numer >= new.numer and trasa = new.trasa;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
