--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_BD_ORDER FOR POZFAK                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 5 or coalesce(old.fake, 0) > 0) then exit;
  update pozfak set ord = 1, numer = numer - 1
    where ref <> old.ref and numer > old.numer and dokument = old.dokument;
end^
SET TERM ; ^
