--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_BU0 FOR ZADANIA                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.data<>old.data or new.datado<>old.datado) then begin
    if(extract(hour from new.data)=0 and
       extract(minute from new.data)=0 and
       extract(hour from new.datado)=0 and
       extract(minute from new.datado)=0)
    then
      new.options = 1;
    else
      new.options = 0;
  end
  if(new.zadanie<>old.zadanie) then
    select wydarzenie from defzadan where ref=new.zadanie into new.wydarzenie;
end^
SET TERM ; ^
