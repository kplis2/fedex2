--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BUBKBLOCK FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.blokada = 3 and new.bkblock = 1) then
    exception bkblock;
  if (new.bkblock = 0) then
    new.bkblockdescript = '';
  if (new.bkblock = 1 and old.bkblock = 0 and (new.bkblockdescript = '' or new.bkblockdescript is null)) then
    exception universal 'Proszę podać powód blokady';
end^
SET TERM ; ^
