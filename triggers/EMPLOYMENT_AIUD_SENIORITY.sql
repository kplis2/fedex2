--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYMENT_AIUD_SENIORITY FOR EMPLOYMENT                     
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
  declare variable fromdate date;
  declare variable todate date;
  declare variable wfromdate date;
  declare variable wtodate date;
  declare variable employee integer;
  declare variable sy smallint;
  declare variable sm smallint;
  declare variable sd smallint;
  declare variable wref integer;
begin
  --DS: przelicza swiadectwa pracy jezeli jest to najwczesniejsza umowa
  --i przecina sie ze swiadectwem pracy
  employee = coalesce(new.employee,old.employee);

  fromdate = coalesce(new.fromdate, old.fromdate);
    if (new.fromdate is not null and old.fromdate is not null) then
      if (new.fromdate < old.fromdate) then
        fromdate = new.fromdate;
      else fromdate = old.fromdate;

  --jesli dany pracownik nie ma wczesniejszej umowy
  if (not exists(select ref
    from employment
      where fromdate < :fromdate
        and employee = coalesce(new.employee,old.employee))) then
  begin
    todate = coalesce(new.todate, old.todate);
    if (new.todate is not null and old.todate is not null) then
      if (new.todate > old.todate) then
        todate = new.todate;
      else todate = old.todate;

    --pobierz pierwsze swiadectwo pracy ktore konczy sie pozniej niz data umowy

    select first 1 w.ref, w.fromdate, w.todate
      from eworkcertifs w
      where w.employee = :employee
        and w.todate >= :fromdate
      order by w.fromdate
      into :wref, :wfromdate, :wtodate;

    if (wref is not null) then
    begin
      --i przelicz staz pracy dla danego swiadectwa, triggery przelicza dla nastepnych jezeli zajdzie potrzeba
      execute procedure count_seniority(:wref, :employee, :wfromdate, :wtodate,0) returning_values sy, sm, sd;
      update eworkcertifs w set w.syears = :sy, w.smonths = :sm, w.sdays = :sd where w.ref = :wref;
    end

  end



 /*
  select min(m.fromdate)
    from employment m
    where m.employee = coalesce(new.employee,old.employee)
    into :fromdate;
  select max(w.todate)
    from eworkcertifs w
    where w.employee = coalesce(new.employee,old.employee)
    into :todate;
  if (fromdate = coalesce(new.fromdate,old.fromdate) and (new.fromdate <= todate or old.fromdate <= todate)) then
  begin
    for
      select w.ref, w.employee, w.fromdate, w.todate
        from eworkcertifs w
        where w.employee = coalesce(new.employee,old.employee)
        --w kolejnosci wiekszy fromdate, todate, ref
       order by w.fromdate, w.todate, w.ref
       into wref, wemployee, wfromdate, wtodate
    do begin
      execute procedure count_seniority(:wref, :wemployee, :wfromdate, :wtodate,0) returning_values sy, sm, sd;
      update eworkcertifs w set w.syears = :sy, w.smonths = :sm, w.sdays = :sd where w.ref = :wref;
    end
  end    */
end^
SET TERM ; ^
