--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BU_ECONTRTYPE FOR EMPLCONTRACTS                  
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.econtrtype <> old.econtrtype) then
  begin
    if (exists (select first 1 1 from emplcontrcondit e where e.emplcontract = new.ref)) then
      exception EMPLCONTRACT_ECONTRTYPE;
  end
end^
SET TERM ; ^
