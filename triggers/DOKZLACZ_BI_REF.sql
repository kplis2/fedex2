--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKZLACZ_BI_REF FOR DOKZLACZ                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DOKZLACZ')
      returning_values new.REF;
end^
SET TERM ; ^
