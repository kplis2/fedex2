--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BU_OPER FOR DECREES                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.account <> old.account
    or new.side <> old.side
    or new.debit <> old.debit
    or new.credit <> old.credit
    or new.descript <> old.descript or (new.descript is not null and old.descript is null) or (new.descript is null and old.descript is not null)
    or new.settlement <> old.settlement or (new.settlement is not null and old.settlement is null ) or (new.settlement is null and old.settlement is not null)
    or new.stransdate <> old.stransdate or (new.stransdate is not null and old.stransdate is null ) or (new.stransdate is null and old.stransdate is not null)
    or new.opendate <> old.opendate or (new.opendate is not null and old.opendate is null ) or (new.opendate is null and old.opendate is not null)
    or new.payday <> old.payday or (new.payday is not null and old.payday is null ) or (new.payday is null and old.payday is not null)
    or new.soddzial <> old.soddzial or (new.soddzial is not null and old.soddzial is null ) or (new.soddzial is null and old.soddzial is not null)
    or new.settlcreate <> old.settlcreate or (new.settlcreate is not null and old.settlcreate is null ) or (new.settlcreate is null and old.settlcreate is not null)
    or new.dist1symbol <> old.dist1symbol or (new.dist1symbol is not null and old.dist1symbol is null ) or (new.dist1symbol is null and old.dist1symbol is not null)
    or new.dist2symbol <> old.dist2symbol or (new.dist2symbol is not null and old.dist2symbol is null ) or (new.dist2symbol is null and old.dist2symbol is not null)
    or new.dist3symbol <> old.dist3symbol or (new.dist3symbol is not null and old.dist3symbol is null ) or (new.dist3symbol is null and old.dist3symbol is not null)
    or new.dist4symbol <> old.dist4symbol or (new.dist4symbol is not null and old.dist4symbol is null ) or (new.dist4symbol is null and old.dist4symbol is not null)
    or new.dist5symbol <> old.dist5symbol or (new.dist5symbol is not null and old.dist5symbol is null ) or (new.dist5symbol is null and old.dist5symbol is not null)
    or new.dist6symbol <> old.dist6symbol or (new.dist6symbol is not null and old.dist6symbol is null ) or (new.dist6symbol is null and old.dist6symbol is not null))
  then begin
    execute procedure get_global_param('AKTUOPERATOR') returning_values new.changeoper;
    new.changedate = current_timestamp(0);
  end
end^
SET TERM ; ^
