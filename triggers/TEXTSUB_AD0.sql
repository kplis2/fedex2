--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TEXTSUB_AD0 FOR TEXTSUB                        
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  execute procedure DESYNC_TEXTSUB(old.symbol);
end^
SET TERM ; ^
