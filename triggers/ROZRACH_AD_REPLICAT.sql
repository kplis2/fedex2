--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_AD_REPLICAT FOR ROZRACH                        
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('ROZRACH', old.slodef, old.slopoz, old.symbfak, old.kontofk, old.company, old.token, old.STATE);
end^
SET TERM ; ^
