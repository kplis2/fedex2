--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_BU0 FOR DEFMAGAZ                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable dokumcount integer;
begin
  if (new.spedgrupalacz is null) then new.spedgrupalacz = 0;
  if (new.chronologiadostaw is null) then new.chronologiadostaw = 0;
  if(new.typ ='E' or new.typ='S') then new.fifo=' ';
  if(new.typ<>old.typ) then begin
   select count(*) from DOKUMNAG where MAGAZYN = old.symbol into :dokumcount;
   if(:dokumcount > 0) then exception DEFMAGAZ_CHANGE_NOT_ALLOWED;
  end
  if(new.symbol <> old.symbol or (new.oddzial <> old.oddzial)) then begin
   select count(*) from DOKUMNAG where MAGAZYN = old.symbol into :dokumcount;
   if(:dokumcount > 0) then exception DEFMAGAZ_CHANGE_NOT_ALLOWED;
   dokumcount = null;
   select count(*) from POZZAM where MAGAZYN = old.symbol into :dokumcount;
   if(:dokumcount > 0) then exception DEFMAGAZ_CHANGE_NOT_ALLOWED;
   update stanycen set MAGAZYN=new.symbol, ODDZIAL = new.oddzial  where MAGAZYN=old.symbol;
   update STANYIL set MAGAZYN=new.symbol,ODDZIAL = new.oddzial  where MAGAZYN=old.symbol;
  end
  if(new.AUTOAKC = 1 AND new.FIFO='R') then
   exception  DEFDOKUM_AUTOACEPT_NOT_ALLOWED;
end^
SET TERM ; ^
