--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AD_AUTOBO FOR BKDOCS                         
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.autobo = 1) then
    update bkperiods set status = 1 where company = old.company and id = old.otable;
end^
SET TERM ; ^
