--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDOPERATORS_BI_REF FOR MWSORDOPERATORS                
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
  begin
    execute procedure gen_ref('MWSORDOPERATORS') returning_values new.ref;
  end
end^
SET TERM ; ^
