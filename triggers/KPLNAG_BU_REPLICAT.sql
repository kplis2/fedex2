--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLNAG_BU_REPLICAT FOR KPLNAG                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if ((new.ref<>old.ref)
    or (new.nazwa<>old.nazwa) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
    or (new.ktm<>old.ktm) or (new.ktm is null and old.ktm is not null) or (new.ktm is not null and old.ktm is null)
    or (new.wersja<>old.wersja) or (new.wersja is null and old.wersja is not null) or (new.wersja is not null and old.wersja is null)
    or (new.wersjaref<>old.wersjaref) or (new.wersjaref is null and old.wersjaref is not null) or (new.wersjaref is not null and old.wersjaref is null)
    or (new.dataotw<>old.dataotw) or (new.dataotw is null and old.dataotw is not null) or (new.dataotw is not null and old.dataotw is null)
    or (new.datalast<>old.datalast) or (new.datalast is null and old.datalast is not null) or (new.datalast is not null and old.datalast is null)
    or (new.datazam<>old.datazam) or (new.datazam is null and old.datazam is not null) or (new.datazam is not null and old.datazam is null)
    or (new.akt<>old.akt) or (new.akt is null and old.akt is not null) or (new.akt is not null and old.akt is null)
    or (new.sumwartmag<>old.sumwartmag) or (new.sumwartmag is null and old.sumwartmag is not null) or (new.sumwartmag is not null and old.sumwartmag is null)
    or (new.cennik<>old.cennik) or (new.cennik is null and old.cennik is not null) or (new.cennik is not null and old.cennik is null)
    or (new.numer<>old.numer) or (new.numer is null and old.numer is not null) or (new.numer is not null and old.numer is null)
    or (new.typmag<>old.typmag) or (new.typmag is null and old.typmag is not null) or (new.typmag is not null and old.typmag is null)
    or (new.glowna<>old.glowna) or (new.glowna is null and old.glowna is not null) or (new.glowna is not null and old.glowna is null)
    ) then
   waschange = 1;

  execute procedure rp_trigger_bu('KPLNAG',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
