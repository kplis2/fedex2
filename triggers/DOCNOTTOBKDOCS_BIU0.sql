--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOCNOTTOBKDOCS_BIU0 FOR DOCNOTTOBKDOCS                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
begin
  if (new.param1 is null) then new.param1 = '';
  if (new.param2 is null) then new.param2 = '';
  if (new.param3 is null) then new.param3 = '';
  if (new.param1 = ' ') then new.param1 = '';
  if (new.param2 = ' ') then new.param2 = '';
  if (new.param3 = ' ') then new.param3 = '';
  if (new.param1 = '' and new.param2 = '' and new.param3 = '') then
    exception universal 'Przynajmniej jedno pole musi być wypełnione.';
end^
SET TERM ; ^
