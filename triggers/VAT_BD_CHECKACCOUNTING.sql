--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VAT_BD_CHECKACCOUNTING FOR VAT                            
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable dictdef integer;
begin
  select ref from slodef where typ = 'VAT'
    into :dictdef;
  execute procedure check_dict_using_in_accounts(dictdef, old.grupa, null);
end^
SET TERM ; ^
