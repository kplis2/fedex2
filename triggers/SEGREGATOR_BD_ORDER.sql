--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SEGREGATOR_BD_ORDER FOR SEGREGATOR                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update segregator set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and rodzic = old.rodzic;
end^
SET TERM ; ^
