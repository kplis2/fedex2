--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_AIU_NEXTYEAR FOR EVACLIMITS                     
  ACTIVE AFTER INSERT OR UPDATE POSITION 1 
AS
begin
/*Aktualizacja nastepnej karty urlopowej jezeli limit urlopu pozostalego sie zmienil
  lub biezaca karta zostaje usunieta*/

  if (inserting or new.restlimit <> coalesce(old.restlimit,0)
       or new.vyear <> old.vyear or new.employee <> old.employee
  ) then
    if (exists(select first 1 1 from evaclimits
                  where employee = new.employee and vyear = new.vyear + 1)
    ) then
      execute procedure e_vac_card((new.vyear + 1) || '/01/01', new.employee);
end^
SET TERM ; ^
