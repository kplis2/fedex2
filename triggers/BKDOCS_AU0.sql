--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AU0 FOR BKDOCS                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable dref decrees_id;
begin
  if (new.vatreg<>old.vatreg or (new.vatreg is NULL and old.vatreg is not NULL)
      or (new.vatreg is not NULL and old.vatreg is NULL))
  then
    update BKVATPOS set VATREG = new.vatreg where BKDOC = new.REF;

  if (new.bkreg <> old.bkreg or (new.bkreg is not null and old.bkreg is null)
      or (new.bkreg is null and old.bkreg is not null)
      or new.period <> old.period or new.transdate<>old.transdate)
  then
    update DECREES
      set bkreg = new.bkreg, period = new.period, transdate = new.transdate
      where bkdoc = new.ref;

  if (new.status<>old.status) then
  begin
    if (new.status>1 and old.status<2) then
    begin
      --naliczenie obrotow
      execute procedure compute_disttovers(new.ref);
    end
    if (new.status<2 and old.status>1) then begin
      -- usuniecie obrotow
      execute procedure uncompute_disttovers(new.ref);
    end
  end
  -- obsluga kont rozliczeniowych - zmiana slodef i slopoz
  if (new.dictdef <> old.dictdef or new.dictpos <> old.dictpos) then
  begin
    for
      select d.ref
        from decrees d
        where d.bkdoc = new.ref
        into :dref
    do begin
      delete from decreematchings m where m.decree = :dref;
    end
  end
end^
SET TERM ; ^
