--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_BI FOR ZADANIA                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable klient integer;
BEGIN
  if(new.ckontrakt=0) then new.ckontrakt=NULL;
  if(new.kontakt=0) then new.kontakt=NULL;
  new.datazal = current_date;
  if(extract(hour from new.data)=0 and
     extract(minute from new.data)=0 and
     extract(hour from new.datado)=0 and
     extract(minute from new.datado)=0)
  then
    new.options = 1;
  else
    new.options = 0;
  select wydarzenie from defzadan where ref=new.zadanie into new.wydarzenie;
END^
SET TERM ; ^
