--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODEFFORMP_AU0 FOR ODEFFORMP                      
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
   if(
     (new.htmlnazwa <> old.htmlnazwa) or
     (new.htmlid <> old.htmlid) or
     (new.htmltyp <> old.htmltyp) or
     (new.htmlwartosc <> old.htmlwartosc) or
     (new.rozmiar <> old.rozmiar) or
     (new.tytul <> old.tytul) or
     (new.komentarz <> old.komentarz) or
     (new.grupa <> old.grupa) or
     (new.poziom <> old.poziom) or
     (new.blad <> old.blad) or
     (new.extras <> old.extras) or
     (new.param <> old.param) or
     (new.numer <> old.numer) or
     (new.wartoscd <> old.wartoscd))
    then
      update ODEFFORMY set STATE=1 where SYMBOL=old.forma;
    if(old.forma <> new.forma ) then
      update ODEFFORMY set STATE=1 where SYMBOL=old.forma or (SYMBOL = new.forma);

end^
SET TERM ; ^
