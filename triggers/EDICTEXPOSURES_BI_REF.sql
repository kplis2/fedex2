--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTEXPOSURES_BI_REF FOR EDICTEXPOSURES                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
 if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EXPOSURETYPES') returning_values new.ref;
end^
SET TERM ; ^
