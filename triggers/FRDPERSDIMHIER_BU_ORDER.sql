--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMHIER_BU_ORDER FOR FRDPERSDIMHIER                 
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 1;
  if (new.hierorder is null) then new.hierorder = old.hierorder;
  if (new.ord = 0 or new.hierorder = old.hierorder) then
  begin
    new.ord = 1;
    exit;
  end
  --hierorderowanie obszarow w rzedzie gdy nie podamy hierorderu
  if (new.hierorder <> old.hierorder) then
    select max(hierorder) from frdpersdimhier where frdperspect = new.frdperspect
      into :maxnum;
  else
    maxnum = 0;
  if (new.hierorder < old.hierorder) then
  begin
    update frdpersdimhier set ord = 0, hierorder = hierorder + 1
      where ref <> new.ref and hierorder >= new.hierorder
        and hierorder < old.hierorder and frdperspect = new.frdperspect;
  end else if (new.hierorder > old.hierorder and new.hierorder <= maxnum) then
  begin
    update frdpersdimhier set ord = 0, hierorder = hierorder - 1
      where ref <> new.ref and hierorder <= new.hierorder
        and hierorder > old.hierorder and frdperspect = new.frdperspect;
  end else if (new.hierorder > old.hierorder and new.hierorder > maxnum) then
    new.hierorder = old.hierorder;
end^
SET TERM ; ^
