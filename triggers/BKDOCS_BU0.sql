--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BU0 FOR BKDOCS                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable status smallint;
  declare variable kind smallint;
  declare variable ptype smallint;
  declare variable account ACCOUNT_ID;
  declare variable yearid int;
  declare variable res smallint;
  declare variable dist1 integer;
  declare variable dist2 integer;
  declare variable dist3 integer;
  declare variable dist4 integer;
  declare variable dist5 integer;
  declare variable dist6 integer;
  declare variable aktuoperator integer;
  declare variable multivaluedist smallint;
  declare variable number_decrees integer;
  declare variable ref_decrees integer;
begin

  select kind
    from bkdoctypes where ref = new.doctype
    into :kind;

  if (new.status <> old.status and old.status = 3) then
    exception BKDOCS_ZAKSIEGOWANY;

  if (new.status < 2 and old.status > 1 and old.checked = 1) then
    exception bkdocs_sprawdzony;

  if (new.checked = 1 and old.checked = 0 and new.status < 2) then
    exception BKDOC_EXPT;

  if (new.status <> old.status) then
  begin
    if (old.status = 0 and new.status > 0) then
    begin
      --Jeżeli jestesmy koretką to nalicz status na korygowanym
      -- sprawdzamy, dodatkowo czy pozycje są niezerowe
      if (new.bkdocsvatcorrectionref is not null) then
      begin
        if(new.sumnetv = 0) then
          exception BDOCS_KOR_ACK;
        execute procedure bkdocs_vatcorstatuscalc(new.bkdocsvatcorrectionref);
      end
      execute procedure bkdocs_check_vat(new.ref);
      if (new.baldebit <> new.balcredit) then
      begin
        if (new.autodoc = 1) then
        begin
          new.status = old.status;
          new.accoper = null;
          new.bookoper = null;
        end else
        begin
          select yearid from bkperiods where company = new.company
            and id = new.period into :yearid;
          for select account
            from decrees
              where bkdoc = new.ref
            into :account
          do begin
            execute procedure check_account(new.company, :account, :yearid, new.autodoc)
              returning_values res, dist1, dist2, dist3, dist4, dist5, dist6, multivaluedist;
          end
          exception FK_BKDOC_NOT_BALANCED;
        end
      end
      if (not exists(select ref from decrees where bkdoc = new.ref)) then
      begin
        if (new.autodoc = 1) then
        begin
          new.status = old.status;
          new.accoper = null;
          new.bookoper = null;
        end else
        begin
          select ptype from bkperiods where company = new.company and id = new.period
            into :ptype;
          if ((ptype <> 0 or kind = 0) and new.bkdocsvatcorrectionref is null) then
            exception FK_BKDOC_NOT_DECREE;
        end
      end
      select yearid from bkperiods where company = new.company
          and id = new.period into :yearid;
      for select account, ref, number
        from decrees
          where bkdoc = new.ref
        into :account, :ref_decrees, number_decrees
      do begin
        execute procedure check_account(new.company, :account, :yearid, new.autodoc)
          returning_values res, dist1, dist2, dist3, dist4, dist5, dist6, multivaluedist;
        if (res = 1) then
        begin
          if (multivaluedist = 1 and (not exists (select first 1 1 from distpos where bkdoc = new.ref
            and decrees = :ref_decrees ))) then
          begin
            if (new.autodoc = 1) then
            begin
              new.status = old.status;
              new.accoper = null;
              new.bookoper = null;
            end else
              exception DECREES_EXPT 'Brakuje wyróżników w dekrecie na pozycji '|| :number_decrees;
          end
          ref_decrees = null;
          number_decrees = null;
          multivaluedist = null;
          account = null;
        end
      end
    end
    if (old.status < 1 and new.status > 0) then
      new.accdate = CURRENT_TIMESTAMP(0);
    if (old.status < 3 and new.status > 1 and new.status > old.status) then
    begin
      if (new.status = 2 and kind > 0 and new.vatreg is null) then
        exception VATTYPE_DOC;
      new.bookdate = CURRENT_TIMESTAMP(0);
    end
    if (old.status>1 and new.status<2) then
    begin
      new.bookoper = null;
      new.bookdate = null;
    end
    if (old.status>0 and new.status<1) then
    begin
      new.accoper = null;
      new.accdate = null;
    end
    if (old.checked = 1 and new.checked = 0) then
      new.checkdate = null;
    update decrees set status = new.status where bkdoc = new.ref;
    update distpos set status = new.status where bkdoc = new.ref;
    select min(status) from decrees where bkdoc = new.ref
      into :status;
    if (status <> new.status) then
    begin
      new.status = old.status;
      update decrees set status = new.status where bkdoc = new.ref;
      update distpos set status = new.status where bkdoc = new.ref;
    end
    if (new.autodoc = 1) then
    begin
      new.autodoc = 0;
      update decrees set autodoc = 0 where bkdoc = new.ref;
    end
    if (new.status = 3) then
    begin
      select max(nlnumber)
        from bkdocs
        where company = new.company and period = new.period and bkreg = new.bkreg
        into new.nlnumber;
      if (new.nlnumber is null) then
        new.nlnumber = 0;
      new.nlnumber = new.nlnumber + 1;
    end
  end

   if (old.period <> new.period
     and exists(select first 1 1 from bkregs where symbol = new.bkreg
     and yearnum = 0 and company = new.company)) then
       execute procedure BKDOCS_GETNUMBER(new.company, new.bkreg, new.period)
         returning_values new.number;

  if ((old.checked = 0 or old.checked is null) and new.checked = 1) then
    new.checkdate = CURRENT_TIMESTAMP(0);
  if (old.checked = 1 and new.checked = 0) then
    new.checkdate = null;

  if (coalesce(old.currate,1) <> coalesce(new.currate,1) and old.currate = old.curvatrate) then
    new.curvatrate = new.currate;

  if (coalesce(old.vatperiod, '') <> coalesce(new.vatperiod, '')) then begin
    execute procedure get_global_param ('AKTUOPERATOR')
      returning_values aktuoperator;
    new.vatoper = :aktuoperator;
    new.vatoperdate = current_timestamp(0);
  end

  if (new.costdistribution is not null and (old.costdistribution is null or old.costdistribution <> new.costdistribution)) then
    select cdtype from costdistribution where ref = new.costdistribution
      into new.cdtype;

  --zablokowanie mozliwosci zmian niektorych danych na zaakceptowanym dokumencie
  if ((new.period<>old.period or new.bkreg<>old.bkreg or coalesce(new.vatreg,'')<> coalesce(old.vatreg,'')
       or new.transdate<>old.transdate or coalesce(new.symbol,'')<>coalesce(old.symbol,'')) and old.status>0 ) then
     exception BKDOCS_ZAKSIEGOWANY 'Nie można dokonywać zmian na zaakceptowanym dokumencie.';

  --ksiegowanie i akceptacja dokumentu
  if((new.status = 2 and old.status = 0) and (new.accoper is null)) then
      execute procedure get_global_param('AKTUOPERATOR')
        returning_values new.accoper;

  if (old.status = 3) then
    if (old.period <> new.period or old.BKREG <> new.BKREG or old.number <> new.number or
    old.symbol <> new.symbol or old.transdate <> new.transdate or old.docdate <> new.docdate or
    coalesce(old.nlnumber,0) <> coalesce(new.nlnumber,0) or coalesce(old.vatperiod,'') <> coalesce(new.vatperiod,'') ) then
      exception BKDOCS_ZAKSIEGOWANY 'Nie można dokonywać zmian na zaksięgowanym dokumencie.';
end^
SET TERM ; ^
