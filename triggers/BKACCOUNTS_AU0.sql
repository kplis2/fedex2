--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_AU0 FOR BKACCOUNTS                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
  declare variable account ACCOUNT_ID;
  declare variable accounting integer;
  declare variable descript varchar(80);
  declare variable fulldescript varchar(255);
begin
  if (new.bktype <> old.bktype) then
    update accounting set bktype = new.bktype where bkaccount = new.ref;

  -- aktualizacja opisu kont ksiegowych
  if((new.descript<>old.descript) or (new.doopisu<>old.doopisu)) then
  begin
    if((new.yearid = extract(year from current_timestamp(0)))      -- w bieżącym roku bez ograniczeń
        or (not exists(select first 1 1 from accounting         -- lub dla kont bez zapisów
            where bkaccount = new.ref and yearid = new.yearid))) then begin
      for select account, ref
        from accounting
        where bkaccount = new.ref
        and yearid = new.yearid
        and company = new.company
      into :account, :accounting
      do begin
        execute procedure ACCOUNTING_GET_DESCRIPT(:account, new.yearid, new.company)
          returning_values :descript, :fulldescript;
        update accounting set descript = :descript where ref = :accounting;
        update accounting set fulldescript = :fulldescript where ref = :accounting;
      end
    end
  end
end^
SET TERM ; ^
