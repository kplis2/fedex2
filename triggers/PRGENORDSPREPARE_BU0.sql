--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRGENORDSPREPARE_BU0 FOR PRGENORDSPREPARE               
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
    if (new.amount <> old.amount and new.status = 0) then new.status = 1;
    if (new.status = 3) then new.status = 0;
    if (new.nagzam is not null and new.amount > new.amountinord) then
      exception prnagzam_error 'Realizacja zamowienia klienta, nie mozna przekraczac ilosci zamowienia.';
end^
SET TERM ; ^
