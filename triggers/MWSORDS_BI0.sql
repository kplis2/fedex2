--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_BI0 FOR MWSORDS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable NUMBERG varchar(40);
declare variable numpoakcept smallint;
begin
  select stocktakingsettlmode, stocktaking, lotreg from mwsordtypes where ref = new.mwsordtype
    into new.stocktakingsettlmode, new.stocktaking, new.lotreg;
  if (new.stocktakingsettlmode is null) then
    new.stocktakingsettlmode = 0;
  if (new.stocktaking is null) then
    new.stocktaking = 0;
  if (new.lotreg is null) then
    new.lotreg = 0;
  new.regtime = current_timestamp(0);
  if (new.status = 1) then
    new.accepttime = current_timestamp(0);
  if (new.status is null) then new.status = 0;
  if (new.timestartdecl is null) then
    new.timestartdecl = current_timestamp(0);
  if (new.mwsordtype is not null) then
    select mwsordtypes.symbol from mwsordtypes where mwsordtypes.ref = new.mwsordtype
      into new.mwsordtypes;
  if (new.wh is null or new.wh = '') then
    exception WH_NOT_SET;
  if((new.symbol is null or (new.symbol = '')) AND (new.number is null or (new.number = 0))) then begin
    /*numerator*/
    select DEFMAGAZ.numpoakcept, DEFMAGAZ.number_mwsordgen
       from DEFMAGAZ where SYMBOL=new.wh into :numpoakcept, :numberg;
    if(:numpoakcept is null) then numpoakcept = 0;
    if(:numberg is not null) then begin
      if(:numpoakcept = 1 and new.status = 0) then begin
        new.number = -1;
        new.symbol = 'TYM/'||cast(new.ref as varchar(20));
      end else begin
        execute procedure Get_NUMBER(:numberg, new.wh, new.mwsordtypes, new.regtime, new.numblock,null)
          returning_values new.number, new.symbol;
        new.numblock = null;
      end
    end
  end
end^
SET TERM ; ^
