--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDPOZ_AU0 FOR LISTYWYSDPOZ                   
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable waga numeric(15,4);
declare variable objetosc numeric(15,4);
declare variable kontrolapak smallint;
begin
  if(new.ilosc <> old.ilosc and new.doktyp='M') then
    update DOKUMPOZ set ILOSCLWYS = ILOSCLWYS + (new.ilosc - old.ilosc)  where ref = new.dokpoz;
  if(new.ilosc <> old.ilosc and new.doktyp='Z') then
    execute procedure NAGZAM_CHECKWYS(new.dokref);

  select kontrolapak from listywysd where ref = new.dokument
  into :kontrolapak;
  if (:kontrolapak = 0) then
  begin
    if(coalesce(new.waga,0) <> coalesce(old.waga,0)) then begin
      select sum(waga) from listywysdpoz where dokument=new.dokument into :waga;
      if(:waga is null) then waga = 0;
      update LISTYWYSD set WAGA = :waga where ref=new.dokument;
    end
    if (coalesce(new.objetosc,0) <> coalesce(old.objetosc,0)) then begin
      select sum(objetosc) from listywysdpoz where dokument=new.dokument into :objetosc;
      if(:objetosc is null) then objetosc = 0;
      update LISTYWYSD set objetosc = :objetosc where ref=new.dokument;
    end
  end
--  if(new.spedwartosc<>old.spedwartosc or (new.spedwartosc is not null and old.spedwartosc is null) or (new.spedwartosc is null and old.spedwartosc is not null)) then
--   update LISTYWYSD set kosztdost = 0 where ref=new.dokument; --aby przeliczyc koszt
end^
SET TERM ; ^
