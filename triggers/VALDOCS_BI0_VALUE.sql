--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_BI0_VALUE FOR VALDOCS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.tvalue is null) then
    new.tvalue = 0;
  if (new.fvalue is null) then
    new.fvalue = 0;
end^
SET TERM ; ^
