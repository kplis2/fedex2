--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_KOREKTA_AI FOR NAGFAK                         
  ACTIVE AFTER INSERT POSITION 10 
as
declare variable local integer;
declare variable koryg smallint;
declare variable newline varchar(3);
declare variable dozwrejestry varchar(30);
begin
  newline = '
';
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  if(:local = 1) then begin
    select typfak.korekta
      from nagfak
      left join typfak on (typfak.symbol = nagfak.typ)
      where nagfak.ref = new.ref
      into :koryg;
    select stantypfak.fakturujtypfak
      from stantypfak
      where stantypfak.stansprzed = new.stanowisko and stantypfak.typfak = new.typ
      into :dozwrejestry;
    if(:koryg = 1 and :dozwrejestry <> '' and :dozwrejestry not like '%'||new.rejestr||'%') then
       exception universal 'Wybrano niewlasciwy rejestr.'||:newline||'Wybierz jeden z rejestrów: '||:dozwrejestry;
  end
end^
SET TERM ; ^
