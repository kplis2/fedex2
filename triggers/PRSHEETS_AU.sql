--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_AU FOR PRSHEETS                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.status=2 and old.status<>2) then begin
    update PRSHEETS set STATUS=1 where STATUS=2 and KTM=new.ktm and WERSJA=new.wersja and REF<>new.ref;
  end
end^
SET TERM ; ^
