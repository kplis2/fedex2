--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BI_ACCREF FOR DECREES                        
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable accref integer;
  declare variable yearid int;
begin
  if (new.account is not null) then
  begin
    select yearid from bkperiods where company = new.company and id = new.period into :yearid;
    select ref from bkaccounts
      where symbol = substring(new.account from 1 for 3) and yearid = :yearid
        and company = new.company
    into :accref;
    if (new.accref is null or (accref <> new.accref)) then
      new.accref = :accref;
  end
end^
SET TERM ; ^
