--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMACHINES_AD FOR PRMACHINES                     
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if(old.rezzasob > 0) then
    delete from REZZASOBY where PRMACHINE = old.ref;

  delete from dokzlacz d where d.ztable = 'PRMACHINES' and d.zdokum = old.ref;
end^
SET TERM ; ^
