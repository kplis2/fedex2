--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPWOJ49M_BI_REF FOR CPWOJ49M                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPWOJ49M')
      returning_values new.REF;
end^
SET TERM ; ^
