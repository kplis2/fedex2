--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_BU_PRZELICZ FOR PMPOSITIONS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin

-- pozycja planu operacyjnego - dane kosztorysowe
  if(new.quantity<>old.quantity or new.calcprice<>old.calcprice) then
    new.calcval = new.quantity * new.calcprice;

-- pozycja planu operacyjnego - dane budżetowe
  if(new.quantity<>old.quantity or new.budprice<>old.budprice) then
    new.budval = new.quantity * new.budprice;
  -- wartość urealniona
  if(new.quantity<>0 and (new.quantity<>old.quantity or new.realq<>old.realq or new.budval<>old.budval or new.realval<>old.realval)) then
    new.budvalr = (1 - (new.realq/new.quantity)) * new.budval + new.realval;
  if(new.quantity=0) then 
    new.budvalr = 0;

-- pozycja planu operacyjnego - dane wykonawcze
   if (new.usedq > 0) then
     new.usedprice = new.usedval / new.usedq;
   else
    new.usedprice = 0;
-- kolor
   if(new.usedval>new.budval) then new.kolor = 1; else new.kolor = 0;
end^
SET TERM ; ^
