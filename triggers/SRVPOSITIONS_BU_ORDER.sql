--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVPOSITIONS_BU_ORDER FOR SRVPOSITIONS                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 1;
  if (new.number is null) then new.number = old.number;
  if (new.ord = 0 or new.number = old.number) then
  begin
    new.ord = 1;
    exit;
  end
  --numberowanie obszarow w rzedzie gdy nie podamy numberu
  if (new.number <> old.number) then
    select max(number) from srvpositions where srvrequest = new.srvrequest
      into :maxnum;
  else
    maxnum = 0;
  if (new.number < old.number) then
  begin
    update srvpositions set ord = 0, number = number + 1
      where ref <> new.ref and number >= new.number
        and number < old.number and srvrequest = new.srvrequest;
  end else if (new.number > old.number and new.number <= maxnum) then
  begin
    update srvpositions set ord = 0, number = number - 1
      where ref <> new.ref and number <= new.number
        and number > old.number and srvrequest = new.srvrequest;
  end else if (new.number > old.number and new.number > maxnum) then
    new.number = old.number;
end^
SET TERM ; ^
