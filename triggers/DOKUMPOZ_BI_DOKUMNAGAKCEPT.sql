--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BI_DOKUMNAGAKCEPT FOR DOKUMPOZ                       
  ACTIVE BEFORE INSERT POSITION 3 
AS
begin
  if (new.roz_blok=0) then
    if (exists(select ref from dokumnag where dokumnag.ref = new.dokument and dokumnag.akcept = 1)) then
      exception DOKUMNAG_AKCEPT;
end^
SET TERM ; ^
