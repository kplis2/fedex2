--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OLAPCUBESLAYOUTS_BD_OWNER FOR OLAPCUBESLAYOUTS               
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable isadmin smallint;
  declare variable curroperator integer;
begin
  execute procedure get_global_param('AKTUOPERATOR') returning_values :curroperator;
  select o.administrator from operator o where o.ref = :curroperator into :isadmin;
  if (isadmin = 1 and old.owner <> 0) then exception cant_delete_olap;
  if (isadmin = 0 and curroperator <> old.owner) then exception cant_delete_olap;
end^
SET TERM ; ^
