--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETCA_BI_ACCOUNT FOR FXDASSETCA                     
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable sum_share numeric(14,2);
begin
  select sum(fxc.share)
    from fxdassetca fxc
    where fxc.fxdasset = coalesce(new.fxdasset,0)
      and fxc.amperiod = coalesce(new.amperiod,0)
      and fxc.typeamortization = coalesce(new.typeamortization,0)
    into :sum_share;
  sum_share = coalesce(sum_share,0) + coalesce(new.share,0);
  if (sum_share > 100) then
    exception universal 'Bląd - przekroczono 100%.';
  new.typeamortization = coalesce(new.typeamortization,0);
end^
SET TERM ; ^
