--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCTYPES_BI0 FOR MWSCONSTLOCTYPES               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSCONSTLOCTYPES') returning_values new.ref;
end^
SET TERM ; ^
