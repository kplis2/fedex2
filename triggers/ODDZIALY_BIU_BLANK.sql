--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODDZIALY_BIU_BLANK FOR ODDZIALY                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.oddzial = '') then new.oddzial = null;
  if (new.nazwa = '') then new.nazwa = null;
  if (new.symbol = '') then new.symbol = null;
end^
SET TERM ; ^
