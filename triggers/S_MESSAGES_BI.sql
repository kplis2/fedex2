--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_MESSAGES_BI FOR S_MESSAGES                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable newid varchar(40);
BEGIN
  IF ((NEW.REF IS NULL) or (new.ref=0)) THEN
      NEW.REF = GEN_ID(GEN_S_MESSAGES,1);
  new.senddate = current_timestamp(0);
  if(new.deliverdate is null) then new.deliverdate = current_timestamp(0);
  if(new.status is null) then new.status = 0;
  if(new.priority is null) then new.priority = 0;
  if(new.id is null or (new.id='')) then begin
    select max(id) from S_MESSAGES where
      ((FROMOPER=new.fromoper and TOOPER=new.tooper) or
      (FROMOPER=new.tooper and TOOPER=new.fromoper)) and
      (DELIVERDATE>current_timestamp(0)-0.1) into :newid;
    if(:newid is null or (:newid='')) then newid = new.ref;
    new.id = :newid;
  end
END^
SET TERM ; ^
