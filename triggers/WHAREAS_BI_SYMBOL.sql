--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHAREAS_BI_SYMBOL FOR WHAREAS                        
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable cnt integer;
declare variable tmp varchar(3);
declare variable cnt1 integer;
begin
  if (new.mansymbol is not null and new.mansymbol = 1) then
    exit;
  if (new.whsecdict is null or new.whsecdict = '') then
    select symbol from whsecs where ref = new.whsec into new.whsecdict;
  if (new.whsecrowdict is null  or new.whsecrowdict = '') then
    select symbol from whsecrows where ref = new.whsecrow into new.whsecrowdict;
  if (new.mwsstandseg is not null) then
  begin
    if (new.wh is not null and new.whsecdict is not null and new.whsecrowdict is not null
        and new.mwsstandnumber is not null and new.locnumber is not null
        and new.mwsstandsegtypedict is not null and new.number is not null
    ) then
    begin
      new.longsymbol = new.wh||'-'||new.whsecdict||'-'||new.whsecrowdict||'-'||
         new.number||'-'||new.mwsstandnumber||'-'||new.mwsstandsegtypedict||'-'||new.locnumber;
      select count(ref) from whareas
        where whsecrow = new.whsecrow
          and ((areatype = new.areatype and new.areatype <> 1 and new.areatype <> 3)
            or (areatype in (1,3) and new.areatype in (1,3)))
          and number < new.number and ref <> new.ref
        into cnt;
      if (cnt is null) then cnt = 0;
      if (new.areatype = 1 or new.areatype = 3) then
        select goodaeranumstart from whsecrows where ref = new.whsecrow
          into cnt1;
      if (cnt1 is null) then cnt1 = 0;
      cnt = cnt + cnt1 + 1;
      if (cnt < 10) then tmp = '0'||cnt;
      else tmp =cast(cnt as varchar(3));
      new.symbol = new.whsecdict||'-'||new.whsecrowdict||'-'||tmp;
    end
  end
  else
  begin
    if (new.wh is not null and new.whsecdict is not null and new.whsecrowdict is not null
    ) then
    begin
      select count(REF)
        from whareas
        where whsecrow = new.whsecrow and mwsstandseg is null
          and (number <= new.number or new.number is null)
          and ref <> new.ref
        into cnt;
      if (cnt is null) then cnt = 0;
      cnt = cnt + 1;
      if (cnt < 10) then tmp = '0'||cnt;
      else tmp =cast(cnt as varchar(3));
      new.longsymbol = new.wh||'-'||new.whsecdict||'-'||new.whsecrowdict||'-0-'||tmp;
      new.symbol = new.whsecdict||'-'||new.whsecrowdict||'-0-'||tmp;
    end
  end
end^
SET TERM ; ^
