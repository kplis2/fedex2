--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPEMPLSORTDEF_GEN_EMPLSORTPOS FOR BHPEMPLSORTDEF                 
  ACTIVE AFTER INSERT POSITION 2 
as
begin
/*MWr: Generowanie artykulow BHP wg sortu przypisanego do pracownika. Trigger
  musi byc na After, gdyz na Before nie jest dostepna info. o wybranym sorcie*/

  execute procedure bhpsort_gen_empl(new.ref,0);
end^
SET TERM ; ^
