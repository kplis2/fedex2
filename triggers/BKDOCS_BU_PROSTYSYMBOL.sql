--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BU_PROSTYSYMBOL FOR BKDOCS                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.dictdef is not null) then
  begin
    if (new.status > 1 and (old.symbol <> new.symbol or old.prostysymbol is null)) then
    begin
      new.prostysymbol = new.symbol;
      new.prostysymbol = replace(new.prostysymbol,  ' ',  ''); -- regula nr 1 wycinamy spacje z symbolu
      new.prostysymbol = replace(new.prostysymbol,  ';',  '');
      new.prostysymbol = replace(new.prostysymbol, ',', '');
      new.prostysymbol = replace(new.prostysymbol,  '_',  '');
      new.prostysymbol = replace(new.prostysymbol,  '-',  '');
      new.prostysymbol = replace(new.prostysymbol,  '+',  '');
      new.prostysymbol = replace(new.prostysymbol,  '=',  '');
      new.prostysymbol = replace(new.prostysymbol,  '/',  '');
      new.prostysymbol = replace(new.prostysymbol,  '\',  '');
      new.prostysymbol = replace(new.prostysymbol,  '?',  '');
      new.prostysymbol = replace(new.prostysymbol,  '!',  '');
      new.prostysymbol = replace(new.prostysymbol,  '.',  '');
      new.prostysymbol = replace(new.prostysymbol,  '|',  '');
      -- regula musi byc spujna/zgodna z trigerem na nagfak
    end
  end
  if (new.prostysymbol = '') then
    new.prostysymbol = null;
end^
SET TERM ; ^
