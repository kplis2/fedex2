--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_AU_CENNIK FOR WERSJE                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable status integer;
declare variable refdefcen integer;
declare variable aktuoddzial varchar(20);
declare variable ileprzel integer;
declare variable iledod integer;
begin
  if((new.cena_zakn<>old.cena_zakn) or
     (new.cena_kosztn<>old.cena_kosztn) or
     (new.cena_fabn<>old.cena_fabn)) then
  begin
    execute procedure CENNIK_AKTUMARZA(new.ref);
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
    for select ref from DEFCENNIK
          where AKTUZAKUP=1 and AKT=1 and ((ODDZIAL=:aktuoddzial) or (ODDZIAL='') or (ODDZIAL is NULL))
          order by kolejnosc
        into :refdefcen
    do begin
      execute procedure CENNIK_AKTUALIZUJ(:refdefcen, new.ref, 1, 0) returning_values :status,:ileprzel,:iledod;
    end
  end
end^
SET TERM ; ^
