--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPACCIDENT_BI FOR BHPACCIDENT                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('BHPACCIDENT') returning_values new.REF;
END^
SET TERM ; ^
