--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AI FOR POZFAK                         
  ACTIVE AFTER INSERT POSITION 0 
as
begin
   execute procedure FAK_POZ_CHANGED(new.dokument,new.ref, new.gr_vat, new.pgr_vat);
   if(new.fromdokumpoz > 0) then
     update DOKUMPOZ set FROMPOZFAK = new.ref where ref = new.fromdokumpoz;
  if(new.waga > 0) then
    update NAGFAK set WAGA = WAGA + new.waga where ref =new.dokument;
  if(new.objetosc > 0) then
      update NAGFAK set objetosc = objetosc + new.objetosc where ref =new.dokument;
end^
SET TERM ; ^
