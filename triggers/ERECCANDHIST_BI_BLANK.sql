--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECCANDHIST_BI_BLANK FOR ERECCANDHIST                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  new.regtimestamp = current_timestamp;
  if (new.opertimestamp is null) then new.opertimestamp = new.regtimestamp;
end^
SET TERM ; ^
