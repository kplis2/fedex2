--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETS_BU0 FOR FXDASSETS                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable kodln smallint;
  declare variable isintasset smallint;
begin

  if (coalesce(new.tvalue,0) <> coalesce(old.tvalue,0) or coalesce(new.fvalue,0) <> coalesce(old.fvalue,0)
    or coalesce(new.tredemption,0) <> coalesce(old.tredemption,0) or coalesce(new.fredemption,0) <> coalesce(old.fredemption,0)
    or coalesce(new.tvaluegross,0) <> coalesce(old.tvaluegross,0) or coalesce(new.fvaluegross,0) <> coalesce(old.fvaluegross,0)
    or coalesce(new.tredemptiongross,0) <> coalesce(old.tredemptiongross,0) or coalesce(new.fredemptiongross,0) <> coalesce(old.fredemptiongross,0)
    or coalesce(new.symbol,'') <> coalesce(old.symbol,'') or new.symbol = '' or coalesce(new.amortgrp,'') <> coalesce(old.amortgrp,'')
    or coalesce(new.name,'') <> coalesce(old.name,'') or new.name = ''
  ) then begin

    new.tvalue = coalesce(new.tvalue,0);
    new.fvalue = coalesce(new.fvalue,0);
    new.tredemption = coalesce(new.tredemption,0);
    new.fredemption = coalesce(new.fredemption,0);
    new.tvaluegross = coalesce(new.tvaluegross,0);
    new.fvaluegross = coalesce(new.fvaluegross,0);
    new.tredemptiongross = coalesce(new.tredemptiongross,0);
    new.fredemptiongross = coalesce(new.fredemptiongross,0);
    if (new.name = '') then new.name = null;
    if (new.symbol = '') then new.symbol = null;
    new.amgrp = substring(new.amortgrp from 1 for 1);

    select kodln from slodef
      where typ = 'FXDASSETS'
      into :kodln;
    if (kodln is not null and coalesce(char_length(new.symbol),0)<>kodln) then  -- [DG] XXX ZG119346
      exception bad_length_fkkod;
  
    select isintasset from amortgrp
      where symbol = new.amortgrp
      into :isintasset;
    new.isintasset = isintasset;

    if (coalesce(old.tvalue,0) <> new.tvalue) then
      new.tvaluegross = new.tvaluegross + (new.tvalue - coalesce(old.tvalue,0));
    if (coalesce(old.fvalue,0) <> new.fvalue) then
      new.fvaluegross = new.fvaluegross + (new.fvalue - coalesce(old.fvalue,0));
    if (coalesce(old.tredemption,0) <> new.tredemption) then
      new.tredemptiongross = new.tredemptiongross + (new.tredemption - coalesce(old.tredemption,0));
    if (coalesce(old.fredemption,0) <> new.fredemption) then
      new.fredemptiongross = new.fredemptiongross + (new.fredemption - coalesce(old.fredemption,0));
  end
end^
SET TERM ; ^
