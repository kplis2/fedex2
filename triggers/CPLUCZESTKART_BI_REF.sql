--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZESTKART_BI_REF FOR CPLUCZESTKART                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPLUCZESTKART')
      returning_values new.REF;
end^
SET TERM ; ^
