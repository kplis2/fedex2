--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BIU_BLANK FOR EMPLCONTRACTS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
--Trigger blankujacy, zastepuje wczesniejszy trigger EMPLCONTRACTS_BIU_CHK

  if (new.salary is null) then new.salary = 0;
  if (new.caddsalary is null) then new.caddsalary = 0;
  if (new.funcsalary is null) then new.funcsalary = 0;
  if (new.dictpos = 0) then new.dictpos = null;
  if (new.prcosts is null) then new.prcosts = 0;
  if (new.dischargecontract is null) then new.dischargecontract = 0;
  if (new.branch = '') then new.branch = null;
  if (new.cbranch = '') then new.cbranch = null;
  if (new.department = '') then new.department = null;
  if (new.cdepartment = '') then new.cdepartment = null;
  if (new.enddate is null and new.todate is not null) then new.enddate = new.todate;
  if (new.contrdate is null) then new.contrdate = new.fromdate;

  if (inserting or new.econtrtype <> old.econtrtype or
      coalesce(new.dimnum,0) <> coalesce(old.dimnum,0) or
      coalesce(new.dimden,0) <> coalesce(old.dimden,0)
  ) then begin
    select empltype from econtrtypes
      where contrtype = new.econtrtype
      into new.empltype;

    if (new.empltype <> 1) then
    begin
      new.funcsalary = null;
      new.caddsalary = null;
      new.workcat = null;
      new.dimnum = null;
      new.dimden = null;
      new.workdim = null;
      new.monthstosb = null;
      new.paidto = null;
      new.etermination = null;
    end else if (coalesce(new.dimden,0) <= 0 or coalesce(new.dimnum,0) <= 0 or new.dimnum > new.dimden) then
      exception universal 'Niewłaściwie określony wymiar zatrudnienia.';
    else
      new.workdim = cast(new.dimnum as float) / cast(new.dimden as float);
  end

  if (inserting or new.employee <> old.employee) then
    select company from employees where ref = new.employee into new.company;

  if (inserting or coalesce(new.workpost,0) <> coalesce(old.workpost,0)) then
    if (new.workpost is null) then new.worktype = null;
    else select worktype from edictworkposts where ref = new.workpost into new.worktype;

  if (inserting or coalesce(new.department,'') <> coalesce(old.department,'')) then
    if (new.department is null) then new.departmentlist = '';
    else select departmentlist from departments where symbol = new.department into new.departmentlist;

  --BS39732
  if (new.startdate is null) then
    new.startdate = new.fromdate;
  else if (not (new.fromdate = old.fromdate and new.econtrtype = old.econtrtype)) then begin
    if (exists (select first 1 1 from econtrtypes where contrtype = new.econtrtype and showstartdate = 0)) then
      new.startdate = new.fromdate;
  end
end^
SET TERM ; ^
