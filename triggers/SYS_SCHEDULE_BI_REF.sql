--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SYS_SCHEDULE_BI_REF FOR SYS_SCHEDULE                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('SYS_SCHEDULE')
      returning_values new.ref;
end^
SET TERM ; ^
