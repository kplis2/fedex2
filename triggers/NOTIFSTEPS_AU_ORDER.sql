--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSTEPS_AU_ORDER FOR NOTIFSTEPS                     
  ACTIVE AFTER UPDATE POSITION 20 
as
begin
  if (old.number <> new.number) then
  begin
    update notifsteps n set n.number = n.number + 1
      where n.number >= new.number and n.ref <> new.ref and n.notification = new.notification;
    update notifsteps n set n.number = n.number - 1
      where n.number > old.number and n.ref <> new.ref and n.notification = new.notification;
  end
end^
SET TERM ; ^
