--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAMUP_BI_REF FOR NAGZAMUP                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NAGZAMUP')
      returning_values new.REF;
end^
SET TERM ; ^
