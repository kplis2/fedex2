--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOST_EDERULESAFTER_BU_ORDER FOR SPOSDOST_EDERULESAFTER         
  ACTIVE BEFORE UPDATE POSITION 1 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 1;
  if (new.number is null) then new.number = old.number;
  if (new.ord = 0 or new.number = old.number) then
  begin
    new.ord = 1;
    exit;
  end

  if (new.number <> old.number) then
    select max(number) from SPOSDOST_EDERULESAFTER where sposdost = new.sposdost
      into :maxnum;
  else
    maxnum = 0;
  if (new.number < old.number) then
  begin
    update SPOSDOST_EDERULESAFTER set ord = 0, number = number + 1
      where ref <> new.ref and number >= new.number
        and number < old.number and sposdost = new.sposdost;
  end else if (new.number > old.number and new.number <= maxnum) then
  begin
    update SPOSDOST_EDERULESAFTER set ord = 0, number = number - 1
      where ref <> new.ref and number <= new.number
        and number > old.number and sposdost = new.sposdost;
  end else if (new.number > old.number and new.number > maxnum) then
    new.number = old.number;
end^
SET TERM ; ^
