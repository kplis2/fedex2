--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BI_OBL_POZ FOR POZZAM                         
  ACTIVE BEFORE INSERT POSITION 6 
as
declare variable bn char(1);
declare variable cennik integer;
declare variable cena DECIMAL(14,4);
declare variable rabat DECIMAL(14,2);
declare variable stawka_vat DECIMAL(14,2);
declare variable local integer;
declare variable kurs numeric(14,4);
declare variable wydania smallint;
declare variable zewn smallint;
declare variable zamtyp smallint;
declare variable kilosc decimal(14,4);
declare variable jednprzelicz numeric(14,4);
declare variable jednconst smallint;
declare variable walnag varchar(3);
declare variable walpln varchar(3);
declare variable rabatkask smallint;
declare variable dostawca integer;
declare variable rabatnag varchar(40);
declare variable jednoprzelicz numeric(14,4);
declare variable sprec varchar(20);
declare variable eksport smallint;
begin
  execute procedure check_local('POZZAM',new.ref) returning_values :local;
  if(:local = 1) then begin
    select TYPZAM.WYDANIA,TYPZAM.ZEWN,NAGZAM.TYP,NAGZAM.KILOSC,NAGZAM.DOSTAWCA
      from TYPZAM
        join NAGZAM on (NAGZAM.TYPZAM = TYPZAM.SYMBOL)
        join POZZAM on (POZZAM.ZAMOWIENIE = NAGZAM.REF)
      where POZZAM.REF = new.ref into :wydania,:zewn,:zamtyp,:kilosc, :dostawca;
    select nagzam.rabat, nagzam.waluta,nagzam.kurs,nagzam.bn, nagzam.eksport
      from NAGZAM
      where nagzam.ref = new.zamowienie
      into :rabat, :walnag, :kurs, :bn, :eksport;
    if(:eksport = 1)then
      execute procedure GETCONFIG('VATEXPORT') returning_values new.gr_vat;
    else if(new.gr_vat is not null) then begin
      select vat.stawka from vat where vat.grupa=new.gr_vat into :stawka_vat;
    end else begin
      select vat.stawka, vat.grupa
        from wersje
          join vat on (vat.grupa = wersje.vat)
        where ref = new.wersjaref
        into :stawka_vat, new.gr_vat;
      if (new.gr_vat is null) then
        select vat.stawka,vat.grupa
          from vat
            join towary on(towary.vat = vat.grupa)
          where ktm = new.ktm
          into :stawka_vat,new.gr_vat;
    end
    if(:stawka_vat is null) then stawka_vat = 0;
    if (bn is null) then
      execute procedure GETCONFIG('WGCEN') returning_values :bn;
    if((:bn <> 'B') and (:bn <> 'N') ) then bn = 'N';
    if(:eksport is null) then eksport = 0;
    if(:rabat is null) then rabat = 0;
    if(new.refcennik is not null) then cennik = new.refcennik;
    else select KCENNIK from NAGZAM where nagzam.ref = new.zamowienie into :cennik;
    if(:cennik is null) then
      select grupykli.cennik from klienci join grupykli on(klienci.grupa = grupykli.ref) join nagzam on(klienci.ref = nagzam.klient) where nagzam.ref = new.zamowienie into :cennik;
    /*ustalenie jednostki w zależnosci od rodzaju zamowienia*/
    if(new.jedn is null or (new.jedn = 0)) then begin
      if(:wydania = 1 and :zewn = 1) then
        select REF from TOWJEDN where KTM = new.ktm and C = 2 into new.jedn;
      else if(:wydania = 1 and :zewn = 0)then
        select REF from TOWJEDN where KTM = new.ktm and S = 2 into new.jedn;
      else if(:wydania = 0 ) then
        select ref from TOWJEDN where KTM = new.ktm and Z  = 2 into new.jedn;
      if(new.jedn is null or (new.jedn = 0))then
        select ref from TOWJEDN where KTM = new.KTM and GLOWNA = 1 into new.jedn;
    end
    if(new.jedno is null or (new.jedno = 0))then begin
      if(:wydania = 0) then
        select REF from TOWJEDN where KTM = new.ktm and Z = 2 into new.jedno;
      if(:wydania = 1) then
        select REF from TOWJEDN where KTM = new.ktm and S = 2 into new.jedno;
    end
    if(new.jedno is null or (new.jedno = 0))then
      new.jedno = new.jedn;
    if(new.jedn = new.jedno) then begin
      new.ilosco = new.ilosc;
      new.ilrealo = new.ilreal;
    end else begin
      select PRZELICZ, CONST from TOWJEDN where REF=new.jedno into :jednprzelicz, :jednconst;
      if(new.ilrealo is null or (new.ilrealo = 0) or (:jednconst = 1)) then
        new.ilrealo = new.ilreal / :jednprzelicz;
    end
    select PRZELICZ, CONST from TOWJEDN where REF=new.jedn into :jednprzelicz, :jednconst;
    if(new.iloscm is null or (new.iloscm = 0) or (:jednconst = 1)) then begin
      new.iloscm = new.ilosc * :jednprzelicz;
    end
    if(new.ilrealm is null or (new.ilrealm = 0) or (:jednconst = 1)) then
      new.ilrealm = new.ilreal * :jednprzelicz;
    select TOWJEDN.waga*new.ilosco, TOWJEDN.vol * new.ilosco from TOWJEDN where ref=new.jedno into new.waga, new.objetosc;
    /* obliczenie ceny */
    -- MSt: BS70028: Ceny sprzedazy zerowe dla fejkow
    if (new.fake = 1) then
    begin
      new.cenacen = 0;
      new.cenanet = 0;
      new.cenabru = 0;
      new.cenanetzl = 0;
      new.cenabruzl = 0;
    end
    -- MSt: Koniec
    if(new.cenacen is null) then begin
      if(:bn = 'B') then begin
        select  cenabru, waluta from cennik where cennik = :cennik and ktm = new.ktm and wersja = new.wersja and jedn = new.jedn into new.cenacen, new.walcen;
        if(new.cenacen is null) then
          select  cenabru, waluta from cennik where cennik = :cennik and ktm = new.ktm and wersja = 0 and jedn = new.jedn into new.cenacen, new.walcen;
      end else begin
        select  cenanet, waluta from cennik where cennik = :cennik and ktm = new.ktm and wersja = new.wersja and jedn = new.jedn into new.cenacen, new.walcen;
        if(new.cenacen is null) then
          select  cenanet, waluta from cennik where cennik = :cennik and ktm = new.ktm and wersja = 0 and jedn = new.jedn into new.cenacen, new.walcen;
      end
      cena = new.cenacen;
    end else
      cena = new.cenacen;

    /* przeliczenie walut cennika na walutę dokumenut*/
    if(:kurs is null or :kurs=0) then kurs = 1;
    if(new.walcen <> :walnag and new.walcen is not null and new.walcen <> '') then begin
      execute procedure GETCONFIG('WALPLN') returning_values :walpln;
      if(:walpln is null) then walpln = 'PLN';
      if(new.kurscen is null or new.kurscen=0) then new.kurscen = 1;
      cena = :cena * new.kurscen / :kurs;
    end else new.kurscen = 1;

    /*obliczenie rabatu */
    if(:cena is null) then cena = 0;
    if(new.prec is null or new.prec = 0) then begin
      if(:wydania=0) then execute procedure GETCONFIG('ZAKPREC') returning_values :sprec;
      else execute procedure GETCONFIG('SPRPREC') returning_values :sprec;
      if(:sprec is not null and :sprec<>'') then new.prec = cast(:sprec as integer);
    end
    execute procedure GETCONFIG('RABATNAG') returning_values :rabatnag;
    if(:wydania=1 and :rabatnag='1' and new.rabattab<>0) then begin
      if(new.prec=4) then cena = cast((:cena * (100- new.rabat) /100) as DECIMAL(14,4));
      else cena = cast((:cena * (100- new.rabat) /100) as DECIMAL(14,2));
    end else begin
      if(dostawca is not null and dostawca > 0) then
        select d.rabatkask from dostawcy d where d.ref = :dostawca into :rabatkask;
      if(rabatkask is not null and rabatkask = 1) then begin
        if(new.prec=4) then cena = cast(:cena * (100- :rabat)*(100 - new.rabat)/10000 as DECIMAL(14,4));
        else cena = cast(:cena * (100- :rabat)*(100 - new.rabat)/10000 as DECIMAL(14,2));
      end else begin
        if(new.rabat is not null) then rabat = rabat + new.rabat;
        if(:rabat <> 0)then begin
          if(new.prec=4) then cena = cast((:cena * (100- :rabat) /100) as DECIMAL(14,4));
          else cena = cast((:cena * (100- :rabat) /100) as DECIMAL(14,2));
        end
      end
    end

    /* obliczenie cen */
    if(:bn = 'B') then begin
       new.cenabru = cena;
       new.cenabruzl = cena * :kurs;
       new.wartbru = new.ilosc * new.cenabru;
       new.wartbruzl = new.ilosc * new.cenabruzl;
       new.kwartbru = new.kilosc * new.cenabru;

       new.wartnet = cast((new.wartbru / (1+:stawka_vat / 100)) as DECIMAL(14,2));
       new.wartnetzl = cast((new.wartbruzl / (1+:stawka_vat / 100)) as DECIMAL(14,2));
       new.cenanet = cast((new.cenabru / (1+(:stawka_vat/100))) as DECIMAL(14,2));
       new.cenanetzl = cast((new.cenabruzl / (1+(:stawka_vat/100))) as DECIMAL(14,2));
       new.kwartnet = cast((new.kwartbru / (1+:stawka_vat / 100)) as DECIMAL(14,2));
    end else begin
       new.cenanet = cena;
       new.cenanetzl = cena * :kurs;
       new.wartnet = new.ilosc * new.cenanet;
       new.wartnetzl = new.ilosc * new.cenanetzl;
       new.kwartnet = new.kilosc * new.cenanet;

       new.wartbru = new.wartnet + cast((new.wartnet * (:stawka_vat / 100)) as DECIMAL(14,2));
       new.wartbruzl = new.wartnetzl + cast((new.wartnetzl * (:stawka_vat / 100)) as DECIMAL(14,2));
       new.cenabru = new.cenanet + cast((new.cenanet * (:stawka_vat / 100)) as DECIMAL(14,2));
       new.cenabruzl = new.cenanetzl + cast((new.cenanetzl * (:stawka_vat / 100)) as DECIMAL(14,2));
       new.kwartbru = new.kwartnet + cast((new.wartnet * (:stawka_vat / 100)) as DECIMAL(14,2));
    end
    /*obliczanie cen dla realizaji*/
    if(:bn = 'B') then begin
      new.wartrbru = new.ilreal * new.cenabru;
      new.wartrbruzl = new.ilreal * new.cenabruzl;
      if(:eksport = 0) then begin
        new.wartrnet = cast((new.wartrbru / (1+:stawka_vat / 100)) as DECIMAL(14,2));
        new.wartrnetzl = cast((new.wartrbruzl / (1+:stawka_vat / 100)) as DECIMAL(14,2));
      end else begin
        new.wartrnet = new.wartrbru;
        new.wartrnetzl = new.wartrbruzl;
      end
    end else begin
      new.wartrnet = new.ilreal * new.cenanet;
      new.wartrnetzl = new.ilreal * new.cenanet;
      if(:eksport = 0) then begin
        new.wartrbru = new.wartrnet + cast((new.wartrnet * (:stawka_vat / 100)) as DECIMAL(14,2));
        new.wartrbruzl = new.wartrnetzl + cast((new.wartrnetzl * (:stawka_vat / 100)) as DECIMAL(14,2));
      end else begin
        new.wartrbru = new.wartrnet;
        new.wartrbruzl = new.wartrnetzl;
      end
    end
    /*obliczanie cen magazynowych*/
    new.wartmag = new.ilosc * new.cenamag;
    if(new.wartmag is null) then new.wartmag = 0;
    new.wartrmag = new.ilreal * new.cenamag;
    if(new.wartrmag is null) then new.wartrmag = 0;

    if(:kilosc > 0) then
      if(:zamtyp < 2) then begin
        new.kwartmag = new.wartmag / :kilosc;
      end
      else begin
        new.kwartmag = new.wartrmag / :kilosc;
      end
    else
      new.kwartmag = 0;

/*    new.cenabruzl = new.cenabru * :kurs;
    new.cenanetzl = new.cenanet * :kurs;
    new.wartbruzl = new.wartbru * :kurs;
    new.wartnetzl = new.wartnet * :kurs;
    new.wartrnetzl = new.wartrnet * :kurs;
    new.wartrbruzl = new.wartrbru * kurs;*/
  end
end^
SET TERM ; ^
