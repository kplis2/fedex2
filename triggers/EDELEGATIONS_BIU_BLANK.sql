--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELEGATIONS_BIU_BLANK FOR EDELEGATIONS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
  declare variable numerator varchar(40);
begin
  execute procedure getconfig('EDELEGATIONSNUM') returning_values numerator;
  if(numerator is not null and numerator <> '') then
    execute procedure get_number(:numerator, '', '', current_date, new.ref, new.ref) returning_values new.number, new.symbol;

  if(new.status is null) then new.status = 0;
  if(new.company is null) then
  begin
      select e.company from employees e where e.ref=new.employee into new.company;
  end
end^
SET TERM ; ^
