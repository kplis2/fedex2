--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVHISTORY_BI_REF FOR SRVHISTORY                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVHISTORY')
      returning_values new.REF;
end^
SET TERM ; ^
