--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKYEARS_BIU_WPK FOR BKYEARS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable ret smallint_id;
begin
  --Jeżeli dodajemy albo updatujemy rok ksigowy i podączamy synchronizacje
  --do zakadu wzorcowego
  if (new.patternref <> 0 and coalesce(old.patternref,0) = 0) then
  begin
    execute procedure allow_bkyear_synchronize (new.yearid,new.patternref)
      returning_values :ret;
    if(ret = 0) then
      exception wpk_bkyear_syn_err;
  end
  new.patternref = coalesce(new.patternref,0);
end^
SET TERM ; ^
