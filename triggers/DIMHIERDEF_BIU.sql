--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DIMHIERDEF_BIU FOR DIMHIERDEF                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.isslodef is null) then new.isslodef = 0;
  if (new.isslodef = 1 and new.slodef is null) then
    exception DIMDEF_ISSLODEF 'Wybrano opcję ze wskazaniem słownika';
  if (new.slodef is null and new.elemsaddmeth > 0 and new.elemsaddmeth < 3) then
    exception DIMDEF_NOTSLODEF_ADDWLEM 'Bez słownika tylko ręcznie lub przez zapytanie SQL';
  if (new.name is null or new.name = '') then
    exception DIMDEF_NAME 'Należy wypełnić pole Nazwa';
  if (new.elemsaddmeth = 4 and (new.sqlelem is null or new.sqlelem = '')) then
    exception DIMDEF_SQLELEM 'Brak zapytania generującego elementy';
end^
SET TERM ; ^
