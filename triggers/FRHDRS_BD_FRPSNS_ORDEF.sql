--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRHDRS_BD_FRPSNS_ORDEF FOR FRHDRS                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update frpsns set ord = 0 where frhdr = old.symbol;
end^
SET TERM ; ^
