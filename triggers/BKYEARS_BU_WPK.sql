--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKYEARS_BU_WPK FOR BKYEARS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  --Jeżeli chcemy odpiąć synchronizacje, należy odpiąć synchronizacje w strukturach
  --podlegych
  if (coalesce(old.patternref,0) <> 0 and coalesce(new.patternref,0) = 0) then
  begin
    update bkaccounts b
      set b.pattern_ref = null
      where b.yearid = new.yearid
        and b.company = new.company;
  end
end^
SET TERM ; ^
