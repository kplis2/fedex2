--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_BI_LISTA FOR CPLUCZEST                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.nadrzedny=0) then new.nadrzedny = null;
  if (new.nadrzedny > 0) then begin
    select lista from cpluczest where ref = new.nadrzedny into new.lista;
    if(new.lista is null) then new.lista = ';';
    new.lista = ';' || new.ref || new.lista;
  end else begin
    new.lista = ';'|| new.ref ||';';
  end
end^
SET TERM ; ^
