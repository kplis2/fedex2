--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSPECTS_BI_REF FOR FRDPERSPECTS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDPERSPECTS')
      returning_values new.REF;
end^
SET TERM ; ^
