--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECCANDS_BI_BLANK FOR ERECCANDS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable email string;
begin
  new.regtimestamp = current_timestamp(0);
  if (new.fromwww = 1) then
    new.status = 14;
--  new.info = 0;
  select email from ecandidates where ref = new.candidate into :email;
  if (email <> '') then
  begin
    select count(ref) from ecandidates where email = :email
      into new.qnt;
    new.qnt = new.qnt - 1;
  end
  if (new.qnt = 0) then new.qnt = null;
  if (new.notes is null) then new.notes = '';
  if (new.fromwww is null) then new.fromwww = 0;
end^
SET TERM ; ^
