--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACPLAN_BIU_BLANK FOR EVACPLAN                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (inserting or new.fromdate <> old.fromdate) then
    new.pyear = extract(year from new.fromdate);

  if (new.company is null or new.employee <> old.employee) then
    select company from employees where ref = new.employee
      into new.company;
end^
SET TERM ; ^
