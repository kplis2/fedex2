--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKSTNKAS_AI_RKUPRSTN FOR RKSTNKAS                       
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  -- automatyczne nadawanie praw operatorowi do zakadanego przez niego
  -- stanowiska kasowego
  insert into rkuprstn(stanowisko,operator,readonly,company)
  values(new.kod, (select PVALUE from get_global_param ('AKTUOPERATOR')),0,1);
end^
SET TERM ; ^
