--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_LOGHISTORY_BI FOR S_LOGHISTORY                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if(new.logtime is null) then new.logtime = current_timestamp(0);
  new.ref=gen_id(GEN_S_LOGHISTORY,1);
END^
SET TERM ; ^
