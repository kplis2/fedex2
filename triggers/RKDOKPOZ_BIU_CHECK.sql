--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BIU_CHECK FOR RKDOKPOZ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 8 
AS
begin
  if (new.kwota is null) then exception RK_WYPELNIJ_KWOTE;
--  if (new.kwota <= 0) then exception RKDOKPOZ_KWOATAWZERO;
  if (new.pozoper is null or new.pozoper = 0) then exception RKDOKPOZ_POZOPERNO;
end^
SET TERM ; ^
