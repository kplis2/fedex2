--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_BU__NORMREALIZATION FOR PROPERSRAPS                    
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (old.amount <> new.amount
    or old.maketimefrom <> new.maketimefrom
    or old.maketimeto <> new.maketimeto
  ) then
    select norm
      from XK_PROPERSRAPS_NORM(new.prschedoper, new.maketimeto - new.maketimefrom, new.amount)
      into new.normrealization;
end^
SET TERM ; ^
