--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_NAVIGATOR_BU FOR S_NAVIGATOR                    
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable prevparent varchar(255);
begin
  if(coalesce(new.parent,'')<>coalesce(old.parent,'')) then begin
    if(new.parent='') then new.parent = NULL;
    if(new.prev<>'<NONE>') then begin
      select parent from s_navigator where ref=new.prev into :prevparent;
      if(coalesce(:prevparent,'')<>coalesce(new.parent,'')) then new.prev = '<NONE>';
    end
    if(old.parent is not null) then
      if(not exists(select first 1 1 from s_navigator s where s.parent=old.parent and s.ref<>new.ref)) then
        update s_navigator set childrencount = 0 where ref = old.parent and childrencount = 1;
    if(new.parent is not null) then
      update s_navigator set childrencount = 1 where ref = new.parent and childrencount = 0;
  end
  if(new.prev is null or new.prev='') then new.prev = '<NONE>';
  if(position('=' in new.prev)>0) then begin
    new.prev = substring(new.prev from 1 for position('=' in new.prev)-1);
  end
end^
SET TERM ; ^
