--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSCOMPANY_BD_CHECKLINKS FOR EPERSCOMPANY                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists (select first 1 1 from employees where person = old.person and company = old.company)) then
    exception universal 'Kartoteka osobowa powiązana z kartoteką pracowniczą - usunięcie niemożliwe!';
end^
SET TERM ; ^
