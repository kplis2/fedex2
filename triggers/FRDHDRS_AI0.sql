--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDHDRS_AI0 FOR FRDHDRS                        
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  insert into frdhdrsvers (frdhdr, shortname, number, main, opis)
    values(new.ref, new.shortname, 0, 1, new.frhdr);
end^
SET TERM ; ^
