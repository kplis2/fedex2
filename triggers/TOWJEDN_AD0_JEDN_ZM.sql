--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_AD0_JEDN_ZM FOR TOWJEDN                        
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable wref integer;
declare variable mag varchar(3);
begin
  if(old.dodatkowa <> 1 or old.dodatkowa <> 2) then
    begin
      if (old.dodatkowa = 1) then
        update stanyil s
          set s.iloscjm1 = null,
              s.miara1 = null
          where s.ktm = old.ktm;
      if (old.dodatkowa = 2) then
        update stanyil s
          set s.iloscjm2 = null,
              s.miara2 = null
         where s.ktm = old.ktm;
    end
end^
SET TERM ; ^
