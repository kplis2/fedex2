--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSTEPS_AU_LASR FOR NOTIFSTEPS                     
  ACTIVE AFTER UPDATE POSITION 30 
as
begin
  if ((coalesce(old.notifstep,0) <> coalesce(new.notifstep,0)
      or old.number <> new.number)
    and new.notifstep is not null)
  then
    if (exists(select first 1 1 from notifsteps n where n.notification = new.notification and n.number > new.number)) then
      exception universal 'Reguła zależna musi być wykonana wcześniej';
end^
SET TERM ; ^
