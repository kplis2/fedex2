--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDCELLS_BU0 FOR FRDCELLS                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.planamount <> old.planamount) then
    new.timeofchange = current_timestamp(0);
  if (new.planamount <> old.planamount or new.realamount <> old.realamount) then begin
    new.deviation = new.realamount - new.planamount;
    if(new.planamount = 0) then new.percplanreal = 0;
    else new.percplanreal = 100 * new.realamount / new.planamount;
  end
end^
SET TERM ; ^
