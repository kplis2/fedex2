--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_BU_PR FOR NAGZAM                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable prsheetprdepart varchar(20);
declare variable techjednquantity numeric(14,4);
declare variable batchquantity numeric(14,4);
begin
--wyliczanie deklarowanego czasu zakończenia zlecenia
  if((new.prsheet is not null and (old.prsheet is null or new.prsheet <> old.prsheet))
     or new.kilosc <> old.kilosc) then begin
    select techjednquantity, batchquantity from prsheets where ref = new.prsheet into :techjednquantity, :batchquantity;
    if(batchquantity > 0) then new.techjednquantity = new.kilosc * :techjednquantity / :batchquantity;
  end
--weryfikacja zgodnosci wydzialu na naglowku zamowienia i karty techn.
  if(new.prsheet is not null) then begin
    select prdepart from prsheets where ref = new.prsheet into :prsheetprdepart;
    if(:prsheetprdepart <> new.prdepart) then
      exception prnagzam_error substring('Wydział karty techn.('||:prsheetprdepart||') niezgodny z wydziałem zlecenia('||new.prdepart||')' from 1 for 75);
  end
end^
SET TERM ; ^
