--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODBIORCY_AU0 FOR ODBIORCY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.gl = 1 and old.gl = 0) or
   (new.gl = 1 and ((new.dulica <> old.dulica) or
                    (new.dmiasto <> old.dmiasto) or
                    (new.dpoczta <> old.dpoczta) or
                    (new.dkodp <> old.dkodp) or
                    (new.nazwafirmy <> old.nazwafirmy) or
                    (coalesce(new.grupakup,0)<>coalesce(old.grupakup,0))
                   )
   )) then begin
    update ODBIORCY set GL = 0 where GL = 1 and KLIENT = new.klient and ref <> new.ref;
    update KLIENCI set DNAZWA = new.nazwafirmy, Dmiasto = new.dmiasto, dulica = new.dulica, dkodp = new.dkodp, dpoczta = new.dpoczta, grupakup = new.grupakup
    where REF=new.klient;
  end
  if((new.nazwafirmy <> old.nazwafirmy) or (new.nazwa <> old.nazwa)) then
    update ODBIORCY set odbiorcaspedycdesc = new.nazwafirmy||' ' ||new.nazwa where ODBIORCASPEDYC = new.ref;
end^
SET TERM ; ^
