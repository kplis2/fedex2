--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_ATRYBUTY_BI FOR X_ATRYBUTY                     
  ACTIVE BEFORE INSERT POSITION 10 
AS
begin
  if (coalesce(trim(new.otable),'') = '' or coalesce(new.oref,0) = 0) then
    exception universal 'Podaj OTABLE i OREF.';
  /*JO nie wrzucamy do DEFCECHY
  if (coalesce(trim(new.cecha),'') = '') then
    exception universal 'Podaj ceche.';
  if (not exists (select first 1 1 from defcechy where symbol = new.cecha)) then --teoretycznie nie musi byc tego exceptiona, bo to tabela x-owa
    exception universal 'Cechy '||new.cecha||' nie ma w definicjach.';
  */
  if (new.regdate is null) then
    new.regdate = current_timestamp(0);
  if (new.regoper is null) then
    execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoper;

  if (new.otable = 'WERSJE') then
    select ktm from wersje where ref = new.oref into new.ktm;
end^
SET TERM ; ^
