--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPPLANPOS_BU_CHKSTATUS FOR BHPPLANPOS                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable excmsg varchar(100);
begin
--MWr: Kontrola zmiany statusu. Jezeli anulujemy utracenie status = -2.

  if (new.status <> old.status) then
  begin
    if (new.status = 0 and old.status <> 2) then excmsg = 'Nie można anulować wydania artykułu z innym statusem niż ''wydany''.';
    else if (new.status = 2 and old.status <> 0) then excmsg = 'Nie można wydać artykułu z innym statusem niż ''niewydany''.';
    else if (new.status = 3 and old.status <> 2) then excmsg = 'Nie można oznaczyć utraty dla niewydanego artykułu.';
    else if (new.status = -2 and old.status <> 3) then excmsg = 'Nie można anulować utraty dla nieutraconego artykułu.';

    if (coalesce(excmsg,'') <> '') then
      exception universal :excmsg;
    else
      new.status = abs(new.status);
  end
  /*sprawdzenie czy (new.status = old.status) zrealizowane zostalo w kodzie
    zrodlowym gdyz wykonaloby sie tez przy zatwierdzeniu pustego opisu. */
end^
SET TERM ; ^
