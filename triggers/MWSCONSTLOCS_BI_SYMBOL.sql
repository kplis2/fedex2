--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCS_BI_SYMBOL FOR MWSCONSTLOCS                   
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable symbol varchar(20);
declare variable longsymbol varchar(20);
declare variable cnt integer;
declare variable tmp varchar(3);
begin
  -- Domyslny Symbol lokacji w Ehrle wg maski SS-RR-KK-LL
  -- gdzie
  -- SS - symbol sektoru
  -- RR - numer rzdu
  -- KK - numer kolejnej palety
  -- LL - Poziom
  if (coalesce(new.symbol,'') = '') then begin
  if (new.mwsstandseg is not null) then
    if (new.wh is not null and new.whsecdict is not null and new.whsecrowdict is not null
        and new.mwsstandnumber is not null and new.locnumber is not null
        and new.mwsstandsegtypedict is not null and new.whareanumber is not null
        and new.mwsstandlevelnumber is not null
    ) then
    begin
      select symbol, longsymbol from whareas where ref = new.wharea
        into symbol, longsymbol;
      new.longsymbol = longsymbol||'-'||new.mwsstandlevelnumber;

--PKo numer lokacji zawsze dwucyfrowy
      cnt = new.mwsstandlevelnumber;
      if (cnt < 10) then tmp = '0'||cnt;
      else tmp =cast(cnt as varchar(3));
      new.symbol = symbol||'-'||tmp;
--PKo koniec

    end
  else
    if (new.wh is not null and new.whsecdict is not null and new.whsecrowdict is not null
        and new.whareanumber is not null
    ) then
    begin
      select symbol, longsymbol from whareas where ref = new.wharea
        into symbol, longsymbol;
      new.longsymbol = longsymbol||'-'||'0';
      new.symbol =  symbol||'-'||'0';
    end
  end
end^
SET TERM ; ^
