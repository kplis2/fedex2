--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCERTIFS_BU_CRTNOCRT FOR EWORKCERTIFS                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (exists(select c.ref
    from ewrkcrtnocontr c
    where c.workcertif = new.ref
      and (c.todate > new.todate or c.fromdate < new.fromdate))) then
  exception okres_nieskladkowy;
end^
SET TERM ; ^
