--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EAPPLICATIONS_BU_CHANGES FOR EAPPLICATIONS                  
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
--MWr: Ustawienie znacznika czasowego i operatora przy zmianie statusu

  if (new.status <> old.status) then
  begin
    execute procedure get_global_param ('AKTUOPERATOR')
      returning_values new.chgoperator;
    new.chgtimestamp = current_timestamp(0);
  end
end^
SET TERM ; ^
