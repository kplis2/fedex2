--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOT_AD0 FOR DOKUMNOT                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable NUMBERG varchar(40);
declare variable numpoakcept smallint;
declare variable local smallint;
declare variable BLOCKREF integer;
begin
  if(old.numer <> 0) then begin
    select NUMBER_NOTGEN, NUMPOAKCEPT from DEFMAGAZ where  symbol = old.magazyn into :numberg, :numpoakcept;
    if(:numpoakcept is null) then numpoakcept = 0;
    execute procedure CHECK_LOCAL('DOKUMNOT',old.ref) returning_values :local;
    if(:numberg is not null and :local = 1) then begin
      if (:numpoakcept = 0 or old.akcept <> 0) then begin
        execute procedure free_number(:numberg, old.magazyn, old.typ, old.data, old.numer, 0) returning_values :blockref;
      end
    end
  end
end^
SET TERM ; ^
