--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_AD0 FOR ZADANIA                        
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if(old.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYZAD(old.ckontrakt);
  if(old.wfworkflow is not null) then execute procedure wfworkflow_status_set(old.wfworkflow);

  delete from dokzlacz d where d.ztable = 'ZADANIA' and d.zdokum = old.ref;

end^
SET TERM ; ^
