--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_BIU0 FOR WHSECROWS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.nogoodassoc is null) then new.nogoodassoc = 0;
  if (new.symbol is null or new.symbol = '') then
    exception WHSECROWS_SYMBOL_NOT_DEF;
  if (new.coordyb is null) then
    select coordyb from whsecs where ref = new.whsec into new.coordyb;
  if (new.penalty is null) then new.penalty = 0;
end^
SET TERM ; ^
