--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_WHSECROWS_BU0_TYPE FOR WHSECROWS                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.x_type is distinct from old.x_type) then begin
    if (old.x_type is not null
        and (exists(select first 1 1 from whareas a where a.x_row_type = old.x_type and a.whsecrow = new.ref)
              or exists(select first 1 1 from mwsconstlocs c where c.x_row_type = old.x_type and c.whsecrow = new.ref))) then
        exception universal 'Regał ma już założone lokacje. Zmiana typu nie możliwa';
    else if (new.x_type is null) then
      new.x_type = 0;

  end
end^
SET TERM ; ^
