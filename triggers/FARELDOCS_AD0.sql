--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FARELDOCS_AD0 FOR FARELDOCS                      
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  update fxdassets set oddzial = old.obranch, department = old.odepartment,
    comparment = old.ocompartment, person = old.operson,
    tbksymbol = old.otbksymbol, fbksymbol = old.ofbksymbol
    where ref = old.fxdasset;
end^
SET TERM ; ^
