--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BIU_INNEKS FOR RKDOKPOZ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
  declare variable konto varchar(20);
begin
  select rkstnkas.konto
    from rkdoknag
      join rkstnkas on (rkdoknag.stanowisko=rkstnkas.kod)
    where rkdoknag.ref=new.dokument
    into :konto;
  if(:konto is null) then konto = '';
  if(new.konto2 is null or new.konto2 = '') then
    new.inneks = 0;
  else
    if(new.konto2 <> :konto) then
      new.inneks = 1;
    else 
      new.inneks = 0;
end^
SET TERM ; ^
