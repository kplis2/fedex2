--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTORZY_BU1 FOR SPEDYTORZY                     
  ACTIVE BEFORE UPDATE POSITION 1 
as
begin
  if(coalesce(new.oddzial,'')<>coalesce(old.oddzial,'') and coalesce(new.oddzial,'')='') then
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
end^
SET TERM ; ^
