--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_LISTYWYSD_AU_INTEGRACJA FOR LISTYWYSD                      
  ACTIVE AFTER UPDATE POSITION 101 
as
    declare variable int_id string120;
    declare variable ilpot quantity_mws;
    declare variable tmp_sposdost sposdost_id;
begin
  --trigger sluzacy do wysylania do wapro informacji po spakowaniu listu spedycyjnego
  if (new.akcept in (1,2) and old.akcept =0 ) then
  begin
    for 
        select dp.int_id, sum(lr.iloscmag), dg.sposdost
            from dokumpoz dp
                join dokumnag dg on (dg.ref = dp.dokument)
                join listywysdpoz lp on (lp.dokpoz = dp.ref)
                join listywysdroz lr on (lr.listywysdpoz = lp.ref)
            where dg.grupasped = new.refdok
            group by dp.int_id, dg.sposdost
        into :int_id, :ilpot , :tmp_sposdost do
    begin
        --wyslanie do wapro informacji o spakowanej ilosci na pozycji
         update x_imp_dokumpoz xp
            set xp.status = 13,
                xp.iloscpotw = coalesce(:ilpot,0)
            where xp.pozid = :int_id;
    end
    --KPI
      if (:tmp_sposdost = 1 ) then
      begin
        insert into listywysdroz_opk ( LISTWYSD ) values ( new.ref);
      end
    ---eKPI
    --po zakonczeniu pakowania insert informacji o paczkach do tabeli eksportowej
    --dla kazdej wz wysylamy caly komplet paczek z listu spedycyjnego ustalenia z uruchomienia 29052019 MKD
      insert into X_IMP_PACZKI (NAGID, SYMBOL, NRLISTU,
                                NRPACZKI, DATAGEN, STATUS, DATAPRZET, REF_SENTE)
        select IIF(dn.int_id is not distinct from '', NULL, dn.int_id), --jezeli int_id jest pustym stringiem to zamieniam na null
          dn.int_symbol,
          IIF(:tmp_sposdost = 1, 'odbior wlasny',ld.symbolsped),  -- jezeli sposdost = 2 (odbior wlasny) to numer listu ustawiam na osbior wlasny
          opk.kodkresk, ld.dataakc, 10, current_timestamp, opk.ref
      from listywysdroz_opk opk
        join listywysd ld on ld.ref = opk.listwysd
        join dokumnag dn on dn.grupasped = ld.x_grupasped
        where opk.listwysd = new.ref;
  end
end^
SET TERM ; ^
