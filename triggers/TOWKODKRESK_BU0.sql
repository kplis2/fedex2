--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWKODKRESK_BU0 FOR TOWKODKRESK                    
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select ktm from WERSJE where ref=new.wersjaref into new.ktm;
  else if(new.ktm is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = 0 into new.wersjaref;
  if(new.towjednref is null or (new.towjednref = 0)) then
     select REF from TOWJEDN where KTM = new.ktm and towjedn.glowna > 0 into new.towjednref;
  /*blokada odznaczenia glownego bez powodu - czyli jak nie ma innego glownego*/
  if(new.kodkresk = '') then exception KODKRESK_EMPTY;
  if(new.gl = 0 and old.gl = 1)then begin
    select count(*) from TOWKODKRESK where wersjaref = new.wersjaref and gl = 1 and ref<>new.ref into :cnt;
    if(:cnt = 0 or (:cnt is null))then
      new.gl = 1;
  end
end^
SET TERM ; ^
