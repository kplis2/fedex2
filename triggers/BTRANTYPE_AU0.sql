--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANTYPE_AU0 FOR BTRANTYPE                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if(new.rights <> old.rights or new.rightsgrup <> old.rightsgrup) then
    update BTRANSFERS set RIGHTS = new.rights, RIGHTSGRUP = new.rightsgrup where BTRANSFERS.btype = new.symbol;
end^
SET TERM ; ^
