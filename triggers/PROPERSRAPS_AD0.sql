--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_AD0 FOR PROPERSRAPS                    
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable amount numeric(14,4);
declare variable amountcur numeric(14,4);
declare variable docref integer;
declare variable amountresultcalctype smallint;
begin
  if(old.amount <> 0) then begin
    amount = 0;
    select prrs.amountresultcalctype
      from  prschedopers prps
        left join prshopers prrs on (prps.shoper = prrs.ref)
      where prps.ref = old.prschedoper
      into :amountresultcalctype;
    for select sum(por.amount)
      from propersraps por
        left join econtractsposdef e on (por.econtractsposdef = e.ref)
      where por.prschedoper = old.prschedoper and por.ref <> old.ref
        and (e.ref is null or e.ref is not null and e.calcamountresult = 1)
      group by por.econtractsdef, por.econtractsposdef
      into :amountcur
    do begin
      --dla MAX
      if (:amountresultcalctype = 0) then
        if(amountcur > amount) then amount = amountcur;
      --dla MIN
      else if (:amountresultcalctype = 1) then
        if(amountcur < amount) then amount = amountcur;
      --dla SUMA
      else if (:amountresultcalctype = 2) then
        amount = amount + amountcur;
    end
    update prschedopers pso set pso.amountresult = :amount where pso.ref = old.prschedoper;
  end
  if (old.docid is not null) then
  begin
    select d.ref
      from dokumnag d
      where d.ref2 = old.docid
      into :docref;

    if (:docref is not null and :docref > 0) then
    begin
      update dokumnag d set d.akcept = 0 where ref = :docref;
      delete from dokumnag where ref = :docref;
    end
    docref = null;

    select d.ref2
      from dokumnag d
      where d.ref = old.docid
      into :docref;

    if (:docref is not null and :docref > 0) then
    begin
      update dokumnag d set d.akcept = 0 where ref = :docref;
      delete from dokumnag where ref = :docref;
    end

    update dokumnag d set d.akcept = 0 where ref = old.docid;
    delete from dokumnag where ref = old.docid;
  end
  if(not exists(select ref from propersraps where prschedoper = old.prschedoper and ref <> old.ref))
    then execute procedure PRSCHEDOPERS_STATUSCHECK(null, null, old.prschedoper);
end^
SET TERM ; ^
