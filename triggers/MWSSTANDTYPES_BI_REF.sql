--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDTYPES_BI_REF FOR MWSSTANDTYPES                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSSTANDTYPES') returning_values new.ref;
end^
SET TERM ; ^
