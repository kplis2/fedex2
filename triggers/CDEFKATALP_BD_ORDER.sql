--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDEFKATALP_BD_ORDER FOR CDEFKATALP                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update cdefkatalp set ord = 0, numer = numer - 1
    where akronim <> old.akronim and numer > old.numer and cdefkatal = old.cdefkatal;
end^
SET TERM ; ^
