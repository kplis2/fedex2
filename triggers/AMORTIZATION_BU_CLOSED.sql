--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMORTIZATION_BU_CLOSED FOR AMORTIZATION                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable period_status integer;
  declare variable company integer;
begin
  select company
      from fxdassets f
      where f.ref = new.fxdasset
  into :company;

  select status from amperiods where
  ref = old.amperiod
  and company = :company
  into :period_status;
  if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można naliczyć amortyzacji dla zamkniętego okresu!';

  select status from amperiods where
  ref = new.amperiod
  and company = :company
  into :period_status;
  if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można naliczyć amortyzacji dla zamkniętego okresu!';
end^
SET TERM ; ^
