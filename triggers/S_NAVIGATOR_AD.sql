--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_NAVIGATOR_AD FOR S_NAVIGATOR                    
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable prefix varchar(255);
declare variable section varchar(255);
declare variable ref varchar(255);
declare variable prev varchar(255);
declare variable number integer;
declare variable changedfields varchar(1024);
begin
  if(old.initializing=1) then exit;
  if(old.childrencount>0) then exception UNIVERSAL 'Usuń najpierw elementy podrzedne!';
  if(old.parent is not null) then
    if(not exists(select first 1 1 from s_navigator s where s.parent=old.parent and s.ref<>old.ref)) then
      update s_navigator set childrencount = 0 where ref = old.parent;

  prefix = 'Navigator:';
  section = :prefix || old.ref;
  -- usun parametry w ramach okreslonego namespace
  delete from s_appini s where s.section=:section and s.namespace=old.namespace;

  -- przesortuj elementy
  execute procedure SYS_ORDER_APPINI(old.namespace,old.parent,:prefix,old.ref,old.prev,NULL);
  -- pobierz zmienione numery
  update s_navigator set INITIALIZING=1 where coalesce(PARENT,'')=coalesce(old.parent,'');
  for select REF,PREV,NUMBER,CHANGEDFIELDS from sys_get_navigator(old.namespace, NULL, 'REF,PREV,PARENT,NUMBER,CHANGEDFIELDS')
    where coalesce(PARENT,'')=coalesce(old.parent,'')
    into :ref,:prev,:number,:changedfields
  do begin
    update s_navigator set NUMBER=:number, PREV=:prev, CHANGEDFIELDS=:changedfields where ref=:ref and (NUMBER<>:number or PREV<>:prev or CHANGEDFIELDS<>:changedfields);
  end
  update s_navigator set INITIALIZING=0 where INITIALIZING=1;

end^
SET TERM ; ^
