--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDOKWIT_BU0 FOR DEFDOKWIT                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.forma <> old.forma or
     new.id <> old.id or
     new.tytul <> old.tytul)
  then new.state = 1;
end^
SET TERM ; ^
