--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWKODKRESK_AI0 FOR TOWKODKRESK                    
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if(new.gl = 1) then begin
    update TOWKODKRESK set GL = 0 where WERSJAREF = new.wersjaref and GL = 1 and ref<>new.ref;
    update WERSJE set KODKRESK = new.kodkresk where ref=new.wersjaref and ((kodkresk <> new.kodkresk) or (kodkresk is null));
  end
end^
SET TERM ; ^
