--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATAL_BU_REPLICAT FOR CKATAL                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or (new.cdefkatal <> old.cdefkatal ) or (new.cdefkatal is not null and old.cdefkatal is null) or (new.cdefkatal is null and old.cdefkatal is not null)
      or (new.numer <> old.numer ) or (new.numer is not null and old.numer is null) or (new.numer is null and old.numer is not null)
      or (new.nazwa <> old.nazwa ) or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null)
      or (new.dokref <> old.dokref ) or (new.dokref is not null and old.dokref is null) or (new.dokref is null and old.dokref is not null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('CKATAL',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
