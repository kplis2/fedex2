--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_BU_REPLICAT FOR SLOPOZ                         
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
  declare variable kart smallint;
begin
  waschange = 0;
  select kartoteka from SLODEF where ref= new.slownik into :kart;
  if((new.slownik <> old.slownik)
   or (new.kod <> old.kod) or (new.kod is not null and old.kod is null ) or (new.kod is null and old.kod is not null)
   or (new.nazwa <> old.nazwa ) or (new.nazwa is not null and old.nazwa is null ) or (new.nazwa is null and old.nazwa is not null)
   or (new.kontoks <> old.kontoks ) or (new.kontoks is not null and old.kontoks is null ) or (new.kontoks is null and old.kontoks is not null)
   or (new.opis <> old.opis ) or (new.opis is not null and old.opis is null ) or (new.opis is null and old.opis is not null)
  ) then
   waschange = 1;

  execute procedure rp_trigger_bu('SLOPOZ',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
