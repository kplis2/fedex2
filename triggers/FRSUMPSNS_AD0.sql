--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRSUMPSNS_AD0 FOR FRSUMPSNS                      
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if (old.sumpsn <> old.frpsn and old.frversion = 0) then
  begin
      update frpsns set countord = 0 where ref = old.frpsn and algorithm = 2;
  end
end^
SET TERM ; ^
