--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_BI_REF FOR SLOPOZ                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SLOPOZ')
      returning_values new.REF;
end^
SET TERM ; ^
