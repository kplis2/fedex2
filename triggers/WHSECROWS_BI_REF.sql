--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_BI_REF FOR WHSECROWS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('WHSECROWS') returning_values new.ref;
end^
SET TERM ; ^
