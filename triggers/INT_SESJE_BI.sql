--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_SESJE_BI FOR INT_SESJE                      
  ACTIVE BEFORE INSERT POSITION 5 
as
begin
  if (new.datains is null) then
    new.datains = current_timestamp(0);

  if (new.kierunek is null) then
    new.kierunek = 0;
end^
SET TERM ; ^
