--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERTY_AD_WERSJA FOR OFERTY                         
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if (coalesce(old.orgoferta,0) <> 0) then
    update oferty o set o.popoferta = old.popoferta where o.popoferta = old.ref;
  else if (exists (select ref from oferty where orgoferta = old.ref)) then
    exception OFERTY_ORGDELETE;
end^
SET TERM ; ^
