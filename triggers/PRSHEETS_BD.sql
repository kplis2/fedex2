--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_BD FOR PRSHEETS                       
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if(old.status <> 0) then
    exception UNIVERSAL 'Możliwe usuwanie tylko karty niezatwierdzonej !';
  update prshopers set opermaster = null where prshopers.sheet = old.ref;
  delete from prshopers where prshopers.sheet = old.ref;
  delete from prshmat where sheet = old.ref;
  delete from prshtools where sheet = old.ref;
end^
SET TERM ; ^
