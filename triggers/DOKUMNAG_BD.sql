--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BD FOR DOKUMNAG                       
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (exists (select ref from propersraps where docid = old.ref)) then
    exception PREVIOUS_OPERS_RQUIRED 'Dokument powiązany z raportem produkcyjnym';
end^
SET TERM ; ^
