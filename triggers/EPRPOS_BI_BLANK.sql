--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_BI_BLANK FOR EPRPOS                         
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
/*Trigger blankujacy; Laczy wczesniej istniejace triggery EPRPOS_BI_OPERATOR
  oraz EPRPOS_BI0_SET_EPREMPL (PR31748)*/

  new.cauto = coalesce(new.cauto,0);
  new.regtimestamp = current_timestamp(0);

  execute procedure get_global_param ('AKTUOPERATOR')
    returning_values new.regoperator;

  if (new.eprempl is null) then
    select ref from eprempl
      where employee = new.employee and epayroll = new.payroll
      into new.eprempl;
end^
SET TERM ; ^
