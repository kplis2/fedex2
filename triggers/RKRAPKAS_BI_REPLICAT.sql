--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_BI_REPLICAT FOR RKRAPKAS                       
  ACTIVE BEFORE INSERT POSITION 2 
AS
begin
 execute procedure rp_trigger_bi('RKRAPKAS',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
