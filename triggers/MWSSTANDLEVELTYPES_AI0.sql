--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDLEVELTYPES_AI0 FOR MWSSTANDLEVELTYPES             
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable maxcap numeric(14,2);
declare variable maxvol numeric(14,2);
begin
  select sum(loadcapacity), sum(volume)
    from mwsstandleveltypes where mwsstandtype = new.mwsstandtype
    into maxcap, maxvol;
  update mwsstandtypes set loadcapacity = :maxcap, volume = :maxvol
    where ref = new.mwsstandtype;
end^
SET TERM ; ^
