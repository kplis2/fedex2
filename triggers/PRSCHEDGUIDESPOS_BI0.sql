--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESPOS_BI0 FOR PRSCHEDGUIDESPOS               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.amount is null) then new.amount = 0;
  if(new.amountzreal is null) then new.amountzreal = 0;
  if(new.amountzrealakc is null) then new.amountzrealakc = 0;
  if(new.autodocout is null) then new.autodocout = 0;
  if(new.activ is null) then new.activ = 0;
  select stan
    from prschedguides
    where ref = new.prschedguide
  into new.stan;
  if(new.stan is null or (new.stan = '') or (new.stan = ' ')) then new.stan = 'N';
  if(new.number is null) then new.number = 1;
  if(new.amountshortage is null) then new.amountshortage = 0;
  if(new.pggamountcalc is null) then new.pggamountcalc = 0;
  if(new.amountoverlimit is null) then new.amountoverlimit = 0;
--  if(new.prschedoper is null) then exception prschedguides_error 'Brak dowiązania operacji harmonogramu do pozycji przewodnika. ';
  if(new.kamountnorm is null or new.kamountnorm = 0) then begin
    select (case when p.ilosc > 0 and n.kilosc > 0 then p.ilosc / n.kilosc else 1 end)
      from pozzam p
      left join nagzam n on (n.ref = p.zamowienie)
      where p.ref = new.pozzamref
    into new.kamountnorm;
    select amount * new.kamountshortage * new.kamountnorm
      from prschedguides
      where ref = new.prschedguide
    into new.amount;
  end
  if(new.kamountshortage is null or new.kamountshortage = 0) then
    exception prschedguides_error 'Brak określenia współczynnika braków na pozycji przewodnika';
  else if(new.kamountnorm is null or new.kamountnorm = 0) then
    exception prschedguides_error 'Brak określenia współczynnika normatywnego na pozycji przewodnika';

  if (new.prschedopernumber is null) then
    select p.number
      from prschedopers p
      where p.ref = new.prschedoper
      into new.prschedopernumber;
  if (new.wersjaref is not null) then begin
    select w.ktm, w.nrwersji
      from wersje w
      where w.ref = new.wersjaref
      into new.ktm, new.wersja;
  end else if (new.ktm is not null and new.wersja is not null) then begin
    select w.ref
      from wersje w
      where w.ktm = new.ktm
        and w.nrwersji = new.wersja
      into new.wersjaref;
  end
  if (new.wersjaref is null) then
    exception prschedguides_error 'Niepoprawnie uzupelniony KTM i wersja.';
end^
SET TERM ; ^
