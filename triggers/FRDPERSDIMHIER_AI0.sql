--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMHIER_AI0 FOR FRDPERSDIMHIER                 
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  insert into frdpersdimvals (frdpersdimshier, frpsn)
    select new.ref, frpsns.ref
      from frdimhier
        join frhdrs on (frhdrs.symbol = frdimhier.frhdr)
        join frpsns on (frpsns.frhdr = frhdrs.symbol)
      where frdimhier.ref = new.frdimshier;
end^
SET TERM ; ^
