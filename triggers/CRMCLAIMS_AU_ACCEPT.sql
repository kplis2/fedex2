--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMCLAIMS_AU_ACCEPT FOR CRMCLAIMS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable wartosc varchar(255);
declare variable sql varchar(255);
begin
select wartosc from konfig where akronim = 'PUNKPROC' into :wartosc;
  if (new.accept = 1) then begin
     sql = 'execute procedure '|| wartosc || '(' || new.ref || ', 1)';
     execute statement :sql;
  end
  if (new.accept = 0) then begin
  sql = 'execute procedure '|| wartosc || '(' || new.ref || ', 0)';
  execute statement :sql;
  end
end^
SET TERM ; ^
