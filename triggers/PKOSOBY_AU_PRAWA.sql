--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_AU_PRAWA FOR PKOSOBY                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(
    (new.nazwa <> old.nazwa) or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
    (new.telefon <> old.telefon) or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null)
  ) then begin
    update KONTAKTY set PKOSOBYNAZWA = new.NAZWA, PKOSOBYTELEFON = new.TELEFON
    where PKOSOBA = new.REF;
    update ZADANIA set PKOSOBYNAZWA = new.NAZWA, PKOSOBYTELEFON = new.TELEFON
    where OSOBA = new.REF;
    update CKONTRAKTY set PKOSOBYNAZWA = new.NAZWA
    where OSOBA = new.REF;
    update CNOTATKI set PKOSOBYNAZWA = new.NAZWA, PKOSOBYTELEFON = new.TELEFON
    where PKOSOBA = new.REF;
  end
end^
SET TERM ; ^
