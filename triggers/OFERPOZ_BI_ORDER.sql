--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERPOZ_BI_ORDER FOR OFERPOZ                        
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 1 and new.numer is not null) then begin
    new.ord = 0;
    exit;
  end
  select max(numer) from oferpoz where oferta = new.oferta
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update oferpoz set ord = 1, numer = numer + 1
      where numer >= new.numer and oferta = new.oferta;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
