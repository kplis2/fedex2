--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODDZIAL_BU_REPLICAT FOR ODDZIALY                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.ODDZIAL<>old.ODDZIAL
      or new.NAZWA<>old.NAZWA or (new.NAZWA is null and old.NAZWA is not null) or (new.NAZWA is not null and old.NAZWA is null)      
      or new.MAGAZYN<>old.MAGAZYN or (new.MAGAZYN is null and old.MAGAZYN is not null) or (new.MAGAZYN is not null and old.MAGAZYN is null)      
      or new.MAG2<>old.MAG2 or (new.MAG2 is null and old.MAG2 is not null) or (new.MAG2 is not null and old.MAG2 is null)      
      or new.REJFAKSPR<>old.REJFAKSPR or (new.REJFAKSPR is null and old.REJFAKSPR is not null) or (new.REJFAKSPR is not null and old.REJFAKSPR is null)      
      or new.REJFAKZAK<>old.REJFAKZAK or (new.REJFAKZAK is null and old.REJFAKZAK is not null) or (new.REJFAKZAK is not null and old.REJFAKZAK is null)      
      or new.SYMBOL<>old.SYMBOL or (new.SYMBOL is null and old.SYMBOL is not null) or (new.SYMBOL is not null and old.SYMBOL is null)
      or new.NUMER<>old.NUMER or (new.NUMER is null and old.NUMER is not null) or (new.NUMER is not null and old.NUMER is null)      
      or new.LOCATION<>old.LOCATION or (new.LOCATION is null and old.LOCATION is not null) or (new.LOCATION is not null and old.LOCATION is null)      
      or new.LOCATIONBIT<>old.LOCATIONBIT or (new.LOCATIONBIT is null and old.LOCATIONBIT is not null) or (new.LOCATIONBIT is not null and old.LOCATIONBIT is null)      
      or new.CITY<>old.CITY or (new.CITY is null and old.CITY is not null) or (new.CITY is not null and old.CITY is null)      
      or new.STREET<>old.STREET or (new.STREET is null and old.STREET is not null) or (new.STREET is not null and old.STREET is null)      
      or new.PHONE<>old.PHONE or (new.PHONE is null and old.PHONE is not null) or (new.PHONE is not null and old.PHONE is null)      
      or new.EMAIL<>old.EMAIL or (new.EMAIL is null and old.EMAIL is not null) or (new.EMAIL is not null and old.EMAIL is null)      
      or new.NUMBER<>old.NUMBER or (new.NUMBER is null and old.NUMBER is not null) or (new.NUMBER is not null and old.NUMBER is null)      
      or new.ONWWW<>old.ONWWW or (new.ONWWW is null and old.ONWWW is not null) or (new.ONWWW is not null and old.ONWWW is null)      
      or new.CPWOJ16M<>old.CPWOJ16M or (new.CPWOJ16M is null and old.CPWOJ16M is not null) or (new.CPWOJ16M is not null and old.CPWOJ16M is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('ODDZIALY',old.oddzial, null, null, null, null, old.token, old.state,
        new.oddzial, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
