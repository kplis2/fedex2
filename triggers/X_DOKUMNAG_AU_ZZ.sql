--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMNAG_AU_ZZ FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 130 
as
  declare variable refmwsord mwsconstlocs_id;
begin
  -- MKD ZG 133728
  if (new.akcept = 1 and old.akcept <> 1 and new.typ = 'ZZ') then
  begin
    execute procedure x_mws_akcept_and_realize_cor( new.ref )
      returning_values REFMWSORD;
  end
end^
SET TERM ; ^
