--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMROZ_BU0 FOR DOKUMROZ                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable bn varchar(1);
declare variable staw numeric(14,4);
declare variable cnt integer;
declare variable korref integer;
begin
  if(new.blokoblnag is null) then new.blokoblnag = 0;
  if(new.ack<>7 and new.ilosc <> old.ilosc) then new.iloscl = new.ilosc;
  new.wartosc = new.cena * new.ilosc;
  new.wartoscl = new.cena * new.iloscl;
  new.wart_koszt = new.cena_koszt * new.ilosc;
  if(old.ack = 0) then begin
     new.pcena = new.cena;
     new.pwartosc = new.wartosc;
  end else if (new.pcena is null) then begin
    select min(REF) from dokumnotp where DOKUMROZKOR = new.ref into :korref;
    if(:korref > 0) then
      select dokumnotp.cenaold from DOKUMNOTP where ref = :korref into new.pcena;
      if(new.pcena is null) then
        new.pcena = new.cena;
    new.pwartosc = new.ilosc * new.pcena;
  end
  if(new.cenasnetto > 0 or (new.cenasbrutto > 0)) then begin
    /*policzenie wartosci*/
    select DEFDOKUM.KAUCJABN
    from DOKUMPOZ
    left join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
    left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
    where DOKUMPOZ.ref = new.pozycja
    into :bn;
    select VAT.STAWKA
    from DOKUMPOZ
    left join TOWARY on (TOWARY.KTM = DOKUMPOZ.KTM)
    left join VAT on (TOWARY.VAT = VAT.GRUPA)
    where DOKUMPOZ.REF = new.pozycja
    into :staw;
    if(:bn = 'B') then begin
       new.wartsbrutto = new.ilosc * new.cenasbrutto;
       new.wartsnetto = new.wartsbrutto/(1+:staw/100);
    end else begin
       new.wartsnetto = new.ilosc * new.cenasnetto;
       new.wartsbrutto = new.wartsnetto*(1+:staw/100);
    end
  end
  if(old.ack = 1 and new.ack = 0)then begin
    /*sprawdzenie, czy ma jakies powiazane noty korygujace*/
    select count(*) from DOKUMNOTP where DOKUMROZKOR = old.ref into :cnt;
    if(:cnt > 0) then
      exception DOKUMROZ_HASNOTKOR;
  end
end^
SET TERM ; ^
