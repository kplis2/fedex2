--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZSAD_BU_ORDER FOR POZSAD                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (NEW.ord is null) then new.ord = 1;
  if (new.numer is null) then new.numer = old.numer;
  if (new.ord = 0 or new.numer = old.numer) then
  begin
    new.ord = 1;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.numer <> old.numer) then
    select max(numer) from pozsad where faktura = new.faktura
      into :maxnum;
  else
    maxnum = 0;
  if (new.numer < old.numer) then
  begin
    update pozsad set ord = 0, numer = numer + 1
      where ref <> new.ref and numer >= new.numer
        and numer < old.numer and faktura = new.faktura;
  end else if (new.numer > old.numer and new.numer <= maxnum) then
  begin
    update pozsad set ord = 0, numer = numer - 1
      where ref <> new.ref and numer <= new.numer
        and numer > old.numer and faktura = new.faktura;
  end else if (new.numer > old.numer and new.numer > maxnum) then
    new.numer = old.numer;
end^
SET TERM ; ^
