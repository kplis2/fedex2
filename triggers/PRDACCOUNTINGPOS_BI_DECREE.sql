--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTINGPOS_BI_DECREE FOR PRDACCOUNTINGPOS               
  ACTIVE BEFORE INSERT POSITION 2 
AS
declare variable bkperiod varchar(6);
declare variable bkdocsymbol varchar(20);
declare variable prdversperiod varchar(6);
declare variable prdaccounting integer;
begin
 select p.fromperiod, p.prdaccounting from prdversions p where p.ref = new.prdversion into :prdversperiod, :prdaccounting;
 select first 1 b.symbol, b.period from decrees d join bkdocs b on (b.ref = d.bkdoc)
   where d.period>=:prdversperiod and d.prdaccounting = :prdaccounting and d.ref <> new.decree
 into :bkdocsymbol, :bkperiod;
 if(coalesce(:bkdocsymbol,'') <> '') then
   exception prd_deducted 'Istnieje dokument RMK ' || :bkdocsymbol || '(' || :bkperiod || '). Nie można dodać!';
end^
SET TERM ; ^
