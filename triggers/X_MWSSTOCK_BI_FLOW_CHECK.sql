--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSSTOCK_BI_FLOW_CHECK FOR MWSSTOCK                       
  INACTIVE BEFORE INSERT POSITION 5 
as
declare variable status smallint_id;
declare variable msg string255;
begin

  -- xxx KBI deaktywuje To raczej sprawdzamy na triggerach na masacts
  if (exists (select first 1 1 from mwsconstlocs c where c.ref = new.mwsconstloc and c.x_row_type=1)) then
  begin
    select STATUS, MSG
      from X_WMS_CHECK_LOT_AND_VERS_4_FLOW(new.mwsconstloc, new.vers , new.x_partia)
      into :STATUS, :MSG ;
    if (status=0) then exception universal :msg;
  end
end^
SET TERM ; ^
