--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_AU_OPERATOR FOR MWSORDS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.operator <> old.operator) then
    update mwsacts set operator = new.operator where mwsord = new.ref;
end^
SET TERM ; ^
