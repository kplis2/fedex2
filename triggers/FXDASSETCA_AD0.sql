--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETCA_AD0 FOR FXDASSETCA                     
  ACTIVE AFTER DELETE POSITION 0 
as
  declare variable status smallint;
begin
  select a.status
    from amperiods a
    where a.ref = old.amperiod
    into :status;
  if (status = 2) then
    exception fxdassetca_error 'Okres jest już zamknięty!';
end^
SET TERM ; ^
