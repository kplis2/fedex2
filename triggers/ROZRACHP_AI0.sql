--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_AI0 FOR ROZRACHP                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable zaprej varchar(255);
declare variable sprzedzestid integer;
begin
  execute procedure ROZRACH_OBL_NAG(new.kontofk, new.slodef, new.slopoz, new.symbfak, new.company);
  execute procedure GETCONFIG('ZAPREJ') returning_values :zaprej;
  if (zaprej = '1' and new.faktura is not null) then   begin
     execute statement 'execute procedure NAGFAK_OBL_ZAP(' || new.faktura || ')';
  end
  select SPRZEDZESTID from rozrach
    where company = new.company and slodef = new.slodef and slopoz = new.slopoz
      and kontofk = new.kontofk and symbfak = new.symbfak
    into :sprzedzestid;
  if (sprzedzestid > 0) then
    execute procedure SPRZEDZEST_OBL_ROZRACH(:sprzedzestid);
  if (coalesce(new.faktura, 0) <> 0) then
    update rozrach r
      set r.faktura = new.faktura
      where r.symbfak = new.symbfak
        and r.slodef = new.slodef
        and r.slopoz = new.slopoz
        and r.kontofk = new.kontofk
        and r.company = new.company
        and coalesce(r.faktura, 0) = 0;
end^
SET TERM ; ^
