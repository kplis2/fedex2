--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DK_EXPP_BI_ORDER FOR DK_EXPP                        
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from dk_expp where dkexp = new.dkexp and dkdok = new.dkdok
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update dk_expp set ord = 0, numer = numer + 1
      where numer >= new.numer and dkexp = new.dkexp and dkdok = new.dkdok;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
