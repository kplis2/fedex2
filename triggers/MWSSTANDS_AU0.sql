--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDS_AU0 FOR MWSSTANDS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.number <> old.number) then
  begin
    update mwsstandsegs set mwsstandnumber = new.number where mwsstand = new.ref;
    update whareas set mwsstandnumber = new.number where mwsstand = new.ref;
    update mwsconstlocs set mwsstandnumber = new.number where mwsstand = new.ref;
  end
  if (new.whsec <> old.whsec) then
    update mwsstandsegs set whsec = new.whsec, whsecdict = new.whsecdict
      where mwsstand = new.ref;
  if (new.whsecrow <> old.whsecrow) then
    update mwsstandsegs set whsecrow = new.whsecrow, whsecdict = new.whsecrowdict
      where mwsstand = new.ref;
end^
SET TERM ; ^
