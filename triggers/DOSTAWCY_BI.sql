--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_BI FOR DOSTAWCY                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
  declare variable wymagajnip VARCHAR(40);
  declare variable dl smallint;
  declare variable contactcontrol varchar(100);
BEGIN
 if(new.powiazany is null) then new.powiazany = 0;
 if(new.stoppay is null) then new.stoppay = 0;
 if(new.bank ='') then new.bank = null;
 if(new.zagraniczny is null) then new.zagraniczny = 0;
 if(new.rachunek is null) then new.rachunek = '';
 if(new.crm is null) then new.crm = 0;
 if(new.id = '') then exception DOSTAWCY_IDEMPTY;
 if(new.krajid = '') then new.krajid = null;
 if (new.krajid is not null) then
  select name from countries where symbol = new.krajid into new.kraj;
 if(new.waluta  = '') then new.waluta = NULL;
  if(new.waluta is null) then begin
    execute procedure GETCONFIG('WALPLN') returning_values new.waluta;
    if(new.waluta is null) then new.waluta = 'PLN';
  end
 if(new.dostawamiara = '') then new.dostawamiara = null;
 if(new.rabatsum is null) then new.rabatsum = 0;
 if(new.rabatkask is null) then new.rabatkask = 0;

  -- czy wymagac NIPu
  if (new.firma=1 and new.zagraniczny=0 and new.nip='') then
  begin
    execute procedure GETCONFIG('WYMAGAJNIP') returning_values :wymagajnip;
    if (:wymagajnip = '1') then exception BRAK_NIPU;
  end
  -- utworzenie prostego nipu
  new.prostynip = replace(new.nip, '-', '');
  new.regdate = current_date;
  -- nadanie kodu ksiegowego
  if (new.kontofk is null or (new.kontofk='')) then begin
    execute procedure X_BLANK_KODFK(new.company, 'DOSTAWCY', new.ref, null) returning_values new.kontofk;
  end
  -- sprawdzenie dlugosci kodu ksiegowego
  select KODLN from slodef where typ='DOSTAWCY' into :dl;
  if (dl is not null) then
    if (coalesce(char_length(new.kontofk),0)<>0 and coalesce(char_length(new.kontofk),0) <> :dl) then  -- [DG] XXX ZG119346
      exception BAD_LENGTH_FKKOD;
  -- obsluga blokady dostawcy do przelewow
  if (new.blokada is null) then new.blokada=0;
  if (new.blokada=0) then new.opisblokady='';

    -- sprawdzenie poprawnosci telefonu
  execute procedure get_config('CONTACTCONTROL',2) returning_values :contactcontrol;
  if (:contactcontrol is null) then contactcontrol = '0';
  if ((:contactcontrol = '1') and (new.telefon is not null)) then
    execute procedure contact_control('PHONE',new.telefon,0,'DOSTAWCY','TELEFON',new.ref,new.id);
END^
SET TERM ; ^
