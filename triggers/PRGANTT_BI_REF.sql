--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRGANTT_BI_REF FOR PRGANTT                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('PRGANTT') returning_values new.ref;
end^
SET TERM ; ^
