--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKROZ_BIU_REF FOR RKDOKROZ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0 ) then execute procedure gen_ref('RKDOKROZ') returning_values new.ref;
end^
SET TERM ; ^
