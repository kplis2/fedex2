--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_AI_SYNCHRO FOR DOSTAWCY                       
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable synchro smallint;
declare variable refout integer;
declare variable test varchar (60);
declare variable imie varchar(255);
declare variable nazwafizycznych smallint;
begin
  select konfig.wartosc from konfig where konfig.akronim = 'SYNCHROKLIENT'
    into :synchro;
  if (synchro = 1) then
  begin
   if (new.synchro is null) then
   begin
     if (new.firma = 0) then begin
       nazwafizycznych = 0;
       execute procedure getconfig('NAZWAFIZYCZNYCH')
         returning_values :nazwafizycznych;
       if (nazwafizycznych = '1') then
         imie = new.id;
     end
     execute procedure GEN_REF('KLIENCI') returning_values :refout;
      insert into KLIENCI(REF, NAZWA, FSKROT, KONTOFK,MIASTO, KODP,
          ULICA, NRDOMU, NRLOKALU, TELEFON, FAX, EMAIL, WWW,
          AKTYWNY, POCZTA, KRAJ, NIP, REGON, FIRMA,
          TYP, KODZEWN, WALUTA,DOSTAWCA, IMIE, SYNCHRO, COMPANY, POWIAZANY, cpowiat)
        values (:refout,new.NAZWA, new.ID, new.KONTOFK,new.MIASTO, new.KODP,
          new.ULICA, new.NRDOMU, new.NRLOKALU, new.TELEFON, new.FAX, new.EMAIL, new.WWW,
          new.AKTYWNY, new.POCZTA, new.KRAJ, new.NIP, new.REGON, new.FIRMA,
          new.TYP, new.KODZEWN,new.WALUTA,new.ref, :imie, 1, new.COMPANY, new.powiazany, new.cpowiat);
    end
    if(new.synchro=1) then update DOSTAWCY set synchro=null where REF=new.ref;
  end
end^
SET TERM ; ^
