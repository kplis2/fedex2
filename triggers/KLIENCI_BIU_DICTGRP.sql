--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BIU_DICTGRP FOR KLIENCI                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.dostawca is not null and old.dostawca is null) then begin
    execute procedure gen_ref('DICTGRP') returning_values new.dictgrp;
    update dostawcy set dictgrp=new.dictgrp where ref=new.dostawca;
  end
  else if (new.dostawca is not null and old.dostawca is not null and new.dostawca <> old.dostawca) then begin
    update dostawcy set dictgrp=null where ref=old.dostawca;
    execute procedure gen_ref('DICTGRP') returning_values new.dictgrp;
    update dostawcy set dictgrp=new.dictgrp where ref=new.dostawca;
  end
  else if (new.dostawca is null and old.dostawca is not null) then begin
    new.dictgrp = null;
    update dostawcy set dictgrp=new.dictgrp where ref=old.dostawca;
  end
end^
SET TERM ; ^
