--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSHEETSTATES_BIU_BLANK FOR NOTIFSHEETSTATES               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (coalesce(new.rights,';') = ';') then new.rights = '';
  if (coalesce(new.rightsgroup,';') = ';') then new.rightsgroup = '';
  if (new.number is null) then new.number = 0;
end^
SET TERM ; ^
