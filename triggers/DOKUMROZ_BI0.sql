--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMROZ_BI0 FOR DOKUMROZ                       
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable bn char(1);
declare variable staw numeric(14,2);
declare variable  faket smallint;
declare variable ktmt ktm_id;
begin
  if (exists (select first 1 1 from dokumpoz where fake = 1 and ref = new.pozycja)) then
    exception dokumpoz_fake;
  if(new.dostawa is null) then new.dostawa = 0;
  if(new.serialnr is null) then new.serialnr = '';
  new.wartosc = new.ilosc * new.cena;
  new.wart_koszt = new.cena_koszt * new.ilosc;
  new.iloscl = new.ilosc;
  new.pcena = new.cena;
  new.pwartosc = new.wartosc;
  new.wartoscl = new.wartosc;
  if(new.blokoblnag is null) then new.blokoblnag = 0;
  if(new.ack is null or (new.ack = 0)) then
    select DOKUMNAG.AKCEPT
      from DOKUMPOZ
        left join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.DOKUMENT)
      where DOKUMPOZ.ref = new.POZYCJA
      into new.ACK;
  if(new.cenasnetto > 0 or (new.cenasbrutto > 0)) then begin
    /*policzenie wartosci*/
    select DEFDOKUM.KAUCJABN
      from DOKUMPOZ
        left join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
        left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
      where DOKUMPOZ.ref = new.pozycja
      into :bn;
    select VAT.STAWKA
      from DOKUMPOZ
        left join TOWARY on (TOWARY.KTM = DOKUMPOZ.KTM)
        left join VAT on (TOWARY.VAT = VAT.GRUPA)
      where DOKUMPOZ.REF = new.pozycja
      into :staw;
    if(:bn = 'B') then begin
       new.wartsbrutto = new.ilosc * new.cenasbrutto;
       new.wartsnetto = new.wartsbrutto/(1+:staw/100);
    end else begin
       new.wartsnetto = new.ilosc * new.cenasnetto;
       new.wartsbrutto = new.wartsnetto*(1+:staw/100);
    end
  end
  select DEFDOKUM.wydania, DOKUMNAG.MAGAZYN, DOKUMPOZ.KTM, DOKUMPOZ.WERSJA
    from DOKUMPOZ
      left join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.dokument)
      left join DEFDOKUM on (DEFDOKUM.symbol = DOKUMNAG.TYP)
    where DOKUMPOZ.ref = new.pozycja
    into new.wydania, new.magazyn, new.ktm, new.wersja;
 if(new.ack = 0) then new.opkrozid = null;
end^
SET TERM ; ^
