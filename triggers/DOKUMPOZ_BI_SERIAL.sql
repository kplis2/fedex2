--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BI_SERIAL FOR DOKUMPOZ                       
  ACTIVE BEFORE INSERT POSITION 2 
as
declare variable nserial integer;
begin
  new.serialized = 0;
  if(new.ktm is not null) then begin
    select SERIAL from TOWARY where KTM = new.ktm into new.serialized;
    if(new.serialized = 1) then begin
      select NOTSERIALIZED from DOKUMNAG where REF=new.dokument into nserial;
      if(:nserial = 1) then begin
        new.serialized = 0;
      end
    end
  end
end^
SET TERM ; ^
