--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WALUTY_BU_REPLICAT FOR WALUTY                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.symbol <> old.symbol)
   or(new.kraj <> old.kraj) or (new.kraj is null and old.kraj is not null) or (new.kraj is not null and old.kraj is null)
   or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('WALUTY',old.symbol, null, null, null, null, old.token, old.state,
        new.symbol, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
