--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_AU_TRAINNEED FOR EMPLTRAININGS                  
  ACTIVE AFTER UPDATE POSITION 0 
AS
  declare variable trainneed integer;
begin
  select trainneed
    from etrainings
    where ref = new.training
    into :trainneed;

  if (trainneed is not null) then
  begin
    update empltrainneeds N
      set N.etraining = new.training
      where N.etrainneed = :trainneed and N.employee = new.employee;

    if (new.employee <> old.employee) then
      update empltrainneeds N
        set N.etraining = null
        where N.etrainneed = :trainneed and N.etraining = old.training and N.employee = old.employee;
  end
end^
SET TERM ; ^
