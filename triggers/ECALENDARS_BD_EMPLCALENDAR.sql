--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALENDARS_BD_EMPLCALENDAR FOR ECALENDARS                     
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
--MWr Usuniecie kalendarza jest mozliwe jezeli nie jest/byl on uzywany

  if (exists(select first 1 1 from emplcalendar where calendar = old.ref)) then
    exception ecalendars_delete;
end^
SET TERM ; ^
