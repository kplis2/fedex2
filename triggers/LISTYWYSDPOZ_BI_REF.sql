--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDPOZ_BI_REF FOR LISTYWYSDPOZ                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('LISTYWYSDPOZ')
      returning_values new.REF;
end^
SET TERM ; ^
