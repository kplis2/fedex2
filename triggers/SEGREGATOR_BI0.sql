--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SEGREGATOR_BI0 FOR SEGREGATOR                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(coalesce(new.rights,'') = '') then new.rights = ';';
  if(coalesce(new.rightsgroup,'') = '') then new.rightsgroup = ';';
end^
SET TERM ; ^
