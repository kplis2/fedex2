--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_AU_AUTOACC FOR BANKACCOUNTS                   
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable proc varchar(255);
declare variable sql varchar(1024);
begin
  if (new.hbstatus = 1 and (coalesce(new.hbstatus, 0) <> coalesce(old.hbstatus,0)) and new.dictdef = 1) then begin
      select autoproc from  bankacc
        where bankacc.symbol = new.bankacc
        into :proc;
      if(:proc is not null) then begin
        sql='execute procedure '||:proc||' ('''||new.ref||''')';
        execute statement sql;
      end else begin
        if(new.account = '') then exception SLOBANACC_EMPTYACC;
      end
  end
end^
SET TERM ; ^
