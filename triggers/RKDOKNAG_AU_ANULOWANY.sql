--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AU_ANULOWANY FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.anulowany=old.anulowany) then exit;
  else update rkdokpoz set anulowany = new.anulowany where dokument = new.ref;
end^
SET TERM ; ^
