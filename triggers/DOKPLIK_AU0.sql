--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_AU0 FOR DOKPLIK                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if(new.segregator <> old.segregator) then begin
    if(exists(select s.dokplik from dokpliksegregatory s where s.dokplik = new.ref and s.segregator = new.segregator)) then
      update dokpliksegregatory s set s.segdefault = 1 where s.dokplik = new.ref and s.segregator = new.segregator;
    else
      insert into dokpliksegregatory(dokplik, segregator,segdefault) values (new.ref, new.segregator, 1);
    delete from dokpliksegregatory where segregator = old.segregator and dokplik = new.ref;
  end
  if(new.blokada=0 and old.blokada<>0) then begin
    -- podczas wrzucenia do repozytorium oznacz ze pliki sa juz na serwerze
    update doklink set nowy=0 where nowy=1 and dokplik=new.ref;
  end
end^
SET TERM ; ^
