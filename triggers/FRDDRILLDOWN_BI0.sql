--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDDRILLDOWN_BI0 FOR FRDDRILLDOWN                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDDRILLDOWN')
      returning_values new.REF;
end^
SET TERM ; ^
