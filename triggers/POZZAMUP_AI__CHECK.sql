--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMUP_AI__CHECK FOR POZZAMUP                       
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable ilosc integer;
declare variable suma integer;
begin
   select pz.ilosc from pozzam pz
     where pz.ref = new.refpoz
     into :ilosc;
   select sum(pv.ilosc) from pozzamupview pv
     where pv.refpoz = new.refpoz
     into :suma;
   if(suma > ilosc) then
     exception POZZAMUP_EXPT 'Nie można odebrać większej ilości towaru niż jest ona zamówiona.';
end^
SET TERM ; ^
