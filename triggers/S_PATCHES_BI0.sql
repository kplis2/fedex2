--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_PATCHES_BI0 FOR S_PATCHES                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  new.backup_files = 0;
  new.backup_databases = 0;
  new.pliki = 0;
  new.opis = 0;
  new.esystem = 0;
  new.x_esystem = 0;
  new.sdas = 0;
  new.x_sdas = 0;
  new.appini = 0;
  new.status_aktualizacji = 0;
end^
SET TERM ; ^
