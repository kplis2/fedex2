--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLNAG_BU0 FOR KPLNAG                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.akt = 0) then new.glowna = 0;
  if(new.akt = 0 and old.akt = 1) then
    new.datazam = current_date;
  if(new.akt = 1 and old.akt = 0) then
    new.datazam = null;
end^
SET TERM ; ^
