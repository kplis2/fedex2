--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCIEDI_BI FOR KLIENCIEDI                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable expfak smallint;
declare variable expzam smallint;
declare variable expdok smallint;
declare variable expsped smallint;
begin
  if (new.fromdate > new.todate) then
    exception universal 'Błedny zakres dat!';
  select coalesce(expfak,0),coalesce(expzam,0),coalesce(expdok,0),coalesce(expsped,0)
    from urzsprtyp
    where symbol=new.editype into :expfak,:expzam,:expdok,:expsped;
  if (exists(select first 1 1 from klienciedi k
             join urzsprtyp u on u.symbol=k.editype
             where k.klient = new.klient
             and new.todate >= k.fromdate
             and new.fromdate <= k.todate
             and ((u.expfak=1 and :expfak=1) or (u.expzam=1 and :expzam=1) or (u.expdok=1 and :expdok=1) or (u.expsped=1 and :expsped=1))
             )) then
    exception universal 'Dla wybranego okresu istnieje już metoda wymiany danych!';
end^
SET TERM ; ^
