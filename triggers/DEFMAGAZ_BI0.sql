--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_BI0 FOR DEFMAGAZ                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.pozabilansowy is null) then new.pozabilansowy=0;
  if (new.spedgrupalacz is null) then new.spedgrupalacz = 0;
  if (new.chronologiadostaw is null) then new.chronologiadostaw = 0;
  if(new.nieksiegowy is null) then new.nieksiegowy=0;
  if(new.typ = 'E' or new.typ = 'S') then new.fifo=' ';
  if(new.magbreak is null) then new.magbreak = 0;
  if (new.mwsorddivide is null) then new.mwsorddivide = 0;
  if(new.AUTOAKC = 1 AND new.FIFO='R') then
   exception  DEFDOKUM_AUTOACEPT_NOT_ALLOWED;
end^
SET TERM ; ^
