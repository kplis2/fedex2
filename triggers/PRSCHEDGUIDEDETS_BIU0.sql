--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_BIU0 FOR PRSCHEDGUIDEDETS               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (new.quantity is null) then new.quantity = 0;
  if (new.out is null) then new.out = 1;
  if (new.overlimit is null) then new.overlimit = 0;
  if (new.accept is null) then new.accept = 0;
  if (new.sourceupdate is null) then new.sourceupdate = 0;
  if (new.reportfactor is null) then new.reportfactor = 1;
  if (new.shortage is null) then new.shortage = 0;
  if (new.shortageratio is null or new.shortageratio = 0) then new.shortageratio = 1;
  if (new.byproduct is null) then new.byproduct = 0;
  if (new.autodoc is null) then new.autodoc = 0;
  if ((new.prdepart is null or new.prdepart = '') and new.prschedguide is not null) then
    select g.prdepart
      from prschedguides g
      where g.ref = new.prschedguide
      into new.prdepart;
  -- uzupelnienie materialu z karty technologicznej
  if (new.prshmat is null and new.prschedguidepos is not null) then
    select prshmat from prschedguidespos where ref = new.prschedguidepos into new.prshmat;
  if (new.wersjaref is not null and new.ktm is null) then
    select ktm from wersje where ref = new.wersjaref into new.ktm;
  if ((new.quantity <> old.quantity or new.ktm <> old.ktm or new.wersjaref <> old.wersjaref)
      and new.ssource <> 3 and new.sourceupdate <> 1
      and ((new.ssource = 1 and new.accept > 1) or (new.accept > 0 and new.ssource <> 1))
  ) then
    exception PRSCHEDGUIDEDETS_ERROR 'Rozliczenie zaakceptopwane - popraw źródło rozliczenia';
end^
SET TERM ; ^
