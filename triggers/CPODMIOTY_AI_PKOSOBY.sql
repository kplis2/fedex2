--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AI_PKOSOBY FOR CPODMIOTY                      
  ACTIVE AFTER INSERT POSITION 1 
as
declare variable cnt integer;
begin
  if(new.firma = 0) then begin
    select count(*) from PKOSOBY where CPODMIOT = new.REF into :cnt;
    if(:cnt > 1) then exception CPODMIOTY_PKWIECEJNIZ1;
    if(cnt = 1) then begin
      update PKOSOBY set IMIE = new.imie, nazwisko = new.nazwisko, nazwafirmy = new.nazwafirmy, plec = new.plec,
          dataur = new.dataur, wyksztal = new.wyksztal, zawod = new.zawod,
          telefon = new.telefon, fax = new.fax, email = new.email, komorka = new.komorka,
          ptelefon = new.ptelefon, pfax = new.pfax, pemail = new.pemail, pkomorka = new.pkomorka,
          aktywny = new.aktywny
          where CPODMIOT = new.ref;
    end else begin
      insert into PKOSOBY(CPODMIOT, NAZWISKO, IMIE, NAZWAFIRMY, DATAUR, WYKSZTAL, ZAWOD, KORESPOND,
                          TELEFON, FAX, EMAIL, KOMORKA, PTELEFON, PFAX, PEMAIL, PKOMORKA, AKTYWNY)
        values(new.ref, new.nazwisko, new.imie, new.nazwafirmy, new.dataur, new.wyksztal, new.zawod, new.korespond,
               new.telefon, new.fax, new.email, new.komorka, new.ptelefon, new.pfax, new.pemail, new.pkomorka, new.aktywny);
    end
    /* aktualizacja adresu prywatnego i sluzbowego*/
    update PKOSOBY set PULICA = new.PULICA, PNRDOMU = new.PNRDOMU, PNRLOKALU = new.PNRLOKALU,
                       PMIASTO = new.PMIASTO, PKODP = new.pkodp,
                       PPOCZTA = new.PPOCZTA, PKRAJ = new.PKRAJ,
                       ULICA = new.ULICA, MIASTO = new.MIASTO, KODP = new.kodp,
                       POCZTA = new.POCZTA, KRAJ = new.KRAJ
                        where CPODMIOT = new.ref;
    update PKOSOBY set KORESPOND = new.korespond where CPODMIOT = new.ref;
    /* aktualizacja adresu korespondencyjnego */
    update PKOSOBY set KULICA = new.KULICA, KNRDOMU = new.KNRDOMU, KNRLOKALU = new.KNRLOKALU,
                      KMIASTO = new.KMIASTO, KKODP = new.kkodp,
                      KPOCZTA = new.KPOCZTA, KKRAJ = new.KKRAJ
            where CPODMIOT = new.ref;
  end
end^
SET TERM ; ^
