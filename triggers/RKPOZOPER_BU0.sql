--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKPOZOPER_BU0 FOR RKPOZOPER                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable slotyp varchar(15);
begin
  if(new.typ <> old.typ ) then
  begin
    select SLODEF.TYP from slodef JOIN RKDEFOPER on ( SLODEF.ref = RKDEFOPER.slodef)
      where RKDEFOPER.symbol = new.operacja
      into :slotyp;
    if (slotyp is null) then
    begin
      select RKDEFOPER.slowymog from RKDEFOPER
        where symbol = new.operacja
        into :slotyp;
      if(:slotyp <> '1') then
        slotyp = null;
    end
    if (slotyp is null and new.typ > 0) then
      exception RKPOZOPER_NOTSLO;
    if (slotyp <> 'KLIENCI' and slotyp <> 'DOSTAWCY' and new.typ>1) then
      exception RKPOZOPER_NOTKLI;
  end
  if (new.konto = '') then
    new.konto = null;
  if (new.pm is null or (new.pm <> '+' and new.pm <> '-')) then
    exception RKPOZOPER_PM new.pm;
end^
SET TERM ; ^
