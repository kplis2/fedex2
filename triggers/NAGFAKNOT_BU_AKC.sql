--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKNOT_BU_AKC FOR NAGFAKNOT                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.TRESC<>old.TRESC or new.PTRESC<>old.PTRESC or new.DATA<>old.DATA) then
    if(old.AKCEPTACJA=1) then exception nagfaknot_expt 'Nota zaakceptowana, nie wolno poprawiać!';
end^
SET TERM ; ^
