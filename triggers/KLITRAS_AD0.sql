--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLITRAS_AD0 FOR KLITRAS                        
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable tras integer;
begin
  if(old.glowna = 1) then begin
    select max(trasa) from KLITRAS where KLIENT = old.klient into :tras;
    if(:tras > 0) then
      update KLITRAS set GLOWNA = 1 where KLIENT = old.klient and TRASA = :tras;
  end
end^
SET TERM ; ^
