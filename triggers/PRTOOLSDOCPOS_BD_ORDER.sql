--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCPOS_BD_ORDER FOR PRTOOLSDOCPOS                  
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.ord = 0) then exit;
  update PRTOOLSDOCPOS set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and prtoolsdoc = old.prtoolsdoc;
end^
SET TERM ; ^
