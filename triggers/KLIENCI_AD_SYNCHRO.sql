--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AD_SYNCHRO FOR KLIENCI                        
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable refdost integer ;
declare variable synchro smallint;
begin
 select konfig.wartosc from konfig where konfig.akronim = 'SYNCHROKLIENT'
    into :synchro;
  if (synchro = 1) then
  begin
    select dostawcy.ref from dostawcy where dostawcy.ref = old.dostawca
    into :refdost;
    if (refdost is not null ) then
    delete from dostawcy where dostawcy.ref = :refdost;
  end
end^
SET TERM ; ^
