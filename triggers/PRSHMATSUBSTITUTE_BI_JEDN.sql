--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMATSUBSTITUTE_BI_JEDN FOR PRSHMATSUBSTITUTE              
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  select j.ref
    from towjedn j
    join wersje w on (w.ktm = j.ktm)
    where j.glowna = 1
      and w.ref = new.substitute
    into new.jedn;
end^
SET TERM ; ^
