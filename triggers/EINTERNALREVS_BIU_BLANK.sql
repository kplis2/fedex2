--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EINTERNALREVS_BIU_BLANK FOR EINTERNALREVS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.name = '') then
    new.name = null;
  if (new.code = '') then
    new.code = null;
end^
SET TERM ; ^
