--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_BU_BLOCK FOR PRSCHEDOPERS                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable gref integer;
begin
  if(new.schedblocked = 0) then update prschedguides pg set pg.schedblocked = 2 where pg.ref = new.guide;
  if(new.schedblocked = 1) then update prschedguides pg set pg.schedblocked = 3 where pg.ref = new.guide;
  if(new.schedblocked = 2) then new.schedblocked = 0;
  if(new.schedblocked = 3) then new.schedblocked = 1;
end^
SET TERM ; ^
