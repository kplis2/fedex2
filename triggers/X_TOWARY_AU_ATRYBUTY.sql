--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_TOWARY_AU_ATRYBUTY FOR TOWARY                         
  ACTIVE AFTER UPDATE POSITION 80 
AS
begin
  if (old.ktm is distinct from new.ktm) then
    update x_atrybuty set ktm = new.ktm where otable = 'WERSJE' and ktm = old.ktm;
end^
SET TERM ; ^
