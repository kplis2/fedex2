--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTNAG_BIU0 FOR EPRESENCELISTNAG               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable minutes varchar(10);
declare variable hours varchar(10);
declare variable cut smallint;
declare variable minutesint integer;
declare variable hoursint integer;
declare variable timestampstr varchar(20);
declare variable listdate date;
begin
  if (new.timestart is not null and new.timestart <> '') then
  begin
    select epresencelists.listsdate
      from epresencelists
      where epresencelists.ref = new.epresencelist
      into :listdate;
    new.timestart = replace(new.timestart,',',':');
    new.timestart = replace(new.timestart, '.', ':');
    new.timestart = replace(new.timestart, ';', ':');
    cut = position(':' in new.timestart);
    if (:cut = 0) then
      exception TIME_EXPT;
    else
    begin
      hours = substring(new.timestart from 1 for :cut-1);
      minutes = substring(new.timestart from :cut+1);  -- [DG] XXX ZG119346
      if (coalesce(char_length(:hours),0)=1) then  -- [DG] XXX ZG119346
        hours = '0'||:hours;
      if (coalesce(char_length(:minutes),0)=1) then  -- [DG] XXX ZG119346
        minutes = '0'||:minutes;
      if (coalesce(char_length(:hours),0)<> 2 or coalesce(char_length(:minutes),0)<>2) then  -- [DG] XXX ZG119346
        exception TIME_EXPT 'niewlasciwy format godzin lub minut';
      if (cast(:hours as integer) < 0 or cast(:hours as integer) > 23) then
        exception TIME_EXPT 'niedopuszczalna godzina';
      if (cast(:minutes as integer) < 0 or cast(:minutes as integer) > 59) then
        exception TIME_EXPT 'niedopuszczalny wymiar minut';
      timestampstr = :listdate||' '||:hours||':'||:minutes||':'||'00';
      new.startat = cast(:timestampstr as timestamp);
      new.timestart = :hours||':'||:minutes;
    end
  end
  if (new.timestop is not null and new.timestop <> '') then
  begin
    select epresencelists.listsdate
      from epresencelists
      where epresencelists.ref = new.epresencelist
      into :listdate;
    new.timestop = replace(new.timestop,',',':');
    new.timestop = replace(new.timestop, '.', ':');
    new.timestop = replace(new.timestop, ';', ':');
    cut = position(':' in new.timestop);
    if (:cut = 0) then
      exception TIME_EXPT;
    else
    begin
      hours = substring(new.timestop from 1 for :cut-1);
      minutes = substring(new.timestop from :cut+1 for coalesce(char_length(new.timestop),0));  -- [DG] XXX ZG119346
      if (coalesce(char_length(:hours),0)=1) then  -- [DG] XXX ZG119346
        hours = '0'||:hours;
      if (coalesce(char_length(:minutes),0)=1) then  -- [DG] XXX ZG119346
        minutes = '0'||:minutes;
      if (coalesce(char_length(:hours),0)<> 2 or coalesce(char_length(:minutes),0)<>2) then  -- [DG] XXX ZG119346
        exception TIME_EXPT 'niewlasciwy format godzin lub minut';
      if (cast(:hours as integer) < 0 or cast(:hours as integer) > 23) then
        exception TIME_EXPT 'niedopuszczalna godzina';
      if (cast(:minutes as integer) < 0 or cast(:minutes as integer) > 59) then
        exception TIME_EXPT 'niedopuszczalny wymiar minut';
      timestampstr = :listdate||' '||:hours||':'||:minutes||':'||'00';
      new.stopat = cast(:timestampstr as timestamp);
      new.timestop = :hours||':'||:minutes;
    end
  end
  if (new.shift = 2 and new.startat > new.stopat) then
    new.stopat = new.stopat + 1;
  if (new.startat > new.stopat) then
    exception TIME_EXPT 'godzina rozpoczecia musi byc wczesniejsza niz zakonczenia';
  new.jobtimeh = cast((new.stopat - new.startat)*24 as integer);
  new.jobtimeq = cast(cast(cast((new.stopat - new.startat)*96 as integer) as numeric(14,2))/4 as numeric(14,2));
end^
SET TERM ; ^
