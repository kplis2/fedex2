--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLLDEDINSTALS_AIUD_COLLDEDUCT FOR ECOLLDEDINSTALS                
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 1 
AS
begin
--MWr: Aktualizacja kwoty splaconej na naglowku potracenia komorniczego

  if (inserting or deleting or old.insvalue <> new.insvalue) then
  begin
    update ecolldeductions
      set deducted = deducted - iif(not inserting, old.insvalue, 0)
                              + iif(not deleting, new.insvalue, 0)
      where ref = coalesce(new.colldeduction,old.colldeduction);
  end
end^
SET TERM ; ^
