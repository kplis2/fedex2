--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOT_BIBKBLOCK FOR DOKUMNOT                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.bkblock is null) then new.bkblock = 0;
  if (new.bkblockdescript is null) then new.bkblockdescript = '';
end^
SET TERM ; ^
