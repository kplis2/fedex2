--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMUP_AU_CHECK FOR POZZAMUP                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable ilosc integer;
declare variable suma integer;
begin
   if(old.ilosc <> new.ilosc) then begin
     select pz.ilosc from pozzam pz
       where pz.ref = new.refpoz
       into :ilosc;
     select sum(pv.ilosc) from pozzamupview pv
       where pv.refpoz = new.refpoz
       into :suma;
     if(suma > ilosc) then
       exception pozzamup_expt 'Nie można odebrać większej ilości towaru niż jest ona zamówiona.';
   end
end^
SET TERM ; ^
