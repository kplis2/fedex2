--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_BI0 FOR ATRYBUTY                       
  ACTIVE BEFORE INSERT POSITION 0 
as
--declare variable typpola smallint; --[PM]
declare variable kod varchar(20);
declare variable iddefcechw integer_id; --[PM]
begin

  if (new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref
      into new.nrwersji, new.ktm;
  else
    if (new.ktm is not null and new.nrwersji is not null) then
      select ref from WERSJE where ktm=new.ktm and nrwersji=new.nrwersji
        into new.wersjaref;
  if (new.cecha is not null and new.wartosc is not null and new.wartosc<>'') then begin
    execute procedure getcecha(new.ktm, new.cecha, new.wartosc) returning_values :iddefcechw;
    select kod from defcechw where id_defcechw=:iddefcechw into :kod;
    if (kod = '') then new.kod=null;
    else new.kod = :kod;  --[PM] koniec

  end
end^
SET TERM ; ^
