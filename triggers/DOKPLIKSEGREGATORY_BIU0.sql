--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIKSEGREGATORY_BIU0 FOR DOKPLIKSEGREGATORY             
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if(new.segdefault is null) then new.segdefault = 0;
  if(new.segdefault = 1 and not exists(select d.segregator from dokplik d where d.ref = new.dokplik and d.segregator = new.segregator))then exception universal 'Zmiana segregatora domyslnego zablokowana';
end^
SET TERM ; ^
