--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_REPLICAT_BI0 FOR RP_REPLICAT                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if((new.OVERWRITE is NULL)or(new.OVERWRITE = '')) then new.OVERWRITE = 'N';
  if((new.S_OBJ = 0) or (new.S_OBJ is NULL)) then new.S_OBJ = gen_id(GEN_RP_OBJECT,1);
  if((new.D_OBJ = 0) or (new.D_OBJ is NULL)) then new.D_OBJ = gen_id(GEN_RP_OBJECT,1);
  if(new.REF_ID = 0 or (new.REF_ID is null)) then new.REF_ID = gen_id(GEN_RP_REPLICAT,1);
end^
SET TERM ; ^
