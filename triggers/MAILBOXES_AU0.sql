--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MAILBOXES_AU0 FOR MAILBOXES                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable opread varchar(255);
declare variable opedit varchar(255);
declare variable opsend varchar(255);
declare variable foldertyp varchar(1);
begin
    if (new.nazwa <> old.nazwa) then
    UPDATE DEFFOLD
    SET NAZWA = new.nazwa,
        SYMBOL = substring(new.nazwa from 1 for 80)
    WHERE (mailbox = new.ref and domyslny = 2);
    if (coalesce(new.operator,0) <> coalesce(old.operator,0)) then
    begin
      select d.operread, d.operedit, d.opersend from deffold d where d.mailbox = new.ref and d.domyslny = 2
      into :opread, :opedit, :opsend;
      opread = replace(coalesce(opread,';;;'), ';'||coalesce(old.operator,';')||';', ';'||new.operator||';');
      opedit = replace(coalesce(opedit,';;;'), ';'||coalesce(old.operator,';')||';', ';'||new.operator||';');
      opsend = replace(coalesce(opsend,';;;'), ';'||coalesce(old.operator,';')||';', ';'||new.operator||';');
        UPDATE DEFFOLD d
            SET d.operator = new.operator,
                d.operread = :opread,
                d.OPEREDIT = :opedit,
                d.OPERSEND = :opsend
        WHERE mailbox = new.ref and domyslny = 2;
    end

    if (coalesce(new.publicbox ,0) <> coalesce(old.publicbox,0)) then begin
        if(new.publicbox = 1) then
            foldertyp = 'P';
        else
            foldertyp = 'U';

        update deffold d
            set d.typ = :foldertyp
            where d.mailbox = new.ref;
    end
    --exception test_break new.operator;
end^
SET TERM ; ^
