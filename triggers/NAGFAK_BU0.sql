--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BU0 FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable symbolpoakcept smallint;
declare variable akcept integer;
declare variable NUMBERG varchar(40);
declare variable sprzkontrlk integer;
declare variable limitkr decimal(14,2);
declare variable kredyt decimal(14,2);
declare variable cnt integer;
declare variable nieobrot integer;
declare variable sblokadadelay varchar(255);
declare variable blokadadelay integer;
declare variable local smallint;
declare variable blokfk varchar(255);
declare variable wysstan char(1);
declare variable oldnieobrot integer;
declare variable BLOCKREF integer;
declare variable kor integer;
declare variable autozmiana smallint;
declare variable bankdomyslny varchar(64);
declare variable tabkursuzywaj smallint;
declare variable typkurs smallint;
declare variable sredni numeric(14,4);
declare variable zakupu numeric(14,4);
declare variable sprzedazy numeric(14,4);
declare variable dataokres timestamp;
declare variable lokres varchar(6);
declare variable blokadadat TIMESTAMP;
declare variable sposoblrozrach integer;
declare variable sumfakzal numeric(14,2);
declare variable anulakcept integer;
declare variable elsedokanul integer;
declare variable dokmagwokresiekontrola smallint;
declare variable dokmagwokresieokrescnt integer;
declare variable dozapnetto smallint;
declare variable termdost timestamp;
declare variable ktm varchar(40);
declare variable vptransdate smallint;
declare variable ildni integer;
declare variable useildni smallint;
declare variable va smallint;
declare variable oo smallint;
begin
  --weryfikacja okresów dokumentów magazynowych
  if (new.akceptacja <> old.akceptacja and new.akceptacja = 1) then begin
    select coalesce(t.dokmagwokresie,0)
      from typfak t
      where t.symbol = new.typ
    into :dokmagwokresiekontrola;
    if (:dokmagwokresiekontrola = 1) then begin
      select count(distinct d.okres)
        from dokumnag d
        where d.faktura = new.ref
      into :dokmagwokresieokrescnt;
      if (:dokmagwokresieokrescnt > 1) then
        exception NAGFAK_DOKMAGWOKRESIE;
    end
  end
  --koniec weryfikacji okresów dokumentów magazynowych
  if(new.operator = 0) then new.operator = NULL;
  if(new.sprzedawca = 0) then new.sprzedawca = NULL;
  if(new.sprzedawcazew = 0) then new.sprzedawcazew = NULL;
  if(new.sumwartbru is null) then new.sumwartbru = 0;
  if(new.psumwartbru is null) then new.psumwartbru = 0;
  if(new.sumwartnet is null) then new.sumwartnet = 0;
  if(new.psumwartnet is null) then new.psumwartnet = 0;
  if(new.sumkaucja is null ) then new.sumkaucja = 0;
  if(new.psumkaucja is null ) then new.psumkaucja = 0;
  if(new.zaplacono is null) then new.zaplacono = 0;
  if(new.kwotazal is null) then new.kwotazal = 0;
  if(new.magrozlicz is null) then new.magrozlicz = 0;
  if(new.kosztdost is null) then new.kosztdost = 0;
  if(new.koszt is null) then new.koszt = 0;
  if(new.pkoszt is null) then new.pkoszt = 0;
  if(new.kosztanal is null) then new.kosztanal = 0;
  if(new.kosztkat is null) then new.kosztkat = 0;
  if(new.anulowanie is null) then new.anulowanie = 0;
  if(new.waga is null) then new.waga = 0;
  if(new.objetosc is null) then new.objetosc = 0;
  if(new.rozrachrozval is null) then new.rozrachrozval = 0;
  if(new.rabat is null) then new.rabat = 0;
  if(new.magrozlicz > 0) then
    new.kosztanal = new.koszt;
  else
    new.kosztanal = new.kosztkat;
  if(new.bn is null or (new.bn <>'N' and new.bn <> 'B'))then
   select BN from TYPFAK where SYMBOL = new.typ into new.bn;
  if(new.typ<>old.typ) then begin
    select zakup, sad, eksport, fakturarr, zaliczkowy from TYPFAK where SYMBOL = new.typ
    into new.zakup, new.sad, new.eksport, new.fakturarr, new.zaliczkowy;
  end
  select oo from typfak where symbol = new.typ into :oo;
  if(oo = 1) then
  begin
    if(coalesce(new.klient,0) <> 0) then
      select vatactive from klienci where ref = new.klient into :va;
    if(:va = 0) then
      exception UNIVERSAL'Kontrahent nie jest płatnikiem VAT. Wystawienie faktury OO jest niemożliwe';
  end
  if(new.akceptacja = 1 and new.platnik <> old.platnik) then
    exception NAGFAK_PLATNIKNOTCHANGE;
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  -- sprawdzenie czy pozycje faktur niekotygujacych maja cene <> 0
  if( :local = 1 and new.akceptacja = 1 and new.nieobrot < 3
      and (old.akceptacja = 0 or old.akceptacja = 9) ) then
  begin
    select typfak.korekta from typfak where typfak.symbol = new.typ into :kor;
    if(:kor is null) then kor = 0;
    if(:kor = 0) then begin
      execute procedure POZFAK_CHECK_VALUE(new.ref);
    end
    if(new.kwotazal is null) then new.kwotazal = 0;
    if(new.zaliczkowy > 0 and new.kwotazal = 0) then
      exception NAGFAK_EXPT 'Brak podanej kwoty zaliczki na fakt. zaliczkowej';
    if(exists(select ref from POZFAK
                join towtypes on (towtypes.numer = pozfak.usluga)
              where dokument=new.ref and magazyn is null and towtypes.isservice <>1 and ilosc<>pilosc))
    then exception POZFAK_BEZMAGAZYNU;
  end
  if(new.akceptacja<>1 and new.akceptacja<>8) then begin
    if((new.sumwartnet <> old.sumwartnet) or
       (new.sumwartbru <> old.sumwartbru) or
       (new.sumwartnetzl <> old.sumwartnetzl) or
       (new.sumwartbruzl <> old.sumwartbruzl) or
       (new.kurs<>old.kurs) or
       (new.kwotazal <> old.kwotazal)) then begin
       -- naliczanie pol ESUM...
       if(new.zaliczkowy>0) then begin
         new.esumwartbru = new.pesumwartbru + new.kwotazal;
         new.esumwartbruzl = new.esumwartbru*new.kurs;
         new.esumwartnet = 0;
         new.esumwartnetzl = 0;
       end else begin
         new.esumwartbru = new.sumwartbru;
         new.esumwartnet = new.sumwartnet;
         new.esumwartbruzl = new.sumwartbruzl;
         new.esumwartnetzl = new.sumwartnetzl;
       end
    end
  end
  if((new.sumwartbru <> old.sumwartbru) or
     (new.psumwartbru <> old.psumwartbru) or
     (new.esumwartbru <> old.esumwartbru) or
     (new.pesumwartbru <> old.pesumwartbru) or
     (new.zaplacono <> old.zaplacono) or
     (new.dozaplaty <> old.dozaplaty) or
     (new.nieobrot <> old.nieobrot) or
     (new.kwotazal <> old.kwotazal) or
     (new.sumkaucja <> old.sumkaucja) or
     (new.rozrachrozval <> old.rozrachrozval) or
     (new.akceptacja <> old.akceptacja)) then begin
     nieobrot = 0;
     if((new.nieobrot = 1) or (new.nieobrot = 3)) then nieobrot = 1;
     if(:nieobrot = 1) then
       new.dozaplaty = 0;
     else begin
       select sposoblrozrach, dozaplatynetto
         from typfak
         where symbol = new.typ
       into :sposoblrozrach, :dozapnetto;
       if (:dozapnetto is null) then dozapnetto = 0;
       if(:sposoblrozrach <> 0) then begin
         if (:dozapnetto = 0) then
           new.dozaplaty = (new.esumwartbru - new.pesumwartbru) + (new.sumkaucja - new.psumkaucja) - new.zaplacono;
         else
           new.dozaplaty = (new.esumwartnet - new.pesumwartnet) + (new.sumkaucja - new.psumkaucja) - new.zaplacono;
       end else begin
         if (:dozapnetto = 0) then
           new.dozaplaty = (new.sumwartbru - new.psumwartbru) + (new.sumkaucja - new.psumkaucja) - new.zaplacono;
         else
           new.dozaplaty = (new.sumwartnet - new.psumwartnet) + (new.sumkaucja - new.psumkaucja) - new.zaplacono;
       end
        /*stara wersja
        if(new.akceptacja = 0) then
        */
       if(new.zaplacono = 0) then
         new.dozaplaty = new.dozaplaty - new.rozrachrozval;
     end
  end
  if((new.esumwartbru <> old.esumwartbru) or
     (new.pesumwartbru <> old.pesumwartbru) or
     (new.esumwartbruzl <> old.esumwartbruzl) or
     (new.pesumwartbruzl <> old.pesumwartbruzl)) then begin
      new.ewartbru = new.esumwartbru - new.pesumwartbru;
      new.ewartnet = new.esumwartnet - new.pesumwartnet;
      new.ewartbruzl = new.esumwartbruzl - new.pesumwartbruzl;
      new.ewartnetzl = new.esumwartnetzl - new.pesumwartnetzl;
  end
  if((new.sumwartbru <> old.sumwartbru) or
     (new.psumwartbru <> old.psumwartbru) or
     (new.sumwartbruzl <> old.sumwartbruzl) or
     (new.psumwartbruzl <> old.psumwartbruzl)) then begin
      new.wartbru = new.sumwartbru - new.psumwartbru;
      new.wartnet = new.sumwartnet - new.psumwartnet;
      new.wartbruzl = new.sumwartbruzl - new.psumwartbruzl;
      new.wartnetzl = new.sumwartnetzl - new.psumwartnetzl;
  end
  if(new.akceptacja <> old.akceptacja) then begin
    if(new.akceptacja not in (7,8)) then begin
      execute procedure GETCONFIG('SIDFK_BLOKADADAT') returning_values :sblokadadelay;
      if(:sblokadadelay <> '') then begin
        blokadadat = cast(:sblokadadelay as timestamp);
        if(new.zakup = 1) then begin
          if(new.datasprz <= :blokadadat) then
            exception NAGFAK_BLOKADAKSTERM;
        end else begin
          if(new.data <= :blokadadat) then
            exception NAGFAK_BLOKADAKSTERM;
        end
      end
    end
    if(new.blokada > 0) then begin
      execute procedure GETCONFIG('SIDFK_BLOKADA') returning_values :sblokadadelay;
      if(:sblokadadelay <> '') then
        blokadadelay = cast(:sblokadadelay as integer);
      else blokadadelay = 0;
      if(new.blokada < 3 or current_date >= new.data + :blokadadelay) then begin
        if(new.blokada = 1) then exception NAGFAK_BLOKADAHAND;
        else if(new.blokada = 3) then begin
          if(new.zakup = 0) then
            execute procedure GETCONFIG('SIDFK_SPRBLOK') returning_values :blokfk;
          else
            execute procedure GETCONFIG('SIDFK_ZAKBLOK') returning_values :blokfk;
          if(:blokfk <> '2')then exception NAGFAK_BLOKADAKS;
        end
      end else if(new.blokada = 3) then new.blokada = 0; --zniesienie blokady ksiegowej, jesli mozna
    end else if(new.akceptacja = 1 and old.akceptacja = 0) then begin
      execute procedure GETCONFIG('SIDFK_BLOKADADAT') returning_values :sblokadadelay;
      if(:sblokadadelay <> '') then begin
        blokadadat = cast(:sblokadadelay as timestamp);
        if(new.data <= :blokadadat) then
          exception NAGFAK_BLOKADAKSTERM;
      end
    end

    if(new.akceptacja = 0) then new.dataakc = null;
    else new.dataakc = current_date;
  end
  if(new.datasprz is null) then new.datasprz = new.data;
  if(new.kurs = 0) then new.kurs = 1;
  new.vkurs = new.kurs;
  if(new.reffak is null and old.reffak is not null and :local = 1)then begin
    select NIEOBROT from TYPFAK where SYMBOL = new.TYP into new.nieobrot;
    if(new.nieobrot=1 and new.zaliczkowy=0) then new.nieobrot = 3;
    if(new.nieobrot=1 and new.zaliczkowy>0) then new.nieobrot = 2;
  end
  if(((((new.akceptacja = 0) or (new.akceptacja >= 9))and old.akceptacja = 1)
   or(new.anulowanie>0 and old.anulowanie = 0)) and (:local = 1)) then begin
    --wycofanie akceptacji - spradzenie, czy są zaakceptowane korekty
    if(new.korekta is not null and exists(select ref from nagfak where ref = new.korekta and anulowanie = 0)) then
      exception NAGFAK_KOREKTY_AKC;
--    akcept = null;
--    select count(*) from NAGFAK n where (n.REFK=new.ref and n.anulowanie = 0) into :akcept;
--    if(:akcept > 0) then
    -- sprawdzenie czy istnieja dokumenty pochodne nie anuowane
    if (exists (select ref from nagfak where ref = new.reffak and anulowanie = 0)) then
      exception NAGFAK_FAKTURY_SA;
    --wycofanie akceptacji - sprawdzenie, czy faktura zaliczkowa jest rozliczona
    akcept = null;
    select count(*) from NAGFAKZAL where FAKTURAZAL=new.ref into :akcept;
    if(:akcept > 0) then
      exception NAGFAK_ZALICZKI_AKC;
  end
  if(((((new.akceptacja = 0) or (new.akceptacja = 9))and old.akceptacja = 1)) and (:local = 1)) then begin
    if(new.zafisk = 1) then
      exception NAGFAK_DOK_ZAFISKALIZOWANY;
  end
  if(((new.akceptacja = 0) or (new.akceptacja = 7) or (new.akceptacja >= 9))and (old.akceptacja = 1 or old.akceptacja = 8 )) then begin
    new.proglojdone = 0;
  end
  if(new.akceptacja <> 1 and old.akceptacja = 1) then
    new.wasdeakcept = 1;
--  if(new.pkoszt <> old.pkoszt and ((new.akceptacja = 1) or (new.akceptacja = 8)))then new.wasdeakcept = 1;
  if(new.akceptacja = 0 and old.akceptacja > 0) then begin
    new.odchylkor = 0;
    select SYMBOLPOAKCEPT from STANTYPFAK where STANTYPFAK.stanfaktyczny = new.stanowisko and STANTYPFAK.typfak = new.typ into :symbolpoakcept;
    if(:symbolpoakcept is null) then symbolpoakcept = 0;
    if(:symbolpoakcept = 1) then begin
       --wycofanie numeru faktury przy deakcpetacji
      if(:local = 1) then begin
        if(old.numer > 0) then begin
          numberg = null;
          select number_gen from REJFAK where old.rejestr = REJFAK.symbol into :numberg;
          if(:numberg is not null and :numberg<>'') then begin
            if(new.zakup = 1) then
              execute procedure free_number(:numberg,old.rejestr,old.typ,old.dataotrz,old.numer,new.numblockget) returning_values new.numblock;
            else
              execute procedure free_number(:numberg,old.rejestr,old.typ,old.data,old.numer,new.numblockget) returning_values new.numblock;
            if(new.numblock > 0) then
              update NUMBERBLOCK set operator = new.operakcept where ref=new.numblock;
            new.numer = -1;
            new.symbol = 'TYM/'||cast(new.ref as varchar(20));
            if(new.fakturarr=1) then new.symbfak = new.symbol;
          end
        end
      end
    end
  end
  if((new.wysylka > 0 and old.wysylka is null) or (new.wysylka <> old.wysylka)) then begin
    --tu tylko mogl operator wskazac wysylki niezamknite - lub replikacja
    select listywys.trasa, listywys.dataplwys from LISTYWYS where ref=new.wysylka and STAN = 'O' into new.trasadost, new.termdost;
  end
  -- BS48370 - Zmiana daty po wycofaniu faktury do poprawy, naruszenie chronologii
  if (new.akceptacja in (9,10) and (old.data <> new.data or old.dataotrz <> new.dataotrz)) then begin
    numberg = null;
    select number_gen from REJFAK where old.rejestr = REJFAK.symbol into :numberg;
    if(new.zakup = 1) then begin
      execute procedure move_number(:numberg,new.rejestr,new.typ,old.dataotrz,new.numer,new.dataotrz);
    end else begin
      execute procedure move_number(:numberg,new.rejestr,new.typ,old.data,new.numer,new.data);
    end
  end
  if(new.akceptacja = 1 and ((old.akceptacja = 0) or (old.akceptacja >= 9))) then begin
    new.numblockget = 0;
    new.wydrukowano = 0;
    --sprawdzenie czy typfak jest OO  czy sprzedaż i istnieje pozfak nie OO
    if(new.zakup = 0 and oo = 1 and exists(select first 1 1 from pozfak p where p.dokument=new.ref and p.gr_vat <> 'OO')) then
      exception universal 'Nie można zaakceptować faktury sprzedaży OO z pozycjami ze stawkami VAT innymi niż OO';
    --sprawdzenie czy typfak jest OO  czy zakup i istnieje pozfak OO
    if(new.zakup = 1 and oo = 1 and exists(select first 1 1 from pozfak p where p.dokument=new.ref and p.gr_vat = 'OO')) then
      exception universal 'Nie można zaakceptować faktury zakupu OO z pozycjami ze stawkami VAT OO';
    --kontrola okresu dokumentu
    if(new.dataotrz is null) then new.dataotrz = new.datasprz;
    if(new.zakup = 0) then begin
      dataokres = new.data;
    --Data odbioru ustawiona na date za X dni od dzisiaj(PR57428)
    --useildniodb mowi o tym czy uzywac tego ustawienia i przeliczac ilosc dni(PR58572)
      select ildniodbpowyd, useildniodb
        from sposdost
        where ref = new.sposdost
        into :ildni,:useildni;
      if(:ildni is not null and :ildni <> -1 and :useildni = 1) then begin
        execute procedure getdate_afterworkdays(:ildni,new.datasprz)
        returning_values new.dataodbioru;
      end
    end else begin
      vptransdate = null;
      select first 1 ptransdate --sprawdzam czy istnieje schemat dekretacji tego nagfaka
        from doctobkdocs
        where param1 = new.rejestr
          and param2 = new.typ
          and company = new.company
          and typ = 0
        into :vptransdate;
      if (vptransdate=0) then --jezeli istnieje sprawdzam wedlug jakiego okresu ma byc ksiegowane
        dataokres = new.data; --data sprzedazy
      else if (:vptransdate = 1) then
        dataokres = new.datasprz;
      else
        dataokres = new.dataotrz; -- data otrzymania
    --Data odbioru ustawiona na date sprzedazy(PR57428)
      new.dataodbioru = new.datasprz;
    end
    lokres = cast( extract( year from :dataokres) as char(4));
    if(extract(month from :dataokres) < 10) then
      lokres = :lokres ||'0' ||cast(extract(month from :dataokres) as char(1));
    else begin
      lokres = :lokres ||cast(extract(month from :dataokres) as char(2));
    end
    if((new.okres is null) or (new.okres = '      ')) then
      new.okres = :lokres;
    else if(new.okres <> :lokres) then
      exception NAGFAK_DATA_POZA_OKRES;

    --kontrola ilosci pozycji
    cnt = null;
    select count(*) from POZFAK where DOKUMENT=new.ref into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then exception NAGFAK_BEZPOZYCJI;
    --kontrola symbolu i numeru
    if((new.numer is null) or (new.numer <= 0)) then begin
       select number_gen from rejfak where new.rejestr = rejfak.symbol into :numberg;
       new.oldnumblock = new.numblock;
       if(new.zakup = 1) then
         execute procedure get_number(:numberg,new.rejestr,new.typ,new.dataotrz, new.numblock, new.ref) returning_values new.numer, new.symbol;
       else begin
         execute procedure get_number(:numberg,new.rejestr,new.typ,new.data, new.numblock, new.ref) returning_values new.numer, new.symbol;
       end
       new.numblock = null;
       new.oldsymbol = new.numer || ';' || new.symbol;
    end
    if(new.fakturarr=1) then new.symbfak = new.symbol;
    --kontrola rozliczenia faktur zaliczkowych
    sumfakzal = 0;
    select sum(wartbru) from nagfakzal where faktura=new.ref into :sumfakzal;
    if(:sumfakzal is null) then sumfakzal = 0;
    if(new.sumwartbru-new.psumwartbru>0 and :sumfakzal>new.sumwartbru-new.psumwartbru) then exception NAGFAK_ZALICZKI_ZADUZO;

    if(new.wysylka > 0) then begin
      --nie mozna akceptować z wyslka nie otwarta
      select STAN from LISTYWYS where REF=new.wysylka into :wysstan;
      if(:wysstan <> 'O') then new.wysylka = null;
    end
    if(new.klient > 0) then begin
      cnt = 0;
      select count(*) from KLIENCI where REF=new.klient and klienci.aktywny > 0 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt = 0) then
        exception NAGFAK_EXPT 'Wskazany na dokumencie klient jest zaznaczony jako nieaktywny !';
    end
    if(new.dostawca > 0) then begin
      cnt = 0;
      select count(*) from DOSTAWCY where ref=new.dostawca and dostawcy.aktywny > 0 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt = 0) then
        exception NAGFAK_EXPT 'Wskazany na dokumencie dostawca jest zaznaczony jako nieaktywny !';
    end
    -- naliczanie pol ESUM...
    if(new.zaliczkowy>0) then begin
      new.esumwartbru = new.pesumwartbru + new.kwotazal;
      new.esumwartbruzl = new.esumwartbru*new.kurs;
      if(new.sumwartbru<>0) then begin
        update ROZFAK set ESUMWARTBRU=new.esumwartbru*SUMWARTBRU/new.sumwartbru
        where DOKUMENT=new.ref;
        select sum(ESUMWARTNET), sum(ESUMWARTNETZL) from ROZFAK where DOKUMENT=new.ref
        into new.esumwartnet, new.esumwartnetzl;
      end else begin
        new.esumwartnet = 0;
        new.esumwartnetzl = 0;
      end
    end else begin
      new.esumwartbru = new.sumwartbru;
      new.esumwartnet = new.sumwartnet;
      new.esumwartbruzl = new.sumwartbruzl;
      new.esumwartnetzl = new.sumwartnetzl;
      if(new.sad=0) then begin
        update ROZFAK set ESUMWARTNET=SUMWARTNET, ESUMWARTBRU=SUMWARTBRU,
                          ESUMWARTNETZL=SUMWARTNETZL, ESUMWARTBRUZL=SUMWARTBRUZL,
                          VESUMWARTNETZL=SUMWARTNET*new.vkurs, VESUMWARTBRUZL=SUMWARTBRU*new.vkurs,
                          BLOKADAPRZELICZ=1
        where DOKUMENT=new.ref;
      end else begin
        update ROZFAK set ESUMWARTNET=SUMWARTNET, ESUMWARTBRU=SUMWARTBRU,
                          ESUMWARTNETZL=SUMWARTNETZL, ESUMWARTBRUZL=SUMWARTBRUZL,
                          VESUMWARTNETZL=(select sum(POZSAD.WARTNET-POZSAD.PWARTNET) from POZSAD where POZSAD.FAKTURA=new.ref and POZSAD.GR_VAT=ROZFAK.VAT),
                          VESUMWARTBRUZL=(select sum(POZSAD.WARTNET-POZSAD.PWARTNET+POZSAD.VAT-POZSAD.PVAT) from POZSAD where POZSAD.FAKTURA=new.ref and POZSAD.GR_VAT=ROZFAK.VAT),
                          BLOKADAPRZELICZ=1
        where DOKUMENT=new.ref;
      end
    end
    delete from rozfak r where r.dokument=new.ref
     and coalesce(r.sumwartnet,0)=0 and coalesce(r.sumwartvat,0)=0 and coalesce(r.sumwartbru,0)=0
     and coalesce(r.esumwartnet,0)=0 and coalesce(r.esumwartvat,0)=0 and coalesce(r.esumwartbru,0)=0
     and coalesce(r.psumwartnet,0)=0 and coalesce(r.psumwartvat,0)=0 and coalesce(r.psumwartbru,0)=0
     and coalesce(r.pesumwartnet,0)=0 and coalesce(r.pesumwartvat,0)=0 and coalesce(r.pesumwartbru,0)=0;
  end
  if(new.anulowanie>0 and old.anulowanie = 0) then begin
-- brak możliwosci anulowania zaplaconej faktury gotowkowej
    if(exists(select first 1 1 from RKDOKPOZ where faktura=new.ref and anulowany = 0)) then
      exception NAGFAK_RKDOKPOZ_ANULOWANIE;
    if(new.zaliczkowy>0) then exception NAGFAK_EXPT 'Funkcja anulowania niedostepna dla faktur zaliczkowych';
    new.anulnieobrot = new.nieobrot;
    new.nieobrot =  3;
  end else if(new.anulowanie = 0 and old.anulowanie>0) then begin
  --SN brak mozliwosci odanulowania gdy korekta do dokumentu wczesniej anulowanego
    elsedokanul = 0;
    if(exists(select n.ref from nagfak n where n.korekta = new.ref and n.anulowanie >0)) then
      exception NAGFAKBU0_KORYGANUL;
  --SN brak mozliwosci odanulowania gdy istnieje inna korekta nieanulowana
    if(exists(
      select nk.ref
      from pozfak pc
      left join pozfak pk on(pk.refk = pc.refk)
      left join nagfak nk on (pk.dokument = nk.ref)
      where pc.dokument = new.ref and nk.anulowanie = 0 and nk.ref is not null and pc.refk is not null
    )) then exception NAGFAKBU0_ANUL;
--    if(new.refk is not null) then begin
--      elsedokanul = 1;
--      select anulowanie from NAGFAK n where (n.ref = new.refk) into :elsedokanul;

--      if(elsedokanul <> 0) then exception NAGFAKBU0_KORYGANUL;
--      anulakcept = 0;
--      elsedokanul = 1;
--      select ref, anulowanie from NAGFAK n where (n.refk = new.refk and n.ref <> new.ref) into :anulakcept, :elsedokanul;
--      if(anulakcept <> 0 and elsedokanul = 0) then exception NAGFAKBU0_ANUL;
--    end
  ---
    new.nieobrot = new.anulnieobrot;
  end
  --adres dostawy
  if(new.akceptacja = 0 and ((new.klient <> old.klient) or (new.dostawca <> old.dostawca))) then begin
    if(new.dulica is null or (new.dulica is null)) then begin
      if(new.klient > 0) then
        select KLIENCI.dulica from KLIENCI where ref=new.klient into new.dulica;
      else if (new.dostawca > 0) then
        select dostawcy.ulica from DOSTAWCY where ref=new.dostawca into new.dulica;
    end
    if(new.dmiasto is null or (new.dmiasto is null)) then begin
      if(new.klient > 0) then
        select KLIENCI.dmiasto from KLIENCI where ref=new.klient into new.dmiasto;
      else if (new.dostawca > 0) then
        select dostawcy.miasto from DOSTAWCY where ref=new.dostawca into new.dmiasto;
    end
  end
  if(new.platnik is null or
     (new.klient <> old.klient and (new.platnik is null or (new.platnik = 0)))
  ) then
     new.platnik = new.klient;
  if(new.symbfak is null) then new.symbfak = '';
  if(new.zakup = 1 and new.akceptacja = 1 and old.akceptacja = 1 and (new.symbfak <> old.symbfak)) then
    exception NAGFAK_SYMBFAK_CHANGED;
  if(new.zakup = 0 and new.akceptacja = 1 and old.akceptacja = 1 and (new.symbol <> old.symbol)) then
    exception NAGFAK_SYMBFAK_CHANGED;
  if(new.sposzap is not null) then select FAKTORING from PLATNOSCI where REF=new.sposzap into new.faktoring;
  if(new.slobankacc <> old.slobankacc or (new.slobankacc is not null and old.slobankacc is null) or ((new.dostawca <> old.dostawca) or (new.dostawca is not null and old.dostawca is null))) then begin
    cnt = null;
select count(*) from bankaccounts where DICTDEF = 6 and DICTPOS = new.dostawca and ref=new.slobankacc and act=1 into :cnt;
    if(:cnt is null or (:cnt = 0))then
      new.slobankacc = null;
  end
  --obliczenie kosztu transportu dla dokumentu
  if (new.anulowanie = 0) then begin
    select typfak.korekta from typfak
    join nagfak on (nagfak.typ=typfak.symbol)
       where nagfak.ref = new.ref into  :kor;
    if (((new.waga<>old.waga) or
         (new.objetosc<>old.objetosc) or
         (new.sposdost<>old.sposdost) or
         (new.sposzap<>old.sposzap) or
         (new.sumwartnetzl-new.psumwartnetzl<>old.sumwartnetzl-old.psumwartnetzl) or
         (new.sumwartbruzl-new.psumwartbruzl<>old.sumwartbruzl-old.psumwartbruzl) or
         (new.dkodp<>old.dkodp) or
         (new.flagisped<>old.flagisped) or
         (new.sposdostauto<>old.sposdostauto) or
         (new.termdost<>old.termdost) or
         (new.data<>old.data) or
         (new.iloscpalet<>old.iloscpalet) or
         (new.iloscpaczek<>old.iloscpaczek)
        ) and (new.akceptacja=0) and (new.zakup=0) and (:kor=0)) then begin
      if(new.termdost is null) then termdost = new.data; else termdost = new.termdost;
      select koszt, optyspos, kods, iloscpal, ilosckart from GET_KOSZTDOST(new.sposdost, new.sposzap, new.waga, new.objetosc, new.sumwartnetzl-new.psumwartnetzl,new.sumwartbruzl-new.psumwartbruzl,new.dkodp,new.flagisped,new.sposdostauto,NULL,:termdost,NULL, new.iloscpalet, new.iloscpaczek, '')
        into new.kosztdost, new.sposdost, new.kodsped, new.iloscpalet, new.iloscpaczek;
      if(new.sposdost = 0) then new.sposdost = null;
    end
  end
  if(new.walutowa = 1 and (new.data<> old.data or new.waluta<>old.waluta or new.tabkurs <> old.tabkurs )) then begin
    if(coalesce(new.tabkurs,0) = 0) then
    begin
      tabkursuzywaj = 0; typkurs = -1;
      select bankdomyslny, tabkursuzywaj, typkurs from stansprzed s where s.stanowisko = new.stanowisko
        into :bankdomyslny, :tabkursuzywaj, :typkurs;
      if(tabkursuzywaj = 1) then begin
        if(:typkurs<0) then exception KURSTYP_BRAK;
        sredni = 0; zakupu = 0; sprzedazy = 0; cnt = null;
        select first 1 t.rref, tp.sredni, tp.zakupu, tp.sprzedazy from xk_get_tabkurs(:bankdomyslny,new.data,null, null) t join tabkursp tp on (t.rref = tp.tabkurs)
        where tp.waluta = new.waluta
        into :cnt, :sredni, :zakupu, :sprzedazy;
        if(:typkurs = 0 and :sredni = 0) then exception KURS_BRAKWARTOSCI;
        else if(:typkurs = 1 and :zakupu = 0) then exception KURS_BRAKWARTOSCI;
        else if(:typkurs = 2 and :sprzedazy = 0) then exception KURS_BRAKWARTOSCI;
        if(:typkurs = 0)then new.kurs = :sredni;
        else if(:typkurs = 1)then new.kurs = :zakupu;
        else if(:typkurs = 2)then new.kurs = :sprzedazy;
        new.tabkurs = :cnt;
      end
    end else begin
       tabkursuzywaj = 0; typkurs = -1;
      select tabkursuzywaj, typkurs from stansprzed s where s.stanowisko = new.stanowisko
        into :tabkursuzywaj, :typkurs;
      if(tabkursuzywaj = 1) then begin
        if(:typkurs<0) then exception KURSTYP_BRAK;
        sredni = 0; zakupu = 0; sprzedazy = 0; cnt = null;
        select tp.sredni, tp.zakupu, tp.sprzedazy from tabkurs t join tabkursp tp on (t.ref = tp.tabkurs)
        where tp.waluta = new.waluta and t.ref = new.tabkurs
        into :sredni, :zakupu, :sprzedazy;
        if(:typkurs = 0 and :sredni = 0) then exception KURS_BRAKWARTOSCI;
        else if(:typkurs = 1 and :zakupu = 0) then exception KURS_BRAKWARTOSCI;
        else if(:typkurs = 2 and :sprzedazy = 0) then exception KURS_BRAKWARTOSCI;
        if(:typkurs = 0)then new.kurs = :sredni;
        else if(:typkurs = 1)then new.kurs = :zakupu;
        else if(:typkurs = 2)then new.kurs = :sprzedazy;
      end
    end
  end
  if(new.krajwysylki ='') then new.krajwysylki = null;
  ---sprawdzenie czy istnieje na pozycji faktury sprzedazy asortyment z niewypelnionym PKWIU (wymagane pkwiu na stawce vat)
  if (new.akceptacja <> old.akceptacja and new.akceptacja = 1 and new.zakup = 0 and new.eksport = 0) then begin
    select first 1 p.ktm from pozfak p join towary t on (t.ktm = p.ktm) join vat v on (v.grupa = p.gr_vat)
      where p.dokument = new.ref and p.opk = 0 and coalesce(v.haspkwiu,0) = 1 and coalesce(t.pkwiu,'') = ''
    into :ktm;
    if(coalesce(:ktm, '') <> '') then exception NAGFAK_EXPT  'Pozycja faktury ' || :ktm ||' musi mieć wypełnione PKWiU.';
  end
end^
SET TERM ; ^
