--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_BD_0 FOR ACCSTRUCTUREDISTS              
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable vslownik integer ;
declare variable vkontoks ANALYTIC_ID;
declare variable vcompany integer;
declare variable vyearid integer;
declare variable vsymbol varchar(3);
begin
  if (old.otable = 'SLOPOZ') then
  begin
    -- pobieram dany slownik i konto zeby sprawdzic czy wyrozniki sa gdzies zaksiegowane
    select slownik, kontoks from slopoz where ref = old.oref into :vslownik, :vkontoks;
    execute procedure check_dict_using_in_accounts(:vslownik, :vkontoks,null);
  end
   if (old.otable = 'BKACCOUNTS') then
  begin
  -- pobieram company, yearid oraz symbol konta zeby sprawdzic czy konto jest otwarte
    select company, yearid, symbol
      from bkaccounts
      where ref = old.oref
      into :vcompany, :vyearid, :vsymbol;

    if (exists(
      select ref
        from accounting
        where yearid = :vyearid and account starting with :vsymbol
          and (company = :vcompany or :vcompany is null)))
      then
        exception bkaccount_opened;
  end
end^
SET TERM ; ^
