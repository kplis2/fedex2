--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_BI_WPK FOR BKACCOUNTS                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable current_company companies_id;
declare variable pattern_bkyear bkyears_id;
declare variable is_pattern smallint_id;
declare variable pattern_company companies_id;
begin
  --Jeżeli dodajemy konto, sprawdzić należy, czy rok księgowy jest synchronizowany
  --z wzorcem. Jak tak, to sprawdzamy, czy konto jest we wzorcu
  pattern_bkyear = null;
  -- czy posiada wzorzec
  select b.patternref
    from bkyears b
    where b.company = new.company
      and b.yearid = new.yearid
    into :pattern_bkyear;
  --szukamy zakladu wzorcowego
  select bk.company
    from bkyears bk
    where bk.ref = :pattern_bkyear
    into :pattern_company;
  if (coalesce(pattern_bkyear,0) > 0) then
  begin
    -- Jeżeli rok ksigowy podlega synchronizacji pod dane company to konta dodawane muszą mieć
    -- jakiego parent refa

    if (new.pattern_ref is not distinct from null) then
      exception brak_parrent_ref_przy_sync;
    --jeżeli konto nie istnieje we wzorcu to wyjątek, pod warunkiem, że nie jest
    -- wzorcowym
    if (not exists(select first 1 1
                            from bkaccounts b
                            where ref = new.pattern_ref
                              and company = :pattern_company)) then
    begin
      exception add_bkacc_out_of_pattern;
    end
  end
end^
SET TERM ; ^
