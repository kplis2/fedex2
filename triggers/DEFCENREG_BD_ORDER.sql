--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENREG_BD_ORDER FOR DEFCENREG                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update defcenreg set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and defcennik = old.defcennik;
end^
SET TERM ; ^
