--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCERTIFS_BI_REF FOR EWORKCERTIFS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EWORKCERTIFS')
      returning_values new.REF;
end^
SET TERM ; ^
