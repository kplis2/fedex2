--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BI_GROUPBYSYMBS FOR MWSACTS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.vers > 0 and new.vers is not null) then
    execute procedure mws_mwsacts_groupby(new.vers)
      returning_values(new.keya, new.keyb, new.keyc, new.keyd);
  if (new.keya is null) then new.keya = '';
  if (new.keyb is null) then new.keyb = '';
  if (new.keyc is null) then new.keyc = '';
  if (new.keyd is null) then new.keyd = '';
end^
SET TERM ; ^
