--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSTYPES_AU0 FOR PRTOOLSTYPES                   
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable countgr integer;
begin
  if (new.verificationnr <> old.verificationnr) then begin
    select count(symbol) from prtools where tooltype = new.symbol
          group by tooltype into :countgr;
    if (new.amount <> 0 and new.amountfree <> 0 and
        (:countgr) > 1) then
        exception prtools_error 'Nie można zmienic weryfikacji nr ser.';
  end
end^
SET TERM ; ^
