--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDIMELEMS_BI_REF FOR FRDIMELEMS                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDIMELEMS')
      returning_values new.REF;
end^
SET TERM ; ^
