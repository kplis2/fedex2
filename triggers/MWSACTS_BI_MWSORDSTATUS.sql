--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BI_MWSORDSTATUS FOR MWSACTS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  select status
    from mwsords
  where ref = new.mwsord
  into new.mwsordstatus;
end^
SET TERM ; ^
