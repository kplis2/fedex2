--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDALGORITHMS_BI_REF FOR EDALGORITHMS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDALGORITHMS')
      returning_values new.REF;
end^
SET TERM ; ^
