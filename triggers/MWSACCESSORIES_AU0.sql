--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACCESSORIES_AU0 FOR MWSACCESSORIES                 
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable constlocref integer;
begin
  -- zalozenie lokacji stalej dla akcesoriów magazynowej
  if (new.mwsconstloc is null and new.mwsconstloctype is not null and old.mwsconstloctype is null) then
  begin
    execute procedure gen_ref('MWSCONSTLOCS') returning_values constlocref;
    if (constlocref is not null) then
    begin
      insert into mwsconstlocs (ref, symbol, wh, mwsconstloctype, stocktaking, goodssellav, l, w, h, volume)
        values(:constlocref,new.symbol||'/'||new.ref, new.wh, new.mwsconstloctype, 0, 0, 90, 130, 200, 2.34);
      update mwsaccessories set mwsconstloc = :constlocref where ref = new.ref;
    end
    else
      exception MWSACCESSORY_MWSCONSTLOC_NOTDEF;
  end
end^
SET TERM ; ^
