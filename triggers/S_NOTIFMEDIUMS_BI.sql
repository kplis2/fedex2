--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_NOTIFMEDIUMS_BI FOR S_NOTIFMEDIUMS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  IF (NEW.REF IS NULL) THEN
      NEW.REF = GEN_ID(GEN_S_NOTIFMEDIUMS,1);
  select SYMBOL from S_DEFMEDIUMS where (REF=new.defmedium) into new.symbol;
end^
SET TERM ; ^
