--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILADR_AU_KONTAKT FOR EMAILADR                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  execute procedure WYREJESTRUJ_KONTAKT(old.ref);
  execute procedure REJESTRUJ_KONTAKT(new.ref,new.emailwiad,new.cpodmiot,new.pkosoba);
end^
SET TERM ; ^
