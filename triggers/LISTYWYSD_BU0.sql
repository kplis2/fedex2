--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_BU0 FOR LISTYWYSD                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable psposdost integer;
declare variable klient integer;
declare variable sposzap integer;
declare variable sposzaps varchar(100);
declare variable LOKRES VARCHAR(6);
declare variable dataokres timestamp;
declare variable spedytor varchar(80);
declare variable sumwartnetgr numeric(14,2);
declare variable sumwartbrugr numeric(14,2);
begin
  if(new.sellmistake is null) then new.sellmistake = 1;
  if(new.acceptmistake is null) then new.acceptmistake = 1;
  if(new.checkmistake is null) then new.checkmistake = 1;
  if(new.waga is null) then new.waga = 0;
  if(new.objetosc is null) then new.objetosc = 0;
  if(new.symbolsped is null) then new.symbolsped = '';
  if (
      ((new.waga<>old.waga) or (new.sposdost<>old.sposdost) or (new.objetosc <> old.objetosc) or
       (new.kosztdostbez =0 and old.kosztdostbez = 1) or (new.kosztdost = 0) or
       (new.kodp <> old.kodp) or (new.kodp is not null and old.kodp is null) or
       (new.przebieg<>old.przebieg) or (new.przebieg is not null and old.przebieg is null) or
       (new.przebieg is null and old.przebieg is not null)
      ) and (new.akcept < 2) and (new.kosztdostbez = 0)
  ) then begin
    if(new.odbiorcaid > 0) then
      select KLIENT from ODBIORCY where REF=new.odbiorcaid into :klient;
    else begin
      select max(DOKUMNAG.KLIENT) from br_listywysd_dok(new.ref) left join DOKUMNAG on (DOKUMNAG.REF = BR_LISTYWYSD_DOK.REF) where BR_LISTYWYSD_DOK.TYP='M' into :klient;
    end
    if(:klient is null) then
      select max(KLIENT) from NAGFAK where LISTYWYSD = new.ref into :klient;

    select max(nagfak.sposzap) from NAGFAK where LISTYWYSD = new.ref into :sposzap;
    if(:sposzap is null) then
      select max(dokumnag.sposzap) from br_listywysd_dok(new.ref) left join DOKUMNAG on (DOKUMNAG.REF = br_listywysd_dok.ref) where br_listywysd_dok.TYP='M' into :sposzap;
    if(:sposzap is null and :klient is not null) then
      select klienci.sposplat from KLIENCI where ref=:klient into :sposzap;
    if(:sposzap is null) then
      execute procedure get_config('SPOSZAP',2) returning_values :sposzaps;
    if (sposzaps is not null and sposzaps <> '') then
      sposzap = cast(sposzaps as integer);
    spedytor = NULL;
    if(new.listawys is not null) then
      select spedytor from listywys where ref=new.listawys into :spedytor;
    select
      sum(cast(dokumpoz.cenanet * listywysdpoz.ilosc * (1-defdokum.koryg*2) as numeric(14,2))),
      sum(cast(dokumpoz.cenabru * listywysdpoz.ilosc * (1-defdokum.koryg*2) as numeric(14,2)))
      from listywysdpoz
      join dokumpoz on (listywysdpoz.dokpoz=dokumpoz.ref)
      join dokumnag on (dokumnag.ref=dokumpoz.dokument)
      left join defdokum on (defdokum.symbol=dokumnag.typ)
      where listywysdpoz.dokument = new.ref
      into :sumwartnetgr, :sumwartbrugr;
    if(:sumwartnetgr is null) then sumwartnetgr = 0;
    if(:sumwartbrugr is null) then sumwartbrugr = 0;
    select koszt, optyspos, kods from GET_KOSZTDOST(new.sposdost,:sposzap,new.waga,new.objetosc,:sumwartnetgr,:sumwartbrugr,new.kodp,new.flagisped,new.sposdostauto,:spedytor,new.termdost,NULL,new.iloscpalet,new.iloscpaczek,'')
    into new.kosztdost, :psposdost, new.kodsped;
  end
  if(old.akcept = 0 and new.akcept > 0) then
    new.dataakc = current_time;
  if(old.akcept < 2 and new.akcept = 2) then
    new.datazam = current_time;
  if(new.akcept = 0 and old.akcept > 0) then
    new.dataakc = null;
  if(new.akcept < 2 and old.akcept = 2) then
    new.datazam = null;
  if(new.listawys is null and old.listawys is not null) then
    new.wyschecked = 0;
  if(new.termdost is null or (new.termdost = '1899-12-30') ) then new.termdost = current_date;
  if(new.okres is null or (new.okres = '') or (new.okres='      ') or(new.datazam <> old.datazam)) then begin
    dataokres = new.datazam;
    if(:dataokres is null) then
    begin
      if(new.dataakc is null) then
        dataokres = current_date;
      else
        dataokres = new.dataakc;
    end
    LOKRES = cast( extract( year from :dataokres) as char(4));
    if(extract(month from :dataokres) < 10) then
       LOKRES = :LOKRES ||'0' ||cast(extract(month from :dataokres) as char(1));
    else
       LOKRES = :LOKRES ||cast(extract(month from :dataokres) as char(2));
    new.okres = :lokres;
  end
  if(coalesce(new.kontrahent,'')<>coalesce(old.kontrahent,'') or (new.adrtyp<>old.adrtyp)) then begin
    if(new.adrtyp='F') then new.fromplace = substring(new.kontrahent from 1 for 20);
    if(new.adrtyp='T') then new.toplace = substring(new.kontrahent from 1 for 20);
  end
end^
SET TERM ; ^
