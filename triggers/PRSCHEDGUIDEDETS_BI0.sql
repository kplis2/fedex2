--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_BI0 FOR PRSCHEDGUIDEDETS               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ssource = 3) then new.accept = 2;
  -- zródla pochodzenia rozpisek
  if (new.stable is null) then new.stable = '';
  if (new.ssource = 0) then
    new.stable = 'DOKUMNAG';
  else if (new.ssource = 1) then
    new.stable = 'PRSCHEDGUIDEDETS';
  else if (new.ssource = 2) then
    new.stable = 'POZZAM';
  if (new.ssource = 3) then
    new.stable = 'OTHER';
  if (new.prdepart is null) then
    select first 1 prdepart
      from prschedguides
      where ref = new.prschedguide
    into new.prdepart;
end^
SET TERM ; ^
