--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCS_BI0 FOR PRTOOLSDOCS                    
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  if (new.regdate is null) then new.regdate = current_timestamp(0);
  if (new.docdate is null) then new.docdate = current_date;
  if (new.akcept is null) then new.akcept = 0;
  if (new.regoperator is null) then
    execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoperator;
  if (new.number is null) then
  begin
    new.number = -1;
    new.symbol = 'TYM/'||cast(new.ref as varchar(20));
  end
  if (coalesce(new.period,'') = '') then
    select d.okres
      from datatookres(cast(new.docdate as date),null,null,null,1) d
      into new.period;
end^
SET TERM ; ^
