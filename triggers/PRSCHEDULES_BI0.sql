--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDULES_BI0 FOR PRSCHEDULES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRSCHEDULES')
      returning_values new.REF;
  if(exists(select ref from prschedules
    where prdepart = new.prdepart and prschedgroup = new.prschedgroup)
  ) then exception prschedules_error substring('Istnieje już harmonogram grupy: '||new.prschedgroup||' na wydziale: '||new.prdepart from 1 for 70);
  if (new.main is null) then
    select main from prschedgroups where symbol = new.prschedgroup into new.main;
  if(new.main is null) then new.main = 0;
end^
SET TERM ; ^
