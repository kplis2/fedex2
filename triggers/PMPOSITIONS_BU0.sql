--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_BU0 FOR PMPOSITIONS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.pmelement<>old.pmelement) then
    select PMPLAN from PMELEMENTS where REF=new.pmelement into new.pmplan;
  if(new.ktm='') then new.ktm = null;

end^
SET TERM ; ^
