--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_AU_PRODTRANSFER_UP FOR PROPERSRAPS                    
  ACTIVE AFTER UPDATE POSITION 2 
AS
begin
  if (coalesce(new.amount,0) > coalesce(old.amount,0)) then
  begin
    -- przekazanie wyrobu gotowego jako surowiec na inny przewodnik
    if (exists(select first 1 1 from prschedguides where ref = new.prschedguide and prodtransfer = 1)) then
      execute procedure prod_transfer(new.ref,1);
  end
end^
SET TERM ; ^
