--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTCEN_AD0 FOR DOSTCEN                        
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable dostawca integer;
declare variable waluta varchar(10);
declare variable oddzial varchar(20);
begin
  if(old.gl = 1) then begin
    -- jako glownego dostawce ustaw innego przypadkowego
    dostawca = null;
    waluta = null;
    oddzial = null;
    select first 1 DOSTAWCA, WALUTA, ODDZIAL from DOSTCEN where WERSJAREF = old.wersjaref
      into :dostawca, :waluta, :oddzial;
    if(:dostawca is not null) then
      update DOSTCEN set GL  = 1 where WERSJAREF = old.wersjaref and DOSTAWCA = :dostawca and WALUTA = :waluta and ODDZIAL = :oddzial;
  end
  if(old.cenazakgl = 1) then begin
    -- jako glowna cene ustaw inna przypadkowa ale w tej samej walucie i tym samym oddziale
    dostawca = null;
    select first 1 DOSTAWCA from DOSTCEN where WERSJAREF = old.wersjaref and WALUTA = old.waluta and ODDZIAL = old.oddzial into :dostawca;
    if(:dostawca is not null) then begin
      update DOSTCEN set CENAZAKGL  = 1 where WERSJAREF = old.wersjaref and DOSTAWCA = :dostawca and WALUTA = old.waluta and ODDZIAL = old.oddzial;
    end else begin
    -- jak sie nie da to ustaw cene przypadkkowego innego oddzialu ale w tej samej walucie
      select first 1 DOSTAWCA,ODDZIAL from DOSTCEN where WERSJAREF = old.wersjaref and WALUTA = old.waluta into :dostawca,:oddzial;
      if(:dostawca is not null) then begin
        update DOSTCEN set CENAZAKGL  = 1 where WERSJAREF = old.wersjaref and DOSTAWCA = :dostawca and WALUTA = old.waluta and ODDZIAL = :oddzial;
      end else begin
      -- jak sie nie da to ustaw cene przypadkowa
        select first 1 DOSTAWCA,WALUTA,ODDZIAL from DOSTCEN where WERSJAREF = old.wersjaref into :dostawca,:waluta,:oddzial;
        if(:dostawca is not null) then begin
          update DOSTCEN set CENAZAKGL  = 1 where WERSJAREF = old.wersjaref and DOSTAWCA = :dostawca and WALUTA = :waluta and ODDZIAL = :oddzial;
        end
      end
    end
  end
  if(old.akt = 1) then begin
    -- ustaw znacznik akt na dowolnym innym rekordzie tego dostawcy i towaru
    waluta = null;
    oddzial = null;
    select first 1 WALUTA,ODDZIAL from DOSTCEN where WERSJAREF = old.wersjaref and DOSTAWCA = old.dostawca
    into :waluta, :oddzial;
    if(:waluta is not null) then
      update DOSTCEN set AKT=1 where WERSJAREF=old.wersjaref and DOSTAWCA = old.dostawca and WALUTA = :waluta and ODDZIAL = :oddzial;
  end
end^
SET TERM ; ^
