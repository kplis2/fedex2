--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLPOSDEFS_BI_REF FOR EDECLPOSDEFS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDECLPOSDEFS')
      returning_values new.REF;
end^
SET TERM ; ^
