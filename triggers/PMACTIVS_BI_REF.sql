--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMACTIVS_BI_REF FOR PMACTIVS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('PMACTIVS') returning_values new.REF;
end^
SET TERM ; ^
