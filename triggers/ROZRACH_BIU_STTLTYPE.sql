--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BIU_STTLTYPE FOR ROZRACH                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
AS
  declare variable sttltype char(1);
begin
  if (new.fsopertype is not null) then
    select sttltype from fsopertypes where number = new.fsopertype  into :sttltype;
  if (sttltype is not null) then new.typ = sttltype;
  if (new.typ is null or new.typ = ' ') then
  begin
    if (new.baldebit <> 0) then new.typ = 'N';
    if (new.balcredit <> 0) then new.typ = 'Z';
  end
  -- obsluga blokady rozrachunku do przelewow
  if (new.blokada is null) then new.blokada=0;
  if (new.blokada=0) then new.opisblokady='';
end^
SET TERM ; ^
