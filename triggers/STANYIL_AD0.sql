--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_AD0 FOR STANYIL                        
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable magmaster varchar(3);
declare variable oddzialtowary varchar(10);
declare variable ilospotkplall numeric(14,4);
begin
  if(old.ilosc <> 0) then begin
    select MAGMASTER from DEFMAGAZ where symbol = old.magazyn into :magmaster;
    if(:magmaster <> '') then
      update STANYIL set ILOSCPOD = ILOSCPOD  - (old.ilosc+old.iloscpod) where WERSJAREF = old.wersjaref and MAGAZYN = :magmaster;
  end
  if(old.ilosc <> 0) then begin
    execute procedure getconfig('STANYMAGONTOWARY') returning_values :oddzialtowary;
    if(old.oddzial = :oddzialtowary) then begin
      update TOWARY set TOWARY.iloscwoddziale = ILOSCWODDZIALE - old.ilosc where KTM = old.ktm;
    end
  end
  if (old.ilosc - old.zablokow > 0) then
    execute procedure CALCULATE_KOMPLETY_POT(old.wersjaref,old.magazyn,old.usluga);
  if (old.usluga = 2 and old.iloscpot > 0) then
  begin
    select sum(stanyil.iloscpot) from stanyil where stanyil.wersjaref = old.wersjaref
      into :ilospotkplall;
    if (:ilospotkplall is null) then
      ilospotkplall = 0;
    update wersje set wersje.ilospotkplall = :ilospotkplall where wersje.ref = old.wersjaref;
  end
end^
SET TERM ; ^
