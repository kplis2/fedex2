--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWARY_BU_REPLICAT FOR TOWARY                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.KTM<>old.KTM or
      new.sww <> old.sww or (new.sww is null and old.sww is not null) or (new.sww is not null and old.sww is null) or
      new.pkwiu <> old.pkwiu or (new.pkwiu is null and old.pkwiu is not null) or (new.pkwiu is not null and old.pkwiu is null) or
      new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null) or
      new.opispod <> old.opispod or (new.opispod is null and old.opispod is not null) or (new.opispod is not null and old.opispod is null) or
      new.opisroz <> old.opisroz or (new.opisroz is null and old.opisroz is not null) or (new.opisroz is not null and old.opisroz is null) or
      new.miara <> old.miara or (new.miara is null and old.miara is not null) or (new.miara is not null and old.miara is null) or
      new.akt <> old.akt or (new.akt is null and old.akt is not null) or (new.akt is not null and old.akt is null) or
      new.grupa <> old.grupa or (new.grupa is null and old.grupa is not null) or (new.grupa is not null and old.grupa is null) or
      new.usluga <> old.usluga or (new.usluga is null and old.usluga is not null) or (new.usluga is not null and old.usluga is null) or
      new.serial <> old.serial or (new.serial is null and old.serial is not null) or (new.serial is not null and old.serial is null) or
      new.dniwazn <> old.dniwazn or (new.dniwazn is null and old.dniwazn is not null) or (new.dniwazn is not null and old.dniwazn is null) or
      new.iloscwopak <> old.iloscwopak or (new.iloscwopak is null and old.iloscwopak is not null) or (new.iloscwopak is not null and old.iloscwopak is null) or
      new.marzamin <> old.marzamin or (new.marzamin is null and old.marzamin is not null) or (new.marzamin is not null and old.marzamin is null) or
      new.vat <> old.vat or (new.vat is null and old.vat is not null) or (new.vat is not null and old.vat is null) or
      new.vat2 <> old.vat2 or (new.vat2 is null and old.vat2 is not null) or (new.vat2 is not null and old.vat2 is null) or
      new.symbol_dost <> old.symbol_dost or (new.symbol_dost is null and old.symbol_dost is not null) or (new.symbol_dost is not null and old.symbol_dost is null) or
      new.witryna<>old.witryna or (new.witryna is null and old.witryna is not null) or (new.witryna is not null and old.witryna is null)) then
   waschange = 1;

  execute procedure rp_trigger_bu('TOWARY',old.ktm, null, null, null, null, old.token, old.state,
        new.ktm, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
