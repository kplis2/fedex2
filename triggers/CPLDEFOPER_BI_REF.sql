--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLDEFOPER_BI_REF FOR CPLDEFOPER                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPLDEFOPER')
      returning_values new.REF;
end^
SET TERM ; ^
