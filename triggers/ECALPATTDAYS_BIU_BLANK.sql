--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALPATTDAYS_BIU_BLANK FOR ECALPATTDAYS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
  declare variable zero_time varchar(5);
begin
  zero_time = '00:00';

  if(new.workstart is null) then new.workstart = zero_time;
  if(new.workend is null) then new.workend = zero_time;

  new.pyear = cast(extract(year from new.pdate) as integer);
  new.pmonth = cast(extract(month from new.pdate) as smallint);
  new.pday = cast(extract(day from new.pdate) as smallint);
  new.dayofweek = cast( extract( weekday from new.pdate) as smallint);

  new.workstartstr = substring(new.workstart from 1 for 5);
  new.workendstr = substring(new.workend from 1 for 5);
  if (new.workstartstr containing zero_time and new.daykind <> 1) then
    new.workstartstr = '';
  if (new.workendstr containing zero_time and new.daykind <> 1) then
    new.workendstr = '';

  new.workstartstr2 = substring(new.workstart2 from 1 for 5);
  new.workendstr2 = substring(new.workend2 from 1 for 5);
  if (new.workstartstr2 containing zero_time and new.daykind <> 1) then
    new.workstartstr2 = '';
  if (new.workendstr2 containing zero_time and new.daykind <> 1) then
    new.workendstr2 = '';

  --wyliczenie czasu pracy
  execute procedure count_worktime(new.workstart,new.workend,new.workstart2,new.workend2,new.daykind)
    returning_values new.worktime;
  execute procedure durationstr2(new.worktime) returning_values new.worktimestr;

  if (new.pattkind = 1) then
  begin
    if (exists(select first 1 1 from ecalpattdays
                 where calendar = new.calendar
                   and pattkind = 1
                   and pyear = new.pyear
                   and dayofweek = new.dayofweek
                   and ref <> new.ref)
    ) then
      exception doubled_day;
 end
end^
SET TERM ; ^
