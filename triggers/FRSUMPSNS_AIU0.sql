--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRSUMPSNS_AIU0 FOR FRSUMPSNS                      
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
as
begin
  if (new.sumpsn <> new.frpsn and new.frversion = 0) then
  begin
    update frpsns set countord = 0 where ref = new.frpsn and algorithm = 2;
  end
end^
SET TERM ; ^
