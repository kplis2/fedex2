--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCPOS_BD0 FOR PRTOOLSDOCPOS                  
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (exists (select first 1 1 from prtoolsdocs d where d.ref = old.prtoolsdoc and d.akcept <> 0)) then
    exception prtoolsdocs_error 'Nie można usuwac pozycji na zaakceptowanym dokumentcie';
end^
SET TERM ; ^
