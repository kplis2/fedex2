--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STKRMAG_BI0 FOR STKRMAG                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.priorytet is null) then new.priorytet = 0;
  if(new.priorytet < 0) then exception STKRMAG_PRIORYTETTOSMALL;
  new.lastchange = current_time;
end^
SET TERM ; ^
