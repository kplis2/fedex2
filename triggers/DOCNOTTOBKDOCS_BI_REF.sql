--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOCNOTTOBKDOCS_BI_REF FOR DOCNOTTOBKDOCS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DOCNOTTOBKDOCS')
      returning_values new.REF;
end^
SET TERM ; ^
