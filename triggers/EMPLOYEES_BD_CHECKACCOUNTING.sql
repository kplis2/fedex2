--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYEES_BD_CHECKACCOUNTING FOR EMPLOYEES                      
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable dictdef integer;
begin
  select ref from slodef where typ = 'EMPLOYEES'
    into :dictdef;
  execute procedure check_dict_using_in_accounts(dictdef, old.symbol, old.company);
end^
SET TERM ; ^
