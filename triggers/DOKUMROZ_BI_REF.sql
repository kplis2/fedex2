--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMROZ_BI_REF FOR DOKUMROZ                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DOKUMROZ')
      returning_values new.REF;
end^
SET TERM ; ^
