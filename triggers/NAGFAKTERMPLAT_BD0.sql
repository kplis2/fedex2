--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKTERMPLAT_BD0 FOR NAGFAKTERMPLAT                 
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable akc smallint;
begin
  if(old.doktyp='F') then begin
    select n.akceptacja from nagfak n where n.ref = old.dokref into :akc;
    if (:akc = 1) then exception NAGFAK_AKCEPT 'Modyfikacja tabeli spłat dla zaakceptowanej faktury niemożliwa';
  end else if(old.doktyp='M') then begin
    select n.akcept from dokumnag n where n.ref = old.dokref into :akc;
    if (:akc = 1) then exception NAGFAK_AKCEPT 'Modyfikacja tabeli spłat dla zaakceptowanego dokumentu niemożliwa';
  end
end^
SET TERM ; ^
