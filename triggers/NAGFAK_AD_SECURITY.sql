--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AD_SECURITY FOR NAGFAK                         
  ACTIVE AFTER DELETE POSITION 5 
AS
declare variable opis varchar(1024);
declare variable skrot varchar(255);
begin
  if (old.akceptacja = 0 and old.wasdeakcept = 1 and old.numer = -1) then begin
    if (old.zakup = 0) then
      select fskrot from klienci where ref = old.klient into :skrot;
    else
      select id from dostawcy where ref = old.dostawca into :skrot;
    if (:skrot is null) then skrot = '';
    opis = old.oldsymbol || '; ' || old.stanowisko || '; ' || old.rejestr || '; ' || :skrot || '; ' || old.data || '; ' || old.wartnet;
    insert into security_log(logfil, tablename, item, opis)
      values('NAGFAK', 'NAGFAK', 'DELETE', :opis);
  end
end^
SET TERM ; ^
