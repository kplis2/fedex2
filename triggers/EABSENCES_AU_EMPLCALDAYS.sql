--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AU_EMPLCALDAYS FOR EABSENCES                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.fromdate <> old.fromdate or new.todate <> old.todate
    or new.employee <> old.employee or new.ref <> old.ref
    or old.correction <> new.correction  -- BS55316
  )then begin
    update emplcaldays set absence = null, absencediff = null
      where employee = old.employee
        and cdate between old.fromdate and old.todate
        and absence is not null;       -- BS55316

    if (new.correction <> 1) then        -- BS55316
      update emplcaldays set absence = new.ref, absencediff = null
        where employee = new.employee and cdate between new.fromdate and new.todate;
  end
end^
SET TERM ; ^
