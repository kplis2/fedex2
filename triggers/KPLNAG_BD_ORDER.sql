--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLNAG_BD_ORDER FOR KPLNAG                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update kplnag set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and ktm = old.ktm;
end^
SET TERM ; ^
