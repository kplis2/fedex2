--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SEGREGATOR_AU0 FOR SEGREGATOR                     
  ACTIVE AFTER UPDATE POSITION 1 
as
begin
  if(new.lista <> old.lista) then
     update SEGREGATOR set LISTA = NULL where RODZIC = new.ref;
  if(coalesce(new.rodzic,0) <> coalesce(old.rodzic,0)) then
    execute procedure DOKPLIKSEGREGATORY_REFRESH2(new.REF);
end^
SET TERM ; ^
