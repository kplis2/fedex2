--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMAT_BU FOR PRSHMAT                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable reported smallint;
begin
  if (new.stopcascade < 0 or new.stopcascade is null) then new.stopcascade = 0;
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if (new.opermaster is null or new.opermaster = 0)
    then exception prsheets_data 'Definiowanie surowca dozwolone wyłącznie dla operacji.';
  new.egrossquantity = replace(new.egrossquantity,',','.');
  new.enetquantity = replace(new.enetquantity,',','.');
  new.econdition = replace(new.econdition,',','.');
  if (new.autodocout > 0 and old.autodocout = 0) then
  begin
    select o.reported
      from prshopers o
      where o.ref = new.opermaster
      into reported;
    if (reported is null) then reported = 0;
    if (reported = 0) then
      exception prsheets_data 'Autowydawanie surowców tylko dla operacji raportowanych';
  end
end^
SET TERM ; ^
