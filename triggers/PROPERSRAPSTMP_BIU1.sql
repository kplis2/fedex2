--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPSTMP_BIU1 FOR PROPERSRAPSTMP                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (new.regtime is null) then
    new.regtime = current_timestamp(0);
  if (new.amount is null) then new.amount = 0;
  if (new.amountshortages is null) then new.amountshortages = 0;
  if (inserting or (updating and new.amountshortages > 0 and coalesce(new.prshortagetype,'') = '')) then
    select d.prshortagestypedefault
      from prdeparts d
      where d.symbol = new.prdepart
      into new.prshortagetype;
  if (new.wersjaref is null and new.ktm is not null and new.wersja is not null) then
    select ref from wersje where ktm = new.ktm and nrwersji = new.wersja
      into new.wersjaref;
end^
SET TERM ; ^
