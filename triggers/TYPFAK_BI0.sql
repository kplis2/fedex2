--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPFAK_BI0 FOR TYPFAK                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.korekta is null) then new.korekta = 0;
  if(new.sad is null) then new.sad = 0;
  if(new.fiskalny is null) then new.fiskalny = 0;
  if(new.walutowy is null) then new.walutowy = 0;
  if(new.korekta = 0) then new.korwart = 0;
  if(new.danedod = '') then new.danedod = null;
  if(new.intrastat is null) then new.intrastat = 0;
  if (new.korsamo is null) then new.korsamo = 0;
  if(new.dozaplatynetto is null) then new.dozaplatynetto = 0;
  if (new.drukbezvat is null) then new.drukbezvat = 0;
end^
SET TERM ; ^
