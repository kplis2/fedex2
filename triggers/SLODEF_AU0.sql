--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_AU0 FOR SLODEF                         
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.kodln<>old.kodln) then
    update accstructure set len=new.kodln
    where dictdef = new.ref;
  if(new.nazwa<>old.nazwa) then
    update cpodmioty set slodefnazwa=new.nazwa
    where slodef=new.ref;
end^
SET TERM ; ^
