--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_AU0 FOR PRSHOPERS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable countprshwork integer;
begin
   -- exception test_break 'X'||coalesce(new.opertimeupdate,0);
  if((coalesce(new.workplace, '') <> coalesce(old.workplace,'') or
     coalesce(new.opertimepf, 0) <> coalesce(old.opertimepf ,0) or
     coalesce(new.opertime, 0) <> coalesce(old.opertime ,0) or
     coalesce(new.prmachine,0) <> coalesce(old.prmachine,0))
     -- przeciwdzialanie zapetlaniu triggerów
     and coalesce(new.opertimeupdate,0) = 0
     ) then
  begin
    select count(*)
      from  prshopersworkplaces p
    where p.prshoper = new.ref
      and p.main = 1
    into countprshwork ;

     /*
      update prshopersworkplaces
        set prshopersworkplaces.opertimeupdate = 1
      where prshopersworkplaces.prshoper = new.ref
        and prshopersworkplaces.main = 1;
     */
    if(coalesce(countprshwork,0) > 0) then
    begin
      update prshopersworkplaces p
        set p.prworkplace = new.workplace,
            p.prmachine = new.prmachine,
            p.opertime = new.opertime,
            p.opertimepf = new.opertimepf,
            p.opertimeupdate = 1
      where p.prshoper = new.ref
        and p.main = 1;

      update prshopersworkplaces p
        set p.opertimeupdate = 0
      where p.prshoper = new.ref
        and p.main = 1;
    end else
    begin -- w przypadku gdy nie istnieje  stanowisko gówne wstawić to z operacji jesli nie spelnia wrunki aby nim być
      if(coalesce(new.opertimeupdate,0) = 0 and coalesce(new.workplace,'')<> '' ) then
      BEGIN
        INSERT INTO PRSHOPERSWORKPLACES (PRSHOPER, PRWORKPLACE, PRMACHINE, OPERTIMEPF, OPERTIME, MAIN, opertimeupdate)
        VALUES (new.ref, new.workplace, new.prmachine,coalesce(new.opertimepf,0), coalesce(new.opertime,0), 1,1 );

        update PRSHOPERSWORKPLACES p
          set p.opertimeupdate = 0
        where p.prshoper = new.REF
          and P.main = 1;
      END
    end
  end

  if((new.econdition <> old.econdition) or (new.econdition is not null and old.econdition is null) or (new.econdition is null and old.econdition is not null)
    or (new.eoperfactor <> old.eoperfactor) or (new.eoperfactor is not null and old.eoperfactor is null) or (new.eoperfactor is null and old.eoperfactor is not null)
    or (new.eopertime <> old.eopertime) or (new.eopertime is not null and old.eopertime is null) or (new.eopertime is null and old.eopertime is not null)
    or (new.eplacetime <> old.eplacetime) or (new.eplacetime is not null and old.eplacetime is null) or (new.eplacetime is null and old.eplacetime is not null)
  ) then begin
    execute procedure prsheets_calculate(new.sheet,'','S');
  end
  if(new.opertime <> old.opertime ) then execute procedure PRSHEETS_TECHJEDNQUANTITY_CALC(new.sheet);
end^
SET TERM ; ^
