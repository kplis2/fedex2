--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMORTIZATION_BU0 FOR AMORTIZATION                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  select amyear, ammonth from amperiods
    where ref=new.amperiod into new.amyear, new.ammonth;
end^
SET TERM ; ^
