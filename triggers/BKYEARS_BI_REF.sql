--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKYEARS_BI_REF FOR BKYEARS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BKYEARS')
      returning_values new.REF;
end^
SET TERM ; ^
