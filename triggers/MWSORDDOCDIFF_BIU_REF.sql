--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDDOCDIFF_BIU_REF FOR MWSORDDOCDIFF                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('DOKUMROZ') returning_values new.ref;
end^
SET TERM ; ^
