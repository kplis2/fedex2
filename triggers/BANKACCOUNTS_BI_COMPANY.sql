--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_BI_COMPANY FOR BANKACCOUNTS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable dicttable varchar(31);
begin
  select typ from slodef
    where ref = new.dictdef
    into :dicttable;

  if (exists(select * from rdb$relation_fields where rdb$relation_name = :dicttable and rdb$field_name = 'COMPANY')) then
    execute statement 'select company from ' || dicttable || ' where ref = ' || new.dictpos
      into new.company;
end^
SET TERM ; ^
