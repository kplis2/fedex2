--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BD0_RKDOKNAG FOR KLIENCI                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists (select ref from rkdoknag R where R.slodef = 1 and R.slopoz = old.ref)) then
    exception universal 'Dla podmiotu jest wystawiony dokument kasowy!';
end^
SET TERM ; ^
