--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BD_RIGHTS FOR DECREES                        
  ACTIVE BEFORE DELETE POSITION 3 
as
declare variable aktuoper integer;
declare variable konf varchar(255);
declare variable rights varchar(255);
declare variable rightsgroup varchar(255);
declare variable opergroup varchar(1024);
declare variable allaclen integer;
declare variable aclen integer;
declare variable dictdef integer;
declare variable sdnazwa varchar(20);
declare variable spnazwa varchar(40);
declare variable syntetyka varchar(5);

begin
  execute procedure get_global_param ('AKTUOPERATOR') returning_values aktuoper;
  execute procedure get_config('DECREESRIGHTS',0) returning_values :konf;
  if (:konf = 1 and coalesce(old.autodoc,0) <> 1) then
  begin
    -- weryfikacja dostepu do kont syntetycznych
    select  rights, rightsgroup,symbol from bkaccounts where ref = old.accref
           into :rights, :rightsgroup,:syntetyka;
    select grupa from operator where ref = :aktuoper into :opergroup;
    if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
    begin
      if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
        exception rights 'Brak uprawnień do konta syntetycznego ' || :syntetyka;
    end

    -- weryfikacja dostepu do kont analitycznych
    select rights, rightsgroup from accounting ac where ac.company = old.company and ac.yearid = substring(old.period from 1 for 4)
           and ac.account = old.account and ac.currency = old.currency
           into :rights, :rightsgroup;
    if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
    begin
      if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
        exception rights 'Brak uprawnień do konta analitycznego ' || old.account;
    end

    -- weryfikacja dostepu do slownikow analitycznych
    allaclen = 4; --dlugosc sytnetyki + separator
    for select acs.len, acs.dictdef from accstructure acs where acs.bkaccount = old.accref
        into :aclen, :dictdef
    do begin
       select rights, rightsgroup, slodef.nazwa, sp.kod from slopoz sp left join slodef on (slodef.ref = sp.slownik)
             where sp.slownik = :dictdef
             and sp.kod = substring(old.account from :allaclen+1 for  :allaclen + :aclen)
             into :rights, :rightsgroup, :sdnazwa, :spnazwa;
       if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
       begin
         if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
           exception rights 'Brak uprawnień do slownika analitycznego: "'||:sdnazwa||'", pozycja "'||:spnazwa||'"';
       end
       allaclen = allaclen + aclen + 1; --dodajemy dlugosc slownika + 1 na separator
    end
  end
end^
SET TERM ; ^
