--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_BI FOR DOKPLIK                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable segslodef integer;
declare variable segwlasc integer;
declare variable segklasa varchar(20);
declare variable dokslodef integer;
declare variable numbergen varchar(80);
declare variable segnazwa varchar(255);
declare variable typnazwa varchar(255);
declare variable typsymbol varchar(20);
declare variable segsymbol varchar(20);
BEGIN
  select WLASCICIEL,SLODEF,KLASA from SEGREGATOR where SEGREGATOR.REF=new.SEGREGATOR into new.wlasciciel,:segslodef,:segklasa;
  new.klasa = :segklasa;
  if(new.ckontrakt=0) then new.ckontrakt=NULL;
  if(new.zadanie=0) then new.zadanie=NULL;
  if(new.kontakt=0) then new.kontakt=NULL;
  if(new.status is null) then new.status = 0;
  if(new.symbol is null) then new.symbol = '';
  --if(coalesce(new.symbol,'') = '') then new.symbol = 'Brak nazwy (' || new.ref || ')';
  if(new.typ is not null) then begin
    select numerator, symbol, nazwa from dokpliktyp where ref = new.typ into :numbergen, :typsymbol, :typnazwa;
    select symbol, nazwa from SEGREGATOR where SEGREGATOR.REF=new.SEGREGATOR into :segsymbol, :segnazwa;
    if(numbergen is not null)then begin
      if(coalesce(:typsymbol, '')='') then typsymbol = substring(:typnazwa from 1 for 20);
      if(coalesce(:segsymbol, '')='') then segsymbol = substring(:segnazwa from 1 for 20);
      execute procedure get_number(:numbergen,:typsymbol,:segsymbol,new.data, 0, new.ref) returning_values new.numer, new.symbol;
    end
  end
  if(new.typ is null or new.segregator is null or not exists (select d.dokpliktyp from dokpliktypsegregator d where d.dokpliktyp = new.typ and d.segregator = new.segregator)) then begin
    select NAZWA from SEGREGATOR where SEGREGATOR.REF=new.SEGREGATOR into :segnazwa;
    select nazwa from dokpliktyp where ref = new.typ into :typnazwa;
    exception universal 'Typ dokumentu '||typnazwa||' nie jest dozwolony w segregatorze '||segnazwa;
  end
END^
SET TERM ; ^
