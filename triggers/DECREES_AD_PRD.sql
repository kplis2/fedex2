--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_AD_PRD FOR DECREES                        
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  delete from prdaccounting P where P.decree = old.ref;
end^
SET TERM ; ^
