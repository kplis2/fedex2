--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSUPCLR_BIU_REF FOR MWSSUPCLR                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('MWSSUPCLR')
      returning_values new.REF;
end^
SET TERM ; ^
