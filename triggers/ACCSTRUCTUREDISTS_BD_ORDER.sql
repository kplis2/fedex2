--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_BD_ORDER FOR ACCSTRUCTUREDISTS              
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 5) then exit;
  update ACCSTRUCTUREDISTS set ord = 1, number = number - 1 , internalupdate = 1
    where ref <> old.ref and number > old.number and otable = old.otable and oref = old.oref;
end^
SET TERM ; ^
