--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERS_AU0 FOR BTRANSFERS                     
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable maxamount numeric(14,2);
  declare variable btrpos integer;
begin
  if (new.status > 2 and old.status <= 2) then
  begin -- zamrozenie maxamount na pozycjach przelewu
    for
      select ref
        from BTRANSFERPOS
        where BTRANSFER = new.ref
        into :btrpos
    do begin
      execute procedure btransfers_checkmaxamount(btrpos)
        returning_values maxamount;
      update BTRANSFERPOS
        set MAXAMOUNT = :maxamount
        where REF = :btrpos and MAXAMOUNT <> :maxamount;
    end
  end
end^
SET TERM ; ^
