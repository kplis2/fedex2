--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BIU_PREVLIMIT FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
declare variable vaccardyear integer;
begin
/*Wyliczenie urlopu zaleglego. Uzytkownik sam moze zmienic urlop zalegly, jezli
  konfig VACCARDYEAR = NEW.VYEAR, w pp. nalicznie na podstawie karty z poprzeniego roku.
  Nie przeliczamy urolopu zalegleglego dla kart <= VACCARDYEAR (BS40080)*/

  execute procedure get_config('VACCARDYEAR', 2)
    returning_values vaccardyear;

  if ((inserting and new.prevlimith = '0') or (updating and new.vyear > vaccardyear)) then
  begin
  --uzytkownik nie wpisywal wartosci samodzielnie
    new.prevlimit = null;
    select restlimit - supplementallimit  --url. dodatkowy nie przechodzi na nastepny rok
      from evaclimits
      where employee = new.employee and vyear = new.vyear - 1
      into new.prevlimit;
    new.prevlimit = coalesce(new.prevlimit,0);
    if (new.prevlimit < 0) then new.prevlimit = 0;
  end else
  --uzytkownik mogl zmienic wartosc url. zaleglego
    if (new.prevlimith <> coalesce(old.prevlimith,'')) then
      execute procedure hours2minutes(new.prevlimith) returning_values new.prevlimit;
end^
SET TERM ; ^
