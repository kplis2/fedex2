--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_AI_CENNIK FOR WERSJE                         
  ACTIVE AFTER INSERT POSITION 2 
as
declare variable aktuoddzial varchar(255);
declare variable refdefcen integer;
declare variable status integer;
declare variable ileprzel integer;
declare variable iledod integer;
begin
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
  for select ref from DEFCENNIK
        where AKTUZAKUP=1 and AKT=1 and ((ODDZIAL=:aktuoddzial) or (ODDZIAL='') or (ODDZIAL is NULL))
        order by kolejnosc
      into :refdefcen
  do begin
    execute procedure CENNIK_AKTUALIZUJ(:refdefcen, new.ref, 1, 0) returning_values :status,:ileprzel,:iledod;
  end
end^
SET TERM ; ^
