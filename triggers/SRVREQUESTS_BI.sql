--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVREQUESTS_BI FOR SRVREQUESTS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if(new.status is null) then exception SRVREQUEST_BRAK_STATUSU;
  if(new.status is not null) then select nazwa from CFAZY where ref=new.status into new.statusnazwa;
  if(new.regoper is not null) then select nazwa from OPERATOR where ref=new.regoper into new.regopernazwa;
  if(new.SRECDATE is not null and new.CRECDATE is null) then begin
    new.CRECDATE = new.SRECDATE;
  end
  if(new.CPASSDATE is not null and new.SPASSDATE is null) then begin
    new.SPASSDATE = new.CPASSDATE;
  end
  if (new.status is not null) then
    select AKTYWNE from CFAZY where REF = new.status into new.aktywne;
END^
SET TERM ; ^
