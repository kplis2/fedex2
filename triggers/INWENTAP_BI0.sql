--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTAP_BI0 FOR INWENTAP                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable mag varchar(3);
declare variable dat timestamp;
declare variable zpartiami integer;
declare variable zbiorczy integer;
declare variable zamk integer;
declare variable cnt integer;
declare variable strategia varchar(100);
declare variable datapom timestamp;
declare variable cenapom numeric(14,4);
declare variable sumilinw numeric(14,4);
declare variable mws smallint;
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.wersjaref is null) then
    exception INWENTAP_BRAKTOWARU;

  select i.zpartiami, i.zbiorczy, i.zamk, i.magazyn, i.data, d.mws
    from INWENTA i
      left join defmagaz d on (d.symbol = i.magazyn)
    where i.ref = new.inwenta
  into :zpartiami, :zbiorczy, :zamk, :mag, :dat, :mws;

  if(:zpartiami is null) then zpartiami = 0;
  if(:zbiorczy is null) then zbiorczy = 0;
  if(:zbiorczy > 0) then begin
    /*kontrola przed dodaniem drugi raz tego samego rekordu do raportu zbiorczego*/
    select count(*) from INWENTAP where INWENTA = new.inwenta and WERSJAREF = new.wersjaref and
      (cena = new.cena or (cena is null and new.cena is null)) and
      (dostawa = new.dostawa or (dostawa is null and new.dostawa is null))
    into  :cnt;
    if(:cnt > 0) then exception INWENTAP_EXPT 'Wpis na inwentaryzacji zbiorczej dla towaru '||new.ktm||' juz istnieje.';
  end
  if(new.blokadanalicz is null) then new.blokadanalicz = 0;
  if((new.ilstan is null or new.ilstan = 0) and new.blokadanalicz=0) then begin
    /*obliczenie na wąsna rk stanu magazynowego*/
    if(:zpartiami = 0) then
      select sum(stan) from MAG_STAN(:mag, new.ktm, new.wersja, NULL, NULL, :dat) into new.ilstan;
    else
      select sum(stan) from MAG_STAN(:mag, new.ktm, new.wersja, new.cena, new.dostawa, :dat) into new.ilstan;
  end
  if(new.ilkorekta <> 0 and :zbiorczy = 0 and :zamk = 0) then
    new.ilkorekta = 0;
  if(new.ilstan is null) then new.ilstan = 0;
  if(new.ilinw is null) then new.ilinw = 0;
  if(new.ilkorekta is null) then new.ilkorekta = 0;
  if(new.status is null) then new.status = 1;
  if (new.mwsilroz is null) then new.mwsilroz = 0;
  if (mws = 1 and zbiorczy = 1 and (new.ilstan > 0 or new.mwsilroz > 0)) then
    new.ilinw = new.ilstan + new.mwsilroz;
  /*Obliczenie sumy pozycji dla inwentury partiami i wersjami*/
  if(new.blokadanalicz=0) then begin
    select status
      from inwentap_set_status(new.ref, new.wersjaref, new.inwenta, new.dostawa, new.ilstan, new.ilinw)
      into new.status;
  end
  select NAZWA from TOWARY where ktm = new.ktm into new.nazwat;
  if(new.cenaplus is null or (new.cenaplus = 0) ) then
  begin
    execute procedure INWENTAP_ILPLUS_WYCENA(new.ref,new.wersjaref,:mag,new.dostawa) returning_values new.cenaplus;
  end
  if(new.cena is null or (new.cena=0)) then
    select cena_zakn from wersje where ref=new.wersjaref into new.cena;
  new.ilroznica = new.ilinw-new.ilstan;
  new.wartosc = new.ilroznica * new.cenaplus;
  if(:zamk>0 and new.blokadanalicz=0)then
      exception INWENTA_ZAAKCEPTOWANE 'Nie można dodać pozycji na zamkniętej inwentaryzacji!';
  new.updated=1;
end^
SET TERM ; ^
