--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AU_VATPERIOD FOR BKDOCS                         
  ACTIVE AFTER UPDATE POSITION 20 
AS
declare variable pom1 varchar(6);
declare variable pom2 timestamp;
declare variable pom3 timestamp;
declare variable pom4 timestamp;
declare variable korekta smallint;
declare variable zakup smallint;
begin

  if (coalesce(new.vatperiod,'') != ''  and coalesce(old.vatperiod,'') = '' ) then begin
    execute procedure datatookres(new.vatperiod,0,0,0,1)
      returning_values :pom1,  :pom2,  :pom3, :pom4;
    if (new.otable= 'NAGFAK') then begin
      select t.korekta, t.zakup
        from nagfak n
          join typfak t on (t.symbol = n.typ)
        where n.ref = new.oref
      into :korekta, :zakup;
      if (:korekta = 1 and :zakup = 0) then
        update nagfak n set n.dataotrzkor = :pom4 where n.ref = new.oref and n.dataotrzkor is null;
    end
  end
end^
SET TERM ; ^
