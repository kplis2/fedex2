--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BI FOR CKONTRAKTY                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable projectdef integer;
declare variable akt smallint;
declare variable kontofk varchar(10);
declare variable cpodmiotname varchar(100);
BEGIN
  if(new.nazwa is null or new.nazwa = '') then exception CKONTRAKTY_NAZWA;
  akt = 1;
  if(new.FAZA is not NULL) then
    select GRUPA from CFAZY where CFAZY.REF = new.FAZA into new.GRUPA;
  else new.GRUPA = NULL;
  if(new.cpodmiot=0) then new.cpodmiot = NULL;
  if(new.osoba=0) then new.osoba = NULL;
  -- obsuga sownika projektow
  select contrtypes.projectdef from contrtypes
    where contrtypes.ref = new.contrtype into :projectdef;
  if (:projectdef is not null and new.token <> 7) then begin
    if (not exists(select ref from slopoz where slopoz.slownik = :projectdef
        and slopoz.masterref = new.ref)
      ) then
    begin
      select cfazy.document
        from cfazy
        where cfazy.aktywne = 1 and cfazy.ref = new.faza
        into :akt;
      select cpodmioty.nazwa
        from cpodmioty 
        where cpodmioty.ref = new.cpodmiot
        into :cpodmiotname;
      if (:akt is null) then akt = 0;
      insert into slopoz (slownik, akt, nazwa, crm, masterref, masterident, kontoks, kod, opis)
                    values (:projectdef, :akt, new.nazwa, 1, new.ref, new.cpodmiot, null, substring(new.nazwa from 1 for 40), :cpodmiotname);

    end else
      exception universal 'projekt istnieje już w słowniku';
  end
  if(new.faza is null) then select phase from contrtypes where ref = new.contrtype into new.faza;
END^
SET TERM ; ^
