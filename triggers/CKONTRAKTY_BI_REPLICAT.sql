--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BI_REPLICAT FOR CKONTRAKTY                     
  ACTIVE BEFORE INSERT POSITION 5 
AS
begin
 execute procedure rp_trigger_bi('CKONTRAKTY',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
