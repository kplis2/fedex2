--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTNAG_BIU_MAKETIMEH FOR EPRESENCELISTNAG               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  -- jeli pola OD - DO sa wypelnione, a nie jest wypelnione pole MAKETIMEHOURS
  -- jest ono autonatycznie wypelniane
  if(new.STARTAT is not null and new.STOPAT is not null and (new.MAKETIMEHOURS is null or new.MAKETIMEHOURS=0)) then begin
    new.MAKETIMEHOURS = new.STOPAT - new.STARTAT;
  end
end^
SET TERM ; ^
