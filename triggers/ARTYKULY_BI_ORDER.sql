--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ARTYKULY_BI_ORDER FOR ARTYKULY                       
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from artykuly where dzial = new.dzial
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update artykuly set ord = 0, numer = numer + 1
      where numer >= new.numer and dzial = new.dzial;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
