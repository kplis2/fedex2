--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLLDEDUCTIONS_BIU_CHECK FOR ECOLLDEDUCTIONS                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
   if (sign(coalesce(new.initdeduction,0)) * sign(coalesce(new.deductionrate,0)) < 0) then
     exception universal 'Kwota początkowa oraz rata potrącenia nie mogą być przeciwnego znaku!';
end^
SET TERM ; ^
