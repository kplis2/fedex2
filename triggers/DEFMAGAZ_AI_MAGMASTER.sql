--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_AI_MAGMASTER FOR DEFMAGAZ                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable werref integer;
declare variable ilosc numeric(14,4);
begin
  if(new.magmaster is not null and new.magmaster <> '') then
begin
    for
      select WERSJAREF, ILOSC from STANYIL where MAGAZYN = new.symbol
      into :werref, :ilosc
    do begin
      update STANYIL set iloscpod = iloscpod + :ilosc where WERSJAREF = :werref and magazyn = new.magmaster;
    end
    update defmagaz set ismagmaster = 1 where symbol = new.magmaster;
  end
end^
SET TERM ; ^
