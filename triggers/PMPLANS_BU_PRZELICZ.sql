--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANS_BU_PRZELICZ FOR PMPLANS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
 -- sumowanie wartosci przychodowej planowanej
  if(new.calcmval<>old.calcmval or new.calcsval<>old.calcsval or new.calcw<>old.calcw or
     new.calcwval<>old.calcwval or new.calce<>old.calce or new.calceval<>old.calceval or
     new.calcval <> old.calcval) then
    new.calcsumval = new.calcmval + new.calcsval + new.calcwval +  new.calceval + new.calcval;

-- sumowanie wartosci kosztowej planowanej
  if(new.budmval<>old.budmval or new.budsval<>old.budsval or new.calcw<>old.calcw or
    new.budwval<>old.budwval or new.calce<>old.calce or new.budeval<>old.budeval or
    new.budval <> old.budval) then
    new.budsumval = new.budmval + new.budsval + new.budwval +  new.budeval + new.budval;

-- sumowanie wartosci przychodowej rzeczywistej
  if(new.erealmval<>old.erealmval or new.erealsval<>old.erealsval or new.erealw<>old.erealw or
    new.erealwval<>old.erealwval or new.ereale<>old.ereale or new.erealeval<>old.erealeval or
    new.erealval <> old.erealval) then
    new.erealsumval = new.erealmval + new.erealsval + new.erealwval + new.erealeval + new.erealval;

-- sumowanie wartosci kosztowej rzeczywistej
  if(new.realmval<>old.realmval or new.realsval<>old.realsval or new.realw<>old.realw or
    new.realwval<>old.realwval or new.reale<>old.reale or new.realeval<>old.realeval or
    new.realval <> old.realval) then
    new.realsumval = new.realmval + new.realsval + new.realwval + new.realeval + new.realval;

  new.budproc = iif(new.budsumval<>0,100 * new.realsumval / new.budsumval,0);
end^
SET TERM ; ^
