--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAMUP_BI_NR FOR NAGZAMUP                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable nr integer;
declare variable maxnrup integer;
declare variable symbol varchar(35);
begin
  select max(numer) from nagzamup where dostawca=new.dostawca into :nr;
  if(nr is null) then nr = 1;
  else nr = :nr +1;

  select max(numerup) from nagzamup where zamowienie = new.zamowienie into :maxnrup;
  if(maxnrup is null) then maxnrup = 1;
  else maxnrup = :maxnrup + 1;

  select coalesce(id,'') from nagzam where ref = new.zamowienie into :symbol;

  new.numer = :nr;
  new.numerup = :maxnrup;
  if (:symbol <> '') then new.symbol = :symbol || '/' || :maxnrup;
end^
SET TERM ; ^
