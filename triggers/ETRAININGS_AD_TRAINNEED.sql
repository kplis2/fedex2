--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETRAININGS_AD_TRAINNEED FOR ETRAININGS                     
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  update empltrainneeds N
    set N.etraining = null
    where N.etrainneed = old.trainneed and N.etraining = old.ref;
end^
SET TERM ; ^
