--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_AU_DOCFROMOPERRAP FOR PRSHORTAGES                    
  ACTIVE AFTER UPDATE POSITION 2 
as
begin
  if (coalesce(new.amount,0) <> coalesce(old.amount,0)
    or coalesce(new.ktm,'') <> coalesce(old.ktm,'')
  ) then
  begin
    if (exists (select first 1 1 from prschedopers p left join prshmat m on (p.shoper = m.opermaster)
        where p.ref = new.prschedoper and m.autodocout > 0)) then
    begin
      --generowanie dokumentow RW do braków
      execute procedure prschedguide_real(null, new.ref, 1, 1);
    end
  end
end^
SET TERM ; ^
