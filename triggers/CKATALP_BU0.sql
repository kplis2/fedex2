--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATALP_BU0 FOR CKATALP                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable cdefkatal integer;
begin
  if((new.ckatal<>old.ckatal) or (new.symbol<>old.symbol)) then begin
    select CDEFKATAL from CKATAL where REF=new.ckatal into :cdefkatal;
    select NUMER from CDEFKATALP where CDEFKATAL=:cdefkatal and AKRONIM=new.symbol into new.numer;
  end
end^
SET TERM ; ^
