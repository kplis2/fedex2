--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_AU_STATUS FOR PRSHEETS                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable msg varchar(255);
declare variable procmsg varchar(255);
declare variable pomref integer;
declare variable usluga smallint;
begin
  msg = '';
  if (old.status <> new.status and old.status = 0 and new.status in (1,2)) then
  begin
    if (coalesce(new.shtype,'') = '') then msg = :msg || 'typ karty;';
    if (coalesce(new.symbol,'') = '') then msg = :msg || 'symbol karty;';
    if (coalesce(new.ktm,'') = '') then  msg = :msg || 'KTM wyrobu;';
    if (coalesce(new.wersjaref,0) = 0) then msg = :msg || 'wersja KTM;';
    if (coalesce(new.quantity,0) = 0) then msg = :msg || 'ilosc kalk.;';
    if (coalesce(new.jedn,0) = 0) then
      msg = :msg || 'Jednostek;';
    else if (not exists (select first 1 1 from towjedn j where j.ref = new.jedn and j.ktm = new.ktm)) then
      msg = :msg || 'blad jednostki magazynowej;';

    if (coalesce(new.prdepart,'') = '') then msg = :msg || 'Wydzial;';
    if (coalesce(new.batchquantity,0) = 0) then msg = :msg || 'Partia;';
    if (coalesce(new.warehouse,'') = '') then msg = :msg || 'Magazyn;';
    --KS:jesli karta tech.jest na usluge nie sprawdza sie istnienia materialow na karcie
    select t.usluga from prsheets m left join towary t on t.ktm = m.ktm where m.ref = new.ref into :usluga;
    if (usluga <> 1) then
    begin
      if (not exists (select first 1 1 from prshmat m where m.sheet = new.ref)) then msg = :msg || 'Brak materialu do karty;';
    end
    --koniec KS--

    for
      select o.ref
        from prshopers o
        where o.sheet = new.ref
        into :pomref    -- sprawdza operacje
    do begin
      execute procedure prsheet_checkoper(:pomref) returning_values :procmsg;
      if (coalesce(char_length(:msg),0) + coalesce(char_length(:procmsg),0) < 255) then  -- [DG] XXX ZG119346
        msg = :msg || coalesce(:procmsg,'');
      pomref = null;
      procmsg = null;
    end

    for
      select m.ref
        from prshmat m
        where m.sheet = new.ref
        into :pomref      --sprawdza materialy
    do begin
      execute procedure prsheet_checkmat(:pomref) returning_values :procmsg;
      if (coalesce(char_length(:msg),0) + coalesce(char_length(:procmsg),0) < 255) then  -- [DG] XXX ZG119346
        msg = :msg || coalesce(:procmsg,'');
      pomref = null;
      procmsg = null;
    end

    if (coalesce(char_length(:msg),0) > 0) then  -- [DG] XXX ZG119346
    begin
      msg = 'Brak danych w karcie:' || :msg;
      exception prsheets_error substring(:msg from 1 for 70);
    end
  end
end^
SET TERM ; ^
