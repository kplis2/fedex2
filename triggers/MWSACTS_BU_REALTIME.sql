--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BU_REALTIME FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  -- deklarowany czas wykonania zlecenia
  if ((new.timestartdecl is not null and (new.timestartdecl <> old.timestartdecl or old.timestartdecl is null))
      or (new.timestopdecl is not null and (new.timestopdecl <> old.timestopdecl or old.timestopdecl is null))
  ) then
    new.realtimedecl = new.timestopdecl - new.timestartdecl;
  -- rzeczywisty czas wykonania zlecenia
  if (new.status = 2 and old.status <> 2 and new.timestart is null) then
    new.timestart = current_timestamp(0);
  if (new.status = 5 and old.status <> 5 and new.timestop is null) then
  begin
    new.datestop = current_date;
    new.timestop = current_timestamp(0);
  end
  if (new.status = 6 and old.status <> 6 and new.timestop is null) then
    new.timestop = current_timestamp(0);
  if ((new.timestart is not null and (new.timestart <> old.timestart or old.timestart is null))
      or (new.timestop is not null and (new.timestop <> old.timestop or old.timestop is null))
  ) then
    new.realtime = new.timestop - new.timestart;
end^
SET TERM ; ^
