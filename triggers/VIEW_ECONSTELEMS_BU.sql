--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VIEW_ECONSTELEMS_BU FOR VIEW_ECONSTELEMS               
  ACTIVE BEFORE UPDATE POSITION 1 
as
begin
  update econstelems
  set employee = new.employee,
      ecolumn = new.ecolumn,
      fromdate = new.fromdate,
      todate = new.todate,
      evalue = new.evalue
  where (employee = old.employee) and
        (ecolumn = old.ecolumn) and
        (fromdate = old.fromdate);
end^
SET TERM ; ^
