--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKSTANWAL_BD_CHECKEMPTY FOR RKSTANWAL                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.STAN<>0) then exception RKSTANWAL_EXPT 'Próba usunicia nie zarowego stanu (RKSTANWAL)';
end^
SET TERM ; ^
