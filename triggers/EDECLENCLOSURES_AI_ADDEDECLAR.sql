--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLENCLOSURES_AI_ADDEDECLAR FOR EDECLENCLOSURES                
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable tmp integer;
begin
  execute procedure edeclaration_add_header(new.edecldef, null, new.edeclaration)
    returning_values tmp;
end^
SET TERM ; ^
