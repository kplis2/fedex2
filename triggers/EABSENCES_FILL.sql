--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_FILL FOR EABSENCES                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 5 
as
  declare variable worksecs integer;
  declare variable workdays integer;
  declare variable vacsecs integer;
begin
--Wyliczenie ilosci dni i godzin dla danej nieobecnosci

 if (inserting or new.employee <> old.employee or new.fromdate <> old.fromdate or new.todate <> old.todate
    or coalesce(new.worksecs,0) <> coalesce(old.worksecs,0) or coalesce(new.workdays,0) <> coalesce(old.workdays,0)
    or coalesce(new.dimnum,0) <> coalesce(old.dimnum,0) or coalesce(new.dimden,0) <> coalesce(old.dimden,0)
 ) then begin

    new.days = new.todate - new.fromdate + 1;
    new.ayear = extract(year from new.fromdate);
    new.amonth = extract(month from new.fromdate);
  
    execute procedure ecal_work(new.employee, new.fromdate, new.todate, new.emplcontract) --BS76214
      returning_values :workdays, :worksecs, :vacsecs;
  
    if (new.worksecs is null and new.workdays is null) then
    begin
      new.workdays = workdays;
      new.worksecs = worksecs;
      new.vacsecs = vacsecs;
      new.worksecs = (new.worksecs * new.dimnum) / new.dimden;  --korzystajac z workdim stracilibysmy doklanosc wyniku, np. przy workdim = 1/3
    end else
      new.vacsecs = iif(vacsecs <> 0 and new.worksecs = worksecs, vacsecs, new.worksecs);

  --okreslenie godzin
    execute procedure durationstr2(cast(new.worksecs as bigint),0,0,0)
      returning_values new.workhours;
  end

  if (inserting or coalesce(new.vacsecs,0) <> coalesce(old.vacsecs,0)) then
  begin
    execute procedure durationstr2(cast(new.vacsecs as bigint),0,0,0)
      returning_values new.vachours;
  end
end^
SET TERM ; ^
