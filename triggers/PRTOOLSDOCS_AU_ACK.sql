--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCS_AU_ACK FOR PRTOOLSDOCS                    
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable inssue smallint;
  declare variable outside smallint;
  declare variable prtoolstype varchar(20);
  declare variable prtool varchar(20);
  declare variable invno varchar(20);
  declare variable amount numeric(14,4);
  declare variable price numeric(14,2);
  declare variable posref integer;
begin
  if (old.akcept <> new.akcept) then
  begin
    select coalesce(df.inssue,0), coalesce(df.outside,0)
      from prtoolsdefdocs df
      where df.symbol = new.prtoolsdefdoc
        and df.prdsdef = new.prdsdef
      into :inssue, :outside;

    for
      select p.prtoolstype, p.prtool, p.amount,
          case when t.verificationnr = 1 then p.invno else null end,
          p.price, p.ref
        from prtoolsdocpos p
          join prtoolstypes t on (t.symbol = p.prtoolstype)
        where p.prtoolsdoc = new.ref
        into :prtoolstype, :prtool, :amount, :invno, :price, :posref
    do begin
      if (new.akcept = 0) then
        amount = amount * (-1);

      if (not exists(select first 1 1 from prtools p where p.prdsdef = new.prdsdef
        and p.tooltype = :prtoolstype and p.symbol = :prtool
        and (:invno is null or p.invno = :invno) and p.price = :price)
      ) then
        insert into prtools (prdsdef, tooltype, symbol, invno, amount, amountfree, price)
          values (new.prdsdef, :prtoolstype, :prtool, :invno,0,0, :price);

      if (:inssue = 0) then -- dokument przjecia
      begin
        if (:outside = 1) then -- dokument zewnetrzny przjecia zwieksza ilosc podstawowa
          update prtools p set p.amount = p.amount + :amount, p.amountfree = p.amountfree + :amount
            where p.prdsdef = new.prdsdef and p.tooltype = :prtoolstype and p.symbol = :prtool
              and (:invno is null or p.invno = :invno) and p.price = :price;
        else -- dokument wew przjecia zwieksza ilosc wolna
          update prtools p set p.amountfree = p.amountfree + :amount
            where p.prdsdef = new.prdsdef and p.tooltype = :prtoolstype and p.symbol = :prtool
              and (:invno is null or p.invno = :invno) and p.price = :price;
      end else begin --dokument wydania
        if (:outside = 1) then -- dokument zewn. wydania
          update prtools p set p.amount = p.amount - :amount, p.amountfree = p.amountfree - :amount
            where p.prdsdef = new.prdsdef and p.tooltype = :prtoolstype and p.symbol = :prtool
              and (:invno is null or p.invno = :invno) and p.price = :price;
        else -- dokument wew wydania zmniejsza ilosc wolna
          update prtools p set p.amountfree = p.amountfree - :amount
            where p.prdsdef = new.prdsdef and p.tooltype = :prtoolstype and p.symbol = :prtool
              and (:invno is null or p.invno = :invno) and p.price = :price;
      end
      -- dodanie do historii
      if (not exists (select first 1 1 from prtoolshistory h where h.prtoolsdocpos = :posref)) then
        insert into prtoolshistory (PRTOOLS, EMPLOYEES, AMOUNT, PRTOOLSDOC, PRTOOLSDOCPOS, TOOLTYPE,
            INVNO, PRICE, PRDSDEF, PRSCHEDGUIDE, PRSCHEDZAM, PRMACHINE, datereal, hsaction,
            slodef, slopoz)
          values (:prtool, new.employee, :amount, new.ref, :posref, :prtoolstype,
            :invno, :price, new.prdsdef, new.prschedguides, new.prschedzam, new.prmachine, new.docdate, :inssue,
            new.slodef, new.slopoz);
      else
        update prtoolshistory h set h.prtools = :prtool, h.employees = new.employee, h.amount = h.amount + :amount,
            h.prtoolsdoc = new.ref, h.tooltype = :prtoolstype, h.invno = :invno, h.price = :price,
            h.prdsdef = new.prdsdef, h.prschedguide = new.prschedguides, h.prschedzam = new.prschedzam,
            h.prmachine = new.prmachine, h.datereal = new.docdate, h.dateaction = current_timestamp(0), h.hsaction = :inssue,
            h.slodef = new.slodef, h.slopoz = new.slopoz
          where h.prtoolsdocpos = :posref;
      prtoolstype = null;
      prtool = null;
      amount = null;
      invno = null;
      price = null;
      posref = null;
    end
  end
end^
SET TERM ; ^
