--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_AD0 FOR NAGZAM                         
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable numberg varchar(40);
declare variable local smallint;
declare variable usunpochodne smallint;
declare variable BLOCKREF integer;
declare variable reddokum integer;
declare variable ildoprod numeric(14,4);
declare variable ilwyprod numeric(14,4);
begin
  execute procedure CHECK_LOCAL('NAGZAM',old.ref) returning_values :local;
  if((:local = 1) and (old.numer > 0) and (old.org_ref is null or (old.org_ref = 0) or(old.org_ref = old.ref))) then begin
     /*zwolnienie numeru zamówienia*/
     select number_gen from DEFREJZAM where old.rejestr = defrejzam.symbol into :numberg;
     if(old.prdepart is not null) then
       execute procedure free_number(:numberg,old.prdepart,old.typzam,old.datawe,old.numer,0) returning_VALUES :blockref;
     else
      execute procedure free_number(:numberg,old.rejestr,old.typzam,old.datawe,old.numer,0) returning_VALUES :blockref;
  end
  /* jesli zamowienie bylo realizacja, to sprawdzeni, czy oryginalne ma jeszcze jakies realizacje */
  if(old.org_ref > 0 AND old.org_ref <> old.ref) then begin
    execute procedure ZAM_SET_TYP(old.org_ref);
    if(old.kkomplet =1) then
      execute procedure NAGZAM_OBL(old.org_ref);
  end
  if(old.faktura > 0) then
    execute procedure NAGFAK_SYMBOLZAM(old.faktura);
  if(old.kpopref > 0) then
    execute procedure POZZAM_OBL(old.kpopref);
/*MS: moze byc wiele listywysd do nagzama i nie nalezy ich usuwac
  if(old.wysylka > 0) then
    delete from LISTYWYSD where REFZAM = old.ref;*/
  if(:local=1) then begin
    select usunpochodne from DEFREJZAM where SYMBOL=old.rejestr into :usunpochodne;
    if(:usunpochodne=1) then begin
      delete from NAGZAM where REFZRODL=old.ref and typ=0;
    end
  end
  if(old.reddokumgl > 0) then begin
    select min(ref) from NAGZAM where REDDOKUMTYP = old.reddokumtyp and REDDOKUMREF = old.reddokumref into :reddokum;
    if(reddokum > 0) then
      update NAGZAM set REDDOKUMGL = 1 where ref=:reddokum;
  end
  --ilosci do produkcji i wyprodukowane:
  if (old.kpopref is not null and old.kpopprzam is null) then
  begin
    select sum(n.kilosc), sum(n.kilzreal)
      from nagzam n
      where n.kpopref = old.kpopref
        and n.kpopprzam is null
      into :ildoprod, :ilwyprod;
    update pozzam p set p.ildoprod = :ildoprod, p.ilwyprod = :ilwyprod
      where p.ref = old.kpopref;
  end

  delete from NAGFAKTERMPLAT where doktyp='Z' and dokref = old.ref;

  delete from dokzlacz d where d.ztable = 'NAGZAM' and d.zdokum = old.ref;
end^
SET TERM ; ^
