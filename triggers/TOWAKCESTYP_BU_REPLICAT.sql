--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCESTYP_BU_REPLICAT FOR TOWAKCESTYP                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.SYMBOL<>old.SYMBOL
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('TOWAKCESTYP',old.symbol, null, null, null, null, old.token, old.state,
        new.symbol, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
