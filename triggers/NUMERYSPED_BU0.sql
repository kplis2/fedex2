--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMERYSPED_BU0 FOR NUMERYSPED                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.aktywny = 1 and coalesce(old.aktywny, 0) = 0) then
    update numerysped set aktywny = 0
      where ref <> new.ref
        and aktywny = 1
        and spedytor = new.spedytor
        and typ = new.typ;

  if (new.nrakt = new.nrkoniec) then begin
    update numerysped set aktywny = 1
      where ref <> new.ref
        and spedytor = new.spedytor
        and typ = new.typ
        and nrstart > new.nrkoniec
        and nrakt < nrkoniec;
  end
end^
SET TERM ; ^
