--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AU_KOREKTYIL FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if ( new.refk is not null and new.akcept = 1 and old.akcept = 0) then
  begin
    update dokumnag set dokumnag.korektyil = 1
      where dokumnag.ref = new.refk and (dokumnag.korektyil = 0 or
        dokumnag.korektyil is null);
  end
  if ( old.refk is not null and new.akcept = 0 and old.akcept = 1) then
  begin
    if (not exists(select refk from dokumnag where refk = old.refk and akcept = 1)) then
      update dokumnag set dokumnag.korektyil = 0
      where dokumnag.ref = old.refk;
  end
end^
SET TERM ; ^
