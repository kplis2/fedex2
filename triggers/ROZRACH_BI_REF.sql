--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BI_REF FOR ROZRACH                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ROZRACH')
      returning_values new.REF;
end^
SET TERM ; ^
