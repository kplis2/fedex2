--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLLDEDUCTIONS_BIU_ACTDEDUCT FOR ECOLLDEDUCTIONS                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
AS
begin
  if (new.initdeduction is not null) then
    new.actdeduction = new.initdeduction - new.deducted;
  else
    new.actdeduction = null;
end^
SET TERM ; ^
