--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AI_OPAKOWANIE FOR POZFAK                         
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable towkaucja smallint;
  declare variable klientopk smallint;
  declare variable status integer;
  declare variable klient integer;
  declare variable typ varchar(3);
  declare variable opakowanie integer;
begin
  select typ, klient from nagfak where ref=new.dokument into :typ, :klient;
  select opakowanie from typfak where symbol=:typ into :opakowanie;
  select kaucja from towary where ktm = new.ktm into :towkaucja;
  if (:towkaucja = 1 and :opakowanie = 1) then
  begin
    select kaucja from klienci where ref = :klient into :klientopk;
    if (:klientopk >0) then
      execute procedure POZFAK_KAUCJA_OPAKOWANIE(new.ref,NULL, :klientopk) returning_values :status;
  end
end^
SET TERM ; ^
