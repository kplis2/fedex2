--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYEES_AU_WORKTYPE FOR EMPLOYEES                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable a integer;
begin
  if (new.workpost <> old.workpost) then
    update employees
      set worktype = (select worktype from edictworkposts where ref = new.workpost)
      where ref = new.ref;
end^
SET TERM ; ^
