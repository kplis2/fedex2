--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKATAT_BU_REPLICAT FOR DEFKATAT                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or new.katalog <> old.katalog or (new.katalog is null and old.katalog is not null) or (new.katalog is not null and old.katalog is null)
      or new.elem <> old.elem or (new.elem is null and old.elem is not null) or (new.elem is not null and old.elem is null)
      or new.cecha <> old.cecha or (new.cecha is null and old.cecha is not null) or (new.cecha is not null and old.cecha is null)
      or (new.wartosc <> old.wartosc ) or (new.wartosc is not null and old.wartosc is null) or (new.wartosc is null and old.wartosc is not null)
      or new.wyszuk <> old.wyszuk or (new.wyszuk is null and old.wyszuk is not null) or (new.wyszuk is not null and old.wyszuk is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('DEFKATAT',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
