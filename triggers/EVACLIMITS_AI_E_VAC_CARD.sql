--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_AI_E_VAC_CARD FOR EVACLIMITS                     
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  execute procedure e_vac_card(new.vyear || '/1/1', new.employee);
end^
SET TERM ; ^
