--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFBLEDY_BU_REPLICAT FOR DEFBLEDY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.tekst <> old.tekst or (new.tekst is null and old.tekst is not null) or (new.tekst is not null and old.tekst is null)
  ) then execute procedure REPLICAT_STATE('DEFBLEDY',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
end^
SET TERM ; ^
