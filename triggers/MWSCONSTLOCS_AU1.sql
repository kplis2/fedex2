--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCS_AU1 FOR MWSCONSTLOCS                   
  ACTIVE AFTER UPDATE POSITION 1 
AS
declare variable act smallint;
begin
  if (new.act <> old.act and new.wharea is not null and new.mwsstandlevelnumber is not null) then
  begin
    if (new.act > 0) then act = 1;
    else act = 0;
    execute procedure MWS_CALC_FREELOCS (new.ref,:act);
  end
  if (new.goodsav <> old.goodsav) then
    update mwsstock s set s.goodsav = new.goodsav where s.mwsconstloc = new.ref;
end^
SET TERM ; ^
