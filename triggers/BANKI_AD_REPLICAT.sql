--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKI_AD_REPLICAT FOR BANKI                          
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('BANKI', old.symbol, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
