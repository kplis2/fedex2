--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDOKUM_BU0 FOR DEFDOKUM                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
begin
  if (new.symbol <> old.symbol) then
  begin
     if (exists(select ref from DOKUMNAG where TYP=old.symbol)) then
       exception DEFDOKUM_CHANGE_NOT_ALLOW;
  end
  if(new.koryg > 0) then new.korygujacy = '';
  if(new.PRZECIW <> '' and (new.przeciw is not null)) then begin
    cnt = null;
    select count(*) from DEFDOKUM where SYMBOL = new.PRZECIW into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then
        EXCEPTION  DEFDOKUM_PRZECIW_FK;
  end
  if(new.korygujacy <> '' and (new.korygujacy is not null)) then begin
    cnt = null;
    select count(*) from DEFDOKUM where SYMBOL = new.korygujacy into :cnt;
    if(:cnt is null) then cnt = 0;
     if(:cnt = 0) then
        EXCEPTION  DEFDOKUM_KORYGUJACY_FK;
  end
  if(new.opak > 0 and ((new.kaucjabn is null) or (new.kaucjabn = ''))) then
    new.kaucjabn = 'N';
  if(new.miedzyzaw > 0 and ((new.miedzy is null) or (new.miedzy = 0)))then
    new.miedzyzaw = 0;
  if(new.koryg is null) then new.koryg = 0;
  if(new.koryg = 0) then new.korygpow = 0;
  if(new.transport = 0) then new.transportauto = 0;
  if(new.transportauto is null) then new.transportauto = 0;
  if(new.nieobrot is null) then new.nieobrot = 0;
end^
SET TERM ; ^
