--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIGVALS_AD_REPLICAT FOR KONFIGVALS                     
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('KONFIGVALS', old.akronim, old.oddzial, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
