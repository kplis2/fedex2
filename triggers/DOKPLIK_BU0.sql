--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_BU0 FOR DOKPLIK                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable segslodef integer;
declare variable segwlasc integer;
declare variable segklasa varchar(20);
declare variable dokslodef integer;
declare variable numbergen varchar(80);
declare variable segnazwa varchar(255);
declare variable typnazwa varchar(255);
declare variable typsymbol varchar(20);
declare variable segsymbol varchar(20);
begin
  if(new.segregator<>old.segregator) then begin
    select WLASCICIEL,SLODEF,KLASA from SEGREGATOR where SEGREGATOR.REF=new.SEGREGATOR into new.wlasciciel,:segslodef,:segklasa;
    new.klasa = :segklasa;
  end
  if(coalesce(new.typ,'') <> coalesce(old.typ,'')) then begin
    if(old.typ is not null) then begin
      select symbol, nazwa from SEGREGATOR where SEGREGATOR.REF=old.SEGREGATOR into :segsymbol, :segnazwa;
      select numerator, symbol, nazwa from dokpliktyp where ref = old.typ into :numbergen, :typsymbol, :typnazwa;
      if(numbergen is not null and old.numer is not null)then begin
        if(coalesce(:typsymbol, '')='') then typsymbol = substring(:typnazwa from 1 for 20);
        if(coalesce(:segsymbol, '')='') then segsymbol = substring(:segnazwa from 1 for 20);
        execute procedure free_number(:numbergen,:typsymbol,:segsymbol, old.data, old.numer,0) returning_values :dokslodef;
      end
    end
    if(new.typ is not null) then begin
      select symbol, nazwa from SEGREGATOR where SEGREGATOR.REF=new.SEGREGATOR into :segsymbol, :segnazwa;
      select numerator, symbol, nazwa from dokpliktyp where ref = new.typ into :numbergen, :typsymbol, :typnazwa;
      if(numbergen is not null)then begin
        if(coalesce(:typsymbol, '')='') then typsymbol = substring(:typnazwa from 1 for 20);
        if(coalesce(:segsymbol, '')='') then segsymbol = substring(:segnazwa from 1 for 20);
        execute procedure get_number(:numbergen,:typsymbol,:segsymbol,new.data, 0, new.ref) returning_values new.numer, new.symbol;
      end
    end
  end
  if((coalesce(new.typ,0) <> coalesce(old.typ,0) or coalesce(new.segregator,0) <> coalesce(old.segregator,0))  and not exists (select d.dokpliktyp from dokpliktypsegregator d where d.dokpliktyp = new.typ and d.segregator = new.segregator)) then begin
    select NAZWA from SEGREGATOR where SEGREGATOR.REF=new.SEGREGATOR into :segnazwa;
    select nazwa from dokpliktyp where ref = new.typ into :typnazwa;
    exception universal 'Typ dokumentu '||:typnazwa||' nie jest dozwolony w segregatorze '||:segnazwa;
  end
end^
SET TERM ; ^
