--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTDETS_AD0 FOR MWSACTDETS                     
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  -- wysprzatanie stanów cenowych stworzonych pod dostawe
  if (old.rec = 1 and old.stancen is not null and old.mwsconstlocp is null) then
  begin
    if (not exists(select ref from mwsacts where stancen = old.stancen)) then
      if (not exists(select ref from stanycen where ref = old.stancen and cena > 0)) then
        if (not exists(select ref from mwsstock where stancen = old.stancen)) then
          if (not exists (select ref from stanycen where ref = old.stancen and (ilosc > 0 or zablokow > 0) )) then
            delete from stanycen where ref = old.stancen;
  end
end^
SET TERM ; ^
