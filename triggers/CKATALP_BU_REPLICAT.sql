--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATALP_BU_REPLICAT FOR CKATALP                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.symbol <> old.symbol or new.wartosc <> old.wartosc
    or new.wartstr <> old.wartstr or new.numer <> old.numer) then
  update ckatal set state = -1
    where ckatal.ref = old.ckatal;
end^
SET TERM ; ^
