--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFBLEDY_BI_REPLICAT FOR DEFBLEDY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  execute procedure REPLICAT_STATE('DEFBLEDY',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
END^
SET TERM ; ^
