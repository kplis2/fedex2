--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ARTYKULY_AU0 FOR ARTYKULY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.klasa<>old.klasa or (new.klasa is not null and old.klasa is null)) then
    execute procedure DZIALATR_NALICZ(new.ref,0);
end^
SET TERM ; ^
