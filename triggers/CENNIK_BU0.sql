--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_BU0 FOR CENNIK                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable tktm varchar(40);
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
/*  select KTM from TOWJEDN where REF=new.jedn into :tktm;
  if(:tktm <> new.ktm or (:tktm is null))then
    exception CENNIK_WRONGTOWJEDN;*/ -- MS: komentuje bo dodaje mozliwosc zmiany KTM na towarze
  if(new.cennik <> old.cennik) then
    select WALUTA from DEFCENNIK where ref=new.cennik into new.waluta;
  if(new.jedn is null or (new.jedn = 0)) then
    select ref from TOWJEDN where KTM = new.ktm and GLOWNA = 1 into new.jedn;
  if(new.ktm <> old.ktm) then
    select NAZWA from TOWARY where KTM = new.ktm into new.nazwat;
end^
SET TERM ; ^
