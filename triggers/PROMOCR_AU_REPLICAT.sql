--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCR_AU_REPLICAT FOR PROMOCR                        
  ACTIVE AFTER UPDATE POSITION 1 
as
begin
  if((new.constcena <> old.constcena) or (new.constcena is not null and old.constcena is null) or (new.constcena is null and old.constcena is not null) or
     (new.ilosc <> old.ilosc ) or (new.ilosc is not null and old.ilosc is null) or (new.ilosc is null and old.ilosc is not null) or
     (new.wartosc <> old.wartosc ) or (new.wartosc is not null and old.wartosc is null) or (new.wartosc is null and old.wartosc is not null) or
     (new.upust <> old.upust ) or (new.upust is not null and old.upust is null) or (new.upust is null and old.upust is not null) or
     (new.procent <> old.procent ) or (new.procent is not null and old.procent is null) or (new.procent is null and old.procent is not null) or
     (new.constwal <> old.constwal ) or (new.constwal is not null and old.constwal is null) or (new.constwal is null and old.constwal is not null)
   ) then
     update PROMOCJE set STATE = -1 where ref=new.promocja;
end^
SET TERM ; ^
