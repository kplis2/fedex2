--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SECURITY_LOG_AI_TABLES FOR SECURITY_LOG                   
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable item varchar(40);
begin
  item = new.item;
  if(:item not in ('DELETE','EXPORT','QUERY','REPORT')) then item = 'FIELD';
  if (not exists (select first 1 1 from security_tables where tablename = new.tablename and item = :item)) then
    insert into security_tables(tablename,item) values(new.tablename,:item);
end^
SET TERM ; ^
