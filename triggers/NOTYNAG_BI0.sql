--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYNAG_BI0 FOR NOTYNAG                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.status is null) then new.status = 0;
  if (new.slodef > 0 and new.slopoz >0 ) then
  begin
    execute procedure NAME_FROM_DICTPOSREF(new.slodef, new.slopoz) returning_values new.slokod;
    execute procedure KODKS_FROM_DICTPOS(new.slodef, new.slopoz) returning_values new.slokodks;
  end
  if (new.period is null) then
  begin
    select min(id) from bkperiods
      where company = new.company and ord > 0 and sdate <= new.data and fdate >= new.data into
      new.period;
    if (new.period is null) then exception NOTYNAG_WRONGPERIOD;
  end
  if (new.bcredit is null) then new.bcredit = 0;
  if (new.bdebit is null) then new.bdebit = 0;
  if (new.credit is null) then new.credit = 0;
  if (new.debit is null) then new.debit = 0;
  if (new.interests is null) then new.interests = 0;
  if (new.interestszl is null) then new.interestszl = 0;
  if (new.symbol is null) then new.symbol = '';
end^
SET TERM ; ^
