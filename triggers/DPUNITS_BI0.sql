--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DPUNITS_BI0 FOR DPUNITS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.symbol = '') then exception bad_parameter 'Uzupełnij symbol!';
  if (new.name = '') then exception bad_parameter 'Uzupełnij nazwę!';
end^
SET TERM ; ^
