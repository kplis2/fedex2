--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_AU0 FOR PROPERSRAPS                    
  ACTIVE AFTER UPDATE POSITION 1 
AS
declare variable amount numeric(14,4);
declare variable amountcur numeric(14,4);
declare variable amountresultcalctype smallint;
begin
  if(new.amount <> old.amount or
    new.econtractsdef <> old.econtractsdef or (new.econtractsdef is null and old.econtractsdef is not null) or (new.econtractsdef is not null and old.econtractsdef is null) or
    new.econtractsposdef <> old.econtractsposdef or (new.econtractsposdef is null and old.econtractsposdef is not null) or (new.econtractsposdef is not null 
    and old.econtractsposdef is null) or (new.calcamountresult <> old.calcamountresult)
  ) then begin

    amount = 0;
    select prrs.amountresultcalctype
      from  prschedopers prps
        left join prshopers prrs on (prps.shoper = prrs.ref)
      where prps.ref = new.prschedoper
      into :amountresultcalctype;
    for select sum(por.amount)
      from propersraps por
      where por.prschedoper = new.prschedoper and por.calcamountresult = 1
      into :amountcur
    do begin
      --dla MAX
      if (:amountresultcalctype = 0) then
        if(amountcur > amount) then amount = amountcur;
      --dla MIN
      else if (:amountresultcalctype = 1) then
        if(amountcur < amount) then amount = amountcur;
      --dla SUMA
      else if (:amountresultcalctype = 2) then
        amount = amount + amountcur;
    end

    update prschedopers pso set pso.amountresult = :amount where pso.ref = new.prschedoper;
  end
end^
SET TERM ; ^
