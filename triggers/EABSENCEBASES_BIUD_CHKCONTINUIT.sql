--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCEBASES_BIUD_CHKCONTINUIT FOR EABSENCEBASES                  
  ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 0 
as
declare variable eabsence integer;
begin
--nie mozna pozwolic na zmiany w podstawach nieobecnosci, ktora stanowi ciaglosc
--co najmniej jednej rozliczonej na zamknietej liscie plac nieobecnosci

  if (deleting or inserting or updating and (new.takeit <> old.takeit or new.period <> old.period or
       new.sbase <> old.sbase or (new.sbase is null and old.sbase is not null) or (new.sbase is not null and old.sbase is null) or
       new.vbase <> old.vbase or (new.vbase is null and old.vbase is not null) or (new.vbase is not null and old.vbase is null) or
       new.vworkeddays <> old.vworkeddays or (new.vworkeddays is null and old.vworkeddays is not null) or (new.vworkeddays is not null and old.vworkeddays is null) or
       new.sworkeddays <> old.sworkeddays or (new.sworkeddays is null and old.sworkeddays is not null) or (new.sworkeddays is not null and old.sworkeddays is null) or
       new.workdays <> old.workdays or (new.workdays is null and old.workdays is not null) or (new.workdays is not null and old.workdays is null) or
       new.vworkedhours <> old.vworkedhours or (new.vworkedhours is null and old.vworkedhours is not null) or (new.vworkedhours is not null and old.vworkedhours is null))
  ) then begin
     if (deleting) then
       eabsence = old.eabsence;
     else
       eabsence = new.eabsence;

     if (exists(select first 1 1 from eabsences a
                   join epayrolls r on (r.ref = a.epayroll and r.status > 0)
                   where a.baseabsence = :eabsence and a.ref <> :eabsence)
     ) then
       exception BASEABSENCE_CONTINUITY;
  end
end^
SET TERM ; ^
