--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER XK_GOODS_RELOCATE_BI FOR XK_GOODS_RELOCATE              
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  new.regdate = current_timestamp(0);
  if (new.actabc is null and new.actmwsconstloc is not null) then
    select abc from mwsconstlocs where ref = new.actmwsconstloc
      into new.actabc;
end^
SET TERM ; ^
