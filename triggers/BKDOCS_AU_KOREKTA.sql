--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AU_KOREKTA FOR BKDOCS                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.bkdocsvatcorrectionref is null and old.bkdocsvatcorrectionref is not null) then
    delete from bkvatpos b
      where b.bkdoc = new.ref;
end^
SET TERM ; ^
