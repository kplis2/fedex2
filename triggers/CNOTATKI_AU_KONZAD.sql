--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CNOTATKI_AU_KONZAD FOR CNOTATKI                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.tresc<>old.tresc) then begin
    update KONTAKTY set OPIS = new.tresc where NOTATKA=new.ref;
    update ZADANIA set OPIS = new.tresc where NOTATKA=new.ref;
  end
end^
SET TERM ; ^
