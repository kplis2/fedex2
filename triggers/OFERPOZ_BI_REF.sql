--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERPOZ_BI_REF FOR OFERPOZ                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('OFERPOZ')
      returning_values new.REF;
end^
SET TERM ; ^
