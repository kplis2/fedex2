--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_BI_CHECK_ACCOUNT FOR BANKACCOUNTS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable bank varchar(64);
declare variable control smallint_id;
begin
  new.eaccount = replace(replace(new.account, '-', ''),  ' ',  '');

  if (exists(select first 1 1
               from banki
               where symbol = new.bank
                 and coalesce(zagraniczny, 0) = 0)
      or (coalesce(new.bank, '') = '')) then
    select bankshortname
      from efunc_get_bank_from_acc(new.eaccount)
      into bank;-- PR55109 WG; BS106390

    if (bank is not null) then
      new.bank = bank;

  if (new.hbstatus = 1 and coalesce(new.bankacc, '') = '') then
    exception BANKI_BRAK_BANKACC;

  if (new.bank = '') then
    new.bank = null;
end^
SET TERM ; ^
