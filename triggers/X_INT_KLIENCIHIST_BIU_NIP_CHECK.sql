--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_INT_KLIENCIHIST_BIU_NIP_CHECK FOR INT_KLIENCIHIST                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 100 
as
begin
  --ML21122017 ZG116429

  if((inserting or (updating and new.nip is distinct from old.nip))
    and coalesce(new.nip,'') not similar to '[[:ALNUM:]]*'
  )then
    exception universal 'Niedozwolone znaki w polu NIP. Dopuszczalne tylko znaki alfanumeryczne. NIP: '||coalesce(new.nip,'')
      ||' Klient: '||new.klient||' Skrót: '||coalesce(new.fskrot,'');
end^
SET TERM ; ^
