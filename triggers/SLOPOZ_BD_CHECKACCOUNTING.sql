--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_BD_CHECKACCOUNTING FOR SLOPOZ                         
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable isdist smallint;
begin
  select isdist from SLODEF where ref = old.slownik into :isdist;

  execute procedure check_dict_using_in_accounts(old.slownik, old.kontoks, null);

    -- blokada poprawiania wyróżników jeżeli są obroty na koncie
  if(:isdist>0 and exists(select first 1 1 from decrees d where
      (d.dist1ddef=old.slownik and d.dist1symbol=old.kontoks)
      or (d.dist1ddef=old.slownik and d.dist1symbol=old.kontoks)
      or (d.dist2ddef=old.slownik and d.dist2symbol=old.kontoks)
      or (d.dist3ddef=old.slownik and d.dist3symbol=old.kontoks)
      or (d.dist4ddef=old.slownik and d.dist4symbol=old.kontoks)
      or (d.dist5ddef=old.slownik and d.dist5symbol=old.kontoks)
      )) then
        exception SLOPOZ_USED_IN_DECREES;
end^
SET TERM ; ^
