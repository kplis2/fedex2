--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYOPK_BI_REF FOR STANYOPK                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('STANYOPK')
      returning_values new.REF;
end^
SET TERM ; ^
