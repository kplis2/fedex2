--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU0 FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable rozrachauto varchar(255);
declare variable cnt integer;
declare variable qvat integer;
declare variable autozap smallint;
declare variable wn numeric(14,2);
declare variable ma numeric(14,2);
declare variable dokmag varchar(3);
declare variable dokmagk varchar(3);
declare variable dokmago varchar(3);
declare variable dokmagko varchar(3);
declare variable magazyn char(3);
declare variable dokmagonakcept smallint;
declare variable dokmagonakceptu smallint;
declare variable datadokm timestamp;
declare variable stat integer;
declare variable DOKSYM varchar(255);
declare variable rozrachaut integer;
declare variable slodef integer;
declare variable kontofk KONTO_ID;
declare variable kontofkkl KONTO_ID;
declare variable stankas char(2);
declare variable operkas varchar(25) ;
declare variable pozkas integer;
declare variable pozopkkas integer;
declare variable operkasn varchar(25) ;
declare variable pozkasn integer;
declare variable pozopkkasn integer;
declare variable rapkas integer;
declare variable rkdla varchar(255) ;
declare variable rkdlaskrot varchar(60) ; --XXX ZG133796 MKD
declare variable rkdoknag integer;
declare variable rkdokpoz integer;
declare variable status integer;
declare variable slopoz integer;
declare variable klient integer;
declare variable SYMBROZRACH varchar(30);
declare variable rknumer integer;
declare variable rkstanowisko varchar(3);
declare variable rktyp varchar(3);
declare variable rkusun varchar(20);
declare variable dokmagref integer;
declare variable dokmagblok smallint;
declare variable pozfakref integer;
declare variable autocreaterkrap smallint;
declare variable kwotazapp numeric(14,2);
declare variable local smallint;
declare variable aktulocat varchar(255);
declare variable korekta smallint;
declare variable symbolmag varchar(20);
declare variable wartkaucja numeric(14,2);
declare variable refpozsad integer;
declare variable clopozsad numeric(14,2);
declare variable wartnetzl numeric(14,2);
declare variable sumwartnetzl numeric(14,2);
declare variable sumwartmag numeric(14,2);
declare variable sumwartmagpocle numeric(14,2);
declare variable refdokumnag integer;
declare variable refdokumpoz integer;
declare variable refpozfak integer;
declare variable refnagfak integer;
declare variable kwota numeric(14,2);
declare variable hasdokmag smallint;
declare variable cenamag numeric(14,4);
declare variable refpozfakusl integer;
declare variable notdok integer;
declare variable nip varchar(40);
declare variable regon varchar(20);
declare variable zagraniczny smallint;
declare variable beznipu smallint;
declare variable refdokumnotp integer;
declare variable odchylenie numeric(14,2);
declare variable sposzapplat integer;
declare variable usluginamag smallint;
declare variable datadokkas timestamp;
declare variable sposoblrozrach smallint;
declare variable datanoty timestamp;
declare variable sparowany integer;
declare variable newmag integer;
declare variable symbolfak varchar(255);
declare variable newfak integer;
declare variable oldakcept varchar(60);
declare variable newakcept varchar(60);
declare variable symbole varchar(1024);
declare variable pozfak_ref integer;
declare variable pozfak_ktm varchar(40);
declare variable nazwa_towaru varchar(255);
declare variable korsamoT smallint;
declare variable dataT timestamp;
declare variable przelicznik numeric(14,4);
declare variable iloscm numeric(14,4) ;
declare variable pkwiu varchar(255);
declare variable wersjaref integer;
declare variable refdokumroz integer;
declare variable cenadokumroz numeric(14,4);
declare variable period varchar(6);
declare variable pstatus smallint;
declare variable cntmagpoz0 integer;
begin
  if(new.akceptacja <> old.akceptacja) then begin
    if(old.akceptacja = 0) then
      oldakcept  = 'Niezaakceptowana';
    if(old.akceptacja = 1) then
      oldakcept = 'Zaakceptowana';
    if(old.akceptacja = 9) then
      oldakcept = 'Wycofana do poprawy';
    if(old.akceptacja = 10) then
      oldakcept = 'Wycofana do poprawy bez wycofania dokumentu magazynowego';
    if(new.akceptacja = 0) then
      newakcept = 'Niezaakceptowana';
    if(new.akceptacja = 1) then
      newakcept = 'Zaakceptowana';
    if(new.akceptacja = 9) then
      newakcept = 'Wycofana do poprawy';
    if(new.akceptacja = 10) then
      newakcept = 'Wycofana do poprawy bez wycofania dokumentu magazynowego';
  end

  if(new.akceptacja = 9 and (new.odbiorcaid <> old.odbiorcaid or new.dostawca <> old.dostawca or new.klient <> old.klient
      or new.dulica <> old.dulica or new.dnrdomu <> old.dnrdomu or new.dnrlokalu <> old.dnrlokalu
      or new.dmiasto <> old.dmiasto or new.dkodp <> old.dkodp)) then
  begin
  -- zaktualizuj dokumenty magazynowe powiazane przez pozycje
    for select distinct DOKUMNAG.REF
      from POZFAK
      left join DOKUMPOZ on (DOKUMPOZ.FROMPOZFAK=POZFAK.REF)
      left join DOKUMNAG on (DOKUMNAG.ref=DOKUMPOZ.dokument)
      where POZFAK.dokument=new.ref
      into :newmag
      do begin
        update DOKUMNAG set odbiorcaid = new.odbiorcaid,
        dulica = new.dulica, dnrdomu = new.dnrdomu, dnrlokalu = new.dnrlokalu, 
        dmiasto = new.dmiasto, dkodp = new.dkodp, odbiorca = new.odbiorca,
        klient = new.klient, dostawca = new.dostawca
        where ref = :newmag;
      end
    -- zaktualizuj tez dokumenty magazynowe powiazane przez naglowki
    for select DOKUMNAG.REF
      from DOKUMNAG
      where DOKUMNAG.faktura=new.ref
      into :newmag
      do begin
        update DOKUMNAG set odbiorcaid = new.odbiorcaid,
        dulica = new.dulica, dnrdomu = new.dnrdomu, dnrlokalu = new.dnrlokalu,
        dmiasto = new.dmiasto, dkodp = new.dkodp, klient = new.klient,
        termdost = new.termdost, dostawca = new.dostawca, odbiorca = new.odbiorca,
        kosztdostplat = new.kosztdostplat
        where ref = :newmag;
      end
  end

  if(new.rabat <> old.rabat or (new.kurs <> old.kurs)) then begin
    if(new.kurs<>old.kurs) then
      update POZFAK set KURSCEN=new.kurs
        where dokument = new.ref and walcen=new.waluta and kurscen=old.kurs;
    update POZFAK set RABAT=-101 where dokument = new.ref;
  end
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  if((new.akceptacja = 1) and (old.akceptacja =0 or (old.akceptacja >= 9)) and (:local = 1)) then begin
    ---
    for select ref, ktm, wersjaref, nazwat, pkwiu
      from pozfak
      where dokument = new.ref
      into :pozfak_ref, :pozfak_ktm, :wersjaref, :nazwa_towaru, :pkwiu
    do begin
      if(coalesce(:nazwa_towaru,'')='') then begin
        select nazwa from towary where ktm = :pozfak_ktm into :nazwa_towaru;
        update pozfak set nazwat = :nazwa_towaru where ref = :pozfak_ref;
      end
      if(coalesce(:pkwiu,'')='') then begin
        select pkwiu from WERSJE where ref = :wersjaref into :pkwiu;
        if(coalesce(:pkwiu,'')='') then
          select pkwiu from TOWARY where ktm = :pozfak_ktm into :pkwiu;
        update pozfak set pkwiu = :pkwiu where ref = :pozfak_ref;
      end
    end
    ---
    insert into security_log (logfil, tablename, item, key1, oldvalue, newvalue)
      values ('NAGFAK', 'NAGFAK', 'AKCEPTACJA', new.ref, :oldakcept, :newakcept);

     /*czy klient ma NIP lub REGON*/
     if(new.zakup=0 and new.klient<>0) then begin
       select BEZNIPU from TYPFAK where SYMBOL = new.TYP into :beznipu;
       if(:beznipu is null) then beznipu=0;
       select NIP,REGON,ZAGRANICZNY from KLIENCI where REF=new.klient into :nip, :regon, :zagraniczny;
       if(:nip='') then nip=NULL;
       if(:regon='') then regon=NULL;
       if(:nip is null and :regon is null and :beznipu=0 and (:zagraniczny is null or :zagraniczny = 0))
         then exception NAGFAK_KLIBEZNIP;
     end
     /*weryfikacja ilosci pozycji na fakturze*/
     cnt = null;
     select count(*) from POZFAK where DOKUMENT = new.ref into :cnt;
     if(:cnt is null) then cnt = 0;
     if(:cnt = 0) then exception NAGFAK_ACKNOPOZ;
     /*jc - WERYFIKACJA PROMOCJI PAKIETOWYCH na fakturach "z reki"*/
     if(new.zakup = 0 and new.zaliczkowy = 0 and new.nieobrot = 0 and new.skad = 0) then
       execute procedure pakiet_weryfikuj(new.ref, 'F',0,0,0);
     /* zwolnienie blokad na magazynie*/
     for select REF from POZFAK where DOKUMENT = new.ref into :pozfakref
     do
       delete from STANYREZ where POZZAM=:pozfakref and ZREAL = 2;
     /* naliczenie ceny magazynowej (chwilowo jako wartosc) */
     select DOKMAGONAKCEPT,DOKMAGONAKCEPTU, KOREKTA, USLUGINAMAG, KORSAMO from TYPFAK where SYMBOL = new.TYP
       into :dokmagonakcept, :dokmagonakceptu, :korekta, :usluginamag, :korsamoT;

     if (new.zakup = 1 and new.sad = 1  and :korekta = 1) then
      update POZFAK set CENAMAG = PCENAMAG*ILOSCM where dokument = new.ref;
     else if(new.zakup=1) then
      update POZFAK set CENAMAG = CENANETZL*ILOSC, WARTMAGPOCLE = WARTNETZL where dokument = new.ref;
     odchylenie = 0;
     if(new.sad=1) then begin
       /* weryfikacja czy pozycje towarowe SAD-u sa zwiazane z pozycjami SAD */
       cnt = NULL;
       select count(*) from POZFAK
        where dokument = new.ref and usluga<>1 and refpozsad is null and coalesce(fake,0) = 0
       into :cnt;
       if(:cnt is null) then cnt = 0;
       if(:cnt<>0) then exception NAGFAK_ACKNOPOZSAD;
     end
     /* weryfikacja czy korekta koryguje cokolwiek */
     if(new.sad != 1 and :korekta=1 and new.zaliczkowy=0) then begin
       select count(*) from POZFAK where (DOKUMENT = new.ref)
       and ((CENANET<>PCENANET) or (CENABRU<>PCENABRU) or (ILOSC<>PILOSC) or (NAZWAT <> PNAZWAT))
       into :cnt;
       select count(*) from ROZFAK where (DOKUMENT = new.ref)
       and ((SUMWARTNET <> PSUMWARTNET) or (SUMWARTVAT <> PSUMWARTVAT) or (SUMWARTBRU <> PSUMWARTBRU)
        or (SUMWARTNETZL <> PSUMWARTNETZL) or (SUMWARTVATZL <> PSUMWARTVATZL) or (SUMWARTBRUZL <> PSUMWARTBRUZL))
       into :qvat;
       if(:cnt=0 and :qvat=0) then exception NAGFAK_BEZZMIANY;
     end
      /* sprawdzanie czy korekta nie ma daty wczesniejszej niz dokumenty które koryguje */
     if(:korekta=1 and (:korsamoT <> 1 or :korsamoT is null) ) then begin
       dataT = null;
       select max(NF.data) from NAGFAK NF where NF.korekta = new.ref into :dataT;
       if(:dataT is not null and :dataT > new.data) then exception data_korekty;
     end
     /* weryfikacja czy uslugi z tej faktury sa w pelnej kwocie rozbijane na magazyn lub wcale */
     if(new.zakup=1) then
     for select REF,WARTNETZL - pwartnetzl from POZFAK where DOKUMENT = new.ref and USLUGA=1
     into :pozfakref,:wartnetzl
     do begin
       select count(*), sum(KWOTA)
         from POZFAKUSL
         where REFPOZFAK = :pozfakref and (POZFAKUSL.TRYB=0 or POZFAKUSL.TRYB is null)
         into :cnt,:sumwartnetzl;
       if(:cnt is null) then cnt = 0;
       if(:cnt<>0 and :wartnetzl<>:sumwartnetzl) then exception POZFAKUSL_ZLASUMA;
       if(:cnt=0 and :usluginamag=1 and wartnetzl != 0) then exception POZFAK_NOPOZFAKUSL;
       /* TODO Kuba: do POZFAKUSL nierozliczonych, wskazujacych na zaakceptowane faktury nalezy wygenerowac noty magazynowe */
       /* po rozliczeniu ustawic POZFAKUSL.ROZLICZONO = 2 */
       for select POZFAKUSL.REF,POZFAKUSL.REFNAGFAK,POZFAKUSL.REFDOKUMNAG,POZFAKUSL.KWOTA
          from POZFAKUSL
            left join NAGFAK on (NAGFAK.REF=POZFAKUSL.REFNAGFAK)
          where POZFAKUSL.REFPOZFAK = :pozfakref
            and POZFAKUSL.ROZLICZONO=0
            and (POZFAKUSL.TRYB=0 or POZFAKUSL.TRYB is null)
            and (NAGFAK.akceptacja=1 or POZFAKUSL.refnagfak is null)
            and (NAGFAK.REF is not null or POZFAKUSL.refnagfak is null)
          into :refpozfakusl, :refnagfak, :refdokumnag, :kwota
       do begin
         if(:refdokumnag is not null and :refdokumnag<>0) then begin
           /* rozbicie kwoty uslugi na konkretny dokument magazynowy */
           cntmagpoz0 = 0;
           select sum(WARTOSC), sum(iif(dokumpoz.wartosc = 0 and coalesce(dokumpoz.opk,0) = 0 and coalesce(dokumpoz.foc,0) = 0,1,0))
             from DOKUMPOZ
             where DOKUMENT=:refdokumnag
             into :sumwartmag, :cntmagpoz0;

           if (:cntmagpoz0 > 0) then
              execute procedure message_add(0, new.operakcept, null, null,'Faktura '||new.symbfak||' generuje note magazynową do dokumentu '||coalesce((select symbol from dokumnag where ref = :refdokumnag),'')||' która ma '||:cntmagpoz0||' pozycji z ceną 0',
                null, null, null, null, null, null, null, 0,0, (select first 1 ref from s_defmediums where symbol = 'MESSAGE'))returning_values :cntmagpoz0;

           for
             select ref, :kwota*WARTOSC/(:sumwartmag*ILOSC)
             from DOKUMPOZ
             where (DOKUMENT=:refdokumnag)
             into :refdokumpoz, :cenamag
           do begin
             /*okrelenie, czy jest już zalozona nota mgazynowa na dany dokument*/
             notdok = null;
             select max(DOKUMNOT.ref) from DOKUMNOT join DOKUMNAG on (DOKUMNAG.REF=DOKUMNOT.dokumnagkor)
             join DOKUMPOZ on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
             where DOKUMPOZ.REF = :refdokumpoz and DOKUMNOT.FAKTURA = new.ref and DOKUMNOT.akcept = 0
             into :notdok;
             if(:notdok is nulL) then notdok = 0;
             if(:notdok = 0) then begin
               execute procedure GEN_REF('DOKUMNOT') returning_values :notdok;
               select DATA from DOKUMNAG where REF=:refdokumnag into :datanoty;
                -- jesli okres ksiegowy dla tej daty jest zamkniety to bierzmy do noty date otrzymania faktury
               execute procedure date2period(:datanoty, new.company) returning_values :period;
               select p.status from bkperiods p where p.id=:period and p.company= new.company into :pstatus;
               if (:pstatus=2 and new.dataotrz is not null and new.dataotrz > :datanoty) then datanoty = new.dataotrz;
               insert into DOKUMNOT(ref, GRUPA, TYP,MAGAZYN, MAG2, DATA, DOKUMNAGKOR, FAKTURA, UWAGI,OPERATOR,RODZAJ)
               select :notdok, :notdok, DOKUMNAG.TYP, DOKUMNAG.MAGAZYN, DOKUMNAG.MAG2, :datanoty, :refdokumnag, new.ref, DOKUMNAG.uwagi, new.operakcept, 10
               from DOKUMNAG where dokumnag.ref = :refdokumnag;
             end
             /*poprawa rozpisek tej pozycji dokumentu mag.*/
             for select DOKUMNOTP.REF, DOKUMROZ.REF, DOKUMROZ.CENA
               from DOKUMROZ
               left join DOKUMNOTP on (DOKUMNOTP.dokument = :notdok and DOKUMNOTP.DOKUMROZKOR = DOKUMROZ.REF)
               where DOKUMROZ.POZYCJA = :refdokumpoz
               into :refdokumnotp, :refdokumroz, :cenadokumroz
             do begin
               if(:refdokumnotp is null) then
                 insert into DOKUMNOTP(DOKUMENT, DOKUMROZKOR, CENANEW)
                 values (:notdok, :refdokumroz, :cenadokumroz + :cenamag);
               else
                 update DOKUMNOTP set CENANEW = CENANEW + :cenamag
                 where REF=:refdokumnotp;
             end
           end
         end else begin
           /* rozbicie kwoty uslugi na caly dokument VAT */
           select sum(P.CENAMAG*(P.ILOSC-P.PILOSC))
             from POZFAK P
             where P.DOKUMENT=:refnagfak and P.USLUGA<>1
             into :sumwartmag;
           for
             select P.REF, :kwota * P.CENAMAG/:sumwartmag, D.REF
             from POZFAK P
             left join DOKUMPOZ D on (D.FROMPOZFAK=P.REF)
             where (P.DOKUMENT=:refnagfak) and (P.USLUGA<>1)
             into :refpozfak, :cenamag, :refdokumpoz
           do begin
             /*okrelenie ref'a pozycji magazynowej*/
--             refdokumpoz = null;
--             select max(ref) from DOKUMPOZ where FROMPOZFAK = :refpozfak into :refdokumpoz;
             if(:refdokumpoz is null) then
               select FROMDOKUMPOZ from POZFAK where ref=:refpozfak into :refdokumpoz;
             if(:refdokumpoz is null) then
               exception NAGFAK_CRDOKMAG_BEZORGPOZ;
             /*okrelenie, czy jest już zalozona nota mgazynowa na dany dokument*/
             notdok = null;
             select max(DOKUMNOT.ref)
               from DOKUMNOT
               join DOKUMNAG on (DOKUMNAG.REF=DOKUMNOT.dokumnagkor)
               join DOKUMPOZ on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
               where DOKUMPOZ.REF = :refdokumpoz and DOKUMNOT.FAKTURA = new.ref and DOKUMNOT.akcept = 0
               into :notdok;
             if(:notdok is nulL) then notdok = 0;
             if(:notdok = 0) then begin
               execute procedure GEN_REF('DOKUMNOT') returning_values :notdok;
               select DOKUMENT from DOKUMPOZ where REF=:refdokumpoz into :refdokumnag;
               select DATA from DOKUMNAG where REF=:refdokumnag into :datanoty;
               -- jesli okres ksiegowy dla tej daty jest zamkniety to bierzmy do noty date otrzymania faktury
               execute procedure date2period(:datanoty, new.company) returning_values :period;
               select p.status from bkperiods p where p.id=:period and p.company= new.company into :pstatus;
               if (:pstatus=2 and new.dataotrz is not null and new.dataotrz > :datanoty) then datanoty = new.dataotrz;
               insert into DOKUMNOT(ref, GRUPA, TYP,MAGAZYN, MAG2, DATA, DOKUMNAGKOR, FAKTURA, UWAGI,OPERATOR, RODZAJ)
               select :notdok, :notdok, DOKUMNAG.TYP, DOKUMNAG.MAGAZYN, DOKUMNAG.MAG2, :datanoty, :refdokumnag, new.ref, DOKUMNAG.uwagi, new.operakcept, 10
               from DOKUMNAG where dokumnag.ref = :refdokumnag;
             end
             /*poprawa rozpisek tej pozycji dokumentu mag.*/
             for select DOKUMNOTP.REF, DOKUMROZ.REF, DOKUMROZ.CENA
               from DOKUMROZ
               left join DOKUMNOTP on (DOKUMNOTP.dokument = :notdok and DOKUMNOTP.DOKUMROZKOR = DOKUMROZ.REF)
               where DOKUMROZ.POZYCJA = :refdokumpoz
               into :refdokumnotp, :refdokumroz, :cenadokumroz
             do begin
               if(:refdokumnotp is null) then
                 insert into DOKUMNOTP(DOKUMENT, DOKUMROZKOR, CENANEW)
                 values (:notdok, :refdokumroz, :cenadokumroz + :cenamag);
               else
                 update DOKUMNOTP set CENANEW = CENANEW + :cenamag
                 where REF=:refdokumnotp;
             end
           end
         end
         odchylenie = :odchylenie+:kwota;

         update POZFAKUSL set ROZLICZONO = 2 where REF = :refpozfakusl;
       end
       /* dodatkowo wygeneruj noty wg zasad rabatowania zbiorczego*/
       execute procedure RABATUJ_ZBIORCZO(:pozfakref,1) returning_values :kwota, :symbole, :symbole;
       odchylenie = :odchylenie + :kwota;
     end
     /* powiekszenie ceny magazynowej o clo */
     if(new.sad=1) then
     for
       select REF,coalesce(CLO,0) - coalesce(PCLO,0)
       from POZSAD
       where (POZSAD.faktura=new.ref)
       and coalesce(CLO,0) - coalesce(PCLO,0) <> 0
       into :refpozsad,:clopozsad
     do begin
       select sum(WARTNETZL) from POZFAK where REFPOZSAD=:refpozsad into :sumwartnetzl;
       if(:sumwartnetzl<>0) then begin
         update POZFAK set CENAMAG = (PCENAMAG*ILOSCM)+(:clopozsad * WARTNETZL/:sumwartnetzl),
                           WARTMAGPOCLE = WARTNETZL+(:clopozsad*WARTNETZL/:sumwartnetzl)
         where REFPOZSAD=:refpozsad;
         odchylenie = :odchylenie + :clopozsad;
       end
     end
     /* powiekszenie ceny magazynowej o oplate manipulacyjna */
     if(new.sad=1) then begin
       if(new.kosztdost<>0 and new.kosztdost is not null) then begin
         select sum(WARTMAGPOCLE) from POZFAK where dokument=new.ref into :sumwartmagpocle;
         update POZFAK set CENAMAG = CENAMAG+(new.kosztdost*WARTMAGPOCLE/:sumwartmagpocle),
                           WARTMAGPOCLE = WARTMAGPOCLE+(new.kosztdost*WARTMAGPOCLE/:sumwartmagpocle)
         where dokument=new.ref;
         odchylenie = :odchylenie + new.kosztdost;
       end
     end
     /* powiekszenie ceny magazynowej o koszty uslug z innych faktur */
     if(new.zakup=1) then
     for select POZFAKUSL.ref, POZFAKUSL.REFDOKUMNAG,POZFAKUSL.KWOTA
        from POZFAKUSL
          join POZFAK on (POZFAKUSL.refpozfak=POZFAK.ref)
          left join NAGFAK on (POZFAK.dokument=NAGFAK.ref)
        where POZFAKUSL.REFNAGFAK = new.ref
          and POZFAKUSL.ROZLICZONO=0
          and (POZFAKUSL.TRYB=0 or POZFAKUSL.TRYB is null)
          and NAGFAK.akceptacja=1
        into  :refpozfakusl, :refdokumnag, :kwota
     do begin
       if(:refdokumnag is not null) then begin
         select sum(WARTMAGPOCLE) from POZFAK
           left join DOKUMPOZ on (DOKUMPOZ.ref=POZFAK.fromdokumpoz)
           where POZFAK.dokument=new.ref and DOKUMPOZ.dokument=:refdokumnag
           into :sumwartmagpocle;
         if(:sumwartmagpocle<>0) then begin
           update POZFAK set CENAMAG = CENAMAG+(:kwota*WARTMAGPOCLE/:sumwartmagpocle)
           where POZFAK.dokument=new.ref
           and (select DOKUMENT from DOKUMPOZ where REF=POZFAK.FROMDOKUMPOZ) = :refdokumnag;
           odchylenie = :odchylenie+:kwota;
         end
       end else begin
         select sum(WARTMAGPOCLE) from POZFAK where dokument=new.ref into :sumwartmagpocle;
         if(:sumwartmagpocle<>0) then begin
           update POZFAK set CENAMAG = CENAMAG+(:kwota*WARTMAGPOCLE/:sumwartmagpocle)
           where dokument=new.ref;
           odchylenie = :odchylenie+:kwota;
         end
       end
       update POZFAKUSL set ROZLICZONO=1 where REF=:refpozfakusl;
     end
     /* koncowe naliczenie ceny magazynowej (przejscie z wartosci na cene) */
     if (new.zakup = 1) then
     begin
       for
         select t.przelicz, p.ref, p.cenamag, p.iloscm
           from pozfak p
             join towjedn t on (p.jedn = t.ref)
           where p.dokument = new.ref
           into :przelicznik , :pozfak_ref, :cenamag, :iloscm
       do begin
         if (:cenamag <> 0 and :iloscm <> 0) then
            update POZFAK set CENAMAG = CENAMAG/ILOSCM where ref = :pozfak_ref;
         else begin
           if (coalesce(przelicznik,0) = 0) then przelicznik = 1;
           update pozfak p set p.cenamag = p.cenanetzl / :przelicznik where p.ref = :pozfak_ref;
         end
       end
     end

     /* TODO Kuba: jesli dok. mag. juz istnieja to nalezy wygenerowac noty magazynowe */
     /* na podstawie wyznaczonych przeze mnie cen. Jesli ich nie ma to wystarczy uwzglednic na nich naliczona przeze mnie cene */
     /* DONE */
     /* automatyczne wygeneroawnie dokumentu magazynowego, jeli już nie powiazany */
     select max(BLOKADA) from DOKUMNAG where  DOKUMNAG.FAKTURA = new.ref /*and zrodlo = 3*/ into :dokmagblok;
     if(:dokmagonakceptu is null) then dokmagonakceptu = 0;
     if((:dokmagonakcept > 0
         and (:dokmagonakceptu = 0 or new.dodokmag > 0 or new.skad=2))
         and ((new.refdokm is null) or (:dokmagblok = 2) or (new.zakup = 1))
         and ((new.nieobrot=0) or (new.nieobrot=2))
       ) then begin

       select TYPDOK, TYPDOKK,TYPDOKO,TYPDOKKO
         from STANTYPFAK where STANFAKTYCZNY = new.stanowisko and TYPFAK = new.typ
         into :dokmag,:dokmagk,:dokmago,:dokmagko;
       datadokm = new.datasprz;
       if(:dokmagonakcept = 2) then dokmagonakcept = 3;
       else dokmagonakcept = 2;
       execute procedure GETCONFIG('AKTULOCAT') returning_values :aktulocat;
       hasdokmag = 0;
       if((new.refdokm >= 0 and old.akceptacja<>9) or (new.skad=2)) then hasdokmag = 1;
       if(:hasdokmag = 1 and old.akceptacja = 10) then hasdokmag = 2;
       for select distinct POZFAK.MAGAZYN from POZFAK
       left join DEFMAGAZ on (DEFMAGAZ.symbol = POZFAK.MAGAZYN)
       left join ODDZIALY on (ODDZIALY.oddzial = DEFMAGAZ.oddzial)
       where POZFAK.DOKUMENT = new.ref and ODDZIALY.LOCATION=:aktulocat
       into :magazyn
       do begin
         execute procedure NAGFAK_CREATE_DOKMAG(new.ref, :magazyn, :dokmag, :dokmagk, :dokmago, :dokmagko, :datadokm, :dokmagonakcept, :hasdokmag, new.oldnumblock) returning_values :stat,:doksym;
       end
       if(exists(select ref from pozfak where magazyn is null)) then begin
         execute procedure NAGFAK_CREATE_DOKMAG(new.ref, NULL, :dokmag, :dokmagk, :dokmago, :dokmagko, :datadokm, :dokmagonakcept, :hasdokmag, new.oldnumblock) returning_values :stat,:doksym;
       end
       if(:hasdokmag<2 and old.akceptacja<>10 /*and :dokmagonakcept=3  JCZ: w PBH zostalo zdjete, aby zawsze generowal noty, najwyzej ich nie akceptowal*/) then begin
         execute procedure NAGFAK_CREATE_DOKUMNOT(new.ref,:datadokm,:dokmagonakcept) returning_values :sparowany,:doksym;
       end
       if(new.oldnumblock > 0) then begin
         update NUMBERBLOCK set BLOCKMASTER = null where BLOCKMASTER = new.oldnumblock;
         /*od-uzalezenienie blokad*/

       end
/*       select sum(WARTOSC) from DOKUMNAG where FAKTURA = new.ref into :sumwartmag;
       if(:sumwartmag is null) then sumwartmag = 0;
       odchylenie = :odchylenie - :sumwartmag;*/
       select sum(WARTOSC) from DOKUMNOT where FAKTURA = new.ref and REF=GRUPA into :sumwartmag;
       if(:sumwartmag is null) then sumwartmag = 0;
       odchylenie = :odchylenie - :sumwartmag;
       update NAGFAK set odchylkor = :odchylenie where REF=new.ref;
     end else if(old.akceptacja = 9 and new.refdokm is not null) then begin
       for select DOKUMNAG.REF from DOKUMNAG join defdokum on (DOKUMNAG.TYP = DEFDOKUM.SYMBOL)
       where DOKUMNAG.FAKTURA = new.ref and zrodlo = 3
       order by DEFDOKUM.wydania desc
       into :dokmagref
       do begin
         update DOKUMNAG set BLOKADA = 0 where ref=:dokmagref;
         update DOKUMNAG set akcept = 0 where ref=:dokmagref;
         delete from DOKUMNAG where REF = :dokmagref;
       end
     end

     if(old.akceptacja=9) then begin
       --sprawdzanie czy istnieja powiazane dok. magazynowe bez pozycji (jezeli tak to kasujemy je)
       update dokumnag DN set blokada=0 where DN.faktura=new.ref and DN.akcept=0 and (not exists (select first 1 1 from dokumpoz DP where DP.dokument=DN.ref));
       delete from dokumnag DN where DN.faktura=new.ref and DN.akcept=0 and (not exists (select first 1 1 from dokumpoz DP where DP.dokument=DN.ref));
     end
     /* automatyczne tworzenie rozrachunku i operacji do niego */
     select GOTOWKA from STANSPRZED where  stanowisko=new.stanowisko into :autozap;
     if(new.zakup = 0) then
       execute procedure GETCONFIG('ROZRACHAUTO') returning_values :rozrachauto;
     else
       execute procedure GETCONFIG('DROZRACHAUTO') returning_values :rozrachauto;
     if(:rozrachauto is null or (:rozrachauto = '')) then rozrachauto = '0';
     /* automat nie dotyczy akceptacji nieborotowych oraz zwiazaynch z paragonami - jeszcze obrotowymi,
        bo jesli powastaje na podstawie nieobrotowego ( proformy), to rozrachunek ma sie tworzyc */
     if(new.nieobrot = 1 or (new.nieobrot = 3)) then rozrachauto = '-1';
     cnt = null;
     select count(*) from NAGFAK where REFFAK = new.ref and NIEOBROT = 0 into :cnt;
     if(:cnt > 0) then autozap = 0;
     rozrachaut = cast(:rozrachauto as integer);
     if(new.zakup = 0) then begin
         select ref, prefixfk from SLODEF where TYP='KLIENCI' into :slodef, :kontofk;
         if(:slodef is null or (:slodef = 0)) then exception NAGFAK_BEZSLOKL;
         /*zamina klienta i slopoz, jesli przesila sposob platnosci*/
         if(new.sposzap > 0) then
           select POSREDNIKSTALY from PLATNOSCI where ref = new.sposzap into :sposzapplat;
         slopoz = new.platnik;
         klient = new.platnik;
         if(:sposzapplat is null or :sposzapplat = 0) then sposzapplat = :slopoz;
         select KONTOFK from KLIENCI where REF=:klient into :kontofkkl;
         symbrozrach = new.symbol;
     end else if(new.sad<>1) then begin
         select ref, prefixfk from SLODEF where TYP='DOSTAWCY' into :slodef, :kontofk;
         if(:slodef is null or (:slodef = 0)) then exception NAGFAK_BEZSLODOST;
         select KONTOFK from dostawcy where REF=new.dostawca into :kontofkkl;
         if(new.symbfak is null or (new.symbfak = '')) then exception NAGFA_BEZSYMBFAK;
         slopoz = new.dostawca;
         klient = NULL;
         sposzapplat = :slopoz;
         symbrozrach = new.symbfak;
     end else begin
         if(new.symbfak is null or (new.symbfak = '')) then exception NAGFA_BEZSYMBFAK;
         sposzapplat = :slopoz;
     end
     /* automatyczne naliczenie zaplaty*/
       /*bezposrednio na naglowku*/
     if(:autozap > 0 and :rozrachaut = 0 and new.sad<>1) then begin
       if(korekta =0) then
         update NAGFAK set zaplacono = KWOTAZAP where ref=new.ref;
       else if(korekta=1) then
         update NAGFAK set zaplacono = -KWOTAZAP where ref=new.ref;
     /*za pomoca rozrachunkow*/
     end else if(:autozap > 0 and :rozrachaut > 0 and new.sad<>1) then begin
       --odczyt typu rozliczniea z typu dok. VAT
       select sposoblrozrach from TYPFAK where SYMBOL = new.TYP into :sposoblrozrach;
       if(:sposoblrozrach is null) then sposoblrozrach = 0;
       if(:sposoblrozrach = 2) then begin
         wn = 0;
         ma = 0;
       end
       else if(:sposoblrozrach = 0) then begin
         if(new.zakup = 0) then begin
           if((new.sumwartbru - new.psumwartbru) >=0) then
             wn = (new.sumwartbru - new.psumwartbru);
           else
             ma = -(new.sumwartbru - new.psumwartbru);
         end else begin
           if((new.sumwartbru - new.psumwartbru) >=0) then
             ma = (new.sumwartbru - new.psumwartbru);
           else
             wn = -(new.sumwartbru - new.psumwartbru);
         end
       end else if(:sposoblrozrach = 1) then begin
         if(new.zakup = 0) then begin
           if((new.esumwartbru - new.pesumwartbru) >=0) then
             wn = (new.esumwartbru - new.pesumwartbru);
           else
             ma = -(new.esumwartbru - new.pesumwartbru);
         end else begin
           if((new.esumwartbru - new.pesumwartbru) >=0) then
             ma = (new.esumwartbru - new.pesumwartbru);
           else
             wn = -(new.esumwartbru - new.pesumwartbru);
         end
       end
       else
         exception NAGFAK_EXPT;
       if(:kontofk is null) then kontofk = '';
       if(:kontofkkl is null) then kontofkkl = '';
       kontofkkl = :kontofk || :kontofkkl;
       cnt = null;
       select count(*) from ROZRACH where SLOPOZ = :slopoz and SLODEF = :slodef and KONTOFK = :kontofkkl and SYMBFAK = :symbrozrach and company = new.company into :cnt;
       if(:cnt is null) then cnt = 0;
       if(:cnt = 0 and :sposoblrozrach <> 2) then
         insert into ROZRACH(company, SLODEF, SLOPOZ,KONTOFK, KLIENT,SYMBFAK,FAKTURA, DATAOTW,DATAPLAT,WALUTOWY,WALUTA, SKAD, BANKACCOUNT)
         values(new.company, :slodef, :slopoz, :kontofkkl, :klient, :symbrozrach,new.ref,new.data, new.termzap,new.walutowa,new.waluta,1, new.slobankacc);
       if(:rozrachaut > 1 and :sposoblrozrach <> 2) then begin
         if(new.zakup = 0) then
           insert into ROZRACHP(company, SLODEF, SLOPOZ,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,TRESC,WINIEN,MA,WALUTA,KURS, SKAD, SETTLCREATE, STABLE, SREF)
           values(new.company, :slodef,:slopoz,:klient,:symbrozrach,'Akcept. dok. sprz.',new.ref,new.data,'Otwarcie rozrachunku',:wn,:ma,new.waluta, new.kurs,1,1, 'NAGFAK', new.ref);
         else
           insert into ROZRACHP(company, SLODEF, SLOPOZ,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,TRESC,WINIEN,MA,WALUTA,KURS, SKAD, SETTLCREATE, STABLE, SREF)
           values(new.company, :slodef,:slopoz,:klient,:symbrozrach,'Akcept. dok. zakupu',new.ref,new.data,'Otwarcie rozrachunku',:wn,:ma,new.waluta, new.kurs,1,1, 'NAGFAK', new.ref);
       end
       if(:sposzapplat <> :slopoz ) then begin
         --przeniesienie rozrachunku na zaplatnika
           wn = 0;
           ma = 0;
           if(:sposoblrozrach = 0) then begin
             if((new.sumwartbru - new.psumwartbru) >=0) then
               wn = (new.sumwartbru - new.psumwartbru) - new.kwotazap;
             else
               ma = -(new.sumwartbru - new.psumwartbru) - new.kwotazap;
           end
           else if(:sposoblrozrach = 1) then begin
             if((new.esumwartbru - new.pesumwartbru) >=0) then
               wn = (new.esumwartbru - new.pesumwartbru) - new.kwotazap;
             else
               ma = -(new.esumwartbru - new.pesumwartbru) - new.kwotazap;
           end
           else
             exception NAGFAK_EXPT;
           if(:ma <> 0 or :wn <> 0) then begin
             --najpierw zdjecie z rozrachunku wasciwego
             if(new.zakup = 0) then
               insert into ROZRACHP(company, SLODEF, SLOPOZ,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,TRESC,WINIEN,MA,WALUTA,KURS, SKAD, STABLE, SREF)
               values(new.company, :slodef,:slopoz,:klient,:symbrozrach,'Akcept. dok. sprz.',new.ref,new.data,'Przeniesienie na zaplatnika',:ma,:wn,new.waluta, new.kurs,1, 'NAGFAK', new.ref);
             else
                insert into ROZRACHP(company, SLODEF, SLOPOZ,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,TRESC,WINIEN,MA,WALUTA,KURS, SKAD, STABLE, SREF)
                values(new.company, :slodef,:slopoz,:klient,:symbrozrach,'Akcept. dok. zakupu',new.ref,new.data,'Przeniesienie na zaplatnika',:ma,:wn,new.waluta, new.kurs,1, 'NAGFAK', new.ref);
             select KONTOFK from KLIENCI where REF=:sposzapplat into :kontofkkl;
             if(:kontofk is null) then kontofk = '';
             if(:kontofkkl is null) then kontofkkl = '';
             kontofkkl = :kontofk || :kontofkkl;
             insert into ROZRACH(company, SLODEF, SLOPOZ,KONTOFK, KLIENT,SYMBFAK,FAKTURA, DATAOTW,DATAPLAT,WALUTOWY,WALUTA, SKAD, BANKACCOUNT)
             values(new.company, :slodef, :sposzapplat, :kontofkkl, :sposzapplat, :symbrozrach,new.ref,new.data, new.termzap,new.walutowa,new.waluta,1, new.slobankacc);
             if(:rozrachaut > 1) then begin
                insert into ROZRACHP(company, SLODEF, SLOPOZ,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,TRESC,WINIEN,MA,WALUTA,KURS, SKAD, SETTLCREATE, STABLE, SREF)
                values(new.company, :slodef,:sposzapplat,:sposzapplat,:symbrozrach,'Akcept. dok. zakupu',new.ref,new.data,'Przeniesienie na zaplatnika',:wn,:ma,new.waluta, new.kurs,1,1, 'NAGFAK', new.ref);
             end
           end
       end
     end
     /*tworzenie dokumentow kasowych*/
     if(:autozap > 0 and new.kwotazap <>0 and new.sad<>1) then begin
           /* naliczenie zapłaty albo przez rozrachunek, albo przez kasę* */
           select STANTYPFAK.rkdefoper, STANTYPFAK.rkpozoper, STANTYPFAK.rkpozopkoper,
                  STANTYPFAK.rkdefopern, STANTYPFAK.rkpozopern, STANTYPFAK.rkpozopkopern
           from STANTYPFAK join STANSPRZED on ( STANTYPFAK.stansprzed = STANSPRZED.stanowisko)
           where STANTYPFAK.STANSPRZED = new.stanowisko and stantypfak.typfak = new.typ
           into  :operkas, :pozkas,:pozopkkas, :operkasn, :pozkasn,:pozopkkasn;
           stankas = new.rkstnkas;
           kwotazapp = new.kwotazap;
           /*stara wersja
           if(  (:korekta = 1 and ((new.sumwartbru > new.psumwartbru) or (new.sumkaucja > new.psumkaucja)))
             or (:korekta = 0 and (new.esumwartbru < new.pesumwartbru)) )
           */
           if ((:korekta = 1 and new.dozaplaty > 0) or (:korekta = 0 and new.dozaplaty < 0))
           then begin
             operkas = :operkasn;
             pozkas = :pozkasn;
             pozopkkas = :pozopkkasn;
           end else if(:kwotazapp < 0) then begin
             kwotazapp = - :kwotazapp;
             operkas = :operkasn;
             pozkas = :pozkasn;
             pozopkkas = :pozopkkasn;
           end
           if((:stankas <> '') and (:operkas <> '') and (:pozkas > 0)) then begin
             if(new.zakup = 0) then datadokkas = new.data;
             else datadokkas = new.datasprz;
             select min(REF) from RKRAPKAS where  STANOWISKO = :stankas and DATAOD <= :datadokkas and DATADO >= :datadokkas
               and status = 0 into :rapkas;
             if(:rapkas is null or (:rapkas = 0))then begin
               select AUTORAPKASCREATE from STANSPRZED
                 where stanowisko = new.STANOWISKO
                 into :autocreaterkrap;
               if(:autocreaterkrap > 0) then begin
                 insert into RKRAPKAS(STANOWISKO,DATAOD,DATADO) values(:stankas, :datadokkas, :datadokkas);
                 select min(REF) from RKRAPKAS where  STANOWISKO = :stankas and DATAOD <= :datadokkas and DATADO >= :datadokkas
                  and status = 0 into :rapkas;
               end
             end
             if(:rapkas > 0) then begin
               /*sprawdzenie, czy nagłówek dokumentu juz istnieje*/
               rkdoknag = null;
               if(new.zakup = 0) then begin
                 select NAZWA,FSKROT from KLIENCI where REF=:slopoz into :rkdla,:rkdlaskrot;
                 if(:rkdla ='' or (:rkdla is null)) then exception NAGFAK_KLIBEZNAZWY;
               end else begin
                 select NAZWA,ID from DOSTAWCY where REF=:slopoz into :rkdla, :rkdlaskrot;
                 if(:rkdla ='' or (:rkdla is null)) then exception NAGFAK_DOSTBEZNAZWY;
               end
               if(old.akceptacja >= 9)then begin
                 select max(REF) from RKDOKNAG where FROMFAK = new.ref into :rkdoknag;
                 if(:rkdoknag > 0) then begin
                   update RKDOKNAG set OPERACJA = :operkas, SLODEF = :slodef, SLOPOZ = :slopoz,
                     DLA = :rkdla, DLASKROT=:rkdlaskrot, OPIS = 'Zaplata automatyczna systemu sprzedazy', OPERATOR = new.operakcept,
                     ANULOWANY=0, ANULOPIS = '', RKSTNKASPIN=new.rkstnkaspin where REF=:rkdoknag;
                 end
               end
               if(:rkdoknag is null) then begin
                 execute procedure GEN_REF('RKDOKNAG') returning_values :rkdoknag;
                 /*naglowej dokumentu kasowego*/
                 if(new.zakup = 0) then
                    insert into RKDOKNAG(REF, RAPORT, DATA, OPERACJA, SLODEF, SLOPOZ,
                     OPIS,KURS, WALUTOWY,DLA,DLASKROT,FROMFAK, OPERATOR, RKSTNKASPIN)
                    values(:rkdoknag, :rapkas, :datadokkas, :operkas, :slodef, :slopoz,
                       'Zaplata automatyczna systemu sprzedazy',1,0,:rkdla,:rkdlaskrot,new.ref, new.operakcept, new.rkstnkaspin);
                 else
                    insert into RKDOKNAG(REF, RAPORT, DATA, OPERACJA, SLODEF, SLOPOZ,
                     OPIS,KURS, WALUTOWY,DLA,DLASKROT,FROMFAK, OPERATOR, RKSTNKASPIN)
                    values(:rkdoknag, :rapkas, :datadokkas, :operkas, :slodef, :slopoz,
                       'Zaplata automatyczna systemu dostaw',1,0,:rkdla,:rkdlaskrot,new.ref, new.operakcept, new.rkstnkaspin);
               end
               /*pozycja dokumentu kasowego*/
               if(new.sumkaucja - new.psumkaucja <> 0) then begin
                 for select DOKUMNAG.SYMBOL, dokumnag.wartkaucja
                 from DOKUMNAG join DEFDOKUM on (DOKUMNAG.TYP = DEFDOKUM.SYMBOL)
                 where DOKUMNAG.FAKTURA = new.ref and DEFDOKUM.opak > 0 and
                    ((DOKUMNAG.RKDOKNAG is null) or (DOKUMNAG.RKDOKNAG = 0))
                    and dokumnag.wartkaucja > 0
                 into :symbolmag, :wartkaucja
                 do begin
                   if(:wartkaucja > :kwotazapp)then wartkaucja = :kwotazapp;
                   if(:wartkaucja > 0) then begin
                     execute procedure GEN_REF('RKDOKPOZ') returning_values :rkdokpoz;
                     insert into RKDOKPOZ(REF,DOKUMENT,ROZRACHUNEK,FAKTURA, KWOTA, KWOTAZL,POZOPER)
                       values(:rkdokpoz, :rkdoknag, :symbolmag, new.ref,:wartkaucja, :wartkaucja, :pozopkkas);
                     kwotazapp = :kwotazapp - :wartkaucja;
                   end
                 end
                 if(:kwotazapp < 0) then kwotazapp = 0;
               end
               if(:kwotazapp > 0) then begin
                 execute procedure GEN_REF('RKDOKPOZ') returning_values :rkdokpoz;
                 insert into RKDOKPOZ(REF,DOKUMENT,ROZRACHUNEK,FAKTURA, KWOTA, KWOTAZL,POZOPER)
                  values(:rkdokpoz, :rkdoknag, :symbrozrach, new.ref,:kwotazapp, :kwotazapp, :pozkas);
               end
               execute procedure RK_WYSTAW_DOKUMENT(:rkdoknag) returning_values :status;
               update RKDOKNAG set BLOKADA = 0 where BLOKADA >0 and REF=:rkdoknag;
               update NAGFAK set RKDOKNAG = :rkdoknag where REF=new.ref;
             end
               else exception RKRAPKAS_NORAPORT;
           end else if(:rozrachaut > 2) then begin
             /*dokumentow kasowych nie wystawiano - ale mozna wpisac cos bezposrednio do rozrachunkow*/
             wn = 0;
             ma = 0;
             if(new.zakup = 0) then begin
               if((new.sumwartbru - new.psumwartbru) >=0) then
                 ma = new.kwotazap;
               else
                 wn =  new.kwotazap;
             end else begin
               if((new.sumwartbru - new.psumwartbru) >=0) then
                 wn = new.kwotazap;
               else
                 ma =  new.kwotazap;
             end
             insert into ROZRACHP(company, SLODEF, SLOPOZ,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,TRESC,WINIEN,MA,WALUTA,KURS, SKAD)
             values(new.company, :slodef, :slopoz, :klient,:symbrozrach,'Zaplata gotówkowa',new.ref,new.data,'Automatyczna zaplata',:wn,:ma,new.waluta, new.kurs, 1);
           end
     end else if(new.rkdoknag > 0 and old.akceptacja <> 1 and new.sad<>1) then begin
     /*sprawdzenie, czy nie ma dokumentow kasowych do faktury, ktora przestala byc kasowa*/
             rkdoknag = new.rkdoknag;
             select NUMER, STANOWISKO,TYP from RKDOKNAG where REF=new.rkdoknag into :rknumer, :rkstanowisko, :rktyp;
             execute procedure GETCONFIG('KASSPRZUSU') returning_values :rkusun;
             if(:rkusun = '1') then begin
               if(:rknumer > 0) then begin
                 cnt = null;
                 select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer into :cnt;
                 if(:cnt > 0) then begin
                  rkusun = '';
                 end
               end
             end else if(:rkusun = '2') then
               rkusun = '1';
             else rkusun = '';
             rkusun = '1';
             if(:rkusun <>'')then begin
               execute procedure GETCONFIG('KASUSUNLAST') returning_values :rkusun;
               if(:rkusun = '1') then begin
                 /*sprawdzenie, czy usuwac, czy anulowac jednak*/
                 cnt = null;
                 select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and ANULOWANY = 0 and TYP=:rktyp and NUMER > :rknumer into :cnt;
                 if(:cnt > 0) then rkusun = '';
               end
               if(:rkusun ='1' and :rknumer > 0) then begin
                 update RKDOKNAG set NUMER = 0 where REF=new.rkdoknag;
                 delete from  RKDOKNAG where REF=new.rkdoknag;
                 rkdoknag = null;
               end else
                 update RKDOKNAG set ANULOWANY=1, BLOKADA = 0, OPIS = 'Wycofany przez zmiane sposobu platnosci',ANULOPIS='Zmiana parametrów dok. sprzedaży' where REF=new.rkdoknag;
             end else
               exception NAGFA_ACK_SARKDOKNAG;
             if(:rkdoknag is null) then
               update NAGFAK set RKDOKNAG = null where RKDOKNAG is not null and REF=new.ref;

     end
     /*aktualizacja cen zakupu*/
     if(new.zakup = 1) then
       execute procedure nagfak_aktucenzak(new.ref);
     /*aktualizacja znacznikow rozliczenia magazynowego*/
     execute procedure NAGFAK_OBL_MAGROZLICZ(new.ref);
    /*sprawdzanie uzycia dwoch wykluczjacych mechanizmow rozbicia zaplaty*/
    if (exists(select ref from NAGFAKTERMPLAT where DOKTYP='F' and DOKREF = new.ref)
      and exists(select ref from pozfak where dokument = new.ref and termzap is not null)) then begin
        exception NAGFAK_AKCEPT 'Kontrachent oraz pozycje fak. posiadają różne terminy zaplat !';
    end
  end

 if(:local = 1) then begin
  /*zaktualizowanie symbolu dokumentu VAT na dokumentach magazynowcyh*/
  if (new.symbol <> old.symbol or (new.symbol is null and old.symbol is not null or (new.symbol is not null and old.symbol is null))) then begin
    -- zaktualizuj wszystkie dokumenty magazynowe powiazane przez pozycje
    for select distinct DOKUMNAG.REF,DOKUMNAG.FAKTURA
      from POZFAK
      left join DOKUMPOZ on (DOKUMPOZ.FROMPOZFAK=POZFAK.REF)
      left join DOKUMNAG on (DOKUMNAG.ref=DOKUMPOZ.dokument)
      where POZFAK.dokument=new.ref
      into :newmag,:newfak
      do begin
        execute procedure DOKUMNAG_GET_SYMBFAK(:newmag,:newfak) returning_values :symbolfak;
        update dokumnag set dokumnag.symbolfak = :symbolfak where dokumnag.ref = :newmag and dokumnag.symbolfak<>:symbolfak;
      end
    -- zaktualizuj tez dokumenty magazynowe powiazane przez naglowki
    for select DOKUMNAG.REF,DOKUMNAG.FAKTURA
      from DOKUMNAG
      where DOKUMNAG.faktura=new.ref
      into :newmag,:newfak
      do begin
        execute procedure DOKUMNAG_GET_SYMBFAK(:newmag,:newfak) returning_values :symbolfak;
        update dokumnag set dokumnag.symbolfak = :symbolfak where dokumnag.ref = :newmag  and dokumnag.symbolfak<>:symbolfak;
      end
  end
  if(new.nieobrot = 2 and old.nieobrot = 0 and new.grupadok <> new.ref) then
    update NAGFAK set NIEOBROT = 2 where NIEOBROT = 0 and GRUPADOK = new.grupadok;
  else if(new.nieobrot = 0 and old.nieobrot = 2 and new.grupadok <> new.ref) then
    update NAGFAK set NIEOBROT = 0 where NIEOBROT = 2 and GRUPADOK = new.grupadok;
  end
  if (new.listywysd is not null and new.akceptacja = 1 and old.akceptacja <> 1) then
    execute procedure listywysd_obliczsymbolfak(new.listywysd);
  /* uzupelnainie tabeli splat po zmianie klienta/dostawcy */
  if(new.skad = 0) then begin
    if (new.klient <> old.klient or new.dostawca<>old.dostawca) then begin
      delete from NAGFAKTERMPLAT where doktyp = 'F' and dokref = new.ref;
      if (new.dostawca is not null) then begin
        INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
          select 'F', new.ref, new.data +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 6 and k.slopoz = new.dostawca;
      end else if(new.klient is not null) then begin
        INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
          select 'F', new.ref, new.data +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 1 and k.slopoz = new.klient;
      end
    end
  end
end^
SET TERM ; ^
