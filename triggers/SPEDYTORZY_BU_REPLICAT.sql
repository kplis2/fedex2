--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTORZY_BU_REPLICAT FOR SPEDYTORZY                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if(   (new.symbol <> old.symbol ) or (new.symbol is not null and old.symbol is null) or (new.symbol is null and old.symbol is not null)
     or (new.nazwa <> old.nazwa) or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null)
     or (new.ulica <> old.ulica ) or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null)
     or (new.nrdomu <> old.nrdomu ) or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null)
     or (new.nrlokalu <> old.nrlokalu ) or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null)
     or (new.miasto <> old.miasto ) or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null)
     or (new.kodp <> old.kodp ) or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null)
     or (new.osoba <> old.osoba ) or (new.osoba is not null and old.osoba is null) or (new.osoba is null and old.osoba is not null)
     or (new.numerator <> old.numerator ) or (new.numerator is not null and old.numerator is null) or (new.numerator is null and old.numerator is not null)

  ) then
   waschange = 1;

  execute procedure rp_trigger_bu('SPEDYTORZY',old.symbol, null, null, null, null, old.token, old.state,
        new.symbol, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
