--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_BU_REPLICAT FOR RKRAPKAS                       
  ACTIVE BEFORE UPDATE POSITION 2 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.ref <> old.ref)
   or (new.dataod <> old.dataod)
   or (new.datado <> old.datado)
   or (new.stanowisko <> old.stanowisko) or (new.stanowisko is not null and old.stanowisko is null) or (new.stanowisko is null and old.stanowisko is not null)
   or (new.okres  <> old.okres ) or (new.okres is not null and old.okres is null) or (new.okres is null and old.okres is not null)
   or (new.numer <> old.numer ) or (new.numer is not null and old.numer is null) or (new.numer is null and old.numer is not null)
   or (new.symbol <> old.symbol ) or (new.symbol is not null and old.symbol is null) or (new.symbol is null and old.symbol is not null)
   or (new.przychod <> old.przychod ) or (new.przychod is not null and old.przychod is null) or (new.przychod is null and old.przychod is not null)
   or (new.przychodzl <> old.przychodzl ) or (new.przychodzl is not null and old.przychodzl is null) or (new.przychodzl is null and old.przychodzl is not null)
   or (new.rozchod <> old.rozchod ) or (new.rozchod is not null and old.rozchod is null) or (new.rozchod is null and old.rozchod is not null)
   or (new.rozchodzl <> old.rozchodzl ) or (new.rozchodzl is not null and old.rozchodzl is null) or (new.rozchodzl is null and old.rozchodzl is not null)
   or (new.okresnum <> old.okresnum ) or (new.okresnum is not null and old.okresnum is null) or (new.okresnum is null and old.okresnum is not null)
   or (new.status <> old.status and not ((new.status = 3 and old.status = 2) or (new.status = 2 and old.status = 1) )) or (new.status is not null and old.status is null) or (new.status is null and old.status is not null)

  ) then
   waschange = 1;

  execute procedure rp_trigger_bu('RKRAPKAS',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
