--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_AI_TOWARY FOR ATRYBUTY                       
  ACTIVE AFTER INSERT POSITION 3 
as
declare variable atrakr varchar(20);
declare variable sql varchar(255);
begin
  if (new.nrwersji=0 and new.wartosc is not null) then
  begin
    select DEFCECHY.redundantakr from DEFCECHY
      where defcechy.symbol=new.cecha
      into :atrakr;

    if (atrakr<>'') then
    begin
      sql = 'update TOWARY set '||atrakr||'='''||new.wartosc||''' where KTM='''||new.ktm||'''';
      execute statement :sql;
    end
  end
end^
SET TERM ; ^
