--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRPAYROLLS_AI0 FOR ECONTRPAYROLLS                 
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
--  insert into econtrpayrollsnag (operator, bdate, edate, econtrpayroll, opendate, status, employee)
--    select distinct operator.ref, new.dataod, new.datado, new.ref,current_date,0, new.employee
--      from operator
--      where operator.employee is not null and operator.aktywny = 1 and operator.autopresencelistsnag = 1;
  insert into econtrpayrollsnag (bdate, edate, econtrpayroll, opendate, status, employee)
    select distinct new.dataod, new.datado, new.ref,current_date, 0, employees
      from econtractsassign;
end^
SET TERM ; ^
