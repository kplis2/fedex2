--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_BI_REF FOR EPAYROLLS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EPAYROLLS')
      returning_values new.REF;
end^
SET TERM ; ^
