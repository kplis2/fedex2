--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_BI_REF FOR MWSSTOCK                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSSTOCK') returning_values new.ref;
end^
SET TERM ; ^
