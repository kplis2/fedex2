--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_KLIENCIHIST_BU_BLOKADA FOR INT_KLIENCIHIST                
  ACTIVE BEFORE UPDATE POSITION 5 
as
begin
  --ML blokada zmiany danych rozliczeniowych w zaleznosci od is_manual
  -- jezeli zadne z pól w ifie sie nie zmienilo, to znaczy, ze zminily sie dane
  --ZG107762
  if(new.klient is distinct from old.klient
    or new.fskrot is distinct from old.fskrot
    or new.nazwa is distinct from old.nazwa
    or new.imie is distinct from old.imie
    or new.nazwisko is distinct from old.nazwisko
    or new.ulica is distinct from old.ulica
    or new.nrdomu is distinct from old.nrdomu
    or new.nrlokalu is distinct from old.nrlokalu
    or new.miasto is distinct from old.miasto
    or new.poczta is distinct from old.poczta
    or new.kodp is distinct from old.kodp
    or new.wojewodztwo is distinct from old.wojewodztwo
    or new.krajid is distinct from old.krajid
    or new.nip is distinct from old.nip
    or new.zagraniczny is distinct from old.zagraniczny
    or new.typdokvat is distinct from old.typdokvat
    or new.firma is distinct from old.firma
  ) then
  begin
    if(old.is_manual = 0) then
      exception universal 'Nie można edytować danych rozliczeniowych pobranych z witryny.';
    else if (old.is_manual = 2) then
      exception universal 'Nie można edytować danych rozliczeniowych. Dane są już używane';
  end
end^
SET TERM ; ^
