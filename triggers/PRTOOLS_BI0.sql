--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLS_BI0 FOR PRTOOLS                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.amount is null) then new.amount = 0;
  if (new.amountfree is null) then new.amountfree = 0;
  if (new.invno is null) then new.invno = '';
  if (new.changeamount is null) then new.changeamount = 0;
  if (new.price is null) then new.price = 0;
  if (new.miara is null or new.miara = '') then
  begin
    select first 1 miara
      from towary t
        join prtoolstypes p on (t.ktm = p.ktm)
      where p.symbol = new.tooltype
      into new.miara;
    if (new.miara is null) then new.miara = 'szt';
  end
  if (new.descript is null or new.descript = '') then new.descript = new.symbol;
end^
SET TERM ; ^
