--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIKSEGREGATORY_AD0 FOR DOKPLIKSEGREGATORY             
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable segregatory varchar(6000);
declare variable segregator varchar(255);
begin
  segregatory = ';';
  for select segregator
    from dokpliksegregatory
    where dokplik = old.dokplik
    into :segregator
  do begin
    segregatory = segregatory||segregator||';';
    if(coalesce(char_length(segregatory),0)>5000) then exception universal 'Przekroczono dozwoloną liczbe powiązań'; -- [DG] XXX ZG119346
  end
  update dokplik set segregatory = :segregatory where ref = old.dokplik;
  if(old.segdefault in (0,1)) then execute procedure DOKPLIKSEGREGATORY_REFRESH (old.dokplik);
end^
SET TERM ; ^
