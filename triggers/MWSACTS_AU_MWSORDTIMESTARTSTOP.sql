--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AU_MWSORDTIMESTARTSTOP FOR MWSACTS                        
  ACTIVE AFTER UPDATE POSITION 2 
AS
declare variable number integer;
declare variable timestart timestamp;
declare variable timestop timestamp;
begin
  if ((new.status = 6 and old.status <> 6)) then
  begin
    -- okreslenie deklarowanego czasu rozpoczecia zlecenia po zmianie statusu lub czasu rozpoczecia operacji
    if (not exists(select ref from mwsacts where mwsord = new.mwsord
        and number <= new.number and status <> 6)
    ) then
    begin
      select min(number)
        from mwsacts
        where mwsord = new.mwsord and number < new.number and status <> 6
        into number;
      select timestartdecl from mwsacts where mwsord = new.mwsord and number = :number and status <> 6
        into :timestart;
      update mwsords set timestartdecl = :timestart where ref = new.mwsord;
    end
  end
  else
  if ((new.status <> 6 and old.status = 6)
      or ((new.timestartdecl <> old.timestartdecl or (new.timestartdecl is not null and old.timestartdecl is null))
          and new.status <> 6))
  then
  begin
    if (not exists(select ref from mwsacts where mwsord = new.mwsord
        and number < new.number and status <> 6)
    ) then
      update mwsords set timestartdecl = new.timestartdecl where ref = new.mwsord;
  end
  if ((new.status = 6 and old.status <> 6)) then
  begin
    -- okreslenie deklarowanego czasu zakonczena zlecenia po zmianie statusu lub czasu zakonczenia operacji
    if (not exists(select ref from mwsacts where mwsord = new.mwsord
        and number >= new.number and status <> 6)
    ) then
    begin
      select max(number)
        from mwsacts
        where mwsord = new.mwsord and number >= new.number and status <> 6
        into number;
      select timestopdecl from mwsacts where mwsord = new.mwsord and number = :number and status <> 6
        into :timestop;
      update mwsords set timestopdecl = :timestop where ref = new.mwsord;
    end
  end
  else
  if ((new.status <> 6 and old.status = 6)
      or ((new.timestopdecl <> old.timestopdecl or (new.timestopdecl is not null and old.timestopdecl is null))
          and new.status <> 6))
  then
  begin
    if (not exists(select ref from mwsacts where mwsord = new.mwsord
        and number <= new.number and status <> 6)
    ) then
      update mwsords set timestopdecl = new.timestopdecl where ref = new.mwsord;
  end
  -- NIE LICZYMY RZECZYWISTEGO NA DELETE BO SIE NIE DA SKASOWAC OPERACJI POTWIERDZONEJ !!!!
end^
SET TERM ; ^
