--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MEDICALBOARDS_AI_USZCZ FOR MEDICALBOARDS                  
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable data timestamp;
declare variable datausz timestamp;
declare variable valusz numeric(14,2);
begin
  data = null;
  datausz = null;
  valusz = 0;

  select max(mb.meddate) from medicalboards mb where mb.ckontrakt = new.ckontrakt into :data;
       if (data is not null) then  begin
         select ms.meddate, ms.medvalue  from medicalboards ms where ms.meddate = :data and ms.ckontrakt = new.ckontrakt into :datausz, :valusz;
         update ckontrakty ck set ck.uszczerbek = :valusz, ck.datauszczerb = :datausz where ck.ref=new.ckontrakt;
       end
  end^
SET TERM ; ^
