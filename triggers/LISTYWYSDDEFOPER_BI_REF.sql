--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDDEFOPER_BI_REF FOR LISTYWYSDDEFOPER               
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('LISTYWYSDDEFOPER')
      returning_values new.REF;
end^
SET TERM ; ^
