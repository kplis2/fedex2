--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AI_HB FOR RKDOKNAG                       
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  -- naliczanie podsumowań elektronicznego wyciągu bankowego
  if (new.electronic = 1) then
    execute procedure RKRAP_OBL_WAR(new.raport);
  if (new.ticaccount is not null and new.ticaccount <> '' and new.electronic = 0) then
    execute procedure HB_UPDATE_STATEMENT_POSITION(new.ref);
end^
SET TERM ; ^
