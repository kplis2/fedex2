--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTRASTATH_BU0 FOR INTRASTATH                     
  ACTIVE BEFORE UPDATE POSITION 1 
AS
declare variable fromdate date;
declare variable todate date;
declare variable email varchar(255);
begin
  if(new.sumval is null) then new.sumval = 0.0;
  if(new.sumvalstat is null) then new.sumvalstat = 0.0;
  if(new.poscount is null) then new.poscount = 0;
  if(new.progstat is null) then new.progstat = 0;
  if(new.zrodlo is null) then new.zrodlo = 0;
  if(new.statusflag is null) then new.statusflag = 0;

  -- zmiana okresu
  if(new.period <> old.period) then begin
    if(coalesce(new.period, '') <> '') then begin
      select fromdate, todate
        from period2dates(new.period)
        into :fromdate, :todate;
    end
  end

  -- zmiana daty początkowej
  if ((new.fromdate <> old.fromdate) and (coalesce(new.fromdate,'') = ''))
    then new.fromdate = :fromdate;

  -- zmiana daty końcowej
  if ((new.todate <> old.todate) and (coalesce(new.todate,'') = ''))
    then new.todate = :todate;

  -- zmiana adresu email
  if((new.email <> old.email) and (coalesce(new.email,'') = '')) then begin
    select coalesce(konfig.wartosc,'')
      from konfig
      where konfig.akronim like 'INFOMAIL'
      into :email;
    if (:email <> '') then new.email = :email;
    else exception intrastat_email_firma;
  end
end^
SET TERM ; ^
