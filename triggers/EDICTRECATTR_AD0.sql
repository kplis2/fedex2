--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTRECATTR_AD0 FOR EDICTRECATTR                   
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable RecCand_ref int;
begin
  for
    select CANDIDATE from ecandidattr
      where ATTRIBUTE=old.ref
      into :RecCand_ref
  do
    execute procedure eRecCandCompRating(RecCand_ref);
end^
SET TERM ; ^
