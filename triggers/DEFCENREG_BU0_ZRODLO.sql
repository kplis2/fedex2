--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENREG_BU0_ZRODLO FOR DEFCENREG                      
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  if (new.zrodlo <> old.zrodlo) then begin
    if (old.zrodlo = 4) then new.cenniknad = null;
    if (old.zrodlo = 3) then new.magazyn = null;
    if (new.zrodlo = 3 and new.magazyn is null) then
      exception universal 'Magazyn nie może być pusty!';
    else if (new.zrodlo = 4 and new.cenniknad is null) then
      exception universal 'Cennik nie może być pusty!';
  end
end^
SET TERM ; ^
