--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMAT_BI_REF FOR PRSHMAT                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRSHMAT')
      returning_values new.REF;
end^
SET TERM ; ^
