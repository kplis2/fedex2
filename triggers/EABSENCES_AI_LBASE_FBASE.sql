--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AI_LBASE_FBASE FOR EABSENCES                      
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable period varchar(6);
declare variable sbase numeric(14,2);
declare variable takeit smallint;
declare variable vbase numeric(14,2);
declare variable workdays integer;
declare variable vworkeddays integer;
declare variable sworkeddays integer;
declare variable sbasegross numeric(14,2);
declare variable vworkedhours numeric(14,2);
begin
--Przypisanie nieobecnosci podstaw pracowniczych z zadanego zakresu fbase-lbase

  if (coalesce(new.baseabsence, 0) = new.ref and coalesce(new.mflag, 0) = 0) then
  begin
  --nie stwierdzono ciaglosci, wiec stawki zostana wyliczone z biezacych podstaw
    for
      select period, sbase, takeit, vbase, workdays, vworkeddays,
             sworkeddays, sbasegross, vworkedhours
        from eabsemplbases
        where employee = new.employee
          and new.fbase <= period
          and period <= new.lbase
        into :period, :sbase, :takeit, :vbase, :workdays, :vworkeddays,
             :sworkeddays, :sbasegross, :vworkedhours
    do begin
       if (not exists (select first 1 1 from eabsencebases
                          where eabsence = new.ref
                            and period = :period))
       then --sprawdzenie istnienie nie jest raczej konieczne (gdyz uzywamy
            --znaczika mflag), ale na pewno nie zaszkodzi
         insert into eabsencebases (eabsence, employee, period, sbase, takeit, vbase,
           workdays, vworkeddays, sworkeddays, sbasegross, vworkedhours)
         values (new.ref, new.employee, :period, :sbase, :takeit, :vbase,
          :workdays, :vworkeddays, :sworkeddays, :sbasegross, :vworkedhours);
    end
  end else
    update eabsences set mflag = null where ref = new.ref;

end^
SET TERM ; ^
