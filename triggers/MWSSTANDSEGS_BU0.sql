--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDSEGS_BU0 FOR MWSSTANDSEGS                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.mwsconstlocsq is null) then new.mwsconstlocsq = 0;
  if (new.w is null) then new.w = 0;
  if (new.l is null) then new.l = 0;
  if (new.whsecrow <> old.whsecrow) then
    select symbol from whsecrows where ref = new.whsecrow into new.whsecrowdict;
  if (new.whsec <> old.whsec) then
    select symbol from whsecs where ref = new.whsec into new.whsecdict;
end^
SET TERM ; ^
