--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_AI_COMPUTE_BKDOC FOR BKVATPOS                       
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  update bkdocs set
    sumnetv = sumnetv + new.netv,
    sumvatv = sumvatv + new.vatv,
    sumgrossv = sumgrossv + new.grossv,
    curnetval = curnetval + new.currnetv,
    sumnet = sumnet + new.net,
    sumvate = sumvate + new.vate
  where ref = new.bkdoc;
end^
SET TERM ; ^
