--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MULTIMEDIA_BI_REF FOR MULTIMEDIA                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('MULTIMEDIA')
      returning_values new.REF;
end^
SET TERM ; ^
