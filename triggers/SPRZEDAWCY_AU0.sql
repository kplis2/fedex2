--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDAWCY_AU0 FOR SPRZEDAWCY                     
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slodef integer;
declare variable cnt integer;
begin
 if(new.crm = 1 and old.crm = 0) then begin
   select ref from SLODEF where TYP = 'SPRZEDAWCY' into :slodef;
   if(:slodef is null) then exception SLODEF_BEZSPRZEDAWCY;
   select count(*) from CPODMIOTY where SLODEF = :slodef AND SLOPOZ = new.ref into :cnt;
   if(:cnt > 0) then
     update CPODMIOTY set SKROT = substring(new.skrot from 1 for 40), NAZWA = new.nazwa, IMIE = new.imie, NAZWISKO = new.NAZWISKO, FIRMA = new.firma, --XXX ZG133796 MKD
                          TELEFON = new.telefon, EMAIL = new.email,
                           nip = new.nip,
                           ULICA = new.adres, MIASTO = new.miasto, KODP = new.kodpoczt, POCZTA = new.poczta, KONTOFK = new.kontofk
        where SLODEF = :slodef AND SLOPOZ = new.ref ;
   else
     insert into CPODMIOTY(AKTYWNY, SKROT, NAZWA, TELEFON, EMAIL, NAZWISKO, IMIE, FIRMA,
           NIP, SLODEF, SLOPOZ, ULICA, MIASTO, KODP, POCZTA, KONTOFK, company)
      values(1, substring(new.skrot from 1 for 40), new.nazwa, new.telefon, new.email, new.imie, new.nazwisko, new.firma,  --XXX ZG133796 MKD
               new.nip, :slodef, new.ref, new.adres, new.miasto, new.kodpoczt, new.poczta, new.kontofk, new.company);

 end else if(new.crm = 0 and old.crm = 1) then begin
   select ref from SLODEF where TYP = 'SPRZEDAWCY' into :slodef;
   if(:slodef is null) then exception SLODEF_BEZSPRZEDAWCY;
   delete from CPODMIOTY where SLODEF = :slodef AND SLOPOZ = old.ref;
 end
 if(new.crm = 1 and old.crm = 1 and
     (new.skrot <> old.skrot or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.adres <> old.adres or (new.adres is not null and old.adres is null) or (new.adres is null and old.adres is not null) or
      new.nip <> old.nip or (new.nip is not null and old.nip is null) or (new.nip is null and old.nip is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.kodpoczt  <> old.kodpoczt or (new.kodpoczt is not null and old.kodpoczt is null) or (new.kodpoczt is null and old.kodpoczt is not null) or
      new.poczta <> old.poczta or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.firma <> old.firma or (new.firma is not null and old.firma is null) or (new.firma is null and old.firma is not null) or
      new.imie <> old.imie or (new.imie is not null and old.imie is null) or (new.imie is null and old.imie is not null) or
      new.nazwisko <> old.nazwisko or (new.nazwisko is not null and old.nazwisko is null) or (new.nazwisko is null and old.nazwisko is not null) or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null)
     )
   ) then begin
        select ref from SLODEF where TYP = 'SPRZEDAWCY' into :slodef;
        if(:slodef is null) then exception SLODEF_BEZsprzedawcy;
        update CPODMIOTY  set SKROT  = substring(new.skrot from 1 for 40), NAZWA = new.nazwa, imie = new.imie, NAZWISKO = new.nazwisko, firma = new.firma,--XXX ZG133796 MKD
                      ULICA = new.adres , NIP = new.nip, MIASTO = new.miasto,
                      KODP = new.kodpoczt, POCZTA = new.poczta,
                      TELEFON = new.telefon, EMAIL = new.email, KONTOFK = new.kontofk
        where SLODEF = :slodef AND SLOPOZ = new.ref ;

  end
end^
SET TERM ; ^
