--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIKSEGREGATORY_AIU0 FOR DOKPLIKSEGREGATORY             
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
declare variable segregatory varchar(6000);
declare variable segregator varchar(255);
begin
  segregatory = ';';
  for select segregator
    from dokpliksegregatory
    where dokplik = new.dokplik
    into :segregator
  do begin
    segregatory = segregatory||segregator||';';
    if(coalesce(char_length(segregatory),0)>5000) then exception universal 'Przekroczono dozwoloną liczbe powiązań';
  end
  update dokplik set segregatory = :segregatory where ref = new.dokplik;
  if(new.segdefault = 1) then update dokpliksegregatory s set s.segdefault = 0 where s.dokplik = new.dokplik and s.segregator <> new.segregator;
  if(new.segdefault in (0,1)) then execute procedure DOKPLIKSEGREGATORY_REFRESH (new.dokplik);
end^
SET TERM ; ^
