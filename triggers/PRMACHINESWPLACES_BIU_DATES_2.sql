--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMACHINESWPLACES_BIU_DATES_2 FOR PRMACHINESWPLACES              
  INACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable i int;
declare variable fromdate timestamp;
begin
  -- MS: ten trigger byl nieaktywny na release, dla pewnosci komentuje jego tresc!!!
  -- UWAGA!: triger jest kopia trigera 'prmachineswplaces_biu_dates_2'
  --         poza tym ze zmieniono warunek PRMACHINE=new.PRMACHINE
  --         na PRWORKPLACE=new.PRWORKPLACE
  --         oraz komunikaty bledow
/*  if (old.fromdate<>new.FROMDATE or old.TODATE<>new.TODATE or old.PRMACHINE<>new.PRMACHINE or old.PRWORKPLACE<>new.PRWORKPLACE
      or old.fromdate is null ) then Begin
    if (new.TODATE<new.FROMDATE) then exception PRMACHINESWPLACES_EXPT 'Data DO musi być większa niż Data OD.';
    select count(FROMDATE)
      from PRMACHINESWPLACES
      where PRWORKPLACE=new.PRWORKPLACE AND FROMDATE<>new.FROMDATE AND
        new.FROMDATE>=FROMDATE AND new.FROMDATE<=TODATE
      into :i;
    -- okresy rozpoczyna sie wewnatrz innego
    if (i>0) then exception PRMACHINESWPLACES_EXPT 'W tym czasie maszyna będzie wykorzystywana na innym stanowisku.';
    -- teraz sprawdzam date do
    if (new.TODATE is null) then
    begin
      --jesli istnieje okres póżniejszy, to ten zostanie zamknity automatycznie
      for
        select FROMDATE
          from PRMACHINESWPLACES
          where PRWORKPLACE=new.PRWORKPLACE and new.FROMDATE<FROMDATE
          into :fromdate
      do begin
        if (new.TODATE is null OR new.TODATE>=fromdate) then
          new.TODATE = fromdate - 1;
      end
    end
    if (new.TODATE is not null) then
    begin
      -- trzeba sprawdzic date "DO", czy nie jest w srodku innego okresu
      select count(FROMDATE)
        from PRMACHINESWPLACES
        where PRWORKPLACE=new.PRWORKPLACE AND FROMDATE<>new.FROMDATE AND
              new.TODATE>=FROMDATE AND new.TODATE<=TODATE
        into :i;
      -- okres kończy sie wewnatrz innego
      if (i>0) then exception PRMACHINESWPLACES_EXPT 'W tym czasie maszyna będzie wykorzystywana na innym stanowisku.';
      -- jeszcze sprawdzam czy okres nie zawiera innego
      select count(FROMDATE)
        from PRMACHINESWPLACES
        where PRWORKPLACE=new.PRWORKPLACE AND FROMDATE<>new.FROMDATE AND
          new.FROMDATE<=FROMDATE AND new.TODATE>=TODATE
        into :i;
      -- okres zawiera inny okres
      if (i>0) then exception PRMACHINESWPLACES_EXPT 'W tym czasie maszyna będzie wykorzystywana na innym stanowisku.';
    end
    -- zamykam poprzedni okres, jeli byl otwarty
    update PRMACHINESWPLACES
      set TODATE = new.FROMDATE - 1
      where PRWORKPLACE=new.PRWORKPLACE AND FROMDATE<>new.FROMDATE AND
        todate is null and fromdate<new.fromdate;
  end*/
end^
SET TERM ; ^
