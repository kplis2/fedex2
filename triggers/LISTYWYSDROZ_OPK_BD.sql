--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_OPK_BD FOR LISTYWYSDROZ_OPK               
  ACTIVE BEFORE DELETE POSITION 9 
as
begin
  if (exists(select first 1 1 from listywysdroz where listywysd = old.listwysd and opk = old.ref)) then
    exception universal 'Nie można usuwać pełnego opakowania';
end^
SET TERM ; ^
