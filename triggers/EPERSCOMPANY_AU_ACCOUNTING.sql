--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSCOMPANY_AU_ACCOUNTING FOR EPERSCOMPANY                   
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.symbol <> old.symbol) then
    execute procedure person_set_accounting(new.person, new.company, new.symbol);
end^
SET TERM ; ^
