--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_BU_DATY FOR RKRAPKAS                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable lokres varchar(6);
  declare variable skbo  decimal(14,2);
  declare variable skbow  decimal(14,2);
  declare variable smkw  decimal(14,2);
  declare variable smkww  decimal(14,2);
  declare variable refrk integer;
  declare variable numerrk integer;
  declare variable datark timestamp;
  declare variable refprev integer;
  declare variable rokod integer;
  declare variable mscod integer;
  declare variable rokdo integer;
  declare variable mscdo integer;
  declare variable typstan char(1);
  declare variable dzienny integer;
  declare variable kilkadzien integer;
  declare variable otwkilka integer;
  declare variable cnt integer;
begin
  if (new.status is null) then new.status = 0;
  /*okres numerowania*/
  select DZIENNY, KILKADZIENNIE, OTWKILKA
    from RKSTNKAS where KOD = new.stanowisko
    into :dzienny,:kilkadzien, :otwkilka;
/*  if(:typstan is null) then typstan = 'R';*/
  if ((new.dataod <> old.dataod) or (new.datado <> old.datado)) then
  begin

    /* sprawdzenie, czy jaki raport nie nachodzi od dolu */
    cnt = 0;
    if (kilkadzien = 1) then
    begin
      if (exists( select ref
                    from RKRAPKAS
                    where DATADO > new.DATAOD
                      and DATADO < new.datado
                      and stanowisko = new.stanowisko
                      and ref <> new.ref
                      and ref <> old.ref)) then cnt = 1;
    end
    else
    begin
      if (exists( select ref
                    from RKRAPKAS
                    where DATADO >= new.DATAOD
                      and DATADO <= new.datado
                      and stanowisko = new.stanowisko
                      and ref <> new.ref)) then cnt = 1;
    end
    if (cnt > 0) then exception RKRAPKAS_DATAZDOLU;

    /* sprawdzenie, czy jaki raport nie nachodzi od gory */
    cnt = 0;
    if (kilkadzien = 1) then
    begin
      if (exists( select ref
                    from RKRAPKAS
                    where DATAOD > new.DATAOD
                      and DATAOD < new.datado
                      and stanowisko = new.stanowisko
                      and ref <> new.ref
                      and ref <> old.ref)) then cnt = 1;
    end
    else
    begin
      if (exists( select ref
                    from RKRAPKAS
                    where DATAOD >= new.DATAOD
                      and DATAOD <= new.datado
                      and stanowisko = new.stanowisko
                      and ref <> new.ref
                      and ref <> old.ref)) then cnt = 1;
    end
    if (cnt > 0) then exception RKRAPKAS_DATAZGORY;

    /* sprawdzenie, czy inny raport nie polyka danego */
    if (exists( select ref
                 from RKRAPKAS
                 where DATAOD <= new.dataod
                  and DATADO >= new.datado
                  and (:kilkadzien = 0 or new.dataod <> new.datado
                  or  ( new.dataod <> DATAOD and new.datado <> DATADO))
                  and stanowisko = new.stanowisko
                  and ref <> new.ref)) then exception RKRAPKAS_DATAW;

    /* sprawdzenie, czy nowe daty tego samego miesiaca  */
    rokod = extract(year from new.dataod);
    mscod = extract(month from new.dataod);
    rokdo = extract(year from new.datado);
    mscdo = extract(month from new.datado);
    if ((rokod <> rokdo) or (mscod <> mscdo)) then
      exception RKRAPKAS_DATYSPOZAMSC;

    /* sprawdzenie, czy nie nastapila zmiana mieisaca dat */
    rokdo = extract(year from old.dataod);
    mscdo = extract(month from old.dataod);
    if ((rokod <> rokdo) or (mscod <> mscdo)) then
      exception RKRAPKAS_DATYZMMSC;

    if (new.okres <> lokres  or new.dataod is null
       or new.datado is null or new.dataod > new.datado) then
      exception RK_NIEPRAWIDLOWE_DATY;
  end

  if (new.dataod > old.dataod) then
  begin
    if (exists( select ref
                from RKDOKNAG
                  where RAPORT = new.ref
                    and DATA < new.dataod)) then
      exception RKRAPKAS_DATAODDOKNAG;
  end

  if (new.datado < old.datado) then
  begin
    if (exists( select ref
                from RKDOKNAG
                  where RAPORT = new.ref
                  and DATA > new.datado)) then
      exception RKRAPKAS_DATADODOKNAG;
  end
  /*sprawdzenie, czy istnieją raporty z mniejszym numerem poxniejsze */
/* tu by bąd, na przelomie roku, a zwlaszcza przy numeracji niesiecznej
  cnt =  null;
  select count(*) from RKRAPKAS where STANOWISKO = new.stanowisko and NUMER < new.numer and DATAOD > new.dataod
    into :cnt;
  if(:cnt > 0) then exception RKRAPKAS_DATARAPTOSMALL;
*/

end^
SET TERM ; ^
