--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AI_CALC_POZZAM FOR MWSACTS                        
  ACTIVE AFTER INSERT POSITION 10 
AS
declare variable quantitychange smallint;
declare variable quantitycchange smallint;
begin
  if (new.quantity > 0) then
    quantitychange = 1;
  else
    quantitychange = 0;
  if (new.quantityc > 0) then
    quantitycchange = 1;
  else
    quantitycchange = 0;
  -- obliczenie ilosci na pozcycji dokumentu magazynowego
  if (new.quantity > 0 and new.docposid > 0 and new.doctype = 'Z') then
    execute procedure POZZAM_OBL_FROM_MWSACTS(new.doctype, new.docid,new.docposid,new.stocktaking,new.recdoc,quantitychange,quantitycchange);
end^
SET TERM ; ^
