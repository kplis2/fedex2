--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_AU_PRZELICZ FOR PMPOSITIONS                    
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin

  if (new.calcval<>old.calcval or new.budval<>old.budval or new.budvalr<>old.budvalr
      or new.usedval<>old.usedval or new.erealval<>old.erealval or new.positiontype<>old.positiontype) then
    execute procedure pmelements_calc(new.pmelement);

end^
SET TERM ; ^
