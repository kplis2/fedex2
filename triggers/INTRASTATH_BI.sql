--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTRASTATH_BI FOR INTRASTATH                     
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable fromdate date;
declare variable todate date;
declare variable email varchar(255);
begin
  if(new.sumval is null) then new.sumval = 0.0;
  if(new.sumvalstat is null) then new.sumvalstat = 0.0;
  if(new.poscount is null) then new.poscount = 0;
  if(new.progstat is null) then new.progstat = 0;
  if(new.zrodlo is null) then new.zrodlo = 0;
  if(new.statusflag is null) then new.statusflag = 0;

  if((coalesce(new.period, '') <> '') and
    ((coalesce(new.fromdate,'') = '') or (coalesce(new.todate,'') = ''))) then begin
    select fromdate, todate
      from period2dates(new.period)
      into :fromdate, :todate;
    if (coalesce(new.fromdate,'') = '') then new.fromdate = :fromdate;
    if (coalesce(new.todate,'') = '') then new.todate = :todate;
  end

  -- sprawdzenie czy nie powielamy deklaracji
  execute procedure intrastath_is_exist (new.period, new.fromdate, new.todate,
    new.intrtype, new.intrversion, new.decltype);

  if(coalesce(new.email,'') = '') then begin
    select coalesce(konfig.wartosc,'')
      from konfig
      where konfig.akronim like 'INFOMAIL'
      into :email;
    if (:email <> '') then new.email = :email;
    else exception intrastat_email_firma;
  end
end^
SET TERM ; ^
