--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_AU_EPERCHANGES FOR PERSONS                        
  ACTIVE AFTER UPDATE POSITION 2 
AS
begin
  if (coalesce(trim(old.fname),'') <> coalesce(trim(new.fname),'')
    or coalesce(trim(old.sname),'') <> coalesce(trim(new.sname),'')
    or coalesce(trim(old.evidenceno),'') <> coalesce(trim(new.evidenceno),'')
    or coalesce(trim(old.passportno),'') <> coalesce(trim(new.passportno),'')
    or coalesce(trim(old.pesel),'') <> coalesce(trim(new.pesel),'')
    or coalesce(trim(old.nip),'') <> coalesce(trim(new.nip),'')
  ) then
    insert into eperchanges (person, data, fname, sname, evidenceno, passportno, pesel, nip, birthdate)
      values (new.ref, current_timestamp(0), old.fname, old.sname, old.evidenceno, old.passportno, old.pesel, old.nip, old.birthdate);
end^
SET TERM ; ^
