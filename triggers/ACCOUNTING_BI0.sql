--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCOUNTING_BI0 FOR ACCOUNTING                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  select ref, bktype
    from bkaccounts where symbol = substring(new.account from 1 for 3) and yearid = new.yearid
      and company = new.company
    into new.bkaccount, new.bktype;

  execute procedure ACCOUNTING_GET_DESCRIPT(new.account, new.yearid, new.company)
    returning_values new.descript, new.fulldescript;
end^
SET TERM ; ^
