--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_BU0 FOR VALDOCS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable company integer;
begin
    if ((new.doctype <> old.doctype or new.docdate <> old.docdate or new.operdate <> old.operdate
       or new.operiod <> old.operiod or new.liquidation <> old.liquidation or new.tvalue <> old.tvalue
       or new.tvallim <> old.tvallim or new.tallowence <> old.tallowence or new.tredemption <> old.tredemption
       or new.fvalue <> old.fvalue or new.fredemption <> old.fredemption
       or new.amperiod <> old.amperiod or new.corperiod <> old.corperiod)
       and exists (select amortization.ref from amortization where amortization.fxdasset = old.fxdasset
       and amortization.amperiod >= old.operiod)) then
        exception DOKWAR_SRODTRW_ZMIANA;

  if (old.docdate <> new.docdate) then
  begin
    select company
      from fxdassets f
      where f.ref = new.fxdasset
      into :company;

    select a.ref
      from amperiods a
      where a.ammonth = extract(month from new.docdate)
        and a.amyear = extract(year from new.docdate)
        and company = :company
      into new.amperiod;
  end
end^
SET TERM ; ^
