--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AI_REZ FOR POZFAK                         
  ACTIVE AFTER INSERT POSITION 1 
as
declare variable ack smallint;
declare variable blokada smallint;
declare variable nieobrot smallint;
declare variable local smallint;
declare variable skad smallint;
begin
  select NAGFAK.AKCEPTACJA, TYPFAK.blokada , NAGFAK.NIEOBROT, nagfak.skad
  from NAGFAK
  left join TYPFAK on (NAGFAK.typ = TYPFAK.SYMBOL)
  where NAGFAK.REF=new.dokument into :ack, :blokada, :nieobrot, :skad;
  execute procedure CHECK_LOCAL('POZFAK',new.ref) returning_values local;
  if(:ack <> 1 and :blokada = 1 and local = 1 and nieobrot <> 1 and :skad <> 2 and :skad <> 1) then
    execute procedure REZ_SET_KTM_FAK(new.ref,2);
end^
SET TERM ; ^
