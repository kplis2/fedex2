--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SYS_SCHEDULE_BI_BLANK FOR SYS_SCHEDULE                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ACTIVEE is null) then
    new.ACTIVEE = 0;
  new.COMPILED = 0;
  if (new.SOURCE_CODE is null or new.SOURCE_CODE = '') then
    new.SOURCE_CODE = 'public string Execute()
{
  return ";
}';
  if (new.SOURCE_CODE_ERROR_HANDLING is null or new.SOURCE_CODE_ERROR_HANDLING = '') then
    new.SOURCE_CODE_ERROR_HANDLING = 'public void HandleError(Exception e)
{
  return;
}';
end^
SET TERM ; ^
