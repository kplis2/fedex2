--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_ATRYBUTY_BU FOR X_ATRYBUTY                     
  ACTIVE BEFORE UPDATE POSITION 10 
AS
begin
  if ((old.otable is distinct from new.otable or old.oref is distinct from new.oref)
      and (coalesce(trim(new.otable),'') = '' or coalesce(new.oref,0) = 0)
     ) then
    exception universal 'Podaj OTABLE i OREF.';

  if (old.cecha is distinct from new.cecha) then
    begin
      if (coalesce(trim(new.cecha),'') = '') then
        exception universal 'Podaj ceche.';
      if (not exists (select first 1 1 from defcechy where symbol = new.cecha)) then --teoretycznie nie musi byc tego exceptiona, bo to tabela x-owa
        exception universal 'Cechy '||new.cecha||' nie ma w definicjach.';
    end

  if (old.cecha is distinct from new.cecha
      or old.wartosc is distinct from new.wartosc
      or old.i_wartosc is distinct from new.i_wartosc
      or old.d_wartosc is distinct from new.d_wartosc
      or old.n_wartosc is distinct from new.n_wartosc
      ) then
    begin
      new.changedate = current_timestamp(0);
      execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoper;
    end 

  if (new.otable = 'WERSJE' and old.oref is distinct from new.oref) then
    select ktm from wersje where ref = new.oref into new.ktm;

end^
SET TERM ; ^
