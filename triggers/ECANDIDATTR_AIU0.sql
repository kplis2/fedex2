--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECANDIDATTR_AIU0 FOR ECANDIDATTR                    
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
declare variable RecCand_ref int;
begin
  -- przeliczam oceny !
  for
    select REF
      from ereccands where CANDIDATE=new.CANDIDATE
      into :RecCand_ref
  do
    execute procedure eRecCandCompRating(RecCand_ref);
end^
SET TERM ; ^
