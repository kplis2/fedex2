--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_AD_CALCWHAREAS FOR WHSECROWS                      
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable whsecrowsref integer;
begin
  for
    select ref from whsecrows where whsecrows.whsec = old.whsec
      into :whsecrowsref
  do begin
    execute procedure MWS_ROWDIST_CALCULATE(whsecrowsref);
  end
end^
SET TERM ; ^
