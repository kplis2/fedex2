--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_BU_REPLICAT FOR KONTAKTY                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
   or(new.ckontrakt <> old.ckontrakt ) or (new.ckontrakt is null and old.ckontrakt is not null) or (new.ckontrakt is not null and old.ckontrakt is null)
   or(new.cpodmiot <> old.cpodmiot ) or (new.cpodmiot is null and old.cpodmiot is not null) or (new.cpodmiot is not null and old.cpodmiot is null)
   or(new.cpodmiot2 <> old.cpodmiot2 ) or (new.cpodmiot2 is null and old.cpodmiot2 is not null) or (new.cpodmiot2 is not null and old.cpodmiot2 is null)
   or(new.cpodmtelefon <> old.cpodmtelefon ) or (new.cpodmtelefon is null and old.cpodmtelefon is not null) or (new.cpodmtelefon is not null and old.cpodmtelefon is null)
   or(new.emailwiad <> old.emailwiad ) or (new.emailwiad is null and old.emailwiad is not null) or (new.emailwiad is not null and old.emailwiad is null)
   or(new.inout <> old.inout ) or (new.inout is null and old.inout is not null) or (new.inout is not null and old.inout is null)
   or(new.operator <> old.operator ) or (new.operator is null and old.operator is not null) or (new.operator is not null and old.operator is null)
   or(new.opis <> old.opis ) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
   or(new.osoba <> old.osoba ) or (new.osoba is null and old.osoba is not null) or (new.osoba is not null and old.osoba is null)
   or(new.pkosoba <> old.pkosoba ) or (new.pkosoba is null and old.pkosoba is not null) or (new.pkosoba is not null and old.pkosoba is null)
   or(new.pkosobytelefon <> old.pkosobytelefon ) or (new.pkosobytelefon is null and old.pkosobytelefon is not null) or (new.pkosobytelefon is not null and old.pkosobytelefon is null)
   or(new.plik <> old.plik ) or (new.plik is null and old.plik is not null) or (new.plik is not null and old.plik is null)
   or(new.rodzaj <> old.rodzaj ) or (new.rodzaj is null and old.rodzaj is not null) or (new.rodzaj is not null and old.rodzaj is null)
   or(new.zadanie <> old.zadanie ) or (new.zadanie is null and old.zadanie is not null) or (new.zadanie is not null and old.zadanie is null)
   or(new.cakcjanag <> old.cakcjanag) or (new.cakcjanag is null and old.cakcjanag is not null) or (new.cakcjanag is not null and old.cakcjanag is null)
   or(new.data <> old.data) or (new.data is null and old.data is not null) or (new.data is not null and old.data is null)
   or(new.czas <> old.czas) or (new.czas is null and old.czas is not null) or (new.czas is not null and old.czas is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('KONTAKTY',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
