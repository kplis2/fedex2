--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_AU_EMPLOYMENT FOR ECONTRANNEXES                  
  ACTIVE AFTER UPDATE POSITION 1 
as
begin
--MWr(PR24342) Przeniesienie zmian na przebieg zatrudnienia po aktualizacji aneksu/oddelegowania.

  if (exists(select first 1 1 from emplcontracts --PR60202
               where ref = new.emplcontract and empltype = 1)
  ) then begin
    if (new.atype <> old.atype or (new.atype is null and old.atype is not null) or (new.atype is not null and old.atype is null) or
        new.fromdate <> old.fromdate or (new.fromdate is null and old.fromdate is not null) or (new.fromdate is not null and old.fromdate is null) or
        new.todate <> old.todate or (new.todate is null and old.todate is not null) or (new.todate is not null and old.todate is null)
    ) then begin
    --update zlozony (z wykorzystaniem triggerow delete/insert; pewna zmiana REFa na EMPLOYMENT)
      update econtrannexes set mflag = 1 where ref = old.ref;
      execute procedure update_employment_bd_annexe(old.emplcontract, old.fromdate, old.todate, old.atype, old.ref);
      delete from econtrannexes where ref = old.ref;
  
      insert into econtrannexes (ref, employee, fromdate, emplcontract, workpost,
         workdim, dimnum, dimden, branch, department, salary, paymenttype,
         caddsalary, funcsalary, dictdef, dictpos, atype, bksymbol, local,
         todate, sgroup, proportional, ratio, workcat, emplgroup, worksystem,
         epayrule, regtimestamp, regoperator                                                    --PR61955
      ) values (new.ref, new.employee, new.fromdate, new.emplcontract, new.workpost,
         new.workdim, new.dimnum, new.dimden, new.branch, new.department, new.salary, new.paymenttype,
         new.caddsalary, new.funcsalary, new.dictdef, new.dictpos, new.atype, new.bksymbol, new.local,
         new.todate, new.sgroup, new.proportional, new.ratio, new.workcat, new.emplgroup, new.worksystem,
         new.epayrule, new.regtimestamp, new.regoperator
      );
    end else
    if (new.branch <> old.branch or (new.branch is null and old.branch is not null) or (new.branch is not null and old.branch is null) or
        new.department <> old.department or (new.department is null and old.department is not null) or (new.department is not null and old.department is null) or
        new.workpost <> old.workpost or (new.workpost is null and old.workpost is not null) or (new.workpost is not null and old.workpost is null) or
        new.workdim <> old.workdim or (new.workdim is null and old.workdim is not null) or (new.workdim is not null and old.workdim is null) or
        new.salary <> old.salary or (new.salary is null and old.salary is not null) or (new.salary is not null and old.salary is null) or
        new.caddsalary <> old.caddsalary or (new.caddsalary is null and old.caddsalary is not null) or (new.caddsalary is not null and old.caddsalary is null) or
        new.funcsalary <> old.funcsalary or (new.funcsalary is null and old.funcsalary is not null) or (new.funcsalary is not null and old.funcsalary is null) or
        new.paymenttype <> old.paymenttype or (new.paymenttype is null and old.paymenttype is not null) or (new.paymenttype is not null and old.paymenttype is null) or
        new.dictpos <> old.dictpos or (new.dictpos is null and old.dictpos is not null) or (new.dictpos is not null and old.dictpos is null) or
        new.bksymbol <> old.bksymbol or (new.bksymbol is null and old.bksymbol is not null) or (new.bksymbol is not null and old.bksymbol is null) or
        new.local <> old.local or (new.local is null and old.local is not null) or (new.local is not null and old.local is null) or
        new.proportional <> old.proportional or (new.proportional is null and old.proportional is not null) or (new.proportional is not null and old.proportional is null) or
        new.emplgroup <> old.emplgroup or (new.emplgroup is null and old.emplgroup is not null) or (new.emplgroup is not null and old.emplgroup is null) or
        new.worksystem <> old.worksystem or (new.worksystem is null and old.worksystem is not null) or (new.worksystem is not null and old.worksystem is null) or
        new.workcat <> old.workcat or (new.workcat is null and old.workcat is not null) or (new.workcat is not null and old.workcat is null) or
        new.epayrule is distinct from old.epayrule
    ) then begin
    --update prosty (zachowanie REFa na EMPLOYMENT dla powiazanej pozycji)
      execute procedure employment_rechange_annexerefs (new.fromdate, new.emplcontract, old.ref, new.ref);
    end
  end
end^
SET TERM ; ^
