--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHY_AD_REPLICAT FOR DEFCECHY                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('DEFCECHY', old.symbol, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
