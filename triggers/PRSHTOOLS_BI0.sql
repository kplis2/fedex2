--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHTOOLS_BI0 FOR PRSHTOOLS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  new.ehourfactor = replace(new.ehourfactor,',','.');
  new.esheetrate = replace(new.esheetrate,',','.');
  new.econdition = replace(new.econdition,',','.');
  select prtools.tooltype from prtools where symbol = new.symbol into new.tooltype;
end^
SET TERM ; ^
