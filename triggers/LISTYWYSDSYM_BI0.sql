--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDSYM_BI0 FOR LISTYWYSDSYM                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if((new.ref is null) or (new.ref = 0)) then
    execute procedure GEN_REF('LISTYWYSDSYM') returning_values new.ref;
  new.numer = 0;
  select count(*) from LISTYWYSDSYM where LISTYWYSDDOK=new.listywysddok into new.numer;
  if(new.numer is null) then new.numer = 0;
  new.numer = new.numer + 1;
  if(new.symbol is null) then new.symbol = '';
end^
SET TERM ; ^
