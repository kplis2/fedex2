--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_AU_DATAPLAT FOR ROZRACH                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.odecree is not null
      and (new.dataplat <> old.dataplat
        or (new.dataplat is null and old.dataplat is not null)
        or (new.dataplat is not null and old.dataplat is null))
  ) then
    update decrees set payday = new.dataplat where ref = new.odecree and payday <> new.dataplat;
end^
SET TERM ; ^
