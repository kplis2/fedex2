--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_AU0 FOR WHSECROWS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable coordxb numeric(14,2);
declare variable coordxe numeric(14,2);
declare variable whsecrowref integer;
declare variable w numeric(14,2);
declare variable number integer;
begin
  if (new.number <> old.number or new.coordxb <> old.coordxb or new.coordxe <> old.coordxe) then
  begin
    for
      select ref, w, number from whsecrows where whsec = new.whsec
        into whsecrowref, w, number
    do begin
      execute procedure mws_whsecrowsdist_calculate(new.whsec,w,number)
        returning_values (:coordxb, :coordxe);
      update whsecrows set coordxb = :coordxb, coordxe = :coordxe
        where ref = :whsecrowref;
    end
  end
  if (new.symbol <> old.symbol) then
  begin
    update mwsstands set whsecrowdict = new.symbol where whsecrow = new.ref;
    update mwsstandsegs set whsecrowdict = new.symbol where whsecrow = new.ref;
    update mwsstandlevels set whsecrowdict = new.symbol where whsecrow = new.ref;
    update whareas set whsecrowdict = new.symbol where whsecrow = new.ref;
    update mwsconstlocs set whsecrowdict = new.symbol where whsecrow = new.ref;
  end
  if (new.w <> old.w) then
    update whareas set w = new.w where whsecrow = new.ref;
end^
SET TERM ; ^
