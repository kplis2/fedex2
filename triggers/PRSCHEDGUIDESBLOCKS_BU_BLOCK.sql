--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESBLOCKS_BU_BLOCK FOR PRSCHEDGUIDESBLOCKS            
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.blocked is null) then new.blocked = 0;
  update prschedguides pg set pg.blocked = new.blocked where pg.ref = new.guide;
end^
SET TERM ; ^
