--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLARATIONS_BU_REQUIRED FOR EDECLARATIONS                  
  ACTIVE BEFORE UPDATE POSITION 2 
as
  declare variable msg varchar(1024);
begin
  if (new.state > 0 and old.state = 0) then
  begin
    execute procedure edecl_check_required_fields(new.ref)
      returning_values msg;

    if (msg <> '') then
      exception universal msg;
  end
end^
SET TERM ; ^
