--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AU_CALC_POZZAM FOR MWSACTS                        
  ACTIVE AFTER UPDATE POSITION 10 
AS
declare variable quantitychange smallint;
declare variable quantitycchange smallint;
begin
  if (new.quantity <> old.quantity) then
    quantitychange = 1;
  else
    quantitychange = 0;
  if (new.quantityc <> old.quantityc) then
    quantitycchange = 1;
  else
    quantitycchange = 0;
  if (new.mwspallocl is not null and old.mwspallocl is null
      or new.mwspallocl is null and old.mwspallocl is not null) then
  begin
   quantitychange = 1;
   quantitycchange = 1;
  end
  -- obliczenie ilosci na pozycji dokumentu zrodlowego
  if ((new.quantity <> old.quantity or new.quantityc <> old.quantityc or new.status <> old.status)
       and new.docposid > 0 and new.doctype = 'Z'
  ) then
    execute procedure POZZAM_OBL_FROM_MWSACTS(new.doctype, new.docid,new.docposid,new.stocktaking,new.recdoc,quantitychange,quantitycchange);
end^
SET TERM ; ^
