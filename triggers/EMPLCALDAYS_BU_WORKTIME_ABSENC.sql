--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_BU_WORKTIME_ABSENC FOR EMPLCALDAYS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  --DS: gdy na wskutek zmiany kalendarza zmieni sie liczba godzin pracy
  --a jest juz w danym dniu zarejestrowana nieobecnosc
  --oznacz nieobecnosc (na podstawie pola pojawi sie komunikat ze sie dane nie pokrywaja)
  if (new.ecalendar <> old.ecalendar and new.worktime <> old.worktime and new.absence is not null) then
  begin
    new.absencediff = 1;
  end
end^
SET TERM ; ^
