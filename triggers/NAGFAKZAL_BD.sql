--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKZAL_BD FOR NAGFAKZAL                      
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable akcept integer;
begin
  select akceptacja from nagfak where ref=old.faktura into :akcept;
  if(akcept = 1) then
    exception NAGFAKZAL_AKCEPTACJA;
end^
SET TERM ; ^
