--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_AU_WPK FOR ACCSTRUCTURE                   
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable is_pattern smallint_id;
declare variable slave_ref accstructure_id;
begin
  -- Nalezy sprawdzic, czy nie zupdatowalismy wzorca - jezeli tak
  -- to synchronizujemy wszystkie podległe krotki
  if (coalesce(new.pattern_ref,0)=0) then
  begin
    --Nie mamy pattern_ref wiec szansa ze jestesmy wzorcem, zbadajmy zakład
    select c.pattern
      from companies c join bkaccounts b on ( b.company = c.ref )
      where b.ref = new.bkaccount
      into :is_pattern;
    if (coalesce(is_pattern,0)=1) then
    begin
      --jednak jestesmy wzorcem, wiec wynniśmy zsynchronizowac podlegle krotki
      for
        select a.ref
          from accstructure a
          where a.pattern_ref = new.ref
          into :slave_ref
      do begin
        --synchronizacja krotki z wzorcem, ustawiam internal = 1 , żeby nie uruchamiać mechanizmów synchronizacji wtórnej
        execute procedure synchronize_accstrucutre(new.ref,:slave_ref,1);
      end
    end 
  end
end^
SET TERM ; ^
