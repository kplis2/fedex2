--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOSTSPOS_AI0 FOR ECOSTSPOS                      
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
  declare variable percents integer;
begin
  select sum(p.percent)
    from ecostspos p
    where p.ecost = new.ecost
    into :percents;

  if (percents = 100) then
    update ecosts set status=1 where ref=new.ecost;
  else
    update ecosts set status=0 where ref=new.ecost;
end^
SET TERM ; ^
