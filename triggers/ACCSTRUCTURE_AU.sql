--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_AU FOR ACCSTRUCTURE                   
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable yearid integer;
  declare variable company integer;
  declare variable account varchar(20);
  declare variable accounting integer;
  declare variable descript varchar(80);
  declare variable fulldescript varchar(255);
begin
  select yearid,  company
    from bkaccounts
    where ref = new.bkaccount
    into :yearid,  :company;

  if(new.doopisu<>old.doopisu) then begin
  -- aktualizacja opisu kont ksiegowych dla biezacego roku
    if(:yearid = extract(year from current_timestamp(0))) then begin
      for select account, ref
        from accounting
        where bkaccount = new.bkaccount
        and yearid = :yearid
        and company = :company
        into :account, :accounting
      do begin
        execute procedure ACCOUNTING_GET_DESCRIPT(:account, :yearid, :company)
          returning_values :descript, :fulldescript;
        update accounting set descript = :descript where ref = :accounting;
--        update accounting set fulldescript = :fulldescript where ref = :accounting;
      end
    end
  -- dla roku innego niż bieżący zmiana możliwa dla kont bez zapisów
    else if(not exists(select first 1 1 from accounting where bkaccount = new.bkaccount and yearid = :yearid)) then begin
      for select account, ref
        from accounting
        where bkaccount = new.bkaccount
        and yearid = :yearid
        and company = :company
        into :account, :accounting
      do begin
        execute procedure ACCOUNTING_GET_DESCRIPT(:account, :yearid, :company)
          returning_values :descript, :fulldescript;
        update accounting set descript = :descript where ref = :accounting;
--        update accounting set fulldescript = :fulldescript where ref = :accounting;
      end
    end
  end
end^
SET TERM ; ^
