--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWKODKRESK_AU0 FOR TOWKODKRESK                    
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.gl <> old.gl and new.gl = 1) then begin
    update TOWKODKRESK set GL = 0 where WERSJAREF = new.wersjaref and GL = 1 and ref<>new.ref;
  end
  if(new.gl = 1 and (new.kodkresk <> old.kodkresk or new.gl<>old.gl) ) then
    update WERSJE set KODKRESK = new.kodkresk where ref=new.wersjaref;

end^
SET TERM ; ^
