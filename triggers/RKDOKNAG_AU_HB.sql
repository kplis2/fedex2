--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AU_HB FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 5 
as
begin
  if (coalesce(old.ticaccount,'') <> coalesce(new.ticaccount,'')) then
    execute procedure HB_UPDATE_STATEMENT_POSITION(new.ref);
end^
SET TERM ; ^
