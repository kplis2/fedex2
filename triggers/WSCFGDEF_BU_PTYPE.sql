--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WSCFGDEF_BU_PTYPE FOR WSCFGDEF                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.ptype is distinct from old.ptype and old.ptype = 0) then
  begin
    if (exists(select first 1 1 from wscfgval v join epayrules r on (r.ref = v.epayrule)
                 where r.isdefault = 0 and v.wscfgdef = new.symbol and v.company = new.company)
    ) then
      exception universal 'Zmiana okresu niemożliwa - parametr wykorzystywany poza regulaminem domyślnym!';
  end
end^
SET TERM ; ^
