--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTORZY_AD_REPLICAT FOR SPEDYTORZY                     
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('SPEDYTORZY', old.symbol, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
