--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_IMP_TOWKODKRESK_BI FOR X_IMP_TOWKODKRESK              
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_x_imp_towkodkresk_id,1);
  if(new.ilprob is null) then
    new.ilprob = 0;
end^
SET TERM ; ^
