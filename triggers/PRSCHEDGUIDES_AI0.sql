--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_AI0 FOR PRSCHEDGUIDES                  
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable prschedguidelast integer;
begin
  select first 1 ref from prschedguides where prschedzam = new.prschedzam  and status = 0
    order by starttime desc nulls first, numer desc into :prschedguidelast;
  if(prschedguidelast is null or prschedguidelast = new.ref) then begin
    update prschedguides set lastone = 1 where ref = new.ref;
    update prschedguides set lastone = 0 where ref <> new.ref and prschedzam = new.prschedzam;
  end
end^
SET TERM ; ^
