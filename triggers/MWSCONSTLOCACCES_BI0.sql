--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCACCES_BI0 FOR MWSCONSTLOCACCES               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('MWSCONSTLOCACCES')
      returning_values new.REF;
end^
SET TERM ; ^
