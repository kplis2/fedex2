--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPEMPLSORTDEF_BI_REF FOR BHPEMPLSORTDEF                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BHPEMPLSORTDEF')
      returning_values new.REF;
end^
SET TERM ; ^
