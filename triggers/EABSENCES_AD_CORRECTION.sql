--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AD_CORRECTION FOR EABSENCES                      
  ACTIVE AFTER DELETE POSITION 0 
as
  declare variable corrected integer;
  declare variable correction smallint;
  declare variable corepayroll integer;
begin
/* MWr: uaktywnienie nieobecnosci skorygowanej po usunieciu biezacej(korygujacej)
   musi sie wykonac przed triggerem EABSENCES_AIUD_CHKCONTINUITY aby zawarta w nim
   procedura E_GET_ABSENCE_PERIOD_BASES prawidlowo ustalila ciaglosc nieobcnosci
   pozniejszych. Trigger nie moze sie wykonac, jezeli usuniecie spowodowane jest
   update'em na korkecie - do tego celu sluzy znacznik MFLAG*/

  if (old.corrected is not null and coalesce(old.mflag,0) = 0) then
  begin
     --sprawdzenie czy nieobecnosc korygowana nie jest korekta dla jakiejs innej
    select corrected
      from eabsences
      where ref = old.corrected
      into :corrected;
    if (corrected is null) then
      correction = 0;
    else correction = 2;

    --jezeli nieobecnosc korygowana jest korygujaca dla innej nieobecnosci nalezy przywrocic
    --stary corepayroll, w innym przypadku wstawi null
    if (correction = 2) then
      select a.corepayroll
        from eabsences a
        where ref = :corrected
        into :corepayroll;
    
    update eabsences
      set correction = :correction , corepayroll = :corepayroll
      where ref = old.corrected;
  end
end^
SET TERM ; ^
