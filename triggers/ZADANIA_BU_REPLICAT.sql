--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_BU_REPLICAT FOR ZADANIA                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.REF <> old.ref)
   or(new.stan <> old.stan ) or (new.stan is null and old.stan is not null) or (new.stan is not null and old.stan is null)
   or(new.datawyk <> old.datawyk ) or (new.datawyk is null and old.datawyk is not null) or (new.datawyk is not null and old.datawyk is null)
   or(new.opiswyk <> old.opiswyk ) or (new.opiswyk is null and old.opiswyk is not null) or (new.opiswyk is not null and old.opiswyk is null)
   or(new.operwyk <> old.operwyk ) or (new.operwyk is null and old.operwyk is not null) or (new.operwyk is not null and old.operwyk is null)
   or(new.data <> old.data ) or (new.data is null and old.data is not null) or (new.data is not null and old.data is null)
   or(new.prawa <> old.prawa ) or (new.prawa is null and old.prawa is not null) or (new.prawa is not null and old.prawa is null)
   or(new.prawagrup <> old.prawagrup ) or (new.prawagrup is null and old.prawagrup is not null) or (new.prawagrup is not null and old.prawagrup is null)
   or(new.datado <> old.datado ) or (new.datado is null and old.datado is not null) or (new.datado is not null and old.datado is null)
   or(new.priorytet <> old.priorytet ) or (new.priorytet is null and old.priorytet is not null) or (new.priorytet is not null and old.priorytet is null)
   or(new.opis <> old.opis ) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
   or(new.cpodmiot <> old.cpodmiot ) or (new.cpodmiot is null and old.cpodmiot is not null) or (new.cpodmiot is not null and old.cpodmiot is null)
   or(new.osoba <> old.osoba ) or (new.osoba is null and old.osoba is not null) or (new.osoba is not null and old.osoba is null)
   or(new.operzal <> old.operzal ) or (new.operzal is null and old.operzal is not null) or (new.operzal is not null and old.operzal is null)
   or(new.cpodmiot <> old.cpodmiot ) or (new.cpodmiot is null and old.cpodmiot is not null) or (new.cpodmiot is not null and old.cpodmiot is null)
   or(new.ckontrakt <> old.ckontrakt ) or (new.ckontrakt is null and old.ckontrakt is not null) or (new.ckontrakt is not null and old.ckontrakt is null)
   or(new.kontakt <> old.kontakt ) or (new.kontakt is null and old.kontakt is not null) or (new.kontakt is not null and old.kontakt is null)
   or(new.zadanie <> old.zadanie ) or (new.zadanie is null and old.zadanie is not null) or (new.zadanie is not null and old.zadanie is null)
   or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
   or(new.operuczest <> old.operuczest ) or (new.operuczest is null and old.operuczest is not null) or (new.operuczest is not null and old.operuczest is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('ZADANIA',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
