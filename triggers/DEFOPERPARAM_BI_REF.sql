--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERPARAM_BI_REF FOR DEFOPERPARAM                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFOPERPARAM')
      returning_values new.REF;
end^
SET TERM ; ^
