--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRPAYROLLS_BI0 FOR ECONTRPAYROLLS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable cnt integer;
begin
  if (new.operator is null and new.employee is not null) then
  begin
    select count(*) from operator where operator.employee = new.employee
      into :cnt;
/*    if (:cnt = 0) then
      exception OPERATOR_NOT_ASSIGNED;
    else */if (:cnt = 1) then
      select operator.ref from operator where operator.employee = new.employee
        into new.operator;
  end
end^
SET TERM ; ^
