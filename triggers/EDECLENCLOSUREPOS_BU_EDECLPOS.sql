--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLENCLOSUREPOS_BU_EDECLPOS FOR EDECLENCLOSUREPOS              
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  update edeclpos d set d.field = new.field, d.edeclposdef = new.edeclposdef, d.pvalue = new.pvalue
    where d.ref = new.ref;
end^
SET TERM ; ^
