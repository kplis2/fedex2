--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_SYNCDEF_BU_REPLICAT FOR RP_SYNCDEF                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.TABELA<>old.TABELA
     or(new.locatupr <> old.locatupr) or (new.locatupr is null and old.locatupr is not null) or (new.locatupr is not null and old.locatupr is null)
     ) then
   waschange = 1;

  execute procedure rp_trigger_bu('RP_SYNCDEF',old.tabela, null, null, null, null, old.token, old.state,
        new.tabela, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
