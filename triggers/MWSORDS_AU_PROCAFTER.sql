--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_AU_PROCAFTER FOR MWSORDS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable procname varchar(1024);
declare variable refmwsord integer;
declare variable docid integer;
declare variable docgroup integer;
declare variable doctype char;
begin
  if (new.status = 5 and old.status <> 5) then
  begin
    -- najpierw szukamy nazwy procedury na przypisaniu zleceń do magazynów
    select PROCONMWSORDCOMMIT
      from mwsordtypes4wh
      where wh = new.wh and mwsordtype = new.mwsordtype
      into procname;
    -- potem szukamy czy jest definicja procedury na definicji zlecenia
    if (procname is null or procname = '') then
      select PROCONMWSORDCOMMIT from mwsordtypes where ref = new.mwsordtype
        into procname;
    if (procname is null or procname = '') then
      exit;
    else
    begin
      if (new.docid is null) then docid = 0; else docid = new.docid;
      if (new.docgroup is null) then docgroup = 0; else docgroup = new.docgroup;
      if (new.doctype is null) then doctype = 0; else doctype = new.doctype;
      -- generujemy zlecenie
      procname = 'select refmwsord from '||procname||'('||docid||','||docgroup||',null,'''||doctype||''','||new.ref||',null,1,null)';
      if (procname is not null and procname <> '') then
        execute statement procname into refmwsord;
      else 
        exception PROCGENMWSACTS_NOT_SET;
    end
  end
  else if (new.status <> 5 and old.status = 5) then
  begin
    procname = null;
    select m.procafterorddecommit
      from mwsordtypes4wh m
      where m.wh = new.wh and m.mwsordtype = new.mwsordtype
      into :procname;
    if (procname <> '' and procname is not null) then
    begin
      if (new.docid is null) then docid = 0; else docid = new.docid;
      if (new.docgroup is null) then docgroup = 0; else docgroup = new.docgroup;
      if (new.doctype is null) then doctype = 0; else doctype = new.doctype;
      -- generujemy zlecenie
      procname = 'select refmwsord from '||procname||'('||docid||','||docgroup||',null,'''||doctype||''','||new.ref||',null,1,null)';
      execute statement procname
        into refmwsord;
    end
  end else if (new.status = 2 and old.status = 1) then
  begin
    procname = '';
    -- najpierw szukamy nazwy procedury na przypisaniu zleceń do magazynów
    select PROCONMWSORDREAL
      from mwsordtypes4wh
      where wh = new.wh and mwsordtype = new.mwsordtype
      into procname;
    -- potem szukamy czy jest definicja procedury na definicji zlecenia
    if (procname is null or procname = '') then
      select PROCONMWSORDREAL from mwsordtypes where ref = new.mwsordtype
        into procname;
    if (procname is null or procname = '') then
      exit;
    else
    begin
      if (new.docid is null) then docid = 0; else docid = new.docid;
      if (new.docgroup is null) then docgroup = 0; else docgroup = new.docgroup;
      if (new.doctype is null) then doctype = 0; else doctype = new.doctype;
      -- generujemy zlecenie
      procname = 'select refmwsord from '||procname||'('||docid||','||docgroup||',null,'''||doctype||''','||new.ref||',null,1,null)';
      if (procname is not null and procname <> '') then
        execute statement procname into refmwsord;
      else 
        exception PROCGENMWSACTS_NOT_SET;
    end
  end
end^
SET TERM ; ^
