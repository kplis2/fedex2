--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMROZ_AI0 FOR DOKUMROZ                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable mag char(3);
declare variable wydania integer;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable data timestamp;
declare variable dokument integer;
declare variable zamowienie integer;
declare variable dokumref integer;
declare variable zdejblok numeric(14,4);
declare variable korekta smallint;
declare variable opk smallint;
declare variable opktryb smallint;
declare variable slodef integer;
declare variable slopoz integer;
declare variable bn varchar(1);
declare variable dokpozref integer;
declare variable sumilkor numeric(14,4);
declare variable back smallint;
declare variable pozycja integer;
declare variable numer integer;
declare variable koropkrozid integer;
declare variable bktm varchar(40);
declare variable bwersja integer;
declare variable local smallint;
declare variable typ varchar(3);
declare variable ilosc numeric(14,4);
declare variable operakcept integer;
declare variable ujemne smallint;
declare variable datadok timestamp;
declare variable stanyujemne smallint;
begin
 if(new.ack = 1 or new.ack = 9) then begin
  select DEFDOKUM.wydania, DEFDOKUM.KORYG,dokumnag.magazyn, DOKUMPOZ.KTM,DOKUMPOZ.wersja,
    DOKUMNAG.DATA, DOKUMNAG.REF, DOKUMPOZ.FROMZAM,DOKUMPOZ.OPK,DEFDOKUM.OPAK,DOKUMNAG.SLODEF, DOKUMNAG.SLOPOZ,
    DEFDOKUM.kaucjabn, DOKUMPOZ.REF, dokumpoz.bktm, dokumpoz.bwersja, DOKUMNAG.OPERAKCEPT,
    DOKUMNAG.DATA, DEFDOKUM.stanyujemne
  from DOKUMPOZ
  left join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.DOKUMENT)
  left join DEFDOKUM on(DOKUMNAG.TYP = DEFDOKUM.symbol)
     where DOKUMPOZ.ref = NEW.pozycja
     into :wydania,:korekta,:mag,:ktm,:wersja,
          :data, :dokumref, :zamowienie,:opk,:opktryb,:slodef,:slopoz,
          :bn,:dokpozref, :bktm, :bwersja, :operakcept,
          :datadok, :stanyujemne;
  if(new.ack = 9) then begin
    ktm = :bktm;
    wersja = :bwersja;
  end
  if(:wydania = 1) then begin
    if(:korekta = 1) then begin
      /* totak, jak kasowanie dokumentu przychodowego*/
      execute procedure REZ_BLOK_PRZESUN(:mag,:ktm,:wersja,new.cena, new.dostawa,new.ilosc);
      execute procedure REZ_BLOK_CHECK_ex(:mag,:ktm,:wersja,new.cena,new.dostawa,new.ilosc);
    end else begin
      /*wydanie rzeczywiste*/
      execute procedure REZ_BLOK_REALIZE(:zamowienie, :dokumref, :mag,:ktm,:wersja,new.cena,new.dostawa, new.ilosc) returning_values :zdejblok;
      execute procedure REZ_BLOK_CHECK(new.pozycja, new.numer,new.ilosc, :zdejblok);
    end
    execute procedure MINUS_SC(:mag,:ktm,:wersja,new.ilosc,new.cena,new.dostawa,new.serialnr,NULL,new.cena_koszt, new.orgdokumser,:datadok);
    -- kontrola mozliwosci zejscia na stany ujemne dla operatora
    select ILOSC from STANYIL where MAGAZYN=:mag and KTM=:ktm and WERSJA=:wersja into :ilosc;
    if(:ilosc<0) then begin
      select UJEMNE from OPERMAG where MAGAZYN=:mag and OPERATOR = :operakcept into :ujemne;
      if(:ujemne is null) then ujemne = 0;
      if(:ujemne=0) then exception STIL_MINUS 'Brak uprawnień operatora do zejścia na stan ujemny dla: '||:ktm;
      if(:stanyujemne is null) then stanyujemne = 0;
      if(:stanyujemne=0) then exception STIL_MINUS 'Brak uprawnień typu dok. do zejścia na stan ujemny dla: '||:ktm;
    end
  end else begin
    execute procedure REZ_DOST_REALIZE(:zamowienie, :dokumref, :mag,:ktm,:wersja,new.cena,new.dostawa, new.ilosc) returning_values :zdejblok;
    execute procedure PLUS_SC(:mag,:ktm,:wersja,new.ilosc,new.cena,new.dostawa,new.serialnr,:data,new.cena_koszt, new.orgdokumser,:datadok);
    execute procedure rez_blok_rozpisz(:mag, :ktm, :wersja);
  end
  /*jesli to korekta ilosciowa. to uaktualnienie ilosci na dokumencie oryginalnym*/
  if(new.kortoroz> 0) then begin
     update DOKUMROZ set ILOSCL = ILOSCL - new.ilosc where REF=new.kortoroz;
  end
 end
 if(new.serialnr <> '' and new.ack <> 8 and new.ack <> 7) then begin
   execute procedure SERNR_ADD_ROZ(new.pozycja, new.ilosc, new.serialnr);
 end
 execute procedure CHECK_LOCAL('DOKUMPOZ',new.pozycja) returning_values :local;
 if(:local = 1) then
   update DOKUMPOZ set ILOSCL = ILOSCL + new.iloscl, ILOSCROZ = ILOSCROZ +new.ilosc  where ref=new.pozycja;
 /*naliczenie opakowan*/
 if(new.ack <> 8 and new.ack <> 7) then begin
   /*tryb opakowan i tryb kaucji rozliczany wg pozycji oryginalnej*/
   back = 0;
   pozycja = new.pozycja;
   numer = new.numer;
   if(new.kortoroz > 0) then begin
     select  DEFDOKUM.symbol, DEFDOKUM.OPAK, DEFDOKUM.kaucjabn, dokumpoz.ref, dokumroz.numer, dokumpoz.opk
     from DOKUMROZ
     left join dokumpoz on (dokumroz.pozycja = dokumpoz.ref)
     left join DOKUMNAG on (DOKUMNAG.ref = dokumpoz.dokument)
     left join defdokum on (defdokum.symbol = dokumnag.typ)
     where dokumroz.ref = new.kortoroz
     into :typ,:opktryb, :bn, :dokpozref, :numer, :opk;
     back = 1;
     pozycja = dokpozref;

   end
   if(:opk>0) then begin
     if(:opktryb = 1)then
       if(:bn = 'B') then
         execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,new.ilosc,new.cena,new.cenasbrutto,:dokpozref,:back);
       else
         execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,new.ilosc,new.cena,new.cenasnetto,:dokpozref,:back);
     else if(:opktryb = 2 ) then begin
       if(new.kortoroz > 0) then begin
         select opkrozid from DOKUMROZ where ref=new.kortoroz into :koropkrozid;
         if(:koropkrozid > 0) then
           execute procedure OPK_STANYAKTUREAL(:koropkrozid);
       end else begin
         if(:bn = 'B') then
           execute procedure OPK_DEL(:slodef, :slopoz,:ktm,:wersja,new.ilosc,new.cena,new.cenasbrutto,:pozycja, :numer,:back);
         else
           execute procedure OPK_DEL(:slodef, :slopoz,:ktm,:wersja,new.ilosc,new.cena,new.cenasnetto,:pozycja,:numer,:back);
       end
     end
   end
 end
 if(new.blokoblnag = 0) then begin
   select dokument from DOKUMPOZ where ref=new.pozycja into :dokument;
   execute procedure DOKMAG_OBL_WAR(:dokument,0);
 end
end^
SET TERM ; ^
