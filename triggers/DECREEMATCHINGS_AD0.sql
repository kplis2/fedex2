--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREEMATCHINGS_AD0 FOR DECREEMATCHINGS                
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.decreematchinggroup > 0 and old.blockcalc = 0) then
    execute procedure decreematchinggroup_calc(old.decreematchinggroup);
end^
SET TERM ; ^
