--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLPARAMS_TMP_BI_REF FOR EPAYROLLPARAMS_TMP             
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EPAYROLLPARAMS_TMP') returning_values new.ref;
end^
SET TERM ; ^
