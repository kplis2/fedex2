--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRPAYROLLSNAG_BU0 FOR ECONTRPAYROLLSNAG              
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable cnt int;
begin
  if (new.premcalc is null) then new.premcalc = 0;
  if (new.addings is null) then new.addings = 0;
  if ((new.sumworktime > 0)
      and ((new.sumworktime is not null and old.sumworktime is null)
           or (new.sumworktime <> old.sumworktime)
           or (new.sumpoints is not null and old.sumpoints is null)
           or (new.sumpoints <> old.sumpoints))) then
  begin
    new.pointsphour = new.sumpoints/new.sumworktime;
  end
  else
    new.pointsphour = 0;
--  if (new.addings <> old.addings or new.summoney <> old.summoney or
--      (new.addings is not null and old.addings is null) or (new.summoney is not null and old.summoney is null)) then
    new.topay = new.addings + new.summoney;
  if (new.employee <> old.employee) then
  begin
    select count(*) from operator where operator.employee = new.employee
      into :cnt;
/*    if (:cnt = 0) then
      exception OPERATOR_NOT_ASSIGNED;
    else */if (:cnt = 1) then
      select operator.ref from operator where operator.employee = new.employee
        into new.operator;
  end
end^
SET TERM ; ^
