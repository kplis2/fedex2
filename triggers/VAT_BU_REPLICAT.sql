--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VAT_BU_REPLICAT FOR VAT                            
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable waschange smallint;
begin
  waschange = 0;
  if (new.GRUPA<>old.GRUPA
      or new.stawka<>old.stawka or (new.stawka is null and old.stawka is not null) or (new.stawka is not null and old.stawka is null)
  ) then
    waschange = 1;

  execute procedure rp_trigger_bu('VAT',old.grupa, null, null, null, null, old.token, old.state,
        new.grupa, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
