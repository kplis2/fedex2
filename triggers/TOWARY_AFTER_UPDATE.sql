--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWARY_AFTER_UPDATE FOR TOWARY                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable defcennik integer;
declare variable vatbn char(1);
declare variable wersjaref integer;
declare variable stawka numeric(14,2);
declare variable aktuoddzial varchar(255);
declare variable magazyn varchar(3);
declare variable ilosc numeric(14,4);
declare variable stanmin numeric(14,4);
declare variable stanmax numeric(14,4);
declare variable kolor smallint;
declare variable newkolor smallint;
declare variable minmaxstan smallint;
declare variable mwsnazwa string80;
begin
   if(new.AKT <> old.AKT) then begin
        update WERSJE set AKT = new.AKT where KTM = old.KTM;
   end
   if(new.ktm<>old.ktm) then begin
     update ATRYBUTY set KTM=new.ktm where KTM=old.ktm;
     update CENNIK set KTM=new.ktm where KTM=old.ktm;
     update stanyil set KTM=new.ktm where KTM=old.ktm;
   end
   if((new.nazwa <> old.nazwa) or (new.miara <> old.miara) or (new.chodliwy <> old.chodliwy) or
      (new.grupa <> old.grupa) or (new.grupa is not null and old.grupa is null) or (new.grupa is null and old.grupa is not null) or
      (new.usluga <>old.usluga) or (new.altposmode <> old.altposmode))
   then begin
      update WERSJE set NAZWAT = new.nazwa, MIARA = new.miara, CHODLIWY=new.chodliwy, GRUPA = new.grupa, USLUGA=new.usluga where KTM = old.ktm;
      update STANYIL set NAZWAT = new.nazwa, MIARA = new.miara, CHODLIWY=new.chodliwy, GRUPA = new.grupa, USLUGA=new.usluga, ALTPOSMODE=new.altposmode
        where KTM = new.ktm;
      update STANYOPK set NAZWAT = new.nazwa, MIARA = new.miara, CHODLIWY=new.chodliwy, GRUPA = new.grupa, USLUGA=new.usluga where KTM = new.ktm;
      update CENNIK set NAZWAT = new.nazwa where KTM = new.ktm;
      update INWENTAP set NAZWAT = new.nazwa where KTM = new.ktm;
      update GRUPYKUPOFE set NAZWAT = new.nazwa where KTM = new.ktm;
      update STKRPOZ set NAZWAT = new.nazwa, miara = new.miara where KTM = new.ktm;
   end
   if((new.CENA_ZAKB <> old.CENA_ZAKB) or (new.CENA_ZAKN <> old.CENA_ZAKN)
      or (new.CENA_FABB <> old.CENA_FABB) or (new.CENA_FABN <> old.CENA_FABN)
      or (new.CENA_KOSZTB <> old.CENA_KOSZTB) or (new.CENA_KOSZTN <> old.CENA_KOSZTN)
      or (new.kodkresk <> old.kodkresk) or (new.kodkresk is not null and old.kodkresk is null) or (new.kodkresk is null and old.kodkresk is not null)
      or (new.CENA_ZAKNL <> old.CENA_ZAKNL)
      or (new.DNIWAZN <> old.DNIWAZN)
      or (new.MARZAMIN <> old.MARZAMIN) or (new.marzamin is not null and old.marzamin is null) or (new.marzamin is null and old.marzamin is not null)
      or (new.SYMBOL_DOST <> old.SYMBOL_DOST)
      or (new.BKSYMBOL <> old.BKSYMBOL) or (new.BKSYMBOL is not null and old.BKSYMBOL is null) or (new.BKSYMBOL is null and old.BKSYMBOL is not null))
    then  begin
       update wersje set SYMBOL_DOST = new.SYMBOL_DOST, BKSYMBOL = new.bksymbol, CENA_ZAKN = new.CENA_ZAKN, CENA_ZAKB = new.cena_zakb
       ,CENA_fabN = new.CENA_fabN, CENA_fabB = new.cena_fabb
       ,CENA_kosztN = new.CENA_kosztN, CENA_kosztB = new.cena_kosztb
       ,DNIWAZN = new.dniwazn
       ,MARZAMIN = new.marzamin
       ,CENA_ZAKNL = new.cena_zaknl
       ,kodkresk = new.kodkresk
         where KTM = old.KTM and NRWERSJi = 0;
     if (new.symbol_dost<>old.symbol_dost) then update stanyil set symbol_dost = new.symbol_dost where ktm = new.ktm;
   end
   if(new.miara <> old.miara) then begin
     if(exists(select first 1 1 from TOWJEDN where JEDN = new.miara and KTM = old.ktm and GLOWNA = 0)) then
       exception TOWJEDN_EXPT 'Towar '||old.ktm||' już ma jednostkę '||new.miara||'. Najpierw usuń jednostkę dodatkową.';
   end
   if((new.miara <> old.miara)
      or (new.kodkresk <> old.kodkresk) or (new.kodkresk is not null and old.kodkresk is null) or (new.kodkresk is null and old.kodkresk is not null)
     ) then begin
     update TOWJEDN set JEDN = new.miara , KODKRESK = new.kodkresk where KTM = old.ktm and GLOWNA = 1;
   end
  if(new.vat <> old.vat ) then  begin
    select stawka from VAT where grupa = new.vat into :stawka;
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
    for select ref,vatbn from defcennik
    where ((DEFCENNIK.ODDZIAL='') or (DEFCENNIK.oddzial is null) or (DEFCENNIK.oddzial = :aktuoddzial))
    into :defcennik, :vatbn
    do begin
      if(:vatbn is null or :vatbn='N') then begin
        update CENNIK set CENNIK.cenabru = cennik.cenanet * (100 + :stawka)/100
        where CENNIK.KTM = new.ktm and CENNIK.CENNIK=:defcennik;
      end else begin
        update CENNIK set CENNIK.cenanet = cennik.cenabru / ((100 + :stawka)/100)
        where CENNIK.KTM = new.ktm and CENNIK.CENNIK=:defcennik;
        update CENNIK set CENNIK.cenabru = cennik.cenanet * (100 + :stawka)/100
        where CENNIK.KTM = new.ktm and CENNIK.CENNIK=:defcennik;
        for select ref from wersje where ktm=new.ktm into :wersjaref
        do begin
          execute procedure CENNIK_AKTUMARZA(:wersjaref);
        end
      end
    end
  end
  if(coalesce(new.supplymethod,0)<>coalesce(old.supplymethod,0)) then begin
    -- dla metod min/max i DBM podmien pole KOLOR na STANYIL
    if(new.supplymethod>=2) then begin
      for select MAGAZYN,WERSJAREF,ILOSC,STANMIN,STANMAX,KOLOR,MINMAXSTAN
        from STANYIL
        where KTM=new.ktm
        into :magazyn, :wersjaref, :ilosc, :stanmin, :stanmax, :kolor, :minmaxstan
      do begin
        execute procedure GET_STANYIL_KOLOR(:ilosc,:stanmin,:stanmax,:kolor,:minmaxstan,new.ktm) returning_values :newkolor;
        if(coalesce(:newkolor,0)<>coalesce(:kolor,0)) then begin
          update STANYIL set KOLOR=:newkolor where magazyn=:magazyn and wersjaref=:wersjaref;
        end
      end
    end
  end

  if (old.nazwa is distinct from new.nazwa) then
  begin
    execute procedure TOWAR_MWSNAZWA(new.KTM) returning_values :mwsnazwa;
    update towary set mwsnazwa = :mwsnazwa where KTM = new.KTM;
  end
end^
SET TERM ; ^
