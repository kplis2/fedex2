--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIG_BU_REPLICAT FOR KONFIG                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.AKRONIM<>old.AKRONIM or
      new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null) or
      new.wartosc <> old.wartosc or (new.wartosc is null and old.wartosc is not null) or (new.wartosc is not null and old.wartosc is null) or
      new.grupa <> old.grupa or (new.grupa is null and old.grupa is not null) or (new.grupa is not null and old.grupa is null) or
      new.numer <> old.numer or (new.numer is null and old.numer is not null) or (new.numer is not null and old.numer is null) or
      new.typ <> old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null) or
      new.wysokosc <> old.wysokosc or (new.wysokosc is null and old.wysokosc is not null) or (new.wysokosc is not null and old.wysokosc is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('KONFIG',old.akronim, null, null, null, null, old.token, old.state,
        new.akronim, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
