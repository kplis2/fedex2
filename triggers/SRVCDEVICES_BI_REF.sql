--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVCDEVICES_BI_REF FOR SRVCDEVICES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVCDEVICES')
      returning_values new.REF;
end^
SET TERM ; ^
