--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTAXINFO_BIU_CHECK FOR EMPLTAXINFO                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
AS
  declare variable prev_extdate timestamp;
  declare variable prev_autdate date;
  declare variable prev_alloblockdate date;
begin

 if ((new.fromdate > new.autdate or new.fromdate > new.alloblockdate or new.fromdate > new.extdate)
    and (inserting or new.fromdate <> old.fromdate or new.autdate is distinct from old.autdate
      or new.alloblockdate is distinct from old.alloblockdate or new.extdate is distinct from old.extdate)
  ) then begin

    select first 1 autdate, alloblockdate, extdate from empltaxinfo
      where employee = new.employee
        and fromdate < new.fromdate
      order by fromdate desc
      into :prev_autdate, :prev_alloblockdate, prev_extdate;

    if ( (new.fromdate > new.autdate and new.autdate is distinct from prev_autdate)
      or (new.fromdate > new.alloblockdate and new.alloblockdate is distinct from prev_alloblockdate)
      or (new.fromdate > new.extdate and new.extdate is distinct from prev_extdate)
    ) then
      exception universal 'Daty oświadczeń nie mogą być wcześniejsze od daty obowiązywania danych podatkowych!';
  end

  if ((inserting or new.extdate is distinct from old.extdate
      or coalesce(new.extamount,0) <> coalesce(old.extamount,0))
    and ((new.extdate is not null and coalesce(new.extamount,0) = 0)
      or (new.extdate is null and coalesce(new.extamount,0) <> 0))
  ) then
    exception universal 'Oświadczenie dochodu zewnętrznego wymaga podania zarówno jego kwoty jak i daty złożenia!';

end^
SET TERM ; ^
