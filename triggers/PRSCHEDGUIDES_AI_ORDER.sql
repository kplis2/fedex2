--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_AI_ORDER FOR PRSCHEDGUIDES                  
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.ord <> 1) then
    execute procedure order_prschedguides(new.prschedule, new.number, new.number);
end^
SET TERM ; ^
