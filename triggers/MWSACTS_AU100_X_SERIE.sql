--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AU100_X_SERIE FOR MWSACTS                        
  ACTIVE AFTER UPDATE POSITION 100 
AS
begin
    -- usuwanie numeru seryjnego jezeli zamowienie zostao  wycofane/anulowane itp i numer seryjny rejestrowany tylko na wyjsciu
  if (old.status = 5 and new.status <> old.status and new.x_serial_no is not null) then
  begin
    DELETE from x_mws_serie ms where ms.ref = new.x_serial_no and ms.typ = 2;
  end
end^
SET TERM ; ^
