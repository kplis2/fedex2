--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRGANTT_BU0 FOR PRGANTT                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable prmachinesymb varchar(40);
begin
  if (coalesce(new.prmachine,0) <> coalesce(old.prmachine,0)) then
  begin
    prmachinesymb = null;
    select p.symbol
      from prmachines p
      where p.ref = new.prmachine
      into prmachinesymb;
    new.prmachinesymb = prmachinesymb;
  end
  if (coalescE(new.prmachine,0) <> coalesce(old.prmachine,0)) then
    new.descript = new.prschedoper||' ('||new.prmachinesymb||') '||coalescE(new.prmachine,0);
end^
SET TERM ; ^
