--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_OPK_BI_REF FOR LISTYWYSDROZ_OPK               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('LISTYWYSDROZ_OPK')
      returning_values new.REF;
end^
SET TERM ; ^
