--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDOKWIT_AU0 FOR DEFDOKWIT                      
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.forma <> old.forma and new.forma is not null ) then
    /* aktywacja formularza, ktory byl do tej pory nieaktywny - na wszelki wypadek*/
    update ODEFFORMY set akt = 1 where symbol = new.forma and akt = 0;
end^
SET TERM ; ^
