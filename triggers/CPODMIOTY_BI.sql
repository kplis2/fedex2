--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_BI FOR CPODMIOTY                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if (new.synchro = 0) then new.synchro = null;
  if(new.korespond is null) then new.korespond = 0;
  if(new.skrot = '') then new.skrot = NULL;
  if(new.cpowiaty = 0) then new.cpowiaty = NULL;
  if(new.obywatelstwo = 0) then new.obywatelstwo = NULL;
  if(new.datazal is null) then new.datazal = current_date;
  if(new.nip <> '' and new.nip is not null) then
    new.prostynip = replace(new.nip, '-', '');
END^
SET TERM ; ^
