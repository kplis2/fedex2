--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKZAL_BI FOR NAGFAKZAL                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable akcept integer;
declare variable typ varchar(3);
declare variable zaliczkowy integer;
declare variable korekta integer;
declare variable nieobrot integer;
begin
  select akceptacja,typ,zaliczkowy,nieobrot from nagfak where ref=new.faktura into :akcept,:typ,:zaliczkowy,:nieobrot;
  select korekta from typfak where symbol=:typ into :korekta;
  if(:nieobrot not in (0,2)) then
    exception FAKZAL_TO_NIEKSIEG;
  if(akcept = 1 ) then
    exception NAGFAKZAL_AKCEPTACJA;
  if(:zaliczkowy=1) then exception NAGFAKZAL_AKCEPTACJA 'Nie można rozliczać faktury zaliczkowej inną fakturą zaliczkową.';
  if(:zaliczkowy=0 and :korekta=1) then exception NAGFAKZAL_AKCEPTACJA 'Nie można rozliczać faktury zaliczkowej fakturą korygującą.';
  new.pwartnet = new.wartnet;
  new.pwartbru = new.wartbru;
  new.pwartnetzl = new.wartnetzl;
  new.pwartbruzl = new.wartbruzl;
end^
SET TERM ; ^
