--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BU_PALGROUOP FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable cor smallint;
declare variable docid integer;
begin
  if (new.status > 0 and old.status = 0 and new.recdoc = 1 and new.mwsordtypedest = 2
      and new.palgroup is null and new.stocktaking = 0
  ) then
  begin
    select docid from mwsords where ref = new.mwsord into docid;
    if (docid is not null) then
    begin
      select d.koryg
        from dokumnag n
          left join defdokum d on (n.typ = d.symbol)
        where n.ref = :docid
        into cor;
      if (cor is null) then cor = 0;
      if (cor = 0) then
        exception PALGROUP_REQUIRED;
    end
  end
end^
SET TERM ; ^
