--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERSWORKPLACES_BI_REF FOR PRSHOPERSWORKPLACES            
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  IF (NEW.REF IS NULL) THEN
    NEW.REF = GEN_ID(GEN_PRSHOPERSWORKPLACES,1);
end^
SET TERM ; ^
