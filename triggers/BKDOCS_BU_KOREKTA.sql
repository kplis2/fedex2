--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BU_KOREKTA FOR BKDOCS                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  --  jeżeli jestesmy korektą to blokujemy zmiane dokumentu korygoanego ale
  --  pozwalay go wyczyscić. Chronimy rejestr VAT
  if (new.bkdocsvatcorrectionref is distinct from old.bkdocsvatcorrectionref and new.bkdocsvatcorrectionref is not null) then
    exception bkdocs_korr_change;
  --Sprawdzam, czy nie zosta nadany ref korygowany bo jak tak to kopiuj symbol
  if (old.bkdocsvatcorrectionref is null and new.bkdocsvatcorrectionref is not null) then
     select b.symbol
       from bkdocs b
       where b.ref = new.bkdocsvatcorrectionref
       into new.bkdocsvatcorrectionsymbol;
  if (new.bkreg <> old.bkreg and new.bkdocsvatcorrectionref is not null) then
    exception bkdocs_korr_reg;
  if (new.bkdocsvatcorrectionref is null) then
    new.bkdocsvatcorrectionsymbol = null;
end^
SET TERM ; ^
