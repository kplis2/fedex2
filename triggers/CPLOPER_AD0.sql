--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLOPER_AD0 FOR CPLOPER                        
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable proced varchar(255);
declare variable cnt integer;
begin
  execute procedure CPL_OBLUCZEST(old.cpluczest,old.typpkt);
  if(old.typdok <> '' and old.refdok > 0) then begin
    execute procedure CPL_OBLICZPROGLOJDONE(old.typdok, old.refdok);
  end
end^
SET TERM ; ^
