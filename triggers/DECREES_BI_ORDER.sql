--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BI_ORDER FOR DECREES                        
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  --select max(number) from decrees where bkdoc = new.bkdoc
  --select curr_number_decrees from bkdocs where ref = new.bkdoc
    select first 1 number from decrees where bkdoc = new.bkdoc
      order by bkdoc desc, number desc
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update decrees set ord = 0, number = number + 1
      where number >= new.number and bkdoc = new.bkdoc
      order by bkdoc, number;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
