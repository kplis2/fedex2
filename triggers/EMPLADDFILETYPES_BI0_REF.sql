--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILETYPES_BI0_REF FOR EMPLADDFILETYPES               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin

  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMPLADDFILETYPES')
      returning_values new.REF;
end^
SET TERM ; ^
