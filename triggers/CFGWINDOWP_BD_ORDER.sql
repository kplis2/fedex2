--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CFGWINDOWP_BD_ORDER FOR CFGWINDOWP                     
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable maxnum integer;
begin
  if (old.ord = 0) then
    exit;
  select max(number) from cfgwindowp where cfgwindowh = old.cfgwindowh
     into :maxnum;
  if (old.number = :maxnum) then
    exit;
  update cfgwindowp set ord = 0, number = number - 1
    where (cfgwindowh <> old.cfgwindowh or config <> old.config) and number > old.number and cfgwindowh = old.cfgwindowh;
end^
SET TERM ; ^
