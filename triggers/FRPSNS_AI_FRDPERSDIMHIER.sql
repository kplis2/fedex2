--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNS_AI_FRDPERSDIMHIER FOR FRPSNS                         
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable frddhref integer;
begin
  -- aktualizacja wartosci dla perspektyw, po dodaniu nowych wartosci planowanych.
  for
    select frdpersdimhier.ref
      from frhdrs
        join frdimhier on (frdimhier.frhdr = frhdrs.symbol)
        join frdpersdimhier on (frdpersdimhier.frdimshier = frdimhier.ref)
      where
        frhdrs.symbol = new.frhdr
      into :frddhref
  do begin
    insert into frdpersdimvals (frdpersdimshier, frpsn)
      values (:frddhref, new.ref);
  end
end^
SET TERM ; ^
