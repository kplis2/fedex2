--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AU_CALC_MWSORD FOR MWSACTS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable maxnumber integer;
declare variable weight numeric(14,4);
declare variable volume numeric(14,4);
declare variable timestartdecl timestamp;
declare variable timestopdecl timestamp;
declare variable closemode smallint;
declare variable posquantity integer;
declare variable maxstatus smallint;
declare variable maxactsstatus smallint;
declare variable blockstatus smallint;
declare variable orgstatus smallint;
begin
  if (new.weight <> old.weight or new.volume <> old.volume
      or new.timestartdecl <> old.timestartdecl
      or new.timestopdecl <> old.timestopdecl
      or (new.status <> old.status)
      or (new.mwsord <> old.mwsord)
  ) then
  begin
    select t.blockstatus
      from mwsords m
        left join mwsordtypes t on (m.mwsordtype = t.ref)
      where m.ref = new.mwsord
      into blockstatus;
    if (blockstatus is null) then blockstatus = 0;
    select sum(weight),sum(volume), min(timestartdecl), min(timestopdecl)
      from mwsacts
      where mwsord = new.mwsord and status <> 6
      into weight, volume, timestartdecl, timestopdecl;
    if (weight is null) then weight = 0;
    if (volume is null) then volume = 0;
    select status from mwsords where ref = new.mwsord into maxstatus;
    orgstatus = maxstatus;
    select max(number) from mwsacts where mwsord = new.mwsord into :maxnumber;
    if (new.status <> old.status) then
    begin
      select mwsordtypes.closemode
        from mwsords
          join mwsordtypes on (mwsordtypes.ref = mwsords.mwsordtype)
        where mwsords.ref = new.mwsord
        into closemode;
      if (closemode = 1) then
      begin
        select max(status) from mwsacts where status < 6 and mwsord = new.mwsord
          into maxactsstatus;
        if (maxstatus is null) then maxstatus = 0;
        execute procedure mws_mwsord_status_fromacts(new.ref,new.mwsord,0,null,maxactsstatus, null)
          returning_values posquantity;
        if (posquantity = 0) then
        begin
          if (blockstatus = 1) then
            maxstatus = orgstatus;
          else
          maxstatus = maxactsstatus;
        end
      end
    end
    update mwsords set status = :maxstatus, mwsactsnumber = :maxnumber,
        weight = :weight, volume = :volume, timestartdecl = :timestartdecl,
        timestopdecl = :timestopdecl
      where ref = new.mwsord;
  end
end^
SET TERM ; ^
