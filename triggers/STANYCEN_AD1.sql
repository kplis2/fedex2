--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_AD1 FOR STANYCEN                       
  ACTIVE AFTER DELETE POSITION 1 
as
declare variable cnt integer;
begin
   if(old.zablokow > 0 or (old.zarezerw > 0) or (old.zamowiono > 0))then begin
      update STANYIL set ZAREZERW = ZAREZERW - old.ZAREZERW,
                         ZABLOKOW = ZABLOKOW - old.ZABLOKOW,
                         ZAMOWIONO = ZAMOWIONO - old.ZAMOWIONO
      where MAGAZYN=old.magazyn and KTM=old.ktm and WERSJA=old.wersja;
  end
end^
SET TERM ; ^
