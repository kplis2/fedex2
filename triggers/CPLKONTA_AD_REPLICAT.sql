--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLKONTA_AD_REPLICAT FOR CPLKONTA                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('CPLKONTA', old.cpluczest, old.typpkt, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
