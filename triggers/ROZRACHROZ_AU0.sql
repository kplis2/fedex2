--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHROZ_AU0 FOR ROZRACHROZ                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.winien <> old.winien or (new.ma <> old.ma)) then
    execute procedure ROZRACHROZ_OBLDOK(new.doktyp, new.dokref);
end^
SET TERM ; ^
