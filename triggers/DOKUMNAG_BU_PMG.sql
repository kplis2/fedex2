--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BU_PMG FOR DOKUMNAG                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable dp_ktm varchar(40);
declare variable dp_wartosc numeric(14,2);
declare variable dp_ilosc numeric(14,4);
declare variable dp_pmelement integer;
declare variable dp_pmplan integer;
declare variable dp_pmposition integer;
declare variable dp_jedn varchar(10);
declare variable pos_jedn varchar(10);
declare variable przelicz_jedn numeric(14,4);
declare variable rozliczpo integer;
declare variable wydania integer;
declare variable koryg integer;
begin
  -- przy odakceptowaniu trzeba zdjac koszty z planow oper.
  if (old.akcept = 1 and old.akcept <> new.akcept) then
  begin
    select dd.rozliczpo, dd.wydania, dd.koryg from defdokum dd where dd.symbol = new.typ into :rozliczpo, :wydania, :koryg;
    if (rozliczpo = 1) then
      for select dp.ktm, dp.wartosc, dp.ilosc, dp.pmelement, dp.pmplan, dp.pmposition, we.miara
           from dokumpoz dp
           left join wersje we on (dp.wersjaref = we.ref)
           where dp.dokument = new.ref
          into :dp_ktm, :dp_wartosc, :dp_ilosc, :dp_pmelement, :dp_pmplan, :dp_pmposition, :dp_jedn
      do begin
        if (dp_pmposition is not null and dp_pmposition <> 0) then
        begin
          pos_jedn = null;
          przelicz_jedn = null;
          select pmpositions.unit from pmpositions where ref = :dp_pmposition into :pos_jedn;
          if (pos_jedn <> dp_jedn) then
          begin
            select przelicz from towjedn where ktm = :dp_ktm and jedn = :pos_jedn into przelicz_jedn;
            if (przelicz_jedn is null or przelicz_jedn = 0) then
              exception universal 'Brak możliwości rozliczenia materiału. Nieznana jednostka!!' || :dp_ktm|| ' '||:dp_pmelement||pos_jedn;
            else
              dp_ilosc = :dp_ilosc / przelicz_jedn;
          end
          if (koryg = 0) then
            update pmpositions set USEDVAL = USEDVAL - :dp_wartosc, USEDQ = USEDQ - :dp_ilosc where ref = :dp_pmposition;
          else
            update pmpositions set USEDVAL = USEDVAL + :dp_wartosc, USEDQ = USEDQ + :dp_ilosc where ref = :dp_pmposition;
        end
      end
  end
end^
SET TERM ; ^
