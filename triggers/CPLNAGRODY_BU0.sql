--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLNAGRODY_BU0 FOR CPLNAGRODY                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.ktm <> old.ktm or (new.wersjaref <> old.wersjaref) or
     (old.pktconst = 1 and new.ilpkt <> old.ilpkt) or
     (old.pktconst <> new.pktconst)
  )then begin
    select count(*) from CPLOPER where NAGRODA = new.ref and ZREAL = 1 into :cnt;
    if(:cnt > 0) then exception CPLNAGRODY_WYKORZ;
  end
end^
SET TERM ; ^
