--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYS_BU0 FOR LISTYWYS                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable numerator varchar(40);
declare variable cnt integer;
declare variable BLOCKREF integer;
declare variable symbol varchar(100);
begin
  if(new.stan is null or (new.stan = ' ')) then new.stan = 'O';
  if(new.trasa = 0) then new.trasa = null;
  if(new.kierowca = 0) then new.kierowca = null;
  if(new.spedytor = '') then new.spedytor = null;
  if((new.symbol is null) or (new.symbol = ''))then
    new.symbol = 'TYM'||new.ref;
  if((new.trasa <> old.trasa or (new.trasa is not null and old.trasa is null) or (new.trasa is null and old.trasa is not null)) and new.stan <> 'O') then 

exception LISTYWYS_CHANGETRASY;
  if(new.spedytor <> old.spedytor and new.stan <> 'O') then exception LISTYWYS_CHANGESPEDYT;
  if(new.stan <> 'O' and old.stan = 'O') then begin
     execute procedure GETCONFIG('LISTYWYSNUM') returning_values :numerator;
     if(:numerator is null) then numerator = '';
     if(new.dataplwys is null) then exception LISTYWYS_DATAPLWYSNOTDONE;
     if(:numerator <> '') then begin
       execute procedure GET_NUMBER(:numerator,new.spedytor,new.trasa,new.dataplwys, 0, new.ref) returning_values new.numer, :symbol;
       new.symbol = substring(:symbol from 1 for 20);
     end
  end
  if(new.stan = 'O' and old.stan <> 'O')then begin
     execute procedure GETCONFIG('LISTYWYSNUM') returning_values :numerator;
     if(:numerator is null) then numerator = '';
     if(:numerator <> '' and new.numer > 0) then begin
       execute procedure FREE_NUMBER(:numerator, new.spedytor, new.trasa, new.dataplwys,new.numer, 0) returning_values :blockref;
       new.numer = NULL;
       new.symbol = 'TYM'||new.ref;
     end

  end
  if(new.stan = 'Z' and old.stan <> 'Z') then begin
    new.datawys = current_date;
  end else if(new.stan <> 'Z' and old.stan = 'Z') then
    new.datawys = null;
  if(new.stan <> 'Z' and ((new.spedytor<>old.spedytor) or (old.spedytor is null) or (new.trasa<>old.trasa) or (old.trasa is null)  )
     and new.spedytor is not null and new.trasa > 0) then begin
    select count(*) from SPEDYTTRAS where SPEDYTOR = new.spedytor and trasa = new.trasa into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then exception LISTYWYS_SPEDYTORBEZTRASY;
  end
  if((new.datawys<>old.datawys) or (new.datawys is not null and old.datawys is null) or (new.datawys is null and old.datawys is not null) or
     (new.dataplwys<>old.dataplwys) or (new.dataplwys is not null and old.dataplwys is null) or (new.dataplwys is null and old.dataplwys is not null))
  then begin
    if(new.datawys is not null) then
      select okres from datatookres(cast(new.datawys as date),null,null,null,0) into new.okres;
    else if (new.dataplwys is not null) then
      select okres from datatookres(cast(new.dataplwys as date),null,null,null,0) into new.okres;
  end
end^
SET TERM ; ^
