--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_AD_ETRAININGS FOR EMPLTRAININGS                  
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable enumber numeric(14,2);
  declare variable costtype smallint;
  declare variable personcost numeric(14,2);
  declare variable totalcost numeric(14,2);
begin
  select count(ref)
    from empltrainings
    where training = old.training
    into :enumber;

  enumber = coalesce(:enumber, 0);

  select costtype, personcost, totalcost
    from etrainings
    where ref = old.training
    into :costtype, :personcost, :totalcost;

  if (costtype = 0) then
    totalcost = personcost * enumber;
  else if (costtype = 1 and enumber > 0) then
    personcost = totalcost / enumber;
  else if (costtype = 1 and enumber = 0) then
    personcost = 0;

  if (costtype is not null) then
    update etrainings set personcost = :personcost, totalcost = :totalcost where ref = old.training;
end^
SET TERM ; ^
