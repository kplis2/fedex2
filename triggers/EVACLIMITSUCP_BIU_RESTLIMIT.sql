--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITSUCP_BIU_RESTLIMIT FOR EVACLIMITSUCP                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if(new.paylimit is distinct from old.paylimit
     or new.paylimitcons is distinct from old.paylimitcons
     or new.nopaylimit is distinct from old.nopaylimit
     or new.nopaylimitcons is distinct from old.nopaylimitcons ) then
  begin
    new.payrestlimit = new.paylimit - new.paylimitcons;
    new.nopayrestlimit = new.nopaylimit - new.nopaylimitcons;
  end
end^
SET TERM ; ^
