--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MAILBOXES_AI0 FOR MAILBOXES                      
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
    INSERT INTO DEFFOLD (MAILBOX, TYP,  NAZWA, SYMBOL, PIKTOGRAM, DOMYSLNY, ROLA,
       OPERREAD, OPEREDIT, OPERSEND, MAILFOLDER, maxuid, nowewiadomosci, ICON )
        VALUES ( new.ref , 'P', new.nazwa, substring(new.nazwa from 1 for 80), 13, 2, 5,
        ';'||new.operator||';',';'||new.operator||';',';'||new.operator||';','', 0, 0, 'MI_KORESPOND');
end^
SET TERM ; ^
