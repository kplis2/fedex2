--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_BU_AKCEPTACJA FOR RKDOKNAG                       
  ACTIVE BEFORE UPDATE POSITION 5 
AS
declare variable rp_kwota numeric(14,2);
begin
  --podczas akceptacji sprawdzenie, czy suma pozycji jest rowna sumie naglowka
  -- wystarczy to sprawdzac tutaj, bo zamkniecie raportu wymaga, by wszystkie elektroniczne operacje byly zaakceptowane
  if(new.status > 0 and old.status <= 0 and new.electronic = 1) then
  begin
    select sum(case when RP.pm='+' then rp.kwota else rp.kwota * (-1) end)
    from Rkdokpoz rp WHERE (RP.dokument = NEW.REF)
    into rp_kwota;
    if(new.pm = '-') then
      rp_kwota = :rp_kwota * (-1);
    if(coalesce(:rp_kwota,0) <> coalesce(new.kwota,0)) then
      exception universal 'Kwota dokumentu niezgodna z sumą z pozycji';
  end
end^
SET TERM ; ^
