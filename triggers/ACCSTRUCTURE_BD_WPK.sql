--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BD_WPK FOR ACCSTRUCTURE                   
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable ispattern smallint_id;
declare variable pattern_delete string10;
declare variable cascade_delete string10;
begin
  -- Sprawdzamy, czy nie jest zalezny
  if (coalesce(old.pattern_ref,0)=0) then
  begin
    -- Jak nie to może jest wzorcowy
    select c.pattern
      from companies c join bkaccounts b on (c.ref = b.company)
      where c.ref = b.company and b.ref = old.bkaccount
      into :ispattern;
    if (coalesce(ispattern,0)=1) then
    begin
       rdb$set_context('USER_TRANSACTION', 'PATTERN_DELETE', 'TRUE');
      delete from accstructure
        where pattern_ref = old.ref;
    end
  end else begin
    -- w takim razie jest zalezny wiec blokuje, o ile to nie delete z wzorca,
    -- lub kaskadowo z BKACCOUNTS
    cascade_delete = coalesce(rdb$get_context('USER_TRANSACTION', 'PATTERN_DELETE'),'FALSE');
    pattern_delete = coalesce(rdb$get_context('USER_TRANSACTION', 'CASCADE_DELETE'),'FALSE');

    if (cascade_delete <> 'TRUE' and pattern_delete <> 'TRUE') then
      exception wpk_delete_error_accstr;
  end
end^
SET TERM ; ^
