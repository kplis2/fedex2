--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKSTANWAL_BIU_REF FOR RKSTANWAL                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0) then
    execute procedure gen_ref('RKSTANWAL') returning_values new.ref;
end^
SET TERM ; ^
