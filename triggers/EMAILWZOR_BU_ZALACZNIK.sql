--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWZOR_BU_ZALACZNIK FOR EMAILWZOR                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.zalacznik is distinct from old.zalacznik) then
  begin
    new.zalacznik = replace(new.zalacznik,'; ',';');
  end
end^
SET TERM ; ^
