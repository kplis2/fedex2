--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOSTSPOS_AD0 FOR ECOSTSPOS                      
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable percents integer;
begin
  select sum(p.percent)
    from ecostspos p
    where p.ecost = old.ecost
    into :percents;

  if (percents = 100) then
    update ecosts set status=1 where ref=old.ecost;
  else
    update ecosts set status=0 where ref=old.ecost;
end^
SET TERM ; ^
