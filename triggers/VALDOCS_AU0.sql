--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_AU0 FOR VALDOCS                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable tvalue numeric(14,2);
begin
  if (new.tvalue <> old.tvalue) then
  begin
    tvalue = new.tvalue - old.tvalue;
    update fxdassets set tvaluegross = coalesce(tvaluegross,0) + :tvalue where ref = new.fxdasset;
  end
  if (new.fvalue <> old.fvalue) then
  begin
    tvalue = new.fvalue - old.fvalue;
    update fxdassets set fvaluegross = coalesce(fvaluegross,0) + :tvalue where ref = new.fxdasset;
  end
  if (new.tredemption <> old.tredemption) then
  begin
    tvalue = new.tredemption - old.tredemption;
    update fxdassets set tredemptiongross = coalesce(tredemptiongross,0) + :tvalue where ref = new.fxdasset;
  end
  if (new.fredemption <> old.fredemption) then
  begin
    tvalue = new.fredemption - old.fredemption;
    update fxdassets set fredemptiongross = coalesce(fredemptiongross,0) + :tvalue where ref = new.fxdasset;
  end

  -- zmiany dotyczace korekt amortyzacji naliczonej
  if ((new.tredemption <> old.tredemption or new.fredemption <> old.fredemption)
    and exists(select first 1 1 from vdoctype where amcorrection = 1 and symbol = new.doctype)
  ) then begin
    if (exists(select first 1 1 from amortization
                 where amortization.correction = new.ref)
    ) then
      update amortization
         set tamort = new.tredemption,
             famort = new.fredemption
        where correction = new.ref;
    else
      insert into amortization (fxdasset, amperiod,tamort, ttype, famort, ftype, correction, cor_amperiod)
        values (new.fxdasset, new.operiod, new.tredemption, 2, new.fredemption, 2, new.ref, new.corperiod);
  end
end^
SET TERM ; ^
