--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZNAG_AI FOR REZNAG                         
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if (new.status = 3 ) then --nowy status: zamkniety
  begin
    if (new.pmelement is not null and new.pmelement <> 0) then  -- rozliczenie nastpuje tylko gdy wybrany jest element
    begin
      if (new.pmposition is not null) then
      begin
        update pmpositions set USEDVAL = USEDVAL + new.stawka * new.dniwypoz where ref = new.pmposition;
      end
    end
  end
end^
SET TERM ; ^
