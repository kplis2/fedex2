--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_BI_NORMREALIZATION FOR PROPERSRAPS                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  select norm
    from XK_PROPERSRAPS_NORM(new.prschedoper, new.maketimeto - new.maketimefrom, new.amount)
    into new.normrealization;
end^
SET TERM ; ^
