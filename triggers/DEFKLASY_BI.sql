--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKLASY_BI FOR DEFKLASY                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
BEGIN
  execute procedure REPLICAT_STATE('DEFKLASY',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
END^
SET TERM ; ^
