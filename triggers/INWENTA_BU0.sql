--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTA_BU0 FOR INWENTA                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable chronologia integer;
declare variable lokres varchar(6);
declare variable cnt integer;

begin
  if(new.data is null) then new.data = current_date;
  select CHRONOLOGIA from DEFMAGAZ where SYMBOL = new.magazyn into :chronologia;
  if(new.maroznice is null) then new.maroznice = 0;
  if(new.calkowity is null) then new.calkowity = 0;
  if(new.symbol is null) then new.symbol = '';
  if(new.symbol = '') then exception INWENTAP_EXPT 'Brak podanego symbolu arkusza inwentaryzacyjnego.';
  if(new.rozliczeniowy is null) then new.rozliczeniowy = 0;
  if(new.zbiorczy is null) then new.zbiorczy = 0;
  if(new.zpartiami is null) then new.zpartiami = 0;
  if(new.zamk <> old.zamk) then begin
    if(new.zamk  = 0) then begin
      new.inwm = null;
      new.inwp = null;
      new.datazamk = null;
    end/*akceptacja dokument - wystawienie dokumentów w systemie na podstawie inwentaryzacji w osobnej procedurze */
    else new.datazamk = current_date;
  end
  if(:chronologia is null) then chronologia = 0;
  if(new.symbol is null) then new.symbol = '';
  /*kontrola okresu*/
  if(new.data <> old.data) then begin
    if(new.zamk  = 1) then exception INWETNA_DATA_ZMIEN;
    lokres = cast( extract( year from new.data) as char(4));
    if(extract(month from new.data) < 10) then
       lokres = :lokres ||'0' ||cast(extract(month from new.data) as char(1));
    else
       lokres = :lokres ||cast(extract(month from new.data) as char(2));
    new.okres = :lokres;
    if(:chronologia >= 1) then begin
      select count(*) from DOKUMNAG where MAGAZYN=new.magazyn and DATA >= (new.data+:chronologia) and AKCEPT = 1 into :cnt;
      if(:cnt > 0) then exception INWENTA_SA_AKCEPT_DOK_POZ;
    end
    cnt = null;
    select count(*) from INWENTAP where INWENTA = new.ref into :cnt;
    if(:cnt > 0) then exception inwenta_datazmien_sapozycje;
  end
  if(new.calkowity =1 and old.calkowity = 0) then begin
    cnt = null;
    select count(*) from INWENTAP where INWENTA = new.ref into :cnt;
    if(:cnt > 0) then exception inwenta_calkowityzmien_sapozycj;
  end
  if(new.inwentaparent is not null and old.inwentaparent is null or (new.inwentaparent <> old.inwentaparent)) then
    select SYMBOL from INWENTA where REF=new.inwentaparent into new.inwentaparents;
  else if(new.inwentaparent is null and old.inwentaparent is not null) then
    new.inwentaparents = null;
  if(new.symbol <> old.symbol) then
    update INWENTA set INWENTAPARENTS = new.symbol where INWENTAPARENT = new.ref;
  if(new.zpartiami <> old.zpartiami) then begin
    cnt = 0;
    select count(*) from INWENTAP where INWENTA = new.ref into :cnt;
    if(:cnt > 0) then exception INWENTAP_EXPT 'Arkusz posiada pozycje. Zmiana trybu rejestrowania partii niemozliwa';
  end
  if(new.zamk > old.zamk) then begin
    select count(*) from INWENTA where inwenta.inwentaparent = new.ref and zamk < new.zamk into :cnt;
    if(:cnt > 0) then
      exception INWENTA_PODRZEDNENIEZAMKNIETE;
  end
end^
SET TERM ; ^
