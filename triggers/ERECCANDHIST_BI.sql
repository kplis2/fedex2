--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECCANDHIST_BI FOR ERECCANDHIST                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_ereccandhist,1);
end^
SET TERM ; ^
