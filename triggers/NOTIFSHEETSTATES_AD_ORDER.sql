--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSHEETSTATES_AD_ORDER FOR NOTIFSHEETSTATES               
  ACTIVE AFTER DELETE POSITION 20 
as
begin
  update notifsheetstates n set n.number = n.number - 1
    where n.number > old.number;
end^
SET TERM ; ^
