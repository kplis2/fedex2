--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZESTP_BI0 FOR SPRZEDZESTP                    
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable klient varchar(255);
declare variable dostawca varchar(255);

begin
  if(new.wartnet is null) then new.wartnet = 0;
  if(new.wartbru is null) then new.wartbru = 0;
  if(new.dowyplaty is null) then new.dowyplaty = 0;
  if(new.prowizja is null) then new.prowizja = 0;
  if(new.dowyplaty is null) then new.dowyplaty = 0;
  if(new.stan is null) then new.stan = 0;
  if(new.typ is null) then new.typ = 0;
  if(new.stan <> 2)then begin
    new.dowyplaty = new.prowizja;
    if(new.dowyplaty = new.prowizja) then new.stan = 0;
    else new.stan = 1;
  end
  if(new.kontrahent is null or (new.kontrahent = '') ) then begin
    if(new.fak > 0) then begin
      select KLIENCI.fskrot, dostawcy.id
      from NAGFAK left join KLIENCI on (NAGFAK.KLIENT  = KLIENCI.REF)
        left join DOSTAWCY on (DOSTAWCY.REF = NAGFAK.DOSTAWCA)
      where NAGFAK.ref = new.fak
      into :klient, :dostawca;
    end else if(new.zam > 0) then begin
      select KLIENCI.fskrot, dostawcy.id
      from NAGZAM left join KLIENCI on (NAGZAM.KLIENT  = KLIENCI.REF)
        left join DOSTAWCY on (DOSTAWCY.REF = NAGZAM.DOSTAWCA)
      where NAGZAM.ref = new.zam
      into :klient, :dostawca;
    end
    if(:klient <> '') then
      new.kontrahent = :klient;
    else
      new.kontrahent = :dostawca;
  end
end^
SET TERM ; ^
