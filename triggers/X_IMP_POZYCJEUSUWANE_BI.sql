--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_IMP_POZYCJEUSUWANE_BI FOR X_IMP_POZYCJEUSUWANE           
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_x_imp_pozycjeusuwane_id,1);
end^
SET TERM ; ^
