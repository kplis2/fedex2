--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVEHICLETYPES_BI_REF FOR EVEHICLETYPES                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('EVEHICLETYPES')
      returning_values new.REF;
end^
SET TERM ; ^
