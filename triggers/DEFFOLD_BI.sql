--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFOLD_BI FOR DEFFOLD                        
  ACTIVE BEFORE INSERT POSITION 1 
AS
BEGIN
  execute procedure REPLICAT_STATE('DEFFOLD',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
END^
SET TERM ; ^
