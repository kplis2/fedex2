--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELPOS_BD0 FOR EDELPOS                        
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if(exists(select first 1 1 from edelegations where ref = old.delegation)) then begin
    execute procedure EDELPOS_TODAYCLEAR(old.DELEGATION);
  end
end^
SET TERM ; ^
