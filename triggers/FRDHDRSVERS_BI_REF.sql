--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDHDRSVERS_BI_REF FOR FRDHDRSVERS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDHDRSVERS')
      returning_values new.REF;
end^
SET TERM ; ^
