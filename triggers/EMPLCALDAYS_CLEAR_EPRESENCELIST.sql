--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_CLEAR_EPRESENCELIST FOR EMPLCALDAYS                    
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
 if(new.status = 1 and old.status=0) then begin
     if (exists (select first 1 * from epresencelistspos p
                 inner join epresencelists l on l.ref = p.epresencelists
                 where p.employee = new.employee
                    and l.listsdate = new.cdate and
                       (p.timestart is null or p.timestop is null) and
                       (p.newstartat is null or p.newstartat ='' or p.newstopat is null or p.newstopat = ''))
     ) then exception universal 'Nie rozpatrzono czasu do weryfikacji';         --BS56958


     --exception test_break new.cdate || '      ' || old.cdate;
     execute procedure ehrm_epresenecelist_normalize(cast(new.cdate as date),new.employee);
 end
end^
SET TERM ; ^
