--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDEFKATAL_BI_REF FOR CDEFKATAL                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CDEFKATAL')
      returning_values new.REF;
end^
SET TERM ; ^
