--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_BI_OBL_POZ FOR POZFAK                         
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable bn char(1);
declare variable cennik integer;
declare variable cena DECIMAL(14,4);
declare variable rabat DECIMAL(14,2);
declare variable eksport smallint;
declare variable kurs numeric(14,4);
declare variable walnag char(3);
declare variable tabkurs integer;
declare variable sredni numeric(14,4);
declare variable zakupu numeric(14,4);
declare variable sprzedazy numeric(14,4);
declare variable typkurs integer;
declare variable walpln char(3);
declare variable cenywal integer;
declare variable zakup smallint;
declare variable jednprzelicz numeric(14,4);
declare variable jednconst smallint;
declare variable korekta smallint;
declare variable tabkursuzywaj smallint;
declare variable bankdomyslny varchar(64);
declare variable data timestamp;
declare variable magazyn varchar(5);
declare variable oddzial varchar(30);
declare variable location integer;
declare variable aktulocation integer;
declare variable aktuoddzial varchar(30);
declare variable local integer;
declare variable fakturarr smallint;
declare variable rabatkask smallint;
declare variable dostawca integer;
declare variable ntabkurs integer;
declare variable sad smallint;
declare variable cenacenbru numeric(14,4);
declare variable cenacennet numeric(14,4);
declare variable walcen varchar(3);
declare variable cenazak numeric(14,4);
declare variable rabatnag varchar(40);
declare variable scennik varchar(255);
declare variable sprec varchar(20);
declare variable fiskal smallint;
begin
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  select nagfak.zakup, nagfak.bn, nagfak.rabat, nagfak.kurs, nagfak.eksport, nagfak.fakturarr, typfak.korekta, nagfak.dostawca, nagfak.sad, typfak.fiskalny
    from  NAGFAK
    left join typfak on(nagfak.typ = typfak.symbol)
    where nagfak.ref = new.dokument
    into :zakup, :bn, :rabat, :kurs, :eksport, :fakturarr, :korekta, :dostawca, :sad, :fiskal;
  if(new.gr_vat is null or (new.gr_vat = '')) then
  begin
    select vat from wersje where ref = new.wersjaref into new.gr_vat;
    if (new.gr_vat is null) then
      select vat from towary where ktm = new.ktm into new.gr_vat;
  end
  if(new.opk > 0) then rabat = 0;--opakowania nie maią rabatu z nagłówka faktury
  if(:bn is null) then
    select typfak.bn
    from NAGFAK
    left join TYPFAK on (typfak.symbol = nagfak.typ)
    where nagfak.ref = new.dokument into :bn;
  if(:eksport is null) then eksport = 0;
  if(:fakturarr = 1) then
    execute procedure GETCONFIG('VATRR') returning_values new.gr_vat;
  else if(:eksport = 1) then
    execute procedure GETCONFIG('VATEXPORT') returning_values new.gr_vat;
  if(new.gr_vat is not null) then
    select stawka from vat where grupa = new.gr_vat into new.vat;
  else
    new.vat = 0;

  if(new.pgr_vat is not null) then
    select stawka from vat where grupa = new.pgr_vat into new.pvat;
  else
    new.pvat = 0;

  if(:rabat is null) then rabat = 0;
  if(new.refcennik is not null) then cennik = new.refcennik;
  else begin
    select grupykli.cennik
    from nagfak
    left join klienci on(klienci.ref = nagfak.klient)
    left join grupykli on(klienci.grupa = grupykli.ref)
    where nagfak.ref = new.dokument
      into :cennik;
  end
  if(:cennik is null) then begin
    execute procedure GETCONFIG('CENNIK') returning_values :scennik;
    if (scennik <> '') then
      cennik = cast(scennik as integer);
  end
  --ustalenie magazynu dla korekty sprzedazy  - magazyn oddzialu

  if(:korekta = 1 and :zakup = 0 and :local = 1) then begin
    select ODDZIAL from DEFMAGAZ where SYMBOL=new.magazyn into :oddzial;
    select LOCATION from ODDZIALY where ODDZIAL=:Oddzial into :location;
    execute procedure GETCONFIG('AKTULOCAT') returning_values :aktulocation;
    if(:aktulocation <> :location) then begin
      execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
      select oddzialy.magazyn from ODDZIALY where ODDZIAL=:aktuoddzial into :magazyn;
      if(:magazyn is not null and magazyn <> '' and :magazyn <> '   ') then
       new.magazyn = :magazyn;
    end
  end
    --ustalenie jednostki w zależnosci od rodzaju dokumentu
    if(new.jedn is null or (new.jedn = 0)) then begin
      if(:zakup = 0) then
        select REF from TOWJEDN where KTM = new.ktm and C = 2 into new.jedn;
      else
        select ref from TOWJEDN where KTM = new.ktm and Z  = 2 into new.jedn;
      if(new.jedn is null or (new.jedn = 0))then
        select ref from TOWJEDN where KTM = new.KTM and GLOWNA = 1 into new.jedn;
    end
    if(new.jedno is null or (new.jedno = 0))then
      if(:zakup = 0) then
        select REF from TOWJEDN where KTM = new.ktm and S = 2 into new.jedno;
      else
        select ref from TOWJEDN where KTM = new.ktm and Z  = 2 into new.jedno;
    if(new.jedno is null or (new.jedno = 0))then
      new.jedno = new.jedn;
    --ustalenie ilosci od ilosci magazynowej
    select PRZELICZ, CONST from TOWJEDN where REF=new.jedn into :jednprzelicz, :jednconst;
    if(((new.ilosc is null) or (new.ilosc = 0)) and new.iloscm>0)then
      new.ilosc = new.iloscm / :jednprzelicz;
    else if(new.iloscm is null or (new.iloscm = 0) or (:jednconst = 1)) then
      new.iloscm = new.ilosc * :jednprzelicz;
    if(new.jedno = new.jedn) then begin
      new.ilosco = new.ilosc;
    end

    if(((new.pilosc is null) or (new.pilosc = 0)) and new.piloscm>0)then
      new.pilosc = new.piloscm / :jednprzelicz;
    else if(new.piloscm is null or (new.piloscm = 0) or (:jednconst = 1)) then
      new.piloscm = new.pilosc * :jednprzelicz;
    if(new.jedno = new.jedn) then begin
      new.pilosco = new.pilosc;
    end
    if(:korekta <> 1 and (new.ilosc <= 0) and (new.opk=0)) then
      exception POZZAM_NULL_ILOSC;

  -- obliczenie kosztu katalogowego
  if(new.wersjaref is not null and new.iloscm-new.piloscm<>0) then begin
    cenazak = null;
    select cena_zakn from wersje where ref=new.wersjaref into :cenazak;
    if(:cenazak is null) then cenazak = 0;
    if(:zakup=0) then
      new.kosztkat = :cenazak * (new.iloscm-new.piloscm);
    else
      new.kosztkat = -:cenazak * (new.iloscm-new.piloscm);
  end

  -- obliczenie ceny
  select cenanet, cenabru, waluta
    from cennik
    where cennik = :cennik and
      ktm = new.ktm and
      wersja = new.wersja and
      jedn = new.jedn
      into cenacennet, cenacenbru, walcen;
  if(:cenacennet is null) then
    select cenanet, cenabru, waluta
      from cennik
      where cennik = :cennik and
        ktm = new.ktm and
        wersja = 0 and
        jedn = new.jedn
        into cenacennet, cenacenbru, walcen;

  new.orgcenanet = :cenacennet;
  if(new.orgcenanet is null) then new.orgcenanet = 0;
  if(new.cenacen is null and :zakup = 0) then
  begin
    if(:bn = 'B') then
    begin
      new.cenacen = cenacenbru;
      new.walcen = walcen;
    end else
    begin
      new.cenacen = cenacennet;
      new.walcen = walcen;
    end
  end
  --
  if(new.cenacen is null) then new.cenacen=0;
  cena = new.cenacen;
  if(new.kurscen is null or new.kurscen=0) then new.kurscen = 1;
  typkurs = -1;
  -- przeliczenie walut cennika na walutę dokumenut
  select n.waluta, n.kurs, r.cenywal, n.tabkurs from NAGFAK n
  left join REJFAK r on ( N.rejestr = R.SYMBOL)
  where n.REF=new.dokument into :walnag, :kurs, :cenywal, :ntabkurs;
  if(:cenywal=1 and new.walcen <> :walnag and new.walcen is not null and new.walcen <> '') then begin
    tabkursuzywaj = 0;
    select STANSPRZED.TYPKURS, STANSPRZED.TABKURSUZYWAJ, STANSPRZED.bankdomyslny,
           NAGFAK.DATA
           from NAGFAK
    left join STANSPRZED on ( STANSPRZED.STANOWISKO = NAGFAK.stanowisko)
    where NAGFAK.REF = new.dokument into :typkurs, :tabkursuzywaj, :bankdomyslny, :data;
    if (ntabkurs is  not null) then tabkurs = ntabkurs;
    execute procedure GETCONFIG('WALPLN') returning_values :walpln;
    if(:walpln is null) then walpln = 'PLN';
    if(new.walcen <> :walpln ) then begin
      if(new.kurscen is null or new.kurscen=0 or new.kurscen=1) then begin
        if(new.tabkurscen is not null) then begin
          if(:typkurs<0) then exception KURSTYP_BRAK;
          sredni = 0; zakupu = 0; sprzedazy = 0;
          select tp.sredni, tp.zakupu, tp.sprzedazy from tabkurs t join tabkursp tp on (t.ref = tp.tabkurs)
          where t.ref = new.tabkurscen and tp.waluta = new.walcen
          into  :sredni, :zakupu, :sprzedazy;
--          if((:typkurs = 0 and :sredni = 0) or (:typkurs = 1 and :zakupu = 0) or (:typkurs = 2 and :sprzedazy = 0)) then begin
--            select data from tabkurs where ref = new.tabkurscen into :data;
--            exception KURS_BRAKWARTOSCI 'Niezdefiniowany kurs sredni. Bank: '||:bankdomyslny||' Data: '||substring(:data from 1 for 10)||' Waluta: '||new.walcen;
--          end
          if(:typkurs = 0)then new.kurscen = :sredni;
          else if(:typkurs = 1)then new.kurscen = :zakupu;
          else if(:typkurs = 2)then new.kurscen = :sprzedazy;
          if(new.kurscen=0) then new.kurscen = 1;
        end
        else if(tabkursuzywaj = 1) then begin
          if(:typkurs<0) then exception KURSTYP_BRAK;
          sredni = 0; zakupu = 0; sprzedazy = 0;
          select tp.sredni, tp.zakupu, tp.sprzedazy from tabkurs t join tabkursp tp on (t.ref = tp.tabkurs)
          where t.bank = :bankdomyslny and t.data = :data and tp.waluta = new.walcen
          into  :sredni, :zakupu, :sprzedazy;
--          if((:typkurs = 0 and :sredni = 0) or (:typkurs = 1 and :zakupu = 0) or (:typkurs = 2 and :sprzedazy = 0)) then
--            exception KURS_BRAKWARTOSCI 'Niezdefiniowany bieżący kurs. Bank:'||:bankdomyslny||' Data:'||substring(:data from 1 for 10)||' Waluta:'||new.walcen;
          if(:typkurs = 0)then new.kurscen = :sredni;
          else if(:typkurs = 1)then new.kurscen = :zakupu;
          else if(:typkurs = 2)then new.kurscen = :sprzedazy;
          if(new.kurscen=0) then new.kurscen = 1;
        end
        else begin
          select sredni, zakupu, sprzedazy from TABKURSP where TABKURS = :tabkurs and WALUTA = new.walcen into :sredni, :zakupu, :sprzedazy;
          if(:typkurs = 1) then sredni = :zakupu;
          else if(:typkurs = 2) then sredni = :sprzedazy;
          if(:sredni is null) then sredni = 1;
          new.kurscen = :sredni;
        end
      end
    end else new.kurscen = 1;
    if(:kurs is null) then kurs = 1;
    if (new.prec = 4) then
      cena = cast(:cena * new.kurscen / :kurs as numeric(14,4));
    else
      cena = cast(:cena * new.kurscen / :kurs as numeric(14,2));
  end else
    new.walcen = :walnag;
  if(:kurs is null) then kurs = 1;
  /*obliczenie rabatu */
  if(new.prec is null or new.prec = 0) then begin
    if(:zakup=1) then execute procedure GETCONFIG('ZAKPREC') returning_values :sprec;
    else execute procedure GETCONFIG('SPRPREC') returning_values :sprec;
    if(:sprec is not null and :sprec<>'') then new.prec = cast(:sprec as integer);
  end
  if(coalesce(:fiskal,0)=1) then begin
    new.prec = 2;
    cena = cast(:cena as numeric(14,2));
    if(new.ilosc <> cast(new.ilosc as numeric(14,2))) then exception POZFAK_FISK 'Dokladność ilości nie zgodna z dopuszczoną dla paragonu! Dla '||coalesce(new.ktm,'');
  end
  execute procedure GETCONFIG('RABATNAG') returning_values :rabatnag;
  if(:zakup=0 and :rabatnag='1' and new.rabattab<>0) then begin
    if(new.prec=4) then cena = cast((:cena * (100- new.rabat) /100) as DECIMAL(14,4));
    else cena = cast((:cena * (100- new.rabat) /100) as DECIMAL(14,2));
  end else begin
    if(dostawca is not null and dostawca > 0) then
      select d.rabatkask from dostawcy d where d.ref = :dostawca into :rabatkask;
    if(rabatkask is not null and rabatkask = 1) then begin
      if(new.prec=4) then cena = cast(cena * (100- :rabat)*(100 - new.rabat)/10000 as DECIMAL(14,4));
      else cena = cast(cena * (100- :rabat)*(100 - new.rabat)/10000 as DECIMAL(14,2));
    end else begin
       if (new.rabat is not null) then rabat = rabat + new.rabat;
       if(:rabat <> 0)then begin
         if(new.prec=4) then cena = cast((:cena * (100- :rabat) /100) as DECIMAL(14,4));
         else cena = cast((:cena * (100- :rabat) /100) as DECIMAL(14,2));
       end
    end
  end
  --obliczenie cen
  if((:bn = 'B') and ((:zakup=0) or (new.cenabru is null))) then begin
       new.cenabru = cena;
       new.wartbru = new.ilosc * new.cenabru;
       new.pwartbru = new.pilosc * new.pcenabru;
       new.pwartnet = cast((new.pwartbru / (1+new.pvat / 100)) as DECIMAL(14,2));
       if(:eksport = 0) then begin
         new.wartnet = cast((new.wartbru / (1+new.vat / 100)) as DECIMAL(14,2));
         new.cenanet = cast((new.cenabru / (1+(new.vat/100))) as DECIMAL(14,2));
       end else begin
         new.wartnet = new.wartbru;
         new.cenanet = new.cenabru;
       end
  end else if((:bn <> 'B') and ((:zakup=0) or (new.cenanet is null))) then begin
       new.cenanet = cena;
       new.wartnet = new.ilosc * new.cenanet;
       new.pwartnet = new.pilosc * new.pcenanet;
       new.pwartbru = new.pwartnet + cast((new.pwartnet * (new.pvat / 100)) as DECIMAL(14,2));
       if(:eksport = 0) then begin
         new.wartbru = new.wartnet + cast((new.wartnet * (new.vat / 100)) as DECIMAL(14,2));
         new.cenabru = new.cenanet + cast((new.cenanet * (new.vat / 100)) as DECIMAL(14,2));
       end else begin
         new.wartbru = new.wartnet;
         new.cenabru = new.cenanet;
       end
  end
  if(new.pwartnet is null) then new.pwartnet = 0;
  if(new.pwartbru is null) then new.pwartbru = 0;
  if(new.pilosco is null) then new.pilosco = 0;
  select TOWJEDN.waga * (new.ilosco-new.pilosco), TOWJEDN.vol * (new.ilosco-new.pilosco)
  from TOWJEDN where ref=new.jedno into new.waga, new.objetosc;
  if(new.waga is null) then new.waga = 0;
  if(new.objetosc is null) then new.objetosc = 0;
  /*MS: w przypadku SADu bierz kursy z PZI */
  if(:sad=1 and new.fromdokumpoz is not null) then begin
    select dokumnag.kurs
      from dokumpoz
      left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
      where dokumpoz.ref=new.fromdokumpoz into :kurs;
    if(:kurs is null) then kurs = 1;
  end
  if(:bn = 'B') then begin
    new.cenanetzl = new.cenanet * :kurs;
    new.pcenanetzl = new.pcenanet * :kurs;
    new.wartbruzl = new.wartbru * :kurs;
    new.pwartbruzl = new.pwartbru * :kurs;
    new.cenabruzl = new.cenabru * :kurs;
    new.pcenabruzl = new.pcenabru * :kurs;
    if(new.prec = 2) then
    begin
      new.cenanetzl = cast(new.cenanetzl as numeric(14,2));
      new.cenabruzl = cast(new.cenabruzl as numeric(14,2));
    end
    if(:eksport = 0) then begin
      new.wartnetzl = cast((new.wartbruzl / (1+new.vat / 100)) as DECIMAL(14,2));
      new.pwartnetzl = cast((new.pwartbruzl / (1+new.pvat / 100)) as DECIMAL(14,2));
    end else begin
      new.wartnetzl = new.wartbruzl;
      new.pwartnetzl = new.pwartbruzl;
    end
  end else begin
    new.cenanetzl = new.cenanet * :kurs;
    new.pcenanetzl = new.pcenanet * :kurs;
    new.wartnetzl = new.wartnet * :kurs;
    new.pwartnetzl = new.pwartnet * :kurs;
    new.cenabruzl = new.cenabru * :kurs;
    new.pcenabruzl = new.pcenabru * :kurs;
    if(new.prec = 2) then
    begin
      new.cenanetzl = cast(new.cenanetzl as numeric(14,2));
      new.cenabruzl = cast(new.cenabruzl as numeric(14,2));
    end
    if(:eksport = 0) then begin
      new.wartbruzl = new.wartnetzl + cast((new.wartnetzl * (new.vat / 100)) as DECIMAL(14,2));
      new.pwartbruzl = new.pwartnetzl + cast((new.pwartnetzl * (new.pvat / 100)) as DECIMAL(14,2));
    end else begin
      new.wartbruzl = new.wartnetzl;
      new.pwartbruzl = new.pwartnetzl;
    end
  end
  new.wartvatzl = new.wartbruzl - new.wartnetzl;
end^
SET TERM ; ^
