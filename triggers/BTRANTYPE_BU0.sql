--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANTYPE_BU0 FOR BTRANTYPE                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.withslodef is null) then new.withslodef = 0;
  if(new.slodefconst is null) then new.slodefconst = 0;
  if(new.defslodef is null) then new.defslodef = 0;
  if(new.defslodef = 0) then new.slodefconst = 0;
  if(new.rights is null) then new.rights = '';
  if(new.rightsgrup is null) then new.rightsgrup = '';
end^
SET TERM ; ^
