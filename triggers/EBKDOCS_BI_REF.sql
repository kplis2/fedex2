--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EBKDOCS_BI_REF FOR EBKDOCS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EBKDOCS') returning_values new.ref;
end^
SET TERM ; ^
