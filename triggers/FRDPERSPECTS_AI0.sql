--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSPECTS_AI0 FOR FRDPERSPECTS                   
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.timefrdimhier is not null) then
    if (exists (select ref from frdperspects where frdperspects.frhdr = new.frhdr
       and frdperspects.timefrdimhier <> new.timefrdimhier)) then
    begin
      exception FRDPERSPECTS_TIMEDIMHIER_NOTALL;
    end
    else
    begin
      update frdperspects set frdperspects.timefrdimhier = new.timefrdimhier
        where frdperspects.frhdr = new.frhdr;
      update frhdrs set frhdrs.timefrdimhier = new.timefrdimhier
        where frhdrs.symbol = new.frhdr;
    end
end^
SET TERM ; ^
