--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDOKUM_BI0 FOR DEFDOKUM                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable aa integer;
begin
  if(new.symbol is null) then new.symbol = '';
  if(new.symbol = '') then exception DEFDOKUM_SYMBOLEMPTY;
  if(new.PRZECIW <> '' and (new.przeciw is not null)) then begin
    select count(*) from DEFDOKUM where SYMBOL = new.PRZECIW into :aa;
    if(:aa is null) then aa = 0;
     if(:aa = 0) then
        EXCEPTION  DEFDOKUM_PRZECIW_FK;
  end
  if(new.koryg > 0) then new.korygujacy = '';
  if(new.korygujacy <> '' and (new.korygujacy is not null)) then begin
    aa = null;
    select count(*) from DEFDOKUM where SYMBOL = new.korygujacy into :aa;
    if(:aa is null) then aa = 0;
     if(:aa = 0) then
        EXCEPTION  DEFDOKUM_KORYGUJACY_FK;
  end
  if(new.opak > 0 and ((new.kaucjabn is null) or (new.kaucjabn = ''))) then
    new.kaucjabn = 'N';
  if(new.miedzyzaw > 0 and ((new.miedzy is null) or (new.miedzy = 0)))then
    new.miedzyzaw = 0;
  if(new.koryg is null) then new.koryg = 0;
  if(new.koryg = 0) then new.korygpow = 0;
  if(new.transport is null) then new.transport = 0;
  if(new.transport = 0) then new.transportauto = 0;
  if(new.transportauto is null) then new.transportauto = 0;
  if(new.danedod = '') then new.danedod = null;
  if(new.nieobrot is null) then new.nieobrot = 0;
  if(new.przecena is null) then new.przecena = 0;
  if(new.bn is null or new.bn=' ') then new.bn = 'N';
  if(new.dosad is null) then new.dosad = 0;
  if(new.inwent is null) then new.inwent = 0;
end^
SET TERM ; ^
