--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ANALDOSTP_BI1 FOR ANALDOSTP                      
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable wzrost numeric(14,2);
declare variable dnibad integer;
declare variable dzieldni integer;
declare variable dnisprz integer;
begin
   select WZROST,  DODATY - ODDATY, DATAKON - DATAPOCZ, STATYSTYKA  from ANALDOST where REF=new.ANALIZA
         into :wzrost, :dnisprz, :dnibad, :dzieldni;
   if(:wzrost is null) then wzrost = 0;
   if(:dzieldni is null) then dzieldni = 0;
   if(:dzieldni = 0) then dzieldni = 1;
   else if(:dzieldni = 1) then dzieldni = 7;
   else if(:dzieldni = 2) then dzieldni = 31;
   else dzieldni = 365;
   if(:dnibad is null or (:dnibad <=0)) then exception
            ANALDOSTP_CH_WR_DNIBAD;
   new.srednia = new.sprzedaz * :dzieldni  / :dnibad;
   if(new.reczny <> 3) then begin
    new.sredniaw = new.srednia *(100 + :wzrost)/100;
    new.potrzeba = new.sredniaw * :dnisprz/:dzieldni;
   end
end^
SET TERM ; ^
