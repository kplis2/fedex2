--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXESTYPES_BI_REF FOR ECONTRANNEXESTYPES             
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
 if (new.ref is null or new.ref=0) then
   execute procedure GEN_REF('ECONTRANNEXESTYPES') returning_values new.ref;
end^
SET TERM ; ^
