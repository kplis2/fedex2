--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_IMP_ODBIORCY_WAPRO_INF_AD0 FOR ODBIORCY                       
  ACTIVE AFTER DELETE POSITION 100 
AS

begin
if (coalesce(old.int_id,'')<>'') then   -- jeli dokument pochodzi z importu
  execute procedure x_imp_pozycjeusuwane_wapro(5,'ODBIORCY', old.int_id,old.ref);

end^
SET TERM ; ^
