--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZALACZNIKI_ZEW_BU FOR ZALACZNIKI_ZEW                 
  ACTIVE BEFORE UPDATE POSITION 10 
as
begin
  if (old.typ is distinct from new.typ
      and new.typ is null) then
    new.typ = 0;
end^
SET TERM ; ^
