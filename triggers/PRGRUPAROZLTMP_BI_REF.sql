--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRGRUPAROZLTMP_BI_REF FOR PRGRUPAROZLTMP                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null) then
    execute procedure GEN_REF('PRGRUPAROZLTMP') returning_values new.ref;
  new.connectionid = current_connection;
end^
SET TERM ; ^
