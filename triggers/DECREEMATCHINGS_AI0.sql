--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREEMATCHINGS_AI0 FOR DECREEMATCHINGS                
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.decreematchinggroup > 0 and new.blockcalc = 0) then
    execute procedure decreematchinggroup_calc(new.decreematchinggroup);
end^
SET TERM ; ^
