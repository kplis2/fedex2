--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_AU_STAN FOR PRSCHEDGUIDES                  
  INACTIVE AFTER UPDATE POSITION 1 
as
declare variable status integer;
declare variable oldilosc numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable termdost timestamp;
declare variable KKTM varchar(40);
declare variable KWERSJA integer;
declare variable guidepos integer;

begin
/*  if (new.outonpozzamenabled = 0) then
    select NAGZAM.kktm, nagzam.kwersja from NAGZAM where REF=new.zamowienie into :kktm, :kwersja;
  else if (new.outonpozzamenabled > 0) then
  begin
    select ktm, wersja from pozzam where ref = new.pozzam into :kktm, :kwersja;
    if (kktm is null) then
      kktm = new.ktm;
    if (kwersja is null) then
      kwersja = new.wersja;
  end
  if(new.stan <> old.stan)then begin
      update PRSCHEDGUIDESPOS set STAN = new.stan where (STAN <> new.stan or STAN is null)
        and (STAN = old.stan or STAN is null) and PRSCHEDGUIDE = new.ref;
  end
  if(
       (new.starttime <> old.starttime or (new.starttime is not null and old.starttime is null))
        and new.stan <> 'N'
  ) then begin
    for select REF from PRSCHEDGUIDESPOS where PRSCHEDGUIDESPOS.prschedguide = new.ref into :guidepos
    do begin
      update STANYREZ set DATA = new.starttime where ZAMOWIENIE = new.zamowienie and POZZAM = :guidepos;
    end
  end
  if(
       (new.endtime <> old.endtime or (new.endtime is not null and old.endtime is null))
        and new.stan <> 'N'
  ) then begin
    update STANYREZ set DATA = new.endtime where ZAMOWIENIE = new.zamowienie and pozzam = -new.ref;
  end
--obsluga zamkniecia wydania
  if(new.statusmat >=3 and old.statusmat < 3) then begin
    update PRSCHEDGUIDESPOS set STAN = 'N' where prschedguide = new.ref;
  end else if(new.statusmat < 3 and old.statusmat > 3) then begin
    update PRSCHEDGUIDESPOS set STAN = new.stan where prschedguide = new.ref;
  end
-- obsluga rezerwacji naglowka
  termdost = new.endtime;
  if(:termdost is null) then
    select TERMDOST from NAGZAM where ref=new.zamowienie into :termdost;
  if(:termdost is null) then termdost = current_date;
  if (old.warehouse <> new.warehouse) then --Sprawdza czy zosta zmieniony magazyn na zamówieniu jak tak to przenosi rezerwacje
  begin
      ilosc = new.amount;
      execute procedure REZ_SET_KTM(old.zamowienie, -old.ref, :kktm, :kwersja, 0, 0, 0, 'N',:termdost)
        returning_values :status;
      if(:status <> 1) then exception REZ_WRONG;
      else
      begin
        execute procedure REZ_SET_KTM(new.zamowienie, -new.ref, :kktm, :kwersja, :ilosc, 0, 0, new.kstan,:termdost)
          returning_values :status;
        if (:status <> 1) then exception REZ_WRONG;
      end
  end
  if(( (new.kstan = 'N' or (new.kstan <> old.kstan)) and old.kstan <> 'N' and old.kstan is not null))
  then begin
           -- zdjecie blokad/rezerwacji na starych zasadach
--          if(new.ilonklon > 0) then exception POZZAM_CHANGE_NOTALLOW;
           ilosc = old.amount;
           execute procedure REZ_SET_KTM(old.zamowienie,-old.ref, :kktm, :kwersja, 0, 0, 0, 'N',:termdost) returning_values :status;
           if(:status <> 1) then exception REZ_WRONG;
           ilosc = new.amount;
           if(:ilosc > 0) then begin
             execute procedure REZ_SET_KTM(new.zamowienie, -new.ref, :kktm, :kwersja,:ilosc, 0, 0, new.kstan, :termdost) returning_values :status;
             if(:status <> 1) then exception REZ_WRONG;
           end
  end else if(
           (new.kstan <> 'N' and (old.kstan = 'N' or (old.kstan <> new.kstan))and old.kstan is not null)
            or (new.amount <> old.amount)
  )then begin
           oldilosc = old.amount;
           ilosc = new.amount;
           if(((:ilosc <>:oldilosc) or (new.kstan <> old.kstan))) --and new.blokadarez = 0
           then begin
             execute procedure REZ_SET_KTM(new.zamowienie, -new.ref, :kktm, :kwersja,:ilosc, 0, 0, new.kstan, :termdost) returning_values :status;
             if(:status <> 1) then exception REZ_WRONG;
           end
  end*/
end^
SET TERM ; ^
