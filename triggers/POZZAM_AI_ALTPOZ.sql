--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AI_ALTPOZ FOR POZZAM                         
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable docsetsautoadd smallint;
declare variable kplnagref integer;
begin
  execute procedure getconfig('DOCSETSAUTOADD')
    returning_values :docsetsautoadd;
  if (:docsetsautoadd is null) then docsetsautoadd = 0;

  if (coalesce(new.fake,0) = 0 and new.orgpozzam is null and new.popref is null and :docsetsautoadd > 0 and
      exists(select first 1 1 from towary where ktm = new.ktm and usluga <> 1 and coalesce(altposmode,0) > 0)) then
  begin
    select first 1 ref from kplnag
      where ktm = new.ktm and wersjaref = new.wersjaref and akt = 1 and glowna = 1
    into :kplnagref;

    if (:kplnagref is not null) then
    begin
      insert into pozzam(zamowienie, numer, ord, ktm, wersjaref, ilosc, alttopoz, fake, magazyn, mag2,
          cenacen, cenanet, cenabru, cenanetzl, cenabruzl)
        select new.zamowienie, new.numer, 1, p.ktm, p.wersjaref, p.ilosc * new.ilosc, new.ref, 1, new.magazyn, new.mag2,
            0, 0, 0, 0, 0
          from kplpoz p
          where p.nagkpl = :kplnagref;
    end
  end

  if (coalesce(new.fake,0) > 0) then
  begin
    update pozzam
      set havefake = 1
      where ref = new.alttopoz;
  end
end^
SET TERM ; ^
