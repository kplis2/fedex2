--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTWORKTYPES_BI_REF FOR EDICTWORKTYPES                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDICTWORKTYPES')
      returning_values new.REF;
end^
SET TERM ; ^
