--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTRASTATP_BD FOR INTRASTATP                     
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable varsumvalue numeric(14,4);
  declare variable varsumvaluestat numeric(14,4);
  declare variable varposcount integer;
  declare variable intrastathstatus smallint;
begin
  select coalesce(ith.sumval,0), coalesce(ith.sumvalstat,0), coalesce(ith.poscount,0), coalesce(ith.statusflag,0)
    from intrastath ith
    where old.intrastath = ith.ref
  into  :varsumvalue, :varsumvaluestat, :varposcount, :intrastathstatus;

  if(:intrastathstatus <> 0)then
    exception intrastat_docum_notopened;
  else begin
    varsumvalue = :varsumvalue - old.val;
    varsumvaluestat = :varsumvaluestat - old.valstat;
    varposcount = :varposcount - 1;

    update intrastath ith set
      ith.sumval = :varsumvalue,
      ith.sumvalstat = :varsumvaluestat,
      ith.poscount = :varposcount
    where ith.ref = old.intrastath;
  end
end^
SET TERM ; ^
