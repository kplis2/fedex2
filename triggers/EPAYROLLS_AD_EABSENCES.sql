--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_AD_EABSENCES FOR EPAYROLLS                      
  ACTIVE AFTER DELETE POSITION 0 
as
begin
--MWr: aktualizuj na nieobecnosci info o jej rozliczeniu na usuwanej liscie plac

  update eabsences
    set epayroll = null
    where epayroll = old.ref;
end^
SET TERM ; ^
