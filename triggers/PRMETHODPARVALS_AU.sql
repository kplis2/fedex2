--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMETHODPARVALS_AU FOR PRMETHODPARVALS                
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable pvalue numeric(14,2);
begin
  if (old.pvalue <> new.pvalue and exists (select first 1 1 from prmethodparvals p where p.method = new.method and p.prmethodpar = new.prmethodpar)
    and new.fromdate <= current_date
  ) then
    exception prcalc_error 'Parametr jest juz w aktualnym okresie, zmiany zabronione';

  if (old.fromdate <> new.fromdate and (old.fromdate < current_date or new.fromdate < current_date)) then
  begin
    select first 1 p.pvalue
      from prmethodparvals p
      where p.method = new.method
        and p.prmethodpar = new.prmethodpar
        and p.fromdate <= current_date
      order by p.fromdate desc
      into :pvalue;

    update prcalcpars p set p.pvalue = :pvalue
      where p.symbol = new.prmethodpar and p.partype = 'G'
        and exists (select first 1 1 from  prshcalcs c
          where c.ref = p.calc and c.method = new.method);
  end
end^
SET TERM ; ^
