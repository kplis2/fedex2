--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_BIU_COMPANY FOR DOKPLIK                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
AS
begin
  if(new.company is null)then
    begin
      new.company = (select PVALUE from get_global_param('CURRENTCOMPANY'));
    end
end^
SET TERM ; ^
