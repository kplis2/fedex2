--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHW_AU0 FOR DEFCECHW                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable automat smallint;
declare variable katalog integer;
declare variable numer integer;
declare variable id_ref integer;
declare variable cnt integer;
declare variable i integer;
declare variable typ smallint;
declare variable wartosc varchar(255);
declare variable owartosc varchar(255);
begin
 if((new.wartstr <> old.wartstr) or (new.wartstr is not null and old.wartstr is null) or (new.wartstr is null and old.wartstr is not null) or
    (new.wartnum <> old.wartnum) or (new.wartnum is not null and old.wartnum is null) or (new.wartnum is null and old.wartnum is not null) or
    (new.wartdate <> old.wartdate) or (new.wartdate is not null and old.wartdate is null) or (new.wartdate is null and old.wartdate is not null) or
    (new.prefiks <> old.prefiks) or (new.prefiks is not null and old.prefiks is null) or (new.prefiks is null and old.prefiks is not null) or
    (coalesce(new.opis,'') <> coalesce(old.opis,''))
 ) then begin
   select count(*) from DEFCECHW where CECHA = new.cecha and PREFIKS = new.prefiks and
     ( (WARTSTR = new.wartstr) or (WARTNUM = new.wartnum ) or (new.wartdate = new.wartdate)) and
     ID_DEFCECHW <> new.ID_DEFCECHW into :cnt;
   if(:cnt > 0) then
     exception DEFCECHW_VALUEPRESENT;
   for select AUTOMAT, REF from DEFKATAL where CECHA = new.cecha into :automat, :katalog
   do begin
     if(:automat is null ) then automat = 0;
     wartosc = null;
     if(:automat = 1) then begin
       numer = null;
       i = 1;
       /* okreslenie numeru wartosci */
       /*okreslenie typu wartosci */
       select  typ from DEFCECHY where symbol=new.cecha into :typ;
       if(:typ = 1 or (:typ = 0)) then begin
         for select id_defcechw from DEFCECHW  where CECHA = new.cecha order by WARTNUM into :id_ref do begin
           if(:id_ref = new.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = new.wartnum;
         owartosc = old.wartnum;
       end
       if(:typ = 2) then begin
         for select id_defcechw from DEFCECHW  where CECHA = new.cecha order by WARTSTR into :id_ref do begin
           if(:id_ref = new.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = new.wartstr;
         owartosc = old.wartstr;
       end
       if(:typ = 3 or (:typ = 4)) then begin
         for select id_defcechw from DEFCECHW  where CECHA = new.cecha order by WARTDATE into :id_ref do begin
           if(:id_ref = new.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = new.wartdate;
         owartosc = old.wartdate;
       end
       if(:numer is null)then numer = 1;
       /* numer wartosci okrelony */
/*       select count(*) from DEFELEM where KATALOG=:katalog AND WARTOSC = :owartosc into :cnt;
       if(:cnt is null) then cnt = 0;
       if(:cnt = >) then*/
       /* procedura ma tylko zmienić elementy, które istnieją, nie dodwać powótrnie tego, co ktos rcznie usuna  */
       update DEFELEM set NAZWA = new.opis, WARTOSC = :wartosc  where KATALOG = :katalog AND WARTOSC = :owartosc;
/*         insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC)
           values(:katalog, :numer, :wartosc, :wartosc);
       else begin
       end*/
    end
   end
 end
end^
SET TERM ; ^
