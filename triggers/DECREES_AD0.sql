--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_AD0 FOR DECREES                        
  ACTIVE AFTER DELETE POSITION 0 
as
  declare variable bktype smallint;
  declare variable balsum numeric(14,2);
begin
  select b.bktype from bkaccounts b where b.ref = old.accref into :bktype;
  if (bktype < 2) then
  begin
    select baldebit - balcredit from bkdocs b where b.ref = old.bkdoc into :balsum;
    balsum = balsum - old.debit + old.credit;
    update bkdocs b
      set sumdebit = sumdebit - old.debit,
          sumcredit = sumcredit - old.credit,
          baldebit = case when :balsum > 0 then :balsum else 0 end,
          balcredit = case when :balsum < 0 then -:balsum else 0 end
      where b.ref = old.bkdoc;
  end
end^
SET TERM ; ^
