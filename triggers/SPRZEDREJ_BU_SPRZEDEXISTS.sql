--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDREJ_BU_SPRZEDEXISTS FOR SPRZEDREJ                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.rejestr <> old.rejestr or new.magazyn <> old.magazyn) then begin
    if (exists (select S.sprzedawca from sprzedrej S where S.rejestr = new.rejestr and S.klient = new.klient and S.sprzedawca <> new.sprzedawca)) then
      exception SPRZED_EXISTST 'W wybranym rejestrze jest już zdefiniowany sprzedawca.';
    if (exists (select S.sprzedawca from sprzedrej S where S.magazyn = new.magazyn and S.klient = new.klient and S.sprzedawca <> new.sprzedawca)) then
      exception SPRZED_EXISTST 'W wybranym magazynie jest już zdefiniowany sprzedawca.';
  end
end^
SET TERM ; ^
