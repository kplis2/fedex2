--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMACTIVS_BU0 FOR PMACTIVS                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.pmposition<>old.pmposition and new.pmposition is not null) then
    select PMPLAN, PMELEMENT from PMPOSITIONS
      where REF=new.pmposition into new.pmplan, new.pmelement;
end^
SET TERM ; ^
