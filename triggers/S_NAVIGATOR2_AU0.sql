--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_NAVIGATOR2_AU0 FOR S_NAVIGATOR2                   
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable SECTION varchar(255);
begin
   if(coalesce(old.parent, NULL) is not NULL) then
    begin
   -- ustawiamy number dla dziecka
     section = 'NavigatorGroup:'||old.parent||'_'||old.name;
     execute procedure SYS_SET_APPINI('SENTE', :section, 'NUMBER2', new.NUMBER);
     execute procedure SYS_SET_APPINI('SENTE', :section, 'Signature', new.SIGNATURE);
    end
   else
    begin
   --ustawiamy number dla rodzica
     section = 'NavigatorTab:'||old.name;
     execute procedure SYS_SET_APPINI('SENTE', :section, 'NUMBER2', new.NUMBER);
     execute procedure SYS_SET_APPINI('SENTE', :section, 'Signature', new.SIGNATURE);

    end
end^
SET TERM ; ^
