--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_IMP_DOKUMPOZ_WAPRO_INFO_AD0 FOR DOKUMPOZ                       
  INACTIVE AFTER DELETE POSITION 100 
AS
begin
if (coalesce(old.int_id,'')<>'') then   -- jeli dokument pochodzi z importu
  execute procedure x_imp_pozycjeusuwane_wapro(8, 'DOKUMPOZ', old.int_id,old.ref);

end^
SET TERM ; ^
