--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_BI_ORDER FOR WHSECROWS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from whsecrows  where whsec = new.whsec
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update whsecrows set ord = 0, number = number + 1
      where number >= new.number and whsec = new.whsec;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
