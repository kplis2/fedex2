--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFOLD_AU0 FOR DEFFOLD                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
--propagacja praw na pod-foldery
  if(coalesce(new.operread,'') <> coalesce(old.operread,'')) then
    update DEFFOLD set OPERREAD = new.OPERREAD where deffold.rodzic = new.ref and coalesce(OPERREAD,'')=coalesce(old.operread,'');
  if(coalesce(new.operedit,'') <> coalesce(old.operedit,'')) then
    update DEFFOLD set OPEREDIT = new.OPEREDIT where deffold.rodzic = new.ref and coalesce(OPEREDIT,'')=coalesce(old.operedit,'');
  if(coalesce(new.opersend,'') <> coalesce(old.opersend,'')) then
    update DEFFOLD set opersend = new.opersend where deffold.rodzic = new.ref and coalesce(opersend,'')=coalesce(old.opersend,'');
  if(coalesce(new.typ,'') <> coalesce(old.typ,'')) then
    update DEFFOLD set typ = new.typ where deffold.rodzic = new.ref and coalesce(typ,'')=coalesce(old.typ,'');

    if (coalesce(new.mailfolder,'') <> coalesce(old.mailfolder,'')) then begin
        update DEFFOLD d set
            d.mailfolder = replace(d.mailfolder,  old.mailfolder,  new.mailfolder)
        where d.mailbox = new.mailbox and d.rodzic = new.ref;
    end
end^
SET TERM ; ^
