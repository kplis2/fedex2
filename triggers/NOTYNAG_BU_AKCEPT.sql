--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYNAG_BU_AKCEPT FOR NOTYNAG                        
  ACTIVE BEFORE UPDATE POSITION 1 
as
  declare variable numberg varchar(40);
  declare variable blockref integer;
begin
  if (new.status = 1 and old.status = 0) then
  begin
    if (new.notakind = 0) then
      execute procedure GET_CONFIG('WEZWANIANUM', 2) returning_values numberg;
    else if (new.notakind = 1) then
      execute procedure GET_CONFIG('NOTYODSNUM', 2) returning_values numberg;
    else if (new.notakind = 2) then
      execute procedure GETCONFIG('POTWIERDZENIANUM') returning_values :numberg;

    execute procedure get_number(numberg, 'NOTA', 'NOTA', new.data, 0, new.ref)
      returning_values new.number, new.symbol;
  end else if (new.status = 0 and old.status = 1) then
  begin
    if (new.notakind = 0) then
      execute procedure GET_CONFIG('WEZWANIANUM', 2) returning_values numberg;
    else if (new.notakind = 1) then
      execute procedure GET_CONFIG('NOTYODSNUM', 2) returning_values numberg;
    else if (new.notakind = 2) then
      execute procedure GETCONFIG('POTWIERDZENIANUM') returning_values :numberg;

    execute procedure free_number(numberg, 'NOTA', 'NOTA', old.data, old.number, 0)
      returning_values blockref;
    new.symbol = '';
    new.number = null;
  end
end^
SET TERM ; ^
