--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TABKURSP_BU0 FOR TABKURSP                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.zakupu is null) then new.zakupu = 0;
  if(new.sprzedazy is null) then new.sprzedazy = 0;
  if(new.zakupu <>old.zakupu or new.sprzedazy <> old.sprzedazy) then
    new.sredni = (new.zakupu + new.sprzedazy)/2;
end^
SET TERM ; ^
