--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU_PMG FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable pf_ref integer;
declare variable pf_ktm varchar(40);
declare variable pf_nazwat varchar(255);
declare variable pfu_ref integer;
declare variable pfu_pmplan integer;
declare variable pfu_pmelement integer;
declare variable pfu_pmposition integer;
declare variable pfu_kwota numeric(14,4);
declare variable cnt integer;
declare variable zakup smallint;
begin
select zakup from typfak tf where tf.symbol = new.typ into :zakup;
if (zakup = 1) then
 begin          /* faktura zakupu */
  if (new.akceptacja = 1 and new.akceptacja <> old.akceptacja) then
  begin
    for select pf.ref, pf.ktm, pf.nazwat
        from pozfak pf
        where pf.dokument = new.ref
        into :pf_ref, :pf_ktm, :pf_nazwat
    do begin
      for select pfu.ref, pfu.pmelement, pfu.pmposition, pfu.kwota
          from pozfakusl pfu
          where pfu.refpozfak = :pf_ref and pfu.pmelement is not null and tryb = 2
          into :pfu_ref, :pfu_pmelement, :pfu_pmposition, :pfu_kwota
      do begin
         if (pfu_pmposition is null or pfu_pmposition = 0) then
         begin
           select count(ref) from pmpositions where pmelement = :pfu_pmelement and ktm = :pf_ktm and mainref=ref into :cnt;
           if (:cnt > 1) then
             exception universal 'Wybrany element nie posiadaj jednoznaczej pozycji na której można rozliczyć pozycje ' || :pf_ktm;
           else
           begin
             pfu_pmposition=null;
             select ref from pmpositions where pmelement = :pfu_pmelement and ktm = :pf_ktm and mainref=ref into :pfu_pmposition; --szukamy naszego ktmu na pozycjach planu
           end
         end

         if (pfu_pmposition is null or pfu_pmposition = 0) then
         begin
           execute procedure GEN_REF('PMPOSITIONS') returning_values :pfu_pmposition;
           insert into pmpositions (REF, PMELEMENT, PMPLAN, NAME, SYMBOL, KTM, POSITIONTYPE, USEDVAL)    -- gdy nie ma pozycji wspólnej to ja zakladamy
            values (:pfu_pmposition, :pfu_pmelement, :pfu_pmplan, :pf_nazwat, :pf_ktm, :pf_ktm, 'U', :pfu_kwota);
           update pozfakusl set pmposition = :pfu_pmposition, rozliczono = 1  where ref = :pfu_ref;
         end
         else
         begin
           update pmpositions set USEDVAL = USEDVAL + :pfu_kwota where ref = :pfu_pmposition;  --gdy znajdziemy dokladnie taki sam ktm, naliczamy wartosci i ilosc, cena srednia zostaje przeliczona na trig PMPOSITION_BU_PRZELICZ
           update pozfakusl set pmposition = :pfu_pmposition, rozliczono = 1 where ref = :pfu_ref;
         end
      end
    end
  end
  if (old.akceptacja = 1 and old.akceptacja <> new.akceptacja) then
      for select pf.ref, pf.ktm, pf.nazwat
        from pozfak pf
        where pf.dokument = new.ref
        into :pf_ref, :pf_ktm, :pf_nazwat
    do begin
      for select pfu.ref, pfu.pmelement, pfu.pmposition, pfu.kwota
          from pozfakusl pfu
          where pfu.refpozfak = :pf_ref and pfu.pmelement is not null and tryb = 2
          into :pfu_ref, :pfu_pmelement, :pfu_pmposition, :pfu_kwota
        do begin
          if (pfu_pmposition is not null and pfu_pmposition <> 0) then
          begin
            update pmpositions set USEDVAL = USEDVAL - :pfu_kwota where ref = :pfu_pmposition;
            update pozfakusl set rozliczono = 0 where ref = :pfu_ref;
          end
        end
      end
 end
 else   /* faktura sprzedazy */
 begin
   if (new.akceptacja = 1 and new.akceptacja <> old.akceptacja) then
   begin
    for select pf.ref, pf.ktm, pf.nazwat
        from pozfak pf
        where pf.dokument = new.ref
        into :pf_ref, :pf_ktm, :pf_nazwat
    do begin
      for select pfu.ref, pfu.pmelement, pfu.pmposition, pfu.kwota
          from pozfakusl pfu
          where pfu.refpozfak = :pf_ref and pfu.pmelement is not null and tryb = 2
          into :pfu_ref, :pfu_pmelement, :pfu_pmposition, :pfu_kwota
      do begin
       if (pfu_pmelement is not null and pfu_pmelement <> 0) then
         begin
           update pmelements set erealsval = erealsval + :pfu_kwota where ref = :pfu_pmelement;
           update pozfakusl set pmposition = :pfu_pmposition, rozliczono = 1 where ref = :pfu_ref;
         end
      end
    end
  end
  else
    if (old.akceptacja = 1 and old.akceptacja <> new.akceptacja) then
      for select pf.ref, pf.ktm, pf.nazwat
        from pozfak pf
        where pf.dokument = new.ref
        into :pf_ref, :pf_ktm, :pf_nazwat
    do begin
      for select pfu.ref, pfu.pmelement, pfu.pmposition, pfu.kwota
          from pozfakusl pfu
          where pfu.refpozfak = :pf_ref and pfu.pmelement is not null and tryb = 2
          into :pfu_ref, :pfu_pmelement, :pfu_pmposition, :pfu_kwota
        do begin
          if (pfu_pmelement is not null and pfu_pmelement <> 0) then
          begin
            update pmelements set erealsval = erealsval - :pfu_kwota where ref = :pfu_pmelement;
            update pozfakusl set rozliczono = 0 where ref = :pfu_ref;
          end
        end
      end
 end
end^
SET TERM ; ^
