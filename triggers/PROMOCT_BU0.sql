--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCT_BU0 FOR PROMOCT                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.grupatow is null) then new.grupatow = '';
  if(new.ktm is null) then new.ktm = '';
  if(new.ktm='') then new.wersja = 0;
  if(new.wersjaref <> old.wersjaref and new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji from WERSJE where ref=new.wersjaref into new.wersja;
  else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)) then begin
    new.wersjaref = null;
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  end
end^
SET TERM ; ^
