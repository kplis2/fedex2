--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_BU_ORDER FOR PRSCHEDZAM                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable maxnum integer;
begin
  if(new.status < 2) then
  begin
    if (new.ord is null) then new.ord = 1;
    if (new.number is null) then new.number = old.number;
    if (new.ord = 0 or new.number = old.number) then
    begin
      new.ord = 1;
      exit;
    end
    --numerowanie obszarow w rzedzie gdy nie podamy numeru
    if (new.number <> old.number) then
      select max(number) from prschedzam where prschedule = new.prschedule
        into :maxnum;
    else
      maxnum = 0;
    if (new.number < old.number) then
    begin
      update prschedzam set ord = 0, number = number + 1
        where ref <> new.ref and number >= new.number
          and number < old.number and prschedule = new.prschedule;
    end else if (new.number > old.number and new.number <= maxnum) then
    begin
      update prschedzam set ord = 0, number = number - 1
        where ref <> new.ref and number <= new.number
          and number > old.number and prschedule = new.prschedule;
    end else if (new.number > old.number and new.number > maxnum) then
      new.number = old.number;
  end
  else new.number = null;
end^
SET TERM ; ^
