--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCOUNTING_AU_TURNOVERS_COMPANY FOR ACCOUNTING                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.company <> old.company) then
    update turnovers set company = new.company where accounting = new.ref;
end^
SET TERM ; ^
