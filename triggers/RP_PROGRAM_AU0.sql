--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_PROGRAM_AU0 FOR RP_PROGRAM                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if(new.repl_lasttry > old.repl_lasttry) then
  begin
    insert into rp_program_history(location, NAZWA, LASTTIME, FROMLOCAT, TOLOCAT, ERRORTYPE)
      values (new.location, new.nazwa, new.repl_lasttry, new.last_fromlocat, new.last_tolocat, new.last_errortype);
  end
end^
SET TERM ; ^
