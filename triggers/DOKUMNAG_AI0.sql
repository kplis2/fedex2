--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AI0 FOR DOKUMNAG                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable NIEOBROT smallint;
begin
  if(new.AKCEPT = 1) then begin
    execute procedure DOKUMNAG_ACK(new.ref,new.AKCEPT);
    if(new.zamowienie > 0) then
        execute procedure MAG_CHECK_ZAM_ACK(new.zamowienie);
  end
  /* podłaczenie się do faktury*/
    if(new.faktura is not null) then
       execute procedure NAGFAK_UPDATE_MAGCONNECT(new.faktura);
  if(new.zamowienie > 0) then
    execute procedure NAGZAM_HASDOKMAGBEZFAK(new.zamowienie);
  if(new.numblock > 0) then
    update NUMBERBLOCK set DOKDOKUMNAG = new.ref where ref=new.numblock;
  if(new.grupasped > 0) then
    execute procedure dokumnag_grupased_koszt(new.grupasped);

  if(new.zrodlo = 0) then begin -- domyslne terminy platnosci z kontrahenta
    if (new.dostawca is not null) then
      INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
        select 'M', new.ref, new.data +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 6 and k.slopoz = new.dostawca;
    else if (new.klient is not null) then
      INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
        select 'M', new.ref, new.data +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 1 and k.slopoz = new.klient;
  end

end^
SET TERM ; ^
