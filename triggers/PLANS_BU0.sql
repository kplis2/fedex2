--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLANS_BU0 FOR PLANS                          
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.ref is null or (new.ref = 0)) then
    execute procedure GEN_REF('PLANS') returning_values (new.ref);
  if (new.keyamount is null or (new.keyamount <0 or (new.keyamount > 4))) then
    exception universal 'Min. ilosc kluczy to 0 max. 4';
end^
SET TERM ; ^
