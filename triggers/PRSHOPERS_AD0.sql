--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_AD0 FOR PRSHOPERS                      
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  -- po skasowaniu operacji domyslnej dla alternatywy nalezy ustawic null na alternatywie
  update prshopers o set o.operdefault = null
    where o.sheet = old.sheet and o.operdefault = old.ref;
end^
SET TERM ; ^
