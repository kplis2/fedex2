--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER UZYKLI_AU_SYNCHRO FOR UZYKLI                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable procedura varchar(1024);
declare variable synchro smallint;
declare variable d integer;
declare variable d2 integer;
declare variable cpodmiot integer;
declare variable slodefkli integer;
begin
  execute procedure getconfig('SYNCHROUZYKLI') returning_values :synchro;
  if (synchro = 1) then
  begin
    if (new.synchro is null and old.synchro is null and new.pkosoba is not null) then
    begin
      procedura = 'update pkosoby set';
      d = coalesce(char_length(:procedura),0); -- [DG] XXX ZG119346
      if (new.nazwa <> old.nazwa)
        then procedura = :procedura ||' NAZWA = '''||new.nazwa||''',';
      if (new.imie <> old.imie)
       then procedura = :procedura ||' IMIE = '''||new.imie||''',';
      if (new.nazwisko <> old.nazwisko)
        then procedura = :procedura ||' NAZWISKO = '''||new.nazwisko||''',';
      if (new.dmiasto <> old.dmiasto)
        then procedura = :procedura ||' MIASTO = '''||new.dmiasto||''',';
      if (new.dkodp <> old.dkodp)
        then procedura = :procedura ||' KODP = '''||new.dkodp||''',';
      if (new.dulica <> old.dulica)
        then procedura = :procedura ||' ULICA = '''||new.dulica||''',';
      if (new.telkom <> old.telkom)
        then procedura = :procedura ||' KOMORKA = '''||new.telkom||''',';
      if (new.telefon <> old.telefon)
        then procedura = :procedura ||' TELEFON = '''||new.telefon||''',';
      if (new.fax <> old.fax)
        then procedura = :procedura ||' FAX = '''||new.fax||''',';
      if (new.email <> old.email)
        then procedura = :procedura ||' EMAIL = '''||new.email||''',';
      if (new.dpoczta <> old.dpoczta)
        then procedura = :procedura ||' POCZTA = '''||new.dpoczta||''',';
      if (new.uwagi<>old.uwagi)
        then procedura = :procedura ||' OPIS = '''||new.uwagi||''',';
      if (new.pesel<>old.pesel)
        then procedura = :procedura ||' PESEL = '''||new.pesel||''',';
      if (new.aktywny<>old.aktywny)
        then procedura = :procedura ||' AKTYWNY = '''||new.aktywny||''',';
      if (new.windykacja<>old.windykacja)
        then procedura = :procedura ||' WINDYKACJA = '''||new.windykacja||''',';
      if (new.klient<>old.klient) then
      begin
        select ref from slodef where typ='KLIENCI' into :slodefkli;
        select cp.ref from cpodmioty cp where cp.slopoz=new.klient and cp.slodef=:slodefkli
          into :cpodmiot;
        procedura = :procedura ||' CPODMIOT = '''||:cpodmiot||''',';
      end
      d2 = coalesce(char_length(:procedura),0); -- [DG] XXX ZG119346
      if (d2 > d) then
      begin
        procedura = :procedura ||' SYNCHRO = 1 where pkosoby.ref = '
          ||cast(new.pkosoba as varchar (60));
        execute statement procedura;
      end
    end
    if (new.synchro = 1) then begin
      update uzykli set uzykli.synchro = null where uzykli.ref = new.ref;
    end
  end
end^
SET TERM ; ^
