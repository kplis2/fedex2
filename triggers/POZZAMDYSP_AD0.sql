--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMDYSP_AD0 FOR POZZAMDYSP                     
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable ilosc numeric(14,4);
begin
  /* naliczenie ilosci zadysponowanych */
  select sum(ILOSC) from POZZAMDYSP where POZZAM = old.POZZAM into :ilosc;
  if(:ilosc is null) then ilosc = 0;
  update POZZAM set ILDYSP = :ilosc where REF=old.POZZAM and ildysp <> :ilosc;
end^
SET TERM ; ^
