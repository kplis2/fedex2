--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMEASUREACTS_BD_ORDER FOR PRSHMEASUREACTS                
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.ord = 0) then exit;
  update PRSHMEASUREACTS set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and prshoper = old.prshoper;
end^
SET TERM ; ^
