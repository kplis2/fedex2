--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIKTYP_BI0 FOR DOKPLIKTYP                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(coalesce(new.rights,'') = '') then new.rights = ';';
  if(coalesce(new.rightsgroup,'') = '') then new.rightsgroup = ';';
end^
SET TERM ; ^
