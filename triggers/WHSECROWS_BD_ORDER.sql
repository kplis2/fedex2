--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_BD_ORDER FOR WHSECROWS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0 or old.ord = 5) then
    exit;
  select max(number) from whsecrows where whsec = old.whsec
    into :maxnum;
  if (old.number = :maxnum) then
    exit;
  update whsecrows set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and whsec = old.whsec;
end^
SET TERM ; ^
