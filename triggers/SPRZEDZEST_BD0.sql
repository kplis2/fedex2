--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZEST_BD0 FOR SPRZEDZEST                     
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable slodef integer;
declare variable cnt integer;
declare variable skrot varchar(20);
begin
  if(old.status <> 'O') then exception SPRZEDZEST_USUNAKCEPT;
  delete from SPRZEDZESTP where ZESTAWIENIE=old.ref;
  if(old.status <> 'O') then begin
      select ref from SLODEF where TYP = 'SPRZEDAWCY' into :slodef;
      if(:slodef is null) then exception SLODEF_BEZSPRZEDAWCY;
      select SYMBFAK from ROZRACH where SPRZEDZESTID = old.ref into :skrot;
      delete from ROZRACHP where SLODEF = :slodef and SLOPOZ = old.sprzedawca and SYMBFAK = :skrot and SKAD = 5;
      select count(*) from ROZRACHP where SLODEF = :slodef and SLOPOZ = old.sprzedawca and SYMBFAK = :skrot into :cnt;
      if(:cnt > 0) then exception SPRZEDZEST_BYLY_WYPLATY;
      delete from ROZRACH where SLODEF = :slodef and SLOPOZ = old.sprzedawca and SYMBFAK = :skrot and SKAD = 5;
   end
   delete from SPRZEDZESTP where ZESTAWIENIE = old.ref;
end^
SET TERM ; ^
