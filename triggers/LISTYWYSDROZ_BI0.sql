--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_BI0 FOR LISTYWYSDROZ                   
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  select w.ktm, w.nazwat, w.x_mws_serie, w.x_mws_partie, w.x_mws_slownik_ehrle, lwp.wersjaref
    from wersje w
      join listywysdpoz lwp on (w.ref = lwp.wersjaref)
    where lwp.ref = new.listywysdpoz
  into new.ktm, new.nazwat, new.x_mws_serie, new.x_mws_partie, new.x_mws_slowniki_ehrle, new.wersjaref;
end^
SET TERM ; ^
