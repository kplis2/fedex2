--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERGRUPY_BU FOR OPERGRUPY                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.symbol <> old.symbol) then
    exception universal 'Nie można zmieniać symbolu grupy operatorów';
end^
SET TERM ; ^
