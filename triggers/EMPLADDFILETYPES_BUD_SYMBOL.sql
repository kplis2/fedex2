--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILETYPES_BUD_SYMBOL FOR EMPLADDFILETYPES               
  ACTIVE BEFORE UPDATE OR DELETE POSITION 0 
as
begin
  if(old.symbol like 'STD_%' and old.symbol is distinct from new.symbol) then
    exception DELETE_STD_ADDFILE;
end^
SET TERM ; ^
