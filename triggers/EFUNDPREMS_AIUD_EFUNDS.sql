--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EFUNDPREMS_AIUD_EFUNDS FOR EFUNDPREMS                     
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
begin
/* W zależnoci od operacji trigger zwieksza lub zmniejsza wartosc oszczednosci */
  if (inserting or deleting or old.premvalue <> new.premvalue) then
  begin
    update efunds
      set deposit = deposit - iif(not inserting, old.premvalue, 0)
                            + iif(not deleting, new.premvalue, 0)
       where ref = coalesce(new.fund,old.fund);
  end
end^
SET TERM ; ^
