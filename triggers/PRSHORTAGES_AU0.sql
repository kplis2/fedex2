--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_AU0 FOR PRSHORTAGES                    
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable amount numeric(14,4);
declare variable kamountnorm numeric(14,4);
declare variable prschedguidespos integer;
begin
  if (coalesce(new.ktm,'') <> coalesce(old.ktm,'') and coalesce(new.ktm,'') = '') then
    exception prshortages_ktm;
    
  if((coalesce(new.amount,0) <> coalesce(old.amount,0)
      or coalesce(old.headergroupamount,0) <> coalesce(new.headergroupamount,0))
   and new.prschedoper is not null ) then
  begin
    update prschedopers pso set pso.amountshortages =
        coalesce((select sum(p.headergroupamount)
          from prshortages p
            join prshortagestypes t on (t.symbol = p.prshortagestype)
          where p.prschedoper = new.prschedoper
            and coalesce(t.noamountinfluence,0) = 0
            and coalesce(p.headergroup, p.ref) = p.ref),0)
      where pso.ref = new.prschedoper;
  end

  if(new.prschedguidespos is not null and (new.prschedguidespos <> old.prschedguidespos
    or new.amount <> old.amount)
  ) then begin
    update prschedguides g set g.pggamountcalc = 1 where g.ref = new.prschedguide;
    for
      select p.ref, coalesce(p.kamountnorm,1)
        from prschedguidespos p
        where p.prschedguide = new.prschedguide
          and p.prschedopernumber <= (select o.number from prschedopers o where o.ref = new.prschedoper)
        into :prschedguidespos, :kamountnorm
    do begin
      amount = 0;
      select sum((s.amount / coalesce(iif(so.reportedfactor is null,o.reportedfactor,so.reportedfactor),1)) * :kamountnorm)
        from prshortages s
          join prshortagestypes t on (t.symbol = s.prshortagestype)
          join prschedopers o on (o.ref = s.prschedoper)
          left join prshopers so on (so.ref = o.shoper)
        where s.prschedguide = new.prschedguide
          and coalesce(t.noamountinfluence,0) = 0
          and coalesce(t.backasresourse,0) = 0
        into :amount;

      select coalesce(sum(s.amount),0) + coalesce(:amount,0)
        from prshortages s
          join prshortagestypes t on (t.symbol = s.prshortagestype)
        where s.prschedguide = new.prschedguide
          and s.prschedguidespos = :prschedguidespos
          and coalesce(t.noamountinfluence,0) = 0
          and coalesce(t.backasresourse,0) = 1
        into :amount;

      select :amount * (select first 1 oo.reportedfactor from prschedopers oo
          where oo.guide = new.prschedguide and oo.activ = 1 and oo.reported > 0
          order by oo.number desc)
        from rdb$database
        into :amount;
      update prschedguidespos p set p.amountshortage = :amount where p.ref = :prschedguidespos;

      kamountnorm = null;
    end
    update prschedguides g set g.pggamountcalc = 0 where g.ref = new.prschedguide;
  end

  if (coalesce(old.prshortagestype,'') <> coalesce(new.prshortagestype,'')
    or coalesce(old.headergroup,0) <> coalesce(new.headergroup,0)
  ) then
    if (exists(select first 1 1 from prshortagestypes t where t.symbol = new.prshortagestype and t.header = 1)
      and coalesce(new.headergroup,0) = 0
    ) then
      exception PRSHORTAGES_ERROR 'Błąd grupy braków';

  if (exists (select first 1 1 from prshortagestypes t where t.symbol = new.prshortagestype and t.backasresourse = 0 and t.header = 1 and t.noamountinfluence = 0)
    and new.headergroupamount <> new.amount
  ) then
    exception PRSHORTAGES_ERROR 'Brak wymiarowy, blad naliczenia ilosci surowca';
end^
SET TERM ; ^
