--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_BI FOR PRSCHEDOPERS                   
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable eopertime varchar(255);
declare variable prsheet integer;
declare variable prcalc integer;
declare variable ret numeric(16,6);
declare variable amountrate float;
declare variable amount numeric(14,4);
declare variable destiny smallint;
declare variable blocked smallint;
begin
    if (new.activ is null) then new.activ = 0;
    if (new.realprecent is null) then new.realprecent = 0;
    if (new.calcrealprecent is null) then new.calcrealprecent = 0;
    if(new.starttime > new.endtime) then exception prschedopers_start_date;
    if(new.guide is null and new.machine is null) then exception prschedopers_nomachine;
    select schedblocked from prschedguides pg where pg.ref = new.guide into :blocked;
    if(new.schedblocked is null) then new.schedblocked = blocked;
    if (new.schedzam is null and new.guide is not null) then
      select g.prschedzam from prschedguides g where g.ref = new.guide into new.schedzam;
    select OPER, eOPERTIME, coalesce(serial,0)
      from PRSHOPERS
      where ref =new.shoper
      into new.oper, :eopertime, new.serial;
    new.worktime = 0;
    if(:eopertime <> '') then begin
      /*select prschedzam.prsheet, prschedzam.prshcalc from prschedzam where ref=new.schedzam into :prsheet, :prcalc;
      new.worktime = null;
      execute procedure STATEMENT_PARSE(:eopertime,:prsheet,:prcalc,'XP_') returning_values :ret;
      if(new.guide is null or new.guide = 0) then
--  todo - naliczanie ilosci i czasow wg przewodnika
--      select kilosc from
      select (case when prsheets.quantity > 0 then :amount / prsheets.quantity else 1 end)
        from prshopers join prsheets  on (prsheets.ref = prshopers.sheet), nagzam
        where nagzam.ref = new.zamowienie and prshopers.ref = new.shoper
      into :amountrate;
      new.worktime = cast(:ret as float) / 24 * :amountrate; */
      execute procedure prschedopers_calc_worktime(new.ref)
        returning_values new.worktime, new.oper;
    end
  execute procedure timediff2str(new.worktime) returning_values new.wortimestr;
  if(new.status is null) then new.status = 0;
  if(new.amountresult is null) then new.amountresult = 0;
  if(new.amountshortages is null) then new.amountshortages = 0;
  if(new.amountin is null) then new.amountin = 0;
  if(new.calcdeptimes is null) then new.calcdeptimes = 1;
  if(new.readytoharm is null) then new.readytoharm = 0;
  if(new.unreported is null) then new.unreported = 0;
  if(new.oper is null and new.shoper is not null) then
    select OPER from prshopers where ref=new.shoper into new.oper;
  --ustawienie aktywnosci
  select destiny from propers where symbol = new.oper into :destiny;
  if(destiny is null or destiny <> 1) then new.activ = 0;
  new.reported = 0;
  if(new.guide = 0) then new.guide = null;
  select prschedzam.number from PRSCHEDZAM where ref=new.schedzam into new.schedzamnum;
  select prschedguides.numer, status from prschedguides where ref=new.guide into new.guidenum, new.guidestatus;
  if(new.schedzam is null) then new.schedzamnum = -1;
  select o.prqcontrolmeth
    from prshopers o
    where o.ref = new.shoper
    into new.prqcontrolmeth;
  if (new.prqcontrolmeth is null) then new.prqcontrolmeth = 0;
end^
SET TERM ; ^
