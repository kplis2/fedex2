--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRCONDIT_BD_LASTANNEXE FOR EMPLCONTRCONDIT                
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable fdate timestamp;
declare variable sym varchar(40);
begin
/*Trigger blokuje skasowanie warunku z aneksu, jezeli na wczesniejszym aneksie badz umowie
  są również warunki dodane. W takim wypadku z poziomu interfejsu nalezy warunek wyzerowac,
  co bedzie skutkowac zmniejszeniem wymiaru urlopu do 0. Jezeli jednak usuwamy caly aneks,
  chcemy usunąć również warunki, dlatego sprawdzamy zmienna kontekstowa ustawiana na triggerze
  ECONTRANNEXES_BD_CONTRCONDIT. Blokuje ona ten trigger i pozwala usunac warunki wraz z aneksem.
*/

  if(rdb$get_context('USER_TRANSACTION', 'DELETEFROMANNEXE') = '1') then
    exit;
                
  select symbol from edictcontrcondit where ref = old.contrcondit
    into sym;

  if(sym in ('DNI_WOLNE_PLATNE', 'DNI_WOLNE_NIEPLATNE')) then
  begin

    select fromdate from emplcontrhist where annexe = old.annexe
      into fdate;

    if (exists(select first 1 1 from emplcontrhist h
                 join emplcontrcondit c on (c.emplcontract = h.emplcontract
                   and coalesce(c.annexe,0) = coalesce(h.annexe,0))
                 where h.emplcontract = old.emplcontract
                   and c.contrcondit = old.contrcondit
                   and fromdate < :fdate)
    ) then
      exception universal'Nie można usunąć warunku. Należy go wyzerować.';
  end
end^
SET TERM ; ^
