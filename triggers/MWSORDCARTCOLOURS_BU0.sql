--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDCARTCOLOURS_BU0 FOR MWSORDCARTCOLOURS              
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.status = 1 and old.status = 0) then
    begin
      new.accepttime = current_timestamp;
      --[PM] XXX jesli do tego koszyka nic nie zebrano to jest pusty xD
      if (not exists (select first 1 1 from mwsacts ma where ma.mwsord = new.mwsord
          and ma.mwsconstloc = new.cart and ma.status < 6)) then
        begin
          update mwsacts set mwsconstloc = null where mwsord = new.mwsord
            and mwsconstloc = new.cart;
          new.status = 2;
        end

    end
end^
SET TERM ; ^
