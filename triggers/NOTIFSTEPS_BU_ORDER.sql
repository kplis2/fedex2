--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSTEPS_BU_ORDER FOR NOTIFSTEPS                     
  ACTIVE BEFORE UPDATE POSITION 20 
as
declare variable maxnr integer;
begin
  if (old.number <> new.number) then
  begin
    select max(n.number)
      from notifsteps n
      where n.notification = new.notification
        and n.ref <> new.ref
      into :maxnr;

    if (:maxnr is null) then maxnr = 0;
    if (new.number > :maxnr + 1 or new.number <=0) then
      new.number = :maxnr + 1;
  end
end^
SET TERM ; ^
