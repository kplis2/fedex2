--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_BIU_VAT FOR BKVATPOS                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
declare variable sell integer;
begin
  --sprawdzany jaki typ dokumentu, jeżeli sprzedaż to przeposujemy vate do vatv dla kompatybilnosci
  --i tak dok. spr. nie podlega odliczeniu va.
  select g.vtype
    from grvat g
    where g.symbol = new.taxgr
    into :sell;
  if (sell = 1) then
  begin
    new.vatv = new.vate;
  end
end^
SET TERM ; ^
