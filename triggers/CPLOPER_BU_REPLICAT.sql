--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLOPER_BU_REPLICAT FOR CPLOPER                        
  ACTIVE BEFORE UPDATE POSITION 4 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.REF <> old.ref)
   or (new.cpl <> old.cpl)
   or (new.cpluczest <> old.cpluczest)
   or(new.data <> old.data ) or (new.data is null and old.data is not null) or (new.data is not null and old.data is null)
   or(new.operator <> old.operator ) or (new.operator is null and old.operator is not null) or (new.operator is not null and old.operator is null)
   or(new.operacja <> old.operacja ) or (new.operacja is null and old.operacja is not null) or (new.operacja is not null and old.operacja is null)
   or(new.typpkt <> old.typpkt ) or (new.typpkt is null and old.typpkt is not null) or (new.typpkt is not null and old.typpkt is null)
   or(new.PM <> old.PM ) or (new.PM is null and old.PM is not null) or (new.PM is not null and old.PM is null)
   or(new.ilpkt <> old.ilpkt ) or (new.ilpkt is null and old.ilpkt is not null) or (new.ilpkt is not null and old.ilpkt is null)
   or(new.opis <> old.opis ) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
   or(new.zreal <> old.zreal ) or (new.zreal is null and old.zreal is not null) or (new.zreal is not null and old.zreal is null)
   or(new.inout <> old.inout ) or (new.inout is null and old.inout is not null) or (new.inout is not null and old.inout is null)
   or(new.dataoper <> old.dataoper ) or (new.dataoper is null and old.dataoper is not null) or (new.dataoper is not null and old.dataoper is null)
   or(new.cplnagzam <> old.cplnagzam ) or (new.cplnagzam is null and old.cplnagzam is not null) or (new.cplnagzam is not null and old.cplnagzam is null)
   or(new.cplregula <> old.cplregula ) or (new.cplregula is null and old.cplregula is not null) or (new.cplregula is not null and old.cplregula is null)
   or(new.typdok <> old.typdok ) or (new.typdok is null and old.typdok is not null) or (new.typdok is not null and old.typdok is null)
   or(new.refdok <> old.refdok ) or (new.refdok is null and old.refdok is not null) or (new.refdok is not null and old.refdok is null)
   )
    then
   waschange = 1;

  execute procedure rp_trigger_bu('CPLOPER',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
