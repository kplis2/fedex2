--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATALP_AD_REPLICAT FOR CKATALP                        
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  update ckatal set state = -1
    where ckatal.ref = old.ckatal;
end^
SET TERM ; ^
