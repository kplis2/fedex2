--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDTYPES_AU0 FOR MWSSTANDTYPES                  
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable counter integer;
begin
  -- zaozenie od nowa poziomów regalu
  if (new.levelsquant <> old.levelsquant) then
  begin
    counter = 0;
    delete from mwsstandleveltypes where mwsstandtype = new.ref;
    while (:counter < new.levelsquant)
    do begin
      if (new.l > 0 and new.mwsstandsegq > 0) then
        insert into mwsstandleveltypes (mwsstandtype, l, number, w)
          values (new.ref, new.w, :counter, new.l/new.mwsstandsegq);
      else
        insert into mwsstandleveltypes (mwsstandtype, l, number, w)
          values (new.ref, new.w, :counter, 0);
      counter = :counter + 1;
    end
  end

  -- zalozenie od nowa segmentow regalu
  if (new.mwsstandsegq <> old.mwsstandsegq) then
  begin
    counter = 0;
    delete from mwsstandsegtypes where mwsstandtype = new.ref;
    while (:counter < new.mwsstandsegq)
    do begin
      if (new.l > 0 and new.mwsstandsegq > 0) then
        insert into mwsstandsegtypes (mwsstandtype, l, symbol, w)
          values (new.ref, new.w, cast(:counter as char(1)), new.l/new.mwsstandsegq);
      else
        insert into mwsstandsegtypes (mwsstandtype, l, symbol, w)
          values (new.ref, new.w, cast(:counter as char(1)), 0);
      counter = :counter + 1;
    end
  end
  if (new.w <> old.w) then
  begin
    update mwsstandleveltypes set l = new.w where mwsstandtype = new.ref;
    update mwsstandsegtypes set l = new.w where mwsstandtype = new.ref;
  end
  if (new.l > 0 and new.mwsstandsegq > 0) then
  begin
    update mwsstandleveltypes set w = new.l/new.mwsstandsegq where mwsstandtype = new.ref;
    update mwsstandsegtypes set w = new.l/new.mwsstandsegq where mwsstandtype = new.ref;
  end else
  begin
    update mwsstandleveltypes set w = 0 where mwsstandtype = new.ref;
    update mwsstandsegtypes set w = 0 where mwsstandtype = new.ref;
  end
end^
SET TERM ; ^
