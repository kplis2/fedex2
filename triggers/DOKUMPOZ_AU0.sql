--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AU0 FOR DOKUMPOZ                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable ii numeric(15,5);
declare variable cnt integer;
declare variable ack integer;
declare variable wyd integer;
declare variable mag char(3);
declare variable typ char(1);
declare variable fifo char(1);
declare variable stan numeric(14,4);
declare variable mag_typ char(1);
declare variable skad smallint;
declare variable dokumentmain integer;
declare variable usluga smallint;
declare variable ilosckor numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable cena numeric(14,4);
declare variable dostawa integer;
begin
  select D.AKCEPT, D.SKAD, D.DOKUMENTMAIN, D.MAGAZYN, D.WYDANIA
    from DOKUMNAG D
    where D.ref = new.dokument
  into :ack, :skad, :dokumentmain, :mag, :wyd;
  if(new.roz_blok <> 1 AND old.roz_blok <> 1) then begin
    if(:ack = 1 or :ack = 8) then begin
      if((new.wersjaref <> old.wersjaref) or (new.ilosc<>old.ilosc) or
         (new.dostawa <> old.dostawa ) or (new.dostawa is not null and old.dostawa is null)) then begin
        exception DOKUMNAG_AKCEPT :ack||'dostawa'||new.dostawa||'stara dostawa'||coalesce(old.dostawa,'brak')||'+'|| new.wersjaref||'+'||new.ilosc||'+'|| new.ref||new.data||'stara data'||coalesce(old.data,'brak');
      end
--      if(new.wersjaref <> old.wersjaref) then begin
         /* jeli zmieni sie ktm lub wersja, nalezy wycofac rozpiski PRZED zmiana zapisow na pozycji
           - dlatego obsluga przeniesiona do trigera before*/
--        ii = new.ilosc;
--      end else begin
         /* tylko zmiana*/
--         ii = new.ilosc - old.ilosc;
         /* jesli dokument przychodowy, a zmienila sie dostawa lub cena, nalezy zmienc
          zapisy bezposrednio na rozpiskach */
--        if((new.dostawa <> old.dostawa ) or (new.cena <> old.cena) or(new.dostawa is not null and old.dostawa is null)) then begin
--          select WYDANIA from DOKUMNAG join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
--           where (DOKUMNAG.REF = new.DOKUMENT) into :wyd;
--          select DEFMAGAZ.TYP from DOKUMNAG
--           left join DEFMAGAZ on( DOKUMNAG.MAGAZYN = DEFMAGAZ.symbol)
--           where DOKUMNAG.ref= new.dokument into :mag_typ;
--          if(:wyd = 0) then begin
--            update DOKUMROZ set CENA = new.cena, dostawa = new.dostawa where pozycja = old.ref;
--          end else begin
            /* przychodowy - jesli magazyn P lub C, to konieczne  wycofanie i wrzucenie s powrotem - ponowne rozpisanie*/
--            if(:mag_typ = 'P' or (:mag_typ = 'C')) then begin
--               ii = -old.ilosc;
--               execute procedure DOKUMPOZ_CHANGE(old.ref, old.ktm, old.wersja, old.cena, old.dostawa, :ii,0,0);
--               ii = new.ilosc;
--            end
--          end
--        end
--      end
--      if(:ii <> 0) then begin
--         execute procedure DOKUMPOZ_CHANGE(old.ref,new.ktm,new.wersja,new.cena,new.dostawa,:ii,0,0);
--      end
      /*kontrola wyslania paczki*/
      if(new.ilosclwys <> old.ilosclwys or (new.iloscl <> old.iloscl and (new.ilosclwys > 0 or new.iloscl = 0 or old.iloscl = 0))) then
        execute procedure DOKUMNAG_CHECKWYS(new.dokument);
      if(new.iloscl <> old.iloscl) then begin
        update DOKUMNAG set STATE = -1 where REF=new.dokument;
      end
    end else if(:ack <> 7) then begin
      /* jesli dokument nie zaakceptowany, a magazyn reczny i dokument wydania, i zmieniła się cena wydania - dostawa, należy to naniesc na rozpiskę*/
      /* jednoczesnie ilosc poprawiamy na taka, jaka dostepna w danym stanie magazynowym. */
      /* dokument reczny nie moze byc poprawiany zaakceptowany - zalozenie. - nie ma wiec ilosci z pozycji na stanach */
      if((new.ilosc <> old.ilosc) or (new.cena <> old.cena) or
         (new.cena is not null and old.cena is null) or
         (new.cenawal <> old.cenawal) or
         (((new.dostawa <> old.dostawa) or(new.dostawa is not null and old.dostawa is null)) and :ack <> 0) or
         (new.wartsnetto <> old.wartsnetto) or (new.foc <> old.foc))
      then begin
        select DEFMAGAZ.FIFO, DEFMAGAZ.TYP, DEFDOKUM.WYDANIA, DOKUMNAG.MAGAZYN
          from DOKUMNAG
          left join DEFMAGAZ on( DOKUMNAG.magazyn = DEFMAGAZ.symbol)
          left join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
          where (DOKUMNAG.REF = new.DOKUMENT)
          into :fifo, :typ, :wyd, :mag;
        if(:fifo = 'R' and (:typ = 'C' or (:typ = 'P')) and :wyd = 1 and :ack <> 9) then begin
          select ILOSC from STANYCEN where MAGAZYN = :mag AND KTM = new.ktm and WERSJA = new.wersja AND CENA=new.cena into :stan;
          if(:stan is null) then stan = 0;
          if(new.ilosc < :stan) then stan = new.ilosc;
          update DOKUMROZ set cena = new.cena, ilosc = :stan where POZYCJA=new.ref and numer = 1;
        end
        execute procedure DOKMAG_OBL_WAR(old.dokument,0);
      end else if(new.iloscl <> old.iloscl) then
        update DOKUMNAG set STATE = -1 where REF=new.dokument;
    end
  end
  if((new.wartosc <> old.wartosc) or (old.ilosc <> new.ilosc)) then begin
    if(new.wartrealpoz > 0) then
      execute procedure POZZAM_OBL(new.wartrealpoz);
    if(new.wartrealzam > 0) then
      execute procedure NAGZAM_OBL(new.wartrealzam);
  end
  if(new.frompozfak > 0 and ((new.wartosc <> old.wartosc) or (new.pwartosc <> old.pwartosc) or (old.frompozfak is null))) then
    execute procedure POZFAK_OBL_KOSZT(new.frompozfak);
  if(new.frompozfak is null and old.frompozfak is not null) then
    execute procedure POZFAK_OBL_KOSZT(old.frompozfak);
  if(new.wersjaref <> old.wersjaref or (new.wersjaref is not null and old.wersjaref is null)) then
    update DOKUMROZ set KTM=new.ktm, WERSJA=new.wersja where POZYCJA = new.ref;

  -- obliczanie ILOSCL dla pozycji uslugowych
  if(new.kortopoz is not null and new.ilosc<>old.ilosc) then begin
    select USLUGA from TOWARY where KTM=new.ktm into :usluga;
    if(:usluga=1) then begin
      select sum(ILOSC) from DOKUMPOZ where KORTOPOZ=new.kortopoz into :ilosckor;
      if(:ilosckor is null) then ilosckor = 0;
      update DOKUMPOZ set ILOSCL = ILOSC - :ilosckor where REF=new.kortopoz;
    end
  end

  --naliczenie realizacji przewodnikow produkcyjnych
  if(:skad >= 3 and (new.ilosc <> old.ilosc
      or new.wersjaref <> old.wersjaref or new.ktm <> old.ktm)
  )then begin
    execute procedure PRSCHEDGUIDEDET_OBL('DOKUMNAG',:skad,new.ref,:ack);
  end
  if(new.roz_blok = 1) then
    update DOKUMPOZ set ROZ_BLOK = 0 where DOKUMPOZ.ref = new.ref;

  -- naliczenie na nowo stanyarch w wyniku zmiany daty zaakceptowanego dokumentu lub dokumentu wycofanego do poprawy
  if(:ack<>0 and new.data<>old.data) then begin
    for select ilosc,cena,dostawa
      from dokumroz where pozycja=new.ref
      into :ilosc, :cena, :dostawa
      do begin
        if(:wyd=1) then begin
          -- najpierw przyjmij ze stara data
          execute procedure STANYARCH_CHANGE('M', :mag, new.ktm, new.wersja, null, old.data, :ilosc, cast(:ilosc * :cena as numeric(14,2)), :dostawa);
          -- potem wydaj z nowa data
          execute procedure STANYARCH_CHANGE('M', :mag, new.ktm, new.wersja, null, new.data, -:ilosc, cast(-:ilosc * :cena as numeric(14,2)), :dostawa);
        end else begin
          -- najpierw wydaj ze stara data
          execute procedure STANYARCH_CHANGE('M', :mag, new.ktm, new.wersja, null, old.data, -:ilosc, cast(-:ilosc * :cena as numeric(14,2)), :dostawa);
          -- potem przyjmij z nowa data
          execute procedure STANYARCH_CHANGE('M', :mag, new.ktm, new.wersja, null, new.data, :ilosc, cast(:ilosc * :cena as numeric(14,2)), :dostawa);
        end
      end
  end
end^
SET TERM ; ^
