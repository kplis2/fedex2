--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCTYPES_BIU0 FOR MWSCONSTLOCTYPES               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.maxpallocq is null) then new.maxpallocq = 0;
  if (new.pallocrequired is null) then new.pallocrequired = 0;
  if (new.stocktaking is null) then new.stocktaking = 0;
  if (new.minuspossible is null) then new.minuspossible = 0;
  if (new.mwsaccesrequired is null) then new.mwsaccesrequired = 0;
  if (new.coordsrequired is null) then new.coordsrequired = 0;
  if (new.goodavmode is null) then new.goodavmode = 0;
end^
SET TERM ; ^
