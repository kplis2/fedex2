--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKEDHOURS_BIUD_CHECKPAYROLL FOR EWORKEDHOURS                   
  ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 3 
as
declare variable period varchar(60);
declare variable employee integer;
declare variable company integer;
begin
/*MWr: nie powinno sie pozwolic na modyfikacje przepracowanych godzin
       gdy listy plac za ten okres sa juz zamkniete (PR44935) */

  if (inserting or deleting
    or new.period <> old.period
    or new.employee <> old.employee
    or new.ecolumn <> old.ecolumn
    or new.amount is distinct from old.amount
    or new.hdate is distinct from old.hdate
  ) then begin
    period = iif(deleting, old.period, new.period);
    employee = iif(deleting, old.employee, new.employee);
  
    select company from employees
      where ref = :employee
      into :company;
  
    if (exists (select first 1 1 from epayrolls
          where cper = :period and empltype = 1 and status > 0 and company = :company)
    ) then
      exception universal 'Próba ' || trim(iif(deleting, 'usunięcia', 'modyfikacji'))
                                   || ' przepracowanych godzin w zamkniętym okresie!';
  end
end^
SET TERM ; ^
