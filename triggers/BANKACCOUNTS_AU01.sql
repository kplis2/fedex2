--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_AU01 FOR BANKACCOUNTS                   
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.gl = 1 and old.gl = 0) or
   (new.gl = 1 and (new.account <> old.account or (new.bank <> old.bank) or (new.bank is not null and old.bank is null) or (new.bank is null and old.bank is not null)))) then begin
    update bankaccounts set GL = 0 where GL = 1 and DICTDEF = new.DICTDEF and DICTPOS = new.DICTPOS and ref <> new.ref;
    if(new.DICTDEF = 1) then
      update KLIENCI set BANK = new.bank, RACHUNEK = new.account where ref = new.DICTPOS;
    else if (new.DICTDEF = 6) then
      update DOSTAWCY set BANK = new.bank, RACHUNEK = new.account where ref = new.DICTPOS;
  end
  if(new.account <> old.account) then begin
    if(new.DICTDEF = 6) then
      update DOSTAWCY set STATE = -1 where REF=new.DICTPOS;
    else if(new.DICTDEF = 1) then
      update KLIENCI set STATE = -1 where REF=new.DICTPOS;
  end
end^
SET TERM ; ^
