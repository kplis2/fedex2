--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_KLIENCI FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slodef integer;
declare variable crm smallint;
declare variable parentkli integer;
begin
  if(new.synchro is null and old.synchro is null) then begin
    select ref from SLODEF where TYP = 'KLIENCI' into :slodef;
    if(:slodef = new.slodef and
     (new.aktywny <> old.aktywny or (new.aktywny is not null and old.aktywny is null) or (new.aktywny is null and old.aktywny is not null) or
      new.skrot <> old.skrot or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.ulica <> old.ulica or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
      new.nrdomu <> old.nrdomu or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null) or
      new.nrlokalu <> old.nrlokalu or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null) or
      new.nip <> old.nip or (new.nip is not null and old.nip is null) or (new.nip is null and old.nip is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.kodp <> old.kodp or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
      new.poczta <> old.poczta or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
      new.kraj <> old.kraj or (new.kraj is not null and old.kraj is null) or (new.kraj is null and old.kraj is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.fax <> old.fax or (new.fax is not null and old.fax is null) or (new.fax is null and old.fax is not null) or
      new.komorka <> old.komorka or (new.komorka is not null and old.komorka is null) or (new.komorka is null and old.komorka is not null) or
      new.www <> old.www or (new.www is not null and old.www is null) or (new.www is null and old.www is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.emailwindyk <> old.emailwindyk or (new.emailwindyk is not null and old.emailwindyk is null) or (new.emailwindyk is null and old.emailwindyk is not null) or
      new.regon <> old.regon or (new.regon is not null and old.regon is null) or (new.regon is null and old.regon is not null) or
      new.firma <> old.firma or (new.firma is not null and old.firma is null) or (new.firma is null and old.firma is not null) or
      new.imie <> old.imie or (new.imie is not null and old.imie is null) or (new.imie is null and old.imie is not null) or
      new.nazwisko <> old.nazwisko or (new.nazwisko is not null and old.nazwisko is null) or (new.nazwisko is null and old.nazwisko is not null) or
      new.uwagi <> old.uwagi or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null) or
      new.niewyplac <> old.niewyplac or (new.niewyplac is not null and old.niewyplac is null) or (new.niewyplac is null and old.niewyplac is not null) or
      new.kodzewn <> old.kodzewn or (new.kodzewn is not null and old.kodzewn is null) or (new.kodzewn is null and old.kodzewn is not null) or
      new.typ <> old.typ or (new.typ is not null and old.typ is null) or (new.typ is null and old.typ is not null) or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null) or
      new.datazal <> old.datazal or (new.datazal is not null and old.datazal is null) or (new.datazal is null and old.datazal is not null) or
      new.cpwoj16m <> old.cpwoj16m or (new.cpwoj16m is not null and old.cpwoj16m is null) or (new.cpwoj16m is null and old.cpwoj16m is not null) or
      new.region <> old.region or (new.region is not null and old.region is null) or (new.region is null and old.region is not null) or
      new.cpowiaty <> old.cpowiaty or (new.cpowiaty is not null and old.cpowiaty is null) or (new.cpowiaty is null and old.cpowiaty is not null) or
      new.parent <> old.parent or (new.parent is not null and old.parent is null) or (new.parent is null and old.parent is not null)
     ))
    then begin
        parentkli = null;
        if (new.parent is not null and new.parent <> 0) then
        begin
          select slopoz from cpodmioty where ref = new.parent and slodef = 1 into :parentkli;
          if (parentkli is null or parentkli = 0) then
            exception universal 'Błąd synchronizacji. Podmiot nadrzędny nie ma odpowiednika w kartotece klientów.';
        end
        update KLIENCI set AKTYWNY = new.aktywny, FSKROT = new.skrot, NAZWA = new.nazwa,
                      ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu, NIP = new.nip, MIASTO = new.miasto,
                      KODP = new.kodp, POCZTA = new.poczta, KRAJ = new.kraj,
                      REGON = new.regon, TELEFON = new.telefon, KOMORKA = new.komorka, EMAIL = new.email, EMAILWINDYK = new.emailwindyk,
                      WWW = new.www, FAX = new.fax, FIRMA = new.firma, IMIE = new.imie, NAZWISKO = new.nazwisko,
                      UWAGI = new.uwagi, NIEWYPLAC = new.niewyplac, TYP = new.typ, KODZEWN = new.kodzewn, KONTOFK = new.kontofk,
                      DATAZAL = new.datazal, CPWOJ16M = new.cpwoj16m, REGION = new.region, HISTORIA = new.historia,
                      cpowiat = new.cpowiaty, parent = :parentkli
          where REF=new.slopoz;
    end
    if (new.slodef=:slodef) then begin
      select CRM from KLIENCI where REF=new.slopoz into :crm;
      if(:crm='0') then begin
        update KLIENCI set CRM=1
        where REF=new.slopoz;
      end
    end
  end
  if(new.synchro=1) then update cpodmioty set synchro=null where ref=new.ref;
  if(new.historia=1) then update cpodmioty set historia=0 where ref=new.ref;
end^
SET TERM ; ^
