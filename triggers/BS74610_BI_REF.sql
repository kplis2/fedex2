--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BS74610_BI_REF FOR BS74610                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
    if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BS74610')
      returning_values new.REF;
end^
SET TERM ; ^
