--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILES_AUID_EVAC FOR EMPLADDFILES                   
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
as
declare variable sym varchar(10);
begin
  select symbol
    from empladdfiletypes
    where ref = coalesce(new.filetype,old.filetype)
    into sym;
  if(sym = 'STD_OPKKOD') then
  begin
    if(new.kind is distinct from old.kind) then
    begin
      if(inserting or updating) then
        update evaclimits set vacmdlimit = case when new.kind = 3 then 0 else new.kind end
          where employee = new.employee
            and vyear <= coalesce(extract(year from new.todate),extract(year from current_date))
            and vyear >= extract(year from new.fromdate);
      if(deleting) then
        update evaclimits set vacmdlimit = 0
          where employee = old.employee
            and vyear <= coalesce(extract(year from old.todate),extract(year from current_date))
            and vyear >= extract(year from old.fromdate);
    end
  end
end^
SET TERM ; ^
