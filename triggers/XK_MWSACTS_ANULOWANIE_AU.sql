--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER XK_MWSACTS_ANULOWANIE_AU FOR MWSACTS                        
  ACTIVE AFTER UPDATE POSITION 10 
AS
declare variable number integer;
declare variable mwsquick numeric(14,4);
declare variable toinsert numeric(14,4);
declare variable quantity numeric(14,4);
declare variable quantityonlocation numeric(14,4);
declare variable todisp numeric(14,4);
declare variable quantitywh numeric(14,4);
declare variable wh varchar(3);
declare variable mwsconstlocp integer;
declare variable mwspallocp integer;
declare variable wharea integer;
declare variable whareap integer;
declare variable whareag integer;
declare variable wharealogp integer;
declare variable cnt integer;
declare variable whsec integer;
declare variable descript varchar(255);
declare variable buffors varchar(255);
declare variable act smallint;
declare variable tmpnumber integer;
declare variable buffor smallint;
begin
  -- dodanie nowej pozycji po anulowaniu
  if (new.status = 6 and old.status <> 6 and new.mwsordtypedest in (1,8)) then
  begin
    execute procedure get_config('MWSTAKEFROMBUFFOR',2) returning_values buffors;
    if (buffors is null or buffors = '') then
      buffor = 0;
    else
      buffor = cast(buffors as smallint);
    select act from mwsconstlocs where ref = old.mwsconstlocp into act;
    if (act = 0) then
      descript = 'pozycje z inwentaryzacji';
    else
      descript = '';
    cnt = 0;
    todisp = old.quantity;
    select wh from mwsconstlocs where ref = old.mwsconstlocp into wh;
    select sum (m.quantity - m.blocked)
      from mwsstock m
        left join mwsconstlocs c on (c.ref = m.mwsconstloc)
        left join whareas w on (w.ref = c.wharea)
        left join wersje v on (v.ref = m.vers)
      where m.wh = :wh and m.vers = new.vers
      and c.act > 0 and c.locdest in (1,2,3,5)
      and (m.lot = new.lot or coalesce(new.lot,0) = 0)
      and ((m.goodsav in (1,2) and :buffor = 0) or :buffor = 1)
    into quantitywh;
    if (quantitywh < todisp) then
      exit;
    else
    begin
      select max(number) from mwsacts where mwsord = new.mwsord into number;
      while (todisp > 0 and cnt < 10)
      do begin
        number = number + 1;
        cnt = cnt + 1; -- zabezpieczenie aby nie latac za duzo razy gdy sie okaze ze nie ma towaru
        whareag = null;
        mwsconstlocp = null;
        select first 1 m.quantity - m.blocked, ml.whsec,
            m.mwspalloc, m.mwsconstloc, m.wharea
          from mwsstock m
            left join mwsconstlocs ml on (ml.ref = m.mwsconstloc and ml.act > 0)
            left join whsecs ws on (ml.whsec = ws.ref and ws.deliveryarea = 2)
          where
            m.vers = old.vers and m.wh = :wh and m.quantity - m.blocked > 0 and ws.ref is not null
            and m.mwsconstloc is not null and m.mwsconstloc <> old.mwsconstlocp
            and ml.act > 0 and ml.locdest in (1,2,3,5)
            and (m.lot = new.lot or coalesce(new.lot,0) = 0)
            and ((m.goodsav in (1,2) and :buffor = 0) or :buffor = 1)
          order by ml.distfromstartarea
          into quantityonlocation, whsec,
             mwspallocp, mwsconstlocp, whareag;
        if (mwsconstlocp is null) then exit;
        if (:quantityonlocation >= todisp) then
          toinsert = todisp;
        else
          toinsert = quantityonlocation;
        todisp = todisp - toinsert;
        if (toinsert > 0) then
        begin
           insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc,
              wh, wharea, whsec,
              regtime, timestartdecl, timestopdecl, mwsaccessory, realtimedecl,
              flags, descript, priority, lot, recdoc, plus,
              whareal, whareap, wharealogl, wharealogp, number, disttogo, palgroup, autogen,
              docgroup) -- XXX KBI
            values(old.mwsord, 1, old.stocktaking, old.good, old.vers, :toinsert, :mwsconstlocp, :mwspallocp,
                old.mwsconstlocl,old.mwspallocl, old.docid, old.docposid, old.doctype, old.settlmode, old.closepalloc,
                :wh, :whareag, :whsec,
                current_timestamp(0), old.timestartdecl, old.timestopdecl, old.mwsaccessory, old.realtime,
                old.flags, :descript, old.priority, null, 0, 1,
                null, :whareap, null, :wharealogp, :number, 5000, null, 1,
                old.docgroup);  -- XXX KBI
        end
        else
          exit;
      end
    end
  end
end^
SET TERM ; ^
