--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_BD_WPK FOR BKACCOUNTS                     
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable ispattern smallint_id;
begin
  -- Sprawdzamy, czy nie jest zalezny
  if (coalesce(old.pattern_ref,0)=0) then
  begin
    -- Jak nie to może jest wzorcowy
    select c.pattern
      from companies c
      where c.ref = old.company
      into :ispattern;
    if (coalesce(ispattern,0)=1)then
      delete from bkaccounts
        where pattern_ref = old.ref;
  end else begin
    rdb$set_context('USER_TRANSACTION', 'CASCADE_DELETE', 'TRUE');
  end
end^
SET TERM ; ^
