--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_BIU0 FOR MWSORDS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.status is null) then new.status = 0;
  if (new.repal is null) then new.repal = 0;
  if (new.palquantity is null) then new.palquantity = 0;
  if (new.manmwsacts is null) then new.manmwsacts = 0;
  if (new.priority is null) then new.priority = 0;
  if (new.nextmwsord is null) then new.nextmwsord = 0;
  if (new.takefullpal is null) then new.takefullpal = 0;
  if (new.autocommit is null) then new.autocommit = 0;
  if (new.gaveout is null) then new.gaveout = 0;
  if (new.multi is null) then new.multi = 0;
  if (new.multicnt is null) then new.multicnt = 0;
  if((new.branch is null) or (new.branch = '')) then  begin
    select ODDZIAL from DEFMAGAZ where SYMBOL = new.wh into new.branch;
    if(new.branch is null or (new.branch = '')) then
      execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.branch;
  end
  if (new.timestop <> old.timestop or new.timestop is not null and old.timestop is null) then
    new.datestop = cast(new.timestop as date);
  if (new.optord is null) then new.optord = 0;
  if (new.source is null) then new.source = 0;

  if (new.mwsordtypedest = 15) then
  begin
    if(coalesce(new.docsymbs,'') = '') then
      exception universal 'Nie wybrano lokacji';

    if (updating and new.status <> old.status and old.status = 5 and
        exists(select first 1 1 from mwsords where mwsordtypedest = 7 and frommwsord = new.ref)) then
      exception universal 'Nie wolno odakceptowywać zlecenia rozliczonego zlecenia inwentaryzacji!';
  end
end^
SET TERM ; ^
