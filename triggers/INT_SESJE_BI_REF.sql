--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_SESJE_BI_REF FOR INT_SESJE                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('INT_SESJE')
      returning_values new.ref;
end^
SET TERM ; ^
