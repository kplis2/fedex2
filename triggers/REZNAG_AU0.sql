--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZNAG_AU0 FOR REZNAG                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable i integer;
declare variable reznagref integer;
declare variable rezzasobref integer;
begin
  select count(*) from REZPOZ where (REZNAG=new.ref) and (REZZASOB=new.rezzasob) into :i;
  if((i=0) or (new.ref<>old.ref)) then
    update REZPOZ set REZNAG=new.ref,REZZASOB=new.rezzasob where (REZNAG=old.ref) and (REZZASOB=old.rezzasob);
  if(new.status<>old.status) then begin
    for /* Iterate for   table */
        select REZNAG,REZZASOB
        from  REZPOZ
        where (REZNAG=new.ref)
        into :reznagref, :rezzasobref
    do begin
      execute procedure rezzasoby_sprawdz(:reznagref,:rezzasobref);
    end /* for   table */
  end
end^
SET TERM ; ^
