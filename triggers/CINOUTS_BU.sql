--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CINOUTS_BU FOR CINOUTS                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.inweight is null) then new.inweight = 0;
  if(new.outweight is null) then new.outweight = 0;
  if(new.docweight is null) then new.docweight = 0;
  if((new.inweight<>old.inweight) or
     (new.outweight<>old.outweight) or
     (new.docweight<>old.docweight)) then begin
    if(new.outweight<>0 and new.docweight<>0) then new.diffweight = new.outweight-new.inweight-new.docweight;
    else new.diffweight = 0;
  end
end^
SET TERM ; ^
