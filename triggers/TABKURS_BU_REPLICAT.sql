--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TABKURS_BU_REPLICAT FOR TABKURS                        
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.ref <> old.ref)
   or(new.bank <> old.bank)
   or(new.data <> old.data)
   or(new.symbol <> old.symbol ) or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null)
   or(new.akt <> old.akt ) or (new.akt is null and old.akt is not null) or (new.akt is not null and old.akt is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('TABKURS',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
