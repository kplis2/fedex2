--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGROUPS_BU0 FOR PRSCHEDGROUPS                  
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable prschedgroup varchar(20);
begin
  if(new.main <> old.main) then begin
    if(new.main = 1) then begin
      select symbol from prschedgroups where main = 1 and symbol <> new.symbol into :prschedgroup;
      if(prschedgroup is not null and prschedgroup <> '')
        then exception prschedules_error 'Zdefiniowano już główną grupę harmonogramów: '||:prschedgroup;
    end
    update prschedules set main = new.main where prschedgroup = new.symbol;
  end
end^
SET TERM ; ^
