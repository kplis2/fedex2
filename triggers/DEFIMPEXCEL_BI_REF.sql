--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFIMPEXCEL_BI_REF FOR DEFIMPEXCEL                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0) then
    execute procedure gen_ref('DEFIMPEXCEL') returning_values new.ref;
end^
SET TERM ; ^
