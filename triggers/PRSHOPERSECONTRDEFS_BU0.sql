--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERSECONTRDEFS_BU0 FOR PRSHOPERSECONTRDEFS            
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.sheet is null and new.prshoper is not null) then
    select sheet from prshopers where ref = new.prshoper into new.sheet;
end^
SET TERM ; ^
