--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_BU0 FOR PRSCHEDGUIDEDETS               
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.ssource <> old.ssource) then
    execute procedure PRSCHEDGUIDEDETS_ACCEPT(new.ref,old.ssource,new.ssource)
      returning_values new.accept;
  -- zródla pochodzenia rozpisek
  if (new.stable is null) then new.stable = '';
  if (new.ssource = 0 and old.ssource <> 0) then
    new.stable = 'DOKUMNAG';
  else if (new.ssource = 1 and old.ssource <> 1) then
    new.stable = 'PRSCHEDGUIDEDETS';
  else if (new.ssource = 2 and old.ssource <> 2) then
    new.stable = 'POZZAM';
  if (new.ssource = 3 and old.ssource <> 3) then
    new.stable = 'OTHER';
end^
SET TERM ; ^
