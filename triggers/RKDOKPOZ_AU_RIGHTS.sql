--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_AU_RIGHTS FOR RKDOKPOZ                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable aktuoper integer;
declare variable konf varchar(255);
declare variable rights varchar(255);
declare variable rightsgroup varchar(255);
declare variable opergroup varchar(255);
declare variable allaclen integer;
declare variable aclen integer;
declare variable dictdef integer;
declare variable data date;
declare variable company integer;
declare variable waluta varchar(3);
declare variable bkaccount integer;
declare variable sdnazwa varchar(20);
declare variable spnazwa varchar(40);
begin
  execute procedure get_global_param ('AKTUOPERATOR') returning_values aktuoper;
  execute procedure get_global_param ('CURRENTCOMPANY') returning_values company;
  execute procedure get_config('DECREESRIGHTS',0) returning_values :konf;
  if (:konf = 1 and (new.konto <> old.konto or new.kwota <> old.kwota)) then
  begin
    select grupa from operator where ref = :aktuoper into :opergroup;
    select data, waluta from rkdoknag where ref = new.dokument into :data, :waluta;

    select ref, rights, rightsgroup from bkaccounts ba where ba.symbol = substring(new.konto from 1 for 3)
           and ba.yearid = extract(year from :data) and ba.company = :company
           into :bkaccount, :rights, :rightsgroup;

    -- weryfikacja dostepu do kont syntetycznych
    select rights, rightsgroup from bkaccounts where ref = :bkaccount
           into :rights, :rightsgroup;
    if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
    begin
      if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
        exception rights 'Brak uprawnień do konta syntetycznego';
    end

    -- weryfikacja dostepu do kont analitycznych
    select rights, rightsgroup from accounting ac where ac.account = new.konto and ac.currency = :waluta
           and ac.yearid = extract(year from :data) and ac.company = :company
           into :rights, :rightsgroup;
    if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
    begin
      if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
        exception rights 'Brak uprawnień do konta analitycznego ' || new.konto;
    end

    -- weryfikacja dostepu do slownikow analitycznych
    allaclen = 4; --dlugosc sytnetyki + separator
    for select acs.len, acs.dictdef from accstructure acs where acs.bkaccount = :bkaccount
        into :aclen, :dictdef
    do begin
       select rights, rightsgroup, slodef.nazwa, sp.kod from slopoz sp left join slodef on (slodef.ref = sp.slownik)
             where sp.slownik = :dictdef
             and sp.kod = substring(new.konto from :allaclen+1 for  :allaclen + :aclen)
             into :rights, :rightsgroup, :sdnazwa, :spnazwa;
       if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
       begin
         if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
           exception rights 'Brak uprawnień do slownika analitycznego: "'||:sdnazwa||'", pozycja "'||:spnazwa||'"';
       end
       allaclen = allaclen + aclen + 1; --dodajemy dlugosc slownika + 1 na separator
    end

    --TO SAMO DLA POPRZEDNIEGO KONTA
    select data, waluta from rkdoknag where ref = old.dokument into :data, :waluta;

    select ref, rights, rightsgroup from bkaccounts ba where ba.symbol = substring(old.konto from 1 for 3)
           and ba.yearid = extract(year from :data) and ba.company = :company
           into :bkaccount, :rights, :rightsgroup;
    -- weryfikacja dostepu do kont syntetycznych
    select rights, rightsgroup from bkaccounts where ref = :bkaccount
           into :rights, :rightsgroup;
    if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
    begin
      if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
        exception rights 'Brak uprawnień do konta syntetycznego';
    end

    -- weryfikacja dostepu do kont analitycznych
    select rights, rightsgroup from accounting ac where ac.account = old.konto and ac.currency = :waluta
           and ac.yearid = extract(year from :data) and ac.company = :company
           into :rights, :rightsgroup;
    if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
    begin
      if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
        exception rights 'Brak uprawnień do konta analitycznego ' || old.konto;
    end

    -- weryfikacja dostepu do slownikow analitycznych
    allaclen = 4; --dlugosc sytnetyki + separator
    for select acs.len, acs.dictdef from accstructure acs where acs.bkaccount = :bkaccount
        into :aclen, :dictdef
    do begin
       select rights, rightsgroup, slodef.nazwa, sp.kod from slopoz sp left join slodef on (slodef.ref = sp.slownik)
             where sp.slownik = :dictdef
             and sp.kod = substring(old.konto from :allaclen+1 for  :allaclen + :aclen)
             into :rights, :rightsgroup, :sdnazwa, :spnazwa;
       if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
       begin
         if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
           exception rights 'Brak uprawnień do slownika analitycznego: "'||:sdnazwa||'", pozycja "'||:spnazwa||'"';
       end
       allaclen = allaclen + aclen + 1; --dodajemy dlugosc slownika + 1 na separator
    end
  end
end^
SET TERM ; ^
