--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDEFKATALP_AD1 FOR CDEFKATALP                     
  ACTIVE AFTER DELETE POSITION 1 
as
declare variable ckatal integer;
begin
  for select ref from CKATAL where CDEFKATAL = old.CDEFKATAL into :ckatal do begin
    delete from CKATALP where CKATAL = :ckatal and SYMBOL = old.akronim;
  end
end^
SET TERM ; ^
