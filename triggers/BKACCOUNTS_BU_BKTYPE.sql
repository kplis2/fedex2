--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_BU_BKTYPE FOR BKACCOUNTS                     
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  if ((new.bktype < 2 and old.bktype = 2) or (new.bktype = 2 and old.bktype < 2)) then
    if (exists (select ref from accounting A where A.bkaccount = new.ref)) then
      exception BKACCOUNT_OPENED;
end^
SET TERM ; ^
