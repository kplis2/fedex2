--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_BD0_RKDOKNAG FOR PERSONS                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists (select ref from rkdoknag R where R.slodef = 9 and R.slopoz = old.ref)) then
    exception universal 'Dla podmiotu jest wystawiony dokument kasowy!';
end^
SET TERM ; ^
