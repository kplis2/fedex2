--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYRULES_AU_CHECKWSCGVALDATES FOR EPAYRULES                      
  ACTIVE AFTER UPDATE POSITION 2 
AS
begin
--Wywolanie kontroli na WSCFGVAL_BIU_CHECKDATES

  if (new.fromdate is distinct from old.fromdate
    or new.todate is distinct from old.todate
    or new.company is distinct from old.company
  ) then
    update wscfgval set epayrule = epayrule
      where epayrule = new.ref and company = new.company;
end^
SET TERM ; ^
