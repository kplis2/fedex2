--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYBUF_BU FOR STANYBUF                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.status=1 and old.status=0) then begin
    update stanyil set stanmax = new.stanmaxreczny
      where magazyn=new.magazyn and wersjaref=new.wersjaref;
    new.ikona = '';
    new.dnibezzmiany = 0;
    new.dniczarne = null;
    new.dniczerwone = null;
    new.dnizolte = null;
    new.dnizielone = null;
  end
end^
SET TERM ; ^
