--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKATAL_AD0 FOR DEFKATAL                       
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  execute procedure DESYNC_DEFKATAL(old.ref);
end^
SET TERM ; ^
