--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DEFCECHY_BUD_XATR FOR DEFCECHY                       
  ACTIVE BEFORE UPDATE OR DELETE POSITION 80 
AS
begin
  --[PM] XXX #ATRYBUTY
  if (updating and old.symbol is distinct from new.symbol
      and exists (select first 1 1 from x_atrybuty where cecha = new.symbol)
     ) then
    exception universal 'Nie mozna zmieniac. Cecha uzywana w X_ATRYBUTY.';
  else if (deleting and exists (select first 1 1 from x_atrybuty where cecha = old.symbol)) then
    exception universal 'Nie mozna usunac. Cecha uzywana w X_ATRYBUTY.';
end^
SET TERM ; ^
