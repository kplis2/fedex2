--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOT_BIU_COMPANY FOR DOKUMNOT                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
AS
begin
  if(inserting or (updating and new.oddzial<>old.oddzial)) then
    select company from oddzialy where oddzial = new.oddzial
      into new.company;
end^
SET TERM ; ^
