--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BUD_CHKCLOSEDPAYROLL FOR EABSENCES                      
  ACTIVE BEFORE UPDATE OR DELETE POSITION 0 
as
declare variable epayroll integer;
declare variable status smallint;
begin
--MWr: Przeciwdzialnie usunieciu lub modyfikacji nieobecnosci rozliczonej na zamknietej liscie plac

  if (deleting) then
    epayroll = old.epayroll;
  else
    epayroll = new.epayroll;

  if (epayroll is not null and (deleting or
    new.payamount <> old.payamount or (new.payamount is null and old.payamount is not null) or (new.payamount is not null and old.payamount is null) or
    new.monthpaybase <> old.monthpaybase or (new.monthpaybase is null and old.monthpaybase is not null) or (new.monthpaybase is not null and old.monthpaybase is null) or
    new.hourpayamount <> old.hourpayamount or (new.hourpayamount is null and old.hourpayamount is not null) or (new.hourpayamount is not null and old.hourpayamount is null) or
    new.daypayamount <> old.daypayamount or (new.daypayamount is null and old.daypayamount is not null) or (new.daypayamount is not null and old.daypayamount is null) or
    new.fbase <> old.fbase or (new.fbase is null and old.fbase is not null) or (new.fbase is not null and old.fbase is null) or
    new.lbase <> old.lbase or (new.lbase is null and old.lbase is not null) or (new.lbase is not null and old.lbase is null) or
    new.ecolumn <> old.ecolumn or (new.ecolumn is null and old.ecolumn is not null) or (new.ecolumn is not null and old.ecolumn is null) or
    new.fromdate <> old.fromdate or (new.fromdate is null and old.fromdate is not null) or (new.fromdate is not null and old.fromdate is null) or
    new.todate <> old.todate or (new.todate is null and old.todate is not null) or (new.todate is not null and old.todate is null) or
    new.employee <> old.employee or (new.employee is null and old.employee is not null) or (new.employee is not null and old.employee is null) or
    new.epayroll <> old.epayroll or (new.epayroll is null and old.epayroll is not null) or (new.epayroll is not null and old.epayroll is null) or
    new.worksecs <> old.worksecs or (new.worksecs is null and old.worksecs is not null) or (new.worksecs is not null and old.worksecs is null) or
    new.emplcontract is distinct from old.emplcontract)
  ) then begin
    select status from epayrolls
      where ref = :epayroll
      into :status;

    if (coalesce(status,0) > 0) then
      exception absence_closedpayroll;
  end
end^
SET TERM ; ^
