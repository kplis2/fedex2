--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERREJ_BD_ORDER FOR DEFOPERREJ                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then
    exit;
  select max(numer) from defoperrej where rejestr = old.rejestr
     into :maxnum;
  if (old.numer = :maxnum) then
    exit;
  update defoperrej set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and rejestr = old.rejestr;
end^
SET TERM ; ^
