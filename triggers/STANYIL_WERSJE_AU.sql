--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_WERSJE_AU FOR STANYIL                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.ilosc <> old.ilosc or new.zamowiono <> old.zamowiono
      or new.zarezerw <> old.zarezerw or new.zablokow <> old.zablokow) then
  begin
    update wersje set wersje.ilosc = wersje.ilosc + new.ilosc - old.ilosc,
        wersje.zamowiono = wersje.zamowiono + new.zamowiono - old.zamowiono,
        wersje.zarezerw = wersje.zarezerw + new.zarezerw - old.zarezerw,
        wersje.zablokow = wersje.zablokow + new.zablokow - old.zablokow
      where wersje.ref = new.wersjaref;
  end
end^
SET TERM ; ^
